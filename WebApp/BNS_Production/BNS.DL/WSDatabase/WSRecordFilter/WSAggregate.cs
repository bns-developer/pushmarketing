﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace WSDatabase
{
    public class WSAggregate
    {


        public string Field { get; set; }
        public string Type { get; set; }
        public WSRecordsFilter filter { get; internal set; }

      

        public WSAggregate(JObject aggregate)
        {
            if (JSONHelper.isValidFieldForKey(aggregate, "Field"))
                this.Field = (string) aggregate["Field"];

            if (JSONHelper.isValidFieldForKey(aggregate, "Type"))
                this.Type =(string)aggregate["Type"];
        }

        void setDefault()
        {
        }
        public Dictionary<string, object> toDictionary()
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            this.setDefault();
            return parameters;
        }
        public string toQuery()
        {
            this.setDefault();
            String query = "";
            return query;
        }
    }
}