﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace WSDatabase
{
    public class WSRecordsFilter
    {
        public string Alias { get; set; }
        public List<WSAggregate> Aggregates { get; set; }
        public List<string> Expand { get; set; }
        public List<string> Group { get; set; }
        public bool RequiresCounts { get; set; }
        public List<WSSearchFilter> Search { get; set; }
        public List<string> Select { get; set; }
        public int Skip { get; set; }
        public List<WSSort> Sorted { get; set; }
        public string Table { get; set; }
        public int Take { get; set; }
        public List<WSWhereFilter> Where { get; set; }
       
        public WSRecordsFilter()
        {
            this.Where = new List<WSWhereFilter>();
            this.Sorted = new List<WSSort>();
            this.Search = new List<WSSearchFilter>();
            this.Aggregates = new List<WSAggregate>();
            this.Select = new List<string>();
            this.Group = new List<string>();
            this.Expand = new List<string>();
        }

        public WSRecordsFilter(JObject requestJSON)
        {
            if (JSONHelper.isValidFieldForKey(requestJSON, "Take"))
                this.Take = (Int32)requestJSON["Take"];

            if (JSONHelper.isValidFieldForKey(requestJSON, "Skip"))
                this.Skip = (Int32)requestJSON["Skip"];

            if (JSONHelper.isValidFieldForKey(requestJSON, "RequiresCounts"))
                this.RequiresCounts = (Boolean)requestJSON["RequiresCounts"];


            if (JSONHelper.isValidFieldForKey(requestJSON, "Where"))
            {
                this.Where = new List<WSWhereFilter>();
                JArray Where = (JArray)requestJSON["Where"];
                foreach (JObject WhereCondition in Where)
                {
                    WSWhereFilter Wherefilter = new WSWhereFilter(WhereCondition);
                    Wherefilter.filter = this;
                    this.Where.Add(Wherefilter);
                }
            }
            this.Sorted = new List<WSSort>();
            if (JSONHelper.isValidFieldForKey(requestJSON, "Sorted"))
            {
                JArray Sorted = (JArray)requestJSON["Sorted"];
                foreach (JObject sort in Sorted)
                {
                    WSSort sortFilter = new WSSort(sort);
                    sortFilter.filter = this;
                    this.Sorted.Add(sortFilter);
                }
            }
            this.Search = new List<WSSearchFilter>();
            if (JSONHelper.isValidFieldForKey(requestJSON, "Search"))
            {
                JArray Search = (JArray)requestJSON["Search"];
                foreach (JObject search in Search)
                {
                    WSSearchFilter searchFilter = new WSSearchFilter(search);
                    searchFilter.filter = this;
                    this.Search.Add(searchFilter);
                }
            }
            this.Group = new List<string>();
            if (JSONHelper.isValidFieldForKey(requestJSON, "Group"))
            {
                JArray Group = (JArray)requestJSON["Group"];
                foreach (JObject group in Group)
                {
                    this.Group.Add((String)group);
                }
            }
            this.Aggregates = new List<WSAggregate>();
            if (JSONHelper.isValidFieldForKey(requestJSON, "Aggregates"))
            {
                JArray Aggregates = (JArray)requestJSON["Aggregates"];
                foreach (JObject aggregate in Aggregates)
                {
                    WSAggregate aggregateFilter = new WSAggregate(aggregate);
                    aggregateFilter.filter = this;
                    this.Aggregates.Add(aggregateFilter);
                }
            }
            this.Expand = new List<string>();
            if (JSONHelper.isValidFieldForKey(requestJSON, "Expand"))
            {
                JArray Expand = (JArray)requestJSON["Expand"];
                foreach (JObject expand in Expand)
                {
                    this.Expand.Add((String)expand);
                }
            }
            this.Select = new List<string>();
            if (JSONHelper.isValidFieldForKey(requestJSON, "Select"))
            {
                JArray Select = (JArray)requestJSON["Select"];
                foreach (JObject select in Select)
                {
                    this.Select.Add((String)select);
                }
            }
        }

        void setDefault()
        {
            if (this.Sorted != null && this.Sorted.Count == 0)
            {
                WSSort sort = new WSSort();
                sort.Direction = WSDatabaseConfiguration.DEFAULT_SORT_FIELD;
                sort.Name = WSDatabaseConfiguration.DEFAULT_SORT_FIELD;
                sort.filter = this;
                this.Sorted.Add(sort);
            }

            if (this.Skip == 0)
                this.Skip = WSDatabaseConfiguration.DEFAULT_OFFEST;
            if (this.Take == 0)
                this.Take = WSDatabaseConfiguration.DEFAULT_NUMBER_OF_RECORDS;
        }
        public Dictionary<string, object> toDictionary()
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            //this.setDefault();
            return parameters;
        }

        public string toQuery()
        {
            this.setDefault();

            String query = "";

            //Where Filter 
            if (this.Where != null && this.Where.Count != 0)
            {
                String filterString = "";
                foreach (WSWhereFilter filter in this.Where)
                {
                    filterString = filterString + filter.toQuery() + ",";
                }
                filterString = filterString.Substring(0, filterString.Length - 2);
                query = query + " WHERE " + filterString;
            }





            //Grouping
            if (this.Group != null && this.Group.Count != 0)
            {
                String GroupString = "GROUP BY ";
                foreach (string column in this.Group)
                {
                    GroupString = GroupString + column + ", ";
                }
                GroupString = GroupString.Substring(0, GroupString.Length - 2);
                query = query + " " + GroupString;
            }

            if (this.Sorted != null && this.Sorted.Count != 0)
            {
                string sortQuery = "";
                //Sort by
                foreach (WSSort sort in this.Sorted)
                {
                    sortQuery = sortQuery + " " + sort.toQuery() + ", ";
                }
                sortQuery = sortQuery.Substring(0, sortQuery.Length - 2);
                query = query + " " + sortQuery;
            }


            //Skip
            
                query = query + " OFFSET " + this.Skip + " ROWS";

            //Take
            if (this.Take != 0)
                query = query + " FETCH NEXT " + this.Take + " ROWS ONLY ";
            return query;
        }
    }
}