﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WSDatabase
{ 
    public class WSQuery
    {
        public String query { get; set; }

        public WSRecordsFilter filter { get; set; }
        public Dictionary<String, Object> parameters { get; set; }

        public SqlConnection connection;

        public WSTransaction transaction;

        public WSQuery()
        {
         
            this.parameters = new Dictionary<String, Object>();
        }
        public WSQuery(string query)
        {
         
            this.query = query;
            this.parameters = new Dictionary<String, Object>();
        }
        public WSQuery(string query, Dictionary<String, Object> parameters)
        {
         
            this.query = query;
            this.parameters = parameters;
        }
        public WSQuery(string query, Dictionary<String, Object> parameters, WSTransaction transaction)
        {
         
            this.query = query;
            this.transaction = transaction;
            this.parameters = parameters;
        }
        public WSQuery(string query, Dictionary<String, Object> parameters, SqlConnection connection)
        {
            this.query = query;            
            this.parameters = parameters;
        }
        public void cloneConnection()
        {
            this.connection.Close();
            this.connection.Dispose();
        }
    }
}