﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WSDatabase
{
    
    public class WSDatabaseHelper
    {
        public static Boolean isValidField(SqlDataReader reader, string ColumnName)
        {
            if (reader.GetSchemaTable().Rows.OfType<DataRow>().Any(row => row["ColumnName"].ToString() == ColumnName))
            {
                if (reader.IsDBNull(reader.GetOrdinal(ColumnName)))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
            //try
            //{
            //    if (reader.IsDBNull(reader.GetOrdinal(ColumnName)))
            //    {
            //        return false;
            //    }
            //    else
            //    {
            //        return true;
            //    }

            //}
            //catch (IndexOutOfRangeException)
            //{
            //    return false;
            //}
        }

        public static bool isValidField(DataRow row, string v)
        {
            try
            {
                if (row.IsNull(v))
                {
                    return false;
                }
                else
                {
                    return true;
                }

            }
            catch (ArgumentException)
            {
                return false;
            }
        }
    }
}