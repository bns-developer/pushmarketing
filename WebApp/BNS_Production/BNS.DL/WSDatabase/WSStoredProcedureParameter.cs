﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

namespace WSDatabase
{
    public class WSStoredProcedureParameter
    {
        public WSStoredProcedureParameter(Object value, DbType type)
        {
            this.value = value;
            this.dbType = type;

        }
        public DbCommand command { get; set; }
        public Object value { get; set; }
        public DbType dbType { get; set; }
    }
}