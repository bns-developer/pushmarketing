﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WSDatabase
{
    public class WSTransaction : IDisposable
    {        
        public SqlTransaction transaction;
        public SqlConnection connection;
        public WSTransaction(String name)
        {
            connection = new SqlConnection(WSDatabaseConfiguration.CONNECTION_STRING);
            connection.Open();
            transaction = connection.BeginTransaction(name);

        }
        public void commit()
        {
            transaction.Commit();
            connection.Close();
        }
        public void rollback()
        {
            transaction.Rollback();
            connection.Close();
        }

        public void Dispose()
        {
            transaction.Dispose();
            connection.Dispose();
        }
    }
}