angular.module('bns.signup', [])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
    .state('signup', {
        url: "/signup",
        views: {
            "signup": {
                templateUrl: "App/Signup/signup.html",
                controller: 'signupCtrl'
            },
        },
    });
}])
.controller('signupCtrl', ['$state', '$scope', '$rootScope', '$http', 'API_URL', 'API_SERVICE', function ($state, $scope, $rootScope, $http, API_URL, API_SERVICE) {

    $scope.registerButtonName = "Register";
    $scope.submitted = false;
    $scope.dateError = false;
    $scope.successAlert = false;
    $scope.faliureAlert = false;
    $scope.userExist = false;
    $scope.image = "";
    var today = new Date();
    today.setHours(0, 0, 0, 0);

    $scope.newAccount = {
        "name": "",
        "username": "",
        "password": "",
        "website": "",
        "email": "",
        "logo": ""
    };

    $scope.submitForm = function (isValid, form) {
        $scope.submitted = true;
        if (isValid) {            
            $scope.addNewAccount();
        }
        else {
            return false;
        }
    }

    $scope.addNewAccount = function () {
        $scope.registerButtonName = "Registering";
        //console.log($scope.image.file);

        $scope.newAccount.imagePath = $scope.image[0];

        $scope.dateError = false;
        function success(data, status, headers, config) {
            $scope.submitted = false;
            $scope.notify("User registered successfully.", 1);
            $scope.successAlert = true;
            $scope.faliureAlert = false;
            $scope.newAccount = {
                "name": "",
                "username": "",
                "password": "",
                "website": "",
                "email": "",
                "logo": ""
            };
            $scope.loading = false;
            $scope.userExist = false;
            $scope.registerButtonName = "Register";
            $state.go("login");

        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.registerButtonName = "Register";
            $scope.successAlert = false;
            $scope.faliureAlert = true;
            if (data.status == 409) {
                $scope.userExist = true;
                $scope.errorMessage = data.data;
            }
            else {
                //console.log(data);
                $scope.errorMessage = "Could not register. Please check all the fields."
            }
        }
        var header = { 'Content-Type': 'application/json', 'Access-Control-Allow-Headers': 'Content-Type','Access-Control-Allow-Methods: GET':'POST, OPTIONS', 'Access-Control-Allow-Origin':'*'};
        var url = "/accounts/add";
        API_SERVICE.postMultiPartData($scope, $http, $scope.newAccount, url, success, failure);
       
    }
}])
