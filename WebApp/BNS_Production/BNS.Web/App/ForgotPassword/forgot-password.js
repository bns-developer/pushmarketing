angular.module('bns.forgotPassword', [])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
    .state('forgotPassword', {
        url: "/forgot-password",
        views: {
            "forgotPassword": {
                templateUrl: "App/ForgotPassword/forgot-password.html",
                controller: 'forgotPasswordCtrl'
            },
        },
    });
}])
.controller('forgotPasswordCtrl', ['$state', '$scope', '$http', 'API_URL', 'API_SERVICE', '$rootScope', function ($state, $scope, $http, API_URL, API_SERVICE, $rootScope) {
    $scope.forgotPassword = { "email": "" };
    $scope.SendButtonName = "Send";
    $scope.successAlert = false;
    $scope.faliureAlert = false;


    $scope.submitForm = function (isValid, form) {
        $scope.submitted = true;
        if (isValid) {
            forgotPassword();
        }
        else {
            return false;
        }
    }

    function forgotPassword() {
        $scope.SendButtonName = "Loading...";
        $scope.submitted = true;
        if ($scope.forgotPassword.email == "" || $scope.forgotPassword.email == undefined) {
            $scope.SendButtonName = "Send";
        }
        else {
            function success(data, status, headers, config) {
                $scope.notify("Check your email.", 1);
                $scope.submitted = false;
                $scope.successAlert = true;
                $scope.faliureAlert = false;             
                $scope.forgotPassword = {
                    "email": ""
                };
                $scope.loading = false;
                $scope.SendButtonName = "Send";
            }
            function failure(data, status, headers, config) {
                $scope.loading = false;
                $scope.successAlert = false;
                $scope.faliureAlert = true;
                $scope.SendButtonName = "Send";
                if (status == 409) {
                    $scope.errorMessage = data;
                }
                else {
                    $scope.errorMessage = "Could not process your request."
                }
            }
            var url = "/accounts/forgotPassword";
            API_SERVICE.postData($scope, $http, $scope.forgotPassword, url, success, failure);
        }
    }


    }])
