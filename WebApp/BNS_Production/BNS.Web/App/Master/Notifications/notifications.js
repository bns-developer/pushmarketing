﻿angular.module('bns.master.notifications', [])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.notifications', {
        url: '/notifications',
        views: {
            "notifications": {
                templateUrl: 'App/Master/Notifications/notifications.html',
                controller: 'notificationsController'
            }
        }
    });
}])
.controller("notificationsController", ['$scope', '$state', '$http', '$timeout', '$window', 'API_URL', 'API_SERVICE', 'ngDialog', 'DTOptionsBuilder', 'DTColumnDefBuilder', function ($scope, $state, $http, $timeout, $window, API_URL, API_SERVICE, ngDialog, DTOptionsBuilder, DTColumnDefBuilder) {
   
    $scope.notificationUserListLoader = 1;
    var date = new Date();
    $scope.submitted = false;
    $scope.successAlert = false;
    $scope.faliureAlert = false;
    $scope.isUserWithNoContact = false;
    $scope.dtOptionsNotifications = DTOptionsBuilder.newOptions()
        .withDOM('lrftip')
        .withPaginationType('full_numbers')
        .withOption('responsive', true)
        .withOption('scrollX', true)
        //.withOption('bAutoWidth', false)
        //.withOption('scrollCollapse', 'true')
        .withOption('order', [])
        .withButtons([
            {
                extend: 'csvHtml5',
                orientation: 'landscape',
                title: $window.document.title + ' :- Notifications User List' + moment().format('MMMM Do YYYY, h:mm:ss a'),
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [1, 2, 3, 4, 5, 6, 7]
                }
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                title: $window.document.title + ' :- Notifications User List' + moment().format('MMMM Do YYYY, h:mm:ss a'),
                pageSize: 'LEGAL',
                //text: '<i class="fa fa-file-pdf-o"> PDF</i>',
                exportOptions: {
                    columns: [1, 2, 3, 4, 5, 6, 7]
                }
            },
            {
                extend: 'excelHtml5',
                orientation: 'landscape',
                title: $window.document.title + ' :- Notifications User List' + moment().format('MMMM Do YYYY, h:mm:ss a'),
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [1, 2, 3, 4, 5, 6, 7]
                }
            },
            'print']);

    $scope.dtColumnDefsNotificationUserList = [DTColumnDefBuilder.newColumnDef(0).notSortable(), DTColumnDefBuilder.newColumnDef(6).notSortable(), DTColumnDefBuilder.newColumnDef(7).notSortable()];
    $scope.sendButtonName = "Send";
    $scope.changeBreadcrums([{ "name": "Notifications", "state": "bns.master.notifications" }]);
    $scope.allowedContactTypes = [{ "id": "1", "label": "Email" }, { "id": "2", "label": "Phone" }, { "id": "3", "label": "Allowed push notification" }];
    $scope.contactTypes = [{ "id": "1", "label": "Email" }, { "id": "2", "label": "Text Message" }, { "id": "3", "label": "Push Notification" }];
    $scope.userTypes = [{ "id": "1", "label": "Member" }, { "id": "2", "label": "Non-Member" }];
    $scope.notifications = {
        "allowedContactTypes": [],
        "contactType": "",
        "userTypes": []
    }
    $scope.notifications1 = {
        "contactTypes": [{ "id": "1", "label": "Email" }, { "id": "2", "label": "Text Message" }, { "id": "3", "label": "Allowed push notification" }]
    }

    $scope.contactList = [];

    $scope.notificationMessage = {
        "type": "",
        "message": "",
        "subject": "",
        "users": []
    };

    //--------------------------------------  raw variables to retain data ----------------------------------------------------------//
    $scope.emailNotificationModel = {
        "emailIDList": "Email",
        "message": ""
    };
    $scope.textNotificationModel = {
        "contactList": "Text",
        "message": ""
    };
    $scope.pushNotificationModel = {
        "contactList": "Push Notification",
        "message": ""
    };

    //------------------------------------------- Email Modal ---------------------------------------------------------------//
    openSendEmailNotificationModal = function () {
        ngDialog.open({
            template: 'sendEmailNotificationDialog',
            className: 'ngdialog-theme-default modal-large notifications-dialog draggable resizeable',
            scope: $scope,
            closeByDocument: false,
            preCloseCallback: function () { $scope.clearSendEmailNotificationModal() }

        });
        $timeout(function () {
            //$(".resizeable .ngdialog-content").resizable({
            //});
            $(".draggable .ngdialog-content").draggable();
        }, 500);
    }

    $scope.closeSendEmailNotificationModal = function () {
        ngDialog.close({
            template: 'sendEmailNotificationDialog',
            scope: $scope
        });
        $scope.clearSendEmailNotificationModal();
    }

    $scope.clearSendEmailNotificationModal = function () {
        $scope.submitted = false;
        $scope.successAlert = false;
        $scope.faliureAlert = false;
        $scope.isUserWithNoContact = false;
        $scope.notificationMessage = {
            "type": "",
            "message": "",
            "subject": "",
            "users": []
        };
    }

    //------------------------------------------- Text Message Modal -----------------------------------------------------------------//
       openSendTextNotificationModal = function () {
        ngDialog.open({
            template: 'sendTextMessageNotificationDialog',
            className: 'ngdialog-theme-default modal-large notifications-dialog draggable resizeable',
            scope: $scope,
            closeByDocument: false,
            preCloseCallback: function () { $scope.clearSendTextNotificationModal() }

        });
        $timeout(function () {
            //$(".resizeable .ngdialog-content").resizable({
            //});
            $(".draggable .ngdialog-content").draggable();
        }, 500);
    }

       $scope.closeSendTextNotificationModal = function () {
        ngDialog.close({
            template: 'sendTextMessageNotificationDialog',
            scope: $scope
        });
        $scope.clearSendTextNotificationModal();
    }

       $scope.clearSendTextNotificationModal = function () {
        $scope.submitted = false;
        $scope.successAlert = false;
        $scope.faliureAlert = false;
        $scope.isUserWithNoContact = false;
        $scope.notificationMessage = {
            "type": "",
            "message": "",
            "subject": "",
            "users": []
        };
       }

    //--------------------------------------- Push Notification Modal -------------------------------------------------------//
       openSendPushNotificationModal = function () {
           ngDialog.open({
               template: 'sendPushNotificationDialog',
               className: 'ngdialog-theme-default modal-large notifications-dialog draggable',
               scope: $scope,
               closeByDocument: false,
               preCloseCallback: function () { $scope.clearSendPushNotificationModal() }

           });
           $timeout(function () {
               $(".draggable .ngdialog-content").draggable();
           }, 500);
       }
       $scope.closeSendPushNotificationModal = function () {
           ngDialog.close({
               template: 'sendPushNotificationDialog',
               scope: $scope
           });
           $scope.clearSendPushNotificationModal();
       }
       $scope.clearSendPushNotificationModal = function () {
           $scope.submitted = false;
           $scope.successAlert = false;
           $scope.faliureAlert = false;
           $scope.isUserWithNoContact = false;
           $scope.notificationMessage = {
               "type": "",
               "message": "",
               "subject": "",
               "users": []
           };
       }
    //-----------------------------------------------------------------------------------------------------------------------//
    $scope.allowedContactTypesDropDownSetting = { smartButtonMaxItems: 3, smartButtonTextConverter: function (itemText, originalItem) { return itemText; }, scrollable: false };
    $scope.allowedContactTypesButtonText = { buttonDefaultText: 'Choose contact type...', checkAll: 'Select All', uncheckAll: 'Deselect All' };
    $scope.userTypesDropDownSetting = { smartButtonMaxItems: 3, smartButtonTextConverter: function (itemText, originalItem) { return itemText; }, scrollable: false };
    $scope.userTypesButtonText = { buttonDefaultText: 'Choose user type...', checkAll: 'Select All', uncheckAll: 'Deselect All' };

    function checkUserWithNoEmailContact(userList)
    {
        $scope.isUserWithNoContact = false;
        for (var i = 0; i < userList.length; i++) {
            if (userList[i].hasOwnProperty('contacts')) {
                for (var j = 0; j < userList[i].contacts.length; j++) {
                    if (userList[i].contacts[j].ContactType == 'EMAIL' && (userList[i].contacts[j].value == '' || userList[i].contacts[j].value == undefined)) {
                        $scope.isUserWithNoContact = true;
                        break;
                    }
                    else {
                        continue;
                    }
                }
            }
            else {
                $scope.isUserWithNoContact = true;
                break;
            }
        }
        openSendEmailNotificationModal();
    }

    function checkUserWithNoPhoneContact(userList) {
        $scope.isUserWithNoContact = false;
        for (var i = 0; i < userList.length; i++) {
            if (userList[i].hasOwnProperty('contacts')) {
                for (var j = 0; j < userList[i].contacts.length; j++) {
                    if (userList[i].contacts[j].ContactType == 'PHONE' && (userList[i].contacts[j].value == '' || userList[i].contacts[j].value == undefined)) {
                        $scope.isUserWithNoContact = true;
                        break;
                    }
                    else {
                        continue;
                    }
                }
            }
            else {
                $scope.isUserWithNoContact = true;
                break;
            }
        }
        openSendTextNotificationModal();
    }

    function checkHasEmailContacts(userList)
    {
        var isContainsContact = false;
        for(var i=0; i < userList.length; i++)
        {
            if (userList[i].hasOwnProperty('contacts'))
            {
                for (var j = 0; j < userList[i].contacts.length; j++) {
                    if (userList[i].contacts[j].ContactType == 'EMAIL') {
                        isContainsContact = true; 
                        break;
                    }
                }
            }
        }
        if (isContainsContact)
        {
            checkUserWithNoEmailContact(userList);
            //openSendEmailNotificationModal();
        }
        else {
            $scope.notify("Please select at least one user with Email ID!.", 3);
        }
    }
    function checkHasPhoneContacts(userList) {
        var isContainsContact = false;
        for (var i=0; i<userList.length; i++) {
            if (userList[i].hasOwnProperty('contacts')) {
                for (var j = 0; j < userList[i].contacts.length; j++)
                {
                    if (userList[i].contacts[j].ContactType == 'PHONE') {
                        isContainsContact = true;
                        break;
                    }
                }
            }
        }
        if (isContainsContact)
        {
            checkUserWithNoPhoneContact(userList)
           // openSendTextNotificationModal();
        }
        else{
            $scope.notify("Please select at least one user with phone number!", 3);
        }
    }

    function checkUserWithNoPushNotificationId(userList) {
        $scope.isUserWithNoContact = false;
        for (var i = 0; i < userList.length; i++) {
            if (userList[i].hasOwnProperty('userDevices')) {
                var userDevices = userList[i].userDevices;
                if (userDevices.length < 1)
                {
                    $scope.isUserWithNoContact = true;
                    break;
                }
                for (var j = 0; j < userDevices.length; j++)
                {
                    var pushID = userDevices[j].push_id;
                    if (pushID == '' || pushID == undefined) {
                        $scope.isUserWithNoContact = true;
                        break;
                    }
                }
            }
            else {
                $scope.isUserWithNoContact = true;
                break;
            }
        }
        openSendPushNotificationModal();
    }

    function checkHasPushNotificationContacts(userList, type) {
        var isContainsContact = false;
        for (var i = 0; i < userList.length; i++) {
            if (userList[i].hasOwnProperty('userDevices')) {
                var userDevices = userList[i].userDevices;
                if (userDevices.length < 1)
                {
                    continue;
                }
                for (var j = 0; j < userDevices.length; j++)
                {
                    var pushID = userDevices[j].push_id;
                    if (pushID != '' || pushID != undefined) {
                        isContainsContact = true;
                        break;
                    }
                }
            }
        }
        if (isContainsContact) {
            checkUserWithNoPushNotificationId(userList);
        }
        else {
            $scope.notify("Please select at least one user who has opted in for push notifications!", 3);
        }
    }

    $scope.sendNotification = function(contactType)
    {
        switch(contactType)
        {
            case "1":
                checkHasEmailContacts($scope.contactList);
                break;
            case "2":
                checkHasPhoneContacts($scope.contactList);
                break;
            case "3":
                checkHasPushNotificationContacts($scope.contactList);
                break;
            default:
                $scope.notify("Please select how do you want to notify the users!", 3);
                break;
        }
    };

    //---------------------------- selectAll----------------------------------------------//
    $scope.selectAll = function()
    {
        $scope.contactList = [];
        for (var i = 0; i < $scope.notifiactionUsers.length; i++)
        {
            var user = $scope.notifiactionUsers[i];
            user.isChecked = true;
            $scope.contactList.push(user);
        }
    }
    $scope.deselectAll = function () {
        $scope.contactList = [];
        for (var i = 0; i < $scope.notifiactionUsers.length; i++) {
            $scope.notifiactionUsers[i].isChecked = false;
        }
    }

    //------------------------------- API's -----------------------------------------------//
    
    preProcessContactList =  function() {
        var contactList = [];
        var user = {};
        for(var i=0; i<$scope.contactList.length; i++ )
        {
            var tempUser = {};
            user = $scope.contactList[i];
            if (user.hasOwnProperty("id")) {
                tempUser.id = user.id;
            }
            if (user.hasOwnProperty("contacts")) {
                tempUser.contacts = user.contacts;
            }
            if (user.hasOwnProperty("userDevices")) {
                tempUser.userDevices = user.userDevices;
            }
            contactList.push(tempUser);
        }
        return contactList;
    }

    $scope.sendNotificationMessage = function (type) {
        $scope.sendButtonName = "Sending";
        $scope.notificationMessage.type = type;
        $scope.notificationMessage.users = preProcessContactList();

        $scope.submitted = true;
        if ($scope.notificationMessage.message == "" || $scope.notificationMessage.message == undefined) {
            $scope.sendButtonName = "Send";
            if(type==1)
            {
                if ($scope.notificationMessage.subject == ""  || $scope.notificationMessage.subject == undefined) {
                    $scope.sendButtonName = "Send";
                }
            }
        }
        else
        {
            function success(data, status, headers, config) { 
                $scope.submitted = false;
                $scope.successAlert = true;
                $scope.faliureAlert = false;
                switch (type) {
                    case 1:
                        $scope.notify("Emails sent succesfully",1);
                        $scope.closeSendEmailNotificationModal();
                        break;
                    case 2:
                        $scope.notify("Messages sent succesfully",1);
                        $scope.closeSendTextNotificationModal();
                        break;
                    case 3:
                        $scope.notify("Push notifications sent succesfully",1);
                        $scope.closeSendPushNotificationModal();
                        break;
                    default:
                        break;
                }
                $scope.sendButtonName = "Send";
            }
            function failure(data, status, headers, config) {
                $scope.submitted = false;
                $scope.successAlert = false;
                //$scope.faliureAlert = true;
                $scope.sendButtonName = "Send";
                if (status == 409) {
                    //$scope.errorMessage = data;
                    $scope.notify(data, 4);
                }
                else if (status == 500)
                {

                    $scope.notify("Some error occurred. Please try again later", 5);
                }
                else {
                    //$scope.errorMessage = "Could not send notification. Please retry again.";
                    $scope.notify($scope.errorMessage, 4);
                }
                $scope.closeSendPushNotificationModal();
            }
            var url = "/users/notifications";
            API_SERVICE.postData($scope, $http, $scope.notificationMessage, url, success, failure);
        } 
    }

    $scope.addOrRemoveContact = function(user)
    {
        if (user.isChecked)
        {
            $scope.contactList.push(user);
        }
        else {
            var index = $scope.contactList.indexOf(user);
            if (index > -1) {
                $scope.contactList.splice(index, 1);
            }
        }    
    }

    //----------------------API's----------------------------------//
    getNotificationUserList = function () {
        $scope.notificationUserListLoader = 0;
        function success(data, status, headers, config) {
            $scope.notifiactionUsers = data;
            $scope.notificationUserListLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.errorMessage = data;
            $scope.notificationUserListLoader = 2;
        }
        var url = "/users/getAllUsersToSendNotification";
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getNotificationUserList();
    $scope.reloadNotificationUserList = function () {
        getNotificationUserList();
    }
    //--------------------- API for post notification ------------------//
    $scope.sendNotificationAPI = function () {
        $scope.sendButtonName = "Sending";
        $scope.submitted = true;
        function success(data, status, headers, config) {
            $scope.notify("Notification sent successfully.", 1);
            $scope.submitted = false;
            $scope.successAlert = true;
            $scope.faliureAlert = false;
            $scope.closeModal();
            $scope.newStore = {
                "name": "",
                "number": "",
                "country": { "id": "" }
            };
            $scope.loading = false;
            $scope.sendButtonName = "Send";

        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.successAlert = false;
            $scope.faliureAlert = true;
            $scope.sendButtonName = "Send";
            if (status == 409) {
                $scope.errorMessage = data;
                //$scope.notify(data, 4);
            }
            else {
                $scope.errorMessage = "Could not send notification. Please check retry.";
                //$scope.notify($scope.errorMessage, 4);
            }
        }
        var url = "/users/notifications";
        API_SERVICE.postData($scope, $http, $scope.notificarionData, url, success, failure); 
    }

}]);