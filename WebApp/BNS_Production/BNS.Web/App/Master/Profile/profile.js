angular.module('bns.master.profile', ['oc.lazyLoad'])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.profile', {
        url: '/profile',
        views: {
            "profile": {
                templateUrl: 'App/Master/Profile/profile.html',
                controller: 'profileController'
            }
        }        
});
}])
.controller("profileController", ['$scope', '$state', '$http', 'API_URL', 'ngDialog', 'API_SERVICE', 'MEDIA_URL', '$timeout', function ($scope, $state, $http, API_URL, ngDialog, API_SERVICE, MEDIA_URL, $timeout) {

    $scope.image = "";
    $scope.goToEditProfile = function()
    {
        $state.go('master.editProfile');
    }
    $scope.changeBreadcrums([{ "name": "Profile", "state": "bns.master.profile" }]);
    function getAccountDetails() {
        $scope.loading = true;
        $scope.dataLoading = 0;
        function success(data, status, headers, config) {
            $scope.accountDetails = data;
            $scope.imgSrc = MEDIA_URL + data.picUrl;
            console.log($scope.accountDetails);
            $scope.loading = false;
            $scope.dataLoading = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.dataLoading = 2;
        }
        var url = "/accounts/details?account_id=" + 5;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getAccountDetails();
    $scope.reloadData = function () {
        getAccountDetails();
    }
    
}]);