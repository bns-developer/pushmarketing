angular.module('bns.master.dashboard', ['oc.lazyLoad'])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.dashboard', {
        url: '/dashboard',
        views: {
            "dashboard": {
                templateUrl: 'App/Master/Dashboard/dashboard.html',
                controller: 'dashboardController'
            }
        }        
});
}])
.controller("dashboardController", ['$scope', '$http', '$state', 'API_URL', 'API_SERVICE', 'LOADING_MESSAGE', function ($scope, $http, $state, API_URL, API_SERVICE, LOADING_MESSAGE) {
    $scope.changeBreadcrums([]);
    $scope.monthlyUserData = [];
    $scope.cashMonthlyUserData = [];
    $scope.pointsMonthlyUserData = [];
    $scope.promotionMonthlyUserData = [];
    $scope.loading = false;
    $scope.loadingMessage = LOADING_MESSAGE;
    $scope.dataLoader = 1;


    function drawChart() {
        Highcharts.setOptions({
            lang: {
                noData: 'No data present.'
            }
        });

        Highcharts.chart('TimeVsRegUsers', {

            series: [{
                name: 'Registered User',
                data: $scope.monthlyUserData
                //data: [[1483228800000, 567], [1485907200000, 685], [1488326400000, 1025], [1493596800000, 986], [1496275200000, 2358]]
                //data:[[1498847400000, 9], [1488306600000, 0], [1496255400000, 0]]
            }],
            credits: {
                enabled: false
            },
            title: {
                text: 'Registered users in last 5 months'
            },
            yAxis: {
                maxPadding: 0.10,
                title: {
                    text: 'Number of users registered'
                }
            },
            xAxis: {
                maxPadding: 0.10,
                title: {
                    text: 'Month'
                },
                startOnTick: true,
                type: 'datetime',
                dateTimeLabelFormats: {
                    month: '%b %Y'
                }
            }
        });

        Highcharts.chart('CategoriesVsRegUsers', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Offer Categories VS Users'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                maxPadding: 0.10,
                title: {
                    text: 'Month'
                },
                startOnTick: true,
                type: 'datetime',
                dateTimeLabelFormats: {
                    month: '%b'
                }

            },
            yAxis: {
                maxPadding: 0.10,
                min: 0,
                title: {
                    text: 'Number of users availing offer'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Cash',
                data: $scope.cashMonthlyUserData
                //data: [125, 658, 214, 587, 164]
            }, {
                name: 'Points',
                data: $scope.pointsMonthlyUserData
                //data: [658, 421, 56, 596, 256]
            }, {
                name: 'Promotion',
                data: $scope.promotionMonthlyUserData
                //data: [64, 854, 142, 741, 963]
            }]

        });

        //------------   All pie charts --------------------//
        var totalMembers = $scope.dashboardData.totalMembers;
        var anonymousUsers = $scope.dashboardData.anonymousUsers;
        var availedOfferCount = $scope.dashboardData.availedOfferCount;
        var ignoredOfferCount = $scope.dashboardData.ignoredOfferCount;

        var dataToShowMembersVsAnonymousUser = [];
        var dataToshowAvailedVsIgnoredOffers = [];

        if (totalMembers > 0 || anonymousUsers > 0) {
            dataToShowMembersVsAnonymousUser = [{
                name: 'Members',
                y: totalMembers
            }, {
                name: 'Anonymous Users',
                y: anonymousUsers,
                sliced: false,
                selected: true
            }];
        }
        else {
            dataToShowMembersVsAnonymousUser = [];
        }

        if (availedOfferCount > 0 || ignoredOfferCount > 0) {
            dataToshowAvailedVsIgnoredOffers = [{
                name: 'Offers Ignored',
                y: ignoredOfferCount
            }, {
                name: 'Offers Availed',
                y: availedOfferCount,
                sliced: false,
                selected: true
            }];
        }
        else {
            dataToshowAvailedVsIgnoredOffers = [];
        }

        Highcharts.chart('AnonymousAndMembers', {
            colors: ['#533073', '#ADA96E'],

            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Members and Anonymous users'
            },
            credits: {
                enabled: false
            },
            tooltip: {
                pointFormat: '<b>{point.y}</b>'
            },

            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'BNS Users',
                colorByPoint: true,
                data: dataToShowMembersVsAnonymousUser
            }]
        });

        Highcharts.chart('OffersSeenAvailed', {
            colors: ['#E55B3C', '#50B432'],
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            credits: {
                enabled: false
            },
            title: {
                text: 'Offers Ignored & Offers Availed'
            },
            tooltip: {
                pointFormat: '<b>{point.y}</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Offers',
                colorByPoint: true,
                data: dataToshowAvailedVsIgnoredOffers
            }]
        });
    }

    function formatMonthlyUserdata(rawMonthlyUserData)
    {
        if (!rawMonthlyUserData)
        {
            rawMonthlyUserData = [];
        }
        var monthlyUserData = [];
        switch (rawMonthlyUserData.length) {
            case 5: break;
            case 0: var date = new Date(); date.setDate(1); date.setHours(-1);
                rawMonthlyUserData[0] = { "userDate": date, "userCount": 0 };
            case 1: rawMonthlyUserData.splice(1, 0, { "userDate": new Date(moment(rawMonthlyUserData[0].userDate).subtract(1, 'months')), "userCount": 0 });
            case 2: rawMonthlyUserData.splice(2, 0, { "userDate": new Date(moment(rawMonthlyUserData[1].userDate).subtract(1, 'months')), "userCount": 0 });
            case 3: rawMonthlyUserData.splice(3, 0, { "userDate": new Date(moment(rawMonthlyUserData[2].userDate).subtract(1, 'months')), "userCount": 0 });
            case 4: rawMonthlyUserData.splice(4, 0, { "userDate": new Date(moment(rawMonthlyUserData[3].userDate).subtract(1, 'months')), "userCount": 0 });
                break;          
        }      
        for (i = 0; i < rawMonthlyUserData.length ; i++)
        {
            var tempDate = new Date(rawMonthlyUserData[i].userDate)
            //monthlyUserData.push([Date.UTC(tempDate.getFullYear(), tempDate.getMonth(), tempDate.getDay()), rawMonthlyUserData[i].userCount]);
            monthlyUserData.push([tempDate.getTime(), rawMonthlyUserData[i].userCount]);
        }
        return monthlyUserData;
    }
    function formatCategorywiseMonthlyUserData(rawData)
    {
        for(j=0; j<rawData.length; j++)
        {
            if(rawData[j].name =="Cash")
            {
                $scope.cashMonthlyUserData = formatMonthlyUserdata(rawData[j].data);
            }
            else if (rawData[j].name == "Points") {
                $scope.pointsMonthlyUserData = formatMonthlyUserdata(rawData[j].data);
            }
            else if (rawData[j].name == "Promotion") {
                $scope.promotionMonthlyUserData = formatMonthlyUserdata(rawData[j].data);
            }
        }
        //processData();
    }
    function processData()
    {
        drawChart();
    }
    //---------------  getData for dashboard-------------------//
    function getDashboardData() {
        $scope.loading = true;
        $scope.dataLoader = 0;
        function success(data, status, headers, config) {
            $scope.dashboardData = data;           
            $scope.monthlyUserData = formatMonthlyUserdata(data.monthlyUserCount);
            formatCategorywiseMonthlyUserData(data.categoryList);
            $scope.loading = false;
            $scope.dataLoader = 1;
            drawChart();
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.dataLoader = 2;
        }
        var url = "/dashboard/getData";
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getDashboardData();

    $scope.reloadData = function()
    {
        getDashboardData();
    }
    $scope.goToUserList = function () {
        $state.go('master.users');
    }
    $scope.goToPopularOffer = function (offerId) {
        if (offerId != '') 
            $state.go('master.offerDetails', { "offerId": offerId });  
        else
            $scope.notify("Currently there is no popular offer", 2);;
    }
    $scope.goToPopularBeacon = function (beaconId) {
        if (beaconId != '')
            $state.go("master.viewBeacon", { "beaconId": beaconId });    
        else
            $scope.notify("Currently there is no popular beacon", 2);
    }
    $scope.goToPopularKiosk = function (kioskId) {
        if (kioskId != '')
            $state.go('master.kioskDetails', { 'kioskId': kioskId });
        else
            $scope.notify("Currently there is no popular kiosk", 2);
    }
    
   
}]);