angular.module('bns.master.kioskDetails', ['bns.master.kioskDetails.luckyCharacters', 'bns.master.kioskDetails.setKioskOffer'])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.kioskDetails', {
        url: '/kiosks/kioskDetails?kioskId',
        views: {
            "kioskDetails": {
                templateUrl: 'App/Master/Kiosks/KioskDetails/kioskDetails.html',
                controller: 'kioskDetailsController'
            }
        }
    });
}])
.controller("kioskDetailsController", ['$scope', '$http', 'API_URL', 'LOADING_MESSAGE', 'ngDialog', '$stateParams', 'API_SERVICE', 'MEDIA_URL', 'LOADING_MESSAGE', '$timeout', '$state', function ($scope, $http, API_URL, LOADING_MESSAGE, ngDialog, $stateParams, API_SERVICE, MEDIA_URL, LOADING_MESSAGE, $timeout, $state) {
    
    $scope.workableButtonName = "Workable";
    $scope.setNonWorkableButtonName = "Set";
    $scope.loadingMessage = LOADING_MESSAGE;
    $scope.kioskWorkable = true;
    $scope.kioskMonthlyUserData = [];
    $scope.kioskCashMonthlyUserData = [];
    $scope.kioskPointsMonthlyUserData = [];
    $scope.kioskPromotionMonthlyUserData = [];

    
    setWorkableButtonName = function () {
        if ($scope.luckyItemsList.is_workable) {
            $scope.workableButtonName = "Set As Not Workable";
        }
        else {
            $scope.workableButtonName = "Set As Workable";
        }
    }
    //------------- get kiosk Dashboard data -----------------------------//    
    function drawCharts() {
        Highcharts.setOptions({
            lang: {
                noData: 'No data present.'
            }
        });
        Highcharts.chart('TimeVsRegUsers', {

            series: [{
                name: 'Registered User',
                data: $scope.kioskMonthlyUserData
                //data: [[1483228800000, 567], [1485907200000, 685], [1488326400000, 1025], [1493596800000, 986], [1496275200000, 2358]]
            }],
            title: {
                text: 'Registered users in last 5 months'
            },
            credits: {
                enabled: false
            },
            yAxis: {
                maxPadding: 0.10,
                title: {
                    text: 'Number of users registered'
                }
            },
            xAxis: {
                maxPadding: 0.10,
                title: {
                    text: 'Month'
                },
                startOnTick: true,
                type: 'datetime',
                dateTimeLabelFormats: {
                    month: '%b %Y'
                }
            }
        });
        Highcharts.chart('CategoriesVsRegUsers', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Offer Categories VS Users'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                title: {
                    text: 'Month'
                },
                startOnTick: true,
                maxPadding: 0.10,
                type: 'datetime',
                dateTimeLabelFormats: {
                    month: '%b'
                }
            },
            yAxis: {
                min: 0,
                maxPadding: 0.10,
                title: {
                    text: 'Number of users availing offer'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Cash',
                data: $scope.kioskCashMonthlyUserData
                //data: [125, 658, 214, 587, 164]        
            }, {
                name: 'Points',
                data: $scope.kioskPointsMonthlyUserData
                //data: [658, 421, 56, 596, 256]

            }, {
                name: 'Promotion',
                data: $scope.kioskPromotionMonthlyUserData
                //data: [64, 854, 142, 741, 963]

            }]

        });  
    }

    function formatMonthlyUserdata(rawMonthlyUserData) {
        if (!rawMonthlyUserData)
        {
            rawMonthlyUserData = [];
        }
        var monthlyUserData = [];
        switch (rawMonthlyUserData.length) {
            case 5: break;
            case 0: var date = new Date(); date.setDate(1); date.setHours(-1);
                rawMonthlyUserData[0] = { "userDate": date, "userCount": 0 };
            case 1: rawMonthlyUserData.splice(1, 0, { "userDate": new Date(moment(rawMonthlyUserData[0].userDate).subtract(1, 'months')), "userCount": 0 });
            case 2: rawMonthlyUserData.splice(2, 0, { "userDate": new Date(moment(rawMonthlyUserData[0].userDate).subtract(2, 'months')), "userCount": 0 });
            case 3: rawMonthlyUserData.splice(3, 0, { "userDate": new Date(moment(rawMonthlyUserData[0].userDate).subtract(3, 'months')), "userCount": 0 });
            case 4: rawMonthlyUserData.splice(4, 0, { "userDate": new Date(moment(rawMonthlyUserData[0].userDate).subtract(4, 'months')), "userCount": 0 });
                break;
        }
        for (i = 0; i < rawMonthlyUserData.length ; i++) {
            var tempDate = new Date(rawMonthlyUserData[i].userDate);
            monthlyUserData.push([tempDate.getTime(), rawMonthlyUserData[i].userCount]);
        }
        return monthlyUserData;
    }
    function formatCategorywiseMonthlyUserData(rawData) {
        for (j = 0; j < rawData.length; j++) {
            if (rawData[j].name == "Cash") {
                $scope.kioskCashMonthlyUserData = formatMonthlyUserdata(rawData[j].data);
            }
            else if (rawData[j].name == "Points") {
                $scope.kioskPointsMonthlyUserData = formatMonthlyUserdata(rawData[j].data);
            }
            else if (rawData[j].name == "Promotion") {
                $scope.kioskPromotionMonthlyUserData = formatMonthlyUserdata(rawData[j].data);
            }
        }
    }
    function getKioskDashboardInfo() {
        $scope.dashboarDataLoader = 0;
        function success(data, status, headers, config) {
            $scope.kioskDashboardData = data;
            if (data.popularLuckyItem.hasOwnProperty("icon"))
                $scope.mostPopularLuckyItemIcon = MEDIA_URL + data.popularLuckyItem.icon;
            else
                $scope.mostPopularLuckyItemIcon = null;
           
            $scope.kioskMonthlyUserData = formatMonthlyUserdata(data.monthlyUserCount);
            formatCategorywiseMonthlyUserData(data.categoryList);
            $scope.dashboarDataLoader = 1;
            drawCharts();  
        }
        function failure(data, status, headers, config) {
            $scope.dashboarDataLoader = 2;
        }
        var url = "/dashboard/getKioskDashboardData?kiosk_id=" + $stateParams.kioskId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getKioskDashboardInfo();
    $scope.reloadKiosksData = function () {
        getKioskDashboardInfo();
    }

    $scope.changeBreadcrumsKioskDetails = function()
    {
        $scope.changeBreadcrums([{ "name": "Kiosks", "state": "master.kiosks" }, { "name": $scope.luckyItemsList.name, "state": "master.kioskDetails", "params": { "kioskId": $stateParams.kioskId } }]);
    }

    function getLuckyItemsByKioskId() {
        $scope.loading = true;
        $scope.dataLoader = 0;
        function success(data, status, headers, config) {
            $scope.luckyItemsList = data;
            setWorkableButtonName();
            $scope.kioskWorkable = data.is_workable;
            if ($scope.luckyItemsList.luckyItems.length == 0) {
                $scope.isPresent = false;
            }
            else {
                $scope.isPresent = true;
            }
            $scope.changeBreadcrumsKioskDetails();
            $scope.loading = false;
            $scope.dataLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.dataLoader = 2;
        }
        var url = "/LuckyItems/byKioskId?kioskId=" + $stateParams.kioskId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getLuckyItemsByKioskId();
    $scope.reloadKioskLuckyItemsData = function () {
        getLuckyItemsByKioskId();
    }

    
    $scope.goToPopularOffer = function (offerId) {
        if (offerId != '')
            $state.go('master.offerDetails', { "offerId": offerId });
        else
            $scope.notify("Currently there is no popular offer", 2);
    }
    $scope.getLuckyItemPic = function (icon) {
        $scope.imagePath = MEDIA_URL + icon;
        return $scope.imagePath;
    }

    $scope.goSetKioskOffer = function (luckyItemId) {
        $state.go("master.kioskDetails.setKioskOffer", { "kioskId": $stateParams.kioskId, "luckyItemId": luckyItemId });
    }

    $scope.showLuckyboard = function () {
        $state.go('master.kioskDetails.luckyCharacters', { "kioskId": $stateParams.kioskId });
    }
    $scope.setWorkableAPI = function () {
        $scope.workableButtonName = "Wait...";
        var kioskState = "";
        function success(data, status, headers, config) {
            if ($scope.kioskWorkable == true)
            {
                $scope.closeNotWorkableModal();
                kioskState = "Workable";
            }
            else {
                kioskState = "Not Workable";
            }
                
                
            getLuckyItemsByKioskId();
            if ($scope.kioskWorkable == true) {
                $scope.kioskWorkable = false;
                kioskState = "Not Workable";
            } else {
                $scope.kioskWorkable = true;
                kioskState = "Workable";
            }
            $scope.notify("Kiosk state is changed to " + kioskState + " successfully!", 1);
        }
        function failure(data, status, headers, config) {
            setWorkableButtonName();
        }
        debugger
        var url = "/kiosks/changeKioskWorkableState?kioskId=" + $stateParams.kioskId;
        API_SERVICE.postData($scope, $http, "", url, success, failure);
    }
    //----------------------- set beacon as non workable modal -------------------------//

    $scope.setWorkable = function () {
        if ($scope.kioskWorkable) {
            ngDialog.open({
                template: 'setKioskNotWorkableDialog',
                className: 'ngdialog-theme-default modal-large store-dialog draggable',
                scope: $scope,
                closeByDocument: false,
                preCloseCallback: function () { $scope.clearNotWorkableModal() }
            });
            $timeout(function () {
                $(".draggable .ngdialog-content").draggable();
            }, 500);
        }
        else {
            $scope.setWorkableAPI();
        }

    }

    $scope.clearNotWorkableModal = function () {
        $scope.submitted = false;
        $scope.successAlert = false;
        $scope.faliureAlert = false;
    }

    $scope.closeNotWorkableModal = function () {
        ngDialog.close({
            template: 'setKioskNotWorkableDialog',
            scope: $scope
        });
        $scope.clearNotWorkableModal();
    }
    //-------------------------------------------------------------------//

   
}])