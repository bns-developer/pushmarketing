﻿angular.module('bns.master.kioskDetails.luckyCharacters', [])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.kioskDetails.luckyCharacters', {
        url: '/luckyCharacters',
        views: {
            "luckyItems": {
                templateUrl: 'App/Master/Kiosks/KioskDetails/LuckyCharacters/luckyCharacters.html',
                controller: 'luckyCharactersController'
            }
        },
        params:{
            'kioskId':''
        }
    });
}])

.controller("luckyCharactersController", ['$scope', '$http', 'API_URL', '$stateParams', 'API_SERVICE', 'MEDIA_URL', 'LOADING_MESSAGE', '$state', function ($scope, $http, API_URL, $stateParams, API_SERVICE, MEDIA_URL, LOADING_MESSAGE, $state) {
    $scope.changeBreadcrumsKioskDetails();
}])