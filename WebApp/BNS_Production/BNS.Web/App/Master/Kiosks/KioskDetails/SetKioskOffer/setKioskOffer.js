﻿angular.module('bns.master.kioskDetails.setKioskOffer', [])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.kioskDetails.setKioskOffer', {
        url: '/setKioskOffer?luckyItemId',
        views: {
            "setKioskOffer": {
                templateUrl: 'App/Master/Kiosks/KioskDetails/SetKioskOffer/setKioskOffer.html',
                controller: 'setKioskOfferController'
            }
        }
    });
}])

.controller("setKioskOfferController", ['$scope', '$http', 'API_URL', '$stateParams', 'API_SERVICE', 'MEDIA_URL', 'LOADING_MESSAGE', '$state', '$timeout', 'ngDialog', 'DTOptionsBuilder', function ($scope, $http, API_URL, $stateParams, API_SERVICE, MEDIA_URL, LOADING_MESSAGE, $state, $timeout, ngDialog, DTOptionsBuilder) {

    $scope.getLuckyItemPic = function (icon) {
        $scope.imagePath = MEDIA_URL + icon;
        return $scope.imagePath;
    }

    $scope.loadingMessage = LOADING_MESSAGE;
    $scope.deactivateButtonName = "Yes";
   
    $scope.successAlert = false;
    $scope.faliureAlert = false;

    $scope.dtOptionsKioskDefaultOffer = DTOptionsBuilder.newOptions()
        .withOption({ 'responsive': true });
    
    //----------------------- Deactivate active offer for kiosk modal -------------------------//
    $scope.deactivateOffer = function (offerId) {
        $scope.deactivateOfferModel = {"offerId": offerId };
        ngDialog.open({
            template: 'deactivateOfferDialog',
            className: 'ngdialog-theme-default modal-large store-dialog draggable',
            scope: $scope,
            closeByDocument: false,
            preCloseCallback: function () { $scope.cleardeactivateModal() }

        });
        $timeout(function () {
            $(".draggable .ngdialog-content").draggable();
        }, 500);
    }

    $scope.cleardeactivateModal = function () {
        $scope.submitted = false;
        $scope.successAlert = false;
        $scope.faliureAlert = false;
        
    }

    $scope.closeDeactivateModal = function () {
        ngDialog.close({
            template: 'deactivateOfferDialog',
            scope: $scope
        });
        $scope.cleardeactivateModal();
    }
    // --------------------    setDefault offer for kiosk member and nonMember ---------------------------------------//
    $scope.setDefaultOfferMember = function () {
        ngDialog.open({
            template: 'setKioskMemberDefaultOfferDialog',
            className: 'ngdialog-theme-default modal-large resizeable draggable',
            scope: $scope,
            closeByDocument: false,
            preCloseCallback: function () { $scope.clearModal() }
        });
        $timeout(function () {
            $(".resizeable .ngdialog-content").resizable({
                alsoResize: "#also-resize"
            });
            $(".draggable .ngdialog-content").draggable();
        }, 500);
    }
    $scope.setDefaultOfferNonMember = function () {
        ngDialog.open({
            template: 'setKioskNonMemberDefaultOfferDialog',
            className: 'ngdialog-theme-default modal-large resizeable draggable',
            scope: $scope,
            closeByDocument: false,
            preCloseCallback: function () { $scope.clearModal() }
        });
        $timeout(function () {
            $(".resizeable .ngdialog-content").resizable({
                alsoResize: "#also-resize"
            });
            $(".draggable .ngdialog-content").draggable();
        }, 500);
    }

    //-------------------------------------------------------------------//

    $scope.setOffer = function () {
        ngDialog.open({
            template: 'setOfferDialog',
            className: 'ngdialog-theme-default modal-large resizeable draggable',
            scope: $scope,
            closeByDocument: false,
            preCloseCallback: function () { $scope.clearModal() }
        });
        $timeout(function () {
            $(".resizeable .ngdialog-content").resizable({
                alsoResize: "#also-resize"
            });
            $(".draggable .ngdialog-content").draggable();
        }, 500);
    }

    $scope.closeModal = function () {
        ngDialog.close({
            template: 'setOfferDialog',
            scope: $scope,
        });
        $scope.clearModal();
    }

    $scope.clearModal = function () {
        $scope.successAlert = false;
        $scope.faliureAlert = false;
    }

    // -------------------------------------   new Default Offers ---------------------------------------------------//
    function getDefaultOffer(offerType, success_res, failure_res) {
        $scope.loading = true;
        $scope.defaultOfferLoader = 0;
        function success(data, status, headers, config) {
            for (i = 0; i < data.length; i++) {
                if (data[i].OfferType == "MEMBER") {
                    $scope.defaultOfferMember = data[i].offer;
                }
                else if (data[i].OfferType == "NON_MEMBER") {
                    $scope.defaultOfferNonMember = data[i].offer;
                }
            }
            $scope.loading = false;
            $scope.defaultOfferLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.defaultOfferLoader = 2;
        }
        var url = "/Offers/getLuckyItemDefaultOffer?kiosk_id=" + $stateParams.kioskId + "&&lucky_item_id=" + $stateParams.luckyItemId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getDefaultOffer();
    $scope.reloadDefaultOffersList = function () {
        getDefaultOffer();
    }
    //------------------------------------- getNonDefault Offers for kiosk member and nonMeber --------------------------------------//
    function getNonDefaultKioskMemberOffer() {
        $scope.nonDefaultKioskMemberOfferLoader = 0;
        function success_call(data, status, headers, config) {
            $scope.nonDefaultKioskMemberOffer = data;
            $scope.loading = false;
            $scope.nonDefaultKioskMemberOfferLoader = 1;
        }
        function failure_call(data, status, headers, config) {
            $scope.loading = false;
            $scope.nonDefaultKioskMemberOfferLoader = 2;
        }
        getNonDefaultOffer(1, success_call, failure_call);
    }
    getNonDefaultKioskMemberOffer();

    function getNonDefaultKioskNonMemberOffer() {
        $scope.nonDefaultKioskNonMemberOfferLoader = 0;
        function success_call(data, status, headers, config) {
            $scope.nonDefaultKioskNonMemberOffer = data;
            $scope.loading = false;
            $scope.nonDefaultKioskNonMemberOfferLoader = 1;
        }
        function failure_call(data, status, headers, config) {
            $scope.loading = false;
            $scope.nonDefaultKioskNonMemberOfferLoader = 2;
        }
        getNonDefaultOffer(2, success_call, failure_call);
    }
    getNonDefaultKioskNonMemberOffer();

    function getNonDefaultOffer(offerType, success_call, failure_call) {

        function success(data, status, headers, config) {
            success_call(data, status, headers, config);
        }
        function failure(data, status, headers, config) {

        }
        function failure(data, status, headers, config) {
            error_call(data, status, headers, config);
        }
        var url = "/Offers/getLuckyItemNonDefaultOffers?kiosk_id=" + $stateParams.kioskId + "&&lucky_item_id=" + $stateParams.luckyItemId + "&&offer_type=" + offerType;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }

    //----------------------  setDefaultOffer ----------------------------------------------------------------------------//
    $scope.setNonDefaultKioskMemberOffer = function (offerId) {
        $scope.defaultKioskOfferLoader = 0;
        function success_call(data, status, headers, config) {
            getNonDefaultKioskMemberOffer();
            $scope.notify("Successfully changed default offer for member.",1);
            $scope.defaultKioskOfferLoader = 1;
        }
        function failure_call(data, status, headers, config) {
            $scope.loading = false;
            $scope.defaultKioskOfferLoader = 2;
        }
        setNonDefaultOfferActive(1, offerId, success_call, failure_call);
    }

    $scope.setNonDefaultKioskNonMemberOffer = function (offerId) {
        $scope.defaultKioskOfferLoader = 0;
        function success_call(data, status, headers, config) {
            getNonDefaultKioskNonMemberOffer();
            $scope.notify("Successfully changed default offer for non-member.", 1);
            $scope.defaultKioskOfferLoader = 1;
        }
        function failure_call(data, status, headers, config) {
            $scope.loading = false;
            $scope.defaultKioskOfferLoader = 2;
        }
        setNonDefaultOfferActive(2, offerId, success_call, failure_call);
    }


    function setNonDefaultOfferActive(offerType, offerId, success_call, error_call) {

        function success(data, status, headers, config) {
            getDefaultOffer();
            $scope.closeModal();
            success_call(data, status, headers, config);
        }
        function failure(data, status, headers, config) {
            error_call(data, status, headers, config);
        }
        var url = "/Offers/changeLuckyItemDefaultOffer?kiosk_id=" + $stateParams.kioskId + "&&lucky_item_id=" + $stateParams.luckyItemId + "&&offer_id=" + offerId + "&&offer_type=" + offerType;
        API_SERVICE.postData($scope, $http, "", url, success, failure);
    }

//--------------------------------------------------------------------------------------------------------------------//

    function getLuckyItemInfo() {       
        $scope.loading = true;
        $scope.dataLoading = 0;
        function success(data, status, headers, config) {
            $scope.luckyItemInfo = data;
            $scope.changeBreadcrums([{ "name": "Kiosks", "state": "master.kiosks" }, { "name": $scope.luckyItemInfo.name, "state": "master.kioskDetails.luckyCharacters", "params": { "kioskId": $stateParams.kioskId } }, { "name": $scope.luckyItemInfo.luckyItems[0].name, "state": "master.kioskDetails.setKioskOffer", "params": { "luckyItemId": $stateParams.offerId, "kioskId": $stateParams.kioskId } }]);
            $scope.loading = false;
            $scope.dataLoading = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.dataLoading = 2;
        }
        var url = "/luckyItems/byluckyItemId?luckyItemId=" + $stateParams.luckyItemId + "&&kioskId=" + $stateParams.kioskId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getLuckyItemInfo();

    function getInactiveOffers() {
        $scope.loading = true;
        $scope.inactiveOffersLoader = 0;
        function success(data, status, headers, config) {
            $scope.inactiveOffers = data;
            console.log($scope.inactiveOffers);
            $scope.loading = false;
            $scope.inactiveOffersLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.inactiveOffersLoader = 2;
        }
        var url = "/Offers/getLuckyItemInactiveOffers?kiosk_id=" + $stateParams.kioskId + "&&lucky_item_id=" + $stateParams.luckyItemId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getInactiveOffers();

    function getActiveOffers() {
        $scope.loading = true;
        $scope.activeOfferLoader = 0;
        function success(data, status, headers, config) {
            $scope.activeOffers = data;
            console.log($scope.activeOffers);
            $scope.loading = false;
            $scope.activeOfferLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.activeOfferLoader = 2;
        }
        var url = "/Offers/getLuckyItemActiveOffers?kiosk_id=" + $stateParams.kioskId + "&&lucky_item_id=" + $stateParams.luckyItemId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getActiveOffers();

    function getOffersHistory() {
        $scope.loading = true;
        $scope.offerHistoryLoader = 0;
        function success(data, status, headers, config) {
            $scope.offersHistory = data;
            console.log($scope.activeOffers);
            $scope.loading = false;
            $scope.offerHistoryLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.offerHistoryLoader = 2;
        }
        var url = "/Offers/getLuckyItemOffersHistory?kiosk_id=" + $stateParams.kioskId + "&&lucky_item_id=" + $stateParams.luckyItemId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getOffersHistory();
    //-------------------- Deactivate offer ------------------------//
    $scope.deactivateOfferAPI = function (offerId) {
        $scope.deactivateButtonName = "Deactivating";
        function success(data, status, headers, config) {
            getActiveOffers();
            getInactiveOffers();
            $scope.notify("Offer deactivated successfully.", 1);
            $scope.successAlert = true;
            $scope.faliureAlert = false;
            $scope.deactivateOfferModel = {};
            $scope.deactivateButtonName = "Yes";
            $scope.closeDeactivateModal();
        }
        function failure(data, status, headers, config) {
            $scope.successAlert = false;
            $scope.faliureAlert = true;
            $scope.deactivateOfferModel = {};
            $scope.deactivateButtonName = "Yes";
        }
        var url = "/Offers/deactivateKioskLuckyItemOffer?kioskId=" + $stateParams.kioskId + "&&luckyItemId=" + $stateParams.luckyItemId + "&&offerId=" + offerId;
        API_SERVICE.postData($scope, $http, "", url, success, failure);
    }
    //-----------------reload data---------------------//
    $scope.reloadData = function () {
        getLuckyItemInfo();
    }

    $scope.reloadDefaultOffersList = function () {
        getDefaultOffer();
    }

    $scope.reloadActiveOffersList = function () {
        getActiveOffers();
    }

    $scope.reloadOffersHistory = function () {
        getOffersHistory();
    }

    $scope.reloadNonDefaultOffersList = function () {
        getNonDefaultOffers();
    }

    $scope.isOfferActive = function (isOfferActive) {
        if (isOfferActive)
            isOfferActive = "Active";
        else
            isOfferActive = "-";
        return isOfferActive;
    }

    $scope.reloadInactiveOffersList = function () {
        getInactiveOffers();
    }

    $scope.setActive = function (offerId) {
        function success(data, status, headers, config) {
            getActiveOffers();
            getInactiveOffers();
            $scope.successAlert = true;
            $scope.faliureAlert = false;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.successAlert = false;
            $scope.faliureAlert = true;
        }
        var url = "/Offers/setOfferForLuckyItem?kiosk_id=" + $stateParams.kioskId + "&&lucky_item_id=" + $stateParams.luckyItemId + "&&offer_id=" + offerId;
        API_SERVICE.postData($scope, $http, "", url, success, failure);
    }
    
    
}])