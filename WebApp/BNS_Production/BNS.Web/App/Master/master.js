angular.module('bns.master', [
    'bns.master.dashboard',
    'bns.master.stores',
    'bns.master.kiosks',
    'bns.master.luckyCharacters',
    'bns.master.setKioskOffer',
    'bns.master.kioskDetails',
    'bns.master.beacons',
    'bns.master.offers',
    'bns.master.offerDetails',
    'bns.master.users',
    'bns.master.userDetails',
    'bns.master.anonymousUserDetails',
    'bns.master.profile',
    'bns.master.editProfile',
    'bns.master.resetPassword',
    'bns.master.terms',
    'bns.master.offersCategories',
    'bns.master.viewBeacon',
    'bns.master.socialLogin',
    'bns.master.termsSample',
    'bns.master.offersSample',
    'bns.master.socialLogin',
    'bns.master.offerChecklist',
    'bns.master.storeDetails',
    'bns.master.administration',
    'bns.master.notifications',
    'bns.master.administrationUserDetails',
    'bns.master.pass',
    'bns.master.viewUserPass',
    'bns.master.viewTemplate',
    'bns.master.editTemplate',
    'bns.master.editSystemPass',
    'bns.master.viewSystemPass',
    'bns.master.addSystemPass',
    'bns.master.addTemplate'
])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
    .state('master', {
        url: "",
        abstract: true,
        views: {
            "master": {
                templateUrl: "App/Master/master.html",
                controller: 'masterCtrl'
            },
        },
    });
}])
.controller('masterCtrl', ['$state', '$scope', '$window', '$http', '$interval','API_SERVICE', 'TOKEN_SERVICE', 'MEDIA_URL', '$rootScope', 'LOADING_MESSAGE', function ($state, $scope, $window, $http, $interval, API_SERVICE, TOKEN_SERVICE, MEDIA_URL, $rootScope, LOADING_MESSAGE) {
    $scope.dataBreadcrums = [];
    $scope.isSelected = 'active';
    $scope.loadingMessage = LOADING_MESSAGE;
    $scope.accountName = '';

    $scope.sideBarLinkActive = function (nameOfLink) {
        var UrlLink = $state.current.url;
        if (UrlLink.includes(nameOfLink)) {
            return 'active';
        }
        else
            return '';
    }

    $scope.changeBreadcrums = function (newVal) {
        $scope.dataBreadcrums = newVal;
    };

    if (TOKEN_SERVICE.RestoreState()['access_token'] == null || TOKEN_SERVICE.RestoreState()['access_token'] == '' || TOKEN_SERVICE.RestoreState()['access_token'] == undefined) {
        $state.go('login');
    }

    $scope.goToState = function (breadCrum) {
        if (breadCrum.hasOwnProperty('params'))
            $state.go(breadCrum.state, breadCrum.params);
        else
            $state.go(breadCrum.state);
    }


    function getAccountDetails() {
        $scope.loading = true;
        $scope.dataLoading = 0;
        function success(data, status, headers, config) {
            $scope.accountDetails = data;
            $window.document.title = $scope.accountName = data.name;
            $scope.imgSrc = MEDIA_URL + data.picUrl;
            //Here we are changing icon in header
            var link = document.querySelector("link[rel*='icon']") || document.createElement('link');
            link.type = 'image/x-icon';
            link.rel = 'shortcut icon';
            link.href = $scope.imgSrc;
            document.getElementsByTagName('head')[0].appendChild(link);


            console.log($scope.accountDetails);
            $scope.loading = false;
            $scope.dataLoading = 1;
        }
        function failure(data, status, headers, config) {
            $window.document.title = "BNS";

            $scope.loading = false;
            $scope.dataLoading = 2;
        }
        var url = "/accounts/details";
        API_SERVICE.getData($scope, $http, url, success, failure);

    }
    getAccountDetails();
    $scope.reloadAccountDetails = function () {
        getAccountDetails();
    }

    $scope.goToResetPassword = function () {
        $state.go('master.resetPassword');
    }

    $scope.logout = function () {
        TOKEN_SERVICE.Delete();
        $window.document.title = "BNS";
        var link = document.querySelector("link[rel*='icon']") || document.createElement('link');
        imgSrc = "../../../libs/assets/images/real_blank.png";
        link.type = 'image/x-icon';
        link.rel = 'shortcut icon';
        link.href = imgSrc;
        document.getElementsByTagName('head')[0].appendChild(link);
        $state.go('login');
    }

    var updateDate =function ()
    {
        $scope.currentDate = new Date();
    }
    updateDate();
    $interval(updateDate, 1000);

}])
.directive('currency', function ($filter, $locale) {
    var decimalSep = $locale.NUMBER_FORMATS.DECIMAL_SEP;
    var toNumberRegex = new RegExp('[^0-9\\' + decimalSep + ']', 'g');
    var trailingZerosRegex = new RegExp('\\' + decimalSep + '0+$');
    var filterFunc = function (value) {
        //if(!angular.isUndefined(value)){
        return value.toLocaleString('en-US');
        //}      
    };

    function getCaretPosition(input) {
        if (!input) return 0;
        if (input.selectionStart !== undefined) {
            return input.selectionStart;
        } else if (document.selection) {
            // Curse you IE
            input.focus();
            var selection = document.selection.createRange();
            selection.moveStart('character', input.value ? -input.value.length : 0);
            return selection.text.length;
        }
        return 0;
    }

    function setCaretPosition(input, pos) {
        if (!input) return 0;
        if (input.offsetWidth === 0 || input.offsetHeight === 0) {
            return; // Input's hidden
        }
        if (input.setSelectionRange) {
            input.focus();
            input.setSelectionRange(pos, pos);
        } else if (input.createTextRange) {
            // Curse you IE
            var range = input.createTextRange();
            range.collapse(true);
            range.moveEnd('character', pos);
            range.moveStart('character', pos);
            range.select();
        }
    }

    function toNumber(currencyStr) {
        if (currencyStr[currencyStr.length - 1] != "." && currencyStr.slice(-2) != ".0" && currencyStr != "") {
            return Number(parseFloat(currencyStr.replace(/,/g, "")).toFixed(2));
        }
        if (currencyStr == "") {
            return 0;
        }
    }

    return {
        restrict: 'A',
        require: 'ngModel',
        link: function postLink(scope, elem, attrs, modelCtrl) {
            modelCtrl.$formatters.push(filterFunc);
            modelCtrl.$parsers.push(function (newViewValue) {
                var oldModelValue = modelCtrl.$modelValue;
                var newModelValue = toNumber(newViewValue);
                modelCtrl.$viewValue = filterFunc(newModelValue);
                var pos = getCaretPosition(elem[0]);
                elem.val(modelCtrl.$viewValue);
                var newPos = pos + modelCtrl.$viewValue.length -
                  newViewValue.length;
                if ((oldModelValue === undefined) || isNaN(oldModelValue)) {
                    newPos -= 3;
                }
                setCaretPosition(elem[0], newPos);
                return newModelValue;
            });
        }
    };
});
