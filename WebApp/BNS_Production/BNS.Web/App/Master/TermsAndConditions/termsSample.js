﻿angular.module('bns.master.termsSample', [])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.termsSample', {
        url: '/termsSample',
        views: {
            "termsSample": {
                templateUrl: 'App/Master/TermsAndConditions/termsSample.html',
                controller: 'termsSampleController'
            }
        }
    });
}])
.controller("termsSampleController", ['$scope', '$http', 'API_URL', 'LOADING_MESSAGE', 'API_SERVICE', '$state', 'ngDialog', '$timeout',
    function ($scope, $http, API_URL, LOADING_MESSAGE, API_SERVICE, $state, ngDialog, $timeout) {

        $scope.changeBreadcrums([{ "name": "Terms And Conditions", "state": "master.terms" }, { "name": "Sample Document", "state": "" }]);

        $scope.download = function () {
            window.open("../../../libs/assets/T&CUpload.xlsx", "_blank");
        };

    }])