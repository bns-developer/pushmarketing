angular.module('bns.master.stores', ['datatables'])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.stores', {
        url: '/stores',
        views: {
            "stores": {
                templateUrl: 'App/Master/Stores/stores.html',
                controller: 'storesController'
            }
        }

    });
}])
.controller("storesController", ['$scope', '$http', '$window', 'API_URL', 'LOADING_MESSAGE', 'ngDialog', 'API_SERVICE', '$timeout', 'DTOptionsBuilder', 'DTColumnDefBuilder', function ($scope, $http, $window, API_URL, LOADING_MESSAGE, ngDialog, API_SERVICE, $timeout, DTOptionsBuilder, DTColumnDefBuilder) {
    $scope.loadingMessage = LOADING_MESSAGE;
    $scope.changeBreadcrums([{ "name": "My Stores", "state": "master.stores" }]);
    $scope.saveButtonName = "Save";
    $scope.deleteButtonName = "Yes";
    $scope.updateButtonName = "Update";
    $scope.submitted = false;
    $scope.successAlert = false;
    $scope.failureAlert = false;
    $scope.newStore = {
        "name": "",
        "number": "",
        "country": {"id":""}
    };
    $scope.editStoreModel = {
        "name": "",
        "number": "",
        "id": "",
        "country": {"id":""}
    };
    $scope.viewStoreData = {
        "name": "",
        "number": "",
        "id": "",
        "country": { "id": "" }
    };
    $scope.deleteStoreModel = {
        "name": "",
        "number": "",
        "id": "",
        "country": { "id": "" }
    };


    $scope.dtOptionsStores = DTOptionsBuilder.newOptions()
       .withDOM('lrftip')
       .withPaginationType('full_numbers')
       .withOption('responsive', true)
       .withOption('scrollX', true)
       //.withOption('bAutoWidth', false)
       //.withOption('scrollCollapse', 'true')
       .withOption('order', [])
       .withButtons([
           {
               extend: 'csvHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Stores ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1]
               }
           },
           {
               extend: 'pdfHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Stores ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1]
               }
           },
           {
               extend: 'excelHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Stores ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1]
               }
           },
           'print']);

    $scope.dtColumnDefsStores = [
        DTColumnDefBuilder.newColumnDef(2).notSortable()
    ];

        
        $scope.addStore = function () {
            ngDialog.open({
                template: 'addStoreDialog',
                className: 'ngdialog-theme-default modal-large store-dialog draggable',
                scope: $scope,
                closeByDocument: false,
                preCloseCallback: function () { $scope.clearModal() }

            });
            $timeout(function () {
                $(".draggable .ngdialog-content").draggable();
            }, 500);
        }

        $scope.closeModal = function () {
            ngDialog.close({
                template: 'addStoreDialog',
                scope: $scope
            });
            $scope.clearModal();
        }
        
        $scope.clearModal = function () {
            $scope.submitted = false;
            $scope.successAlert = false;
            $scope.faliureAlert = false;
            $scope.newStore = {
                        "name": "",
                        "number": "",
                        "country": { "id": "" }
                    };
        }

    //-----------------------update store modal  -----------------------//
        $scope.editStore = function (store) {
            $scope.editStoreModel.name = store.name;
            $scope.editStoreModel.number = store.number;
            $scope.editStoreModel.country.id = store.country.id;
            $scope.editStoreModel.id = store.id;

            ngDialog.open({
                template: 'updateStoreDialog',
                className: 'ngdialog-theme-default modal-large store-dialog draggable',
                scope: $scope,
                closeByDocument: false,
                preCloseCallback: function () { $scope.clearUpdateModal() }

            });
            $timeout(function () {
                $(".draggable .ngdialog-content").draggable();
            }, 500);
        }

        $scope.clearUpdateModal = function () {
            $scope.submitted = false;
            $scope.successAlert = false;
            $scope.faliureAlert = false;
            $scope.editStoreModel = {
                "name": "",
                "number": "",
                "id": "",
                "country": { "id": "" }
            };
        }

        $scope.closeUpdateModal = function () {
            ngDialog.close({
                template: 'updateStoreDialog',
                scope: $scope
            });
            $scope.clearUpdateModal();
        }
    //----------------------- View Store modal ---------------------------//
        $scope.viewStore = function (viewStoreData) {
            $scope.viewStoreData = viewStoreData;
            ngDialog.open({
                template: 'viewStoreDialog',
                className: 'ngdialog-theme-default modal-large beacon-dialog draggable',
                scope: $scope,
                preCloseCallback: function () { $scope.clearViewStoreModal() }
            });
            $timeout(function () {
                $(".draggable .ngdialog-content").draggable();
            }, 500);
        }
        $scope.clearViewStoreModal = function () {
            $scope.submitted = false;
            $scope.successAlert = false;
            $scope.faliureAlert = false;
            $scope.viewStoreData = {
                "name": "",
                "number": "",
                "id": "",
                "country": { "id": "" }
            };
        }

        $scope.closeViewStoreModal = function () {
            ngDialog.close({
                template: 'viewStoreDialog',
                scope: $scope
            });
            $scope.clearViewStoreModal();
        }


    //----------------------- Delete store modal -------------------------//
        $scope.deleteStore = function (store) {
            $scope.deleteStoreModel.name = store.name;
            $scope.deleteStoreModel.number = store.number;
            $scope.deleteStoreModel.country.id = store.country.id;
            $scope.deleteStoreModel.id = store.id;
            ngDialog.open({
                template: 'deleteStoreDialog',
                className: 'ngdialog-theme-default modal-large store-dialog draggable',
                scope: $scope,
                closeByDocument: false,
                preCloseCallback: function () { $scope.clearDeleteModal() }

            });
            $timeout(function () {
                $(".draggable .ngdialog-content").draggable();
            }, 500);
        }

        $scope.clearDeleteModal = function () {
            $scope.submitted = false;
            $scope.successAlert = false;
            $scope.faliureAlert = false;
            $scope.deleteStoreModel = {
                "name": "",
                "number": "",
                "id": "",
                "country": { "id": "" }
            };
        }

        $scope.closeDeleteModal = function () {
            ngDialog.close({
                template: 'deleteStoreDialog',
                scope: $scope
            });
            $scope.clearDeleteModal();
        }

        function getAllStoresList() {
            $scope.loading = true;
            $scope.dataLoader = 0;
            function success(data, status, headers, config) {
                $scope.stores = data;                
                $scope.loading = false;
                $scope.dataLoader = 1;
            }
            function failure(data, status, headers, config) {
                $scope.loading = false;
                $scope.dataLoader = 2;
            }
            var url = "/Stores/all";
            API_SERVICE.getData($scope, $http, url, success, failure);
        }
        getAllStoresList();

        function getAllCountries() {
            $scope.loading = true;
            function success(data, status, headers, config) {
                $scope.countries = data;
                $scope.loading = false;               
            }
            function failure(data, status, headers, config) {
                $scope.loading = false;
                $scope.notify("Something went wrong list of countries is not loaded. Please reload page.", 4);
            }
            var url = "/countries/all";
            API_SERVICE.getData($scope, $http, url, success, failure);
        }
        getAllCountries();

        $scope.reloadStoreList = function () {
            getAllStoresList();
        }

        $scope.updateStoreAPI = function () {
            $scope.updateButtonName = "Updating";
            $scope.submitted = true;
            if ($scope.editStoreModel.id == "" || $scope.editStoreModel.id == undefined || $scope.editStoreModel.name == "" || $scope.editStoreModel.name == undefined || $scope.editStoreModel.number == "" || $scope.editStoreModel.number == undefined || $scope.editStoreModel.country.id == "" || $scope.editStoreModel.country.id == undefined) {
                $scope.updateButtonName = "Update";
            }
            else {
                function success(data, status, headers, config) {
                    getAllStoresList();
                    $scope.notify("Store with name \"" + $scope.editStoreModel.name + "\"  updated successfully.", 1);
                    $scope.submitted = false;
                    $scope.successAlert = true;
                    $scope.faliureAlert = false;
                    $scope.closeUpdateModal();
                    $scope.editStoreModel = {
                        "name": "",
                        "number": "",
                        "id": "",
                        "country": { "id": "" }
                    };
                    $scope.loading = false;
                    $scope.updateButtonName = "Update";
                }
                function failure(data, status, headers, config) {
                    $scope.loading = false;
                    $scope.successAlert = false;
                    $scope.faliureAlert = true;
                    $scope.updateButtonName = "Update";
                    if (status == 409) {
                        $scope.errorMessage = data;
                    }
                    else {
                        $scope.errorMessage = "Could not update store. Please check all the fields."
                    }
                }
                var url = "/Stores/update";
                API_SERVICE.postData($scope, $http, $scope.editStoreModel, url, success, failure);
            }
        }
        $scope.deleteStoreAPI = function () {
            $scope.deleteButtonName = "Deleting";
            $scope.submitted = true;
            if ($scope.deleteStoreModel.id == "" || $scope.deleteStoreModel.id == undefined || $scope.deleteStoreModel.name == "" || $scope.deleteStoreModel.name == undefined || $scope.deleteStoreModel.number == "" || $scope.deleteStoreModel.number == undefined || $scope.deleteStoreModel.country.id == "" || $scope.deleteStoreModel.country.id == undefined) {
                $scope.deleteButtonName = "Yes";
            }
            else {
                function success(data, status, headers, config) {
                    getAllStoresList();
                    $scope.notify("Store with name \"" + $scope.deleteStoreModel.name + "\", successfully deleted from the list.", 1);
                    $scope.submitted = false;
                    $scope.successAlert = true;
                    $scope.faliureAlert = false;
                    $scope.closeDeleteModal();
                    $scope.deleteStoreModel = {
                        "name": "",
                        "number": "",
                        "id": "",
                        "country": { "id": "" }
                    };
                    $scope.loading = false;
                    $scope.deleteButtonName = "Yes";
                }
                function failure(data, status, headers, config) {
                    $scope.loading = false;
                    $scope.successAlert = false;
                    $scope.faliureAlert = true;
                    $scope.deleteButtonName = "Yes";
                    if (status == 409 || status == 500) {
                        $scope.errorMessage = data;
                    }
                    else {
                        $scope.errorMessage = "Could not delete store."
                    }
                    $scope.notify("$scope.errorMessage.", 4);
                    $scope.closeDeleteModal();
                }
                var url = "/Stores/delete";
                API_SERVICE.postData($scope, $http, $scope.deleteStoreModel, url, success, failure);
            }
        }

        $scope.saveNewStore = function () {
            $scope.saveButtonName = "Saving";
            $scope.submitted = true;
            if ($scope.newStore.name == "" || $scope.newStore == undefined || $scope.newStore.number == "" || $scope.newStore.number == undefined || $scope.newStore.country.id == "" || $scope.newStore.country.id == undefined)
            {
                $scope.saveButtonName = "Save";
            }
            else
            {
                function success(data, status, headers, config) {
                    getAllStoresList();
                    $scope.notify("Store with name \"" + $scope.newStore.name + "\", successfully added to the list.", 1);
                    $scope.submitted = false;
                    $scope.successAlert = true;
                    $scope.faliureAlert = false;
                    $scope.closeModal();
                    $scope.newStore = {
                        "name": "",
                        "number": "",
                        "country": { "id": "" }
                    };
                    $scope.loading = false;
                    $scope.saveButtonName = "Save";
                    
                }
                function failure(data, status, headers, config) {
                    $scope.loading = false;
                    $scope.successAlert = false;
                    $scope.faliureAlert = true;
                    $scope.saveButtonName = "Save";
                    if(status == 409)
                    {                       
                        $scope.errorMessage = data;
                        //$scope.notify(data, 4);
                    }
                    else
                    {                     
                        $scope.errorMessage = "Could not add store to the list. Please check all the fields.";
                        //$scope.notify($scope.errorMessage, 4);
                    }
                }
                var url = "/Stores/save";
                API_SERVICE.postData($scope, $http, $scope.newStore, url, success, failure);
            }          
        }
    }])