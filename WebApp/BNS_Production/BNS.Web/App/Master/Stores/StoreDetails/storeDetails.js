﻿angular.module('bns.master.storeDetails', [])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.storeDetails', {
        url: '/stores/storeDetails?storeId',
        views: {
            "storeDetails": {
                templateUrl: 'App/Master/Stores/StoreDetails/storeDetails.html',
                controller: 'storeDetailsController'
            }
        }
    });
}])
.controller("storeDetailsController", ['$scope', '$state', '$stateParams', '$http', 'API_URL', 'API_SERVICE', 'MEDIA_URL', 'LOADING_MESSAGE', function ($scope, $state, $stateParams, $http, API_URL, API_SERVICE, MEDIA_URL, LOADING_MESSAGE) {
    
    $scope.dataLoader = 1;
    $scope.loadingMessage = LOADING_MESSAGE;
    //---- draw Chart function definition --------------//
    function drawChart() {
        Highcharts.setOptions({
            lang: {
                noData: 'No data present.'
            }
        });

        Highcharts.chart('TimeVsRegUsers', {

            series: [{
                name: 'Registered User',
                data: $scope.monthlyUserData
                //data: [[1483228800000, 567], [1485907200000, 685], [1488326400000, 1025], [1493596800000, 986], [1496275200000, 2358]]
            }],
            title: {
                text: 'Registered users in last 5 months'
            },
            credits: {
                enabled: false
            },
            yAxis: {
                maxPadding: 0.10,
                title: {
                    text: 'Number of users registered'
                }
            },
            xAxis: {
                maxPadding: 0.10,
                title: {
                    text: 'Month'
                },
                startOnTick: true,
                type: 'datetime',
                dateTimeLabelFormats: {
                    month: '%b %Y'
                }
            }
        });

        Highcharts.chart('CategoriesVsRegUsers', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Offer Categories VS Users'
            },
            credits: {
                enabled: false
            },
            yAxis: {
                maxPadding: 0.10,
                min: 0,
                title: {
                    text: 'Number of users availing offer'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Cash',
                data: $scope.cashMonthlyUserData
                //data: [125, 658, 214, 587, 164]
            }, {
                name: 'Points',
                data: $scope.pointsMonthlyUserData
                //data: [658, 421, 56, 596, 256]
            }, {
                name: 'Promotion',
                data: $scope.promotionMonthlyUserData
                //data: [64, 854, 142, 741, 963]
            }],
            xAxis: {
                maxPadding: 0.10,
                title: {
                    text: 'Month'
                },
                startOnTick: true,
                type: 'datetime',
                dateTimeLabelFormats: {
                    month: '%b'
                }
            },

        });
    }
    //----------- formating dashboard data ----------------------//
    function formatMonthlyUserdata(rawMonthlyUserData) {
        if (!rawMonthlyUserData)
        {
            rawMonthlyUserData = [];
        }
        var monthlyUserData = [];
        switch (rawMonthlyUserData.length) {
            case 5: break;
            case 0: var date = new Date(); date.setDate(1); date.setHours(-1);
                rawMonthlyUserData[0] = { "userDate": date, "userCount": 0 };
            case 1: rawMonthlyUserData.splice(1, 0, { "userDate": new Date(moment(rawMonthlyUserData[0].userDate).subtract(1, 'months')), "userCount": 0 });
            case 2: rawMonthlyUserData.splice(2, 0, { "userDate": new Date(moment(rawMonthlyUserData[0].userDate).subtract(2, 'months')), "userCount": 0 });
            case 3: rawMonthlyUserData.splice(3, 0, { "userDate": new Date(moment(rawMonthlyUserData[0].userDate).subtract(3, 'months')), "userCount": 0 });
            case 4: rawMonthlyUserData.splice(4, 0, { "userDate": new Date(moment(rawMonthlyUserData[0].userDate).subtract(4, 'months')), "userCount": 0 });
                break;
        }
        for (i = 0; i < rawMonthlyUserData.length ; i++) {
            var tempDate = new Date(rawMonthlyUserData[i].userDate)
            //monthlyUserData.push([Date.UTC(tempDate.getFullYear(), tempDate.getMonth(), tempDate.getDay()), rawMonthlyUserData[i].userCount]);
            monthlyUserData.push([tempDate.getTime(), rawMonthlyUserData[i].userCount]);
        }
        return monthlyUserData;
    }
    function formatCategorywiseMonthlyUserData(rawData) {
        for (j = 0; j < rawData.length; j++) {
            if (rawData[j].name == "Cash") {
                $scope.cashMonthlyUserData = formatMonthlyUserdata(rawData[j].data);
            }
            else if (rawData[j].name == "Points") {
                $scope.pointsMonthlyUserData = formatMonthlyUserdata(rawData[j].data);
            }
            else if (rawData[j].name == "Promotion") {
                $scope.promotionMonthlyUserData = formatMonthlyUserdata(rawData[j].data);
            }
        }
    }
    //-------------  Store dashboard data api ---------//

    function getStoreDetails() {
        $scope.dataLoader = 0;
        function success(data, status, headers, config) {
            $scope.storeInfo = data;
            $scope.changeBreadcrums([{ "name": "My Stores", "state": "master.stores" }, { "name": $scope.storeInfo.name, "state": "master.storeDetails" }]);
            $scope.dataLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.dataLoader = 2;
        }
        var url = "/stores/details?storeId=" + $stateParams.storeId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getStoreDetails();

    function getStoreDashboardData() {
        $scope.loading = true;
        $scope.dataLoader = 0;
        function success(data, status, headers, config) {
            $scope.storeDashboardData = data;
            $scope.monthlyUserData = formatMonthlyUserdata(data.monthlyUserCount);
            formatCategorywiseMonthlyUserData(data.categoryList);
            $scope.loading = false;
            $scope.dataLoader = 1;
            drawChart();
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.dataLoader = 2;
        }
        var url = "/dashboard/getStoreDashboardData?store_id=" + $stateParams.storeId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getStoreDashboardData();

    $scope.goToPopularOffer = function (offerId) {
        if (offerId != '')
            $state.go('master.offerDetails', { "offerId": offerId });
        else
            $scope.notify("Currently there is no popular offer", 2);;
    }
    $scope.goToPopularBeacon = function (beaconId) {
        if (beaconId != '')
            $state.go("master.viewBeacon", { "beaconId": beaconId });
        else
            $scope.notify("Currently there is no popular beacon", 2);
    }
    $scope.goToPopularKiosk = function (kioskId) {
        if (kioskId != '')
            $state.go('master.kioskDetails', { 'kioskId': kioskId });
        else
            $scope.notify("Currently there is no popular kiosk", 2);
    }
   

}]);