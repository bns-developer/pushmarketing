﻿angular.module('bns.master.resetPassword', ['oc.lazyLoad'])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.resetPassword', {
        url: '/resetPassword',
        views: {
            "resetPassword": {
                templateUrl: 'App/Master/ResetPassword/resetPassword.html',
                controller: 'resetPasswordController'
            }
        }
    });
}])
.controller("resetPasswordController", ['$scope', '$state','$http', 'API_URL', 'API_SERVICE', 'MEDIA_URL', function ($scope, $state, $http, API_URL, API_SERVICE, MEDIA_URL) {
    $scope.resetPasswordButtonName = "Reset Password";
    $scope.passwordNotMatch = false;
    $scope.resetPassword = {
        "oldPassword": "",
        "newPassword": "",
        "confirmPassword": ""
    };
    $scope.changeBreadcrums([{ "name": "Reset Password", "state": "bns.master.resetPassword" }]);
    $scope.submitForm = function (isValid, form) {
        $scope.submitted = true;
        if ($scope.resetPassword.newPassword != $scope.resetPassword.confirmPassword)
        {
            $scope.passwordNotMatch = true;
            isValid = false;
        }    
        else
            $scope.passwordNotMatch = false;

        if (isValid) {
            resetPassword();
        }
        else {
            return false;
        }
    }

    function resetPassword() {
        $scope.resetPasswordButtonName = "Loading...";
        $scope.submitted = true;
        if ($scope.resetPassword.newPassword == "" || $scope.resetPassword.newPassword == undefined || $scope.resetPassword.oldPassword == "" || $scope.resetPassword.oldPassword == undefined) {
            $scope.resetPasswordButtonName = "Reset Password";
        }
        else {
            function success(data, status, headers, config) {
                $scope.notify("Password updated successfully.", 1);
                $scope.submitted = false;
                $scope.successAlert = true;
                $scope.faliureAlert = false;
                $scope.resetPassword = {
                    "oldPassword": "",
                    "newPassword": ""
                };
                $scope.resetPasswordButtonName = "Reset Password";
                $state.go("master.dashboard");
            }
            function failure(data, status, headers, config) {
                $scope.successAlert = false;
                $scope.faliureAlert = true;
                $scope.resetPasswordButtonName = "Reset Password";
                if (status == 409) {
                    $scope.errorMessage = data;
                }
                else {
                    $scope.errorMessage = "Could not update password. Please check all the fields."
                }
            }
            var url = "/accounts/resetPassword";
            API_SERVICE.postData($scope, $http, $scope.resetPassword, url, success, failure);
        }
    }

}]);