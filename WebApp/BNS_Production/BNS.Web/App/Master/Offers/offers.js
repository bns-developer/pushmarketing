angular.module('bns.master.offers', ['oc.lazyLoad'])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.offers', {
        url: '/offers',
        views: {
            "offers": {
                templateUrl: 'App/Master/Offers/offers.html',
                controller: 'offersController'
            }
        }

    });
}])
    .directive('fileInput', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attributes) {
                element.bind('change', function () {
                    $parse(attributes.fileInput)
                    .assign(scope, element[0].files)
                    scope.$apply()
                });
            }
        };
    }])


.controller("offersController", ['$scope', '$http', '$window', 'API_URL', 'ngDialog', 'API_SERVICE', 'LOADING_MESSAGE', '$timeout', '$state', 'DTOptionsBuilder', 'DTColumnDefBuilder', function ($scope, $http, $window, API_URL, ngDialog, API_SERVICE, LOADING_MESSAGE, $timeout, $state, DTOptionsBuilder, DTColumnDefBuilder) {
    $scope.saveButtonName = "Save";
    $scope.deleteButtonName = "Yes";
    $scope.updateButtonName = "Update";
    $scope.submitted = false;
    $scope.dateError = false;
    $scope.successAlert = false;
    $scope.faliureAlert = false;
    $scope.image = "";
    $scope.loadingMessage = LOADING_MESSAGE;
    $scope.defaultBeaconMemberOffer = [];
    $scope.defaultBeaconNonMemberOffer = [];
    $scope.defaultKioskMemberOffer = [];
    $scope.defaultKioskNonMemberOffer = [];

    $scope.dtOptionsOffers = DTOptionsBuilder.newOptions()
       .withDOM('lrftip')
       .withPaginationType('full_numbers')
       .withOption('responsive', true)
       .withOption('scrollX', true)
       //.withOption('bAutoWidth', false)
       //.withOption('scrollCollapse', 'true')
       .withOption('order', [])
       .withButtons([
           {
               extend: 'csvHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Offers ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
               }
           },
           {
               extend: 'pdfHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Offers ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
               }
           },
           {
               extend: 'excelHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Offers ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
               }
           },
           'print']);

    $scope.dtColumnDefsOffers = [DTColumnDefBuilder.newColumnDef(11).notSortable()];

    $scope.uploadOffer = {
        offersSheet: ''
    };

    var today = new Date();
    today.setHours(0, 0, 0, 0);
    $scope.changeBreadcrums([{ "name": "Offers", "state": "master.offers" }]);

    $('#offerDatetime').daterangepicker({

        timePicker: true,
        timePickerIncrement: 1,
        format: 'MM/DD/YYYY h:mm:ss A',
        minDate: today,
        orientation: "auto",
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear'
        },
        "startDate": new Date(),
        "endDate": new Date()
    });

    angular.element('.cancelBtn').click(function () {
        angular.element('#offerDatetime')[0].value = "";
    });

    $scope.offerTypes = [
        {type : "Member", value : 1},
        {type: "Non-member", value: 2 },
        {type: "Both", value: 3 }
    ];

    $scope.newOffer = {
        "categories": "",
        "countries": "",
        "title": "",
        "description": "",
        "startTime": "",
        "endTime": "",
        "number": "",
        "imagePath": "",
        "value": "",
        "offerType":""
    };

    $scope.editOfferModel = {
        "id": "",
        "categories": "",
        "countries": "",
        "title": "",
        "description": "",
        "startTime": "",
        "endTime": "",
        "number": "",
        "imagePath": "",
        "value": "",
        "offerType": ""
    };
    $scope.deleteOfferModel = {
        "id": "",
        "number": "",
        "title": "" 
    };

    //------------------ here we will create category object-----------//
    $scope.category = {
        "id": "",
        "value": ""
    };

    setTimeout(function () {
        $('input[name="daterangepicker-date-time"]').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' });

    }, 1000);
   
    //----------------------- Delete offer modal -------------------------//

    $scope.deleteOffer = function (offer) {

        $scope.deleteOfferModel.id = offer.id;
        $scope.deleteOfferModel.title = offer.title;
        $scope.deleteOfferModel.number = offer.number;
        
        ngDialog.open({
            template: 'deleteOfferDialog',
            className: 'ngdialog-theme-default modal-large store-dialog draggable',
            scope: $scope,
            closeByDocument: false,
            preCloseCallback: function () { $scope.clearDeleteModal() }

        });
        $timeout(function () {
            $(".draggable .ngdialog-content").draggable();
        }, 500);
    }

    $scope.clearDeleteModal = function () {
        $scope.submitted = false;
        $scope.successAlert = false;
        $scope.faliureAlert = false;
        $scope.deleteOfferModel = {
            "id": "",
            "number": "",
            "title": ""
        };
    }

    $scope.closeDeleteModal = function () {
        ngDialog.close({
            template: 'deleteOfferDialog',
            scope: $scope
        });
        $scope.clearDeleteModal();
    }
    //------------------ set dafault beacon offer for member modal ----------------//
     $scope.setDefaultBeaconOfferMember = function () {
        ngDialog.open({
            template: 'setBeaconMemberDefaultOfferDialog',
            className: 'ngdialog-theme-default modal-large resizeable draggable',
            scope: $scope,
            closeByDocument: false,
            preCloseCallback: function () { $scope.clearModal() }
        });
        $timeout(function () {
            $(".resizeable .ngdialog-content").resizable({
                alsoResize: "#also-resize"
            });
            $(".draggable .ngdialog-content").draggable();
        }, 500);
     }
     $scope.setDefaultBeaconOfferNonMember = function () {
         ngDialog.open({
             template: 'setBeaconNonMemberDefaultOfferDialog',
             className: 'ngdialog-theme-default modal-large resizeable draggable',
             scope: $scope,
             closeByDocument: false,
             preCloseCallback: function () { $scope.clearModal() }
         });
         $timeout(function () {
             $(".resizeable .ngdialog-content").resizable({
                 alsoResize: "#also-resize"
             });
             $(".draggable .ngdialog-content").draggable();
         }, 500);
     }
     $scope.setDefaultKioskOfferMember = function () {
         ngDialog.open({
             template: 'setKioskMemberDefaultOfferDialog',
             className: 'ngdialog-theme-default modal-large resizeable draggable',
             scope: $scope,
             closeByDocument: false,
             preCloseCallback: function () { $scope.clearModal() }
         });
         $timeout(function () {
             $(".resizeable .ngdialog-content").resizable({
                 alsoResize: "#also-resize"
             });
             $(".draggable .ngdialog-content").draggable();
         }, 500);
     }
     $scope.setDefaultKioskOfferNonMember = function () {
         ngDialog.open({
             template: 'setKioskNonMemberDefaultOfferDialog',
             className: 'ngdialog-theme-default modal-large resizeable draggable',
             scope: $scope,
             closeByDocument: false,
             preCloseCallback: function () { $scope.clearModal() }
         });
         $timeout(function () {
             $(".resizeable .ngdialog-content").resizable({
                 alsoResize: "#also-resize"
             });
             $(".draggable .ngdialog-content").draggable();
         }, 500);
     }
    //-------------------------------------------------------------------//
  
    $scope.deleteOfferAPI = function () {
        $scope.deleteButtonName = "Deleting";
        $scope.submitted = true;
        if ($scope.deleteOfferModel.id == "" || $scope.deleteOfferModel.id == undefined) {
            $scope.deleteButtonName = "Yes";
        }
        else {
            function success(data, status, headers, config) {
                getAllOffersList();
                $scope.notify("Offer with title \"" + $scope.deleteOfferModel.title + "\", successfully deleted from the list.", 1);
                $scope.submitted = false;
                $scope.successAlert = true;
                $scope.faliureAlert = false;
                $scope.closeDeleteModal();
                $scope.deleteOfferModel = {
                    "id": "",
                    "number": "",
                    "title": ""
                };
                $scope.loading = false;
                $scope.deleteButtonName = "Yes";
            }
            function failure(data, status, headers, config) {
                $scope.loading = false;
                $scope.successAlert = false;
                $scope.faliureAlert = true;
                $scope.deleteButtonName = "Yes";
                if (status == 409) {
                    $scope.errorMessage = data;
                }
                else if (status == 500) {
                    $scope.errorMessage = data;
                }
                else {
                    $scope.errorMessage = "Could not delete offer."
                }
            }
            var url = "/Offers/delete";
            API_SERVICE.postData($scope, $http, $scope.deleteOfferModel, url, success, failure);
        }
    }
    

    $scope.addOffer = function () {
        ngDialog.open({
            template: 'addOfferDialog',
            className: 'draggable',
            scope: $scope,
            preCloseCallback: function () { $scope.clearModal() }
        });
        $timeout(function () {
            $(".draggable ngdialog-content").draggable();
        }, 500);
    }

    $scope.closeModal = function () {
        ngDialog.close({
            template: 'addOfferDialog',
            scope: $scope
        });
        $scope.clearModal();
    }

    $scope.clearModal = function () {
        $scope.submitted = false;
        $scope.successAlert = false;
        $scope.faliureAlert = false;
        $scope.date = "";
        $scope.selectedCategories = [];
        $scope.newOffer = {
            "categories": [],
            "countries": [],
            "terms": [],
            "title": "",
            "description": "",
            "startTime": "",
            "endTime": "",
            "number": "",
            "imagePath": "",
            "value": "",
            "offerType": ""
        };
        $('#fileInput').val(null);
        $('#offerDatetime').val('');
        $scope.dateError = false;
        getAllCategories();
    }

    $scope.checkDate = function () {
        $scope.newOffer.startTime = ($('#offerDatetime').val()).split('-')[0];
        $scope.newOffer.endTime = ($('#offerDatetime').val()).split('-')[1];
        if ($scope.newOffer.startTime == "" || $scope.newOffer.endTime == "" || $scope.newOffer.startTime == undefined || $scope.newOffer.endTime == undefined) {
            $scope.dateError = true;
        }
        else {
            $scope.dateError = false;
        }
    }

    $scope.saveNewOffer = function (form) {
        if ($scope.newOffer.countries != "" || $scope.newOffer.countries.length>0)
            $scope.changeSelectedCountryCurrency();
        console.log($('#offerDatetime').val());       
        $scope.submitted = true;

        $scope.newOffer.startTime = ($('#offerDatetime').val()).split('-')[0];
        $scope.newOffer.endTime = ($('#offerDatetime').val()).split('-')[1];
        $scope.newOffer.imagePath = $scope.image[0];
        if (form.$valid == false || $scope.InvalidCountryGroupSelected || $scope.newOffer.categories == "" || $scope.newOffer.categories == undefined || $scope.newOffer.countries == "" || $scope.newOffer.countries == undefined || $scope.newOffer.title == ""
            || $scope.newOffer.title == undefined || $scope.newOffer.description == "" || $scope.newOffer.description == undefined || $scope.newOffer.startTime == "" || $scope.newOffer.startTime == undefined
            || $scope.newOffer.endTime == "" || $scope.newOffer.endTime == undefined || $scope.newOffer.duration == "" || $scope.newOffer.duration == undefined ) {
            if ($scope.newOffer.startTime == "" || $scope.newOffer.startTime == undefined
            || $scope.newOffer.endTime == "" || $scope.newOffer.endTime == undefined) {
                $scope.dateError = true;
            }
            else {
                $scope.dateError = false;
            }
            $scope.saveButtonName = "Save";
        }
        else {
            $scope.saveButtonName = "Saving";
            $scope.dateError = false;
            function success(data, status, headers, config) {
                getAllOffersList();
                getNonDefaultBeaconMemberOffer();
                getNonDefaultBeaconNonMemberOffer();
                getNonDefaultKioskMemberOffer();
                getNonDefaultKioskNonMemberOffer();
                $scope.selectedCategories = [];
                $scope.notify("Offer with name \"" + $scope.newOffer.title + "\", successfully added to the list.", 1);
                $scope.submitted = false;
                $scope.successAlert = true;
                $scope.faliureAlert = false;
                $scope.image = "";
                $scope.clearModal();
                $('#fileInput').val(null);
                $('#offerDatetime').val('');
                $scope.loading = false;
                $scope.saveButtonName = "Save";
                $scope.closeModal();
            }
            function failure(data, status, headers, config) {                           
                $scope.loading = false;
                $scope.saveButtonName = "Save";
                $scope.successAlert = false;
                $scope.faliureAlert = true;
                if (data.status == 409) {
                    $scope.errorMessage = data.data;
                }
                else {
                    $scope.errorMessage = "Could not add offer to the list. Please check all the fields."
                }
            }
            var url = "/Offers/save";
            API_SERVICE.postMultiPartData($scope, $http, $scope.newOffer, url, success, failure);
            //API_SERVICE.postData($scope, $http, $scope.newOffer, url, success, failure);
        }
    }
    
    function getAllOffersList() {
        $scope.loading = true;
        $scope.dataLoader = 0;
        function success(data, status, headers, config) {
            $scope.offers = data;
            console.log($scope.offers);
            $scope.loading = false;
            $scope.dataLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.dataLoader = 2;
        }
        var url = "/Offers/all";
        API_SERVICE.getData($scope, $http, url, success, failure);

    }
    getAllOffersList();

    $scope.getTermsAndConditions = function () {        
        function success(data, status, headers, config) {            
            $scope.termsAndConditions = data;
        }
        function failure(data, status, headers, config) {
            $scope.notify("Terms and conditions are not loaded", 4);
        }
        var url = '/TermsAndConditions/get';
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    $scope.getTermsAndConditions();

    function getAllCountries() {
        $scope.loading = true;
        function success(data, status, headers, config) {
            for(var i in data){
                data[i].name = data[i].name + " ("+ data[i].currency +")";
            }
            $scope.countries = data;
            console.log($scope.stores);
            $scope.loading = false;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
        }
        var url = "/countries/all";
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getAllCountries();

    function getAllCategories() {
        $scope.loading = true;
        function success(data, status, headers, config) {
            //$scope.categories = data;
            //setTimeout(function () {
                $scope.newOffer.categories = data;
            //}, 0);
            console.log($scope.categories);
            $scope.loading = false;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
        }
        var url = "/Categories/all";
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getAllCategories();

    //------------------ get default offers by accounts ----------------//
    //---   /Offers/getDefaultOffersByAccount

    function getDefaultOffers() {
        $scope.loading = true;
        $scope.defaultOfferLoader = 0;
        function success(data, status, headers, config) {
            $scope.defaultOffers = data;
            for (var i = 0; i < data.length; i++)
            {
                var offer = data[i];
                if (offer.DeviceType == "BEACON")
                {
                    if (offer.OfferType == "MEMBER")
                    {
                        $scope.defaultBeaconMemberOffer.push(offer);
                    }
                    else if (offer.OfferType == "NON_MEMBER") {
                        $scope.defaultBeaconNonMemberOffer.push(offer);
                    }

                }
                else if (offer.DeviceType == "KIOSK")
                {
                    if (offer.OfferType == "MEMBER") {
                        $scope.defaultKioskMemberOffer.push(offer);
                    }
                    else if (offer.OfferType == "NON_MEMBER") {
                        $scope.defaultKioskNonMemberOffer.push(offer);
                    }
                }
            }
            //$scope.offer.date = $scope.offer[0].createdOn.split(' ')[0];
            //table.ajax.reload();
            $scope.loading = false;
            $scope.defaultOfferLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.defaultOfferLoader = 2;
        }
        var url = "/Offers/getDefaultOffersByAccount";
        API_SERVICE.getData($scope, $http, url, success, failure);

    }
    getDefaultOffers();
    $scope.reloadDefaultOffers = function()
    {
        getDefaultOffers();
    }

    //-------------- set Default offers------------------//
    $scope.setNonDefaultBeaconMemberOffer = function(offerId) {
        $scope.defaultBeaconMemberOfferLoader = 0;       
        function success_call(data, status, headers, config) {
            getNonDefaultBeaconMemberOffer();
            $scope.closeModal();
            $scope.notify("Successfully set beacon Default offer for Members.",1);
            $scope.defaultBeaconMemberOfferLoader = 1;
        }
        function failure_call(data, status, headers, config) {
            $scope.loading = false;
            $scope.defaultBeaconMemberOfferLoader = 2;
        }
        setNonDefaultOfferActive(1, 1, offerId, success_call, failure_call);
    }

    $scope.setNonDefaultBeaconNonMemberOffer = function(offerId) {
        $scope.defaultBeaconMemberOfferLoader = 0;
        function success_call(data, status, headers, config) {
            getNonDefaultBeaconNonMemberOffer();
            $scope.closeModal();
            $scope.notify("Successfully set beacon Default offer for Non-Members.", 1);
            $scope.defaultBeaconMemberOfferLoader = 1;
        }
        function failure_call(data, status, headers, config) {
            $scope.loading = false;
            $scope.defaultBeaconMemberOfferLoader = 2;
        }
        setNonDefaultOfferActive(1, 2, offerId, success_call, failure_call);
    }

    $scope.setNonDefaultKioskMemberOffer = function(offerId) {
        $scope.defaultBeaconMemberOfferLoader = 0;
        function success_call(data, status, headers, config) {
            getNonDefaultKioskMemberOffer();
            $scope.closeModal();
            $scope.notify("Successfully set kiosk Default offer for Members.", 1);
            $scope.defaultBeaconMemberOfferLoader = 1;
        }
        function failure_call(data, status, headers, config) {
            $scope.loading = false;
            $scope.defaultBeaconMemberOfferLoader = 2;
        }
        setNonDefaultOfferActive(2, 1, offerId, success_call, failure_call);
    }

    $scope.setNonDefaultKioskNonMemberOffer = function (offerId) {
        $scope.defaultBeaconMemberOfferLoader = 0;       
        function success_call(data, status, headers, config) {
            getNonDefaultKioskNonMemberOffer();
            $scope.closeModal();
            $scope.notify("Successfully set kiosk Default offer for Non-Members.", 1);
            $scope.defaultBeaconMemberOfferLoader = 1;
        }
        function failure_call(data, status, headers, config) {
            $scope.loading = false;
            $scope.defaultBeaconMemberOfferLoader = 2;
        }
        setNonDefaultOfferActive(2, 2, offerId, success_call, failure_call);
    }
    

    function setNonDefaultOfferActive(deviceType, offerType, offerId, success_call, error_call) {

        function success(data, status, headers, config) {
            $scope.defaultBeaconMemberOffer = [];
            $scope.defaultBeaconNonMemberOffer = [];
            $scope.defaultKioskMemberOffer = [];
            $scope.defaultKioskNonMemberOffer = [];
            getDefaultOffers();
            success_call(data, status, headers, config);
        }
        function failure(data, status, headers, config) {
            error_call(data, status, headers, config);
        }
        var url = "/Offers/changeDeviceDefaultOffer?deviceType=" + deviceType + "&&offerType=" + offerType + "&&offerId=" + offerId;
        API_SERVICE.postData($scope, $http, "", url, success, failure);
    }

    //----------------------getNonDefault-------------------------------//
    function getNonDefaultBeaconMemberOffer() {
        $scope.nonDefaultBeaconMemberOfferLoader = 0;
        function success_call(data, status, headers, config) {
            $scope.nonDefaultBeaconMemberOffer = data;
            //$scope.offer.date = $scope.offer[0].createdOn.split(' ')[0];
            //table.ajax.reload();
            $scope.loading = false;
            $scope.nonDefaultBeaconMemberOfferLoader = 1;
        }
        function failure_call(data, status, headers, config) {
            $scope.loading = false;
            $scope.nonDefaultBeaconMemberOfferLoader = 2;
        }
        getNonDefaultOffer(1, 1, success_call, failure_call);
    }
    getNonDefaultBeaconMemberOffer();

    function getNonDefaultBeaconNonMemberOffer() {
        $scope.nonDefaultBeaconNonMemberOfferLoader = 0;
        function success_call(data, status, headers, config) {
            $scope.nonDefaultBeaconNonMemberOffer = data;
            //$scope.offer.date = $scope.offer[0].createdOn.split(' ')[0];
            //table.ajax.reload();
            $scope.loading = false;
            $scope.nonDefaultBeaconNonMemberOfferLoader = 1;
        }
        function failure_call(data, status, headers, config) {
            $scope.loading = false;
            $scope.nonDefaultBeaconNonMemberOfferLoader = 2;
        }
        getNonDefaultOffer(2, 1, success_call, failure_call);
    }
    getNonDefaultBeaconNonMemberOffer();

    function getNonDefaultKioskMemberOffer() {
        $scope.nonDefaultKioskMemberOfferLoader = 0;
        function success_call(data, status, headers, config) {
            $scope.nonDefaultKioskMemberOffer = data;
            //$scope.offer.date = $scope.offer[0].createdOn.split(' ')[0];
            //table.ajax.reload();
            $scope.loading = false;
            $scope.nonDefaultKioskMemberOfferLoader = 1;
        }
        function failure_call(data, status, headers, config) {
            $scope.loading = false;
            $scope.nonDefaultKioskMemberOfferLoader = 2;
        }
        getNonDefaultOffer(1, 2, success_call, failure_call);
    }
    getNonDefaultKioskMemberOffer();

    function getNonDefaultKioskNonMemberOffer() {
        $scope.nonDefaultKioskNonMemberOfferLoader = 0;
        function success_call(data, status, headers, config) {
            $scope.nonDefaultKioskNonMemberOffer = data;
            //$scope.offer.date = $scope.offer[0].createdOn.split(' ')[0];
            //table.ajax.reload();
            $scope.loading = false;
            $scope.nonDefaultKioskNonMemberOfferLoader = 1;
        }
        function failure_call(data, status, headers, config) {
            $scope.loading = false;
            $scope.nonDefaultKioskNonMemberOfferLoader = 2;
        }
        getNonDefaultOffer(2, 2, success_call, failure_call);
    }
    getNonDefaultKioskNonMemberOffer();

    function getNonDefaultOffer(offerType, deviceType, success_call, failure_call) {

        function success(data, status, headers, config) {
            success_call(data, status, headers, config);
        }
        function failure(data, status, headers, config) {
           
        }
        function failure(data, status, headers, config) {
            error_call(data, status, headers, config);
        }
        var url = "/Offers/getNonDefaultOffersByType?offerType=" + offerType + "&&deviceType=" + deviceType;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }

    //-----------------------------------------------------//

    $scope.reloadDefaultBeaconMemberOfferList = function () {

    }
    $scope.reloadOfferList = function () {
        getAllOffersList();
    }

    $scope.reloadSelectPicker = function () {
        $('.selectpicker').selectpicker('refresh');
    }
    
    $scope.onSelectListChange = function () {
        if ($scope.selectedCategories == undefined){
            for (var index in $scope.newOffer.categories) {
                $scope.newOffer.categories[index]["is_selected"] = false;
            }
            $scope.newOffer.is_country_disabled = true;
            $scope.newOffer.countries = [];
            return;
        }
        for (var index in $scope.newOffer.categories) {
            if (isSelectedChecked($scope.newOffer.categories[index].id))
                $scope.newOffer.categories[index].is_selected = true;
            else
                $scope.newOffer.categories[index].is_selected = false;
            $scope.newOffer.is_country_disabled = false;
        }
    }

    $scope.$watch('newOffer.is_country_disabled',function(newValue, oldValue){
        $timeout(function(){
                $("#countryDropdown").selectpicker('refresh');
            },100);
    });

    function isSelectedChecked(id) {
        for (var selectedCategory in $scope.selectedCategories) {
            if (id == $scope.selectedCategories[selectedCategory]) {
                return true;
            }
        }
        return false;
    }

    $scope.submit = function (newOfferForm) {
        var x = newOfferForm;
    }

    $scope.submitUpdatedOffer = function (updateOfferForm) {
        var x = updateOfferForm;
    }

    function isCategoryTypeCurrency() {
        var isCurrency = false;

        for (var j = 0; j < $scope.newOffer.categories.length; j++)
        {
            if($scope.newOffer.categories[j].is_selected && $scope.newOffer.categories[j].CategoryUnit=='CURRENCY')
            {
                isCurrency = true;
                break;
            }
        }
        return isCurrency;
    }

    $scope.changeSelectedCountryCurrency = function(){
        if($scope.newOffer.countries){
            $scope.selectedCountryCurrency = $scope.newOffer.countries[0].currency;
            var selectedCurrencyIcon = "";
            for (var i in $scope.newOffer.countries) {
                if (selectedCurrencyIcon != "" && selectedCurrencyIcon != $scope.newOffer.countries[i].currency) {
                    if (isCategoryTypeCurrency()) {
                        $scope.InvalidCountryGroupSelected = true;
                        return;
                    }
                    else {
                        $scope.InvalidCountryGroupSelected = false;
                    }
                }
                else {
                    selectedCurrencyIcon = $scope.newOffer.countries[i].currency;
                    $scope.InvalidCountryGroupSelected = false;
                }
            }
        }
        else{
            $scope.selectedCountryCurrency = "";
        }    
    }
    $scope.gotoOfferDetails = function (id) {
        $state.go('master.offerDetails', { "offerId": id });
    }

    //Import excel for uploading bulk offers START
    $scope.uploadOffers = function () {
        $scope.uploaded = false;
        $scope.uploadButtonName = 'Upload';
        ngDialog.open({
            template: 'uploadOffers',
            className: 'ngdialog-theme-default modal-large beacon-dialog draggable',
            scope: $scope,
            preCloseCallback: function () { $scope.clearUploadModal() }
        });
        $timeout(function () {
            $(".draggable .ngdialog-content").draggable();
        }, 500);
    };

    $scope.fileNameChanged = function (files) {
        if (files.files.length == 0) {
            $scope.fileSelected = false;
            $scope.offersSheet = '';
        } else {
            $scope.fileSelected = true;
            $scope.offersSheet = files.files[0];
        }
    };

    $scope.import = function () {
        if ($scope.offersSheet == '' || $scope.offersSheet == undefined || $scope.offersSheet == null) {
            $scope.alertMode = 'danger';
            $scope.uploadTitle = 'Failure';
            $scope.uploadMessage = '"Please select an excel file before uploading."';
            $scope.uploaded = true;
        } else {
            $scope.uploadButtonName = 'Uploading';
            $scope.uploaded = false;
            console.log($scope.offersSheet);
            $scope.uploadOffer.offersSheet = $scope.offersSheet;
            function success(data, status, headers, config) {
                console.log(data.data);
                $scope.alertMode = 'success';
                $scope.uploadTitle = 'Success';
                $scope.uploadMessage = data.data;
                $scope.uploadButtonName = 'Upload';
                $scope.uploaded = true;
                getAllOffersList();
            }
            function failure(data, status, headers, config) {
                $scope.alertMode = 'danger';
                $scope.uploadTitle = 'Failure';
                $scope.uploadMessage = data.data;
                $scope.uploaded = true;
                $scope.uploadButtonName = 'Upload';
            }
            var url = "/Offers/upload";
            API_SERVICE.postMultiPartData($scope, $http, $scope.uploadOffer, url, success, failure);
        }
    };

    $scope.clearUploadModal = function () {
        $scope.offersSheet = '';
    }

    $scope.showSample = function () {
        $scope.closeModal();
        $state.go('master.offersSample');
    };

    $scope.download = function () {
        window.open("../../../libs/assets/T&CUpload.xlsx", "_blank");
    };

    //Import excel for uploading bulk offers END

}]);