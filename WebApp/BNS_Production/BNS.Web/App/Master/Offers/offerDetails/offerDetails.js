angular.module('bns.master.offerDetails', ['oc.lazyLoad'])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.offerDetails', {
        url: '/offers/offerDetails?offerId',
        views: {
            "offerDetails": {
                templateUrl: 'App/Master/Offers/offerDetails/offerDetails.html',
                controller: 'offerDetailsController'
            }
        }

    });
}])
    .directive('fileInput', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attributes) {
                element.bind('change', function () {
                    $parse(attributes.fileInput)
                    .assign(scope, element[0].files)
                    scope.$apply()
                });
            }
        };
    }])

.controller("offerDetailsController", ['$scope', '$http', 'API_URL', 'ngDialog', 'API_SERVICE', 'LOADING_MESSAGE', '$timeout', '$stateParams', 'MEDIA_URL', function ($scope, $http, API_URL, ngDialog, API_SERVICE, LOADING_MESSAGE, $timeout, $stateParams, MEDIA_URL) {
    $scope.updateButtonName = "Update";
    $scope.saveButtonName = "Save";
    $scope.loadingMessage = LOADING_MESSAGE;
    $scope.image = "";
    var today = new Date();
    today.setHours(0, 0, 0, 0);
    $scope.offerMonthlyUserData = [];
      
    $('#offerDatetime').daterangepicker({

        timePicker: true,
        timePickerIncrement: 1,
        format: 'MM/DD/YYYY h:mm:ss A',
        minDate: today,
        orientation: "auto",
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear'
        },
        "startDate": new Date(),
        "endDate": new Date()
    });

    angular.element('.cancelBtn').click(function () {
        angular.element('#offerDatetime')[0].value = "";
    });

    $scope.offerTypes = [
       { type: "MEMBER", value: 1 },
       { type: "NON_MEMBER", value: 2 },
       { type: "BOTH", value: 3 }
    ];
    $scope.editOfferModel = {
    "termsAndConditions":[],
        "id": "",
        "categories": [],
        "countries": [],
        "title": "",
        "description": "",
        "startTime": "",
        "endTime": "",
        "number": "",
        "imagePath": "",
        "value": "",
        "OfferType": "" 
    };
    $scope.reloadSelectPicker = function () {
        $('.selectpicker').selectpicker('refresh');
    }
    findOfferType = function(offerType){
        for(i=0;i<$scope.offerTypes.length;i++)
        {
            if ($scope.offerTypes[i].type == offerType)
                return $scope.offerTypes[i];
        }
    }
    getTerms = function(terms)
    {
        var termsAndCondition = [];
        for(i=0; i<terms.length; i++)
        {
            var obj = {'id':'','label':''};
            obj.id = terms[i].id;
            obj.label = terms[i].title;
            termsAndCondition.push(obj);
        }
        return termsAndCondition;
    }

    $scope.offerMonthlyUserData = [];
    $scope.offerCashMonthlyUserData = [];
    $scope.offerPointsMonthlyUserData = [];
    $scope.offerPromotionMonthlyUserData = [];
    function drawCharts() {
        Highcharts.setOptions({
            lang: {
                noData: 'No data present.'
            }
        });

        var availedOfferCount = $scope.offerDashboardData.offerAvailedCountForChart;
        var ignoredOfferCount = $scope.offerDashboardData.offerIgnoredCountForChart;
        var dataToshowAvailedVsIgnoredOffers = [];
        if (availedOfferCount > 0 || ignoredOfferCount > 0) {
            dataToshowAvailedVsIgnoredOffers = [{
                name: 'Number of Times Offers Ignored',
                y: ignoredOfferCount
            }, {
                name: 'Number of Times Offers Availed',
                y: availedOfferCount,
                sliced: false,
                selected: true
            }];
        }
        else {
            dataToShowMembersVsAnonymousUser = [];
        }

        Highcharts.chart('OffersSeenAvailedOfferDetails', {
            colors: ['#E55B3C', '#50B432'],
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'No. of times the offer has been availed and No. of times the offer has been ignored'
            },
            credits: {
                enabled: false
            },
            tooltip: {
                pointFormat: '<b>{point.y}</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Offers',
                colorByPoint: true,
                data: dataToshowAvailedVsIgnoredOffers
            }]
        });

        Highcharts.chart('TimeVsRegUsersOfferDetails', {

            series: [{
                name: 'Registered User',
                data: $scope.offerMonthlyUserData
                //data: [[1483228800000, 567], [1485907200000, 685], [1488326400000, 1025], [1493596800000, 986], [1496275200000, 2358]]
            }],
            title: {
                text: 'Registered users in last 5 months'
            },
            credits: {
                enabled: false
            },
            yAxis: {
                maxPadding: 0.10,
                title: {
                    text: 'Number of users registered'
                }
            },
            xAxis: {
                maxPadding: 0.10,
                title: {
                    text: 'Month'
                },
                startOnTick: true,
                type: 'datetime',
                dateTimeLabelFormats: {
                    month: '%b %Y'
                }
            }
        });
    }

    //------------- get offer Dashboard data -----------------------------//
    function formatMonthlyUserdata(rawMonthlyUserData) {
        if (!rawMonthlyUserData)
        {
            rawMonthlyUserData = [];
        }
        var monthlyUserData = [];
        switch (rawMonthlyUserData.length) {
            case 5: break;
            case 0: var date = new Date(); date.setDate(1); date.setHours(-1);
                rawMonthlyUserData[0] = { "userDate": date, "userCount": 0 };
            case 1: rawMonthlyUserData.splice(1, 0, { "userDate": new Date(moment(rawMonthlyUserData[0].userDate).subtract(1, 'months')), "userCount": 0 });
            case 2: rawMonthlyUserData.splice(2, 0, { "userDate": new Date(moment(rawMonthlyUserData[0].userDate).subtract(2, 'months')), "userCount": 0 });
            case 3: rawMonthlyUserData.splice(3, 0, { "userDate": new Date(moment(rawMonthlyUserData[0].userDate).subtract(3, 'months')), "userCount": 0 });
            case 4: rawMonthlyUserData.splice(4, 0, { "userDate": new Date(moment(rawMonthlyUserData[0].userDate).subtract(4, 'months')), "userCount": 0 });
                break;
        }
        for (i = 0; i < rawMonthlyUserData.length ; i++) {
            var tempDate = new Date(rawMonthlyUserData[i].userDate)
            //monthlyUserData.push([Date.UTC(tempDate.getFullYear(), tempDate.getMonth(), tempDate.getDay()), rawMonthlyUserData[i].userCount]);
            monthlyUserData.push([tempDate.getTime(), rawMonthlyUserData[i].userCount]);
        }
        return monthlyUserData;
    }

    function getOfferDashboardInfo() {
        $scope.offerDashboardDataLoader = 0;      
        function success(data, status, headers, config) {
            $scope.offerDashboardData = data;
            $scope.offerMonthlyUserData = formatMonthlyUserdata(data.monthlyUserCount);
            $scope.offerDashboardDataLoader = 1;
            $timeout(function () { drawCharts(); }, 200);
            
        }
        function failure(data, status, headers, config) {
            $scope.offerDashboardDataLoader = 2;      
        }
        var url = "/dashboard/getOfferDashboardData?offer_id=" + $stateParams.offerId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getOfferDashboardInfo();
    $scope.reloadOfferDashboardData = function () {
        getOfferDashboardInfo();
    }
    //----------------------- Update offer modal -------------------------//
    function buildCountries(countryList)
    {
        countries = [];
        for (i = 0; i < countryList.length; i++) {
            var obj = { 'id': '', 'label': '' };
            obj.id = countryList[i].id;
            obj.label = countryList[i].name;
            countries.push(obj);
        }
        return countries;
    }
    $scope.setEditOfferModel = function (offer) {
     
        $scope.reloadSelectPicker();
        $scope.editOfferModel.OfferType = findOfferType(offer.OfferType);
        $scope.editOfferModel.id = offer.id;
        $scope.editOfferModel.categories = offer.categories;
        $scope.editOfferModel.countries = buildCountries(offer.countries);
        //$scope.editOfferModel.countries = offer.countries;
        $scope.editOfferModel.title = offer.title;
        $scope.editOfferModel.description = offer.description;
        $scope.editOfferModel.termsAndConditions =[];
        $scope.editOfferModel.termsAndConditions = getTerms(offer.termsAndConditions);
        //$scope.editOfferModel.termsAndConditions = offer.termsAndConditions;
        $scope.editOfferModel.duration = offer.duration;
        $scope.editOfferModel.startTime = offer.startTime;
        $scope.editOfferModel.endTime = offer.endTime;
        $scope.date = offer.startTime + " - " + offer.endTime;
        $scope.editOfferModel.number = offer.number;
        $scope.editOfferModel.imagePath = offer.imagePath;
        $scope.editOfferModel.value = offer.value;
    }

    $scope.updateOffer = function () {
        ngDialog.open({
            template: 'updateOfferDialog',
            className: 'draggable',
            scope: $scope,
            preCloseCallback: function () { $scope.clearUpdateModal() }
        });
        $timeout(function () {
            $(".draggable ngdialog-content").draggable();
        }, 500);
    }
    $scope.closeUpdateModal = function () {
        ngDialog.close({
            template: 'updateOfferDialog',
            scope: $scope
        });
        $scope.clearUpdateModal();
    }

    $scope.clearUpdateModal = function () {
        $scope.submitted = false;
        $scope.successAlert = false;
        $scope.faliureAlert = false;
        $scope.date = "";
        $scope.editOfferModel = {
            "termsAndConditions": [],
            "id": "",
            "categories": [],
            "countries": [],
            "title": "",
            "description": "",
            "startTime": "",
            "endTime": "",
            "number": "",
            "imagePath": "",
            "value": "",
            "OfferType": ""
        };
        $('#fileInput').val(null);
        $('#offerDatetime').val('');
        $scope.dateError = false;
        getAllCategories();
    }
    // example for mmultiselect button
    $scope.countriesDropDownSetting = { disable: true, smartButtonMaxItems: 5, smartButtonTextConverter: function (itemText, originalItem) { return itemText; } };
    $scope.termsAndConditionDropDownSetting = { smartButtonMaxItems: 5, smartButtonTextConverter: function (itemText, originalItem) { return itemText; } };
    $scope.termsAndConditionsButtonText = { buttonDefaultText: 'Choose terms and conditions...', checkAll: 'Select All', uncheckAll: 'Deselect All' };
    $scope.countriesButtonText = { buttonDefaultText: 'Choose Countries...', checkAll: 'Select All', uncheckAll: 'Deselect All' };

    function getAllCountries() {
        $scope.loading = true;
        function success(data, status, headers, config) {
            for (var i in data) {
                data[i].name = data[i].name + " (" + data[i].currency + ")";
                data[i].label = data[i].name;
            }
            $scope.countries = data;
            console.log($scope.stores);
            $scope.loading = false;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
        }
        var url = "/countries/all";
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getAllCountries();

    function builTermsAndCondition(terms) {
        termsAndConditions = [];
        for(i=0; i<terms.length; i++)
        {
            var obj = { 'id': '', 'label': '' };
            obj.id = terms[i].id;
            obj.label = terms[i].title;
            termsAndConditions.push(obj);
        }
        return termsAndConditions;
    }

    getTermsAndConditions = function () {
        function success(data, status, headers, config) {
            $scope.termsAndConditions = builTermsAndCondition(data);
        }
        function failure(data, status, headers, config) {
            $scope.notify("Terms and conditions are not loaded", 4);
        }
        var url = '/TermsAndConditions/get';
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getTermsAndConditions();

    function buildCategories(rawCategories)
    {
        for (i = 0; i < rawCategories.length; i++) {
            rawCategories[i].label = rawCategories[i].name;       
        }
        return rawCategories;
    }

    function getAllCategories() {
        $scope.loading = true;
        function success(data, status, headers, config) {
            //$scope.categories = data;
            //setTimeout(function () {
            $scope.categories = buildCategories(data);
            //}, 0);
            console.log($scope.categories);
            $scope.loading = false;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
        }
        var url = "/Categories/all";
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getAllCategories();

    getOfferDetails = function () {
        $scope.loading = true;
        $scope.dataLoader = 0;
        function success(data, status, headers, config) {
            $scope.offerDetails = data;
            $scope.changeBreadcrums([{ "name": "Offers", "state": "master.offers" }, { "name": data.title, "state": "master.offerDetails", "params": { "offerId": $stateParams.offerId } }]);
            $scope.loading = false;
            $scope.dataLoader = 1;

            if (data.hasOwnProperty('imagePath') && data.imagePath != 'No Image') {
                $scope.offerImg = MEDIA_URL + $scope.offerDetails.imagePath;
            } else {
                if (data.categories[0].name == 'Cash') {
                    $scope.offerImg = MEDIA_URL + '/Resources/offerImages/Cash.jpg';
                } else if (data.categories[0].name == 'Points') {
                    $scope.offerImg = MEDIA_URL + '/Resources/offerImages/Points.jpg';
                } else if (data.categories[0].name == 'Promotion') {
                    $scope.offerImg = MEDIA_URL + '/Resources/offerImages/Promotions.jpg';
                } else {
                    $scope.offerImg = MEDIA_URL + '/Resources/offerImages/defaultNoCategory.jpg';
                }
            }

        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.dataLoader = 2;
        }
        var url = "/Offers/details?offerId=" + $stateParams.offerId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getOfferDetails();
    $scope.reloadOfferData = function () {
        getOfferDetails();
    }

    $scope.getOfferImg = function (icon) {
        $scope.imagePath = MEDIA_URL + icon;
        return $scope.imagePath;
    }
    //----------Update offer API---------------------//
    $scope.updateNewOffer = function (form) {

        console.log("Update API under progress.");
        $scope.saveButtonName = "Saving";
        //---- here add code for updating beacon api call ------------//
        console.log($('#offerDatetime').val());
        console.log($scope.image.file);
        $scope.submitted = true;

        $scope.editOfferModel.startTime = ($('#offerDatetime').val()).split('-')[0];
        $scope.editOfferModel.endTime = ($('#offerDatetime').val()).split('-')[1];
        $scope.editOfferModel.imagePath = $scope.image[0];
        if (form.$valid == false || $scope.InvalidCountryGroupSelected || $scope.editOfferModel.categories == "" || $scope.editOfferModel.categories == undefined || $scope.editOfferModel.countries == "" || $scope.editOfferModel.countries == undefined || $scope.editOfferModel.title == ""
            || $scope.editOfferModel.title == undefined || $scope.editOfferModel.description == "" || $scope.editOfferModel.description == undefined || $scope.editOfferModel.startTime == "" || $scope.editOfferModel.startTime == undefined
            || $scope.editOfferModel.endTime == "" || $scope.editOfferModel.endTime == undefined || $scope.editOfferModel.duration == "" || $scope.editOfferModel.duration == undefined ) {
            if ($scope.editOfferModel.startTime == "" || $scope.editOfferModel.startTime == undefined
            || $scope.editOfferModel.endTime == "" || $scope.editOfferModel.endTime == undefined) {
                $scope.dateError = true;
            }
            else {
                $scope.dateError = false;
            }
            $scope.saveButtonName = "Save";
        }
        else {
            $scope.saveButtonName = "Saving";
            $scope.dateError = false;
            function success(data, status, headers, config) {
                getOfferDetails();
                $scope.notify("Offer with title \"" + $scope.editOfferModel.title + "\" updated successfully.", 1);
                $scope.submitted = false;
                $scope.successAlert = true;
                $scope.faliureAlert = false;
                $scope.image = "";
                $scope.editOfferModel = {
                    "termsAndConditions": [],
                    "id": "",
                    "categories": [],
                    "countries": [],
                    "title": "",
                    "description": "",
                    "startTime": "",
                    "endTime": "",
                    "number": "",
                    "imagePath": "",
                    "value": "",
                    "OfferType": ""
                };
                $('#fileInput').val(null);
                $('#offerDatetime').val('');
                $scope.loading = false;
                $scope.closeUpdateModal();
                $scope.saveButtonName = "Save";
            }
            function failure(data, status, headers, config) {
                $scope.loading = false;
                $scope.saveButtonName = "Save";
                $scope.successAlert = false;
                $scope.faliureAlert = true;
                if (status == 409) {
                    $scope.errorMessage = data;
                }
                else {
                    $scope.errorMessage = "Could not add offer to the list. Please check all the fields."
                }
            }
            var url = "/Offers/update";
            API_SERVICE.postMultiPartData($scope, $http, $scope.editOfferModel, url, success, failure);
        }
    }

    $scope.changeSelectedCountryCurrency = function () {
        if ($scope.newOffer.countries) {
            $scope.selectedCountryCurrency = $scope.newOffer.countries[0].currency;
            var selectedCurrencyIcon = "";
            for (var i in $scope.newOffer.countries) {
                if (selectedCurrencyIcon != "" && selectedCurrencyIcon != $scope.newOffer.countries[i].currency) {
                    $scope.InvalidCountryGroupSelected = true;
                    return;
                }
                else {
                    selectedCurrencyIcon = $scope.newOffer.countries[i].currency;
                    $scope.InvalidCountryGroupSelected = false;
                }
            }
        }
        else {
            $scope.selectedCountryCurrency = "";
        }
    }
    $scope.onSelectListChange = function () {
        //if ($scope.selectedCategories == undefined) {
        //    for (var index in $scope.newOffer.categories) {
        //        $scope.newOffer.categories[index]["is_selected"] = false;
        //    }
        //    $scope.newOffer.is_country_disabled = true;
        //    $scope.newOffer.countries = [];
        //    return;
        //}
        //for (var index in $scope.newOffer.categories) {
        //    if (isSelectedChecked($scope.newOffer.categories[index].id))
        //        $scope.newOffer.categories[index].is_selected = true;
        //    else
        //        $scope.newOffer.categories[index].is_selected = false;
        //    $scope.newOffer.is_country_disabled = false;
        //}
    }
    $scope.checkDate = function () {
        $scope.editOfferModel.startTime = ($('#offerDatetime').val()).split('-')[0];
        $scope.editOfferModel.endTime = ($('#offerDatetime').val()).split('-')[1];
        if ($scope.editOfferModel.startTime == "" || $scope.editOfferModel.endTime == "" || $scope.editOfferModel.startTime == undefined || $scope.editOfferModel.endTime == undefined) {
            $scope.dateError = true;
        }
        else {
            $scope.dateError = false;
        }
    }

}]);