﻿angular.module('bns.master.offersSample', [])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.offersSample', {
        url: '/offersSample',
        views: {
            "offersSample": {
                templateUrl: 'App/Master/Offers/offersSample.html',
                controller: 'offersSampleController'
            }
        }
    });
}])
.controller("offersSampleController", ['$scope', '$http', 'API_URL', 'LOADING_MESSAGE', 'API_SERVICE', '$state', 'ngDialog', '$timeout',
    function ($scope, $http, API_URL, LOADING_MESSAGE, API_SERVICE, $state, ngDialog, $timeout) {

        $scope.changeBreadcrums([{ "name": "Offers", "state": "master.offers" }, { "name": "Sample Document", "state": "" }]);

        $scope.download = function () {
            window.open("../../../libs/assets/offerUpload.xlsx", "_blank");
        };

    }])