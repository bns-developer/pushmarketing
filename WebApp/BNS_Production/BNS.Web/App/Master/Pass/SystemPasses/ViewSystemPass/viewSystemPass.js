﻿angular.module('bns.master.viewSystemPass', [])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.viewSystemPass', {
        url: '/pass/systemPass/viewSystemPass?passID',
        views: {
            "viewSystemPass": {
                templateUrl: 'App/Master/Pass/SystemPasses/ViewSystemPass/viewSystemPass.html',
                controller: 'viewSystemPassController'
            }
        }
    });
}])

.controller("viewSystemPassController", ['$scope', '$state', '$http', '$stateParams', 'API_URL', 'ngDialog', 'API_SERVICE', 'MEDIA_URL', '$timeout', function ($scope, $state, $http, $stateParams, API_URL, ngDialog, API_SERVICE, MEDIA_URL, $timeout) {
    $scope.changeBreadcrums([{ "name": "Pass", "state": "bns.master.pass" }]);
    $scope.passID = $stateParams.passID;
    /*  ---------------------- Navigation Function --------------------------  */
    $scope.goBack = function () {
        $state.go("master.pass");
    }
    $scope.goToEditSystemPass = function (passID) {
        $state.go('master.editSystemPass', { 'passID': passID });
    }
}])