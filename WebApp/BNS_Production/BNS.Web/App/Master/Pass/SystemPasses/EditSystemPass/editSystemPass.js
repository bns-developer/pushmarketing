﻿angular.module('bns.master.editSystemPass', [])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.editSystemPass', {
        url: '/pass/systemPass/editSystemPass?passID',
        views: {
            "editSystemPass": {
                templateUrl: 'App/Master/Pass/SystemPasses/EditSystemPass/editSystemPass.html',
                controller: 'editSystemPassController'
            }
        }
    });
}])

.controller("editSystemPassController", ['$scope', '$state', '$http', '$stateParams', 'API_URL', 'ngDialog', 'API_SERVICE', 'MEDIA_URL', '$timeout', function ($scope, $state, $http, $stateParams, API_URL, ngDialog, API_SERVICE, MEDIA_URL, $timeout) {
    $scope.changeBreadcrums([{ "name": "Pass", "state": "bns.master.pass" }]);
    $scope.passID = $stateParams.passID;
    /*  ---------------------- Navigation Function --------------------------  */
    $scope.goBack = function () {
        $state.go("master.pass");
    }
    /*  ----------------------  Save Function  ------------------------------- */
    $scope.saveSystemPass = function(){
        $scope.notify("Save System Pass Module is under progress", 2);
    }
}])