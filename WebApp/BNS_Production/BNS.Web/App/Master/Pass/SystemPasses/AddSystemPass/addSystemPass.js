﻿angular.module('bns.master.addSystemPass',['oc.lazyLoad'])
    
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.addSystemPass', {
        url: '/addSystemPass',
        views: {
            "addSystemPass": {
                templateUrl: 'App/Master/Pass/SystemPasses/AddSystemPass/addSystemPass.html',
                controller: 'addSystemPassController'
            }
        }
    });
}])
.controller("addSystemPassController", ['$scope', '$state', '$http', '$window', 'API_URL', 'ngDialog', 'API_SERVICE', 'MEDIA_URL', '$timeout', 'DTOptionsBuilder', 'DTColumnDefBuilder', function ($scope, $state, $http, $window, API_URL, ngDialog, API_SERVICE, MEDIA_URL, $timeout, DTOptionsBuilder, DTColumnDefBuilder) {
    /*  ---------------------- Navigation Function --------------------------  */
    $scope.goBack = function () {
        $state.go("master.pass");
    }
    /*  ----------------------  Save Function  ------------------------------- */
    $scope.saveSystemPass = function () {
        $scope.notify("Save System Pass Module is under progress", 2);
    }
}])