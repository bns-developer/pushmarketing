﻿angular.module('bns.master.addTemplate', ['oc.lazyLoad'])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.addTemplate', {
        url: '/addTemplate',
        views: {
            "addTemplate": {
                templateUrl: 'App/Master/Pass/Templates/AddTemplate/addTemplate.html',
                controller: 'addTemplateController'
            }
        }
    });
}])
.controller("addTemplateController", ['$scope', '$state', '$http', '$window', '$document', 'API_URL', 'ngDialog', 'API_SERVICE', 'MEDIA_URL', '$timeout', 'DTOptionsBuilder', 'DTColumnDefBuilder', function ($scope, $state, $http, $window, $document, API_URL, ngDialog, API_SERVICE, MEDIA_URL, $timeout, DTOptionsBuilder, DTColumnDefBuilder) {
    /* ----------------------- Initailize Variables ----------------------------- */
    $scope.newPassTemplate = {
        "passType" : ""
    };
    $scope.demoQrCodeImage = MEDIA_URL + "/Resources/Images/DemoQRCode.png";
    $scope.eventImageType = 1;
    /*  ---------------------- Initialize Color Picker ---------------------------  */
    $scope.backgroundColor = '#37abab';
    $scope.labelTextColor = '#48ad25';
    $scope.valueTextColor = '#cf1e1e';
     
    $('#backgroundColorPicker').colorpicker({
        color: $scope.backgroundColor,
        format: null
    });
    $('#labelTextColorPicker').colorpicker({
        color: $scope.labelTextColor,
        format: null
    });
    $('#valueTextColorPicker').colorpicker({
        color: $scope.valueTextColor,
        format: null
    });
    /* ----------------------- Remove Image -------------------------------------- */
    $scope.removeIcon = function () {
        delete $scope.iconImageModel;
    }

    $scope.removeStripImage = function () {
        delete $scope.stripImageModel;
    }
    $scope.removeLogo = function () {
        delete $scope.logoImageModel;
    }
    $scope.removePersonalizationLogo = function () {
        delete $scope.personalizationLogoImageModel;
    }
    $scope.removeFooterImage = function () {
        delete $scope.footerImageModel;
    }
    $scope.removeBackgroundImage = function () {
        delete $scope.backgroundImageModel;
    }
    $scope.removeThumbnailImage = function()
    {
        delete $scope.thumbnailImageModel;
    }
    /*  ---------------------- Declare pass module variables ---------------------- */
    $scope.passTypes = [
        { "type": "Transit", "value": "1" },
        { "type": "Coupon", "value": "2" },
        { "type": "Store Card", "value": "3" },
        { "type": "Membership", "value": "4" },
        { "type": "Event Ticket", "value": "5" }
    ];
    $scope.timeZones = [
        { "timezone": "Z", "value": "1" },
        { "timezone": "+01:00", "value": "2" },
        { "timezone": "+02:00", "value": "3" },
        { "timezone": "+03:00", "value": "4" },
        { "timezone": "+04:00", "value": "5" },
        { "timezone": "+05:00", "value": "6" },
        { "timezone": "+06:00", "value": "7" },
        { "timezone": "+07:00", "value": "8" },
        { "timezone": "+08:00", "value": "9" },
        { "timezone": "+09:00", "value": "10" },
        { "timezone": "+10:00", "value": "11" },
        { "timezone": "+11:00", "value": "12" },
        { "timezone": "+12:00", "value": "13" },
        { "timezone": "+13:00", "value": "14" },
        { "timezone": "+14:00", "value": "15" },
        { "timezone": "-12:00", "value": "16" },
        { "timezone": "-11:00", "value": "17" },
        { "timezone": "-10:00", "value": "18" },
        { "timezone": "-09:30", "value": "19" },
        { "timezone": "-09:00", "value": "20" },
        { "timezone": "-08:00", "value": "21" },
        { "timezone": "-07:00", "value": "22" },
        { "timezone": "-06:00", "value": "23" },
        { "timezone": "-05:00", "value": "24" },
        { "timezone": "-04:30", "value": "25" },
        { "timezone": "-04:00", "value": "26" },
        { "timezone": "-03:30", "value": "27" },
        { "timezone": "-03:00", "value": "28" },
        { "timezone": "-02:30", "value": "29" },
        { "timezone": "-02:00", "value": "30" },
        { "timezone": "-01:00", "value": "31" },
        { "timezone": "+03:30", "value": "32" },
        { "timezone": "+04:30", "value": "33" },
        { "timezone": "+05:30", "value": "34" },
        { "timezone": "+05:45", "value": "35" },
        { "timezone": "+06:30", "value": "36" },
        { "timezone": "+08:45", "value": "37" },
        { "timezone": "+09:30", "value": "38" },
        { "timezone": "+10:30", "value": "39" },
        { "timezone": "+11:30", "value": "40" },
        { "timezone": "+12:45", "value": "41" },
    ];
    /*  ---------------------- Navigation Function --------------------------  */
    $scope.goBack = function()
    {
        $state.go("master.pass");
    }
    /*  ----------------------  Save Function  ------------------------------- */
    $scope.savePassTemplate = function () {
        $scope.notify("Save Pass Template Module is under progress", 2);
    }
}])