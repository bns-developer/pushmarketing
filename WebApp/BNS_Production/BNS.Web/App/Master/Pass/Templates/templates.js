﻿angular.module('bns.master.templates', [])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.templates', {
        url: '/pass/templates',
        views: {
            "templates": {
                templateUrl: 'App/Master/Pass/Template/templates.html',
                controller: 'templatesController'
            }
        }
    });
}])

.controller("templatesController", ['$scope', '$state', '$http', '$stateParams', 'API_URL', 'ngDialog', 'API_SERVICE', 'MEDIA_URL', '$timeout', function ($scope, $state, $http, $stateParams, API_URL, ngDialog, API_SERVICE, MEDIA_URL, $timeout) {
    $scope.changeBreadcrums([{ "name": "Pass", "state": "bns.master.pass" }, { "name": "Templates", "state": "bns.master.templates" }]);
    $scope.passId = $stateParams.passID;
}])