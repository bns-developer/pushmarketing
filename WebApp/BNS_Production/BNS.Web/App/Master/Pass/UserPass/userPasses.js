﻿angular.module('bns.master.userPasses',['oc.lazyLoad'])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.userPasses', {
        url: '/userPass',
        views: {
            "userPasses": {
                templateUrl: 'App/Master/Pass/UserPass/userPasses.html',
                controller: 'passController'
            }
        }
    });
}])
.controller("passController", ['$scope', '$state', '$http', 'API_URL', 'ngDialog', 'API_SERVICE', 'MEDIA_URL', '$timeout', function ($scope, $state, $http, API_URL, ngDialog, API_SERVICE, MEDIA_URL, $timeout) {
    $scope.dataLoader = 1;
    $scope.changeBreadcrums([{ "name": "Pass", "state": "bns.master.pass" }, { "name": "Pass", "state": "bns.master.userPasses" }]);
}]);