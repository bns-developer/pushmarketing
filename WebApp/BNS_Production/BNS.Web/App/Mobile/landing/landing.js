angular.module('bns.mobile.landing', [])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
    .state('mobile.landing', {
        url: "/beacon/landing",
        views: {
            "landing": {
                templateUrl: "App/Mobile/landing/landing.html",
                controller: 'landingCtrl'
            },
        }
    });
}])
.controller('landingCtrl', ['$state', '$scope', '$stateParams','$rootScope', 'API_URL', 'IMAGE_URL', 'API_SERVICE', '$http', function ($state, $scope, $stateParams, $rootScope, API_URL, IMAGE_URL, API_SERVICE, $http) {

    //alert($rootScope.beaconId);

    function getAllOffers() {
        function success(data, status, headers, config) {
            $scope.offers = data;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
        }
        var url = "/Offers/getOffersForLandingPage";
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getAllOffers();

    $scope.gotoViewOffer = function (offerId, categories, imagePath, endTime) {
        $scope.offer = { categories: "", offerId: "", imagePath: "", endTime: "" };
        $scope.offer.offerId = offerId;
        $scope.offer.categories = categories;
        $scope.offer.imagePath = imagePath;
        $scope.offer.endTime = endTime;
        $state.go("mobile.viewOffer", { "offer": $scope.offer });
    }

    $scope.getBackgroundPic = function (offer) {
        if (offer.hasOwnProperty('imagePath') && (offer.imagePath != "No Image")) {
            return(IMAGE_URL + (offer.imagePath));
        }
        else
        {
            if (offer.categories[0].name == "Cash") {
                return (IMAGE_URL + "/Resources/offerImages/Cash.jpg");
            } else if (offer.categories[0].name == "Points") {
                return (IMAGE_URL + "/Resources/offerImages/Points.jpg");
            } else if (offer.categories[0].name == "Promotion") {
                return (IMAGE_URL + "/Resources/offerImages/Promotions.jpg");
            }
        }
        
    }

    $scope.getTimeLeft = function (endTime) {
        endTime = new Date(endTime);
        return moment(endTime).fromNow();
    }

    $scope.currentDate = new Date();

}])
