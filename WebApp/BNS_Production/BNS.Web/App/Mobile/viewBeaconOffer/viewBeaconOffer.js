﻿angular.module('bns.mobile.viewBeaconOffer', [])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
    .state('mobile.viewBeaconOffer', {
        url: "/beaconCurrentOffer?bid",
        views: {
            "viewBeaconOffer": {
                templateUrl: "App/Mobile/viewBeaconOffer/viewBeaconOffer.html",
                controller: 'viewBeaconOfferCtrl',
            },
        }
    });
}])
.controller('viewBeaconOfferCtrl', ['$state', '$scope', '$rootScope', '$stateParams', 'API_URL', 'IMAGE_URL', 'API_SERVICE', '$http', 'TimerService', function ($state, $scope, $rootScope, $stateParams, API_URL, IMAGE_URL, API_SERVICE, $http, TimerService) {

    $rootScope.beaconId = $stateParams.bid;
    $.cookie("beaconId", $rootScope.beaconId, { expires: 120 });
    $scope.details = {
        'memberId': undefined,
        'beaconGuid': $stateParams.bid,
        'accountId' : undefined
    }
    $scope.userOffer = { categories: "", offerId: "", imagePath: "", endTime: "" };

    $scope.typeId = 0;

    //function getBeaconOffer() {
    //    function success(data, status, headers, config) {
    //        $scope.beaconOffer = data;
    //        if (data.hasOwnProperty('imagePath'))
    //        {
    //            $scope.imgsrc = IMAGE_URL + data.imagePath;
    //        }
    //        else {
    //            if (data.categories[0].name == "Cash") {
    //                $scope.imgsrc = IMAGE_URL + "/Resources/offerImages/Cash.jpg";
    //            } else if (data.categories[0].name == "Points") {
    //                $scope.imgsrc = IMAGE_URL + "/Resources/offerImages/Points.jpg";
    //            } else if (data.categories[0].name == "Promotions") {
    //                $scope.imgsrc = IMAGE_URL +  "/Resources/offerImages/Promotions.jpg";
    //            }
    //        }
          
    //    }
    //    function failure(data, status, headers, config) {
    //        $scope.loading = false;
    //    }
    //    var url = "/Offers/getCurrentActiveOfferForBeacon?beacon_id=" + $rootScope.beaconId;
    //    API_SERVICE.getData($scope, $http, url, success, failure);
    //}
    //getBeaconOffer();

    $scope.getUserOffer = function () {
        var val = $.cookie('memberId');
        if (val == undefined) {
        } else {
            $scope.details.memberId = val;
        }
        function success(data, status, headers, config) {
            console.log(data);
            $.cookie("memberId", data.memberId, { expires: 365 });
            $scope.beaconOffer = data.offer;
            $scope.typeId = data.typeId;
            $scope.beaconId = data.beaconId;
            $scope.userOffer.accountId = data.accountId;
            TimerService.initializeTimer(data.offer.duration);
            if (data.hasOwnProperty('imagePath')) {
                $scope.imgsrc = IMAGE_URL + data.offer.imagePath;
            }
            else {
                if (data.offer.categories[0].name == "Cash") {
                    $scope.imgsrc = IMAGE_URL + "/Resources/offerImages/Cash.jpg";
                } else if (data.offer.categories[0].name == "Points") {
                    $scope.imgsrc = IMAGE_URL + "/Resources/offerImages/Points.jpg";
                } else if (data.offer.categories[0].name == "Promotions") {
                    $scope.imgsrc = IMAGE_URL + "/Resources/offerImages/Promotions.jpg";
                }
            }
        }
        function failure(data, status, headers, config) {
            alert(data);
        }
        var url = "/Offers/getUserOfferForBeacon";
        API_SERVICE.postData($scope, $http, $scope.details, url, success, failure);
    }
    $scope.getUserOffer();

    $scope.gotoNextPage = function () {
        $scope.userOffer.categories = $scope.beaconOffer.categories;
        $scope.userOffer.offerId = $scope.beaconOffer.id;
        $scope.userOffer.imagePath = $scope.imgsrc;
        $scope.userOffer.endTime = $scope.beaconOffer.endTime;
        $scope.userOffer.beaconId = $scope.beaconId;

        var commonCurrencySymbol = "$";
        if ($scope.beaconOffer.countries.length > 0)
            commonCurrencySymbol = $scope.beaconOffer.countries[0].currency;
        for(var i in $scope.userOffer.categories){
            if($scope.userOffer.categories[i].name == 'Cash')
                $scope.userOffer.categories[i]["currencySymbol"] = commonCurrencySymbol;
        }
        if ($scope.typeId == 2)
        {
            $state.go('mobile.getUserDetail', { 'userOffer': $scope.userOffer });
        } else 
        {
            $state.go('mobile.getMemberID', { 'userOffer': $scope.userOffer });
        }
    }
    
    $scope.gotoLanding = function()
    {
        $state.go('mobile.landing')
    }

    $scope.getTimeLeft = function (endTime) {
        endTime = new Date(endTime);
        return moment(endTime).fromNow();
    }

    $(window).bind("beforeunload",function(event) {
        return "Offer will be discarded. Are you sure you want to continue?";
    });

    $scope.$on('$destroy', function() {
        delete window.onbeforeunload;
    });

}])
