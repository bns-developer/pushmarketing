angular.module('bns.mobile.getMemberID', [])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
    .state('mobile.getMemberID', {
        url: "/beacon/memberDetails",
        views: {
            "getMemberID": {
                templateUrl: "App/Mobile/getMemberID/getMemberID.html",
                controller: 'getMemberIDCtrl'
            },
        },
        params: {
            userOffer: {
            }
        }
    });
}])
.controller('getMemberIDCtrl', ['$state', '$scope', '$rootScope', '$stateParams', 'API_SERVICE', '$http', 'IMAGE_URL', 'TimerService', function ($state, $scope, $rootScope, $stateParams, API_SERVICE, $http, IMAGE_URL, TimerService) {

    $scope.memberId = 0;

    if ($stateParams.userOffer == undefined) {
        $state.go("mobile.landing");
        return;
    }

    $scope.userDetails = {
        memberId: 0,
        offerId: 0,
        beaconId: $stateParams.userOffer.beaconId,
        accountId: $stateParams.userOffer.accountId
    };

    console.log($stateParams);

    function init() {
        $scope.userOffer = { categories: "", offerId: "", imagePath: "", endTime: "" };
        $scope.userOffer.categories = $stateParams.userOffer.categories;
        $scope.userOffer.offerId = $stateParams.userOffer.offerId;
        $scope.userOffer.imagePath = $stateParams.userOffer.imagePath;
        $scope.userOffer.endTime = $stateParams.userOffer.endTime;

        if ($scope.userOffer.hasOwnProperty('imagePath')) {
            $scope.imgsrc = $scope.userOffer.imagePath;
        }
        else {
            if ($scope.userOffer.categories[0].name == "Cash") {
                $scope.imgsrc = "/Resources/offerImages/Cash.jpg";
            } else if ($scope.userOffer.categories[0].name == "Points") {
                $scope.imgsrc = "/Resources/offerImages/Points.jpg";
            } else if ($scope.userOffer.categories[0].name == "Promotions") {
                $scope.imgsrc = "/Resources/offerImages/Promotions.jpg";
            }
        }
        
    }
    init();

    $scope.getOffer = function (offerId) {
        function success(data, status, headers, config) {
            console.log(data);
            $scope.offer = data;
            $scope.userDetails.offerId = data.id;
        }
        function failure(data, status, headers, config) {
            alert(data);
        }
        var url = "/Offers/details?offerId=" + offerId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    $scope.getOffer($scope.userOffer.offerId);

    $scope.submitForm = function () {
        var val = $.cookie('memberId');
        alert(val);
        if (val == $scope.userDetails.memberId) {
            function success(data, status, headers, config) {
                console.log(data);
                $scope.offer = data;
                TimerService.stopTimer();
                $state.go('mobile.congoNewUser', { 'userOffer': $scope.userOffer });
            }
            function failure(data, status, headers, config) {
                alert(data);
            }
            var url = "/users/member/acceptOffer";
            API_SERVICE.postData($scope, $http, $scope.userDetails, url, success, failure);
        } else {
            alert("Incorrect Member ID")
        }
    }

    }])
