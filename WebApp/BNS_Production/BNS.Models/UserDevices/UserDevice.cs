﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.BNSModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using WSDatabase;
using System.Data.SqlClient;

namespace BNS.Models.UserDevices
{
    public class UserDevice : BNSModel
    {

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String push_id { get; set; }

        public int userDeviceType;

        public enum User_Device_Type
        {
            ANDROID = 1,
            IOS = 2,
            BROWSER = 3
        };

        private User_Device_Type _type;

        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public User_Device_Type UserDeviceType
        {
            get { return _type; }
            set { _type = value; }
        }

        public UserDevice()
        {
        }

        public UserDevice(JObject userDeviceJson)
        {
            if (JSONHelper.isValidFieldForKey(userDeviceJson, "id"))
                this.id = (Int64)userDeviceJson["id"];

            if (JSONHelper.isValidFieldForKey(userDeviceJson, "userDeviceType"))
            {
                userDeviceType = (int)userDeviceJson["userDeviceType"];
                setType(userDeviceType);
            }
                
            if (JSONHelper.isValidFieldForKey(userDeviceJson, "push_id"))
                push_id = (String)userDeviceJson["push_id"];
        }

        public UserDevice(SqlDataReader reader)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());

            if (WSDatabaseHelper.isValidField(reader, "USER_DEVICE_ID"))
            {
                this.id = reader.GetInt64(reader.GetOrdinal("USER_DEVICE_ID"));
            }

            if (WSDatabaseHelper.isValidField(reader, "USER_DEVICE_PUSH_ID"))
            {
                push_id = reader.GetString(reader.GetOrdinal("USER_DEVICE_PUSH_ID"));
            }

            if (WSDatabaseHelper.isValidField(reader, "USER_DEVICE_TYPE"))
            {              
                userDeviceType = reader.GetInt32(reader.GetOrdinal("USER_DEVICE_TYPE"));
                setType((int)userDeviceType);
            }

            if (WSDatabaseHelper.isValidField(reader, "USER_DEVICE_CREATED_ON"))
            {
                createdOn = reader.GetDateTime(reader.GetOrdinal("USER_DEVICE_CREATED_ON")).ToString();
            }

            if (WSDatabaseHelper.isValidField(reader, "USER_DEVICE_UPDATED_ON"))
            {
                updatedOn = reader.GetDateTime(reader.GetOrdinal("USER_DEVICE_UPDATED_ON")).ToString();
            }
        }

        private void setType(int code)
        {
            switch (code)
            {
                case 1: this.UserDeviceType = User_Device_Type.ANDROID; break;
                case 2: this.UserDeviceType = User_Device_Type.IOS; break;
                case 3: this.UserDeviceType = User_Device_Type.BROWSER; break;
            }
        }

        public override void save()
        {
            throw new NotImplementedException();
        }

        public override Dictionary<string, object> toDictionary()
        {
            throw new NotImplementedException();
        }
         
        public override void update()
        {
            throw new NotImplementedException();
        }
    }
}