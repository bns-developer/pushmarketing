﻿using Newtonsoft.Json;
using System;
using System.Data.SqlClient;
using Newtonsoft.Json.Linq;
using WSDatabase;

namespace BNS.Models.Countries
{
    public class Country
    {
        
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Int64 id { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String name { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String flagPath { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String countryCode { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public char currency { get; set; }

        public Country(JObject countryJson)
        {
            if (JSONHelper.isValidFieldForKey(countryJson, "id"))
               id = (Int64)countryJson["id"];

            if (JSONHelper.isValidFieldForKey(countryJson, "name"))
               name = (String)countryJson["name"];

            if (JSONHelper.isValidFieldForKey(countryJson, "flag_path"))
                flagPath = (String)countryJson["flag_path"];

            if (JSONHelper.isValidFieldForKey(countryJson, "country_code"))
                countryCode = (String)countryJson["country_code"];


            if (JSONHelper.isValidFieldForKey(countryJson, "countryCode"))
                countryCode = (String)countryJson["countryCode"];

            if (JSONHelper.isValidFieldForKey(countryJson, "currency"))
                currency = (char)countryJson["currency"];
        }

        public Country(SqlDataReader reader)
        {
            if (WSDatabaseHelper.isValidField(reader, "COUNTRY_ID"))
            {              
                id = reader.GetInt64(reader.GetOrdinal("COUNTRY_ID"));               
            }
            if (WSDatabaseHelper.isValidField(reader, "COUNTRY_NAME"))
            {                             
                name = reader.GetString(reader.GetOrdinal("COUNTRY_NAME"));         
            }
            if (WSDatabaseHelper.isValidField(reader, "COUNTRY_CURRENCY"))
            {
                currency = Convert.ToChar(reader.GetValue(reader.GetOrdinal("COUNTRY_CURRENCY")));               
            }
            if (WSDatabaseHelper.isValidField(reader, "COUNTRY_FLAG_PATH"))
            {            
                flagPath = reader.GetString(reader.GetOrdinal("COUNTRY_FLAG_PATH"));
            }
            if (WSDatabaseHelper.isValidField(reader, "COUNTRY_CODE"))
            {            
                countryCode = reader.GetString(reader.GetOrdinal("COUNTRY_CODE"));            
            }

        }

        public Country()
        {
        }
    }
}