﻿using BNS.Models.Accounts;
using BNS.Models.BNSModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WSDatabase;

namespace BNS.Models.TermsAndConditions
{
    public class TermsAndConditions : BNSModel
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public new Int64 id { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Account account { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String title { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String description { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime created_at { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime updated_at { get; set; }

        public override void save()
        {
            throw new NotImplementedException();
        }

        public override void update()
        {
            throw new NotImplementedException();
        }

        public override Dictionary<string, object> toDictionary()
        {
            throw new NotImplementedException();
        }

        public TermsAndConditions(SqlDataReader reader)
        {
            if (WSDatabaseHelper.isValidField(reader, "TERMS_AND_CONDITIONS_ID"))
            {
                this.id = reader.GetInt64(reader.GetOrdinal("TERMS_AND_CONDITIONS_ID"));
            }
            if(WSDatabaseHelper.isValidField(reader, "TERMS_AND_CONDITIONS_ACCOUNT_ID"))
            {
                this.account = new Account();
                this.account.id = reader.GetInt64(reader.GetOrdinal("TERMS_AND_CONDITIONS_ACCOUNT_ID"));
            }
            if(WSDatabaseHelper.isValidField(reader, "TERMS_AND_CONDITIONS_TITLE"))
            {
                this.title = reader.GetString(reader.GetOrdinal("TERMS_AND_CONDITIONS_TITLE"));
            }
            if(WSDatabaseHelper.isValidField(reader, "TERMS_AND_CONDITIONS_DESCRIPTION"))
            {
                this.description = reader.GetString(reader.GetOrdinal("TERMS_AND_CONDITIONS_DESCRIPTION"));
            }
            if(WSDatabaseHelper.isValidField(reader, "TERMS_AND_CONDITIONS_CREATED_AT"))
            {
                this.created_at = reader.GetDateTime(reader.GetOrdinal("TERMS_AND_CONDITIONS_CREATED_AT"));
            }
            if(WSDatabaseHelper.isValidField(reader, "TERMS_AND_CONDITIONS_UPDATED_AT"))
            {
                this.updated_at = reader.GetDateTime(reader.GetOrdinal("TERMS_AND_CONDITIONS_UPDATED_AT"));
            }
        }

        public TermsAndConditions()
        {
        }

    }
}