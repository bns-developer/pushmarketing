﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.BNSModels;
using BNS.Models.Countries;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;

using System.Data.SqlClient;
using BNS.DL.WSDatabase;
using WSDatabase;

namespace BNS.Models.Contacts
{
    public class Contact : BNSModel
    {

        public enum Contact_Type
        {
            EMAIL = 1,
            PHONE = 2,
            WEBSITE = 3,
            FACEBOOK = 4,
            TWITTER = 5
        };
        private Contact_Type _type;

        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Contact_Type ContactType
        {
            get { return _type; }
            set { _type = value; }
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Country country { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String value { get; set; }

        Dictionary<string, object> contacts = new Dictionary<string, object>();

        public Contact(JObject userContactJson)
        {
            if (JSONHelper.isValidFieldForKey(userContactJson, "id"))
                this.id = (Int64)userContactJson["id"];

            if (JSONHelper.isValidFieldForKey(userContactJson, "value"))
            {
                value = (String)userContactJson["value"];
                if (value.Contains("@"))
                {
                    ContactType = Contact_Type.EMAIL;
                }
                else
                {
                    ContactType = Contact_Type.PHONE;
                }
            }

            if (JSONHelper.isValidFieldForKey(userContactJson, "country"))
            {
                JObject userContactCountryJObject = (JObject)userContactJson["country"];
                country = new Country(userContactCountryJObject);
            }
                
        }

        public Contact(String contactValue, Country usercountry)
        {
            country = usercountry;
            value = contactValue;
            if(value.Contains("@"))
            {
                ContactType = Contact_Type.EMAIL;
            }
            else
            {
                ContactType = Contact_Type.PHONE;
            }
        }

        public Contact(String contactValue)
        {
            value = contactValue;
            if (value.Contains("@"))
            {
                ContactType = Contact_Type.EMAIL;
            }
            else
            {
                ContactType = Contact_Type.PHONE;
            }
        }

        public Contact(SqlDataReader reader)
        {
            if (WSDatabaseHelper.isValidField(reader, "CONTACT_ID"))
            {
                this.id = reader.GetInt64(reader.GetOrdinal("CONTACT_ID"));
            }

            if (WSDatabaseHelper.isValidField(reader, "CONTACT_TYPE"))
            {
                Int64 i = reader.GetInt64(reader.GetOrdinal("CONTACT_TYPE"));
                this.setType((int)i);               
            }

            if (WSDatabaseHelper.isValidField(reader, "CONTACT_VALUE"))
            {
                value = reader.GetString(reader.GetOrdinal("CONTACT_VALUE"));
            }

            if (WSDatabaseHelper.isValidField(reader, "CONTACT_CREATED_ON"))
            {
                this.createdOn = reader.GetDateTime(reader.GetOrdinal("CONTACT_CREATED_ON")).ToString();               
            }

            if (WSDatabaseHelper.isValidField(reader, "CONTACT_UPDATED_ON"))
            {
                this.createdOn = reader.GetDateTime(reader.GetOrdinal("CONTACT_UPDATED_ON")).ToString();
            }

            if (WSDatabaseHelper.isValidField(reader, "COUNTRY_ID"))
            {
                if (ContactType == Contact_Type.PHONE)
                country = new Country(reader);
            }    
        }

        public Contact()
        {
        }

        private void setType(int code)
        {
            switch (code)
            {
                case 1: this.ContactType = Contact_Type.EMAIL; break;
                case 2: this.ContactType = Contact_Type.PHONE; break;
                case 3: this.ContactType = Contact_Type.WEBSITE; break;
                case 4: this.ContactType = Contact_Type.FACEBOOK; break;
                case 5: this.ContactType = Contact_Type.TWITTER; break;
            }
        }

        public override Dictionary<string, object> toDictionary()
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            if (this.id != 0)
                parameters["id"] = this.id;
            if (country != null)
                parameters["countryId"] = country.id;

            if (ContactType == Contact_Type.EMAIL)            
                parameters["type"] = 1;
            
            if (ContactType == Contact_Type.PHONE)            
                parameters["type"] = 2;

            if (ContactType == Contact_Type.WEBSITE)
                parameters["type"] = 3;

            if (ContactType == Contact_Type.FACEBOOK)
                parameters["type"] = 4;

            if (ContactType == Contact_Type.TWITTER)
                parameters["type"] = 5;

            parameters["value"] = value;            
            parameters["updatedOn"] = DateTime.UtcNow.ToString();
            parameters["createdOn"] = DateTime.UtcNow.ToString();
            return parameters;
        }

        public override void save()
        {
            throw new NotImplementedException();
        }

        public override void update()
        {
            throw new NotImplementedException();
        }
    }
}