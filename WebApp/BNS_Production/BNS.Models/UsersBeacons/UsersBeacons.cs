﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WSDatabase;

namespace BNS.Models.UsersBeacons
{
    public class UsersBeacons
    {

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Int64 user_id { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Int64 beacon_id { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Int64 offer_id { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime date { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int is_registered { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int count { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Boolean is_availed { get; set; }
        public UsersBeacons()
        {

        }

        public UsersBeacons(SqlDataReader reader)
        {
            if (WSDatabaseHelper.isValidField(reader, "USER_ID"))
            {
                this.user_id = reader.GetInt64(reader.GetOrdinal("USER_ID"));
            }

            if (WSDatabaseHelper.isValidField(reader, "BEACON_ID"))
            {
                this.beacon_id = reader.GetInt64(reader.GetOrdinal("BEACON_ID"));
            }

            if (WSDatabaseHelper.isValidField(reader, "OFFER_ID"))
            {
                this.offer_id = reader.GetInt64(reader.GetOrdinal("OFFER_ID"));
            }

            if (WSDatabaseHelper.isValidField(reader, "DATE"))
            {
                this.date = reader.GetDateTime(reader.GetOrdinal("DATE"));
            }

            if (WSDatabaseHelper.isValidField(reader, "IS_REGISTERED"))
            {
                this.is_registered = reader.GetInt32(reader.GetOrdinal("IS_REGISTERED"));
            }

            if (WSDatabaseHelper.isValidField(reader, "COUNT"))
            {
                this.count = (int)reader.GetValue(reader.GetOrdinal("COUNT"));               
            }

            if (WSDatabaseHelper.isValidField(reader, "IS_AVAILED"))
            {
                this.is_availed = reader.GetBoolean(reader.GetOrdinal("IS_AVAILED"));
            }

        }

    }
}