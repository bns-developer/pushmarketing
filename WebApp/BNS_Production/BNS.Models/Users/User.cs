﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.Accounts;
using BNS.Models.BNSModels;
using BNS.Models.Contacts;
using Newtonsoft.Json;
using BNS.Models.Beacons;
using BNS.Models.Kiosks;
using BNS.Models.Devices;
using BNS.Models.Offers;
using System.Data.SqlClient;
using BNS.DL.WSDatabase;
using WSDatabase;
using Microsoft.Owin;
using System.Security.Claims;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using BNS.Models.UserDevices;

namespace BNS.Models.Users
{
    public class User : BNSModel
    {

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Contact contact { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Account  account { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String memberId { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String username { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String password { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<Contact> contacts { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String uniqueId { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String socialService { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Beacon beacon { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Kiosk kiosk { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Device device { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Offer offer { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int type { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Offer latestOffer { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String lastAactivityDate { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int is_registered = 0;

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool is_availed { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String userDate { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public UserDevice userDevice { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<UserDevice> userDevices { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int userOfferStatus { get; set; }
       

        public enum User_Type
        {
            MEMBER = 1,
            NON_MEMBER = 2,           
        };
        private User_Type _type;

        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public User_Type UserType
        {
            get { return _type; }
            set { _type = value; }
        }

        public User(HttpContext current)
        {
            if (HttpContext.Current.Request.Form.AllKeys.Contains("email"))
            {
                String emailId = Convert.ToString(HttpContext.Current.Request.Form.GetValues("email").FirstOrDefault());
                contact = new Contact(emailId);
            }
            if (HttpContext.Current.Request.Form.AllKeys.Contains("username"))
            {
                username = Convert.ToString(HttpContext.Current.Request.Form.GetValues("username").FirstOrDefault());
            }
            if (HttpContext.Current.Request.Form.AllKeys.Contains("password"))
            {
                password = Convert.ToString(HttpContext.Current.Request.Form.GetValues("password").FirstOrDefault());
            }
        }

        public User(JObject userJson)
        {
            if (JSONHelper.isValidFieldForKey(userJson, "id"))
                this.id = (Int64)userJson["id"];

            if (JSONHelper.isValidFieldForKey(userJson, "contacts"))
            {
                contacts = new List<Contact>();
                JArray userContactJsonArray = (JArray)userJson.SelectToken("contacts");
                foreach (JObject item in userContactJsonArray)
                {
                    Contact contact = new Contact(item);
                    contacts.Add(contact);
                }
            }

            if (JSONHelper.isValidFieldForKey(userJson, "userDevice"))
            {
                JObject userDeviceJObject = (JObject)userJson["userDevice"];
                userDevice = new UserDevice(userDeviceJObject);
            }

            if (JSONHelper.isValidFieldForKey(userJson, "userDevices"))
            {
                userDevices = new List<UserDevice>();
                JArray userDeviceJsonArray = (JArray)userJson.SelectToken("userDevices");
                foreach(JObject item in userDeviceJsonArray)
                {
                    userDevice = new UserDevice(item);
                    userDevices.Add(userDevice);
                }              
            }

        }

        public User()
        {
            contact = new Contact();
            contacts = new List<Contact>();
            account = new Account();
            kiosk = new Kiosk();
            beacon = new Beacon();
            offer = new Offer();
        }

        private void setType(int code)
        {
            switch (code)
            {
                case 1: this.UserType = User_Type.MEMBER; break;
                case 2: this.UserType = User_Type.NON_MEMBER; break;               
            }
        }

        public User(SqlDataReader reader)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());

            if (WSDatabaseHelper.isValidField(reader, "USER_ID"))
            {
                this.id = reader.GetInt64(reader.GetOrdinal("USER_ID"));
            }

            if(WSDatabaseHelper.isValidField(reader, "TYPE"))
            {
                //var temp = reader.GetString(reader.GetOrdinal("TYPE"));
                //this.type = reader.GetInt32(reader.GetOrdinal("TYPE"));
                this.type = (int)reader.GetValue(reader.GetOrdinal("TYPE"));
                setType(type);
            }

            if(WSDatabaseHelper.isValidField(reader, "USER_NAME"))
            {
                this.username = reader.GetString(reader.GetOrdinal("USER_NAME"));
            }

            if (WSDatabaseHelper.isValidField(reader, "ACCOUNT_ID"))
            {
                this.account = new Account(reader);
            }

            if (WSDatabaseHelper.isValidField(reader, "USER_MEMBER_ID"))
            {
                memberId = reader.GetString(reader.GetOrdinal("USER_MEMBER_ID"));
            }

            if (WSDatabaseHelper.isValidField(reader, "USER_CREATED_ON"))
            {               
                DateTime date = reader.GetDateTime(reader.GetOrdinal("USER_CREATED_ON"));
                date = date.AddMinutes(-offSet);
                createdOn = date.ToString();
            }

            if (WSDatabaseHelper.isValidField(reader, "USER_UPDATED_ON"))
            {
                DateTime date = reader.GetDateTime(reader.GetOrdinal("USER_UPDATED_ON"));
                date = date.AddMinutes(-offSet);
                updatedOn = date.ToString();
            }

            if (WSDatabaseHelper.isValidField(reader, "CONTACT_ID"))
            {               
                contact = new Contact(reader);
            }

            if (WSDatabaseHelper.isValidField(reader, "DEVICE_ID"))
            {
                device = new Device(reader);
            }           

            if (WSDatabaseHelper.isValidField(reader, "OFFER_ID"))
            {
                offer = new Offer(reader);
            }

            if (WSDatabaseHelper.isValidField(reader, "USER_LATEST_OFFER"))
            {
                latestOffer = new Offer();
                latestOffer.id = reader.GetInt64(reader.GetOrdinal("USER_LATEST_OFFER"));
            }

            if (WSDatabaseHelper.isValidField(reader, "USER_LAST_ACTIVITY"))
            {               
                DateTime date = reader.GetDateTime(reader.GetOrdinal("USER_LAST_ACTIVITY"));
                date = date.AddMinutes(-offSet);
                lastAactivityDate = date.ToString();
            }

            if (WSDatabaseHelper.isValidField(reader, "IS_OFFER_AVAILED"))
            {
                is_availed = reader.GetBoolean(reader.GetOrdinal("IS_OFFER_AVAILED"));
            }

            if (WSDatabaseHelper.isValidField(reader, "USER_DATE"))
            {               
                DateTime date = reader.GetDateTime(reader.GetOrdinal("USER_DATE"));
                date = date.AddMinutes(-offSet);
                userDate = date.ToString();
            }

            if (WSDatabaseHelper.isValidField(reader, "USER_SOCIAL_SERVICE"))
            {
                socialService = reader.GetString(reader.GetOrdinal("USER_SOCIAL_SERVICE"));
            }

        }

        public override void save()
        {
            throw new NotImplementedException();
        }
        
        public User getContextUser(IOwinContext context)
        {
            ClaimsPrincipal user = context.Authentication.User;
            this.id = Convert.ToInt32(user.Claims.FirstOrDefault(claim => claim.Type == "userId").Value);
            this.account = new Account();
            this.account.id = Convert.ToInt32(user.Claims.FirstOrDefault(claim => claim.Type == "accountId").Value);

            //this.Email = user.Claims.FirstOrDefault(claim => claim.Type == "EmailId").Value;
            //this.FirstName = user.Claims.FirstOrDefault(claim => claim.Type == "FirstName").Value;
            //this.LastName = user.Claims.FirstOrDefault(claim => claim.Type == "LastName").Value;
            //this.IsAreaManager = bool.Parse(user.Claims.FirstOrDefault(claim => claim.Type == "IsAreaManager").Value);
            return this;
         }

        public override Dictionary<string, object> toDictionary()
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            if (this.id != 0)
                parameters["id"] = this.id;
            if (contact != null)
                parameters["contactId"] = contact.id;
            if (account != null)
                parameters["accountId"] = account.id;
            if (memberId != null)
                parameters["memberId"] = memberId;            
            if (UserType == User_Type.MEMBER)
            {
                type = 1;
            }
            else if(UserType == User_Type.NON_MEMBER)
            {
                type = 2;
            }
            if (type != 0)
                parameters["type"] = type;            
            parameters["createdOn"] = DateTime.UtcNow.ToString();
            parameters["updatedOn"] = DateTime.UtcNow.ToString();
           
            return parameters;
        }

        public override void update()
        {
            throw new NotImplementedException();
        }
    }
}