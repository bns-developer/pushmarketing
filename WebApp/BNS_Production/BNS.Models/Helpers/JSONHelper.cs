﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNS.Models.Helpers
{
    public class JSONHelper
    {
        public static Boolean isValidFieldForKey(JObject json, String key)
        {

            if (json.HasValues)
            {
                try
                {

                    if (json[key].ToString().Equals(""))
                    {
                        return false;

                    }
                    else
                    {
                        return true;
                    }
                }
                catch
                {
                    return false;
                }
            }
            return false;
        }
    }
}