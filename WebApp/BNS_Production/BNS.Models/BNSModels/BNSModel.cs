﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.Users;
using Newtonsoft.Json;
using System.Data.SqlClient;

namespace BNS.Models.BNSModels
{
    public abstract class BNSModel
    {

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Int64 id { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String name { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String number { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String createdOn { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String updatedOn { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public User createdBy { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public User updatedBy { get; set; }

        public String setDateTimeString(SqlDataReader reader, String key)
        {
            DateTime Date = reader.GetDateTime(reader.GetOrdinal(key));
            String datetime = Date.Month + "/" + Date.Day + "/" + Date.Year + " " + Date.Hour + ":" + Date.Minute + ":" + Date.Second;
            return datetime;
        }

        public abstract void save();
        public abstract void update();
        public abstract Dictionary<string, object> toDictionary();
    }
}