﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WSDatabase;

namespace BNS.Models.SocialNetworkingServices
{
    public class SocialNetworkingService
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Int64 id { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String name { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String imageUrl { get; set; }


        public SocialNetworkingService()
        {
        }

        public SocialNetworkingService(SqlDataReader reader)
        {
            if (WSDatabaseHelper.isValidField(reader, "SERVICE_ID"))
            {
                id = reader.GetInt64(reader.GetOrdinal("SERVICE_ID"));
            }
            if (WSDatabaseHelper.isValidField(reader, "SERVICE_NAME"))
            {
                name = reader.GetString(reader.GetOrdinal("SERVICE_NAME"));
            }          
            if (WSDatabaseHelper.isValidField(reader, "SERVICE_IMAGE_URL"))
            {
                imageUrl = reader.GetString(reader.GetOrdinal("SERVICE_IMAGE_URL"));
            }         
        }

        public SocialNetworkingService(JObject serviceJson)
        {
            if (JSONHelper.isValidFieldForKey(serviceJson, "id"))
                id = (Int64)serviceJson["id"];

            if (JSONHelper.isValidFieldForKey(serviceJson, "name"))
                name = (String)serviceJson["name"];

            if (JSONHelper.isValidFieldForKey(serviceJson, "image_url"))
                imageUrl = (String)serviceJson["image_url"];        
        }

    }
}