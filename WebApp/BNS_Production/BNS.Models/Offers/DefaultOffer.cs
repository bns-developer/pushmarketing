﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.BNSModels;
using BNS.Models.Accounts;
using BNS.Models.Offers;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Data.SqlClient;
using WSDatabase;

namespace BNS.Models.Offers
{
    public class DefaultOffer : BNSModel
    {
        public enum Offer_Type
        {
            MEMBER = 1,
            NON_MEMBER = 2,            
        };
        private Offer_Type _type1;

        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Offer_Type OfferType
        {
            get { return _type1; }
            set { _type1 = value; }
        }

        public enum Device_Type
        {
            BEACON = 1,
            KIOSK = 2,
        };
        private Device_Type _type2;

        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Device_Type DeviceType
        {
            get { return _type2; }
            set { _type2 = value; }
        }       

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Account account { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Offer offer { get; set; }

        private void setOfferType(int code)
        {
            switch (code)
            {
                case 1: this.OfferType = Offer_Type.MEMBER; break;
                case 2: this.OfferType = Offer_Type.NON_MEMBER; break;                
            }
        }

        private void setDeviceType(int code)
        {
            switch (code)
            {
                case 1: this.DeviceType = Device_Type.BEACON; break;
                case 2: this.DeviceType = Device_Type.KIOSK; break;
            }
        }
        public DefaultOffer(SqlDataReader reader)
        {
            if (WSDatabaseHelper.isValidField(reader, "DEFAULT_OFFER_ID"))
            {
                this.id = reader.GetInt64(reader.GetOrdinal("DEFAULT_OFFER_ID"));
            }         
           
            if (WSDatabaseHelper.isValidField(reader, "DEFAULT_OFFER_DEVICE_TYPE"))
            {
                int i = (int)reader.GetValue(reader.GetOrdinal("DEFAULT_OFFER_DEVICE_TYPE"));
                this.setDeviceType(i);
            }

            if (WSDatabaseHelper.isValidField(reader, "DEFAULT_OFFER_OFFER_TYPE"))
            {
                int i = (int)reader.GetValue(reader.GetOrdinal("DEFAULT_OFFER_OFFER_TYPE"));
                this.setOfferType(i);
            }

            if (WSDatabaseHelper.isValidField(reader, "DEFAULT_OFFER_OFFER_ID"))
            {
                offer = new Offer(reader);
            }

            if (WSDatabaseHelper.isValidField(reader, "DEFAULT_OFFER_ACCOUNT_ID"))
            {
                account = new Account();
                account.id = reader.GetInt64(reader.GetOrdinal("DEFAULT_OFFER_ACCOUNT_ID"));
            }

        }

        public override void save()
        {
            throw new NotImplementedException();
        }

        public override Dictionary<string, object> toDictionary()
        {
            throw new NotImplementedException();
        }

        public override void update()
        {
            throw new NotImplementedException();
        }
    }
}