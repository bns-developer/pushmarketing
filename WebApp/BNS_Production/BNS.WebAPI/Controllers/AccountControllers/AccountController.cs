﻿using System;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Script.Serialization;
using BNS.Wrapper;
using BNS.WebAPI.Controllers.BNSControllers;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Web;
using System.Linq;
using BNS.DAL.DALAccount;
using System.Net;
using BNS.Models.Accounts;
using BNS.Models.Users;
using BNS.DAL.DALUser;
using BNS.DAL.DALContact;



namespace BNS.API.Controllers.AccountControllers
{ 
    [RoutePrefix("api/v1/accounts")]
    public class AccountController : BNSController
    {
        DALAccountManager accountManager = new DALAccountManager();
        DALUserManager userManager = new DALUserManager();
        DALContactManager contactManager = new DALContactManager();
        ResponseWrapper responseWrapper = new ResponseWrapper();
        
             
        [Route("add")]
        [HttpPost]
       
        //Add new account
        public HttpResponseMessage addAaccount()
        {
            String userName = Convert.ToString(HttpContext.Current.Request.Form.GetValues("userName").FirstOrDefault());
            String email = Convert.ToString(HttpContext.Current.Request.Form.GetValues("email").FirstOrDefault());

            if (userManager.checkIfUserNameExists(userName))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Username Already Exist");
            }
            else if(contactManager.checkIfAccountEmailIdExixts(email))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Email Id Already Exist");
            }
            else
            {
                Account newAccount = new Account(HttpContext.Current);
                newAccount.id = accountManager.addAccount(newAccount);
                if (newAccount.id != 0)
                {
                    JObject accountId = new JObject();
                    accountId["id"] = newAccount.id;
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, accountId);
                }
                else
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Sign Up Error");
                }
            }
        }

        [Route("details")]
        [HttpGet]
        [Authorize]
        //Account details
        public HttpResponseMessage GetDetails()
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            Account account = new Account();
            account.id = requestUser.account.id;
            account = accountManager.getDetails(account);
            var response = Request.CreateResponse(HttpStatusCode.OK, account);
            return response;
        }

        [Route("info")]
        [HttpGet]
        public override HttpResponseMessage GetInfo([FromUri] Int64 account_id)
        {
            throw new NotImplementedException();
        }
        
        [Route("update")]
        [HttpPost]
        [Authorize]
        //Update an account
        public HttpResponseMessage update()
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            Account updateAccount = new Account(HttpContext.Current);
            updateAccount.id = requestUser.account.id;         
            if (accountManager.update(updateAccount))
                return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Account Updated");
            else
                return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Aaccount Not Updated");
        }

        [Route("forgotPassword")]
        [HttpPost]
        //Forgot password 
        public HttpResponseMessage forgotPassword([FromBody] JObject requestJSON)
        {
            String emailId = (String)requestJSON["email"];
            if (contactManager.verifyEmailId(emailId))
            {
                User user = new User();
                user.contact.value = emailId;
                if(accountManager.sendForgotPasswordEmail(user))
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Password is send to the registered Email Id.");
                }
                else
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Password not found");
                }
            }
            else
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Please enter the registered Email Id.");
            }               
        }

        [Route("resetPassword")]
        [HttpPost]
        [Authorize]
        //Reset password 
        public HttpResponseMessage resetPassword([FromBody] JObject requestJSON)
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            String oldPassword = (String)requestJSON["oldPassword"];
            String newPassword = (String)requestJSON["newPassword"];
            requestUser.password = oldPassword;
            if (accountManager.verifyOldPassword(requestUser))
            {
                if(accountManager.resetPassword(requestUser, newPassword))
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Password reset successfuly.");
                }
                else
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Password could not reset.");
                }
            }
            else
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Please enter the valid password.");
            }
        }

        public override HttpResponseMessage save([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }

        public override HttpResponseMessage GetAll()
        {
            throw new NotImplementedException();
        }    

        public override HttpResponseMessage update([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }

        public override HttpResponseMessage delete([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }

        public override HttpResponseMessage deleteMultiple([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }
    }
}
