﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BNS.WebAPI.Controllers.BNSControllers;
using Newtonsoft.Json.Linq;
using BNS.Models.Accounts;
using BNS.Models.Beacons;
using BNS.Wrapper;
using BNS.BL.BLBeacon;
using BNS.DAL.DALBeacon;
using BNS.Models.Users;
using BNS.Models.Stores;
using System.Web;
using BNS.DAL.CommunicationManager.Passbook;
using PassKitAPIWrapper;

namespace BNS.WebAPI.Controllers.BeaconControllers
{
    [RoutePrefix("api/v1/beacons")]
    public class BeaconController : BNSController
    {
        BeaconBL BLmanager = new BeaconBL();
        DALBeaconManager DALmanager = new DALBeaconManager();
        ResponseWrapper responseWrapper = new ResponseWrapper();
       
        [Route("add")]
        [Authorize]
        [HttpPost]
        //Add new beacon
        public override HttpResponseMessage save([FromBody] JObject requestJSON)
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());

            JObject storeJObject = (JObject)requestJSON["store"];
            Store store = new Store(storeJObject);
            Int64 storeId = store.id;
            String beaconName = (String)requestJSON["name"];
            String beaconNumber = (String)requestJSON["number"];
            Beacon newBeacon = new Beacon(requestJSON);
            
            if (!DALmanager.checkDefaultOffer(requestUser.account.id))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Forbidden, "Please set the default offers first to proceed");
            }
            else if (DALmanager.checkIfNameExists(beaconName, storeId))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Beacon name already exist for this store.");

            }

            else if (DALmanager.checkIfNumberExists(beaconNumber, storeId))
            {

                return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Beacon number already exist for this store.");
            }           
            else
            {
                newBeacon.createdBy = requestUser;
                newBeacon.updatedBy = requestUser;

                if (BLmanager.save(newBeacon))
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Beacon  Saved");
                }
                else
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Beacon Not Saved");
                }
            }
        }
     
        [Route("all")]
        [HttpGet]
        //All beacons by accountId
        public override HttpResponseMessage GetAll()
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            List<Beacon> beacons = DALmanager.getAll(requestUser.account);
            var response = Request.CreateResponse(HttpStatusCode.OK, beacons);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, beacons);
        }
  
        [Route("details")]
        [HttpGet]
        [Authorize]
        //Beacon details
        public override HttpResponseMessage GetInfo([FromUri]Int64 beaconId)
        {
            Beacon beacon = new Beacon();
            beacon.id = beaconId;
            beacon = DALmanager.getDetails(beacon);
            var response = Request.CreateResponse(HttpStatusCode.OK, beacon);
            return response;
        }

        [Route("update")]
        [HttpPost]
        [Authorize]
        public override HttpResponseMessage update([FromBody] JObject requestJSON)
        {
            Int64 beaconId = (Int64)requestJSON["id"];
            if(DALmanager.isWokable(beaconId))
            {
                User requestUser = new User();
                requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
                Beacon updateBeacon = new Beacon(requestJSON);
                updateBeacon.createdBy = requestUser;
                updateBeacon.updatedBy = requestUser;

                if (DALmanager.update(updateBeacon))
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Beacon Updated");
                else
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Beacon Not Updated");
            }
            else
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "This beacon is not workable, set it as workable to update.");
            }
            
        }

        [Route("delete")]
        [HttpPost]
        [Authorize]
        public override HttpResponseMessage delete([FromBody] JObject requestJSON)
        {
            Int64 beaconId = (Int64)requestJSON["id"];
            if(DALmanager.isWokable(beaconId))
            {
                if (DALmanager.isBeaconUsedForRegistration(beaconId))
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Unable to delete the beacon as there is a data associated with this beacon.");
                }
                else
                {
                    if (DALmanager.delete(beaconId))
                        return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Beacon Deleted Successfully.");
                    else
                        return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Beacon Not Deleted.");
                }
            }
            else
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "This beacon is not workable, set it as workable to delete.");
            }
            
        }

        [Route("changeBeaconWorkableState")]
        [HttpPost]
        [Authorize]        
        public HttpResponseMessage changeBeaconWorkableState([FromUri] Int64 beaconId)
        {
            if (DALmanager.changeBeaconWorkableState(beaconId))
                return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Beacon state updated successfuly.");
            else
                return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Beacon state not updated");
        }

        [Route("getInfo")]
        [HttpGet]
        public HttpResponseMessage getInfo()
        {
            passbookService ps = new passbookService();
            PassKitResponse response = ps.getInfo();
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, response);
        }

        public override HttpResponseMessage deleteMultiple([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }     
    }
}
