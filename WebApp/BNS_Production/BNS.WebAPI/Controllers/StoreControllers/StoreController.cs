﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using BNS.Wrapper;
using System.Web.Script.Serialization;
using BNS.WebAPI.Controllers.BNSControllers;
using Newtonsoft.Json.Linq;
using BNS.Models.Stores;
using BNS.Models.BNSModels;
using BNS.Models.Accounts;
using BNS.Models.Users;
using BNS.BL.BLStore;
using BNS.DAL.DALStore;
using System.Web;

namespace BNS.API.Controllers.StoreControllers
{
    [RoutePrefix("api/v1/Stores")]
    public class StoreController : BNSController
    {
        StoreBL BLmanager = new StoreBL();
        DALStoreManager DALmanager = new DALStoreManager();
        ResponseWrapper responseWrapper = new ResponseWrapper();

        [Route("save")]
        [HttpPost]
        [Authorize]
        //Save store
        public override HttpResponseMessage save([FromBody] JObject requestJSON)
        {

            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());           
                        
            String storeName = (String)requestJSON["name"];
            String storeNumber = (String)requestJSON["number"];           
            Store newStore = new Store(requestJSON);

            if (DALmanager.checkIfNameExists(storeName,requestUser.account.id))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Store Name Already Exist.");

            }

            else if (DALmanager.checkIfNumberExists(storeNumber, requestUser.account.id))
            {

                return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Store Number Already Exist");
            }
            else
            {
                newStore.createdBy = requestUser;
                newStore.updatedBy = requestUser;
                newStore.account = requestUser.account;
                if (BLmanager.save(newStore))
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Store  Saved");
                }
                else
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Store Not Saved");
                }
            }
                              
        }

        [Route("all")]
        [HttpGet]
        [Authorize]
        //All Stores by accountId
        public override HttpResponseMessage GetAll()
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            List<Store> stores = DALmanager.getAll(requestUser.account);
            var response = Request.CreateResponse(HttpStatusCode.OK, stores);
           
           return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, stores);
        }

        [Route("update")]
        [HttpPost]
        [Authorize]
        public override HttpResponseMessage update([FromBody] JObject requestJSON)
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            Store updateStore = new Store(requestJSON);
            updateStore.createdBy = requestUser;
            updateStore.updatedBy = requestUser;
            updateStore.account = requestUser.account;
            if (DALmanager.update(updateStore))
                return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Store Updated");
            else
                return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Store Not Updated");
        }

        [Route("delete")]
        [HttpPost]
        [Authorize]
        public override HttpResponseMessage delete([FromBody] JObject requestJSON)
        {
            Int64 storeId = (Int64)requestJSON["id"];
            if (DALmanager.isStoreUsedForBeacon(storeId) || DALmanager.isStoreUsedForKiosk(storeId))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Unable to delete the store as devices are added to this store.");
            }
            else
            {
                if (DALmanager.delete(storeId))
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Store Deleted Successfully.");
                else
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Store Not Deleted.");
            }
        }

        [Route("deleteMultiple")]
        [HttpGet]
        public override HttpResponseMessage deleteMultiple([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }

        [Route("details")]
        [HttpGet]
        public override HttpResponseMessage GetInfo([FromUri] Int64 storeId)
        {
            Store store = new Store();
            store.id = storeId;
            store = DALmanager.getDetails(store);
            var response = Request.CreateResponse(HttpStatusCode.OK, store);
            return response;
        }       

        [Route("GetStoresForDropDown")]
        [HttpGet]
        [Authorize]
        //All Stores by accountId
        public HttpResponseMessage getStoresForDropDown()
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
                       
            List<Store> stores = DALmanager.getStoresForDropDown(requestUser.account);
            var response = Request.CreateResponse(HttpStatusCode.OK, stores);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, stores);
        }

    }
}
