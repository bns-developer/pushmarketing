﻿using BNS.Wrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BNS.Models.SocialNetworkingServices;
using BNS.DAL.DALSocialNetworkingService;
using BNS.Models.Users;
using System.Web;
using Newtonsoft.Json.Linq;

namespace BNS.WebAPI.Controllers.SocialNetworkingServiceControllers
{
    [RoutePrefix("api/v1/socialNetworkingServices")]
    public class SocialNetworkingServiceController : ApiController
    {
        ResponseWrapper responseWrapper = new ResponseWrapper();
        DALSocialNetworkingServiceManager manager = new DALSocialNetworkingServiceManager();

        [Route("add")]
        [HttpPost]
        [Authorize]
        //Save Social Networking Service for account
        public HttpResponseMessage addSelectedService([FromBody] JObject requestJSON)
        {

            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());

            Int64 serviceId = (Int64)requestJSON["serviceId"];            

            if (manager.checkIfServiceExists(serviceId, requestUser.account.id))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "This service is already exist.");

            }
            else
            {               
                if (manager.addSelectedService(serviceId, requestUser.account.id) != 0)
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Service added successfuly.");
                }
                else
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Unable to add the service.");
                }
            }

        }

        [Route("getAll")]
        [HttpGet]
        [Authorize]
        //All Social Networking Services
        public HttpResponseMessage getAllServices()
        {
            List<SocialNetworkingService> SocialNetworkingServices = manager.getAllServices();
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, SocialNetworkingServices);
        }

        [Route("getSelectedServices")]
        [HttpGet]
        [Authorize]
        //Selected Social Networking Services
        public HttpResponseMessage getSelectedServices()
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            List<SocialNetworkingService> SocialNetworkingServices = manager.getSelectedServices(requestUser.account.id);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, SocialNetworkingServices);
        }

        [Route("getUnselectedServices")]
        [HttpGet]
        [Authorize]
        //Selected Social Networking Services
        public HttpResponseMessage getUnselectedServices()
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            List<SocialNetworkingService> SocialNetworkingServices = manager.getUnselectedServices(requestUser.account.id);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, SocialNetworkingServices);
        }

        [Route("getSelectedServicesForMobile")]
        [HttpGet]
        //Selected Social Networking Services for mobile
        public HttpResponseMessage getSelectedServicesForMobile([FromUri] Int64 accountId)
        {           
            List<SocialNetworkingService> SocialNetworkingServices = manager.getSelectedServices(accountId);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, SocialNetworkingServices);
        }




    }
}
