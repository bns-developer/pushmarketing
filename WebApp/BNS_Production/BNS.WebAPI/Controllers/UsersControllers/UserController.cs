﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BNS.WebAPI.Controllers.BNSControllers;
using Newtonsoft.Json.Linq;
using BNS.Wrapper;
using BNS.Models.Users;
using BNS.Models.Contacts;
using BNS.Models.Accounts;
using BNS.DAL.DALUser;
using BNS.BL.BLContact;
using BNS.DAL.DALContact;
using BNS.BL.BLUser;
using BNS.Models.Countries;
using BNS.Models.Beacons;
using BNS.Models.Kiosks;
using BNS.Models.Offers;
using System.Web;
using BNS.DAL.DALKiosk;
using BNS.DAL.DALOffer;
using BNS.DAL.DALBeacon;
using Newtonsoft.Json;
using System.IO;
using BNS.Models.Notifications;
using BNS.DAL.DALNotification;
using WSDatabase;

namespace BNS.WebAPI.Controllers.UsersControllers
{
    [RoutePrefix("api/v1/users")]
    public class UserController : BNSController
    {
        UserBL userBLmanager = new UserBL();
        DALUserManager userDALmanager = new DALUserManager();
        ContactBL contactBLmanager = new ContactBL();
        DALContactManager contactDALmanager = new DALContactManager();
        DALBeaconManager beaconManager = new DALBeaconManager();
        DALNotificationManager notificationManager = new DALNotificationManager();

        ResponseWrapper responseWrapper = new ResponseWrapper();

        
        [Route("registrationFromBeacon")]
        [HttpPost]      
        //User Registration From Beacon
        public  HttpResponseMessage saveUser([FromBody] JObject requestJSON)
        {            
            String emailId = String.Empty;
            String Phone = String.Empty;        
            if (JSONHelper.isValidFieldForKey(requestJSON, "email_id"))
                emailId = (String)requestJSON["email_id"];
            if (JSONHelper.isValidFieldForKey(requestJSON, "phone"))
                Phone = (String)requestJSON["phone"];
            int registred_from = (int)requestJSON["registered_from"];
            Int64 accountId = (Int64)requestJSON["accountId"];
            Int64 user_id = (Int64)requestJSON["userId"];
            String memberId = (String)requestJSON["memberId"];          
            Account account = new Account();
            account.id = (Int64)requestJSON["accountId"];
            String uniqueId = String.Empty;
            if (JSONHelper.isValidFieldForKey(requestJSON, "unique_id"))            
                uniqueId = (String)requestJSON["unique_id"];           
                
            if (emailId != String.Empty && contactDALmanager.checkIfEmailIdExists(emailId, accountId))
            {
                if(uniqueId != String.Empty)
                {
                    User user = new User();
                    user = userDALmanager.getUserDetailsByUniqueId(uniqueId, accountId);
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Your Email Id is already registered with us using " + user.socialService + " account. Please try with some different social account.");
                }
                else
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Your Email Id is already registered with us. Please try with some different Email Id.");
                }               
            }
            else if(uniqueId != String.Empty && userDALmanager.checkIfUniqueIdExixts(uniqueId, accountId))
            {
                User user = new User();
                user = userDALmanager.getUserDetailsByUniqueId(uniqueId, accountId);
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "You are already registered with us using " + user.socialService + " account.");
            }
            else
            {
                Contact newUserContact1 = new Contact();
                Contact newUserContact2 = new Contact();
                Offer offer = new Offer();
                User newUser = new User();                
                JObject countryJObject = (JObject)requestJSON["country"];
                Country newUserCountry = new Country(countryJObject);

                if(newUserCountry.id != 0)
                {
                    if (emailId != "")
                    {
                        newUserContact1 = new Contact(emailId, newUserCountry);
                        newUser.contacts.Add(newUserContact1);
                    }
                    if (Phone != "")
                    {
                        newUserContact2 = new Contact(Phone, newUserCountry);
                        newUser.contacts.Add(newUserContact2);
                    }                   
                }                          
                else
                { 
                    if(emailId != "")
                    {
                        newUserContact1 = new Contact(emailId);
                        newUser.contacts.Add(newUserContact1);
                    }
                    if(Phone != "")
                    {
                        newUserContact2 = new Contact(Phone);
                        newUser.contacts.Add(newUserContact2);
                    }
                }


                JObject offerJObject = (JObject)requestJSON["offer"];
                offer = new Offer(offerJObject);                          
                newUser.id = user_id;
                newUser.is_registered = 1;
                newUser.account = account;
                newUser.offer = offer;                           
                newUser.memberId = memberId;

                if (JSONHelper.isValidFieldForKey(requestJSON, "unique_id"))
                    newUser.uniqueId = (String)requestJSON["unique_id"];
                if (JSONHelper.isValidFieldForKey(requestJSON, "social_service"))
                    newUser.socialService = (String)requestJSON["social_service"];

                switch (registred_from)
                {
                    case 1:
                        JObject beaconJObject = (JObject)requestJSON["beacon"];
                        Beacon beacon = new Beacon(beaconJObject);
                        newUser.beacon = beacon;
                        break;

                    case 2:
                        JObject kioskJObject = (JObject)requestJSON["kiosk"];
                        Kiosk kiosk = new Kiosk(kioskJObject);
                        newUser.kiosk = kiosk;
                        break;
                }

                User user = userBLmanager.saveUserFromBeacon(newUser);
                if (user.id != 0)
                {                   
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, user);
                }
                else
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "User Not Saved");
                }
            }

        }

   
        [Route("notifications")]
        [HttpPost]
        //Send notification to users
        public HttpResponseMessage userNotifications([FromBody] JObject requestJSON)
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());

            Notification notification = new Notification(requestJSON);
            notification.sender = requestUser.account;
            if(notificationManager.sendNotifications(notification))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Notifications sent.");
            }
            else
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Unable To Sent Notifications.");
            }       
        }


        [Route("registration")]
        [HttpPost]
        //User Registration From kiosk
        public override HttpResponseMessage save([FromBody] JObject requestJSON)
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            String emailId = (String)requestJSON["email_id"];
            String Phone = (String)requestJSON["phone"];
            int registred_from = (int)requestJSON["registered_from"];
            JObject kioskJObject = (JObject)requestJSON["kiosk"];
            Kiosk kiosk = new Kiosk(kioskJObject);
            if(new DALKioskManager().isWokable(kiosk.id))
            {
                if (contactDALmanager.checkIfEmailIdExists(emailId, requestUser.account.id))
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Your Email Id is already registered with us. Click on Enter Member ID button to enter your member id.");
                }
                else
                {

                    JObject countryJObject = (JObject)requestJSON["country"];
                    Country newUserCountry = new Country(countryJObject);

                    JObject offerJObject = (JObject)requestJSON["offer"];
                    Offer offer = new Offer(offerJObject);

                    Contact newUserContact1 = new Contact(emailId, newUserCountry);
                    Contact newUserContact2 = new Contact(Phone, newUserCountry);

                    User newUser = new User();
                    newUser.is_registered = 1;
                    newUser.type = 1;
                    newUser.account = requestUser.account;
                    newUser.offer = offer;
                    newUser.contacts.Add(newUserContact1);
                    newUser.contacts.Add(newUserContact2);
                    newUser.kiosk = kiosk;

                    User user = userBLmanager.saveUser(newUser);
                    if (user.id != 0)
                    {                       
                        return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, user);
                    }
                    else
                    {
                        return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "User Not Saved");
                    }
                }
            }
            else
            {                
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Forbidden, "Please  contact to admin.");
            }          

        }

        [Route("verifyMemberId")]
        [HttpPost]
        //Verify MemberId From Kiosk
        public HttpResponseMessage verifyMemberId([FromBody] JObject requestJSON)
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            String memberId = (String)requestJSON["member_id"];
            if (!userDALmanager.verifyMemberId(memberId, requestUser.account.id))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Invalid Member Id! Please try again.");
            }
            else
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Member Id verified.");
            }
        }

        [Route("saveMemberOffer")]
        [HttpPost]
        [Authorize]
        //Member Offer From Kiosk
        public  HttpResponseMessage saveMemberOffer([FromBody] JObject requestJSON)
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());

            String memberId = (String)requestJSON["member_id"];
            int registred_from = (int)requestJSON["registered_from"];
            JObject kioskJObject = (JObject)requestJSON["kiosk"];
            Kiosk kiosk = new Kiosk(kioskJObject);
            JObject offerJObject = (JObject)requestJSON["offer"];
            Offer offer = new Offer(offerJObject);

            if (new DALKioskManager().isWokable(kiosk.id))
            {
                if (!userDALmanager.verifyMemberId(memberId, requestUser.account.id))
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Invalid Member Id! Please try again.");
                }
                else
                {                    
                    User member = new User();
                    member.memberId = memberId;
                    member.account = requestUser.account;
                    member.offer = offer;
                    member.kiosk = kiosk;
                    member = new DALUserManager().getUserDetails(member);
                    if(!userDALmanager.isAvailed(member))
                    {
                        User user = userDALmanager.saveMemberOffer(member);
                        if (user.id != 0)
                        {
                            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, user);
                        }
                        else
                        {
                            return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Error in assigning the offer to member");
                        }
                    }
                    else
                    {                       
                        return responseWrapper.CreateResponse(Request, HttpStatusCode.Forbidden, "You have already availed this offer. Please try another offer.");
                    }                   
                }
            }
            else
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Forbidden, "Please  contact to admin.");
            }         
        }

        [Route("all")]
        [HttpGet]
        [Authorize]
        //All registered users 
        public override HttpResponseMessage GetAll()
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            List<User> users = userDALmanager.getAll(requestUser.account);
            var response = Request.CreateResponse(HttpStatusCode.OK, users);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, users);
        }

        [Route("getAnnonymous")]
        [HttpGet]
        [Authorize]
        //All annonymous users 
        public HttpResponseMessage getAnnonymous()
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            List<User> users = userDALmanager.getAnnonymous(requestUser.account);
            var response = Request.CreateResponse(HttpStatusCode.OK, users);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, users);
        }

        [Route("getAllUsersToSendNotification")]
        [HttpGet]
        [Authorize]
        //All annonymous users 
        public HttpResponseMessage getAllUsersToSendNotification()
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            List<User> users = userDALmanager.getAllUsersToSendNotification(requestUser.account);
            var response = Request.CreateResponse(HttpStatusCode.OK, users);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, users);
        }

        [Route("getUserActivities")]
        [HttpGet]
        [Authorize]
        //Registered user's activities 
        public HttpResponseMessage getUserActivities([FromUri] Int64 userId)
        {

            List<User> users = userDALmanager.getUserActivities(userId);
            var response = Request.CreateResponse(HttpStatusCode.OK, users);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, users);
        }

        [Route("getAnnonymousActivities")]
        [HttpGet]
        [Authorize]
        //Annonymous user's activities 
        public HttpResponseMessage getAnnonymousActivities([FromUri] Int64 userId)
        {

            List<User> users = userDALmanager.getAnnonymousActivities(userId);
            var response = Request.CreateResponse(HttpStatusCode.OK, users);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, users);
        }

        [Route("getUserDetailsById")]
        [HttpGet]
        [Authorize]
        //Registered user details
        public override HttpResponseMessage GetInfo([FromUri] Int64 userId)
        {
            User user = userDALmanager.getUserDetailsById(userId);
            var response = Request.CreateResponse(HttpStatusCode.OK, user);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, user);

        }

        [Route("getMemberId")]
        [HttpPost]    
        //Retrieve member id from mobile
        public HttpResponseMessage getMemberId([FromBody] JObject requestJSON)
        {
            User user = new User();
            user.account.id = (Int64)requestJSON["accountId"];
            user.contact = new Contact();
            user.contact.value = (String)requestJSON["email"];
            user.contact.ContactType = Contact.Contact_Type.EMAIL;

            if (contactDALmanager.checkIfEmailIdExists(user.contact.value, user.account.id))
            {
                user = userDALmanager.getMemberId(user);               
                if (user.memberId != null)
                {
                    userDALmanager.sendMemberIdMail(user);
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Member Id is sent to you, please check the mailbox.");
                }
                else
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Member Id Not Available.");
                }                
            }
            else
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Your Email Id is not registered with us. Please try with some different Email Id.");
            }
        }

        [Route("getUserMemberId")]
        [HttpPost]
        [Authorize]
        //Retrieve member id from kiosk 
        public HttpResponseMessage getUserMemberId([FromBody] JObject requestJSON)
        {
            User requestUser = new User();
            User user = new User();

            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            user.account = requestUser.account;
            user.contact.value = (String)requestJSON["email"];
            user.contact.ContactType = Contact.Contact_Type.EMAIL;

            if (contactDALmanager.checkIfEmailIdExists(user.contact.value, user.account.id))
            {
                user = userDALmanager.getMemberId(user);               
                if (user.memberId != null)
                {
                    userDALmanager.sendMemberIdMail(user);
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Member Id is sent to you, please check the mailbox.");
                }
                else
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Member Id Not Available.");
                }
            }
            else
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Your Email Id is not registered with us. Please try with some different Email Id.");
            }
        }

        [Route("getUserOfferCodeDetails")]
        [HttpPost]
        [Authorize]
        //Retrieve user_offer details from offer code
        public HttpResponseMessage getUserOfferCodeDetails([FromBody] JObject requestJSON)
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            Offer offer = new Offer();
            offer.account = requestUser.account;
            String code = (String)requestJSON["code"];
            offer.code = code;

            if(new DALOfferManager().checkOfferCode(offer))
            {
                if(new DALOfferManager().checkOfferCodeForAccount(offer))
                {
                    Dictionary<string, object> responseObject = new Dictionary<string, object>();
                    User user = new User();
                    User registration = new User();
                    user.offer = offer;
                    user = userDALmanager.getUserOfferDetails(user);
                    registration = userDALmanager.getUserDetailsById(user.id);
                    responseObject.Add("userOfferDetails", user);
                    responseObject.Add("userRegistrationDetails", registration);
                    if (user.id != 0)
                    {
                        return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, responseObject);
                    }
                    else
                    {
                        return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Error!");
                    }

                }
                else
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Offer code not valid for the this account");
                }
            }
            else
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Invalid Offer Code.");
            }

        }

        [Route("setUserOfferCodeStatus")]
        [HttpPost]
        [Authorize]
        //Change Offer Code Status
        public HttpResponseMessage setUserOfferCodeStatus([FromBody] JObject requestJSON)
        {
            User user = new User();
            user.id = (Int64)requestJSON["user_id"];
            user.offer.id = (Int64)requestJSON["offer_id"];
            user.offer.code = (String)requestJSON["code"];
            int status = (int)requestJSON["status"];

            if(userDALmanager.setUserOfferCodeStatus(user, status))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, user);
            }        
            else
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Error!");
            }                
        }

        [Route("getAllUsersOfferCode")]
        [HttpGet]
        [Authorize]
        //Retrieve user_offer details from account id
        public HttpResponseMessage getAllUsersOfferCode()
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            List<User> usersList = new List<User>();
            usersList = userDALmanager.getUsersOfferCodeByStatus(requestUser.account, 1);
            var response = Request.CreateResponse(HttpStatusCode.OK, usersList);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, usersList);
        }

        [Route("getFlagedUserOfferCode")]
        [HttpGet]
        [Authorize]
        //Retrieve user_offer details from account id
        public HttpResponseMessage getFlagedUserOfferCode()
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            List<User> usersList = new List<User>();
            usersList = userDALmanager.getUsersOfferCodeByStatus(requestUser.account, 2);
            var response = Request.CreateResponse(HttpStatusCode.OK, usersList);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, usersList);
        }

        [Route("saveUserPushId")]
        [HttpPost] 
        //Save user device's Push Id
        public HttpResponseMessage saveUserPushId([FromBody] JObject requestJSON)
        {
            User user = new User(requestJSON);
            user.userDevice.id = userDALmanager.saveUserPushId(user);
            if (user.userDevice.id != 0)
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, user.userDevice.id);
            }
            else
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Unable to save the response, please try again.");
            }            
        }

        public override HttpResponseMessage delete([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }

        public override HttpResponseMessage deleteMultiple([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }  

        public override HttpResponseMessage update([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }

        //[Route("member/acceptOffer")]
        //[HttpPost]
        ////Member Offer From Beacon
        //public HttpResponseMessage acceptOffer([FromBody] JObject userDetails)
        //{
        //    var response = Request.CreateResponse(HttpStatusCode.OK);

        //    String memberId = (String)userDetails["memberId"];
        //    Int64 accountId = (Int64)userDetails["accountId"];
        //    Int64 beaconId = (Int64)userDetails["beaconId"];
        //    Int64 offerId = (Int64)userDetails["offerId"];

        //    String Phone = String.Empty;
        //    String emailId = String.Empty;
        //    String uniqueId = String.Empty;
        //    if (JSONHelper.isValidFieldForKey(userDetails, "email_id"))
        //        emailId = (String)userDetails["email_id"];
        //    if (JSONHelper.isValidFieldForKey(userDetails, "unique_id"))
        //        uniqueId = (String)userDetails["unique_id"];
        //    if (JSONHelper.isValidFieldForKey(userDetails, "phone"))
        //        Phone = (String)userDetails["phone"];

        //    if (!userDALmanager.verifyMemberId(memberId, accountId))
        //    {
        //        response = Request.CreateResponse(HttpStatusCode.Conflict, "Invalid Member Id! Please try again.");
        //    }
        //    //else if(uniqueId != String.Empty && !userDALmanager.verifyUserUniqueId(memberId, uniqueId, accountId))
        //    //{
        //    //    response = Request.CreateResponse(HttpStatusCode.Conflict, "Invalid Member Id! Please try again.");
        //    //}
        //    else
        //    {
        //        User user = new User();
        //        user.memberId = memberId;
        //        user.account.id = accountId;
        //        user = new DALUserManager().getUserDetails(user);
        //        user.beacon.id = beaconId;
        //        user.offer.id = offerId;
        //        //user.memberId = memberId;
        //        //user.account.id = accountId;

        //        bool isRegistered = false;
        //        bool updateUsersBeacons = false;
        //        bool userBeaconsPresent = false;
        //        bool isAvailed = false;
        //        //user = userDALmanager.getUserDetailsByMemberId(user);
        //        userBeaconsPresent = userDALmanager.userBeaconsPresent(user);
        //        if (!userBeaconsPresent)
        //        {
        //            bool add = beaconManager.addUserBeacons(user.id, user.beacon.id, user.offer.id);
        //        }
        //        isAvailed = userDALmanager.isAvailed(user);
        //        if (!isAvailed)
        //        {
        //            //user = userDALmanager.getUserDetailsByMemberId(user);
        //            isRegistered = userDALmanager.isRegisteredUser(user);
        //            updateUsersBeacons = userDALmanager.updateUsersBeacons(user);

        //            if (updateUsersBeacons)
        //            {
        //                //getting user contact details and sending mail
        //                user = new DALUserManager().getDetailsFromMemberId(user); 
        //                response = Request.CreateResponse(HttpStatusCode.OK, user);
        //            }
        //            else
        //            {
        //                response = Request.CreateResponse(HttpStatusCode.Conflict, "Failed");
        //            }
        //        } else
        //        {
        //            response = Request.CreateResponse(HttpStatusCode.Forbidden, "You have already availed this offer. Please try another offer.");
        //        }

        //    }
        //    return response;
        //}

        [Route("member/acceptOffer")]
        [HttpPost]

        public HttpResponseMessage acceptMemberOffer([FromBody] JObject userDetails)
        {
           String Phone = String.Empty;
            String emailId = String.Empty;
            String uniqueId = String.Empty;
            String socialService = String.Empty;

            User user = new User();
            user.memberId = (String)userDetails["memberId"];
            user.account.id = (Int64)userDetails["accountId"];
            user.beacon.id = (Int64)userDetails["beaconId"];
            user.offer.id = (Int64)userDetails["offerId"];
            user = new DALUserManager().getUserDetails(user); //user_id, type

            if (JSONHelper.isValidFieldForKey(userDetails, "email_id"))
                emailId = (String)userDetails["email_id"];
            if (JSONHelper.isValidFieldForKey(userDetails, "unique_id"))
                uniqueId = (String)userDetails["unique_id"];
            if (JSONHelper.isValidFieldForKey(userDetails, "phone"))
                Phone = (String)userDetails["phone"];
            if (JSONHelper.isValidFieldForKey(userDetails, "social_service"))
                socialService = (String)userDetails["social_service"];

            if (!userDALmanager.verifyMemberId(user.memberId, user.account.id)) //_______________(Working)
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Invalid Member Id! Please try again.");
            }
            if (uniqueId != String.Empty && !userDALmanager.verifyUserUniqueId(uniqueId, user.id))//uniqueId not null && !uniqueId exists against current user_______________(Working)
            {
                user = userDALmanager.getUserSocialLoginDetails(user);
                if (userDALmanager.isUserUniqueIdExists(uniqueId, user.account.id))//check if uniqueId saved against any other user_______________(Working)
                {
                    if(user.socialService != null)//Get previous login details, and show response     
                    {
                        return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Please try again with " + user.socialService);
                    }
                    else
                    {
                        return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Unable to login with " + socialService + ".");
                    }                                                    
                }
                if (emailId != String.Empty && !userDALmanager.isUserEmailIdExists(emailId, user.id))//emailId not null && check if new social login emailId exists_______________(Working)
                {
                    if (contactDALmanager.checkIfEmailIdExists(emailId, user.account.id))
                    {
                        return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Your " + socialService + " linked Email ID is already registered with us. Please try again with " + user.socialService + ".");
                    }
                    else //Save contact
                    {
                        using (WSTransaction transaction = new WSTransaction("SAVE_USER_NEW_CONTACT"))
                        {
                            user.contacts.Add(new Contact(emailId));
                            if (Phone != String.Empty)
                                user.contacts.Add(new Contact(Phone));
                            foreach (Contact contact in user.contacts)
                            {
                                contact.id = contactDALmanager.saveContact(contact, transaction);
                                if (contact.id != 0)
                                {
                                    user.contact = contact;
                                    userDALmanager.saveUserContacts(user, transaction);
                                }                                    
                                else
                                    transaction.rollback();
                            }
                            transaction.commit();
                        }
                    }
                }
                using (WSTransaction transaction = new WSTransaction("NEW_SOCIAL_LOGIN_DETAILS"))
                {
                    user.uniqueId = uniqueId;
                    user.socialService = socialService;
                    if (userDALmanager.saveUserSocialLogin(user, transaction))
                    {
                        if (!userDALmanager.userBeaconsPresent(user))
                        {
                            beaconManager.addUserBeacons(user.id, user.beacon.id, user.offer.id);
                        }
                        if (!userDALmanager.isAvailed(user))
                        {
                            if (userDALmanager.updateUsersBeacons(user))
                            {
                                //getting user contact details and sending mail
                                user = new DALUserManager().getDetailsFromMemberId(user);
                                transaction.commit();
                                return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, user);
                            }
                            else
                            {
                                transaction.rollback();
                                return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Failed");
                            }
                        }
                        else
                        {
                            transaction.commit();
                            return responseWrapper.CreateResponse(Request, HttpStatusCode.Forbidden, "You have already availed this offer. Please try another offer.");
                        }
                    }
                    else
                    {
                        transaction.rollback();
                        return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Failed");
                    }
                }
            }
            else //give offer
            {
                if (!userDALmanager.userBeaconsPresent(user))
                {
                    beaconManager.addUserBeacons(user.id, user.beacon.id, user.offer.id);
                }
                if (!userDALmanager.isAvailed(user))
                {
                    if (userDALmanager.updateUsersBeacons(user))
                    {
                        //getting user contact details and sending mail
                        user = new DALUserManager().getDetailsFromMemberId(user);
                        return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, user);
                    }
                    else
                    {
                        return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Failed");
                    }
                }
                else
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.Forbidden, "You have already availed this offer. Please try another offer.");
                }

            }
        }


    }
}
