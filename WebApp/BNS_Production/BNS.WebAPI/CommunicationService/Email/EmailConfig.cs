﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNS.WebAPI.CommunicationService.Email    
{
    public class EmailConfig
    {
        public const string smtpAddress = "smtp.bestnetworksystems.com";
        public const int portNumber = 587;

        public const string ExceptionDisplayName = "BNS Developer";
        public const string OffersDisplayName = "BNS Offer"; //by default it's offers
        public const string SupportDisplayName = "BNS Support"; // by default its support

        public const string emailFromSupport = "support@bestnetworksystems.com";
        public const string supportPassword = "@Best123";

        public const string emailFromOffers = "offers@bestnetworksystems.com";
        public const string offersPassword = "@Best123";
      
    }
}