﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Sockets;
using System.Web;

namespace BNS.WebAPI.CommunicationService.Email
{
    public class BnsEmailService : IDisposable
	{
        internal bool sendException(string[] emails, Exception exception)
        {
            bool enableSSL = false;
            String body = createEmailBody(exception);

            try
            {
                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress(EmailConfig.emailFromSupport, EmailConfig.ExceptionDisplayName);
                    foreach (string email in emails)
                    {
                        mail.To.Add(new MailAddress(email));
                    }

                    mail.Subject = "BNS Exception: " + exception.Message;
                    mail.Body = body;
                    mail.IsBodyHtml = true;
                    using (SmtpClient smtp = new SmtpClient(EmailConfig.smtpAddress, EmailConfig.portNumber))
                    {
                        smtp.Credentials = new NetworkCredential(EmailConfig.emailFromSupport, EmailConfig.supportPassword);
                        smtp.EnableSsl = enableSSL;
                        smtp.Send(mail);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                return false;
            }
        }

        private string createEmailBody(Exception exception)
		{
			string body = string.Empty;
			string exceptionTemplate = HttpContext.Current.Server.MapPath("~/EmailTemplates/exceptionTemplate.html");
			using (StreamReader reader = new StreamReader(exceptionTemplate))
			{
				body = reader.ReadToEnd();
			}
			
			if(exception.HelpLink == null)
			{
				string baseUrl = HttpContext.Current.Request.Url.Authority;
				exception.HelpLink = "http://" + baseUrl + "/DefaultPages/ExceptionList.html";
			}
	 
			body = body.Replace("{exception.Message}","An error has occurred.");
			body = body.Replace("{exception.ExceptionMessage}", exception.Message);            
			body = body.Replace("{exception.ExceptionType}", exception.GetType().ToString());
			body = body.Replace("{exception.StackTrace}", exception.StackTrace);
			body = body.Replace("{exception.HelpLink}",  exception.HelpLink);         
			body = body.Replace("{exception.System}", System.Environment.GetEnvironmentVariable("COMPUTERNAME"));
			body = body.Replace("{exception.IPAddress}", this.LocalIPAddress().ToString());
			return body;
		}

		private IPAddress LocalIPAddress()
		{
			if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
			{
				return null;
			}

			IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());

			return host
				.AddressList
				.FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork);
		}

		public void Dispose()
		{
			
		}
	}
}