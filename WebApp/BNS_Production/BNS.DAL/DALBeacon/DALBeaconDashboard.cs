﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.Offers;
using WSDatabase;
using System.Data.SqlClient;
using BNS.DAL.DALOffer;
using BNS.Models.Categories;
using BNS.DAL.DALCategory;

namespace BNS.DAL.DALBeacon
{
    public class DALBeaconDashboard
    {
        public Dictionary<string, object> getBeaconDashboardDetails(Int64 beaconId, Int64 accountId)
        {
            Dictionary<string, object> responseObject = new Dictionary<string, object>();

            Int64 totalUsers = 0;
            Int64 members = 0;
            Int64 nonMembers = 0;
            Offer popularOffer = new Offer();
            List<Dictionary<string, object>> monthlyUserCount = new List<Dictionary<string, object>>();
            List<Category> categoryList = new List<Category>();

            totalUsers = getTotalUsersCount(beaconId);
            members = getMembersCount(beaconId);
            nonMembers = getNonMembersCount(beaconId);
            popularOffer = getPopularOffer(beaconId);
            monthlyUserCount = getRegisteredUsersOfLast5Months(beaconId);
            categoryList = getOfferCategoriesVsUsersOfLast5Months(beaconId, accountId);

            responseObject.Add("totalUsers", totalUsers);
            responseObject.Add("members", members);
            responseObject.Add("nonMembers", nonMembers);
            responseObject.Add("popularOffer", popularOffer);
            responseObject.Add("monthlyUserCount", monthlyUserCount);
            responseObject.Add("categoryList", categoryList);

            return responseObject;
        }

        public Int64 getTotalUsersCount(Int64 id)
        {
            Int64 count = 0;
            string query = QueryConstants.BEACON_TOTAL_USERS;
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    count = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("TOTAL_USERS")));
                }
            }
            Query.connection.Close();
            return count;
        }

        public Int64 getMembersCount(Int64 id)
        {
            Int64 count = 0;
            string query = QueryConstants.BEACON_MEMBERS;
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    count = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("MEMBERS")));
                }
            }
            Query.connection.Close();
            return count;
        }

        public Int64 getNonMembersCount(Int64 id)
        {
            Int64 count = 0;
            string query = QueryConstants.BEACON_NON_MEMBERS;
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    count = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("NON_MEMBERS")));
                }
            }
            Query.connection.Close();
            return count;
        }

        public Offer getPopularOffer(Int64 id)
        {
            Offer offer = new Offer();
            string query = QueryConstants.BEACON_POPULAR_OFFER;
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offer.id = reader.GetInt64(reader.GetOrdinal("OFFER_ID"));
                }
            }
            Query.connection.Close();
            offer = new DALOfferManager().getDetails(offer);
            return offer;
        }

        public List<Dictionary<string, object>> getRegisteredUsersOfLast5Months(Int64 id)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);

            List<Dictionary<string, object>> monthlyUserCount = new List<Dictionary<string, object>>();
            Int64 count = 0;
            int month = 0;
            int year = 0;

            DateTime userDate;            
            DateTime _START = currentDateTime.AddMonths(-5).AddDays(1-currentDateTime.Day).AddHours(-currentDateTime.Hour).AddMinutes(-currentDateTime.Minute).AddSeconds(-currentDateTime.Second);
            DateTime _END = currentDateTime.AddDays(-currentDateTime.Day+1).AddHours(-currentDateTime.Hour).AddMinutes(-currentDateTime.Minute).AddSeconds(-currentDateTime.Second-1);

            string query = QueryConstants.REGISTERED_USERS_FROM_BEACON_BY_MONTH;
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = id;
            Query.parameters["_START"] = _START;
            Query.parameters["_END"] = _END;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Dictionary<string, object> obj = new Dictionary<string, object>();
                    count = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("USER_COUNT")));
                    month = Convert.ToInt16(reader.GetValue(reader.GetOrdinal("_MONTH")));
                    year = Convert.ToInt16(reader.GetValue(reader.GetOrdinal("_YEAR")));
                    userDate = new DateTime((int)year, (int)month, 1);
                    obj.Add("userCount", count);
                    obj.Add("userDate", userDate);
                    monthlyUserCount.Add(obj);
                }
            }
            Query.connection.Close();
            return monthlyUserCount;
        }

        public List<Category> getOfferCategoriesVsUsersOfLast5Months(Int64 beaconId, Int64 accountId)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);

            List<Category> categories = new List<Category>();
            categories = new DALCategoryManager().getAll(accountId);
            Int64 count = 0;
            Int64 categoryId;
            int month = 0;
            int year = 0;

            DateTime userDate;
            DateTime _START = currentDateTime.AddMonths(-5).AddDays(1 - currentDateTime.Day).AddHours(-currentDateTime.Hour).AddMinutes(-currentDateTime.Minute).AddSeconds(-currentDateTime.Second);
            DateTime _END = currentDateTime.AddDays(-currentDateTime.Day + 1).AddHours(-currentDateTime.Hour).AddMinutes(-currentDateTime.Minute).AddSeconds(-currentDateTime.Second - 1);

            string query = QueryConstants.USER_COUNT_BY_OFFER_CATEGORIES_FROM_BEACON;
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beaconId;
            Query.parameters["_START"] = _START;
            Query.parameters["_END"] = _END;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Dictionary<string, object> obj = new Dictionary<string, object>();
                    count = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("USER_COUNT")));
                    month = Convert.ToInt16(reader.GetValue(reader.GetOrdinal("_MONTH")));
                    year = Convert.ToInt16(reader.GetValue(reader.GetOrdinal("_YEAR")));
                    categoryId = reader.GetInt64(reader.GetOrdinal("CATEGORY_ID"));
                    userDate = new DateTime((int)year, (int)month, 1);
                    obj.Add("userCount", count);
                    obj.Add("userDate", userDate);
                    foreach (Category category in categories)
                    {
                        if (category.id == categoryId)
                        {
                            category.data.Add(obj);
                        }
                    }
                }
            }
            Query.connection.Close();
            return categories;
        }
    }
}