﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.Offers;
using WSDatabase;
using System.Data.SqlClient;
using BNS.Models.Users;
using BNS.DAL.DALUser;


namespace BNS.DAL.DALOffer
{
    public class DALOfferDashboard
    {
        public Dictionary<string, object> getOfferDashboardDetails(Int64 offerId)
        {
            Dictionary<string, object> responseObject = new Dictionary<string, object>();

            Int64 registeredUsers = 0;
            Int64 offerSeenCount = 0;
            Int64 offerAvailedCount = 0;
            Int64 beaconCount = 0;
            Int64 kioskCount = 0;
            Int64 offerAvailedCountForChart = 0;
            Int64 offerIgnoredCountForChart = 0;
            List<User> availedBy = new List<User>();
            List<Dictionary<string, object>> monthlyUserCount = new List<Dictionary<string, object>>();

            registeredUsers = getRegisteredUsersCount(offerId);
            offerSeenCount = getOfferSeenCount(offerId);
            offerAvailedCount = getOfferAvailedUsersCount(offerId);
            beaconCount = getBeaconCount(offerId);
            kioskCount = getKioskCount(offerId);
            availedBy = offerAvailedBy(offerId);
            offerAvailedCountForChart = getOfferAvailedCount(offerId);
            offerIgnoredCountForChart = getOfferIgnoredCount(offerId);
            monthlyUserCount = getRegisteredUsersOfLast5Months(offerId);

            responseObject.Add("registeredUsers", registeredUsers);
            responseObject.Add("offerSeenCount", offerSeenCount);
            responseObject.Add("offerAvailedCount", offerAvailedCount);
            responseObject.Add("beaconCount", beaconCount);
            responseObject.Add("kioskCount", kioskCount);
            responseObject.Add("offerAvailedCountForChart", offerAvailedCountForChart);
            responseObject.Add("offerIgnoredCountForChart", offerIgnoredCountForChart);
            responseObject.Add("availedBy", availedBy);
            responseObject.Add("monthlyUserCount", monthlyUserCount);

            return responseObject;
        }

        public Int64 getRegisteredUsersCount(Int64 id)
        {
            Int64 count = 0;
            string query = QueryConstants.REGISTERED_USERS;
            WSQuery Query = new WSQuery(query);
            Query.parameters["offerId"] = id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    count = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("REGISTERED_USERS")));
                }
            }
            Query.connection.Close();
            return count;
        }

        public Int64 getOfferSeenCount(Int64 id)
        {
            Int64 count = 0;
            string query = QueryConstants.OFFER_SEEN;
            WSQuery Query = new WSQuery(query);
            Query.parameters["offerId"] = id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    count = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("OFFER_SEEN")));
                }
            }
            Query.connection.Close();
            return count;
        }
   
        public Int64 getOfferAvailedUsersCount(Int64 id)
        {
            Int64 count = 0;
            string query = QueryConstants.OFFER_AVAILED;
            WSQuery Query = new WSQuery(query);
            Query.parameters["offerId"] = id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    count = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("OFFER_AVAILED")));
                }
            }
            Query.connection.Close();
            return count;
        }

        public Int64 getBeaconCount(Int64 id)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);
            
            Int64 count = 0;
            string query = QueryConstants.BEACON_COUNT;
            WSQuery Query = new WSQuery(query);
            Query.parameters["offerId"] = id;
            Query.parameters["currentDateTime"] = currentDateTime;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    count = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("BEACON_COUNT")));
                }
            }
            Query.connection.Close();
            return count;
        }

        public Int64 getKioskCount(Int64 id)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);
            
            Int64 count = 0;
            string query = QueryConstants.KIOSK_COUNT;
            WSQuery Query = new WSQuery(query);
            Query.parameters["offerId"] = id;
            Query.parameters["currentDateTime"] = currentDateTime;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    count = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("KIOSK_COUNT")));
                }
            }
            Query.connection.Close();
            return count;
        }

        public List<User> offerAvailedBy(Int64 id)
        {
            List<User> availedBy = new List<User>();

            String query = QueryConstants.OFFER_AVAILED_BY;
            WSQuery Query = new WSQuery(query);
            Query.parameters["offerId"] = id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);         
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    availedBy.Add(new User(reader));
                }
            }
            Query.connection.Close();
            foreach (User user in availedBy)
            {
                user.contacts = new DALUserManager().getCantactsForUser(user.id);                
            }

            return availedBy;
        }

        public Int64 getOfferAvailedCount(Int64 id)
        {
            Int64 count = 0;
            string query = QueryConstants.OFFER_AVAILED_COUNT;
            WSQuery Query = new WSQuery(query);
            Query.parameters["offerId"] = id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    count = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("OFFER_AVAILED_COUNT")));
                }
            }
            Query.connection.Close();
            return count;
        }

        public Int64 getOfferIgnoredCount(Int64 id)
        {
            Int64 count = 0;
            string query = QueryConstants.OFFER_IGNORED_COUNT;
            WSQuery Query = new WSQuery(query);
            Query.parameters["offerId"] = id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    count = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("OFFER_IGNORED_COUNT")));
                }
            }
            Query.connection.Close();
            return count;
        }

        public List<Dictionary<string, object>> getRegisteredUsersOfLast5Months(Int64 id)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);

            List<Dictionary<string, object>> monthlyUserCount = new List<Dictionary<string, object>>();
            Int64 count = 0;
            int month = 0;
            int year = 0;

            DateTime userDate;
            DateTime _START = currentDateTime.AddMonths(-5).AddDays(1 - currentDateTime.Day).AddHours(-currentDateTime.Hour).AddMinutes(-currentDateTime.Minute).AddSeconds(-currentDateTime.Second);
            DateTime _END = currentDateTime.AddDays(-currentDateTime.Day + 1).AddHours(-currentDateTime.Hour).AddMinutes(-currentDateTime.Minute).AddSeconds(-currentDateTime.Second - 1);

            string query = QueryConstants.TOTAL_REGISTERED_USERS_OFFERS_BY_MONTH;
            WSQuery Query = new WSQuery(query);
            Query.parameters["offerId"] = id;
            Query.parameters["_START"] = _START;
            Query.parameters["_END"] = _END;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Dictionary<string, object> obj = new Dictionary<string, object>();
                    count = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("USER_COUNT")));
                    month = Convert.ToInt16(reader.GetValue(reader.GetOrdinal("_MONTH")));
                    year = Convert.ToInt16(reader.GetValue(reader.GetOrdinal("_YEAR")));
                    userDate = new DateTime((int)year, (int)month, 1);
                    obj.Add("userCount", count);
                    obj.Add("userDate", userDate);
                    monthlyUserCount.Add(obj);
                }
            }
            Query.connection.Close();
            return monthlyUserCount;
        }

    }
}