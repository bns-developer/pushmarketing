﻿using BNS.DL.WSDatabase;
using BNS.Models.Stores;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using BNS.Models.BNSModels;
using BNS.Models.Accounts;
using WSDatabase;
using BNS.Models.Countries;

namespace BNS.DAL.DALStore
{
    public class DALStoreManager
    {      
        public Int64 save(Store store)
        {
            using (WSTransaction transaction = new WSTransaction("SAVE_STORE"))
            {
                String query = "INSERT INTO Stores(name, number, country_id, account_id, created_on, updated_on, created_by, updated_by) OUTPUT Inserted.id  VALUES(@name, @number, @countryId, @accountId, @createdOn, @updatedOn, @createdBy, @updatedBy);";
                WSQuery Query = new WSQuery(query, store.toDictionary(),transaction);
                store.id = WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQueryReturnID(Query);
                if(store.id != 0 )
                {
                    transaction.commit();
                    return store.id;
                }
                else
                {
                    transaction.rollback();
                    return 0;
                }
            }
        }

        public List<Store> getAll(Account account)
        {
            String query = QueryConstants.ALL_STORES_BY_ACCOUNT + " STORE.account_id = " + account.id.ToString() + " ORDER BY STORE.updated_on DESC";
            WSQuery Query = new WSQuery(query);           
            Query.parameters["accountId"] = account.id;                    
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Store> storesList = new List<Store>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    storesList.Add(new Store(reader));
                }               
            }
            Query.connection.Close();
            return storesList;
        }

        public List<Store> getStoresForDropDown(Account account)
        {
            String query = QueryConstants.ALL_STORES_FOR_DROPDOWN_BY_ACCOUNT + " STORE.account_id = " + account.id.ToString() + " ORDER BY STORE.name ASC";
            WSQuery Query = new WSQuery(query);         
            Query.parameters["accountId"] = account.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Store> storesList = new List<Store>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    storesList.Add(new Store(reader));
                }
                Query.connection.Close();
            }
            return storesList;
        }

        public bool checkIfNameExists(String storeName, Int64 account)
        {
            String query = "SELECT CASE WHEN EXISTS (Select * from Stores where name = @storeName and account_id = @accountId) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["storeName"] = storeName;
            Query.parameters["accountId"] = account;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }

        public bool checkIfNumberExists(String storeNumber, Int64 account)
        {
            String query = "SELECT CASE WHEN EXISTS (Select * from Stores where number = @storeNumber and account_id = @accountId) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["storeNumber"] = storeNumber;
            Query.parameters["accountId"] = account;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }

        public bool isStoreUsedForBeacon(Int64 storeId)
        {
            WSQuery Query = new WSQuery();
            Query.query = "SELECT * FROM Beacons WHERE store_id = @storeId";
            Query.parameters["storeId"] = storeId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            return reader.HasRows;
        }

        public bool isStoreUsedForKiosk(Int64 storeId)
        {
            WSQuery Query = new WSQuery();
            Query.query = "SELECT * FROM Kiosks WHERE store_id = @storeId";
            Query.parameters["storeId"] = storeId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            return reader.HasRows;
        }

        public Int64 getAccountIdFromBeaconId(Int64 beacon_id)
        {
            Int64 accountId = 0;
            String query = QueryConstants.GET_ACCOUNT_ID;
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beacon_id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    accountId = reader.GetInt64(reader.GetOrdinal("ACCOUNT_ID"));
                }
            }
            return accountId;
        }

        public bool update (Store store)
        {
            using (WSTransaction transaction = new WSTransaction("UPDATE_STORE"))
            {
                String query = "UPDATE Stores SET name = @name, number = @number, country_id = @countryId, updated_on =  @updatedOn, updated_by = @updatedBy WHERE id = @id";
                WSQuery Query = new WSQuery(query, store.toDictionary(), transaction);
                if (!WSDatabaseManager.sharedDatabaseManagerinstance.executeUpdateQuery(Query))
                {
                    transaction.rollback();
                    return false;
                }
                transaction.commit();
                return true;
            }
        }

        public bool delete(Int64 storeId)
        {
            using (WSTransaction transaction = new WSTransaction("DELETE_STORE"))
            {
                WSQuery Query = new WSQuery();
                Query.query = "DELETE FROM Stores WHERE id = @storeId";
                Query.parameters["storeId"] = storeId;
                Query.transaction = transaction;
                if (!WSDatabaseManager.sharedDatabaseManagerinstance.executeDeleteQuery(Query))
                {
                    transaction.rollback();
                    return false;
                }
                transaction.commit();
                return true;
            }
        }

        public Store getDetails(Store store)
        {
            String query = QueryConstants.STORE_DETAILS;
            WSQuery Query = new WSQuery(query);
            Query.parameters["storeId"] = store.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {                   
                    store = new Store(reader);
                    store.country = new Country(reader);
                }
            }
            Query.connection.Close();
            return store;
        }
    }
}