﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.Stores;
using BNS.Models.Beacons;
using BNS.Models.Kiosks;
using BNS.Models.Offers;
using BNS.Models.Categories;
using WSDatabase;
using System.Data.SqlClient;
using BNS.DAL.DALKiosk;
using BNS.DAL.DALBeacon;
using BNS.DAL.DALOffer;
using BNS.DAL.DALCategory;

namespace BNS.DAL.DALStore
{
    public class DALStoreDashboard
    {
        public Dictionary<string, object> getStoreDashboardData(Int64 storeId, Int64 accountId)
        {
            Dictionary<string, object> responseObject = new Dictionary<string, object>();

            Int64 Members = 0;           
            Int64 nonMembers = 0;
            Int64 beaconCount = 0;
            Int64 kioskCount = 0;                              
            Beacon popularBeacon = new Beacon();
            Kiosk popularKiosk = new Kiosk();
            Offer popularOffer = new Offer();
            List<Dictionary<string, object>> monthlyUserCount = new List<Dictionary<string, object>>();
            List<Dictionary<string, object>> usersCountByOfferCategory = new List<Dictionary<string, object>>();
            List<Category> categoryList = new List<Category>();

            Members = getMembersCount(storeId);
            nonMembers = getNonMembersCount(storeId);
            beaconCount = getBeaconCount(storeId);
            kioskCount = getKioskCount(storeId);
            popularBeacon = getPopularBeacon(storeId);
            popularKiosk = getPopularkiosk(storeId);
            popularOffer = getPopularOffer(storeId);
            monthlyUserCount = getRegisteredUsersOfLast5Months(storeId);
            categoryList = getOfferCategoriesVsUsersOfLast5Months(storeId, accountId);

            responseObject.Add("Members", Members);
            responseObject.Add("nonMembers", nonMembers);
            responseObject.Add("beaconCount", beaconCount);
            responseObject.Add("kioskCount", kioskCount);
            responseObject.Add("popularBeacon", popularBeacon);
            responseObject.Add("popularKiosk", popularKiosk);
            responseObject.Add("popularOffer", popularOffer);
            responseObject.Add("monthlyUserCount", monthlyUserCount);
            responseObject.Add("categoryList", categoryList);
            return responseObject;
        }

        public Int64 getMembersCount(Int64 id)
        {
            Int64 count = 0;
            string query = QueryConstants.TOTAL_STORE_MEMBERS;
            WSQuery Query = new WSQuery(query);
            Query.parameters["storeId"] = id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    count = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("MEMBER_COUNT")));
                }
            }
            Query.connection.Close();
            return count;
        }

        public Int64 getNonMembersCount(Int64 id)
        {
            Int64 count = 0;
            string query = QueryConstants.TOTAL_NON_MEMBERS;
            WSQuery Query = new WSQuery(query);
            Query.parameters["storeId"] = id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    count = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("NON_MEMBER_COUNT")));
                }
            }
            Query.connection.Close();
            return count;
        }

        public Int64 getBeaconCount(Int64 id)
        {
            Int64 count = 0;
            string query = QueryConstants.STORE_BEACON_COUNT;
            WSQuery Query = new WSQuery(query);
            Query.parameters["storeId"] = id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    count = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("BEACON_COUNT")));
                }
            }
            Query.connection.Close();
            return count;
        }

        public Int64 getKioskCount(Int64 id)
        {
            Int64 count = 0;
            string query = QueryConstants.STORE_KIOSK_COUNT;
            WSQuery Query = new WSQuery(query);
            Query.parameters["storeId"] = id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    count = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("KIOSK_COUNT")));
                }
            }
            Query.connection.Close();
            return count;
        }

        public Beacon getPopularBeacon(Int64 id)
        {
            Beacon beacon = new Beacon();
            string query = QueryConstants.POPULAR_STORE_BEACON;
            WSQuery Query = new WSQuery(query);
            Query.parameters["storeId"] = id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    beacon.id = reader.GetInt64(reader.GetOrdinal("BEACON_ID"));
                }
            }
            Query.connection.Close();
            beacon = new DALBeaconManager().getDetails(beacon);
            return beacon;
        }

        public Kiosk getPopularkiosk(Int64 id)
        {
            Kiosk kiosk = new Kiosk();
            string query = QueryConstants.POPULAR_STORE_KIOSK;
            WSQuery Query = new WSQuery(query);
            Query.parameters["storeId"] = id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    kiosk.id = reader.GetInt64(reader.GetOrdinal("KIOSK_ID"));
                }
            }
            Query.connection.Close();
            kiosk = new DALKioskManager().getDetails(kiosk);
            return kiosk;
        }

        public Offer getPopularOffer(Int64 id)
        {
            Offer offer = new Offer();
            string query = QueryConstants.POPULAR_STORE_OFFER;
            WSQuery Query = new WSQuery(query);
            Query.parameters["storeId"] = id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offer.id = reader.GetInt64(reader.GetOrdinal("OFFER_ID"));
                }
            }
            Query.connection.Close();
            offer = new DALOfferManager().getDetails(offer);
            return offer;
        }

        public List<Dictionary<string, object>> getRegisteredUsersOfLast5Months(Int64 id)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);

            List<Dictionary<string, object>> monthlyUserCount = new List<Dictionary<string, object>>();
            Int64 count = 0;
            int month = 0;
            int year = 0;
            DateTime userDate;
            DateTime _START = currentDateTime.AddMonths(-5).AddDays(1 - currentDateTime.Day).AddHours(-currentDateTime.Hour).AddMinutes(-currentDateTime.Minute).AddSeconds(-currentDateTime.Second);
            DateTime _END = currentDateTime.AddDays(-currentDateTime.Day + 1).AddHours(-currentDateTime.Hour).AddMinutes(-currentDateTime.Minute).AddSeconds(-currentDateTime.Second - 1);

            string query = QueryConstants.TOTAL_REGISTERED_USERS_FROM_STORE_BY_MONTH;
            WSQuery Query = new WSQuery(query);
            Query.parameters["storeId"] = id;
            Query.parameters["_START"] = _START;
            Query.parameters["_END"] = _END;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Dictionary<string, object> obj = new Dictionary<string, object>();
                    count = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("USER_COUNT")));
                    month = Convert.ToInt16(reader.GetValue(reader.GetOrdinal("_MONTH")));
                    year = Convert.ToInt16(reader.GetValue(reader.GetOrdinal("_YEAR")));
                    userDate = new DateTime((int)year, (int)month, 1);

                    obj.Add("userCount", count);
                    //obj.Add("userDate", userDate.ToString("MMM yyyy"));
                    obj.Add("userDate", userDate);
                    monthlyUserCount.Add(obj);
                }
            }
            Query.connection.Close();
            return monthlyUserCount;
        }

        public List<Category> getOfferCategoriesVsUsersOfLast5Months(Int64 storeId, Int64 accountId)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);

            List<Category> categories = new List<Category>();
            categories = new DALCategoryManager().getAll(accountId);
            Int64 count = 0;
            Int64 categoryId;
            int month = 0;
            int year = 0;

            DateTime userDate;
            DateTime _START = currentDateTime.AddMonths(-5).AddDays(1 - currentDateTime.Day).AddHours(-currentDateTime.Hour).AddMinutes(-currentDateTime.Minute).AddSeconds(-currentDateTime.Second);
            DateTime _END = currentDateTime.AddDays(-currentDateTime.Day + 1).AddHours(-currentDateTime.Hour).AddMinutes(-currentDateTime.Minute).AddSeconds(-currentDateTime.Second - 1);

            string query = QueryConstants.TOTAL_USER_OFFER_CATEGORIES_FROM_STORE_BY_MONTH;
            WSQuery Query = new WSQuery(query);
            Query.parameters["storeId"] = storeId;
            Query.parameters["_START"] = _START;
            Query.parameters["_END"] = _END;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Dictionary<string, object> obj = new Dictionary<string, object>();
                    count = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("USER_COUNT")));
                    month = Convert.ToInt16(reader.GetValue(reader.GetOrdinal("_MONTH")));
                    year = Convert.ToInt16(reader.GetValue(reader.GetOrdinal("_YEAR")));
                    categoryId = reader.GetInt64(reader.GetOrdinal("CATEGORY_ID"));
                    userDate = new DateTime((int)year, (int)month, 1);
                    obj.Add("userCount", count);
                    obj.Add("userDate", userDate);
                    foreach (Category category in categories)
                    {
                        if (category.id == categoryId)
                        {
                            category.data.Add(obj);
                        }
                    }
                }
            }
            Query.connection.Close();
            return categories;
        }
    }
}