﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.DL.WSDatabase;
using System.Data.SqlClient;
using BNS.Models.Kiosks;
using BNS.Models.Accounts;
using BNS.Models.LuckyItems;
using BNS.DAL.DALLuckyItem;
using BNS.DAL;
using WSDatabase;
using System.Web.Configuration;
using BNS.Models.Stores;
using BNS.Models.Countries;
using BNS.Models.Users;
using BNS.Models.Offers;
using BNS.Models.UsersKiosks;

namespace BNS.DAL.DALKiosk
{
    public class DALKioskManager
    {
        public Int64 saveKiosk(Kiosk kiosk)
        {
            using (WSTransaction transaction = new WSTransaction("SAVE_KIOSK"))
            {
                String query = "INSERT INTO Kiosks(name, number, store_id, is_workable, created_on, updated_on, created_by, updated_by) OUTPUT Inserted.id  VALUES(@name, @number, @storeId, @isWorkable, @createdOn, @updatedOn, @createdBy, @updatedBy);";
                WSQuery Query = new WSQuery(query, kiosk.toDictionary(), transaction);
                kiosk.id = WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQueryReturnID(Query);

                List<DefaultOffer> defaultOffers = getDefaultOffersForNewKiosk(kiosk.createdBy.account.id);                

                kiosk.luckyItems = this.getAllLuckeyItemForKiosk(kiosk);
                if (kiosk.luckyItems.Count != 0)
                {
                    foreach (LuckyItem item in kiosk.luckyItems)
                    {
                        String luckyItemUrl = getUrl(kiosk.id, item.id);
                        item.shortUrl = UrlShortener.shortenIt(luckyItemUrl);
                        bool is_inserted = this.setLuckyItemsToKiosk(kiosk, item, transaction);
                        if (is_inserted && defaultOffers.Count == 2)
                        {
                            foreach (DefaultOffer offer in defaultOffers)
                            {
                                if (!this.setDefaultOfferToKioskLuckyItem(kiosk, item, offer, transaction))
                                {
                                    transaction.rollback();
                                    return kiosk.id = 0;
                                }
                            }                                                       
                        }
                        else
                        {
                            transaction.rollback();
                            return kiosk.id = 0;
                        }
                    }
                    transaction.commit();
                }
                else
                {
                    transaction.rollback();
                    kiosk.id = 0;
                }
                return kiosk.id;
            }
        }

        public List<DefaultOffer> getDefaultOffersForNewKiosk(Int64 accountId)
        {
            List<DefaultOffer> offers = new List<DefaultOffer>();
            String query = "SELECT offer_id AS DEFAULT_OFFER_ID, offer_type AS DEFAULT_OFFER_OFFER_TYPE FROM default_offers WHERE device_type = 2 and account_id = @accountId";
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = accountId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offers.Add(new DefaultOffer(reader));
                }
            }
            Query.connection.Close();
            return offers;
        }

        private List<LuckyItem> getAllLuckeyItemForKiosk(Kiosk kiosk)
        {
            WSQuery Query = new WSQuery();
            Query.query = QueryConstants.All_LUCKY_ITEMS_BY_ACCOUNT + kiosk.store.id.ToString() + " AND STORE.account_id = LUCKY_ITEM.account_id;";
            Query.parameters["storeId"] = kiosk.store.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<LuckyItem> luckyItems = new List<LuckyItem>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    luckyItems.Add(new LuckyItem(reader));
                }              
            }
            Query.connection.Close();
            return luckyItems;
        }

        public String getUrl(Int64 kioskId, Int64 luckyItemId)
        {
            UriBuilder uriBuilder = new UriBuilder();
            uriBuilder.Scheme = "https";
            uriBuilder.Host = WebConfigurationManager.AppSettings["hostName"].ToString();
            uriBuilder.Path = @"/luckyItemCurrentOffer?kioskId=" + kioskId.ToString() + "&luckyItemId=" + luckyItemId.ToString();
            Uri uri = uriBuilder.Uri;
            return uri.ToString();
        }

        public Kiosk getDetails(Kiosk kiosk)
        {
            String query = QueryConstants.KIOSK_DETAILS;
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = kiosk.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    kiosk = new Kiosk(reader);
                    kiosk.store = new Store(reader);
                    kiosk.store.country = new Country(reader);
                }
            }
            Query.connection.Close();
            return kiosk;
        }

        public Kiosk getKioskDetails(Kiosk kiosk)
        {
            String query = QueryConstants.KIOSK_DETAILS_FOR_DEVICE;
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = kiosk.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    kiosk = new Kiosk(reader);                   
                }
            }
            Query.connection.Close();
            return kiosk;
        }

        public bool setLuckyItemsToKiosk(Kiosk kiosk, LuckyItem luckyItem, WSTransaction transaction)
        {
            //String query = "INSERT INTO Kiosks_lucky_items(kiosk_id, lucky_item_id, created_on, updated_on, created_by, updated_by) VALUES(@kioskId, @luckyItemId, @createdOn, @updatedOn, @createdBy, @updatedBy);";
            String query = "INSERT INTO Kiosks_lucky_items(kiosk_id, lucky_item_id, short_url, count, created_on, updated_on, created_by, updated_by) VALUES(@kioskId, @luckyItemId, @shortUrl, @count, @createdOn, @updatedOn, @createdBy, @updatedBy);";
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = kiosk.id;
            Query.parameters["luckyItemId"] = luckyItem.id;            
            Query.parameters["shortUrl"] = luckyItem.shortUrl;
            Query.parameters["count"] = 0;
            Query.parameters["createdOn"] = DateTime.UtcNow.ToString();
            Query.parameters["updatedOn"] = DateTime.UtcNow.ToString();
            Query.parameters["createdBy"] = kiosk.createdBy.id;
            Query.parameters["updatedBy"] = kiosk.updatedBy.id;
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);
        }

        public bool setDefaultOfferToKioskLuckyItem(Kiosk kiosk, LuckyItem luckyItem, DefaultOffer offer, WSTransaction transaction)
        {
            String query = "INSERT INTO Kiosks_Lucky_Items_Offers(kiosk_id, lucky_item_id, offer_id, is_default, default_type, created_on, updated_on, created_by, updated_by) VALUES(@kioskId, @luckyItemId, @offerId, @isDefault, @defaultType, @createdOn, @updatedOn, @createdBy, @updatedBy);";
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = kiosk.id;
            Query.parameters["luckyItemId"] = luckyItem.id;
            Query.parameters["offerId"] = offer.id;            
            Query.parameters["isDefault"] = 1;
            if (offer.OfferType == DefaultOffer.Offer_Type.MEMBER)
            {
                Query.parameters["defaultType"] = 1;
            }
            if (offer.OfferType == DefaultOffer.Offer_Type.NON_MEMBER)
            {
                Query.parameters["defaultType"] = 2;
            }
            Query.parameters["createdOn"] = DateTime.UtcNow.ToString();
            Query.parameters["updatedOn"] = DateTime.UtcNow.ToString();
            Query.parameters["createdBy"] = kiosk.createdBy.id;
            Query.parameters["updatedBy"] = kiosk.updatedBy.id;
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);
        }

        public List<Kiosk> getAll(Account account)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);
            
            String query = QueryConstants.ALL_KIOSKS_BY_ACCOUNT + account.id.ToString() + " ORDER BY KIOSK.updated_on DESC";
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = account.id;
            Query.parameters["currentDateTime"] = currentDateTime;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Kiosk> kiosksList = new List<Kiosk>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    kiosksList.Add(new Kiosk(reader));
                }
            }
            Query.connection.Close();
            return kiosksList;
        }      

        public List<Kiosk> getKiosksByStoreId(Int64 storeId)
        {
            String query = QueryConstants.ALL_KIOSKS_BY_STORE;
            WSQuery Query = new WSQuery(query);
            Query.parameters["storeId"] = storeId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Kiosk> kiosksList = new List<Kiosk>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    kiosksList.Add(new Kiosk(reader));
                }
            }
            Query.connection.Close();
            return kiosksList;
        }      

        public bool updateKioskLuckyItemCount(Int64 kioskId, Int64 luckyItemId)
        {
            String query = "UPDATE Kiosks_lucky_items SET count = count + 1, updated_on = @updateOn WHERE kiosk_id = @kioskId AND lucky_item_id = @luckyItemId";
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = kioskId;
            Query.parameters["luckyItemId"] = luckyItemId;
            Query.parameters["updateOn"] = DateTime.UtcNow.ToString();
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeUpdateQuery(Query);
        }

        //public bool checkDefaultOffer(Int64 accountId)
        //{
        //    bool memberDefault = isDefaultOffersSetForBeacon(accountId, 1);
        //    bool nonMemberDefault = isDefaultOffersSetForBeacon(accountId, 2);
        //    if (memberDefault && nonMemberDefault)
        //        return true;
        //    else
        //        return false;
        //}
        //public bool isDefaultOffersSetForBeacon(Int64 accountId, int offerType)
        //{
        //    String query = "SELECT CASE WHEN EXISTS (Select * from default_offers where device_type = 1 and offer_type = @offerType and account_id = @accountId) THEN 1 ELSE 0 END AS Result";
        //    WSQuery Query = new WSQuery(query);
        //    Query.parameters["accountId"] = accountId;
        //    Query.parameters["offerType"] = offerType;
        //    return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        //}

        public bool checkIfNameExists(String kioskName, Int64 storeId)
        {                                               
            String query = "SELECT CASE WHEN EXISTS (Select * from Kiosks where name = @kioskName and store_id = @storeId) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskName"] = kioskName;
            Query.parameters["storeId"] = storeId;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }

        public bool checkIfNumberExists(String kioskNumber, Int64 storeId)
        {
            String query = "SELECT CASE WHEN EXISTS (Select * from Kiosks where number = @kioskNumber and store_id = @storeId) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskNumber"] = kioskNumber;
            Query.parameters["storeId"] = storeId;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }

        public bool update(Kiosk kiosk)
        {
            using (WSTransaction transaction = new WSTransaction("UPDATE_KIOSK"))
            {
                String query = "UPDATE Kiosks SET name = @name, number = @number, store_id = @storeId, updated_on =  @updatedOn, updated_by = @updatedBy WHERE id = @id";
                WSQuery Query = new WSQuery(query, kiosk.toDictionary(), transaction);
                if (!WSDatabaseManager.sharedDatabaseManagerinstance.executeUpdateQuery(Query))
                {
                    transaction.rollback();
                    return false;
                }
                transaction.commit();
                return true;
            }
        }

        public bool updateFromDevice(Kiosk kiosk)
        {
            using (WSTransaction transaction = new WSTransaction("UPDATE_KIOSK"))
            {
                String query = "UPDATE Kiosks SET name = @name, number = @number, updated_on =  @updatedOn, updated_by = @updatedBy WHERE id = @id";
                WSQuery Query = new WSQuery(query, kiosk.toDictionary(), transaction);
                if (!WSDatabaseManager.sharedDatabaseManagerinstance.executeUpdateQuery(Query))
                {
                    transaction.rollback();
                    return false;
                }
                transaction.commit();
                return true;
            }
        }

        public bool isKioskUsedForRegistration(Int64 kioskId)
        {
            WSQuery Query = new WSQuery();
            Query.query = "SELECT * FROM users_kiosks WHERE kiosk_id = @kioskId";
            Query.parameters["kioskId"] = kioskId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            return reader.HasRows;
        }

        public bool delete(Int64 kioskId)
        {
            using (WSTransaction transaction = new WSTransaction("DELETE_KIOSK"))
            {
                this.deleteKioskLuckyItem(kioskId, transaction);
                this.deleteKioskOffer(kioskId, transaction);
                WSQuery Query = new WSQuery();
                Query.query = "DELETE FROM Kiosks WHERE id = @kioskId";
                Query.parameters["kioskId"] = kioskId;
                Query.transaction = transaction;
                if (!WSDatabaseManager.sharedDatabaseManagerinstance.executeDeleteQuery(Query))
                {
                    transaction.rollback();
                    return false;
                }
                transaction.commit();
                return true;
            }
        }

        public bool deleteKioskLuckyItem(Int64 kioskId, WSTransaction transaction)
        {
            WSQuery Query = new WSQuery();
            Query.query = "IF EXISTS (SELECT * FROM Kiosks_lucky_items WHERE kiosk_id = @kioskId) BEGIN DELETE FROM Kiosks_lucky_items WHERE kiosk_id = @kioskId END";
            Query.parameters["kioskId"] = kioskId;
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeDeleteQuery(Query);
        }

        public bool deleteKioskOffer(Int64 kioskId, WSTransaction transaction)
        {
            WSQuery Query = new WSQuery();
            Query.query = "IF EXISTS (SELECT * FROM Kiosks_Lucky_Items_Offers WHERE kiosk_id = @kioskId) BEGIN DELETE FROM Kiosks_Lucky_Items_Offers WHERE kiosk_id = @kioskId END";
            Query.parameters["kioskId"] = kioskId;
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeDeleteQuery(Query);
        }

        public bool changeKioskWorkableState(Int64 kioskId)
        {
            String query = "IF EXISTS(SELECT * FROM Kiosks where id = @kioskId and is_workable = 1) UPDATE Kiosks SET is_workable = 0, updated_on = @updatedOn where id= @kioskId ELSE UPDATE Kiosks SET is_workable = 1, updated_on = @updatedOn where id= @kioskId";
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = kioskId;
            Query.parameters["updatedOn"] = DateTime.UtcNow.ToString();
            bool result = WSDatabaseManager.sharedDatabaseManagerinstance.executeUpdateQuery(Query);
            return result;
        }

        public bool isWokable(Int64 kioskId)
        {
            String query = "SELECT CASE WHEN EXISTS (Select * from Kiosks where is_workable = 1 and id = @kioskId) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = kioskId;           
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }

        public bool settingAuthentication(User user)
        {
            String query = "SELECT CASE WHEN EXISTS (Select * from users join Kiosks on Kiosks.id = @kioskId join Stores on Stores.id = Kiosks.store_id where username = @username and password = @password and users.account_id = Stores.account_id) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = user.kiosk.id;
            Query.parameters["username"] = user.username;
            Query.parameters["password"] = user.password;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }

        public bool checkDefaultOffer(Int64 accountId)
        {
            bool memberDefault = isDefaultOffersSetForKiosk(accountId, 1);
            bool nonMemberDefault = isDefaultOffersSetForKiosk(accountId, 2);
            if (memberDefault && nonMemberDefault)
                return true;
            else
                return false;
        }

        public bool isDefaultOffersSetForKiosk(Int64 accountId, int offerType)
        {
            String query = "SELECT CASE WHEN EXISTS (Select * from default_offers where device_type = 2 and offer_type = @offerType and account_id = @accountId) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = accountId;
            Query.parameters["offerType"] = offerType;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }

        public bool updateOrAddUserKiosk(UserKiosk userKiosk)
        {
            String updateUserKiosk = "UPDATE users_kiosks SET count = count + 1, date = @date WHERE user_id = @userId AND kiosk_id = @kioskId AND offer_id = @offerId";
            String addUserKiosk = "INSERT INTO users_kiosks (user_id, kiosk_id, offer_id, date, is_registered, is_availed, count) VALUES (@userId, @kioskId, @offerId, @date, @isRegistered, @isAvailed, @seenCount)";
            String query = "IF EXISTS (SELECT * FROM users_kiosks WHERE user_id = @userId AND kiosk_id = @kioskId AND offer_id = @offerId) " + updateUserKiosk + " ELSE " + addUserKiosk;          
            WSQuery Query = new WSQuery(query);
            Query.parameters["userId"] = userKiosk.user_id;
            Query.parameters["kioskId"] = userKiosk.kiosk_id;
            Query.parameters["offerId"] = userKiosk.offer_id;
            Query.parameters["date"] = DateTime.UtcNow.ToString();
            Query.parameters["isRegistered"] = 0;
            Query.parameters["seenCount"] = 1;
            Query.parameters["isAvailed"] = 0;

            return (WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query));            
        }      

    }
}