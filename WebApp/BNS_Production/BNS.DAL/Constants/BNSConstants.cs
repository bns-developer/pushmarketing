﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNS.DAL.Constants
{
    public static class BNSConstants
    {
        public const int RELATED_TO_ENTITY_TYPE_ACCOUNT = 2;
        public const int RELATED_TO_ENTITY_TYPE_PROGRAM = 3;
        public const int RELATED_TO_ENTITY_TYPE_PROJECT = 4;
        public const string OFFER_ICON_FILES_BASE_PATH = "/Uploads/Offers/";
        public const string ACCOUNT_LOGO_FILES_BASE_PATH = "/Uploads/Logos/";        
    }
}