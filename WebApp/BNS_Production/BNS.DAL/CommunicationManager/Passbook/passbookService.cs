﻿using System;
using System.Collections.Generic;
using PassKitAPIWrapper;
using BNS.Models.Beacons;
using System.Web.Configuration;
using System.Web;

namespace BNS.DAL.CommunicationManager.Passbook
{
    public class passbookService
    {
        public passbookService()
        {
            
        }
        public String getPassForBeacon(Beacon beacon)
        {
            String uploadLogoId = getImageId(beacon);
            string template_name = WebConfigurationManager.AppSettings["templateName"].ToString();
            PassKit pk = new PassKit(passbookConfig.apiKey, passbookConfig.apiSecret);
            
            String shortUrl = beacon.shortUrl;                    
            Dictionary<string, string> data = new Dictionary<string, string>();           
            data["barcodeContent"] = beacon.shortUrl;
            data["beacon link"] = "<a href=" + beacon.shortUrl + ">Click here</a>";
            if(beacon.beacon_uuid != null)
            {
                data["beacon1name"] = beacon.beacon_uuid;
                data["beacon1uuid"] = beacon.beacon_uuid;
                data["beacon1beaconAlert"] = beacon.notification;
                
            }
            if(beacon.address != null && beacon.notification != null && beacon.latitude != 0 && beacon.longitude != 0)
            {
                data["location1address"] = beacon.address;
                data["location1locationAlert"] = beacon.notification;
                data["location1latitude"] = beacon.latitude.ToString();
                data["location1longitude"] = beacon.longitude.ToString();
            }

            if (uploadLogoId != null)
            {
                data["logoImage"] = uploadLogoId;
            }

            PassKitResponse result = pk.IssuePass(template_name, data);
            String passUrl = String.Empty;
            String passSerialNo = String.Empty;
            Dictionary<string, dynamic> response = result.response;
            if(response.ContainsKey("url") && response.ContainsKey("url"))
            {
                passUrl = response["url"];
            }
            return passUrl;
        }      

        public PassKitResponse getInfo()
        {
            string template_name = "MiamiBeachFinal";            
            PassKit pk = new PassKit(passbookConfig.apiKey, passbookConfig.apiSecret);     
            return pk.GetTemplateFieldNames(template_name);
            //return pk.GetPasses(template_name);
        }

        public String getImageId(Beacon beacon)
        {
            PassKit pk = new PassKit(passbookConfig.apiKey, passbookConfig.apiSecret);
            String image_path = HttpContext.Current.Server.MapPath("~" + beacon.createdBy.account.picUrl);
            String image_id = null;
            PassKitResponse uploadResult = pk.UploadImage(image_path, PassKitImageType.icon);
            if (uploadResult.response["success"])
            {
                image_id = (string)uploadResult.response["imageID"];
            }
            return image_id;
        }
    }
}