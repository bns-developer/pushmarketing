﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.SocialNetworkingServices;
using WSDatabase;
using System.Data.SqlClient;

namespace BNS.DAL.DALSocialNetworkingService
{
    public class DALSocialNetworkingServiceManager
    {
        public List<SocialNetworkingService> getAllServices()
        {
            WSQuery Query = new WSQuery();
            Query.query = QueryConstants.ALL_SERVICES;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);

            List<SocialNetworkingService> SocialNetworkingServices = new List<SocialNetworkingService>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    SocialNetworkingServices.Add(new SocialNetworkingService(reader));
                }
            }
            Query.connection.Close();
            return SocialNetworkingServices;
        }

        public List<SocialNetworkingService> getSelectedServices(Int64 accountId)
        {
            WSQuery Query = new WSQuery();
            Query.query = QueryConstants.SELECTED_SERVICES;
            Query.parameters["accountId"] = accountId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);

            List<SocialNetworkingService> SocialNetworkingServices = new List<SocialNetworkingService>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    SocialNetworkingServices.Add(new SocialNetworkingService(reader));
                }
            }
            Query.connection.Close();
            return SocialNetworkingServices;
        }

        public List<SocialNetworkingService> getUnselectedServices(Int64 accountId)
        {
            WSQuery Query = new WSQuery();
            Query.query = QueryConstants.UNSELECTED_SERVICES;
            Query.parameters["accountId"] = accountId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);

            List<SocialNetworkingService> SocialNetworkingServices = new List<SocialNetworkingService>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    SocialNetworkingServices.Add(new SocialNetworkingService(reader));
                }
            }
            Query.connection.Close();
            return SocialNetworkingServices;
        }

        public bool checkIfServiceExists(Int64 serviceId, Int64 accountId)
        {
            String query = "SELECT CASE WHEN EXISTS (Select * from accounts_social_networking_services where account_id = @accountId and social_networking_service_id = @serviceId) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = accountId;
            Query.parameters["serviceId"] = serviceId;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }

        public Int64 addSelectedService(Int64 serviceId, Int64 accountId)
        {
            String query = "INSERT INTO accounts_social_networking_services(account_id, social_networking_service_id) OUTPUT Inserted.id  VALUES(@accountId, @serviceId)";
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = accountId;
            Query.parameters["serviceId"] = serviceId;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQueryReturnID(Query);
        }

    }
}