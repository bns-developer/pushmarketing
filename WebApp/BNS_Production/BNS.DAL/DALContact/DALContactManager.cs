﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.DL.WSDatabase;
using System.Data.SqlClient;
using BNS.Models.Contacts;
using WSDatabase;

namespace BNS.DAL.DALContact
{
    public class DALContactManager
    {
        public bool checkIfEmailIdExists(String emailId, Int64 account)
        {
            String query = "Select CONTACT.id , CONTACT.value, NewUSER.id, NewUSER.contact_id, NewUSER.account_id from contacts CONTACT JOIN users NewUSER ON NewUSER.contact_id = CONTACT.id where CONTACT.value like '" + emailId + "' AND NewUSER.account_id = @accountId";
            WSQuery Query = new WSQuery(query);
            Query.parameters["emailId"] = emailId;
            Query.parameters["accountId"] = account;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            return reader.HasRows;           
        }

        public bool checkIfAccountEmailIdExixts(String emailId)
        {
            String query = "SELECT CASE WHEN EXISTS (Select * from contacts where value = @emailId) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["emailId"] = emailId;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }

        public bool verifyEmailId(String emailId)
        {
            String query = "SELECT CASE WHEN EXISTS (SELECT * FROM contacts CONTACT join users USR ON USR.contact_id = CONTACT.id WHERE CONTACT.value = @emailId AND USR.username IS NOT NULL AND USR.password IS NOT NULL) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["emailId"] = emailId;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }

        public Int64 saveContact(Contact contact, WSTransaction transaction)
        {
            String query = "";
            if (contact.country != null)
                query = "INSERT INTO contacts(country_id, type, value, created_on, updated_on) OUTPUT Inserted.id  VALUES(@countryId, @type, @value, @createdOn, @updatedOn)";                      
            else
                query = "INSERT INTO contacts(type, value, created_on, updated_on) OUTPUT Inserted.id  VALUES(@type, @value, @createdOn, @updatedOn)";

            WSQuery Query = new WSQuery(query,contact.toDictionary());
            Query.transaction = transaction;
            contact.id = WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQueryReturnID(Query);           
            return contact.id;          
        }        

        public Int64 saveAccountContact(Contact contact, WSTransaction transaction)
        {
            String query = "INSERT INTO contacts(type, value, created_on, updated_on) OUTPUT Inserted.id  VALUES(@type, @value, @createdOn, @updatedOn);";
            WSQuery Query = new WSQuery(query, contact.toDictionary());
            Query.transaction = transaction;
            contact.id = WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQueryReturnID(Query);
            return contact.id;
        }
        
    }
}