﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using BNS.Models.Accounts;
using BNS.DL.WSDatabase;
using BNS.DAL.Constants;
using System.Data.SqlClient;
using WSDatabase;
using System.IO;
using BNS.DAL.DALUser;
using BNS.DAL.DALCategory;
using BNS.DAL.DALLuckyItem;
using BNS.DAL.DALContact;
using BNS.Models.Users;
using System.Net.Mail;
using System.Net;
using System.Dynamic;
using System.Web.Configuration;
using RazorEngine;
using RazorEngine.Templating;
using BNS.DAL.CommunicationManager.Email;
using BNS.Models.Contacts;

namespace BNS.DAL.DALAccount
{
    public class DALAccountManager
    {
        public DALCategoryManager categoryManager = new DALCategoryManager();
        public DALLuckyItemManager luckyItemManager = new DALLuckyItemManager();

        public Int64 addAccount(Account account)
        {
            using (WSTransaction transaction = new WSTransaction("SAVE_ACCOUNT"))
            {
                account.picUrl = getPicUrl(account);
                String query = "INSERT INTO Accounts(name, pic_url, created_on, updated_on) OUTPUT Inserted.id VALUES(@name, @picUrl, @createdOn, @updatedOn)";
                WSQuery Query = new WSQuery(query, account.toDictionary(), transaction);
                account.id = WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQueryReturnID(Query);
                if (account.id != 0)
                {
                    account.admin.contact.id = new DALContactManager().saveAccountContact(account.admin.contact, transaction);
                    account.contact.id = new DALContactManager().saveAccountContact(account.contact, transaction);
                    account.admin.id = new DALUserManager().creatUser(account, transaction);                                 
                   if (account.admin.id != 0 && account.admin.contact.id != 0 && account.contact.id != 0)
                    {
                        new DALUserManager().saveUserContacts(account.admin, transaction);
                        saveAccountContact(account, transaction);
                        if (categoryManager.setCategoriesForNewAccount(account, transaction) && luckyItemManager.setLuckyItemsForNewAccount(account, transaction))
                        {
                            sendNewAccountEmail(account);
                            transaction.commit();
                            return account.id;
                        }
                        else
                        {
                            transaction.rollback();
                            return 0;
                        }
                    }
                    else
                    {
                        transaction.rollback();
                        return 0;
                    }
                }
                else
                {
                    transaction.rollback();
                    return 0;
                }
            }
        }

        public bool saveAccountContact(Account account, WSTransaction transaction)
        {
            String query = "INSERT INTO accounts_contacts (account_id, contact_id, type, created_on, updated_on) VALUES(@accountId, @contactId, @type, @createdOn, @updatedOn)";
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = account.id;
            Query.parameters["contactId"] = account.contact.id;
            Query.parameters["type"] = (account.contact.ContactType == Contact.Contact_Type.WEBSITE ? 3 : (account.contact.ContactType == Contact.Contact_Type.FACEBOOK ? 4 : 5));
            Query.parameters["updatedOn"] = DateTime.UtcNow.ToString();
            Query.parameters["createdOn"] = DateTime.UtcNow.ToString();
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);
        }

        private string PopulateBodyForNewAccountEmail(Account account)
        {
            dynamic model = new ExpandoObject();
            model.picUrl = WebConfigurationManager.AppSettings["IMAGE_PATH"].ToString() + account.picUrl;
            model.sender = account.name;     
                                                                 
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("")))
            {
                body = reader.ReadToEnd();
            }
            body = Engine.Razor.RunCompile(body, new Random().Next(100, 10000).ToString(), null, (object)model); //rendering the template with our model
           
            return body;
        }

        public void sendNewAccountEmail(Account account)
        {            
            bool enableSSL = false;         
            string emailTo = account.admin.contact.value;
            string subject = "Welcome To BNS";

            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress(EmailConfig.emailFromSupport, EmailConfig.SupportDisplayName);
                mail.To.Add(emailTo);
                mail.Subject = subject;
                //mail.Body = PopulateBodyForNewAccountEmail(account);
                mail.Body = "New Account Created";              
                mail.IsBodyHtml = true;

                using (SmtpClient smtp = new SmtpClient(EmailConfig.smtpAddress, EmailConfig.portNumber))
                {
                    smtp.Credentials = new NetworkCredential(EmailConfig.emailFromSupport, EmailConfig.supportPassword);
                    smtp.EnableSsl = enableSSL;
                    smtp.Send(mail);
                }
            }
        }

        public String getPicUrl(Account account)
        {
            if (account.logo != null)
            {
                String logoName = account.name;
                logoName = logoName.Replace(' ', '_');
                String fileExtension = System.IO.Path.GetExtension(account.logo.FileName);
                String fileName = logoName + "_" + DateTime.UtcNow.ToString().Replace("/", "_").Replace(":", "_").Replace(" ", "_") + "_"; 
                account.logo.SaveAs(HttpContext.Current.Server.MapPath("~" + BNSConstants.ACCOUNT_LOGO_FILES_BASE_PATH) + fileName + fileExtension);
                account.picUrl = BNSConstants.ACCOUNT_LOGO_FILES_BASE_PATH + fileName + fileExtension;
                return account.picUrl;
            }
            else
            {
                return null;
            }
        }

        public Account getDetails(Account account)
        {
            String query = QueryConstants.ACCOUNT_DETAILS;
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = account.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    account = new Account(reader);
                }
            }
            reader.Close();
            Query.connection.Close();

            account.accountContacts = getCantactsForAccount(account.id);
            return account;
        }

        public List<Contact> getCantactsForAccount(Int64 accountId)
        {
            String query = QueryConstants.CONTACTS_FOR_ACCOUNT;
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = accountId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Contact> contactsList = new List<Contact>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    contactsList.Add(new Contact(reader));
                }
            }
            reader.Close();
            Query.connection.Close();
            return contactsList;
        }

        public Int64 isValidUser(String username, String password)
        {
            Dictionary<String, Object> parameters = new Dictionary<String, Object>();
            parameters["username"] = username;
            parameters["password"] = password;

            if (username.Equals("novomatic@gmail.com") && password.Equals("pass@novomatic"))
            {
                return 101;
            }
            else
            {
                return 0;
            }

            //String query = "SELECT * FROM Users Where email = @username AND password= @password;";
            //System.Data.SqlClient.SqlDataReader reader = DatabaseManager.sharedDatabaseManagerinstance.getRecordsFromDatabase(query, parameters);
            //if (reader.HasRows)
            //{
            //    Int64 user = 0;
            //    while (reader.Read())
            //    {
            //        user = reader.GetInt64(reader.GetOrdinal("id"));
            //    }
            //    Session session = new Session();
            //    session.user = user;
            //    SessionManager.sharedInstance.save(session);
            //    return user;
            //}
            //else

            //{
            //    return 0;
            //}
        }

        public bool update(Account account)
        {
            using (WSTransaction transaction = new WSTransaction("UPDATE_ACCOUNT"))
            {
                String path = (HttpContext.Current.Server.MapPath("~"));               
                path = path.Replace("\\", "/");
                Account oldAccount = getDetails(account);
                account.picUrl = account.logo != null ? getPicUrl(account) : oldAccount.picUrl;
                if(account.picUrl != oldAccount.picUrl)
                {
                    if (File.Exists(path + oldAccount.picUrl))
                    {
                        File.Delete(path + oldAccount.picUrl);
                    }
                }
                account.updatedOn = DateTime.UtcNow.ToString();

                String query = QueryConstants.ACCOUNT_UPDATE;
                WSQuery Query = new WSQuery(query);
                Query.parameters["name"] = account.name;
                Query.parameters["picUrl"] = account.picUrl;
                Query.parameters["accountId"] = account.id;
                Query.parameters["updatedOn"] = account.updatedOn;
                Query.transaction = transaction;
                if (!WSDatabaseManager.sharedDatabaseManagerinstance.executeUpdateQuery(Query))
                {
                    transaction.rollback();
                    return false;
                }
                else
                {
                    if(account.accountContacts.Count != 0)
                    {
                        foreach(Contact contact in account.accountContacts)
                        {
                            Int64 contactId = updateContactForAccount(account.id, contact, transaction);
                            if(contactId != 0)
                            {
                                contact.id = contactId;
                                Int64 accountContactId = updateAccountContact(account.id, contact, transaction);                               
                            }                                                       
                        }                        
                    }
                    else
                    {
                        transaction.rollback();
                        return false;
                    }
                    transaction.commit();
                    return true;
                }              
            }
        }

        public Int64 updateContactForAccount(Int64 accountId, Contact contact, WSTransaction transaction)
        {
            String checkIfExists = "(select * from accounts_contacts join contacts on contacts.id = accounts_contacts.contact_id where accounts_contacts.type = @type and accounts_contacts.account_id = @accountId)";
            String updateContact = " UPDATE contacts  SET value = @value OUTPUT Inserted.id where id = (Select contacts.id from accounts_contacts join contacts on contacts.id = accounts_contacts.contact_id where accounts_contacts.type = @type and accounts_contacts.account_id = @accountId)";
            String insertContact = "INSERT INTO contacts(type, value, created_on, updated_on) OUTPUT Inserted.id  VALUES(@type, @value, @createdOn, @updatedOn)";
            String query = "IF EXISTS " + checkIfExists + updateContact + " ELSE " + insertContact;
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = accountId;
            Query.parameters["type"] = (contact.ContactType == Contact.Contact_Type.WEBSITE ? 3 : (contact.ContactType == Contact.Contact_Type.FACEBOOK ? 4 : 5));
            Query.parameters["value"] = contact.value;      
            Query.parameters["createdOn"] = DateTime.UtcNow.ToString();
            Query.parameters["updatedOn"] = DateTime.UtcNow.ToString();          
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQueryReturnID(Query);            
        }
  
        public Int64 updateAccountContact(Int64 accountId, Contact contact, WSTransaction transaction)
        {
            String checkAccountContact = "(Select * from accounts_contacts where accounts_contacts.type = @type and accounts_contacts.account_id = @accountId and accounts_contacts.contact_id = @contactId) ";           
            String insertAaccountContact = " INSERT INTO accounts_contacts (account_id, contact_id, type, created_on, updated_on) OUTPUT Inserted.id VALUES(@accountId, @contactId, @type, @createdOn, @updatedOn)";
            String query = "IF NOT EXISTS " + checkAccountContact + insertAaccountContact;
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = accountId;
            Query.parameters["contactId"] = contact.id;
            Query.parameters["type"] = (contact.ContactType == Contact.Contact_Type.WEBSITE ? 3 : (contact.ContactType == Contact.Contact_Type.FACEBOOK ? 4 : 5));     
            Query.parameters["createdOn"] = DateTime.UtcNow.ToString();
            Query.parameters["updatedOn"] = DateTime.UtcNow.ToString();
            Query.transaction = transaction;
           Int64 accountContactId = WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQueryReturnID(Query);
            return accountContactId;
        }

        public bool logout(HttpRequestMessage request)
        {
            //return SessionManager.sharedInstance.clearSession(request);
            return true;
        }

        private string PopulateBodyForForgotPasswordEmail(User user)
        {
            dynamic model = new ExpandoObject();
            model.picUrl = WebConfigurationManager.AppSettings["IMAGE_PATH"].ToString() + user.account.picUrl;
            model.sender = user.account.name;

            string body = string.Empty;
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("")))
            {
                body = reader.ReadToEnd();
            }
            body = Engine.Razor.RunCompile(body, new Random().Next(100, 10000).ToString(), null, (object)model); //rendering the template with our model

            return body;
        }

        public bool sendForgotPasswordEmail(User user)
        {
            user = getPassword(user);
            if (user.password != null || user.username != null)
            {               
                bool enableSSL = false;                
                string emailTo = user.contact.value;
                string subject = "Account Credentials";

                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress(EmailConfig.emailFromSupport, EmailConfig.SupportDisplayName);
                    mail.To.Add(emailTo);
                    mail.Subject = subject;
                    //mail.Body = PopulateBodyForForgotPasswordEmail(user);
                    mail.Body = "Username : " + user.username + "\n Password : " + user.password;
                    mail.IsBodyHtml = true;

                    using (SmtpClient smtp = new SmtpClient(EmailConfig.smtpAddress, EmailConfig.portNumber))
                    {
                        smtp.Credentials = new NetworkCredential(EmailConfig.emailFromSupport, EmailConfig.supportPassword);
                        smtp.EnableSsl = enableSSL;
                        smtp.Send(mail);
                        return true;
                    }
                }
            }
            else
            {
                return false;
            }           
        }

        public User getPassword(User user)
        {
            String query = QueryConstants.GET_ACCOUNT_PASSWORD;
            WSQuery Query = new WSQuery(query);
            Query.parameters["emailId"] = user.contact.value;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    user.username = reader.GetString(reader.GetOrdinal("USER_NAME"));
                    user.password = reader.GetString(reader.GetOrdinal("PASSWORD"));
                }
            }
            Query.connection.Close();
            return user;
        }

        public bool verifyOldPassword(User user)
        {
            String query = "SELECT CASE WHEN EXISTS (SELECT * FROM users USR WHERE USR.id = @userId AND USR.account_id = @accountId AND USR.password = @oldPassword) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["userId"] = user.id;
            Query.parameters["accountId"] = user.account.id;
            Query.parameters["oldPassword"] = user.password;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }

        public bool resetPassword(User user, String newPassword)
        {
            String query = "UPDATE users SET password = @newPassword, updated_on = @updatedOn WHERE id = @userId AND account_id = @accountId";
            WSQuery Query = new WSQuery(query);
            Query.parameters["userId"] = user.id;
            Query.parameters["accountId"] = user.account.id;
            Query.parameters["newPassword"] = newPassword;
            Query.parameters["updatedOn"] = DateTime.UtcNow.ToString();
            if (WSDatabaseManager.sharedDatabaseManagerinstance.executeUpdateQuery(Query))
            {
                user = getAccountContact(user);
                sendResetPasswordEmail(user);
                return true;
            }
            else
            {
                return false;
            }           
        }

        User getAccountContact(User user)
        {
            String query = "SELECT CONTACT.value AS EMAIL FROM users USR JOIN contacts CONTACT ON USR.contact_id = CONTACT.id WHERE USR.id = @userId";
            WSQuery Query = new WSQuery(query);
            Query.parameters["userId"] = user.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    user.contact.value = reader.GetString(reader.GetOrdinal("EMAIL"));
                }
            }
            Query.connection.Close();
            return user;
        }

        private string PopulateBodyForResetPasswordEmail(User user)
        {
            dynamic model = new ExpandoObject();
            model.picUrl = WebConfigurationManager.AppSettings["IMAGE_PATH"].ToString() + user.account.picUrl;
            model.sender = user.account.name;

            string body = string.Empty;
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("")))
            {
                body = reader.ReadToEnd();
            }
            body = Engine.Razor.RunCompile(body, new Random().Next(100, 10000).ToString(), null, (object)model); //rendering the template with our model

            return body;
        }

        public void sendResetPasswordEmail(User user)
        {                   
            bool enableSSL = false;           
            string emailTo = user.contact.value;
            string subject = "Account Password Reset";

            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress(EmailConfig.emailFromSupport);
                mail.To.Add(emailTo);
                mail.Subject = subject;
                //mail.Body = PopulateBodyForResetPasswordEmail(user);
                mail.Body = "Account password has been reset.";
                mail.IsBodyHtml = true;

                using (SmtpClient smtp = new SmtpClient(EmailConfig.smtpAddress, EmailConfig.portNumber))
                {
                    smtp.Credentials = new NetworkCredential(EmailConfig.emailFromSupport, EmailConfig.supportPassword);
                    smtp.EnableSsl = enableSSL;
                    smtp.Send(mail);
                }
            }
        }

    }
}
        