﻿using BNS.DAL.DALBeacon;
using BNS.DAL.DALOffer;
using BNS.Models.Offers;
using BNS.Models.Users;
using BNS.Models.UsersBeacons;
using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BNS.RE
{
    public class RuleEngine
    {
        DALOfferManager DALmanager = new DALOfferManager();
        DALBeaconManager beaconManager = new DALBeaconManager();

        public Offer ruleEngine(User user, Int64 beaconId, Int64 typeId, Int64 accountId, String beaconGuid)
        {           
            Offer offer = new Offer();
            List<Offer> offersList = new List<Offer>();
            offersList = DALmanager.getActiveBeaconOffers(typeId, beaconId, accountId);
            if(offersList.Count != 0)
            {
                offer = DALmanager.getOffersForBeacon(beaconId, typeId, user.id, accountId);
                if(offer.id == 0)
                {
                    offer = DALmanager.getCurrentActiveOfferForBeacon(beaconGuid,typeId, accountId);
                }
                //offer = offersList[0];
            }
            else
            {
                offer = DALmanager.getCurrentActiveOfferForBeacon(beaconGuid, typeId, accountId);
            }

            UsersBeacons userBeacon = new UsersBeacons();

            userBeacon = beaconManager.getUserBeacon(user.id, beaconId, offer.id); 
            if(userBeacon.user_id == 0)
            {
                bool add = beaconManager.addUserBeacons(user.id, beaconId, offer.id);
            }
            else
            {
                beaconManager.updateUserBeacon(userBeacon);
            }

            return offer;
        }
    }
}
