﻿using System;
using System.Web;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BNS.DAL.DALOffer;
using BNS.DAL.DALKiosk;
using BNS.Models.Offers;
using BNS.Models.Users;
using BNS.Models.UsersKiosks;

namespace BNS.RE
{
    public class KioskRuleEngine
    {
        DALOfferManager DALmanager = new DALOfferManager();
        DALKioskManager kioskManager = new DALKioskManager();

        public Offer RuleEngine(UserKiosk userKiosk)
        {
            Offer offer = new Offer();
            List<Offer> offersList = new List<Offer>();
            offersList = DALmanager.getLuckyItemActiveOffers(userKiosk.kiosk_id, userKiosk.lucky_item_id);
            if (offersList.Count != 0)
            {
                offer = DALmanager.getKioskOfferForUser(userKiosk);
                if (offer.id == 0)
                {
                    offer = DALmanager.getLuckyItemCurrentActiveOfferForKiosk(userKiosk);
                    userKiosk.offer_id = offer.id;
                }
                else
                {
                    userKiosk.offer_id = offer.id;
                }
            }
            else
            {
                offer = DALmanager.getLuckyItemCurrentActiveOfferForKiosk(userKiosk);
                userKiosk.offer_id = offer.id;
            }

            kioskManager.updateOrAddUserKiosk(userKiosk);

            return offer;
        }                 

    }
}
