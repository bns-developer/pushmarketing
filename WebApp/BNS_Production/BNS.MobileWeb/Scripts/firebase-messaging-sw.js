﻿    importScripts('https://www.gstatic.com/firebasejs/4.3.0/firebase-app.js');
    importScripts('https://www.gstatic.com/firebasejs/4.3.0/firebase-messaging.js');

var config = {
    apiKey: "AIzaSyCYePqaAIoLDvjcCjSifpFbpePGNOzBb_w",
    authDomain: "best-network-systems.firebaseapp.com",
    databaseURL: "https://best-network-systems.firebaseio.com",
    projectId: "best-network-systems",
    storageBucket: "best-network-systems.appspot.com",
    messagingSenderId: "509813089928"
};

if (!firebase.apps.length) {
    firebase.initializeApp(config);
}
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {

    const title = payload.data.title;

    const options = {
        body: payload.data.body
    };

    return self.registration.showNotification(title, options)

})