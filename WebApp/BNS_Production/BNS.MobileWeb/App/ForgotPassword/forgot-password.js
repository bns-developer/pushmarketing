angular.module('bns.forgotPassword', [])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
    .state('forgotPassword', {
        url: "/forgot-password",
        views: {
            "forgotPassword": {
                templateUrl: "App/ForgotPassword/forgot-password.html",
                controller: 'forgotPasswordCtrl'
            },
        },
    });
}])
.controller('forgotPasswordCtrl', ['$state', '$scope', '$rootScope', function ($state, $scope, $rootScope) {

    }])
