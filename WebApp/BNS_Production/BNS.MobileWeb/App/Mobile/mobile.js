angular.module('bns.mobile', [
    'bns.mobile.landing',
    'bns.mobile.viewOffer',
    'bns.mobile.getUserDetail',
    'bns.mobile.getMemberID',
    'bns.mobile.congoNewUser',
    'bns.mobile.congoUser',
    'bns.mobile.viewBeaconOffer'
    ])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
    .state('mobile', {
        url: "",
        abstract: true,
        views: {
            "mobile": {
                templateUrl: "App/Mobile/mobile.html",
                controller: 'mobileCtrl'
            },
        },
    });
}])
.factory('TimerService',function($state){
  return {
    expires:null,
    counter:null,
    countDownTimer: function(){
        var timeDiff = expires - new Date();
        if (timeDiff <= 0) {
            clearInterval(counter);
            counter = false;
            $(".CountDownTimer").text("00:00");
            $state.go("mobile.landing");
            return;
        }
        var minutes = Math.floor(timeDiff / 60000);
        var seconds = ((timeDiff % 60000) / 1000).toFixed(0);
        var milliSeconds = (new Date(timeDiff).getMilliseconds()).toFixed(0);
        var minutes = minutes < 10 ? "0" + minutes: minutes;
        var seconds = seconds < 10 ? "0" + seconds: seconds;
        if(milliSeconds < 100)
            milliSeconds = "0" + milliSeconds;
        else if(milliSeconds < 10)
            milliSeconds = "000" + milliSeconds;
        $(".CountDownTimer").text("Your offer expires in " + minutes + ":" + seconds + " minutes");
    },
    initializeTimer: function(minutes){
      expires = new Date(new Date().getTime() + minutes * 60000) 
      counter = setInterval(this.countDownTimer, 1);
    },
    stopTimer: function(){
      if(counter != null && counter != undefined){
          clearInterval(counter);
          counter = false;
          $(".CountDownTimer").text("00:00");
          return;
      }
    },
    isRunning:function(){
      if (typeof counter !== 'undefined') {
        return counter;
      }
      else
      {
        return false;
      }
      
    }


  };
})
.run(['$rootScope', '$state','TimerService', function ($rootScope, $state,TimerService) {

    //$rootScope.$on('$stateChangeStart',
    //function (event, toState, toParams, fromState, fromParams) {
    //  $state.current.name;
    //  if (toState.name != "mobile.getUserDetail" && toState.name != "mobile.getMemberID" && TimerService.isRunning()) {
    //      var r = confirm("Offer will be discarded. Are you sure you want to continue?");
    //      if (r == true) {
    //        event.preventDefault();
    //        TimerService.stopTimer();
    //        $state.go("mobile.landing");
    //      }
    //      else
    //      {
    //        event.preventDefault();
    //      }
    //    }
    //})
}])
.controller('mobileCtrl', ['$state', '$scope', '$rootScope', function ($state, $scope, $rootScope) {

    //var config = {
    //    apiKey: "AIzaSyCYePqaAIoLDvjcCjSifpFbpePGNOzBb_w",
    //    authDomain: "best-network-systems.firebaseapp.com",
    //    databaseURL: "https://best-network-systems.firebaseio.com",
    //    projectId: "best-network-systems",
    //    storageBucket: "best-network-systems.appspot.com",
    //    messagingSenderId: "509813089928"
    //};

    //firebase.initializeApp(config);

    //const messaging = firebase.messaging();

    //if (Notification.permission == 'granted') {
    //    alert('granted');
    //} else if (Notification.permission == 'denied') {
    //    alert('denied');
    //} else if (Notification.permission == 'prompt') {
    //    alert('prompt');
    //}
    //messaging.requestPermission()
    //.then(function () {
    //    console.log("Request Granted");
    //    return messaging.getToken()
    //    .then(function (token) {
    //        console.log(token);
    //    });
    //})
    //.catch(function (err) {
    //    console.log("Request Not Granted :", err);
    //})

    //messaging.onMessage(function (payload) {
    //    console.log('On message', payload);
    //});

    $scope.currentDate = new Date();
    // Check if site is opened in mobile browser
    window.mobilecheck = function() {
      var check = false;
      (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
      return check;
  };

  if(!window.mobilecheck()){
      alert("This link can be accessed only from mobile browser!");
      return false;
  }

}])
