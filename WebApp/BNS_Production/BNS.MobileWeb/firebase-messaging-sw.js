﻿    importScripts('https://www.gstatic.com/firebasejs/4.3.0/firebase-app.js');
    importScripts('https://www.gstatic.com/firebasejs/4.3.0/firebase-messaging.js');

var config = {
    apiKey: "AIzaSyCYePqaAIoLDvjcCjSifpFbpePGNOzBb_w",
    authDomain: "best-network-systems.firebaseapp.com",
    databaseURL: "https://best-network-systems.firebaseio.com",
    projectId: "best-network-systems",
    storageBucket: "best-network-systems.appspot.com",
    messagingSenderId: "509813089928"
};

if (!firebase.apps.length) {
    firebase.initializeApp(config);
}
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {

    const title = payload.data.title;
   
    const options = {
        subtitle: payload.data.subtitle,
        body: payload.data.body,        
        icon: payload.data.icon,
        badge: payload.data.badge,
        image: payload.data.image,
        requireInteraction: true
    };

    return self.registration.showNotification(title, options)
})

self.addEventListener('notificationclick', function (event) {
    const clickedNotification = event.notification;
    clickedNotification.close();
    // Do something as the result of the notification click
    const examplePage = "https://offer.bestnetworksystems.com/beacon/landing";
    const promiseChain = clients.openWindow(examplePage);
    //const promiseChain = doSomething();
    event.waitUntil(promiseChain);
})