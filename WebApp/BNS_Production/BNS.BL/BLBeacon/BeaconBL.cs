﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.DAL.DALBeacon;
using BNS.Models.Beacons;
namespace BNS.BL.BLBeacon
{
    public class BeaconBL
    {
        DALBeaconManager manager = new DALBeaconManager();
        public bool save(Beacon beacon)
        {
            return manager.saveBeacon(beacon);
        }

    }
}