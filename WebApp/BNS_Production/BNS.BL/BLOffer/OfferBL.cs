﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.DAL.DALOffer;
using BNS.Models.Offers;
using Newtonsoft.Json.Linq;
using BNS.Models.Countries;
using BNS.Models.Categories;
using BNS.Models.Kiosks;
using BNS.Models.Accounts;
using BNS.DAL.Constants;
using System.Data;
using System.Data.OleDb;
using static BNS.Models.Offers.Offer;
using BNS.DAL.DALCategory;
using BNS.Models.TermsAndConditions;
using BNS.DAL.DALTermsAndConditions;
using BNS.Models.Users;
using BNS.DAL.DALCountry;

namespace BNS.BL.BLOffer
{
    public class OfferBL
    {
        DALCategoryManager categoryManager = new DALCategoryManager();
        DALOfferManager manager = new DALOfferManager();
        DALOfferManager DALmanager = new DALOfferManager();
        DALTermsAndConditions termsAndConditionsManager = new DALTermsAndConditions();
        DALCountryManager countryManager = new DALCountryManager();

        public bool save(Offer offer)
        {
           return manager.saveOffer(offer);                        
        }

        public bool saveOfferForLuckyItem(Int64 kioskId, Int64 luckyItemId, Offer offer)
        {
            return manager.saveKioskLuckyItemOffer(kioskId, luckyItemId, offer);
        }

        public bool saveOfferForBeacon(Int64 beaconId, Offer offer)
        {
            return manager.saveBeaconOffer(beaconId, offer);
        }

        public Dictionary<string,int> uploadOffers(HttpFileCollection files,User requestUser)
        {
            Dictionary<string, int> result = new Dictionary<string, int>();
            int recordsInserted = 0;
            int recordsFailed = 0;

            if (files != null)
            {
                foreach (string fileName in files)
                {
                    HttpPostedFile excelFile = files[fileName];
                    String fileExtension = System.IO.Path.GetExtension(excelFile.FileName);
                    String uploadFileName = "ExcelToRead" + DateTime.UtcNow.ToString().Replace("/", "_").Replace(":", "_").Replace(" ", "_");
                    excelFile.SaveAs(HttpContext.Current.Server.MapPath("~" + BNSConstants.OFFER_ICON_FILES_BASE_PATH) + uploadFileName + fileExtension);
                    String FilePath = HttpContext.Current.Server.MapPath("~" + BNSConstants.OFFER_ICON_FILES_BASE_PATH) + uploadFileName + fileExtension;
                    string excelConnString;

                    if (fileExtension != ".xls" && fileExtension != ".xlsx")
                    {
                        result["Status"] = -2;
                        return result;
                    }

                    if (fileExtension == ".xls")
                    {
                        excelConnString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR=yes'", FilePath);
                    }
                    else
                    {
                        excelConnString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0 Xml;HDR=yes'", FilePath);
                    }

                    DataSet dataset = new DataSet();
                    DataTable dataTable = new DataTable();

                    using (OleDbConnection connection = new OleDbConnection(excelConnString))
                    {
                        OleDbCommand oleDbcommand = new OleDbCommand("Select * FROM [Sheet1$]", connection);

                        OleDbDataAdapter oleDbDataAdapter = new OleDbDataAdapter();
                        oleDbDataAdapter.SelectCommand = oleDbcommand;
                        oleDbDataAdapter.Fill(dataTable);

                        foreach (DataColumn column in dataTable.Columns)
                            column.ColumnName = column.ColumnName.Replace(" ", "").ToLower();

                        int typeIndex = dataTable.Columns.IndexOf("OfferType");
                        if (typeIndex == -1)
                        {
                            result["Status"] = -1;
                            return result;
                        }

                        int countriesIndex = dataTable.Columns.IndexOf("Countries");
                        if (countriesIndex == -1)
                        {
                            result["Status"] = -1;
                            return result;
                        }

                        int categoriesIndex = dataTable.Columns.IndexOf("Categories");
                        if (categoriesIndex == -1)
                        {
                            result["Status"] = -1;
                            return result;
                        }

                        int offerNumberIndex = dataTable.Columns.IndexOf("OfferNumber");
                        if (offerNumberIndex == -1)
                        {
                            result["Status"] = -1;
                            return result;
                        }

                        int titleIndex = dataTable.Columns.IndexOf("Title");
                        if (titleIndex == -1)
                        {
                            result["Status"] = -1;
                            return result;
                        }

                        int descriptionIndex = dataTable.Columns.IndexOf("Description");
                        if (descriptionIndex == -1)
                        {
                            result["Status"] = -1;
                            return result;
                        }

                        int fromDateIndex = dataTable.Columns.IndexOf("FromDate");
                        if (fromDateIndex == -1)
                        {
                            result["Status"] = -1;
                            return result;
                        }

                        int toDateIndex = dataTable.Columns.IndexOf("ToDate");
                        if (toDateIndex == -1)
                        {
                            result["Status"] = -1;
                            return result;
                        }

                        int offerDurationIndex = dataTable.Columns.IndexOf("OfferDuration");
                        if (offerDurationIndex == -1)
                        {
                            result["Status"] = -1;
                            return result;
                        }

                        int termsAndConditionsIndex = dataTable.Columns.IndexOf("TermsAndConditions");
                        if (termsAndConditionsIndex == -1)
                        {
                            result["Status"] = -1;
                            return result;
                        }

                        int valueIndex = dataTable.Columns.IndexOf("Value");
                        if (valueIndex == -1)
                        {
                            result["Status"] = -1;
                            return result;
                        }

                        foreach (DataRow row in dataTable.Rows)
                        {
                            Offer offer = new Offer();
                            offer.account = requestUser.account;
                            int skip = 0;

                            if (row[typeIndex].ToString() == "Both")
                            {
                                offer.OfferType = Offer_Type.BOTH;
                            }
                            else if (row[typeIndex].ToString() == "Member")
                            {
                                offer.OfferType = Offer_Type.MEMBER;
                            }
                            else if (row[typeIndex].ToString() == "Non-Member")
                            {
                                offer.OfferType = Offer_Type.NON_MEMBER;
                            }
                            else
                            {
                                skip = 1;
                            }

                            List<Category> categories = new List<Category>();
                            String categoriesString = row[categoriesIndex].ToString().Replace(" ", "");
                            String valuesString = row[valueIndex].ToString().Replace(" ", "");
                            char[] delimiter = { ',' };
                            string[] categoriesName = categoriesString.Split(delimiter);
                            string[] values = valuesString.Split(delimiter);
                            if (categoriesName.Length == values.Length)
                            {
                                for (int i = 0; i < categoriesName.Length; i++)
                                {
                                    Category category = new Category();
                                    category.id = categoryManager.getIdFromName(categoriesName[i], requestUser.account);
                                    if (category.id == 0)
                                    {
                                        skip = 1;
                                    }
                                    else
                                    {
                                        category.value = values[i].Replace("'","");
                                        categories.Add(category);
                                    }
                                }
                                offer.categories = categories;
                            }
                            else
                            {
                                skip = 1;
                            }

                            List<Country> countries = new List<Country>();
                            String countriesString = row[countriesIndex].ToString().Replace(" ", "");
                            string[] countriesName = countriesString.Split(delimiter);
                            foreach (string countryName in countriesName)
                            {
                                Country country = new Country();
                                country = countryManager.getIdFromName(countryName);
                                if (country.id == 0)
                                {
                                    skip = 1;
                                }
                                else
                                {
                                    if (countries.Count() > 0)
                                    {
                                        Country temp = new Country();
                                        temp = countries.First<Country>();
                                        if(temp.currency != country.currency)
                                        {
                                            skip = 1;
                                        }
                                        else
                                        {
                                            countries.Add(country);
                                        }                                       
                                    }
                                    else
                                    {
                                        countries.Add(country);
                                    }
                                }
                            }
                            offer.countries = countries;

                            string offerNumber = row[offerNumberIndex].ToString();
                            bool offerNumberExists = DALmanager.checkIfNumberExists(offerNumber, requestUser.account.id);
                            if (offerNumberExists)
                            {
                                skip = 1;
                            }
                            else
                            {
                                offer.number = offerNumber;
                            }

                            if (row[titleIndex].ToString() != "" || row[titleIndex].ToString() != null)
                            {
                                offer.title = row[titleIndex].ToString();
                            }
                            else
                            {
                                skip = 1;
                            }

                            if (row[descriptionIndex].ToString() != "" || row[descriptionIndex].ToString() != null)
                            {
                                offer.description = row[descriptionIndex].ToString();
                            }
                            else
                            {
                                skip = 1;
                            }

                            if (row[fromDateIndex].ToString() != "" || row[fromDateIndex].ToString() != null)
                            {
                                offer.startTime = row[fromDateIndex].ToString();
                            }
                            else
                            {
                                skip = 1;
                            }

                            if (row[toDateIndex].ToString() != "" || row[toDateIndex].ToString() != null)
                            {
                                offer.endTime = row[toDateIndex].ToString();
                            }
                            else
                            {
                                skip = 1;
                            }

                            if (DateTime.Parse(offer.startTime) > DateTime.Parse(offer.endTime))
                            {
                                skip = 1;
                            }

                            if (row[offerDurationIndex].ToString() != "" || row[offerDurationIndex].ToString() != null)
                            {
                                string durationString = row[offerDurationIndex].ToString();
                                offer.duration = Convert.ToInt64(durationString);
                            }
                            else
                            {
                                skip = 1;
                            }

                            List<TermsAndCondition> termsAndConditions = new List<TermsAndCondition>();
                            String termsAndConditionsString = row[termsAndConditionsIndex].ToString();
                            string[] termAndConditionsName = termsAndConditionsString.Split(delimiter);
                            foreach (string termAndConditionName in termAndConditionsName)
                            {
                                TermsAndCondition termAndCondition = new TermsAndCondition();
                                if (termAndConditionName != "" || termAndConditionName != null)
                                {
                                    termAndCondition.id = termsAndConditionsManager.getIdFromName(termAndConditionName, requestUser.account);
                                    if (termAndCondition.id == 0)
                                    {
                                        skip = 1;
                                    }
                                    else
                                    {
                                        termsAndConditions.Add(termAndCondition);
                                    }
                                }
                                else
                                {
                                    skip = 1;
                                }
                            }
                            offer.termsAndConditions = termsAndConditions;
                            offer.createdBy = requestUser;
                            offer.updatedBy = requestUser;
                            offer.imagePath = "No Image";
                            if (skip == 1)
                            {
                                recordsFailed = recordsFailed + 1;
                            }
                            else
                            {
                                if (manager.saveOffer(offer))
                                {
                                    recordsInserted = recordsInserted + 1;
                                }
                                else
                                {
                                    recordsFailed = recordsFailed + 1;
                                }
                            }
                        }
                        connection.Close();
                    }

                    System.IO.File.Delete(FilePath);
                }
            }
            result["Status"] = 1;
            result["Failed"] = recordsFailed;
            result["Inserted"] = recordsInserted;
            return result;
        }

    }
}