﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.Contacts;
using BNS.DAL.DALContact;

namespace BNS.BL.BLContact
{
    public class ContactBL
    {
        DALContactManager manager = new DALContactManager();
        public bool saveContact(Contact EmailContact, Contact PhoneContact)
        {
            EmailContact.ContactType = Contact.Contact_Type.EMAIL;
            PhoneContact.ContactType = Contact.Contact_Type.PHONE;          
            return manager.saveContact(EmailContact) && manager.saveContact(PhoneContact);
        }
    }
}