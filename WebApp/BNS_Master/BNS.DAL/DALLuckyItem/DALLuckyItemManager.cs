﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.LuckyItems;
using BNS.Models.Accounts;
using BNS.DL.WSDatabase;
using System.Data.SqlClient;
using BNS.Models.Kiosks;
using BNS.Models.Stores;
using BNS.Models.Countries;

namespace BNS.DAL.DALLuckyItem
{
    public class DALLuckyItemManager
    {
        public Kiosk getKioskLuckyItems(Kiosk kiosk)
        {
            String query = QueryConstants.All_LUCKY_ITEMS_BY_KIOSK + kiosk.id.ToString();
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = kiosk.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.getRecordsFromDatabase(Query);
            kiosk.luckyItems = new List<LuckyItem>();
            if (reader.HasRows)
            {             
                while (reader.Read())
                {
                    kiosk.name = reader.GetString(reader.GetOrdinal("KIOSK_NAME"));
                    kiosk.luckyItems.Add(new LuckyItem(reader));
                }
            }
            return kiosk ;
        }
        
        public Kiosk getLuckyItem(Kiosk kiosk, Int64 luckyItemId)
        {
            WSQuery Query = new WSQuery();
            //Query.query = "SELECT Kiosks.name AS KIOSK_NAME, Stores.name AS STORE_NAME, Countries.id AS COUNTRY_ID, Countries.name AS COUNTRY_NAME, LUCKY_ITEM.id AS LUCKY_ITEM_ID, LUCKY_ITEM.name AS LUCKY_ITEM_NAME, LUCKY_ITEM.icon AS LUCKY_ITEM_ICON FROM dbo.Kiosks INNER JOIN dbo.Stores ON Stores.id = Kiosks.store_id INNER JOIN dbo.Countries ON Countries.id = Stores.country_id INNER JOIN Lucky_Items LUCKY_ITEM ON LUCKY_ITEM.id =" + luckyItemId.ToString()+" WHERE Kiosks.id = "+ kiosk.id.ToString();
            Query.query = QueryConstants.LUCKY + luckyItemId.ToString() + QueryConstants.LUCKY2 + kiosk.id.ToString();
            Query.parameters["kioskId"] = kiosk.id;
            Query.parameters["luckyItemId"] = luckyItemId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.getRecordsFromDatabase(Query);
            kiosk.luckyItems = new List<LuckyItem>();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    kiosk.name = reader.GetString(reader.GetOrdinal("KIOSK_NAME"));
                    kiosk.luckyItems.Add(new LuckyItem(reader));
                    kiosk.store = new Store(reader);
                    kiosk.store.country = new Country(reader);
                }
            }
            return kiosk;
        }

        public Kiosk getLuckyItemsForKiosk(Kiosk kiosk)
        {
            String query = QueryConstants.All_LUCKY_ITEMS_BY_KIOSK + kiosk.id.ToString();
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = kiosk.id;

            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.getRecordsFromDatabase(Query);
            kiosk.luckyItems = new List<LuckyItem>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    kiosk.name = reader.GetString(reader.GetOrdinal("KIOSK_NAME"));
                    kiosk.luckyItems.Add(new LuckyItem(reader));
                }
            }
            return kiosk;
        }
    }
}