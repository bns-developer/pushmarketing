﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNS.DAL
{
    public class ProgramQueryConstants
    {
        //Programs
        public const String PROGRAM_ID = "PROGRAM_ID";
        public const String PROGRAM_ACCOUNT_ID = "PROGRAM_ACCOUNT_ID";
        public const String PROGRAM_NAME = "PROGRAM_NAME";
        public const String PROGRAM_DESCRIPTION = "PROGRAM_DESCRIPTION";
        public const String PROGRAM_STATUS = "PROGRAM_STATUS";
        public const String PROGRAM_MANAGER_ID = "PROGRAM_MANAGER_ID";
        public const String PROGRAM_MANAGER_NAME = "PROGRAM_MANAGER_NAME";
        public const String PROGRAM_MANAGER_ICON = "PROGRAM_MANAGER_ICON";
        public const String PROGRAM_CREATEDON = "PROGRAM_CREATEDON";
        public const String PROGRAM_CREATED_USER_ID = "PROGRAM_CREATED_USER_ID";
        public const String PROGRAM_CREATED_USER_NAME = "PROGRAM_CREATED_USER_NAME";
        public const String PROGRAM_CREATED_USER_ICON = "PROGRAM_CREATED_USER_ICON";
        public const String PROGRAM_UPDATEDON = "PROGRAM_UPDATEDON";
        public const String PROGRAM_UPDATED_USER_ID = "PROGRAM_UPDATED_USER_ID";
        public const String PROGRAM_UPDATED_USER_NAME = "PROGRAM_UPDATED_USER_NAME";
        public const String PROGRAM_UPDATED_USER_ICON = "PROGRAM_UPDATED_USER_ICON";

        public const String PROGRAM_ALIAS = "PROGRAM.id as " + PROGRAM_ID + ", PROGRAM.account as " + PROGRAM_ACCOUNT_ID + ", PROGRAM.name as " + PROGRAM_NAME + ", PROGRAM.description as " + PROGRAM_DESCRIPTION + ", PROGRAM.status as " + PROGRAM_STATUS + ", PROGRAM.createdOn as " + PROGRAM_CREATEDON + ", PROGRAM.updatedOn as " + PROGRAM_UPDATEDON;
        public const String PROGRAM_MANAGER_ALIAS = "PROGRAM_MANAGER.id as " + PROGRAM_MANAGER_ID + ", PROGRAM_MANAGER.name as " + PROGRAM_MANAGER_NAME + ", PROGRAM_MANAGER.icon as " + PROGRAM_MANAGER_ICON;
        public const String PROGRAM_UPDATED_USER_ALIAS = "PROGRAM_UPDATED_USER.id as " + PROGRAM_UPDATED_USER_ID + ", PROGRAM_UPDATED_USER.name as " + PROGRAM_UPDATED_USER_NAME + ", PROGRAM_UPDATED_USER.icon as" + PROGRAM_UPDATED_USER_ICON;
        public const String PROGRAM_CREATED_USER_ALIAS = "PROGRAM_CREATED_USER.id as " + PROGRAM_CREATED_USER_ID + ", PROGRAM_CREATED_USER.name as " + PROGRAM_CREATED_USER_NAME + ",PROGRAM_CREATED_USER.id as " + PROGRAM_CREATED_USER_ICON;

        public const String ALL_PROGRAMS_BY_ACCOUNT = "select " + PROGRAM_ALIAS + ", " + PROGRAM_MANAGER_ALIAS + "," + PROGRAM_UPDATED_USER_ALIAS + "," + PROGRAM_CREATED_USER_ALIAS + " from Programs PROGRAM left join Users PROGRAM_MANAGER on PROGRAM.manager=PROGRAM_MANAGER.id  left join Users PROGRAM_CREATED_USER on PROGRAM.createdBy=PROGRAM_CREATED_USER.id left join Users	PROGRAM_UPDATED_USER on PROGRAM.updatedBy=PROGRAM_UPDATED_USER.id where PROGRAM.account = @account";
        public const String GET_PROGRAM_DETAILS_QUERY = "select PROGRAM.id as PROGRAM_ID, PROGRAM.account as PROGRAM_ACCOUNT_ID, PROGRAM.name as  PROGRAM_NAME, PROGRAM.description as PROGRAM_DESCRIPTION, PROGRAM.status as PROGRAM_STATUS,PROGRAM_MANAGER.id as PROGRAM_MANAGER_ID,PROGRAM_MANAGER.name as PROGRAM_MANAGER_NAME, PROGRAM.createdOn as PROGRAM_CREATEDON, PROGRAM_CREATED_USER.id as PROGRAM_CREATED_USER_ID, PROGRAM_CREATED_USER.name as PROGRAM_CREATED_USER_NAME, PROGRAM.updatedOn as PROGRAM_UPDATEDON, PROGRAM_UPDATED_USER.id as PROGRAM_UPDATED_USER_ID, PROGRAM_UPDATED_USER.name as PROGRAM_UPDATED_USER_NAME from Programs PROGRAM left join Users PROGRAM_MANAGER on PROGRAM.manager=PROGRAM_MANAGER.id  left join Users PROGRAM_CREATED_USER on PROGRAM.createdBy=PROGRAM_CREATED_USER.id left join Users	PROGRAM_UPDATED_USER on PROGRAM.updatedBy=PROGRAM_UPDATED_USER.id where PROGRAM.id=@id";
        public const String UPDATE_PROGRAM_STATUS_QUERY = "Update Programs set status=@status, updatedOn=@updatedOn, updatedBy=@updatedBy where id=@id";
        public const String INSERT_PROGRAM_QUERY = "Insert into Programs(account,name,description,status,manager,createdBy,updatedBy) values(@account,@name,@description,@status,@manager,@createdBy,@updatedBy)";
        public const String UPDATE_PROGRAM_QUERY = "Update Programs set name=@name, description=@description, status=@status, manager=@manager, updatedOn=@updatedOn, updatedBy=@updatedBy where id=@id";
    }
}