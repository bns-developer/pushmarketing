﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.Countries;
using BNS.DL.WSDatabase;
using System.Data.SqlClient;


namespace BNS.DAL.DALCountry
{
    public class DALCountryManager
    {
        public List<Country> getAll()
        {
            WSQuery Query = new WSQuery();
            Query.query = "select COUNTRY.id as COUNTRY_ID, COUNTRY.name as COUNTRY_NAME, COUNTRY.flag_path as COUNTRY_FLAG_PATH from Countries COUNTRY";
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.getRecordsFromDatabase(Query);

            List<Country> CountriesList = new List<Country>();
            if(reader.HasRows)
            {
                while(reader.Read())
                {
                    CountriesList.Add(new Country(reader));                      
                }
            }

            return CountriesList;
        }
    }
}