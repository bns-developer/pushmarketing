﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.Categories;
using BNS.DL.WSDatabase;
using System.Data.SqlClient;
using BNS.Models.Accounts;

namespace BNS.DAL.DALCategory
{
    public class DALCategoryManager
    {
        public List<Category> getAll(Account account)
        {
            WSQuery Query = new WSQuery();
            Query.query = "SELECT CATEGORY.id as CATEGORY_ID, CATEGORY.name as CATEGORY_NAME, CATEGORY.account_id as CATEGORY_ACCOUNT_ID from Categories CATEGORY WHERE CATEGORY.account_id = " + account.id.ToString();
            Query.parameters["accountId"] = account.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.getRecordsFromDatabase(Query);

            List<Category> CategoriesList = new List<Category>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    CategoriesList.Add(new Category(reader));
                }
            }

            return CategoriesList;
        }
    }
}