﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.Accounts;
using BNS.Models.Beacons;
using BNS.DL.WSDatabase;
using System.Data.SqlClient;
using BNS.Models.Stores;
using BNS.Models.Countries;

namespace BNS.DAL.DALBeacon
{
    public class DALBeaconManager
    {
        public bool saveBeacon(Beacon beacon)
        {
            using (WSTransaction transaction = new WSTransaction("SAVE_BEACON"))
            {
                String query1 = "INSERT INTO Beacons(name, number, store_id, created_on, updated_on, created_by, updated_by) OUTPUT Inserted.id  VALUES(@name, @number, @storeId, @createdOn, @updatedOn, @createdBy, @updatedBy);";
                WSQuery Query = new WSQuery(query1,beacon.toDictionary(), transaction);               
                beacon.id = WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQueryReturnID(Query);
                if (beacon.id != 0)
                {
                    if (!this.saveBeaconDefaultOffer(beacon, transaction))
                    {
                        transaction.rollback();
                        return false;
                    }
                }
                else
                {
                    transaction.rollback();
                }
                transaction.commit();
                return true;
            }
                
        }

        public bool saveBeaconDefaultOffer(Beacon beacon, WSTransaction transaction)
        {
            String query = "INSERT INTO Beacons_Offers(beacon_id, offer_id, is_default, created_on, updated_on, created_by, updated_by) VALUES(@beaconId, @offerId, @isDefault, @createdOn, @updatedOn, @createdBy, @updatedBy);";
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beacon.id;
            Query.parameters["offerId"] = 26;
            Query.parameters["isDefault"] = 1;
            Query.parameters["createdOn"] = DateTime.Now.ToString();
            Query.parameters["updatedOn"] = DateTime.Now.ToString();
            Query.parameters["createdBy"] = beacon.createdBy.id;
            Query.parameters["updatedBy"] = beacon.updatedBy.id;
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);
        }


        public List<Beacon> getAll(Account account)
        {
            String query = QueryConstants.ALL_BEACONS_BY_ACCOUNT + account.id.ToString() + " ORDER BY BEACON.updated_on DESC";
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = account.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.getRecordsFromDatabase(Query);
            List<Beacon> beaconsList = new List<Beacon>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    beaconsList.Add(new Beacon(reader));
                }
            }
            return beaconsList;
        }

        public bool checkIfNameExists(String beaconName, Int64 storeId)
        {
            String query = "Select * from Beacons where name like @beaconName and store_id like @storeId";
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconName"] = beaconName;
            Query.parameters["storeId"] = storeId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.getRecordsFromDatabase(Query);
            return reader.HasRows;
        }

        public bool checkIfNumberExists(String beaconNumber)
        {
            WSQuery Query = new WSQuery();
            Query.query = "Select * from Beacons where number like @beaconNumber";
            Query.parameters["beaconNumber"] = beaconNumber;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.getRecordsFromDatabase(Query);
            return reader.HasRows;
        }

        public Beacon getDetails(Beacon beacon)
        {
            String query = QueryConstants.BEACON_DETAILS + beacon.id.ToString();
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beacon.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.getRecordsFromDatabase(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    beacon.name = reader.GetString(reader.GetOrdinal("BEACON_NAME"));
                    beacon.store = new Store(reader);
                    beacon.store.country = new Country(reader);
                }
            }
            return beacon;
        }

    }
}