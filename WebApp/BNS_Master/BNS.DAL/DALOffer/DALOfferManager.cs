﻿using System;
using System.Collections.Generic;
using System.Web;
using BNS.Models.Offers;
using BNS.DL.WSDatabase;
using BNS.Models.Countries;
using BNS.Models.Categories;
using BNS.Models.Accounts;
using System.Data.SqlClient;
using BNS.DAL.Constants;
using BNS.Models.Kiosks;

namespace BNS.DAL.DALOffer
{
    public class DALOfferManager
    {       
        public bool saveOffer(Offer offer)
        {
            using (WSTransaction transaction = new WSTransaction("SAVE_OFFER"))
            {
                if (offer.file != null)
                {
                    String fileExtension = System.IO.Path.GetExtension(offer.file.FileName);
                    String fileName = "BNS_" + DateTime.Now.ToString().Replace("/", "_").Replace(":", "_").Replace(" ", "_") + "_"; ;
                    offer.file.SaveAs(HttpContext.Current.Server.MapPath("~" + BNSConstants.OFFER_ICON_FILES_BASE_PATH) + fileName + fileExtension);
                    offer.imagePath = BNSConstants.OFFER_ICON_FILES_BASE_PATH + fileName + fileExtension;
                }
                String query = "INSERT INTO Offers(number, title, description, start_time, end_time, image_path, account_id, created_on, updated_on, created_by, updated_by) OUTPUT Inserted.id VALUES(@number, @title, @description, @startTime, @endTime, @imagePath, @account, @createdOn, @updatedOn, @createdBy, @updatedBy);";
                WSQuery Query = new WSQuery(query, offer.toDictionary(), transaction);
                offer.id = WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQueryReturnID(Query);
                if (offer.id != 0)
                {
                    foreach (Category category in offer.categories)
                    {
                        if(! this.saveOfferCategory(offer.id, category, transaction))
                        {
                            transaction.rollback();
                            return false;
                        }
                    }
                    foreach (Country country in offer.countries)
                    {
                        if(! this.saveOfferCountry(offer.id, country, transaction))
                        {
                            transaction.rollback();
                            return false;
                        }
                    }
                    transaction.commit();
                    return true;
                }
                else
                {
                    transaction.rollback();
                    return false;
                }
               
            }                    
        }

        //public bool addOffer(Offer offer)
        //{
        //    String query = "INSERT INTO Offers(number, title, description, start_time, end_time, image_path, account_id, created_on, updated_on, created_by, updated_by) OUTPUT Inserted.id VALUES(@number, @title, @description, @startTime, @endTime, @imagePath, @account, @createdOn, @updatedOn, @createdBy, @updatedBy);";
        //    WSQuery Query = new WSQuery(query, offer.toDictionary());
        //    offer.id = WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQueryReturnID(Query);
        //    if (offer.id != 0)
        //    {
        //        foreach (Category category in offer.categories)
        //        {
        //            this.saveOfferCategory(offer.id, category);
        //        }
        //        foreach (Country country in offer.countries)
        //        {
        //            this.saveOfferCountry(offer.id, country);
        //        }
        //        return true;
        //    }
        //    else
        //        return false;
        //}

        public List<Offer> getAll(Account account)
        {
            String query = QueryConstants.ALL_OFFERS_BY_ACCOUNT + account.id.ToString() + " ORDER BY OFFER_CREATED_ON DESC";
            WSQuery Query = new WSQuery(query);       
            Query.parameters["accountId"] = account.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.getRecordsFromDatabase(Query);
            List<Offer> offersList = new List<Offer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offersList.Add(new Offer(reader));
                }
            }
            return offersList;
        }

        public bool checkIfNumberExists(String offerNumber, Int64 account)
        {
            String query = "Select * from Offers where number like @offerNumber and account_id like @accountId";
            WSQuery Query = new WSQuery(query);           
            Query.parameters["offerNumber"] = offerNumber;
            Query.parameters["accountId"] = account;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.getRecordsFromDatabase(Query);
            return reader.HasRows;
        }

        public bool saveOfferCountry(Int64 offer, Country country, WSTransaction transaction)
        {
            String query = "INSERT INTO Offers_Countries(offer_id, country_id) VALUES(@offerId, @countryId);";
            WSQuery Query = new WSQuery(query);
            Query.parameters["offerId"] = offer;
            Query.parameters["countryId"] = country.id;
            Query.transaction = transaction;

            return WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);
        }

        public bool saveOfferCategory(Int64 offer, Category category, WSTransaction transaction)
        {
            String query = "INSERT INTO Offers_Categories(offer_id, category_id, value) VALUES(@offerId, @categoryId, @value);";
            WSQuery Query = new WSQuery(query);
            Query.parameters["offerId"] = offer;
            Query.parameters["categoryId"] = category.id;
            Query.parameters["value"] = category.value;
            Query.transaction = transaction;

            return WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);
        }

        public bool saveKioskLuckyItemOffer(Int64 kioskId, Int64 luckyItemId, Offer offer) 
        {
            String query = "INSERT INTO Kiosks_Lucky_Items_Offers(kiosk_id, lucky_item_id, offer_id, is_default, created_on, updated_on, created_by, updated_by) VALUES(@kioskId, @luckyItemId, @offerId, @isDefault, @createdOn, @updatedOn, @createdBy, @updatedBy);";
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = kioskId;
            Query.parameters["luckyItemId"] = luckyItemId;
            Query.parameters["offerId"] = offer.id;
            Query.parameters["isDefault"] = 0;
            Query.parameters["createdOn"] = DateTime.Now.ToString();
            Query.parameters["updatedOn"] = DateTime.Now.ToString();
            Query.parameters["createdBy"] = offer.createdBy.id;
            Query.parameters["updatedBy"] = offer.updatedBy.id;          

            return WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);
        }

        public List<Offer> getLuckyItemDefaultOffer(Int64 kioskId, Int64 luckyItemId)
        {
            String query = QueryConstants.LUCKY_ITEM_DEFAULT_OFFER + "KIOSKS_LICKY_ITEM_OFFER.kiosk_id = " + kioskId.ToString() + " AND KIOSKS_LICKY_ITEM_OFFER.lucky_item_id = " + luckyItemId.ToString() + " AND KIOSKS_LICKY_ITEM_OFFER.is_default = 1";
            WSQuery Query = new WSQuery(query);      
            Query.parameters["kioskId"] = kioskId;
            Query.parameters["luckyItemId"] = luckyItemId;           
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.getRecordsFromDatabase(Query);
            List<Offer> offersList = new List<Offer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offersList.Add(new Offer(reader));
                }
            }
            return offersList;
        }

        public List<Offer> getLuckyItemOfferForKiosk(Int64 kioskId, Int64 luckyItemId)
        {
            String query = QueryConstants.KIOSK_LUCKY_ITEM_OFFER + "KIOSKS_LICKY_ITEM_OFFER.kiosk_id = " + kioskId.ToString() + " AND KIOSKS_LICKY_ITEM_OFFER.lucky_item_id = " + luckyItemId.ToString() + " AND KIOSKS_LICKY_ITEM_OFFER.is_default = 1";
            WSQuery Query = new WSQuery(query);    
            Query.parameters["kioskId"] = kioskId;
            Query.parameters["luckyItemId"] = luckyItemId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.getRecordsFromDatabase(Query);
            List<Offer> offersList = new List<Offer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Offer offer = new Offer(reader);
                    offer.shortUrl = "https://www.google.com";
                    offersList.Add(offer);               
                }
            }
            return offersList;
        }

        public List<Offer> getLuckyItemCurrentActiveOfferForKiosk(Int64 kioskId, Int64 luckyItemId)
        {
            String currentDateTime = DateTime.Now.ToString();
            String query = QueryConstants.KIOSK_LUCKY_ITEM_CURRENT_OFFER + "KIOSKS_LICKY_ITEM_OFFER.kiosk_id = " + kioskId.ToString() + " AND KIOSKS_LICKY_ITEM_OFFER.lucky_item_id = " + luckyItemId.ToString() + " AND KIOSKS_LICKY_ITEM_OFFER.is_default = 0 AND OFFER.end_time >= '" + currentDateTime + "'";
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = kioskId;
            Query.parameters["luckyItemId"] = luckyItemId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.getRecordsFromDatabase(Query);
            List<Offer> offersList = new List<Offer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Offer offer = new Offer(reader);
                    offer.shortUrl = "https://www.google.com";
                    offersList.Add(offer);
                }
            }
            return offersList;
        }

        public List<Offer> getLuckyItemActiveOffers(Int64 kiosk_id, Int64 lucky_item_id)
        {                         
            String currentDateTime = DateTime.Now.ToString();
            String query = QueryConstants.LUCKY_ITEM_ACTIVE_OFFERS + "KIOSKS_LICKY_ITEM_OFFER.kiosk_id = " + kiosk_id.ToString() + " AND KIOSKS_LICKY_ITEM_OFFER.lucky_item_id = " + lucky_item_id.ToString() + " AND KIOSKS_LICKY_ITEM_OFFER.is_default = 0 AND OFFER.end_time >= '" + currentDateTime + "'";
            WSQuery Query = new WSQuery(query);       
            Query.parameters["kioskId"] = kiosk_id;
            Query.parameters["luckyItemId"] = lucky_item_id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.getRecordsFromDatabase(Query);
            List<Offer> offersList = new List<Offer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {                 
                    offersList.Add(new Offer(reader));
                }
            }
            return offersList;
        }

        public List<Offer> getLuckyItemOffersHistory(Int64 kiosk_id, Int64 lucky_item_id)
        {           
            String currentDateTime = DateTime.Now.ToString();
            String query = QueryConstants.LUCKY_ITEM_OFFERS_HISTORY + "KIOSKS_LICKY_ITEM_OFFER.kiosk_id = " + kiosk_id.ToString() + " AND KIOSKS_LICKY_ITEM_OFFER.lucky_item_id = " + lucky_item_id.ToString() + " AND KIOSKS_LICKY_ITEM_OFFER.is_default = 0 AND OFFER.end_time < '" + currentDateTime + "'";
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = kiosk_id;
            Query.parameters["luckyItemId"] = lucky_item_id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.getRecordsFromDatabase(Query);
            List<Offer> offersList = new List<Offer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offersList.Add(new Offer(reader));
                }
            }
            return offersList;
        }

        public List<Offer> getLuckyItemInactiveOffers(Int64 kiosk_id, Int64 lucky_item_id)
        {
            String currentDateTime = DateTime.Now.ToString();
            String query = QueryConstants.LUCKY_ITEM_INACTIVE_OFFERS + " OFFER.end_time > '" + currentDateTime + "' AND OFFER.id NOT IN (SELECT KIOSKS_LUCKY_ITEM_OFFER.offer_id FROM Kiosks_Lucky_Items_Offers KIOSKS_LUCKY_ITEM_OFFER WHERE KIOSKS_LUCKY_ITEM_OFFER.kiosk_id = " + kiosk_id.ToString() + " AND KIOSKS_LUCKY_ITEM_OFFER.lucky_item_id = " + lucky_item_id.ToString() + " )";
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = kiosk_id;
            Query.parameters["luckyItemId"] = lucky_item_id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.getRecordsFromDatabase(Query);
            List<Offer> offersList = new List<Offer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offersList.Add(new Offer(reader));
                }
            }
            return offersList;
        }


        public bool saveBeaconOffer(Int64 beaconId, Offer offer)
        {
            String query = "INSERT INTO Beacons_Offers(beacon_id, offer_id, is_default, created_on, updated_on, created_by, updated_by) VALUES(@beaconId, @offerId, @isDefault, @createdOn, @updatedOn, @createdBy, @updatedBy);";
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beaconId;
            Query.parameters["offerId"] = offer.id;
            Query.parameters["isDefault"] = 0;
            Query.parameters["createdOn"] = DateTime.Now.ToString();
            Query.parameters["updatedOn"] = DateTime.Now.ToString();
            Query.parameters["createdBy"] = offer.createdBy.id;
            Query.parameters["updatedBy"] = offer.updatedBy.id;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);
        }

        public List<Offer> getBeaconDefaultOffer(Int64 beacon_id)
        {
            String query = QueryConstants.BEACON_DEFAULT_OFFER + beacon_id.ToString() + " AND BEACON_OFFER.is_default = 1";
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beacon_id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.getRecordsFromDatabase(Query);
            List<Offer> offersList = new List<Offer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offersList.Add(new Offer(reader));
                }
            }
            return offersList;
        }

        public List<Offer> getBeaconActiveOffers(Int64 beacon_id)
        {
            String currentDateTime = DateTime.Now.ToString();
            String query = QueryConstants.BEACON_ACTIVE_OFFERS + beacon_id.ToString() + " AND BEACON_OFFER.is_default = 0 AND OFFER.end_time >= '" + currentDateTime + "'";
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beacon_id;            
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.getRecordsFromDatabase(Query);
            List<Offer> offersList = new List<Offer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offersList.Add(new Offer(reader));
                }
            }
            return offersList;
        }

        public List<Offer> getBeaconOffersHistory(Int64 beacon_id)
        {
            String currentDateTime = DateTime.Now.ToString();
            String query = QueryConstants.BEACON_OFFERS_HISTORY + beacon_id.ToString() + " AND BEACON_OFFER.is_default = 0 AND OFFER.end_time < '" + currentDateTime + "'";
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beacon_id;            
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.getRecordsFromDatabase(Query);
            List<Offer> offersList = new List<Offer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offersList.Add(new Offer(reader));
                }
            }
            return offersList;
        }

        public List<Offer> getBeaconInactiveOffers(Int64 beacon_id)
        {
            String currentDateTime = DateTime.Now.ToString();
            String query = QueryConstants.BEACON_INACTIVE_OFFERS + " OFFER.end_time >= '" + currentDateTime + "' AND OFFER.id NOT IN (SELECT BEACON_OFFER.offer_id FROM Beacons_Offers BEACON_OFFER WHERE BEACON_OFFER.beacon_id = " + beacon_id.ToString() + " )";
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beacon_id;           
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.getRecordsFromDatabase(Query);
            List<Offer> offersList = new List<Offer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offersList.Add(new Offer(reader));
                }
            }
            return offersList;
        }
    }
}