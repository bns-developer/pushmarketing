﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.DL.WSDatabase;
using System.Data.SqlClient;
using BNS.Models.Contacts;

namespace BNS.DAL.DALContact
{
    public class DALContactManager
    {
        public bool checkIfEmailIdExists(String emailId, Int64 account)
        {
            String query = "Select CONTACT.id , CONTACT.value, USER1.id, USER.contact_id, USER.account_id from Contacts CONTACT JOIN Users USER ON USER.contact_id = CONTACT.id where CONTACT.value like " + emailId + " AND USER.account_id = " + account.ToString();
            WSQuery Query = new WSQuery(query);
            Query.parameters["storeName"] = emailId;
            Query.parameters["accountId"] = account;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.getRecordsFromDatabase(Query);
            return reader.HasRows;

        }

        public bool saveContact(Contact contact)
        {
            String query = "INSERT INTO Contacts(country_id, type, value, created_on, updated_on, created_by, updated_by) OUTPUT Inserted.id  VALUES(@countryId, @type, @value, @createdOn, @updatedOn, @createdBy, @updatedBy);";
            WSQuery Query = new WSQuery(query,contact.toDictionary());
            contact.id = WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQueryReturnID(Query);
            return true;
        }
    }
}