﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNS.DAL
{
    public class QueryConstants
    {
        //STORES ALIAS
        public const String STORE_ID = "STORE_ID";
        public const String STORE_NUMBER = "STORE_NUMBER";
        public const String STORE_NAME = "STORE_NAME";
        public const String STORE_ACCOUNT_ID = "STORE_ACCOUNT_ID";
        public const String STORE_COUNTRY = "STORE_COUNTRY";
        public const String STORE_CREATED_ON = "STORE_CREATED_ON";
        public const String STORE_UPDATED_ON = "STORE_UPDATED_ON";
        public const String STORE_CREATED_BY = "STORE_CREATED_BY";
        public const String STORE_UPDATED_BY = "STORE_UPDATED_BY";
        public const String STORE_ALIAS = "STORE.id AS " + STORE_ID + ", STORE.number AS " + STORE_NUMBER + ", STORE.name AS " + STORE_NAME + ", STORE.country_id AS " + STORE_COUNTRY;
        public const String STORE_ALIAS_MINIMAL = "STORE.id AS " + STORE_ID + ", STORE.name AS " + STORE_NAME;
        public const String STORE_KIOSK_ALIAS = "STORE.id AS " + STORE_ID + ", STORE.name AS " + STORE_NAME + ", STORE.account_id AS " + STORE_ACCOUNT_ID;
        public const String STORE_BEACON_ALIAS = "STORE.id AS " + STORE_ID + ", STORE.name AS " + STORE_NAME + ", STORE.account_id AS " + STORE_ACCOUNT_ID;
        public const String ALL_STORES_BY_ACCOUNT = "SELECT " + STORE_ALIAS + ", " + COUNTRY_ALIAS + " FROM Stores STORE INNER JOIN Countries COUNTRY ON STORE.country_id = COUNTRY.id where";
        public const String ALL_STORES_FOR_DROPDOWN_BY_ACCOUNT = "SELECT " + STORE_ALIAS_MINIMAL + " FROM Stores STORE where";

        //COUNTRIES ALIAS
        public const String COUNTRY_ID = "COUNTRY_ID";
        public const String COUNTRY_NAME = "COUNTRY_NAME";
        public const String COUNTRY_FLAG_PATH = "COUNTRY_FLAG_PATH";
        public const String COUNTRY_ALIAS = "COUNTRY.id AS " + COUNTRY_ID + ", COUNTRY.name AS " + COUNTRY_NAME;

        //KIOSKS ALIAS
        public const String KIOSK_ID = "KIOSK_ID";
        public const String KIOSK_NUMBER = "KIOSK_NUMBER";
        public const String KIOSK_NAME = "KIOSK_NAME";
        public const String KIOSK_PUSH_ID = "KIOSK_PUSH_ID";
        public const String KIOSK_STORE = "KIOSK_STORE";
        public const String KIOSK_CREATED_ON = "KIOSK_CREATED_ON";
        public const String KIOSK_UPDATED_ON = "KIOSK_UPDATED_ON";
        public const String KIOSK_CREATED_BY = "KIOSK_CREATED_BY";
        public const String KIOSK_UPDATED_BY = "KIOSK_UPDATED_BY";
        public const String KIOSK_ALIAS = "KIOSK.id AS " + KIOSK_ID + ", KIOSK.number AS " + KIOSK_NUMBER + ", KIOSK.name AS " + KIOSK_NAME + ", KIOSK.store_id AS " + KIOSK_STORE;
        public const String ALL_KIOSKS_BY_ACCOUNT = "SELECT " + KIOSK_ALIAS + ", " + STORE_KIOSK_ALIAS + " FROM Kiosks KIOSK INNER JOIN Stores STORE ON KIOSK.store_id = STORE.id where STORE.account_id = ";
        public const String LUCKY = "SELECT KIOSK.name AS " + KIOSK_NAME + ", STORE.name AS " + STORE_NAME + ", COUNTRY.name AS " + COUNTRY_NAME + ", COUNTRY.id AS " + COUNTRY_ID + ", " + LUCKY_ITEM_ALIAS + " FROM Kiosks KIOSK INNER JOIN Stores STORE ON STORE.id = KIOSK.store_id INNER JOIN Countries COUNTRY ON COUNTRY.id = STORE.country_id INNER JOIN Lucky_Items LUCKY_ITEM ON LUCKY_ITEM.id = ";
        public const String LUCKY2 = " WHERE KIOSK.id = ";

        //OFFERS ALIAS
        public const String OFFER_ID = "OFFER_ID";
        public const String OFFER_ACCOUNT = "OFFER_ACCOUNT";
        public const String OFFER_NUMBER = "OFFER_NUMBER";
        public const String OFFER_TITLE = "OFFER_TITLE";
        public const String OFFER_DESCRIPTION = "OFFER_DESCRIPTION";
        public const String OFFER_IMAGE_PATH = "OFFER_IMAGE_PATH";
        public const String OFFER_START_TIME = "OFFER_START_TIME";
        public const String OFFER_END_TIME = "OFFER_END_TIME";
        public const String OFFER_CREATED_ON = "OFFER_CREATED_ON";
        public const String OFFER_UPDATED_ON = "OFFER_UPDATED_ON";
        public const String OFFER_CREATED_BY = "OFFER_CREATED_BY";
        public const String OFFER_UPDATED_BY = "OFFER_UPDATED_BY";
        public const String OFFER_ALIAS = "OFFER.id AS " + OFFER_ID + ", OFFER.number AS " + OFFER_NUMBER + ", OFFER.title AS " + OFFER_TITLE + ", OFFER.description AS " + OFFER_DESCRIPTION + ", OFFER.start_time AS " + OFFER_START_TIME + ", OFFER.end_time AS " + OFFER_END_TIME;
        public const String OFFER_ALIAS1 = "OFFER.id AS " + OFFER_ID + ", OFFER.title AS " + OFFER_TITLE + ", OFFER.description AS " + OFFER_DESCRIPTION + ", OFFER.created_on AS " + OFFER_CREATED_ON + ", OFFER.account_id AS " + OFFER_ACCOUNT;
        public const String OFFER_ALIAS2 = "OFFER.id AS " + OFFER_ID + ", OFFER.title AS " + OFFER_TITLE + ", OFFER.description AS " + OFFER_DESCRIPTION + ", OFFER.created_by AS " + OFFER_CREATED_BY + ", OFFER.created_on AS " + OFFER_CREATED_ON;
        public const String ALL_OFFERS_BY_ACCOUNT = "SELECT " + OFFER_ALIAS1 + ", " + CATEGORY_ALIAS + " FROM Offers OFFER  JOIN Offers_Categories OFFER_CATEGORY ON OFFER.id = OFFER_CATEGORY.offer_id JOIN Categories CATEGORY ON CATEGORY.id = OFFER_CATEGORY.category_id WHERE OFFER.account_id = ";
        public const String LUCKY_ITEM_DEFAULT_OFFER = "SELECT " + OFFER_ALIAS2 + ", " + CATEGORY_ALIAS + " FROM Offers OFFER JOIN Kiosks_Lucky_Items_Offers KIOSKS_LICKY_ITEM_OFFER ON KIOSKS_LICKY_ITEM_OFFER.offer_id = OFFER.id JOIN Offers_Categories OFFER_CATEGORIES ON OFFER.id = OFFER_CATEGORIES.offer_id JOIN Categories CATEGORY ON CATEGORY.id = OFFER_CATEGORIES.category_id WHERE ";
        public const String LUCKY_ITEM_ACTIVE_OFFERS = "SELECT " + OFFER_ALIAS + ", " + CATEGORY_ALIAS + " FROM Offers OFFER JOIN Kiosks_Lucky_Items_Offers KIOSKS_LICKY_ITEM_OFFER ON KIOSKS_LICKY_ITEM_OFFER.offer_id = OFFER.id JOIN Offers_Categories OFFER_CATEGORIES ON OFFER.id = OFFER_CATEGORIES.offer_id JOIN Categories CATEGORY ON CATEGORY.id = OFFER_CATEGORIES.category_id WHERE ";
        public const String LUCKY_ITEM_OFFERS_HISTORY = "SELECT " + OFFER_ALIAS + ", " + CATEGORY_ALIAS + " FROM Offers OFFER JOIN Kiosks_Lucky_Items_Offers KIOSKS_LICKY_ITEM_OFFER ON KIOSKS_LICKY_ITEM_OFFER.offer_id = OFFER.id JOIN Offers_Categories OFFER_CATEGORIES ON OFFER.id = OFFER_CATEGORIES.offer_id JOIN Categories CATEGORY ON CATEGORY.id = OFFER_CATEGORIES.category_id WHERE ";
        public const String KIOSK_LUCKY_ITEM_OFFER = "SELECT " + OFFER_ALIAS + ", " + CATEGORY_ALIAS + ", OFFER_CATEGORY.value AS " + OFFER_CATEGORY_VALUE + " FROM Offers OFFER JOIN Kiosks_Lucky_Items_Offers KIOSKS_LICKY_ITEM_OFFER ON KIOSKS_LICKY_ITEM_OFFER.offer_id = OFFER.id JOIN Offers_Categories OFFER_CATEGORIES ON OFFER.id =OFFER_CATEGORIES.offer_id JOIN Categories CATEGORY ON CATEGORY.id = OFFER_CATEGORIES.category_id JOIN Offers_Categories OFFER_CATEGORY ON OFFER_CATEGORY.category_id = CATEGORY.id AND OFFER_CATEGORY.offer_id = OFFER.id WHERE ";
        public const String KIOSK_LUCKY_ITEM_CURRENT_OFFER = "SELECT TOP 1 " + OFFER_ALIAS + ", " + CATEGORY_ALIAS + ", OFFER_CATEGORY.value AS " + OFFER_CATEGORY_VALUE + " FROM Offers OFFER JOIN Kiosks_Lucky_Items_Offers KIOSKS_LICKY_ITEM_OFFER ON KIOSKS_LICKY_ITEM_OFFER.offer_id = OFFER.id JOIN Offers_Categories OFFER_CATEGORIES ON OFFER.id =OFFER_CATEGORIES.offer_id JOIN Categories CATEGORY ON CATEGORY.id = OFFER_CATEGORIES.category_id JOIN Offers_Categories OFFER_CATEGORY ON OFFER_CATEGORY.category_id = CATEGORY.id AND OFFER_CATEGORY.offer_id = OFFER.id WHERE ";
        public const String LUCKY_ITEM_INACTIVE_OFFERS = "SELECT DISTINCT " + OFFER_ALIAS + ", " + CATEGORY_ALIAS + " FROM Offers OFFER JOIN Offers_Categories OFFER_CATEGORIES ON OFFER.id = OFFER_CATEGORIES.offer_id JOIN Categories CATEGORY ON CATEGORY.id = OFFER_CATEGORIES.category_id WHERE ";
        public const String BEACON_ACTIVE_OFFERS = "SELECT " + OFFER_ALIAS + ", " + CATEGORY_ALIAS + " FROM Offers OFFER JOIN Beacons_Offers BEACON_OFFER ON BEACON_OFFER.offer_id = OFFER.id JOIN Offers_Categories OFFER_CATEGORIES ON OFFER.id = OFFER_CATEGORIES.offer_id JOIN Categories CATEGORY ON CATEGORY.id = OFFER_CATEGORIES.category_id WHERE BEACON_OFFER.beacon_id = ";
        public const String BEACON_OFFERS_HISTORY = "SELECT " + OFFER_ALIAS + ", " + CATEGORY_ALIAS + " FROM Offers OFFER JOIN Beacons_Offers BEACON_OFFER ON BEACON_OFFER.offer_id = OFFER.id JOIN Offers_Categories OFFER_CATEGORIES ON OFFER.id = OFFER_CATEGORIES.offer_id JOIN Categories CATEGORY ON CATEGORY.id = OFFER_CATEGORIES.category_id WHERE BEACON_OFFER.beacon_id = ";
        public const String BEACON_INACTIVE_OFFERS = "SELECT DISTINCT " + OFFER_ALIAS + ", " + CATEGORY_ALIAS + " FROM Offers OFFER JOIN Offers_Categories OFFER_CATEGORIES ON OFFER.id = OFFER_CATEGORIES.offer_id JOIN Categories CATEGORY ON CATEGORY.id = OFFER_CATEGORIES.category_id WHERE ";

        //CATEGORIES ALIAS
        public const String CATEGORY_ID = "CATEGORY_ID";
        public const String CATEGORY_NAME = "CATEGORY_NAME";
        public const String CATEGORY_ACCOUNT = "CATEGORY_ACCOUNT";
        public const String CATEGORY_ALIAS = "CATEGORY.id AS " + CATEGORY_ID + ", CATEGORY.name AS " + CATEGORY_NAME;


        //OFFERS_CATEGORIES ALIAS
        public const String OFFER_CATEGORY_OFFERID = "OFFER_CATEGORY_OFFERID";
        public const String OFFER_CATEGORY_CATEGORYID = "OFFER_CATEGORY_CATEGORYID";
        public const String OFFER_CATEGORY_VALUE = "OFFER_CATEGORY_VALUE";

        //LUCKY_ITEMS ALIAS
        public const String LUCKY_ITEM_ID = "LUCKY_ITEM_ID";
        public const String LUCKY_ITEM_NAME = "LUCKY_ITEM_NAME";
        public const String LUCKY_ITEM_ICON = "LUCKY_ITEM_ICON";
        public const String LUCKY_ITEM_ACCOUNT = "LUCKY_ITEM_ACCOUNT";
        public const String LUCKY_ITEM_ALIAS = "LUCKY_ITEM.id AS " + LUCKY_ITEM_ID + ", LUCKY_ITEM.name AS " + LUCKY_ITEM_NAME + ", LUCKY_ITEM.icon AS " + LUCKY_ITEM_ICON + ", LUCKY_ITEM.account_id AS " + LUCKY_ITEM_ACCOUNT;
        public const String All_LUCKY_ITEMS_BY_KIOSK = "SELECT KIOSK.name AS KIOSK_NAME, " + LUCKY_ITEM_ALIAS + " FROM Lucky_Items LUCKY_ITEM JOIN Kiosks_lucky_items KIOSK_LUCKY_ITEM ON KIOSK_LUCKY_ITEM.lucky_item_id = LUCKY_ITEM.id INNER JOIN Kiosks KIOSK ON KIOSK.id = KIOSK_LUCKY_ITEM.kiosk_id WHERE KIOSK_LUCKY_ITEM.kiosk_id = ";
        public const String All_LUCKY_ITEMS_BY_ACCOUNT = "SELECT " + LUCKY_ITEM_ALIAS  + ", STORE.id AS STORE_ID, STORE.account_id AS STORE_ACCOUNT_ID FROM Lucky_Items LUCKY_ITEM JOIN Stores STORE ON STORE.id = ";

        //KIOSKS_LUCKY_ITEMS_OFFERS ALIAS
        public const String KIOSK_LUCKY_ITEM_OFFER_KIOSKID = "KIOSK_LUCKY_ITEM_OFFER_KIOSKID";
        public const String KIOSK_LUCKY_ITEM_OFFER_LUCKYITEMID = "KIOSK_LUCKY_ITEM_OFFER_LUCKYITEMID";
        public const String KIOSK_LUCKY_ITEM_OFFER_OFFERID = "KIOSK_LUCKY_ITEM_OFFER_OFFERID";
        public const String KIOSK_LUCKY_ITEM_OFFER_SHORTURL = "KIOSK_LUCKY_ITEM_OFFER_SHORTURL";
        public const String KIOSK_LUCKY_ITEM_OFFER_CREATEDON = "KIOSK_LUCKY_ITEM_OFFER_CREATEDON";
        public const String KIOSK_LUCKY_ITEM_OFFER_UPDATEDON = "KIOSK_LUCKY_ITEM_OFFER_UPDATEDON";
        public const String KIOSK_LUCKY_ITEM_OFFER_CREATEDBY = "KIOSK_LUCKY_ITEM_OFFER_CREATEDBY";
        public const String KIOSK_LUCKY_ITEM_OFFER_UPDATEDBY = "KIOSK_LUCKY_ITEM_OFFER_UPDATEDBY";
        public const String KIOSKS_LUCKY_ITEMS_OFFERS_ALIAS = "KIOSK_LUCKY_ITEM.kiosk_id AS " + KIOSK_LUCKY_ITEM_OFFER_KIOSKID + ", KIOSK_LUCKY_ITEM.lucky_item_id AS " + KIOSK_LUCKY_ITEM_OFFER_LUCKYITEMID;

        //BEACONS ALIAS
        public const String BEACON_ID = "BEACON_ID";
        public const String BEACON_NUMBER = "BEACON_NUMBER";
        public const String BEACON_NAME = "BEACON_NAME";
        public const String BEACON_STORE = "BEACON_STORE";
        public const String BEACON_CREATED_ON = "BEACON_CREATED_ON";
        public const String BEACON_UPDATED_ON = "BEACON_UPDATED_ON";
        public const String BEACON_CREATED_BY = "BEACON_CREATED_BY";
        public const String BEACON_UPDATED_BY = "BEACON_UPDATED_BY";
        public const String BEACON_ALIAS = "BEACON.id AS " + BEACON_ID + ", BEACON.number AS " + BEACON_NUMBER + ", BEACON.name AS " + BEACON_NAME + ", BEACON.store_id AS " + BEACON_STORE ;
        public const String ALL_BEACONS_BY_ACCOUNT = "SELECT " + BEACON_ALIAS + ", " + STORE_BEACON_ALIAS + " FROM Beacons BEACON INNER JOIN Stores STORE ON BEACON.store_id = STORE.id where STORE.account_id = ";
        public const String BEACON_DETAILS = "SELECT BEACON.name AS " + BEACON_NAME + ", STORE.id AS " + STORE_ID + ", STORE.name AS " + STORE_NAME + ", COUNTRY.id AS " + COUNTRY_ID + ", COUNTRY.name AS " + COUNTRY_NAME + " FROM Beacons BEACON INNER JOIN Stores STORE ON STORE.id = BEACON.store_id INNER JOIN Countries COUNTRY ON COUNTRY.id = STORE.country_id WHERE BEACON.id = ";
        public const String BEACON_DEFAULT_OFFER = "SELECT " + OFFER_ALIAS2 + ", " + CATEGORY_ALIAS + " FROM Offers OFFER JOIN Beacons_Offers BEACON_OFFER ON BEACON_OFFER.offer_id = OFFER.id JOIN Offers_Categories OFFER_CATEGORIES ON OFFER.id = OFFER_CATEGORIES.offer_id JOIN Categories CATEGORY ON CATEGORY.id = OFFER_CATEGORIES.category_id WHERE BEACON_OFFER.beacon_id = ";

        //BEACONS_OFFERS ALIAS
        public const String BEACON_OFFER_BEACONID = "BEACON_OFFER_BEACONID";
        public const String BEACON_OFFER_OFFERID = "BEACON_OFFER_OFFERID";
        public const String BEACON_OFFER_SHORT_URL = "BEACON_OFFER_SHORT_URL";
        public const String BEACON_OFFER_CREATED_ON = "BEACON_OFFER_CREATED_ON";
        public const String BEACON_OFFER_UPDATED_ON = "BEACON_OFFER_UPDATED_ON";
        public const String BEACON_OFFER_CREATED_BY = "BEACON_OFFER_CREATED_BY";
        public const String BEACON_OFFER_UPDATED_BY = "BEACON_OFFER_UPDATED_BY";

    }
}