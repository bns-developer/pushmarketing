﻿using BNS.DL.WSDatabase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BNS.DAL.DALUser
{
    public class DALUserManager
    {        
        public Int64 isMember(String id)
        {
            if(id.Equals("NVMTC23645"))
            {
                return 1145;
            }
            else
            {
                return 0;
            }
        }

        public Int64 isRegister(string email, string phone)
        {
            if(email.Equals("nonmember@gmail.com") && phone.Equals("9876543210"))
            {
                return 1623;
            }
            else
            {
                if(email == null || phone == null)
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
            }
        }
    }
}