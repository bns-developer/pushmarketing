﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.DL.WSDatabase;
using System.Data.SqlClient;
using BNS.Models.Kiosks;
using BNS.Models.Accounts;
using BNS.Models.LuckyItems;
using BNS.DAL.DALLuckyItem;
using BNS.DAL;

namespace BNS.DAL.DALKiosk
{
    public class DALKioskManager
    {

        public Int64 saveKiosk(Kiosk kiosk)
        {
            using (WSTransaction transaction = new WSTransaction("SAVE_KIOSK"))
            {
                String query = "INSERT INTO Kiosks(name, store_id, created_on, updated_on, created_by, updated_by) OUTPUT Inserted.id  VALUES(@name, @storeId, @createdOn, @updatedOn, @createdBy, @updatedBy);";
                WSQuery Query = new WSQuery(query, kiosk.toDictionary(), transaction);
                kiosk.id = WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQueryReturnID(Query);                
                kiosk.luckyItems = this.getAllLuckeyItemForKiosk(kiosk);
                foreach (LuckyItem item in kiosk.luckyItems)
                {
                    if (this.setLuckyItemsToKiosk(kiosk, item, transaction))
                    {
                        if (!this.setDefaultOfferToKioskLuckyItem(kiosk, item, transaction))
                            transaction.rollback();                            
                    }
                    else
                    {
                        transaction.rollback();
                    }
                }
                transaction.commit();
            }
            return kiosk.id;
        }

    private List<LuckyItem> getAllLuckeyItemForKiosk(Kiosk kiosk)
        {
            WSQuery query = new WSQuery();
            query.query = QueryConstants.All_LUCKY_ITEMS_BY_ACCOUNT + kiosk.store.id.ToString() + " AND STORE.account_id = LUCKY_ITEM.account_id;";
            query.parameters["storeId"] = kiosk.store.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.getRecordsFromDatabase(query);
            List<LuckyItem> luckyItems = new List<LuckyItem>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {                   
                    luckyItems.Add(new LuckyItem(reader));                   
                }
            }
            return luckyItems;
        }

    public bool setLuckyItemsToKiosk(Kiosk kiosk, LuckyItem luckyItem, WSTransaction transaction)
    {
        String query = "INSERT INTO Kiosks_lucky_items(kiosk_id, lucky_item_id, created_on, updated_on, created_by, updated_by) VALUES(@kioskId, @luckyItemId, @createdOn, @updatedOn, @createdBy, @updatedBy);";
        WSQuery Query = new WSQuery(query);
        Query.parameters["kioskId"] = kiosk.id;
        Query.parameters["luckyItemId"] = luckyItem.id;
        Query.parameters["createdOn"] = DateTime.Now.ToString();
        Query.parameters["updatedOn"] = DateTime.Now.ToString();
        Query.parameters["createdBy"] = kiosk.createdBy.id;
        Query.parameters["updatedBy"] = kiosk.updatedBy.id;
        Query.transaction = transaction;
        return WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);
    }

    public bool setDefaultOfferToKioskLuckyItem(Kiosk kiosk, LuckyItem luckyItem, WSTransaction transaction)
    {
        String query = "INSERT INTO Kiosks_Lucky_Items_Offers(kiosk_id, lucky_item_id, offer_id, is_default, created_on, updated_on, created_by, updated_by) VALUES(@kioskId, @luckyItemId, @offerId, @isDefault, @createdOn, @updatedOn, @createdBy, @updatedBy);";
        WSQuery Query = new WSQuery(query);
        Query.parameters["kioskId"] = kiosk.id;
        Query.parameters["luckyItemId"] = luckyItem.id;
        if (luckyItem.id % 2 == 0)
        {
            Query.parameters["offerId"] = 1;
        }
        else
        {
            Query.parameters["offerId"] = 5;
        }
        Query.parameters["isDefault"] = 1;
        Query.parameters["createdOn"] = DateTime.Now.ToString();
        Query.parameters["updatedOn"] = DateTime.Now.ToString();
        Query.parameters["createdBy"] = kiosk.createdBy.id;
        Query.parameters["updatedBy"] = kiosk.updatedBy.id;
        Query.transaction = transaction;
        return WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);
    }

    public List<Kiosk> getAll(Account account)
    {
        String query = QueryConstants.ALL_KIOSKS_BY_ACCOUNT + account.id.ToString() + " ORDER BY KIOSK.updated_on DESC";
        WSQuery Query = new WSQuery(query);
        Query.parameters["accountId"] = account.id;
        SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.getRecordsFromDatabase(Query);
        List<Kiosk> kiosksList = new List<Kiosk>();
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                kiosksList.Add(new Kiosk(reader));
            }
        }
        return kiosksList;
    }

    public bool checkIfNameExists(String kioskName, Int64 storeId)
    {
        String query = "Select * from Kiosks where name like @kioskName and store_id like @storeId";
        WSQuery Query = new WSQuery(query);
        Query.parameters["kioskName"] = kioskName;
        Query.parameters["storeId"] = storeId;
        SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.getRecordsFromDatabase(Query);
        return reader.HasRows;
    }

    public bool checkIfNumberExists(String kioskNumber)
    {
        WSQuery Query = new WSQuery();
        Query.query = "Select * from Kiosks where number like @kioskNumber";
        Query.parameters["kioskNumber"] = kioskNumber;
        SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.getRecordsFromDatabase(Query);
        return reader.HasRows;
    }
}
}