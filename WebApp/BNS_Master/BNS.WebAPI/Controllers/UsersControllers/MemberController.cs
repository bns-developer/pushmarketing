﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BNS.DAL.DALUser;
using Newtonsoft.Json.Linq;

namespace BNS.API.Controllers.UsersControllers
{
    [RoutePrefix("api/v1/users")]
    public class MemberController : ApiController
    {      
        DALUserManager user = new DALUserManager();

        [Route("member")]
        [HttpPost]

        public HttpResponseMessage OfferForMember([FromBody]JObject credentials)
        {
            try
            {
                String member_id = (string)credentials["member_id"];
                String offer_id = (string)credentials["offer_id"];
                Int64 userId = user.isMember(member_id);

                if (userId != 0)
                {
                    var res = Request.CreateResponse(HttpStatusCode.OK, "Successful");
                    return res;
                }
                else
                {
                    var res = Request.CreateResponse(HttpStatusCode.OK, "Not a member");
                    return res;
                }
            }
            catch(NullReferenceException ex)
            {
                var res = Request.CreateResponse(HttpStatusCode.BadRequest, "Missing data");
                return res;
            }
            

        }
    }
}
