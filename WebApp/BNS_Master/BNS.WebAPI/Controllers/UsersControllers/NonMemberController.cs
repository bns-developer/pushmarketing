﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BNS.DAL.DALUser;
using Newtonsoft.Json.Linq;

namespace BNS.API.Controllers.UsersControllers
{
    [RoutePrefix("api/v1/users")]
    public class NonMemberController : ApiController
    {
        DALUserManager user = new DALUserManager();

        [Route("nonmember")]
        [HttpPost]
        public HttpResponseMessage OfferForNonMember([FromBody]JObject credentials)
        {
            try
            {
                String email = (string)credentials["email"];
                String phone = (string)credentials["phone"];

                Int64 userId = user.isRegister(email, phone);

                if(userId == 0 )
                {
                    var res = Request.CreateResponse(HttpStatusCode.Created, "Successfully Created");
                    return res;
                }
                else if(userId == -1)
                {
                    throw new NullReferenceException();
                }
                else
                {
                    var res = Request.CreateResponse(HttpStatusCode.OK, "Already a Member");
                    return res;
                }

            }
            catch(NullReferenceException Ex)
            {
                var res = Request.CreateResponse(HttpStatusCode.BadRequest, "Missing data");
                return res;
            }
            

        }
    }
}
