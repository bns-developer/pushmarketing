﻿using BNS.Wrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Script.Serialization;
using BNS.WebAPI.Controllers.BNSControllers;
using Newtonsoft.Json.Linq;
using BNS.Models.Accounts;
using BNS.Models.Users;
using BNS.Models.Offers;
using BNS.BL.BLOffer;
using BNS.DAL.DALOffer;
using BNS.Models.Countries;
using BNS.Models.Categories;
using System.Web;
using BNS.WebAPI.Authorization;

namespace BNS.API.Controllers.OfferControllers
{
    [RoutePrefix("api/v1/Offers")]
    public class OfferController : BNSController
    {
        OfferBL BLmanager = new OfferBL();
        DALOfferManager DALmanager = new DALOfferManager();
        ResponseWrapper responseWrapper = new ResponseWrapper();
      
        [Authorized]
        [Route("save")]
        [HttpPost]
        //Save an offer account 
        public HttpResponseMessage save()
        {
            HttpFileCollection files = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files : null;
            String offerNumber = Convert.ToString(HttpContext.Current.Request.Form.GetValues("number").FirstOrDefault());
            Account account = new Account();
            account.id = 1;
            User requestUser = new User();
            List<string[]> countries = new List<string[]>();
            requestUser.id = 1;

            if (DALmanager.checkIfNumberExists(offerNumber, account.id))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Offer Number Already Exist");
            }
            else
            {
                Offer newOffer = new Offer(HttpContext.Current);
                newOffer.createdBy = requestUser;
                newOffer.updatedBy = requestUser;
                newOffer.account = account;
   
                String[] Result1 = HttpContext.Current.Request.Form.GetValues("countries");
                int length = HttpContext.Current.Request.Form.Count;
                while(length >= 0)
                {
                    if (HttpContext.Current.Request.Form.GetValues("countries[" + length + "]") != null)
                    
                    countries.Add(HttpContext.Current.Request.Form.GetValues("countries[" + length + "]"));
                    length = length - 1;
                }
                //int[] countryArray = Result1.Select(x => int.Parse(x)).ToArray();
                //foreach (int item in countryArray)
                //{
                //    Country country = new Country();
                //    country.id = item;
                //    newOffer.countries.Add(country);

                //}

                String[] Result2 = HttpContext.Current.Request.Form.GetValues("categories");
                int[] categoryArray = Result2.Select(x => int.Parse(x)).ToArray();
                //foreach (int item in countryArray)
                //{
                //    Category category = new Category();
                //    category.id = item;
                //    newOffer.categories.Add(category);

                //}

                if (BLmanager.save(newOffer))
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Offer  Saved");
                }
                else
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Offer Not Saved");
                }
            }           
            
        }

        [Route("addOffer")]
        [HttpPost]
        public  HttpResponseMessage addOffer([FromBody] JObject requestJSON)
        {
            Account account = new Account();
            account.id = 1;
            User requestUser = new User();
            requestUser.id = 1;
            String offerNumber = (String)requestJSON["number"];
            
            if (DALmanager.checkIfNumberExists(offerNumber, account.id))
            {               
              return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Offer Number Already Exist");
            }
            else
            {

                JArray countryJsonArray = (JArray)requestJSON.SelectToken("countries");
                JArray categoryJsonArray = (JArray)requestJSON.SelectToken("categories");


                Offer newOffer = new Offer(requestJSON);
                newOffer.createdBy = requestUser;
                newOffer.updatedBy = requestUser;
                newOffer.account = account;

                foreach (JValue item in countryJsonArray)
                {                   
                    Country country = new Country(); 
                    country.id = Convert.ToInt64(item);
                    newOffer.countries.Add(country);
                }
                foreach (JValue item in categoryJsonArray)
                {
                    Category category = new Category();
                    category.id = Convert.ToInt64(item);
                    newOffer.categories.Add(category);
                }
                if (BLmanager.save(newOffer))
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Offer  Saved");
                }
                else
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Offer Not Saved");
                }
            }
            
        }

        [Route("all")]
        [HttpGet]
        //All offers by accountId
        public override HttpResponseMessage GetAll()
        {
            Account account = new Account();
            account.id = 1;
            List<Offer> offer = DALmanager.getAll(account);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
        }


        [Route("setOfferForLuckyItem")]
        [HttpPost]
        // Set an offer for kiosk's lucky item
        public HttpResponseMessage saveKioskLuckyItemOffer([FromUri] Int64 kiosk_id, [FromUri] Int64 lucky_item_id, [FromUri] Int64 offer_id)
        {
            Account account = new Account();
            account.id = 1;
            User requestUser = new User();
            requestUser.id = 1;

            Offer offer = new Offer();
            offer.id = offer_id;
            offer.createdBy = requestUser;
            offer.updatedBy = requestUser;

            if (BLmanager.saveOfferForLuckyItem(kiosk_id, lucky_item_id, offer))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Offer Activated Successfully");
            }
            else
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Offer Not Activated");
            }

        }

        [Route("getLuckyItemDefaultOffer")]
        [HttpGet]
        //Default offer for kiosk's lucky item
        public  HttpResponseMessage getDefaultOffer([FromUri] Int64 kiosk_id, [FromUri] Int64 lucky_item_id)
        {           
            List<Offer> offer = DALmanager.getLuckyItemDefaultOffer(kiosk_id, lucky_item_id);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
        }

        [Route("getLuckyItemActiveOffers")]
        [HttpGet]
        // Active offers for kiosk's lucky item
        public HttpResponseMessage getActiveOffers([FromUri] Int64 kiosk_id, [FromUri] Int64 lucky_item_id)
        {
            List<Offer> offer = DALmanager.getLuckyItemActiveOffers(kiosk_id, lucky_item_id);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
        }

        [Route("getLuckyItemOffersHistory")]
        [HttpGet]
        // Offers history of kiosk's lucky item
        public HttpResponseMessage getOffersHistory([FromUri] Int64 kiosk_id, [FromUri] Int64 lucky_item_id)
        {
            List<Offer> offer = DALmanager.getLuckyItemOffersHistory(kiosk_id, lucky_item_id);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
        }

        [Route("getLuckyItemInactiveOffers")]
        [HttpGet]
        // Inactive offers kiosk's lucky item
        public HttpResponseMessage getInactiveOffers([FromUri] Int64 kiosk_id, [FromUri] Int64 lucky_item_id)
        {
            List<Offer> offer = DALmanager.getLuckyItemInactiveOffers(kiosk_id, lucky_item_id);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
        }
               

        [Route("setOfferForBeacon")]
        [HttpPost]
        // Set an offer for beacon
        public HttpResponseMessage saveBeaconOffer([FromUri] Int64 beacon_id, [FromUri] Int64 offer_id)
        {
            Account account = new Account();
            account.id = 1;
            User requestUser = new User();
            requestUser.id = 1;

            Offer offer = new Offer();
            offer.id = offer_id;
            offer.createdBy = requestUser;
            offer.updatedBy = requestUser;

            if (BLmanager.saveOfferForBeacon(beacon_id, offer))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Offer Activated Successfully");
            }
            else
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Offer Not Activated");
            }

        }

        [Route("getBeaconDfaultOffer")]
        [HttpGet]
        //Default offer for beacon
        public HttpResponseMessage getDefaultOffer([FromUri] Int64 beacon_id)
        {
            List<Offer> offer = DALmanager.getBeaconDefaultOffer(beacon_id);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
        }

        [Route("getBeaconActiveOffers")]
        [HttpGet]
        // Active offers for beacon
        public HttpResponseMessage getActiveOffers([FromUri] Int64 beacon_id)
        {
            List<Offer> offer = DALmanager.getBeaconActiveOffers(beacon_id);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
        }

        [Route("getBeaconOffersHistory")]
        [HttpGet]
        // Offers history of kiosk's lucky item
        public HttpResponseMessage getOffersHistory([FromUri] Int64 beacon_id)
        {
            List<Offer> offer = DALmanager.getBeaconOffersHistory(beacon_id);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
        }

        [Route("getBeaconInactiveOffers")]
        [HttpGet]
        // Inactive offers kiosk's lucky item
        public HttpResponseMessage getInactiveOffers([FromUri] Int64 beacon_id)
        {
            List<Offer> offer = DALmanager.getBeaconInactiveOffers(beacon_id);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
        }

        public override HttpResponseMessage delete([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }

        public override HttpResponseMessage deleteMultiple([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }       

        public override HttpResponseMessage GetInfo([FromUri] long id)
        {
            throw new NotImplementedException();
        }
       
        public override HttpResponseMessage update([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }

        //[Route("getOffers")]
        //[HttpGet]
        //public JsonResult<ResponseWrapper> GetItems()
        //{
        //    String json = "[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"500\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $500 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\"}]";
        //    JavaScriptSerializer serializer = new JavaScriptSerializer();
        //    var jsonObject = serializer.Deserialize<dynamic>(json);
        //    return Json(new ResponseWrapper(status: 200, desc: "Ok", obj: jsonObject));
        //}

        public override HttpResponseMessage save([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }

        [Route("getLuckyItemOfferForKiosk")]
        [HttpGet]
        //Default offer for kiosk's lucky item
        public HttpResponseMessage getLuckyItemOfferForKiosk([FromUri] Int64 kiosk_id, [FromUri] Int64 lucky_item_id)
        {
            List<Offer> offer = DALmanager.getLuckyItemOfferForKiosk(kiosk_id, lucky_item_id);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
        }

        [Route("getLuckyItemCurrentActiveOfferForKiosk")]
        [HttpGet]
        //Current active offer for kiosk's lucky item
        public HttpResponseMessage getLuckyItemCurrentActiveOfferForKiosk([FromUri] Int64 kiosk_id, [FromUri] Int64 lucky_item_id)
        {
            List<Offer> offer = DALmanager.getLuckyItemCurrentActiveOfferForKiosk(kiosk_id, lucky_item_id);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
        }
    }
}
