﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BNS.WebAPI.Controllers.BNSControllers
{
    public abstract class BNSController : ApiController
    {
        //public ConversionHelper conversionHelper = new ConversionHelper();       
        [Route("all")]
        [HttpGet]
        public abstract HttpResponseMessage GetAll();
       
        [Route("details")]
        [HttpGet]
        public abstract HttpResponseMessage GetInfo([FromUriAttribute] Int64 id);
       
        [Route("save")]
        [HttpGet]

        public abstract HttpResponseMessage save([FromBodyAttribute] JObject requestJSON);
       
        [Route("update")]
        [HttpGet]
        public abstract HttpResponseMessage update([FromBodyAttribute] JObject requestJSON);

      
        [Route("delete")]
        [HttpGet]
        public abstract HttpResponseMessage delete([FromBodyAttribute] JObject requestJSON);

       
        [Route("deleteMultiple")]
        [HttpGet]
        public abstract HttpResponseMessage deleteMultiple([FromBodyAttribute] JObject requestJSON);
    }
}
