﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BNS.WebAPI.Controllers.BNSControllers;
using Newtonsoft.Json.Linq;
using BNS.DAL.DALContact;
using BNS.Wrapper;
using BNS.Models.Accounts;
using BNS.Models.Users;
using BNS.Models.Contacts;
using BNS.BL.BLContact;

namespace BNS.WebAPI.Controllers.ContactControllers
{
    public class ContactController : BNSController
    {
        ContactBL BLmanager = new ContactBL();
        DALContactManager DALmanager = new DALContactManager();     
        ResponseWrapper responseWrapper = new ResponseWrapper();

        //Register new user
        [Route("Register")]
        [HttpGet]
        public override HttpResponseMessage save([FromBody] JObject requestJSON)
        {
            Account account = new Account();
            account.id = 1;
            User requestUser = new User();
            requestUser.id = 1;

            String emailId = (String)requestJSON["email_id"];
            String Phone = (String)requestJSON["phone"];
            Int64 countryId = (Int64)requestJSON["country_id"];

            if (DALmanager.checkIfEmailIdExists(emailId, account.id))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Your Email Id is already registered with us.");
            }
            else
            {
                Contact EmailContact = new Contact();
                EmailContact.value = emailId;
                EmailContact.country.id = countryId;
                EmailContact.createdBy = requestUser;
                EmailContact.updatedBy = requestUser;

                Contact PhoneContact = new Contact();
                PhoneContact.value = Phone;
                PhoneContact.country.id = countryId;
                PhoneContact.createdBy = requestUser;
                PhoneContact.updatedBy = requestUser;

                if (BLmanager.saveContact(EmailContact,PhoneContact))
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Registration Successful!");
                }
                else
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Registration Failed!");
                }                                            
            }
        }

        public override HttpResponseMessage delete([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }

        public override HttpResponseMessage deleteMultiple([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }

        public override HttpResponseMessage GetAll()
        {
            throw new NotImplementedException();
        }

        public override HttpResponseMessage GetInfo([FromUri] long id)
        {
            throw new NotImplementedException();
        }

        

        public override HttpResponseMessage update([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }
    }
}
