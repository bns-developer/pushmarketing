﻿using BNS.Wrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Script.Serialization;
using BNS.WebAPI.Controllers.BNSControllers;
using Newtonsoft.Json.Linq;
using BNS.Models.Accounts;
using BNS.Models.LuckyItems;
using BNS.DAL.DALLuckyItem;
using BNS.Models.Kiosks;



namespace BNS.API.Controllers.Lucky_ItemControllers
{
    [RoutePrefix("api/v1/luckyItems")]
    public class LuckyItemController : BNSController
    {

        DALLuckyItemManager DALmanager = new DALLuckyItemManager();
        ResponseWrapper responseWrapper = new ResponseWrapper();

        [Route("byKioskId")]
        [HttpGet]
        //All Lucky Items by kioskId
        public  HttpResponseMessage LuckyItemsByKioskId([FromUri]Int64 kioskId)
        {
            Kiosk kiosk = new Kiosk();
            kiosk.id = kioskId;
            kiosk = DALmanager.getKioskLuckyItems(kiosk);
            var response = Request.CreateResponse(HttpStatusCode.OK, kiosk);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, kiosk);
        }

        [Route("byluckyItemId")]
        [HttpGet]
        public HttpResponseMessage LuckyItemsById([FromUri]Int64 luckyItemId, [FromUri]Int64 kioskId)
        {
            Kiosk kiosk = new Kiosk();
            kiosk.id = kioskId;
            kiosk = DALmanager.getLuckyItem(kiosk, luckyItemId);
            var response = Request.CreateResponse(HttpStatusCode.OK, kiosk);
            return response;
        }

        public override HttpResponseMessage GetAll()
        {
            throw new NotImplementedException();
        }

        public override HttpResponseMessage delete([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }

        public override HttpResponseMessage deleteMultiple([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }      

        public override HttpResponseMessage GetInfo([FromUri] long id)
        {
            throw new NotImplementedException();
        }

        //[Route("getItems")]
        //[HttpGet]
        //public JsonResult<ResponseWrapper> GetItems()
        //{
        //    String json = "[{\"id\":1,\"name\":\"Coin\",\"icon\":\"~/Resources/Images/coin.png\",\"offer\":[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"1000\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $1000 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\",\"short_url\":\"https://www.google.com\"}]},{\"id\":2,\"name\":\"4 Leaf Clover\",\"icon\":\"~/Resources/Images/4-leaf-clover.png\",\"offer\":[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"500\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $500 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\",\"short_url\":\"https://www.google.com\"}]},{\"id\":3,\"name\":\"Horse Shoe\",\"icon\":\"~/Resources/Images/horse-shoe.png\",\"offer\":[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"500\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $500 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\",\"short_url\":\"https://www.google.com\"}]},{\"id\":4,\"name\":\"Number\",\"icon\":\"~/Resources/Images/number.png\",\"offer\":[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"500\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $500 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\",\"short_url\":\"https://www.google.com\"}]},{\"id\":5,\"name\":\"Day\",\"icon\":\"~/Resources/Images/day.png\",\"offer\":[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"500\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $500 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\",\"short_url\":\"https://www.google.com\"}]},{\"id\":6,\"name\":\"Rainbow\",\"icon\":\"~/Resources/Images/rainbow.png\",\"offer\":[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"500\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $500 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\",\"short_url\":\"https://www.google.com\"}]},{\"id\":7,\"name\":\"Surprise\",\"icon\":\"~/Resources/Images/surprise.png\",\"offer\":[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"500\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $500 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\",\"short_url\":\"https://www.google.com\"}]},{\"id\":8,\"name\":\"Rabbit Foot\",\"icon\":\"~/Resources/Images/rabbit-foot.png\",\"offer\":[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"500\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $500 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\",\"short_url\":\"https://www.google.com\"}]},{\"id\":9,\"name\":\"Pot of Gold\",\"icon\":\"~/Resources/Images/pot-of-gold.png\",\"offer\":[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"500\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $500 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\",\"short_url\":\"https://www.google.com\"}]},{\"id\":10,\"name\":\"Wheel of Fortune\",\"icon\":\"~/Resources/Images/wheel-of-fortune.png\",\"offer\":[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"500\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $500 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\",\"short_url\":\"https://www.google.com\"}]},{\"id\":11,\"name\":\"Money\",\"icon\":\"~/Resources/Images/money.png\",\"offer\":[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"500\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $500 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\",\"short_url\":\"https://www.google.com\"}]},{\"id\":12,\"name\":\"Charm\",\"icon\":\"~/Resources/Images/charm.png\",\"offer\":[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"500\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $500 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\",\"short_url\":\"https://www.google.com\"}]},{\"id\":13,\"name\":\"Color\",\"icon\":\"~/Resources/Images/color.png\",\"offer\":[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"500\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $500 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\",\"short_url\":\"https://www.google.com\"}]},{\"id\":14,\"name\":\"Birthday\",\"icon\":\"~/Resources/Images/birthday.png\",\"offer\":[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"500\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $500 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\",\"short_url\":\"https://www.google.com\"}]},{\"id\":15,\"name\":\"Dragon\",\"icon\":\"~/Resources/Images/dragon.png\",\"offer\":[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"500\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $500 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\",\"short_url\":\"https://www.google.com\"}]},{\"id\":16,\"name\":\"Pet\",\"icon\":\"~/Resources/Images/pet.png\",\"offer\":[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"500\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $500 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\",\"short_url\":\"https://www.google.com\"}]},{\"id\":17,\"name\":\"Dream\",\"icon\":\"~/Resources/Images/dream.png\",\"offer\":[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"500\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $500 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\",\"short_url\":\"https://www.google.com\"}]},{\"id\":18,\"name\":\"Winner\",\"icon\":\"~/Resources/Images/winner.png\",\"offer\":[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"500\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $500 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\",\"short_url\":\"https://www.google.com\"}]},{\"id\":19,\"name\":\"Pick One\",\"icon\":\"~/Resources/Images/pick-one.png\",\"offer\":[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"500\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $500 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\",\"short_url\":\"https://www.google.com\"}]},{\"id\":20,\"name\":\"Extra Toy\",\"icon\":\"~/Resources/Images/extra-toy.png\",\"offer\":[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"500\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $500 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\",\"short_url\":\"https://www.google.com\"}]}]";
        //    //String json = "[{\"id\":1,\"name\":\"Coin\",\"icon\":\"~/Resources/Images/extra-toy.png\",\"offer\":[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"500\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $500 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\",\"short_url\":\"https://www.google.com\"}]},{\"id\":2,\"name\":\"4 Leaf Clover\",\"icon\":\"~/Resources/Images/4-leaf-clover.png\",\"offer\":[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"500\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $500 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\",\"short_url\":\"https://www.google.com\"}]},{\"id\":3,\"name\":\"Horse Shoe\",\"icon\":\"~/Resources/Images/horse-shoe.png\",\"offer\":[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"500\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $500 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\",\"short_url\":\"https://www.google.com\"}]},{\"id\":4,\"name\":\"Number\",\"icon\":\"~/Resources/Images/number.png\",\"offer\":[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"500\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $500 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\",\"short_url\":\"https://www.google.com\"}]},{\"id\":5,\"name\":\"Day\",\"icon\":\"~/Resources/Images/day.png\",\"offer\":[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"500\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $500 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\",\"short_url\":\"https://www.google.com\"}]},{\"id\":6,\"name\":\"Rainbow\",\"icon\":\"~/Resources/Images/rainbow.png\",\"offer\":[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"500\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $500 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\",\"short_url\":\"https://www.google.com\"}]},{\"id\":7,\"name\":\"Surprise\",\"icon\":\"~/Resources/Images/surprise.png\",\"offer\":[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"500\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $500 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\",\"short_url\":\"https://www.google.com\"}]},{\"id\":8,\"name\":\"Rabbit Foot\",\"icon\":\"~/Resources/Images/rabbit-foot.png\",\"offer\":[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"500\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $500 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\",\"short_url\":\"https://www.google.com\"}]},{\"id\":9,\"name\":\"Pot of Gold\",\"icon\":\"~/Resources/Images/pot-of-gold.png\",\"offer\":[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"500\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $500 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\",\"short_url\":\"https://www.google.com\"}]},{\"id\":1,\"name\":\"Coin\",\"icon\":\"~/Resources/Images/coin.png\",\"offer\":[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"500\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $500 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\",\"short_url\":\"https://www.google.com\"}]}]";

        //    JavaScriptSerializer serializer = new JavaScriptSerializer();
        //    var jsonObject = serializer.Deserialize<dynamic>(json);
        //    return Json(new ResponseWrapper(status: 200, desc: "Ok", obj: jsonObject));
            
        //}

        public override HttpResponseMessage save([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }

        public override HttpResponseMessage update([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }

        //Lucky Items for Kiosk by kioskId
        [Route("getLuckyItemsForKiosk")]
        [HttpGet]

        public HttpResponseMessage getLuckyItemsForKiosk([FromUri]Int64 kioskId)
        {
            Kiosk kiosk = new Kiosk();
            kiosk.id = kioskId;
            kiosk = DALmanager.getLuckyItemsForKiosk(kiosk);
            var response = Request.CreateResponse(HttpStatusCode.OK, kiosk);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, kiosk);
        }
    }
}
