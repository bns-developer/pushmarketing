﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BNS.Wrapper;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;
using System.Web.Http.Results;
using BNS.WebAPI.Controllers.BNSControllers;
using BNS.BL.BLKiosk;
using BNS.DAL.DALKiosk;
using BNS.Models.Accounts;
using BNS.Models.Users;
using BNS.Models.Kiosks;
using BNS.Models.Stores;
using Newtonsoft.Json;
using BNS.WebAPI.Authorization;

namespace BNS.API.Controllers.KioskControllers
{
    [RoutePrefix("api/v1/kiosks")]
    public class KioskController : BNSController
    {
        KioskBL BLmanager = new KioskBL();
        DALKioskManager DALmanager = new DALKioskManager();
        ResponseWrapper responseWrapper = new ResponseWrapper();

        [Route("all")]
        [HttpGet]
        [Authorized]
        //All kiosks by accountId
        public override HttpResponseMessage GetAll()
        {
            Account account = new Account();
            account.id = 1;
            List<Kiosk> kiosks = DALmanager.getAll(account);
            var response = Request.CreateResponse(HttpStatusCode.OK, kiosks);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, kiosks);
        }

        public override HttpResponseMessage delete([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }

        public override HttpResponseMessage deleteMultiple([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }

        public override HttpResponseMessage GetInfo([FromUri] long id)
        {
            throw new NotImplementedException();
        }

        

        public override HttpResponseMessage update([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }


        //[Route("addKiosk")]
        //[HttpPost]
        //public JsonResult<ResponseWrapper> addKiosk([FromBody]JObject kiosk) 
        //{
        //    try
        //    {
        //        Int64 storename = (Int64)kiosk["store_id"];
        //        String kioskname = (string)kiosk["kiosk_name"];

        //        if (!storename.Equals(null) && !kioskname.Equals(null))
        //        {
        //            String json = "{\"id\":125}";
        //            JavaScriptSerializer serializer = new JavaScriptSerializer();
        //            var jsonObject = serializer.Deserialize<dynamic>(json);
        //            return Json(new ResponseWrapper(status: 200, desc: "Successfully Created", obj: jsonObject));                               
        //        }
        //        else
        //        {
        //            String json = "{\"id\":0}";
        //            JavaScriptSerializer serializer = new JavaScriptSerializer();
        //            var jsonObject = serializer.Deserialize<dynamic>(json);
        //            return Json(new ResponseWrapper(status: 400, desc: "Missing data", obj: jsonObject));                   
        //        }
        //    }
        //    catch(NullReferenceException Ex)
        //    {
        //        String json = "{\"id\":0}";
        //        JavaScriptSerializer serializer = new JavaScriptSerializer();
        //        var jsonObject = serializer.Deserialize<dynamic>(json);
        //        return Json(new ResponseWrapper(status: 400, desc: "Missing data", obj: jsonObject));
        //    }           

        //}

     
        [Route("add")]
        [HttpPost]
        [Authorized]
        //kiosk will call this API to add itself
        public override HttpResponseMessage save([FromBody] JObject requestJSON)
        {
            User requestUser = new User();
            requestUser.id = 1;

            JObject storeJObject = (JObject)requestJSON["store"];
            Store store = new Store(storeJObject);
            Int64 storeId = store.id;

            String kioskName = (String)requestJSON["name"];
            //String kioskNumber = (String)requestJSON["number"];
            Kiosk newKiosk = new Kiosk(requestJSON);

            if (DALmanager.checkIfNameExists(kioskName, storeId))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Kiosk name already exist for this store.");

            }

            //else if (DALmanager.checkIfNumberExists(kioskNumber))
            //{

            //    return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Kiosk number already exist.");
            //}
            else
            {
                newKiosk.createdBy = requestUser;
                newKiosk.updatedBy = requestUser;
                Int64 id = BLmanager.save(newKiosk);
                if ( id != 0)
                {
                    JObject kioskId = new JObject();
                    kioskId["id"] = id;
                        
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, kioskId);
                }
                else
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Kiosk Not Saved");
                }
            }
        }

    }
}
