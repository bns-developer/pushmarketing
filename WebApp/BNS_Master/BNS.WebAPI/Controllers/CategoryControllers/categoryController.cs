﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BNS.DAL.DALCategory;
using BNS.Wrapper;
using BNS.Models.Accounts;
using BNS.Models.Categories; 

namespace BNS.WebAPI.Controllers.CategoryControllers
{

    [RoutePrefix("api/v1/Categories")]
    public class CategoryController : ApiController
    {
        DALCategoryManager manager = new DALCategoryManager();
        ResponseWrapper responseWrapper = new ResponseWrapper();       

        [Route("all")]
        [HttpGet]
        public HttpResponseMessage GetAll()
        {
            Account account = new Account();
            account.id = 1;
            List<Category> CategoriesList = manager.getAll(account);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, CategoriesList);
        }
    }
}
