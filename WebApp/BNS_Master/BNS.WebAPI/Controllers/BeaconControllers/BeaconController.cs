﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BNS.WebAPI.Controllers.BNSControllers;
using Newtonsoft.Json.Linq;
using BNS.Models.Accounts;
using BNS.Models.Beacons;
using BNS.Wrapper;
using BNS.BL.BLBeacon;
using BNS.DAL.DALBeacon;
using BNS.Models.Users;
using BNS.Models.Stores;

namespace BNS.WebAPI.Controllers.BeaconControllers
{
    [RoutePrefix("api/v1/Beacons")]
    public class BeaconController : BNSController
    {
        BeaconBL BLmanager = new BeaconBL();
        DALBeaconManager DALmanager = new DALBeaconManager();
        ResponseWrapper responseWrapper = new ResponseWrapper();

        //Add new beacon
        [Route("add")]
        [HttpPost]
        public override HttpResponseMessage save([FromBody] JObject requestJSON)
        {
            User requestUser = new User();
            requestUser.id = 1;

            JObject storeJObject = (JObject)requestJSON["store"];
            Store store = new Store(storeJObject);
            Int64 storeId = store.id;

            String beaconName = (String)requestJSON["name"];
            String beaconNumber = (String)requestJSON["number"];
            Beacon newBeacon = new Beacon(requestJSON);
            

            if (DALmanager.checkIfNameExists(beaconName, storeId))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Beacon name already exist for this store.");

            }

            else if (DALmanager.checkIfNumberExists(beaconNumber))
            {

                return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Beacon number already exist.");
            }
            else
            {
                newBeacon.createdBy = requestUser;
                newBeacon.updatedBy = requestUser;

                if (BLmanager.save(newBeacon))
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Beacon  Saved");
                }
                else
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Beacon Not Saved");
                }
            }
        }

        //All beacons by accountId
        [Route("getAll")]
        [HttpGet]
        public override HttpResponseMessage GetAll()
        {
            Account account = new Account();
            account.id = 1;
            List<Beacon> beacons = DALmanager.getAll(account);
            var response = Request.CreateResponse(HttpStatusCode.OK, beacons);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, beacons);
        }

        //Beacon details
        [Route("details")]
        [HttpGet]
        public override HttpResponseMessage GetInfo([FromUri]Int64 beaconId)
        {
            Beacon beacon = new Beacon();
            beacon.id = beaconId;
            beacon = DALmanager.getDetails(beacon);
            var response = Request.CreateResponse(HttpStatusCode.OK, beacon);
            return response;
        }

        public override HttpResponseMessage delete([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }

        public override HttpResponseMessage deleteMultiple([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }     

        public override HttpResponseMessage update([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }
    }
}
