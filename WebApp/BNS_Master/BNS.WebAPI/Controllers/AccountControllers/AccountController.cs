﻿using System;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Script.Serialization;
using BNS.Wrapper;

namespace BNS.API.Controllers.AccountControllers
{ 
    [RoutePrefix("api/v1/accounts")]
    public class AccountController : ApiController
    {
        
        [HttpGet]
        [Route("GetAccount")]
        public JsonResult<ResponseWrapper> GetAccount()
        {

            String json = "{\"id\":101,\"name\":\"Novomatic\",\"email\":\"novomatic@gmail.com\",\"password\":\"pass@novomatic\",\"pic_url\":\"~/Resources/Images/Novo2.png\"}";
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var jsonObject = serializer.Deserialize<dynamic>(json);
            return Json(new ResponseWrapper(status: 200, desc: "Ok", obj: jsonObject));
        }
    }
}
