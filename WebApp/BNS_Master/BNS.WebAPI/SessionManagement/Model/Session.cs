﻿using BNS.DL.WSDatabase;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace BNS.WebAPI.SessionManagement
{
    public class Session 
    {
        public Int64 id { get; set; }

        public String token { get; set; }

        public Int64 user { get; set; }

        public Int64 expiryTime { get; set; }

        public Session(SqlDataReader reader)
        {
            if (WSDatabaseHelper.isValidField(reader, "SESSION_ID"))
                this.id = reader.GetInt64(reader.GetOrdinal("SESSION_ID"));

            if (WSDatabaseHelper.isValidField(reader, "SESSIONUSER"))
                this.user = reader.GetInt64(reader.GetOrdinal("SESSIONUSER"));

            if (WSDatabaseHelper.isValidField(reader, "SESSION_TOKEN"))
                this.token = reader.GetString(reader.GetOrdinal("SESSION_TOKEN"));
            
            if (WSDatabaseHelper.isValidField(reader, "SESSION_EXPIRYTIME"))
                this.expiryTime = reader.GetInt64(reader.GetOrdinal("SESSION_EXPIRYTIME"));
        }

        public Session()
        {
            this.token = Guid.NewGuid().ToString("N");
            this.expiryTime = 600;
        }

        public Dictionary<string,Object> toDictionary()
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters["sessionUser"] = this.user;
            parameters["sessionKey"] = this.token;
            parameters["expiryTime"] = this.expiryTime;      
            return parameters;
        }
    }
}