﻿using BNS.WebAPI.SessionManagement;
using BNS.Wrapper;
using System.Net;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace BNS.WebAPI.Authorization
{
    public class AuthorizedAttribute : AuthorizationFilterAttribute
    {

        ResponseWrapper responseWrapper = new ResponseWrapper();
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (!SessionManager.sharedInstance.isAuthorized(actionContext))
                actionContext.Response = responseWrapper.CreateResponse(actionContext.Request, HttpStatusCode.Forbidden, "Session Has Been Expired. Please Log-In Again!!");
        }
        public bool Authorize(System.Security.Principal.IPrincipal principal) { return true; }
    }
}