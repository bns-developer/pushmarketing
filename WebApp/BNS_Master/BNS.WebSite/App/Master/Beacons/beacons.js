angular.module('bns.master.beacons', [])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.beacons', {
        url: '/beacons',
        views: {
            "beacons": {
                templateUrl: 'App/Master/Beacons/beacons.html',
                controller: 'beaconsController'
            }
        }

    });
}])
.controller("beaconsController", ['$scope','$http', 'API_URL',
    function ($scope,$http, API_URL) {
        var table_advanced = function() {

        var handleDatatable = function(){
            var spinner = $( ".spinner" ).spinner();
            var table = $('#table_id').dataTable( {
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "order": [[ 1, "asc" ]],
                "aoColumnDefs" : [ {
                    'bSortable' : false,
                    'aTargets' : [ 0 ]
                } ]
            } );

            var tableTools = new $.fn.dataTable.TableTools( table, {
                "sSwfPath": "libs/assets/vendors/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                "buttons": [
                    "copy",
                    "csv",
                    "xls",
                    "pdf",
                    { "type": "print", "buttonText": "Print me!" }
                ]
            } );
            $(".DTTT_container").css("float","right");
        };
        return{
            init: function () {
                handleDatatable();
            }
        };
    }(jQuery);
    
    setTimeout(function(){
        table_advanced.init();
    },500);
    }])