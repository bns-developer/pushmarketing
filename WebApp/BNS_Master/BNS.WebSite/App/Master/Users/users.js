angular.module('bns.master.users', [])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.users', {
        url: '/users',
        views: {
            "users": {
                templateUrl: 'App/Master/Users/users.html',
                controller: 'usersController'
            }
        }

    });
}])
.controller("usersController", ['$scope','$http', 'API_URL',
    function ($scope,$http, API_URL) {
var table_advanced = function() {

        var handleDatatable = function(){
            var spinner = $( ".spinner" ).spinner();
            var table = $('#table_id').dataTable( {
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "order": [[ 1, "asc" ]],
                "aoColumnDefs" : [ {
                    'bSortable' : false,
                    'aTargets' : [ 0 ]
                } ]
            } );

            var tableTools = new $.fn.dataTable.TableTools( table, {
                "sSwfPath": "../assets/vendors/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                "buttons": [
                    "copy",
                    "csv",
                    "xls",
                    "pdf",
                    { "type": "print", "buttonText": "Print me!" }
                ]
            } );
            $(".DTTT_container").css("float","right");
        };
        return{
            init: function () {
                handleDatatable();
            }
        };
    }(jQuery);
    
    setTimeout(function(){
        table_advanced.init();
    },500);
    }])