angular.module('bns.master.offers', ['oc.lazyLoad'])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.offers', {
        url: '/offers',
        views: {
            "offers": {
                templateUrl: 'App/Master/Offers/offers.html',
                controller: 'offersController'                
            }
        }

    });
}])
    .directive('fileInput', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attributes) {
                element.bind('change', function () {
                    $parse(attributes.fileInput)
                    .assign(scope, element[0].files)
                    scope.$apply()
                });
            }
        };
    }])
.controller("offersController", ['$scope','$http', 'API_URL','ngDialog','API_SERVICE',function ($scope,$http, API_URL, ngDialog,API_SERVICE) {

    $scope.submitted = false;
    $scope.dateError = false;
    $scope.successAlert = false;
    $scope.faliureAlert = false;
    $scope.image = "";

    $('#offerDatetime').daterangepicker({
        timePicker: true,
        timePickerIncrement: 1,
        format: 'MM/DD/YYYY h:mm:ss A',
        minDate: new Date(),
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear'
        },
        "startDate": new Date(),
        "endDate": new Date
    });
    $scope.newOffer = {
        "categories": "",
        "countries": "",
        "title": "",
        "description": "",
        "startTime": "",
        "endTime": "",
        "number": "",
        "imagePath": "",
        "value": ""
    };

        var table_advanced = function() {

        var handleDatatable = function(){
            var spinner = $( ".spinner" ).spinner();
            var table = $('#table_id').dataTable( {
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "order": [],
                "aoColumnDefs" : [ {
                    'bSortable' : false,
                    'aTargets' : [ 0 ]
                },
                {
                    'bSortable': false,
                    'aTargets': [9]
                }]
            } );

            var tableTools = new $.fn.dataTable.TableTools( table, {
                "sSwfPath": "libs/assets/vendors/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                "buttons": [
                    "copy",
                    "csv",
                    "xls",
                    "pdf",
                    { "type": "print", "buttonText": "Print me!" }
                ]
            } );
            $(".DTTT_container").css("float","right");
            
        };
        return{
            init: function () {
                handleDatatable();
            }
        };
    }(jQuery);
    
    setTimeout(function(){
        table_advanced.init();
        $('input[name="daterangepicker-date-time"]').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' });
        $('.selectpicker').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check'
        });
    },5000);

    $scope.addOffer = function(){        
        ngDialog.open({
                template: 'addOfferDialog',
                scope: $scope,
                preCloseCallback: function () { $scope.clearModal() }
            });
    }

    $scope.closeModal = function () {
        alert("Cancel Clicked...!!!");
        ngDialog.close({
            template: 'addStoreDialog',
            scope: $scope
        });
        $scope.clearModal();
    }

    $scope.clearModal = function () {
        $scope.submitted = false;
        $scope.successAlert = false;
        $scope.faliureAlert = false;
        $scope.date = "";
        $scope.newOffer = {
            "categories": [],
            "countries": [],
            "title": "",
            "description": "",
            "startTime": "",
            "endTime": "",
            "number": "",
            "imagePath": "",
            "value": ""
        };
        $('#offerDatetime').val('');
        $scope.dateError = false;
    }
    
    $scope.checkDate = function () {
        $scope.newOffer.startTime = ($('#offerDatetime').val()).split('-')[0];
        $scope.newOffer.endTime = ($('#offerDatetime').val()).split('-')[1];
        if($scope.newOffer.startTime == "" || $scope.newOffer.endTime == "" || $scope.newOffer.startTime == undefined || $scope.newOffer.endTime == undefined )
        {
            $scope.dateError = true;
        }
        else
        {
            $scope.dateError = false;
        }
    }

    $scope.saveNewOffer = function () {
        console.log($('#offerDatetime').val());
        console.log($scope.image.file);
        $scope.submitted = true;
        $scope.newOffer.startTime = ($('#offerDatetime').val()).split('-')[0];
        $scope.newOffer.endTime = ($('#offerDatetime').val()).split('-')[1];
        $scope.newOffer.imagePath = $scope.image[0];
        if ($scope.newOffer.categories == "" || $scope.newOffer.categories == undefined || $scope.newOffer.countries == "" || $scope.newOffer.countries == undefined || $scope.newOffer.title == ""
            || $scope.newOffer.title == undefined || $scope.newOffer.description == "" || $scope.newOffer.description == undefined || $scope.newOffer.startTime == "" || $scope.newOffer.startTime == undefined
            || $scope.newOffer.endTime == "" || $scope.newOffer.endTime == undefined)
        {
            if ($scope.newOffer.startTime == "" || $scope.newOffer.startTime == undefined
            || $scope.newOffer.endTime == "" || $scope.newOffer.endTime == undefined)
            {
                $scope.dateError = true;
            }
            else {
                $scope.dateError = false;
            }
        }
        else {
            $scope.dateError = false;
            function success(data, status, headers, config) {
                getAllOffersList();
                $scope.submitted = false;
                $scope.successAlert = true;
                $scope.faliureAlert = false;
                $scope.newOffer = {
                    "category": [],
                    "country": [],
                    "title": "",
                    "description": "",
                    "startTime": "",
                    "endTime": "",
                    "number": "",
                    "imagePath": "",
                    "value": ""
                };
                $('#offerDatetime').val('');
                $scope.loading = false;
            }
            function failure(data, status, headers, config) {
                $scope.loading = false;
                $scope.successAlert = false;
                $scope.faliureAlert = true;
                if (status == 409) {
                    $scope.errorMessage = data;
                }
                else {
                    $scope.errorMessage = "Could not add offer to the list. Please check all the fields."
                }
            }
            var url = "/Offers/addOffer";
            //API_SERVICE.postMultiPartData($scope, $http, $scope.newOffer, url, success, failure);
            API_SERVICE.postData($scope, $http, $scope.newOffer, url, success, failure);
        }
    }

    function getAllOffersList() {
        $scope.loading = true;
        function success(data, status, headers, config) {
            $scope.offers = data;
            console.log($scope.offers);
            $scope.loading = false;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
        }
        var url = "/Offers/all";
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getAllOffersList();

    function getAllCountries() {
        $scope.loading = true;
        function success(data, status, headers, config) {
            $scope.countries = data;
            console.log($scope.stores);
            $scope.loading = false;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
        }
        var url = "/countries/all";
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getAllCountries();

    function getAllCategories() {
        $scope.loading = true;
        function success(data, status, headers, config) {
            $scope.categories = data;
            console.log($scope.categories);
            $scope.loading = false;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
        }
        var url = "/Categories/all";
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getAllCategories();

}]);