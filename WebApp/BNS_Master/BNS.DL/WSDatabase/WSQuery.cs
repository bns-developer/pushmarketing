﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BNS.DL.WSDatabase
{ 
    public class WSQuery
    {
        public String query { get; set; }

        public WSRecordsFilter filter { get; set; }

        public Dictionary<String, Object> parameters { get; set; }

        public WSTransaction transaction;

        public SqlConnection connection;

        public static string ConString = @"Data Source = DESKTOP-VSQ5UIO\SQLEXPRESS; Initial Catalog = BNSdb; User ID = root; Password = root; MultipleActiveResultSets=True";

        public WSQuery()
        {
            connectToDatabase();
            this.parameters = new Dictionary<String, Object>();
        }

        public WSQuery(string query)
        {
            connectToDatabase();
            this.query = query;
            this.parameters = new Dictionary<String, Object>();
        }
        public WSQuery(string query, Dictionary<String, Object> parameters)
        {
            connectToDatabase();
            this.query = query;
            this.parameters = parameters;
        }

        public WSQuery(string query, Dictionary<String, Object> parameters, WSTransaction transaction)
        {
            connectToDatabase();
            this.query = query;
            this.parameters = parameters;
            this.transaction = transaction;
        }

        private void connectToDatabase()
        {
            connection = new SqlConnection(ConString);
            connection.Open();
        }
    }
}