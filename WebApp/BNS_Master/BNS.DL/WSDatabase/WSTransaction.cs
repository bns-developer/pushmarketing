﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using BNS.DL.WSDatabase;

namespace BNS.DL.WSDatabase
{
    public class WSTransaction : IDisposable
    {
        public SqlTransaction SQLtransaction;
        public SqlConnection SQLconnection;
        public WSTransaction(String name)
        {
            string ConString = @"Data Source = DESKTOP-VSQ5UIO\SQLEXPRESS; Initial Catalog = BNSdb; User ID = root; Password = root; MultipleActiveResultSets=True";
            SQLconnection = new SqlConnection(ConString);
            SQLconnection.Open();
            SQLtransaction = SQLconnection.BeginTransaction(name);

        }
        public void commit()
        {
            SQLtransaction.Commit();
            SQLconnection.Close();
        }
        public void rollback()
        {
            SQLtransaction.Rollback();
            SQLconnection.Close();
        }

        public void Dispose()
        {
            SQLtransaction.Dispose();
            SQLconnection.Dispose();
        }
    }
}