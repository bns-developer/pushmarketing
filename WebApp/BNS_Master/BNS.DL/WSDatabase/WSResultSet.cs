﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BNS.DL.WSDatabase
{
    public class ResultSet
    {
        public SqlDataReader reader { get; set; }
        public ResultSet(SqlDataReader reader)
        {
            this.reader = reader;
        }

    }
}