﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


namespace BNS.DL.WSDatabase
{
    public class WSDatabaseManager
    {
        private static WSDatabaseManager instance;
        public static SqlConnection Connection { get; set; }

        public static string ConString = @"Data Source = DESKTOP-VSQ5UIO\SQLEXPRESS; Initial Catalog = BNSdb; User ID = root; Password = root; MultipleActiveResultSets=True";

        private WSDatabaseManager()
        {
            connectToDatabase();
        }

        public static WSDatabaseManager sharedDatabaseManagerinstance
        {
            get
            {
                if (instance == null)
                {
                    instance = new WSDatabaseManager();
                }
                return instance;
            }
        }

        private void connectToDatabase()
        {
            //string ConString = @"Data Source=DESKTOP-I260SRF\SQLEXPRESS;Initial Catalog=BNSdb;Integrated Security=True;MultipleActiveResultSets=true";          
            Connection = new SqlConnection(ConString);
            Connection.Open();
        }

        public Boolean login(Dictionary<String, Object> queryParameter)
        {
            string query = @"SELECT* FROM Users WHERE email = @email AND password = @password";
            SqlCommand command = new SqlCommand(query, Connection);
            command.Parameters.AddWithValue("@email", queryParameter["username"]);
            command.Parameters.AddWithValue("@password", queryParameter["password"]);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
                return true;
            else
                return false;
        }
        public Boolean insertRecordIntoDatabase(String query, Dictionary<String, Object> queryParameter)
        {
            SqlCommand command = new SqlCommand(query, Connection);

            foreach (String key in queryParameter.Keys)
            {
                String paramkey = "@" + key;
                command.Parameters.AddWithValue(paramkey, queryParameter[key]);
            }
            int rows_affected = command.ExecuteNonQuery();
            if (rows_affected == 0)
                return false;
            else
                return true;
        }

        public Int64 insertRecordIntoDatabaseIfNotExistAndReturnID(String query, Dictionary<String, Object> queryParameter)
        {
            //String query = "IF EXISTS (SELECT * FROM Session WHERE sessionUser = @sessionUser) UPDATE Session SET expiryTime = @expiryTime WHERE sessionUser = @sessionUser  ELSE INSERT INTO Session (sessionUser,sessionKey,expiryTime) VALUES (@sessionUser,@sessionKey,@expiryTime)  ";

            SqlCommand command = new SqlCommand(query, Connection);

            foreach (String key in queryParameter.Keys)
            {
                String paramkey = "@" + key;
                command.Parameters.AddWithValue(paramkey, queryParameter[key]);
            }
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    return reader.GetInt64(reader.GetOrdinal("ID"));
                }
                return 0;
            }
            else
            {
                return 0;
            }
        }

        public Int64 insertRecordIntoDatabaseAndReturnID(String query, Dictionary<String, Object> queryParameter)
        {
            SqlCommand command = new SqlCommand(query, Connection);

            foreach (String key in queryParameter.Keys)
            {
                String paramkey = "@" + key;
                command.Parameters.AddWithValue(paramkey, queryParameter[key]);
            }
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    return reader.GetInt64(reader.GetOrdinal("ID"));
                }
                return 0;
            }
            else
            {
                return 0;
            }
        }
        public Boolean updateRecordsInDatabase(String query, Dictionary<String, Object> queryParameter)
        {
            SqlCommand command = new SqlCommand(query, Connection);
            //command.Transaction = transaction;
            foreach (String key in queryParameter.Keys)
            {
                String paramkey = "@" + key;
                command.Parameters.AddWithValue(paramkey, queryParameter[key]);
            }


            int rows_affected = command.ExecuteNonQuery();
            if (rows_affected == 0)
                return false;
            else
                return true;
        }
        public Boolean deleteRecordsFromDatabase(String query, Dictionary<String, Object> queryParameter)
        {
            SqlCommand command = new SqlCommand(query, Connection);
            foreach (String key in queryParameter.Keys)
            {
                String paramkey = "@" + key;
                command.Parameters.AddWithValue(paramkey, queryParameter[key]);
            }


            try
            {
                int rows_affected = command.ExecuteNonQuery();
                if (rows_affected == 0)
                    return false;
                else
                    return true;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return false;

            }
        }
        public SqlDataReader getRecordsFromDatabase(WSQuery Query)
        {
            SqlCommand command = new SqlCommand(Query.query, Connection);

            if(Query.parameters != null)
            {
                foreach (String key in Query.parameters.Keys)
                {
                    String paramkey = "@" + key;
                    command.Parameters.AddWithValue(paramkey, Query.parameters[key]);
                }
            }

            SqlDataReader reader = command.ExecuteReader();
            return reader;
        }


        public Object getValueFromDatabase(String query, Dictionary<String, Object> queryParameter)
        {
            SqlCommand command = new SqlCommand(query, Connection);
            foreach (String key in queryParameter.Keys)
            {
                String paramkey = "@" + key;
                command.Parameters.AddWithValue(paramkey, queryParameter[key]);
            }
            Object result = command.ExecuteScalar();
            return result;
        }
        public bool isExistInDatabase(String query, Dictionary<String, Object> queryParameter)
        {
            SqlCommand command = new SqlCommand(query, Connection);
            foreach (String key in queryParameter.Keys)
            {
                String paramkey = "@" + key;
                command.Parameters.AddWithValue(paramkey, queryParameter[key]);
            }
            Object result = command.ExecuteScalar();
            return (bool)result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////                                       NEW METHODS                                             ////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////        

        public SqlDataReader executeSelectQuery(WSQuery Query)
        {
            SqlCommand command = new SqlCommand(Query.query, Query.connection);
            if (Query.transaction != null)
            {
                command.Transaction = Query.transaction.SQLtransaction;
                command = new SqlCommand(Query.query, Query.transaction.SQLconnection);
            }
            foreach (String key in Query.parameters.Keys)
            {
                String paramkey = "@" + key;
                command.Parameters.AddWithValue(paramkey, Query.parameters[key]);
            }
            //logger.log(query.query + "  Fired on Database.");
            SqlDataReader reader = command.ExecuteReader();
            return reader;
        }

        public object executeScalar(WSQuery Query)
        {
            Object response = null;
            using (SqlConnection connection = new SqlConnection(ConString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(Query.query, Query.connection);
                if (Query.transaction != null)
                {
                    command.Transaction = Query.transaction.SQLtransaction;
                    command = new SqlCommand(Query.query, Query.transaction.connection);
                }
                foreach (String key in Query.parameters.Keys)
                {
                    String paramkey = "@" + key;
                    command.Parameters.AddWithValue(paramkey, Query.parameters[key]);
                }
                //logger.log(query.query + "  Fired on Database.");
                response = command.ExecuteScalar();
                return response;
            }
        }        

        public bool executeUpdateQuery(WSQuery Query)
        {

            SqlCommand command = new SqlCommand(Query.query, Query.connection);
            if (Query.transaction != null)
            {
                command.Transaction = Query.transaction.SQLtransaction;
                command = new SqlCommand(Query.query, Query.transaction.connection);
            }
            foreach (String key in Query.parameters.Keys)
            {
                String paramkey = "@" + key;
                command.Parameters.AddWithValue(paramkey, Query.parameters[key]);
            }
            //logger.log(query.query + "  Fired on Database.");
            int rows_affected = command.ExecuteNonQuery();

            if (Query.connection != null) Query.connection.Close();
            if (rows_affected == 0)
                return false;
            else
                return true;
        }

        public bool executeDeleteQuery(WSQuery Query)
        {
            SqlCommand command = new SqlCommand(Query.query, Query.connection);
            if (Query.transaction != null)
            {
                command.Transaction = Query.transaction.SQLtransaction;
                command = new SqlCommand(Query.query, Query.transaction.connection);
            }


            foreach (String key in Query.parameters.Keys)
            {
                String paramkey = "@" + key;
                command.Parameters.AddWithValue(paramkey, Query.parameters[key]);
            }
            //logger.log(query.query + "  Fired on Database.");
            int rows_affected = command.ExecuteNonQuery();
            if (rows_affected == 0)
                return false;
            else
                return true;
        }

        public Boolean executeInsertQuery(WSQuery Query)
        {
            if (Query.transaction != null)
            {

                SqlCommand command = new SqlCommand(Query.query, Query.connection, Query.transaction.SQLtransaction);
                foreach (String key in Query.parameters.Keys)
                {
                    String paramkey = "@" + key;
                    command.Parameters.AddWithValue(paramkey, Query.parameters[key]);
                    return command.ExecuteNonQuery() != 0;
                }
                return false;
            }
            else
                return false;
        }

        public Int64 executeInsertQueryReturnID(WSQuery Query)
        {
           
            if (Query.transaction != null)
            {
                SqlCommand command = new SqlCommand(Query.query, Query.transaction.connection,Query.transaction.transaction);
                foreach (String key in Query.parameters.Keys)
                {
                    String paramkey = "@" + key;
                    command.Parameters.AddWithValue(paramkey, Query.parameters[key]);

                }
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        return reader.GetInt64(reader.GetOrdinal("ID"));
                    }
                    return 0;
                }
                else
                {
                    return 0;
                }
            }

            return 0;
           
        }

        public Int64 executeInsertOrUpdateQueryReturnID(WSQuery Query)
        {
            SqlCommand command = new SqlCommand(Query.query, Query.connection);
            if (Query.transaction != null)
            {
                command.Transaction = Query.transaction.transaction;
                command = new SqlCommand(Query.query, Query.transaction.connection);
            }
            foreach (String key in Query.parameters.Keys)
            {
                String paramkey = "@" + key;
                command.Parameters.AddWithValue(paramkey, Query.parameters[key]);
            }
            //logger.log(query.query + "  Fired on Database.");
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    return reader.GetInt64(reader.GetOrdinal("ID"));
                }
                return 0;
            }
            else
            {
                return 0;
            }
        }
    }
}