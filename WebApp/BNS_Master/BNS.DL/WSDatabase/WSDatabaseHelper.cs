﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BNS.DL.WSDatabase
{
    public class WSDatabaseHelper
    {
       

        public static Boolean isValidField(SqlDataReader reader, string ColumnName)
        {

            try
            {
                if (reader.IsDBNull(reader.GetOrdinal(ColumnName)))
                {
                    return false;
                }
                else
                {
                    return true;
                }

            }
            catch (IndexOutOfRangeException)
            {
                return false;
            }
        }
    }
}