﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.BNSModels;
using BNS.Models.Stores;
using BNS.Models.Offers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;
using BNS.DL.WSDatabase;
using BNS.Models.Helpers;

namespace BNS.Models.Beacons
{
    public class Beacon : BNSModel
    {

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Store store { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<Offer> offers { get; set; }


        public Beacon(SqlDataReader reader)
        {
            if (WSDatabaseHelper.isValidField(reader, "BEACON_ID"))
            {
                this.id = reader.GetInt64(reader.GetOrdinal("BEACON_ID"));
            }

            if (WSDatabaseHelper.isValidField(reader, "BEACON_NUMBER"))
            {
                this.number = reader.GetString(reader.GetOrdinal("BEACON_NUMBER"));
            }

            if (WSDatabaseHelper.isValidField(reader, "BEACON_NAME"))
            {
                this.name = reader.GetString(reader.GetOrdinal("BEACON_NAME"));
            }

            if (WSDatabaseHelper.isValidField(reader, "BEACON_STORE"))
            {
                store = new Store(reader);
            }

            if (WSDatabaseHelper.isValidField(reader, "OFFER_ID"))
            {
                offers = new List<Offer>();
                offers.Add(new Offer(reader));
            }
        }

        public Beacon(JObject beaconJson)
        {
            if (JSONHelper.isValidFieldForKey(beaconJson, "id"))
                this.id = (Int64)beaconJson["id"];

            if (JSONHelper.isValidFieldForKey(beaconJson, "name"))
                this.name = (String)beaconJson["name"];

            if (JSONHelper.isValidFieldForKey(beaconJson, "number"))
                this.number = (String)beaconJson["number"];

            if (JSONHelper.isValidFieldForKey(beaconJson, "store"))
            {
                JObject storeJObject = (JObject)beaconJson["store"];
                store = new Store(storeJObject);
            }

            offers = new List<Offer>();
        }

        public Beacon()
        {
        }

        public override Dictionary<string, object> toDictionary()
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            if (this.id != 0)
                parameters["id"] = this.id;
            if (this.name != null)
                parameters["name"] = this.name;
            if (this.number != null)
                parameters["number"] = this.number;
            if (store != null)
                parameters["storeId"] = store.id;
        
            parameters["createdBy"] = createdBy.id;
            parameters["updatedBy"] = updatedBy.id;
            parameters["updatedOn"] = DateTime.Now.ToString();
            parameters["createdOn"] = DateTime.Now.ToString();
            return parameters;
        }
        public override void save()
        {
            throw new NotImplementedException();
        }  

        public override void update()
        {
            throw new NotImplementedException();
        }
    }
}