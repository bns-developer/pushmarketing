﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using BNS.DL.WSDatabase;
using Newtonsoft.Json.Linq;
using BNS.Models.Helpers;

namespace BNS.Models.Countries
{
    public class Country
    {
        
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Int64 id { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String name { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String flagPath { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String countryCode { get; set; }

        public Country(JObject countryJson)
        {
            if (JSONHelper.isValidFieldForKey(countryJson, "id"))
               id = (Int64)countryJson["id"];

            if (JSONHelper.isValidFieldForKey(countryJson, "name"))
               name = (String)countryJson["name"];

            if (JSONHelper.isValidFieldForKey(countryJson, "flag_path"))
                flagPath = (String)countryJson["flag_path"];

            if (JSONHelper.isValidFieldForKey(countryJson, "country_code"))
                countryCode = (String)countryJson["country_code"];
        }

        public Country(SqlDataReader reader)
        {
            if (WSDatabaseHelper.isValidField(reader, "COUNTRY_ID"))
            {              
                id = reader.GetInt64(reader.GetOrdinal("COUNTRY_ID"));               
            }
            if (WSDatabaseHelper.isValidField(reader, "COUNTRY_NAME"))
            {                             
                name = reader.GetString(reader.GetOrdinal("COUNTRY_NAME"));         
            }
            if (WSDatabaseHelper.isValidField(reader, "COUNTRY_FLAG_PATH"))
            {            
                flagPath = reader.GetString(reader.GetOrdinal("COUNTRY_FLAG_PATH"));
            }
            if (WSDatabaseHelper.isValidField(reader, "COUNTRY_CODE"))
            {            
                countryCode = reader.GetString(reader.GetOrdinal("COUNTRY_CODE"));            
            }

        }

        public Country()
        {
        }
    }
}