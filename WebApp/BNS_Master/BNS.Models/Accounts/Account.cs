﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.BNSModels;
using BNS.Models.Users;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using BNS.Models.Helpers;

namespace BNS.Models.Accounts
{
    public class Account : BNSModel
    {            
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String userName { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String password { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String picUrl { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public User admin { get; set; }

        public Account(JObject accountJson)
        {
            if (JSONHelper.isValidFieldForKey(accountJson, "id"))
                id = (Int64)accountJson["id"];

            if (JSONHelper.isValidFieldForKey(accountJson, "name"))
                name = (String)accountJson["name"];
        }

        public Account()
        {
        }

        public override void save()
        {
            throw new NotImplementedException();
        }

        public override void update()
        {
            throw new NotImplementedException();
        }

        public override Dictionary<string, object> toDictionary()
        {
            throw new NotImplementedException();
        }
    }
}