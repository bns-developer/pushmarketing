﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.Accounts;
using BNS.Models.BNSModels;
using BNS.Models.Contacts;
using Newtonsoft.Json;



namespace BNS.Models.Users
{
    public class User : BNSModel
    {

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Contact contactId { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Account  accountId { get; set; }
        
        public override void save()
        {
            throw new NotImplementedException();
        }

        public override Dictionary<string, object> toDictionary()
        {
            throw new NotImplementedException();
        }

        public override void update()
        {
            throw new NotImplementedException();
        }
    }
}