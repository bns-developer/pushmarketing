﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.Offers;
using BNS.Models.BNSModels;
using BNS.Models.Accounts;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using BNS.Models.Helpers;
using System.Data.SqlClient;
using BNS.DL.WSDatabase;
using BNS.Models.Countries;
using BNS.Models.Categories;

namespace BNS.Models.Offers
{
    public class Offer : BNSModel
    {


        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Account account { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String imagePath { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String title { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String description { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String startTime { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String endTime { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<Country> countries { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<Category> categories { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public HttpPostedFile file { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String shortUrl { get; set; }       



        public Offer(JObject offerJson)
        {
            if (JSONHelper.isValidFieldForKey(offerJson, "id"))
                this.id = (Int64)offerJson["id"];

            if (JSONHelper.isValidFieldForKey(offerJson, "number"))
                this.number = (String)offerJson["number"];

            if (JSONHelper.isValidFieldForKey(offerJson, "title"))
                title = (String)offerJson["title"];

            if (JSONHelper.isValidFieldForKey(offerJson, "description"))
                description = (String)offerJson["description"];

            if (JSONHelper.isValidFieldForKey(offerJson, "startTime"))
                startTime = (String)offerJson["startTime"];

            if (JSONHelper.isValidFieldForKey(offerJson, "endTime"))
                endTime = (String)offerJson["endTime"];

            if (JSONHelper.isValidFieldForKey(offerJson, "imagePath"))
            {
                imagePath = (String)offerJson["imagePath"];
            }
            else if (imagePath == "")
            {
                imagePath = "/Resources/images/Offer_Image.png";
            }
            else
            {
                imagePath = "/Resources/images/Offer_Image.png";
            }
            if (JSONHelper.isValidFieldForKey(offerJson, "account"))
            {
                JObject accountJobject = (JObject)offerJson["account"];
                account = new Account(accountJobject);
            }

            countries = new List<Country>();
            categories = new List<Category>();
        }


        public Offer(SqlDataReader reader)
        {
            if (WSDatabaseHelper.isValidField(reader, "OFFER_ID"))
            {
                this.id = reader.GetInt64(reader.GetOrdinal("OFFER_ID"));
            }

            if (WSDatabaseHelper.isValidField(reader, "OFFER_NUMBER"))
            {
                number = reader.GetString(reader.GetOrdinal("OFFER_NUMBER"));
            }

            if (WSDatabaseHelper.isValidField(reader, "OFFER_TITLE"))
            {
                title = reader.GetString(reader.GetOrdinal("OFFER_TITLE"));
            }

            if (WSDatabaseHelper.isValidField(reader, "OFFER_DESCRIPTION"))
            {
                description = reader.GetString(reader.GetOrdinal("OFFER_DESCRIPTION"));
            }

            if (WSDatabaseHelper.isValidField(reader, "OFFER_START_TIME"))
            {
                startTime = reader.GetDateTime(reader.GetOrdinal("OFFER_START_TIME")).ToString();
            }

            if (WSDatabaseHelper.isValidField(reader, "OFFER_END_TIME"))
            {
                endTime = reader.GetDateTime(reader.GetOrdinal("OFFER_END_TIME")).ToString();
            }

            if (WSDatabaseHelper.isValidField(reader, "OFFER_CREATED_ON"))
            {
                this.createdOn = reader.GetDateTime(reader.GetOrdinal("OFFER_CREATED_ON")).ToString();
            }

            if (WSDatabaseHelper.isValidField(reader, "OFFER_UPDATED_ON"))
            {
                this.updatedOn = reader.GetDateTime(reader.GetOrdinal("OFFER_UPDATED_ON")).ToString();
            }

            if (WSDatabaseHelper.isValidField(reader, "CATEGORY_ID"))
            {
                categories = new List<Category>();
                categories.Add(new Category(reader));
            }

            if (WSDatabaseHelper.isValidField(reader, "COUNTRY_ID"))
            {
                countries = new List<Country>();
                countries.Add(new Country(reader));
            }


        }


        public Offer(HttpContext current)
        {
            //id = Convert.ToInt64(HttpContext.Current.Request.Form.GetValues("id").FirstOrDefault());
            number = Convert.ToString(HttpContext.Current.Request.Form.GetValues("number").FirstOrDefault());
            title = Convert.ToString(HttpContext.Current.Request.Form.GetValues("title").FirstOrDefault());
            description = Convert.ToString(HttpContext.Current.Request.Form.GetValues("description").FirstOrDefault());
            startTime = Convert.ToString(HttpContext.Current.Request.Form.GetValues("startTime").FirstOrDefault());
            endTime = Convert.ToString(HttpContext.Current.Request.Form.GetValues("endTime").FirstOrDefault());
            endTime = Convert.ToString(HttpContext.Current.Request.Form.GetValues("endTime").FirstOrDefault());
           
            HttpFileCollection files = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files : null;
            if(files !=  null)
            {
                if(files.Count == 0 )
                {
                    imagePath = "No Image";
                }
                else
                {
                    foreach (string fileName in files)
                    {
                        file = files[fileName];
                    }
                }
            }
            else
            {
                imagePath = "No Image";
            }
                          
            countries = new List<Country>();
            categories = new List<Category>();
        }

        public Offer()
        {
        }

        public override Dictionary<string, object> toDictionary()
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            if (this.id != 0)
                parameters["id"] = this.id;
            if (title != null)
                parameters["title"] = title;
            if (this.number != null)
                parameters["number"] = this.number;
            if (imagePath != null)
                parameters["imagePath"] = imagePath;
            if (description != null)
                parameters["description"] = description;
            if (startTime != null)
                parameters["startTime"] = startTime;
            if (endTime != null)
                parameters["endTime"] = endTime;

            parameters["account"] = account.id;
            parameters["createdBy"] = createdBy.id;
            parameters["updatedBy"] = updatedBy.id;
            parameters["updatedOn"] = DateTime.Now.ToString();
            parameters["createdOn"] = DateTime.Now.ToString();
            return parameters;
        }

        public override void save()
        {
            throw new NotImplementedException();
        }

        public override void update()
        {
            throw new NotImplementedException();
        }
    }
}