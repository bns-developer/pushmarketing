﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.BNSModels;
using BNS.Models.Accounts;
using BNS.Models.Offers;
using Newtonsoft.Json;
using System.Data.SqlClient;
using BNS.DL.WSDatabase;

namespace BNS.Models.LuckyItems
{
    public class LuckyItem : BNSModel
    {

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Account account { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String icon { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<Offer> offers { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Int64? kiosk { get; set; }

        public LuckyItem(SqlDataReader reader)
        {
            if (WSDatabaseHelper.isValidField(reader, "LUCKY_ITEM_ID"))
            {
                this.id = reader.GetInt64(reader.GetOrdinal("LUCKY_ITEM_ID"));
            }          

            if (WSDatabaseHelper.isValidField(reader, "LUCKY_ITEM_NAME"))
            {
                this.name = reader.GetString(reader.GetOrdinal("LUCKY_ITEM_NAME"));
            }

            if (WSDatabaseHelper.isValidField(reader, "LUCKY_ITEM_ICON"))
            {
                icon = reader.GetString(reader.GetOrdinal("LUCKY_ITEM_ICON"));
            }

            if (WSDatabaseHelper.isValidField(reader, "OFFER_ID"))
            {
                offers = new List<Offer>();
                offers.Add(new Offer(reader));
            }

        }

        public LuckyItem()
        {
        }

        public override void save()
        {
            throw new NotImplementedException();
        }

        public override Dictionary<string, object> toDictionary()
        {
            throw new NotImplementedException();
        }

        public override void update()
        {
            throw new NotImplementedException();
        }
    }
}