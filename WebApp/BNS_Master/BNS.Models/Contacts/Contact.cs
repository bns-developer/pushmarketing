﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.BNSModels;
using BNS.Models.Countries;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using BNS.Models.Helpers;

namespace BNS.Models.Contacts
{
    public class Contact : BNSModel
    {

        public enum Contact_Type
        {
            EMAIL = 1,
            PHONE = 2,
            WEBSITE = 3,
            FACEBOOK = 4
        };
        private Contact_Type _type;

        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Contact_Type ContactType
        {
            get { return _type; }
            set { _type = value; }
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Country country { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String value { get; set; }

        Dictionary<string, object> contacts = new Dictionary<string, object>();

        //public Contact(JObject contactJObject)
        //{
        //    if (JSONHelper.isValidFieldForKey(contactJObject, "email_id"))
        //    {

        //    }
        //    if (JSONHelper.isValidFieldForKey(contactJObject, "phone_number"))
        //    {

        //    }
        //    if (JSONHelper.isValidFieldForKey(contactJObject, "country"))
        //    {
        //        JObject countryJObject = (JObject)contactJObject["country"];
        //        this.country = new Country(countryJObject);
        //    }
        //}

        public Contact()
        {
        }

        private void setType(int code)
        {
            switch (code)
            {
                case 1: this.ContactType = Contact_Type.EMAIL; break;
                case 2: this.ContactType = Contact_Type.PHONE; break;
                case 3: this.ContactType = Contact_Type.WEBSITE; break;
                case 4: this.ContactType = Contact_Type.FACEBOOK; break;
            }
        }

        public override Dictionary<string, object> toDictionary()
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            if (this.id != 0)
                parameters["id"] = this.id;
            if (country != null)
                parameters["countryId"] = country.id;

            if (ContactType == Contact_Type.EMAIL)            
                parameters["type"] = 1;
            
            else if (ContactType == Contact_Type.PHONE)            
                parameters["type"] = 2;
            

            parameters["value"] = value;
            parameters["createdBy"] = createdBy.id;
            parameters["updatedBy"] = updatedBy.id;
            parameters["updatedOn"] = DateTime.Now.ToString();
            parameters["createdOn"] = DateTime.Now.ToString();
            return parameters;
        }

        public override void save()
        {
            throw new NotImplementedException();
        }



        public override void update()
        {
            throw new NotImplementedException();
        }
    }
}