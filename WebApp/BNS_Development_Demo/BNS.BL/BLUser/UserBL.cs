﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.DAL.DALUser;
using BNS.Models.Users;

namespace BNS.BL.BLUser
{
    public class UserBL
    {
        DALUserManager manager = new DALUserManager();
        public Int64 saveUser(User User)
        {
            return manager.saveUser(User);
        }
    }
}