﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.DAL.DALOffer;
using BNS.Models.Offers;
using Newtonsoft.Json.Linq;
using BNS.Models.Countries;
using BNS.Models.Categories;
using BNS.Models.Kiosks;

namespace BNS.BL.BLOffer
{
    public class OfferBL
    {
        DALOfferManager manager = new DALOfferManager();
        public bool save(Offer offer)
        {
           return manager.saveOffer(offer);                        
        }

        //public bool add(Offer offer)
        //{
        //    return manager.addOffer(offer);
        //}

        public bool saveOfferForLuckyItem(Int64 kioskId, Int64 luckyItemId, Offer offer)
        {
            return manager.saveKioskLuckyItemOffer(kioskId, luckyItemId, offer);
        }

        public bool saveOfferForBeacon(Int64 beaconId, Offer offer)
        {
            return manager.saveBeaconOffer(beaconId, offer);
        }

    }
}