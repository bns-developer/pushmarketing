angular.module('bns.constants', [])

//.constant("API_URL", "http://dev.webapi.bns.com/api/v1")
//.constant("MEDIA_URL", "http://dev.webapi.bns.com/")
.constant("API_URL", "http://localhost:5040/api/v1")
.constant("MEDIA_URL", "http://localhost:5040")
.constant("IMAGE_URL", "http://localhost:5040")
.constant("LOADING_MESSAGE", "Loading...")