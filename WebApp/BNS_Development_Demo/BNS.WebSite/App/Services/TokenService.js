﻿'use strict';
angular.module('bns.TOKEN_SERVICE', ['bns.constants'])
.factory('TOKEN_SERVICE', ['$rootScope', '$localStorage', function ($rootScope, $localStorage) {

    var service = {

        token: {
            access_token: '',
            token_type: '',
            expires_in: ''
        },

        SaveState: function (userToken) {
            userToken.access_token = 'Bearer ' + userToken.access_token;
            $localStorage.userToken = angular.toJson(userToken);
            localStorage.setItem("access_token", userToken.access_token);
        },

        RestoreState: function () {
            service.token = {};
            service.token = angular.fromJson($localStorage.userToken);
            if (service.token == null || service.token == {} || service.token == undefined)
                service.token = { 'access_token': '' };
            return service.token;
        },
        Delete: function () {
            $localStorage.$reset();
        }
    }

    //$rootScope.$on("savestate", service.SaveState);
    //$rootScope.$on("restorestate", service.RestoreState);

    return service;
}]);