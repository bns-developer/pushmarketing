angular.module('bns.mobile.viewOffer', [])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
    .state('mobile.viewOffer', {
        url: "/beacon/view-offer",
        views: {
            "viewOffer": {
                templateUrl: "App/Mobile/viewOffer/viewOffer.html",
                controller: 'viewOfferCtrl',
            },
        },
        params: {
            offer: { offerId: "", category: "", imagePath: "", endTime: "" }
        }
    });
}])
.controller('viewOfferCtrl', ['$state', '$scope', '$rootScope', '$stateParams', 'API_URL', 'IMAGE_URL', 'API_SERVICE', '$http','TimerService','$timeout', function ($state, $scope, $rootScope, $stateParams, API_URL, IMAGE_URL, API_SERVICE, $http,TimerService, $timeout) {

    if($stateParams.offer == undefined){
            $state.go("mobile.landing");
            return;
        }

    function imgInit() {

        if ($stateParams.offer.hasOwnProperty('imagePath') && ($stateParams.offer.imagePath != "No Image"))
           {
            $scope.imgsrc = IMAGE_URL + $stateParams.offer.imagePath;
          }
        else {
            if ($stateParams.offer.categories[0] == "Cash") {
                $scope.imgsrc = IMAGE_URL + "/Resources/offerImages/Cash.jpg";
            } else if ($stateParams.offer.categories[0] == "Points") {
                $scope.imgsrc = (IMAGE_URL + "/Resources/offerImages/Points.jpg");
            } else if ($stateParams.offer.categories[0] == "Promotion") {
                $scope.imgsrc = (IMAGE_URL +  "/Resources/offerImages/Promotions.jpg");
            }
        }
    }
    imgInit();


    function getOffer() {
        $scope.loading = true;
        function success(data, status, headers, config) {
            $scope.offerToDisplay = data;
            TimerService.initializeTimer(data.duration);
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
        }
        var url = "/Offers/details?offerId=" + $stateParams.offer.offerId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getOffer();

    $scope.gotoUserDetails = function () {
        $scope.userOffer = { categories: "", offerId: "", imagePath: "", endTime: "" };
        $scope.userOffer.categories = $scope.offerToDisplay.categories;
        $scope.userOffer.offerId = $scope.offerToDisplay.id;
        $scope.userOffer.imagePath = $scope.imgsrc;
        $scope.userOffer.endTime = $scope.offerToDisplay.endTime;
        var commonCurrencySymbol = "$";
        if($scope.offerToDisplay.countries.length > 0)
            commonCurrencySymbol = $scope.offerToDisplay.countries[0].currency;
        for(var i in $scope.userOffer.categories){
            if($scope.userOffer.categories[i].name == 'Cash')
                $scope.userOffer.categories[i]["currencySymbol"] = commonCurrencySymbol;
        }
        $state.go('mobile.getUserDetail', { 'userOffer': $scope.userOffer });
    }

    $scope.getTimeLeft = function (endTime) {
        
        return moment(endTime).fromNow();
    }

    $scope.currentDate = new Date();

    $(window).bind("beforeunload",function(event) {
        return "Offer will be discarded. Are you sure you want to continue?";
    });

    $scope.$on('$destroy', function() {
        delete window.onbeforeunload;
    });

    }])
