angular.module('bns.mobile.congoNewUser', [])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
    .state('mobile.congoNewUser', {
        url: "/beacon/congo-new-user",
        views: {
            "congoNewUser": {
                templateUrl: "App/Mobile/congoNewUser/congoNewUser.html",
                controller: 'congoNewUserCtrl'
            },
        },
        params: {
            userOffer: { categories: "", offerId: "", imagePath: "", endTime: "" }
        }
    });
}])
.controller('congoNewUserCtrl', ['$state', '$scope', '$rootScope', '$stateParams', 'API_URL','TimerService', function ($state, $scope, $rootScope, $stateParams, API_URL,TimerService) {    
    //var userOffer= $state.params.userOffer;
    function init() {
        TimerService.stopTimer();

        $scope.categories = $state.params.userOffer.categories;
        
        $scope.imgsrc = $state.params.userOffer.imagePath;
        
        //console.log("User offers ----- " + $stateParams.categories[0].value);
        $scope.currentDate = new Date();
    }
    init();

    }])
