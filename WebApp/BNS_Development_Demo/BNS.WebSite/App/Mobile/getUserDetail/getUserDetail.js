angular.module('bns.mobile.getUserDetail', [])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
    .state('mobile.getUserDetail', {
        url: "/beacon/get-user-details?userOffer",
        views: {
            "getUserDetail": {
                templateUrl: "App/Mobile/getUserDetail/getUserDetail.html",
                controller: 'getUserDetailCtrl'
            },
        }
    });
}])
.controller('getUserDetailCtrl', ['$state', '$scope', '$rootScope', '$stateParams', '$state', 'API_URL', 'IMAGE_URL', 'API_SERVICE', '$http','TimerService', function ($state, $scope, $rootScope, $stateParams, $state, API_URL, IMAGE_URL, API_SERVICE, $http, TimerService) {
    
    if($stateParams.userOffer.offerId == undefined){
        $state.go("mobile.landing");
        return;
    }

    $scope.userdetail = {
        email_id: "",
        phone: "",
        country:{
            id: 1
        },
        registered_from: 1,
        beacon:{
            id: $rootScope.beaconId
        },
        offer:{
            id: $stateParams.userOffer.offerId
        }
    }
    $scope.submitted = false;
    $scope.errorMessage = false;
    $scope.flagBasePath = IMAGE_URL;
    
    function init() {
        
        $scope.userOffer = { categories: "", offerId: "", imagePath: "", endTime: "" };
        $scope.userOffer.categories = $stateParams.userOffer.categories;
        $scope.userOffer.offerId = $stateParams.userOffer.offerId;
        $scope.userOffer.imagePath = $stateParams.userOffer.imagePath;
        $scope.userOffer.endTime = $stateParams.userOffer.endTime;
        $scope.imgsrc = $scope.userOffer.imagePath;
       
    }
    init();

    //$(window).on("scroll", function () {
    //     $('#background').addClass("imageHeight", $(this).scrollTop() > $(window).height());
    //});

    function getCounrtyCodes() {
        function success(data, status, headers, config) {
            $scope.countryCodes = data;
            console.log(data);
        }
        function failure(data, status, headers, config) {
            console.log(data);
        }
        var url = "/countries/getCountryCodes";
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getCounrtyCodes();
    $scope.getTimeLeft = function (endTime) {
        endTime = new Date(endTime);
        return moment(endTime).fromNow();
    }

    $scope.submitForm = function (isValid) {
        $scope.submitted = true;
        if (isValid)
        {
                function success(data, status, headers, config) {
                    $scope.submitted = false;
                    $scope.errorMessage = false;
                    TimerService.stopTimer();
                    $state.go('mobile.congoNewUser', { 'userOffer': $scope.userOffer });
                }
                function failure(data, status, headers, config) {
                    $scope.errorMessage = true;
                    if(status == 409)
                        $scope.errorResponse = data;
                    else
                        $scope.errorResponse = "Something went wrong. Please try again."
                    $scope.submitted = false;
                }
                var url = "/users/registration";
                API_SERVICE.postData($scope, $http, $scope.userdetail, url, success, failure);
        }
    }

    function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

    $scope.currentDate = new Date();

    $(window).bind("beforeunload",function(event) {
        return "Offer will be discarded. Are you sure you want to continue?";
    });

    $scope.$on('$destroy', function() {
        delete window.onbeforeunload;
    });

    }])
