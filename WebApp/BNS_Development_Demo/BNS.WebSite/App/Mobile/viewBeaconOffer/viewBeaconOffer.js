﻿angular.module('bns.mobile.viewBeaconOffer', [])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
    .state('mobile.viewBeaconOffer', {
        url: "/beaconCurrentOffer?bid",
        views: {
            "viewBeaconOffer": {
                templateUrl: "App/Mobile/viewBeaconOffer/viewBeaconOffer.html",
                controller: 'viewBeaconOfferCtrl',
            },
        }
    });
}])
.controller('viewBeaconOfferCtrl', ['$state', '$scope', '$rootScope', '$stateParams', 'API_URL', 'IMAGE_URL', 'API_SERVICE', '$http','TimerService', function ($state, $scope, $rootScope, $stateParams, API_URL, IMAGE_URL, API_SERVICE, $http,TimerService) {
    $rootScope.beaconId = $stateParams.bid;
    function getBeaconOffer() {
        function success(data, status, headers, config) {
            $scope.beaconOffer = data;
            console.log(data);
            TimerService.initializeTimer(data.duration);

            if (data.hasOwnProperty('imagePath') && (data.imagePath != "No Image"))
            {
                $scope.imgsrc = IMAGE_URL + data.imagePath;
            }
            else {
                if (data.categories[0].name == "Cash") {
                    $scope.imgsrc = IMAGE_URL + "/Resources/offerImages/Cash.jpg";
                } else if (data.categories[0].name == "Points") {
                    $scope.imgsrc = IMAGE_URL + "/Resources/offerImages/Points.jpg";
                } else if (data.categories[0].name == "Promotion") {
                    $scope.imgsrc = IMAGE_URL +  "/Resources/offerImages/Promotions.jpg";
                }
            }
          
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
        }
        var url = "/Offers/getCurrentActiveOfferForBeacon?bid=" + $rootScope.beaconId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getBeaconOffer();

    $scope.gotoUserDetails = function () {
        $scope.userOffer = { categories: "", offerId: "", imagePath: "", endTime: "" };
        $scope.userOffer.categories = $scope.beaconOffer.categories;
        $scope.userOffer.offerId = $scope.beaconOffer.id;
        $scope.userOffer.imagePath = $scope.imgsrc;
        $scope.userOffer.endTime = $scope.beaconOffer.endTime;

        var commonCurrencySymbol = "$";
        if ($scope.beaconOffer.countries.length > 0)
            commonCurrencySymbol = $scope.beaconOffer.countries[0].currency;
        for(var i in $scope.userOffer.categories){
            if($scope.userOffer.categories[i].name == 'Cash')
                $scope.userOffer.categories[i]["currencySymbol"] = commonCurrencySymbol;
        }

        $state.go('mobile.getUserDetail', { 'userOffer': $scope.userOffer });
    }
    
    $scope.gotoLanding = function()
    {
        $state.go('mobile.landing')
    }

    $scope.getTimeLeft = function (endTime) {
        endTime = new Date(endTime);
        return moment(endTime).fromNow();
    }

    $(window).bind("beforeunload",function(event) {
        return "Offer will be discarded. Are you sure you want to continue?";
    });

    $scope.$on('$destroy', function() {
        delete window.onbeforeunload;
    });

}])
