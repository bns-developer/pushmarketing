angular.module('bns.master.stores', [])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.stores', {
        url: '/stores',
        views: {
            "stores": {
                templateUrl: 'App/Master/Stores/stores.html',
                controller: 'storesController'
            }
        }

    });
}])
.controller("storesController", ['$scope', '$http', 'API_URL', 'LOADING_MESSAGE', 'ngDialog', 'API_SERVICE', function ($scope, $http, API_URL, LOADING_MESSAGE, ngDialog, API_SERVICE) {
     $scope.loadingMessage = LOADING_MESSAGE;
    $scope.saveButtonName = "Save";
    $scope.submitted = false;
    $scope.successAlert = false;
    $scope.failureAlert = false;
    $scope.newStore = {
        "name": "",
        "number": "",
        "country": {"id":""}
    };
        
        var table_advanced = function() {

            var handleDatatable = function(){
                var spinner = $( ".spinner" ).spinner();
                var table = $('#table_id').dataTable( {
                    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                    "order": [[ 1, "asc" ]],
                    "aoColumnDefs" : [ {
                        'bSortable' : false,
                        'aTargets' : [ 0 ]
                    },,
                    {
                        'bSortable': false,
                        'aTargets': [1]
                    }],
                    "autoWidth": true,
                } );

                var tableTools = new $.fn.dataTable.TableTools( table, {
                    "sSwfPath": "libs/assets/vendors/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                    "buttons": [
                    "copy",
                    "csv",
                    "xls",
                    "pdf",
                    { "type": "print", "buttonText": "Print me!" }
                    ]
                } );
                $(".DTTT_container").css("float","right");
            };
            return{
                init: function () {
                    handleDatatable();
                }
            };
        }(jQuery);
        
        setTimeout(function(){
            table_advanced.init();
        },500);

        $scope.addStore = function () {
            ngDialog.open({
                template: 'addStoreDialog',
                scope: $scope,
                preCloseCallback: function () { $scope.clearModal() }

            });
        }

        $scope.closeModal = function () {
            ngDialog.close({
                template: 'addStoreDialog',
                scope: $scope
            });
            $scope.clearModal();
        }
        
        $scope.clearModal = function () {
            $scope.submitted = false;
            $scope.successAlert = false;
            $scope.faliureAlert = false;
            $scope.newStore = {
                        "name": "",
                        "number": "",
                        "country": { "id": "" }
                    };
        }


        function getAllStoresList() {
            $scope.loading = true;
            $scope.dataLoader = 0;
            function success(data, status, headers, config) {
                $scope.stores = data;
                console.log($scope.stores);
                $scope.loading = false;
                $scope.dataLoader = 1;
            }
            function failure(data, status, headers, config) {
                $scope.loading = false;
                $scope.dataLoader = 2;
            }
            var url = "/Stores/all";
            API_SERVICE.getData($scope, $http, url, success, failure);
        }
        getAllStoresList();

        function getAllCountries() {
            $scope.loading = true;
            function success(data, status, headers, config) {
                $scope.countries = data;
                console.log($scope.stores);
                $scope.loading = false;
            }
            function failure(data, status, headers, config) {
                $scope.loading = false;
            }
            var url = "/countries/all";
            API_SERVICE.getData($scope, $http, url, success, failure);
        }
        getAllCountries();

        $scope.reloadStoreList = function () {
            getAllStoresList();
        }

        $scope.saveNewStore = function () {
            $scope.saveButtonName = "Saving";
            $scope.submitted = true;
            if ($scope.newStore.name == "" || $scope.newStore == undefined || $scope.newStore.number == "" || $scope.newStore.number == undefined || $scope.newStore.country.id == "" || $scope.newStore.country.id == undefined)
            {
                $scope.saveButtonName = "Save";
            }
            else
            {
                function success(data, status, headers, config) {
                    getAllStoresList();
                    $scope.submitted = false;
                    $scope.successAlert = true;
                    $scope.faliureAlert = false;
                    //$scope.closeModal();
                    $scope.newStore = {
                        "name": "",
                        "number": "",
                        "country": { "id": "" }
                    };
                    $scope.loading = false;
                    $scope.saveButtonName = "Save";
                }
                function failure(data, status, headers, config) {
                    $scope.loading = false;
                    $scope.successAlert = false;
                    $scope.faliureAlert = true;
                    $scope.saveButtonName = "Save";
                    if(status == 409)
                    {
                        $scope.errorMessage = data;
                    }
                    else
                    {
                        $scope.errorMessage = "Could not add stor to the list. Please check all the fields."
                    }
                }
                var url = "/Stores/save";
                API_SERVICE.postData($scope, $http, $scope.newStore, url, success, failure);
            }
            console.log($scope.newStore);
        }

    }])