angular.module('bns.master.users', [])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.users', {
        url: '/users',
        views: {
            "users": {
                templateUrl: 'App/Master/Users/users.html',
                controller: 'usersController'
            }
        }
    });
}])
.controller("usersController", ['$scope', '$http', 'API_URL', 'LOADING_MESSAGE', 'API_SERVICE', '$state',
    function ($scope, $http, API_URL, LOADING_MESSAGE, API_SERVICE, $state) {

        $scope.loadingMessage = LOADING_MESSAGE;

        var table_advanced = function () {

            var handleDatatable = function () {
                var spinner = $(".spinner").spinner();
                var table = $('#table_id').dataTable({
                    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                    "order": [[1, "asc"]],
                    "aoColumnDefs": [
                    {
                        'bSortable': false,
                        'aTargets': [5]
                    },
                    {
                        'bSortable': false,
                        'aTargets': [6]
                    },
                    {
                        'bSortable': false,
                        'aTargets': [8]
                    },
                    {
                        'bSortable': false,
                        'aTargets': [9]
                    }],
                    "autoWidth": false,
                    "columns": [
                        { "width": "4%", "className": "alignRight" },
                        null,
                        { "width": "15%" },
                        { "width": "10%" },
                        { "className": "alignRight" },
                        null,
                        null,
                        null,
                        null,
                        null
                        ]
                });

                var tableTools = new $.fn.dataTable.TableTools(table, {
                    "sSwfPath": "../assets/vendors/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                    "buttons": [
                        "copy",
                        "csv",
                        "xls",
                        "pdf",
                        { "type": "print", "buttonText": "Print me!" }
                    ]
                });
                $(".DTTT_container").css("float", "right");
            };
            return {
                init: function () {
                    handleDatatable();
                }
            };
        }(jQuery);

        setTimeout(function () {
            table_advanced.init();
        }, 500);

        function processData(data) {
            var prevId = -1;
            var prevUserData = {};
            var users = [];
            var userData = {};
            for (var i = 0; i < data.length; i++)
            {
               
                if (prevId == userData.id)
                {
                    if (data[i].contact.ContactType === "PHONE")
                    {
                        userData.phone=(data[i].contact.value);
                    }
                    else if (data[i].contact.ContactType === "EMAIL")
                    {
                        userData.email = (data[i].contact.value);
                    }
                    
                }
                else
                {
                    userData = data[i];

                    //if (data[i].contact.ContactType === "PHONE") {
                    //    userData.phone = (data[i].contact.value);
                    //}
                    //else if (data[i].contact.ContactType === "EMAIL") {
                    //    userData.email = (data[i].contact.value);
                    //}

                    prevId = data[i].id;
                }
                if((i+1)<data.length)
                {
                    if(data[i].id !== data[i+1].id)
                    {
                        users.push(userData);
                        userData = {};
                    }
                }
                else if (i == data.length - 1) {
                    users.push(userData);
                }

               
            }

            return users;
        }

        function getAllUsersList() {
            $scope.loading = true;
            $scope.dataLoader = 0;
            function success(data, status, headers, config) {
                $scope.users = data;
                //$scope.users = processData(data);
                console.log($scope.stores);
                $scope.loading = false;
                $scope.dataLoader = 1;
            }
            function failure(data, status, headers, config) {
                $scope.loading = false;
                $scope.dataLoader = 2;
            }
            var url = "/users/all";

            var header = {
                "access-token": "264982hwqudy298344982yrsagad773283e"
            };


            API_SERVICE.getData($scope, $http, url, success, failure, header);
        }
        getAllUsersList();

        $scope.reloadUsersList = function () {
            getAllUsersList();
        }

    }])