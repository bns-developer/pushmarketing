﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BNS.WebAPI.Controllers.BNSControllers;
using Newtonsoft.Json.Linq;
using BNS.Wrapper;
using BNS.Models.Users;
using BNS.Models.Contacts;
using BNS.Models.Accounts;
using BNS.DAL.DALUser;
using BNS.BL.BLContact;
using BNS.DAL.DALContact;
using BNS.BL.BLUser;
using BNS.Models.Countries;
using BNS.Models.Beacons;
using BNS.Models.Kiosks;
using BNS.Models.Offers;
using System.Web;

namespace BNS.WebAPI.Controllers.UsersControllers
{
    [RoutePrefix("api/v1/users")]
    public class UserController : BNSController
    {
        UserBL userBLmanager = new UserBL();
        DALUserManager userDALmanager = new DALUserManager();

        ContactBL contactBLmanager = new ContactBL();
        DALContactManager contactDALmanager = new DALContactManager();

        ResponseWrapper responseWrapper = new ResponseWrapper();

        
        [Route("registration")]
        [HttpPost]
        
        //User Registration
        public override HttpResponseMessage save([FromBody] JObject requestJSON)
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());

            String emailId = (String)requestJSON["email_id"];
            String Phone = (String)requestJSON["phone"];
            int registred_from = (int)requestJSON["registered_from"];

            if (contactDALmanager.checkIfEmailIdExists(emailId, requestUser.account.id))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Your Email Id is already registered with us. Please try with some different Email Id.");
            }
            else
            {

                JObject countryJObject = (JObject)requestJSON["country"];
                Country newUserCountry = new Country(countryJObject);

                JObject offerJObject = (JObject)requestJSON["offer"];
                Offer offer = new Offer(offerJObject);

                Contact newUserContact1 = new Contact(emailId, newUserCountry);
                Contact newUserContact2 = new Contact(Phone, newUserCountry);

                User newUser = new User();
                newUser.is_registered = 1;
                newUser.account = requestUser.account;
                newUser.offer = offer;
                newUser.contacts.Add(newUserContact1);
                newUser.contacts.Add(newUserContact2);

                switch (registred_from)
                {
                    case 1:
                        JObject beaconJObject = (JObject)requestJSON["beacon"];
                        Beacon beacon = new Beacon(beaconJObject);
                        newUser.beacon = beacon;
                        break;

                    case 2:
                        JObject kioskJObject = (JObject)requestJSON["kiosk"];
                        Kiosk kiosk = new Kiosk(kioskJObject);
                        newUser.kiosk = kiosk;
                        break;
                }

                Int64 id = userBLmanager.saveUser(newUser);
                if (id != 0)
                {
                    JObject userId = new JObject();
                    userId["id"] = id;

                    return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, userId);
                }
                else
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "User Not Saved");
                }
            }

        }

        [Route("saveMemberOffer")]
        [HttpPost]
        [Authorize]
        //User Registration
        public  HttpResponseMessage saveMemberOffer([FromBody] JObject requestJSON)
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());

            String memberId = (String)requestJSON["member_id"];
            int registred_from = (int)requestJSON["registered_from"];

            if(!userDALmanager.verifyMemberId(memberId, requestUser.account.id))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Invalid Member Id! Please try again.");
            }
            else
            {
                JObject offerJObject = (JObject)requestJSON["offer"];
                Offer offer = new Offer(offerJObject);

                User member = new User();
                member.memberId = memberId;
                member.account = requestUser.account;
                member.offer = offer;

                switch (registred_from)
                {
                    case 1:
                        JObject beaconJObject = (JObject)requestJSON["beacon"];
                        Beacon beacon = new Beacon(beaconJObject);
                        member.beacon = beacon;
                        break;

                    case 2:
                        JObject kioskJObject = (JObject)requestJSON["kiosk"];
                        Kiosk kiosk = new Kiosk(kioskJObject);
                        member.kiosk = kiosk;
                        break;
                }
                if(userDALmanager.saveMemberOffer(member))
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Offer given to member");
                }
                else
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Error in assigning the offer to member");
                }
                
            }
        }

        [Route("all")]
        [HttpGet]
        [Authorize]
        //All registered users 
        public override HttpResponseMessage GetAll()
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            List<User> users = userDALmanager.getAll(requestUser.account);
            var response = Request.CreateResponse(HttpStatusCode.OK, users);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, users);
        }

        public override HttpResponseMessage delete([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }

        public override HttpResponseMessage deleteMultiple([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }

        
        public override HttpResponseMessage GetInfo([FromUri] long id)
        {
            throw new NotImplementedException();
        }

        public override HttpResponseMessage update([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }
    }
}
