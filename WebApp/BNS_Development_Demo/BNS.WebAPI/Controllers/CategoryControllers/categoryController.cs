﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BNS.DAL.DALCategory;
using BNS.Wrapper;
using BNS.Models.Accounts;
using BNS.Models.Categories;
using System.Web;
using BNS.Models.Users;

namespace BNS.WebAPI.Controllers.CategoryControllers
{

    [RoutePrefix("api/v1/Categories")]
    public class CategoryController : ApiController
    {
        DALCategoryManager manager = new DALCategoryManager();
        ResponseWrapper responseWrapper = new ResponseWrapper();
   
        [Route("all")]
        [HttpGet]
        [Authorize]
        //All categories by account
        public HttpResponseMessage GetAll()
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            List<Category> CategoriesList = manager.getAll(requestUser.account);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, CategoriesList);
        }
    }
}
