﻿using System;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Script.Serialization;
using BNS.Wrapper;
using BNS.WebAPI.Controllers.BNSControllers;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Web;
using System.Linq;
using BNS.DAL.DALAccount;
using System.Net;
using BNS.Models.Accounts;
using BNS.Models.Users;
using BNS.DAL.DALUser;


namespace BNS.API.Controllers.AccountControllers
{ 
    [RoutePrefix("api/v1/accounts")]
    public class AccountController : BNSController
    {
        DALAccountManager accountManager = new DALAccountManager();
        DALUserManager userManager = new DALUserManager();
        ResponseWrapper responseWrapper = new ResponseWrapper();
        
             
        [Route("add")]
        [HttpPost]
       
        //Add new account
        public HttpResponseMessage addAaccount()
        {
            String userName = Convert.ToString(HttpContext.Current.Request.Form.GetValues("userName").FirstOrDefault());

            if (userManager.checkIfUserNameExists(userName))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Username Already Exist");
            }
            else
            {
                Account newAccount = new Account(HttpContext.Current);
                newAccount.id = accountManager.addAccount(newAccount);
                if (newAccount.id != 0)
                {
                    JObject accountId = new JObject();
                    accountId["id"] = newAccount.id;
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, accountId);
                }
                else
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Sign Up Error");
                }
            }
        }


        [Route("details")]
        [HttpGet]
        [Authorize]
        //Account details
        public HttpResponseMessage GetDetails()
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            Account account = new Account();
            account.id = requestUser.account.id;
            account = accountManager.getDetails(account);
            var response = Request.CreateResponse(HttpStatusCode.OK, account);
            return response;
        }



        [Route("info")]
        [HttpGet]
        public override HttpResponseMessage GetInfo([FromUri] Int64 account_id)
        {
            throw new NotImplementedException();
        }

        [HttpGet]
        [Route("GetAccount")]
        public JsonResult<ResponseWrapper> GetAccount()
        {

            String json = "{\"id\":101,\"name\":\"Novomatic\",\"email\":\"novomatic@gmail.com\",\"password\":\"pass@novomatic\",\"pic_url\":\"~/Resources/Images/Novo2.png\"}";
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var jsonObject = serializer.Deserialize<dynamic>(json);
            return Json(new ResponseWrapper(status: 200, desc: "Ok", obj: jsonObject));
        }

        [Route("update")]
        [HttpPost]
        [Authorize]
        //Update an account
        public HttpResponseMessage update()
        {                     
            Account updateAccount = new Account(HttpContext.Current);           
            if (accountManager.update(updateAccount))
                return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Account Updated");
            else
                return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Aaccount Not Updated");
        }

        public override HttpResponseMessage save([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }

        public override HttpResponseMessage GetAll()
        {
            throw new NotImplementedException();
        }    

        public override HttpResponseMessage update([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }

        public override HttpResponseMessage delete([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }

        public override HttpResponseMessage deleteMultiple([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }
    }
}
