﻿using BNS.Wrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Script.Serialization;
using BNS.WebAPI.Controllers.BNSControllers;
using Newtonsoft.Json.Linq;
using BNS.Models.Accounts;
using BNS.Models.Users;
using BNS.Models.Offers;
using BNS.BL.BLOffer;
using BNS.DAL.DALOffer;
using BNS.Models.Countries;
using BNS.Models.Categories;
using System.Web;

using System.Diagnostics;

namespace BNS.API.Controllers.OfferControllers
{
    [RoutePrefix("api/v1/Offers")]
    public class OfferController : BNSController
    {
        OfferBL BLmanager = new OfferBL();
        DALOfferManager DALmanager = new DALOfferManager();
        ResponseWrapper responseWrapper = new ResponseWrapper();       

        [Route("save")]
        [HttpPost]
        [Authorize]
        //Save an offer multipart data
        public HttpResponseMessage save()
        {
            HttpFileCollection files = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files : null;
            String offerNumber = Convert.ToString(HttpContext.Current.Request.Form.GetValues("number").FirstOrDefault());

            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());

            List<string[]> countries = new List<string[]>();
            List<string[]> categories = new List<string[]>();
            

            if (DALmanager.checkIfNumberExists(offerNumber, requestUser.account.id))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Offer Number Already Exist");
            }
            else
            {
                Offer newOffer = new Offer(HttpContext.Current);
                newOffer.createdBy = requestUser;
                newOffer.updatedBy = requestUser;
                newOffer.account = requestUser.account;


                int length = HttpContext.Current.Request.Form.Count;
                while (length >= 0)
                {
                    if (HttpContext.Current.Request.Form.GetValues("countries[" + length + "][id]") != null)
                    {
                        countries.Add(HttpContext.Current.Request.Form.GetValues("countries[" + length + "][id]"));
                    }
                     
                    if (HttpContext.Current.Request.Form.GetValues("categories[" + length + "][is_selected]") != null)
                    {
                        string[] selected = HttpContext.Current.Request.Form.GetValues("categories[" + length + "][is_selected]");
                        if(selected[0].Equals("true"))
                        {
                            categories.Add(HttpContext.Current.Request.Form.GetValues("categories[" + length + "][id]"));
                            categories.Add(HttpContext.Current.Request.Form.GetValues("categories[" + length + "][value]"));
                        }
                        selected = null;                   
                    }
                    length = length - 1;
                }

               foreach(string[] item in countries)
                {
                    Country country = new Country();
                    country.id = Convert.ToInt64(item[0]);
                    newOffer.countries.Add(country);
                }

               for(int i = 0; i <= categories.Count - 1; i++)
                {
                    Category category = new Category();
                    category.id = Convert.ToInt64(categories[i][0]);
                    i++;
                    category.value = categories[i][0].ToString();
                    newOffer.categories.Add(category);
                }

                if (BLmanager.save(newOffer))
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Offer  Saved");
                }
                else
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Offer Not Saved");
                }
            }

        }
     
        [Route("addOffer")]
        [HttpPost]
        [Authorize]
        //Add as offer with JSON
        public  HttpResponseMessage addOffer([FromBody] JObject requestJSON)
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            String offerNumber = (String)requestJSON["number"];
            
            if (DALmanager.checkIfNumberExists(offerNumber, requestUser.account.id))
            {               
              return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Offer Number Already Exist");
            }
            else
            {
                JArray countryJsonArray = (JArray)requestJSON.SelectToken("countries");
                JArray categoryJsonArray = (JArray)requestJSON.SelectToken("categories");

                Offer newOffer = new Offer(requestJSON);
                newOffer.createdBy = requestUser;
                newOffer.updatedBy = requestUser;
                newOffer.account = requestUser.account;

                foreach (JObject item in countryJsonArray)
                {                   
                    Country country = new Country(item);                  
                    newOffer.countries.Add(country);
                }
                foreach (JObject item in categoryJsonArray)
                {
                    bool is_selected = (bool) item["is_selected"];
                    if(is_selected)
                    {
                        Category category = new Category(item);
                        newOffer.categories.Add(category);
                    }                 
                }
                if (BLmanager.save(newOffer))
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Offer  Saved");
                }
                else
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Offer Not Saved");
                }
            }          
        }     
       
        [Route("details")]
        [HttpGet]
        
        //Offer details
        public override HttpResponseMessage GetInfo([FromUri]Int64 offerId)
        {

            Offer offer = new Offer();
            offer.id = offerId;
            offer = DALmanager.getDetails(offer);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return response;
        }

        [Route("all")]
        [HttpGet]
        [Authorize]
        //All offers by accountId
        public override HttpResponseMessage GetAll()
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            List<Offer> offer = DALmanager.getAll(requestUser.account);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
        }

        [Route("setOfferForLuckyItem")]
        [HttpPost]
        [Authorize]
        // Set an offer for kiosk's lucky item
        public HttpResponseMessage saveKioskLuckyItemOffer([FromUri] Int64 kiosk_id, [FromUri] Int64 lucky_item_id, [FromUri] Int64 offer_id)
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            Offer offer = new Offer();
            offer.id = offer_id;
            offer.createdBy = requestUser;
            offer.updatedBy = requestUser;

            if (BLmanager.saveOfferForLuckyItem(kiosk_id, lucky_item_id, offer))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Offer Activated Successfully");
            }
            else
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Offer Not Activated");
            }

        }

        [Route("changeLuckyItemDefaultOffer")]
        [HttpPost]
        [Authorize]
        // Change an default offer of kiosk's lucky item
        public HttpResponseMessage changeLuckyItemDefaultOffer([FromUri] Int64 kiosk_id, [FromUri] Int64 lucky_item_id, [FromUri] Int64 offer_id)
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());

            Offer offer = new Offer();
            offer.id = offer_id;
            offer.createdBy = requestUser;
            offer.updatedBy = requestUser;

            if (DALmanager.changeLuckyItemDefaultOffer(kiosk_id, lucky_item_id, offer))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Offer Changed Successfully");
            }
            else
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Offer Not Changed");
            }

        }

        [Route("getLuckyItemDefaultOffer")]
        [HttpGet]
        [Authorize]
        //Default offer for kiosk's lucky item
        public  HttpResponseMessage getDefaultOffer([FromUri] Int64 kiosk_id, [FromUri] Int64 lucky_item_id)
        {           
            Offer offer = DALmanager.getLuckyItemDefaultOffer(kiosk_id, lucky_item_id);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
        }

        [Route("getLuckyItemNonDefaultOffers")]
        [HttpGet]
        [Authorize]
        //Default offer for kiosk's lucky item
        public HttpResponseMessage getLuckyItemNonDefaultOffers([FromUri] Int64 kiosk_id, [FromUri] Int64 lucky_item_id)
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            List<Offer> offer = DALmanager.getLuckyItemNonDefaultOffers(kiosk_id, lucky_item_id, requestUser.account.id);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
        }

        [Route("getLuckyItemActiveOffers")]
        [HttpGet]
        [Authorize]
        // Active offers for kiosk's lucky item
        public HttpResponseMessage getActiveOffers([FromUri] Int64 kiosk_id, [FromUri] Int64 lucky_item_id)
        {
            List<Offer> offer = DALmanager.getLuckyItemActiveOffers(kiosk_id, lucky_item_id);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
        }

        [Route("getLuckyItemOffersHistory")]
        [HttpGet]
        [Authorize]
        // Offers history of kiosk's lucky item
        public HttpResponseMessage getOffersHistory([FromUri] Int64 kiosk_id, [FromUri] Int64 lucky_item_id)
        {
            List<Offer> offer = DALmanager.getLuckyItemOffersHistory(kiosk_id, lucky_item_id);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
        }

        [Route("getLuckyItemInactiveOffers")]
        [HttpGet]
        [Authorize]
        // Inactive offers kiosk's lucky item
        public HttpResponseMessage getInactiveOffers([FromUri] Int64 kiosk_id, [FromUri] Int64 lucky_item_id)
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            List<Offer> offer = DALmanager.getLuckyItemInactiveOffers(kiosk_id, lucky_item_id, requestUser.account.id);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
        }
               

        [Route("setOfferForBeacon")]
        [HttpPost]
        [Authorize]
        // Set an offer for beacon
        public HttpResponseMessage saveBeaconOffer([FromUri] Int64 beacon_id, [FromUri] Int64 offer_id)
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());

            Offer offer = new Offer();
            offer.id = offer_id;
            offer.createdBy = requestUser;
            offer.updatedBy = requestUser;

            if (BLmanager.saveOfferForBeacon(beacon_id, offer))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Offer Activated Successfully");
            }
            else
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Offer Not Activated");
            }

        }

        [Route("changeBeaconDefaultOffer")]
        [HttpPost]
        [Authorize]
        // Change an default offer of beacon
        public HttpResponseMessage changeBeaconDefaultOffer([FromUri] Int64 beacon_id, [FromUri] Int64 offer_id)
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());

            Offer offer = new Offer();
            offer.id = offer_id;
            offer.createdBy = requestUser;
            offer.updatedBy = requestUser;

            if (DALmanager.changeBeaconDefaultOffer(beacon_id, offer))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Offer Changed Successfully");
            }
            else
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Offer Not Changed");
            }

        }


        [Route("getBeaconDfaultOffer")]
        [HttpGet]
        [Authorize]
        //Default offer for beacon
        public HttpResponseMessage getDefaultOffer([FromUri] Int64 beacon_id)
        {
            Offer defaultOffer = DALmanager.getBeaconDefaultOffer(beacon_id);
            var response = Request.CreateResponse(HttpStatusCode.OK, defaultOffer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, defaultOffer);
        }

        [Route("getBeaconNonDefaultOffers")]
        [HttpGet]
        [Authorize]
        //All offer for beacon excluding the default one
        public HttpResponseMessage getBeaconNonDefaultOffers([FromUri] Int64 beacon_id)
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            List<Offer> nonDefaultOffers = DALmanager.getBeaconNonDefaultOffers(beacon_id, requestUser.account.id);
            var response = Request.CreateResponse(HttpStatusCode.OK, nonDefaultOffers);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, nonDefaultOffers);
        }


        [Route("getBeaconActiveOffers")]
        [HttpGet]
        [Authorize]
        // Active offers for beacon
        public HttpResponseMessage getActiveOffers([FromUri] Int64 beacon_id)
        {
            List<Offer> offer = DALmanager.getBeaconActiveOffers(beacon_id);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
        }

        [Route("getBeaconOffersHistory")]
        [HttpGet]
        [Authorize]
        // Offers history of kiosk's lucky item
        public HttpResponseMessage getOffersHistory([FromUri] Int64 beacon_id)
        {
            List<Offer> offer = DALmanager.getBeaconOffersHistory(beacon_id);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
        }

        [Route("getBeaconInactiveOffers")]
        [HttpGet]
        [Authorize]
        // Inactive offers kiosk's lucky item
        public HttpResponseMessage getInactiveOffers([FromUri] Int64 beacon_id)
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            List<Offer> offer = DALmanager.getBeaconInactiveOffers(beacon_id, requestUser.account.id);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
        }

        [Route("getCurrentActiveOfferForBeacon")]
        [HttpGet]
        
        //Current active offer for beacon(Mobile App)
        public HttpResponseMessage getCurrentActiveOfferForBeacon([FromUri] String bid)
        {
            Offer offer = DALmanager.getCurrentActiveOfferForBeacon(bid);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
        }

        [Route("getOffersForLandingPage")]
        [HttpGet]
        
        //All unexpired offers for landing page(Mobile App)
        public HttpResponseMessage getOffersForLandingPage()
        {
            Account account = new Account();
            account.id = 1;
            List<Offer> offer = DALmanager.getOffersForLandingPage(account);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);            
        }


        public override HttpResponseMessage delete([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }

        public override HttpResponseMessage deleteMultiple([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }              
       
        public override HttpResponseMessage update([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }

        //[Route("getOffers")]
        //[HttpGet]
        //public JsonResult<ResponseWrapper> GetItems()
        //{
        //    String json = "[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"500\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $500 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\"}]";
        //    JavaScriptSerializer serializer = new JavaScriptSerializer();
        //    var jsonObject = serializer.Deserialize<dynamic>(json);
        //    return Json(new ResponseWrapper(status: 200, desc: "Ok", obj: jsonObject));
        //}

        public override HttpResponseMessage save([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }

        [Route("getLuckyItemOfferForKiosk")]
        [HttpGet]
        [Authorize]
        //Default offer for kiosk's lucky item
        public HttpResponseMessage getLuckyItemOfferForKiosk([FromUri] Int64 kiosk_id, [FromUri] Int64 lucky_item_id)
        {
            Offer offer = DALmanager.getLuckyItemOfferForKiosk(kiosk_id, lucky_item_id);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
        }

        [Route("getLuckyItemCurrentActiveOfferForKiosk")]
        [HttpGet]
        
        //Current active offer for kiosk's lucky item
        public HttpResponseMessage getLuckyItemCurrentActiveOfferForKiosk([FromUri] Int64 kiosk_id, [FromUri] Int64 lucky_item_id)
        {
            Offer offer = DALmanager.getLuckyItemCurrentActiveOfferForKiosk(kiosk_id, lucky_item_id);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
        }
    }
}
