﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BNS.WebAPI.Controllers.BNSControllers;
using Newtonsoft.Json.Linq;
using BNS.Models.Accounts;
using BNS.Models.Beacons;
using BNS.Wrapper;
using BNS.BL.BLBeacon;
using BNS.DAL.DALBeacon;
using BNS.Models.Users;
using BNS.Models.Stores;
using System.Web;

namespace BNS.WebAPI.Controllers.BeaconControllers
{
    [RoutePrefix("api/v1/beacons")]
    public class BeaconController : BNSController
    {
        BeaconBL BLmanager = new BeaconBL();
        DALBeaconManager DALmanager = new DALBeaconManager();
        ResponseWrapper responseWrapper = new ResponseWrapper();
       
        [Route("add")]
        [Authorize]
        [HttpPost]
        //Add new beacon
        public override HttpResponseMessage save([FromBody] JObject requestJSON)
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());

            JObject storeJObject = (JObject)requestJSON["store"];
            Store store = new Store(storeJObject);
            Int64 storeId = store.id;

            String beaconName = (String)requestJSON["name"];
            String beaconNumber = (String)requestJSON["number"];
            Beacon newBeacon = new Beacon(requestJSON);
            

            if (DALmanager.checkIfNameExists(beaconName, storeId))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Beacon name already exist for this store.");

            }

            else if (DALmanager.checkIfNumberExists(beaconNumber))
            {

                return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Beacon number already exist.");
            }
            else
            {
                newBeacon.createdBy = requestUser;
                newBeacon.updatedBy = requestUser;

                if (BLmanager.save(newBeacon))
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Beacon  Saved");
                }
                else
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Beacon Not Saved");
                }
            }
        }
     
        [Route("all")]
        [HttpGet]
        [Authorize]
        //All beacons by accountId
        public override HttpResponseMessage GetAll()
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            List<Beacon> beacons = DALmanager.getAll(requestUser.account);
            var response = Request.CreateResponse(HttpStatusCode.OK, beacons);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, beacons);
        }
  
        [Route("details")]
        [HttpGet]
        [Authorize]
        //Beacon details
        public override HttpResponseMessage GetInfo([FromUri]Int64 beaconId)
        {
            Beacon beacon = new Beacon();
            beacon.id = beaconId;
            beacon = DALmanager.getDetails(beacon);
            var response = Request.CreateResponse(HttpStatusCode.OK, beacon);
            return response;
        }

        public override HttpResponseMessage delete([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }

        public override HttpResponseMessage deleteMultiple([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }     

        public override HttpResponseMessage update([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }
    }
}
