﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BNS.Models.Countries;
using BNS.Wrapper;
using BNS.DAL.DALCountry;


namespace BNS.WebAPI.Controllers.CountryControllers
{
    [RoutePrefix("api/v1/countries")]
    public class CountryController : ApiController
    {
        DALCountryManager manager = new DALCountryManager();          
        ResponseWrapper responseWrapper = new ResponseWrapper();

        [Route("all")]
        [HttpGet]
        [Authorize]
        //All countries 
        public HttpResponseMessage GetAll()
        {
            List<Country> Countries = manager.getAll();           
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, Countries);
        }

        
        [Route("getCountryCodes")]
        [HttpGet]
        
        //Country call codes 
        public HttpResponseMessage getCountryCodes()
        {
            List<Country> Countries = manager.getCountryCodes();
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, Countries);
        }


    }
}
