﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Linq;
using System.Net.Http.Headers;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using BNS.DL.WSDatabase;
using WSDatabase;

namespace BNS.WebAPI.SessionManagement
{
    public class SessionManager
    {

        //Shared Instance
        private static SessionManager instance;
        public static SessionManager sharedInstance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SessionManager();
                }
                return instance;
            }
        }

        //Save Session
        internal bool save(Session session)
        {
            String query = "IF EXISTS (SELECT * FROM Sessions WHERE sessionUser = @sessionUser) UPDATE Sessions SET expiryTime = @expiryTime WHERE sessionUser = @sessionUser  ELSE INSERT INTO Sessions (sessionUser,sessionKey,expiryTime) VALUES (@sessionUser,@sessionKey,@expiryTime)  ";
            query = "INSERT INTO Sessions (sessionUser,sessionKey,expiryTime) VALUES (@sessionUser,@sessionKey,@expiryTime)";
            return WSDatabaseManager.sharedDatabaseManagerinstance.insertRecordIntoDatabase(query, session.toDictionary());
        }

        //Clear Session
        internal bool clearSession(HttpRequestMessage request)
        {
            String token = tokenFromRequest(request);            
            WSQuery query = new WSQuery("DELETE FROM Sessions where sessionKey = @key");
            query.parameters["key"] = token;            
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeUpdateQuery(query);
        }

        //Get Session For User
        internal Session getSessionForUser(long user)
        {
            Session session = null;
            WSQuery query = new WSQuery("Select QUSER.id as SESSIONUSER, QUSER.name as USER_NAME, QUSER.account as USER_ACCOUNT,SESSION.id as SESSION_ID,SESSION.sessionKey as SESSION_TOKEN,SESSION.expiryTime as SESSION_EXPIRYTIME  from Users QUSER left join Sessions SESSION on SESSION.sessionUser = QUSER.id where QUSER.id = @user");            
            query.parameters["user"] = user;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    session = new Session(reader);
                }
            }
            return session;
        }

       

        //IsAuthorized
        internal bool isAuthorized(HttpActionContext actionContext)
        {
            String token = this.tokenFromRequestHeader(actionContext);
            if(token == null)
                token = this.tokenFromCookies(actionContext);
            if (token == null)
                return false;
            WSQuery query = new WSQuery("Select SESSION.id as SESSION_ID,SESSION.token as SESSION_TOKEN,SESSION.expiryTime as SESSION_EXPIRYTIME,  QUSER.id as USER_ID  from Users QUSER left join Sessions SESSION on SESSION.[user] = QUSER.id where SESSION.token = @token");
            query.parameters["token"] = token;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(query).HasRows;            
        }

        //Get Token From Cookies
        string tokenFromCookies(HttpActionContext actionContext)
        {
            CookieHeaderValue cookie = actionContext.Request.Headers.GetCookies("auth_headers").FirstOrDefault();
            if (cookie != null) {
                Collection<CookieState> cookies = cookie.Cookies;
                foreach (CookieState state in cookies)
                {
                    if (state.Name.Equals("auth_headers"))
                    {
                        string token = state.Value;
                        return (string)JsonConvert.DeserializeObject<Dictionary<string, object>>(token)["access-token"];
                    }
                }
            }
            return null;
        }

        //Get Token From Request Headers
        string tokenFromRequestHeader(HttpActionContext actionContext)
        {           
            try
            {
                foreach (KeyValuePair<string, IEnumerable<string>> header in actionContext.Request.Headers)
                {
                    if (header.Key.Equals(SessionConfig.ACCSESS_TOKEN_KEY))
                    {
                        string[] stringstokens = (string[])header.Value;
                        return stringstokens[0];
                    }
                }
             
            }           
            catch
            {

            }
            return null;
        }

        //Get Token From Request 
        string tokenFromRequest(HttpRequestMessage request)
        {
            try
            {
                foreach (KeyValuePair<string, IEnumerable<string>> header in request.Headers)
                {
                    if (header.Key.Equals(SessionConfig.ACCSESS_TOKEN_KEY))
                    {
                        string[] stringstokens = (string[])header.Value;
                        return stringstokens[0];
                    }
                }

            }
            catch
            {

            }
            return null;
        }
        //Get Token From Request  Cookies
        string tokenFromRequestCookies(HttpRequestMessage request)
        {
            CookieHeaderValue cookie = request.Headers.GetCookies("auth_headers").FirstOrDefault();
            Collection<CookieState> cookies = cookie.Cookies;
            foreach (CookieState state in cookies)
            {
                if (state.Name.Equals("auth_headers"))
                {
                    string token = state.Value;
                    return (string)JsonConvert.DeserializeObject<Dictionary<string, object>>(token)[SessionConfig.ACCSESS_TOKEN_KEY];
                }
            }
            return null;
        }
    }
}