﻿using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using BNS.Models.Users;
using BNS.DAL.DALUser;

namespace BNS.WebAPI.Authentication
{
    public class BNSAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {

            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
            User user = new User();
            UserLogin newUser = new UserLogin();
            
            user = newUser.isValidUser(context.UserName, context.Password);
            if (user.id == 0)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }      

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            identity.AddClaim(new Claim("userId", user.id.ToString()));
            identity.AddClaim(new Claim("accountId", user.account.id.ToString()));
            //identity.AddClaim(new Claim("sub", user.account.id.ToString()));
            //identity.AddClaim(new Claim("email", "amit@gmail.com"));
            //identity.AddClaim(new Claim("role", "user"));
            //identity.AddClaim(new Claim("role", "user"));
            //context.OwinContext.Set<string>("new data", "Hello New data");
            context.Validated(identity);

        }
    }
}