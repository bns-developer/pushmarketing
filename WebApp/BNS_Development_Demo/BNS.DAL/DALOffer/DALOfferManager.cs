﻿using System;
using System.Collections.Generic;
using System.Web;
using BNS.Models.Offers;
using BNS.DL.WSDatabase;
using BNS.Models.Countries;
using BNS.Models.Categories;
using BNS.Models.Accounts;
using System.Data.SqlClient;
using BNS.DAL.Constants;
using BNS.Models.Kiosks;
using System.Linq;
using WSDatabase;
using System.Data;
using BNS.DAL.DALBeacon;

namespace BNS.DAL.DALOffer
{
    public class DALOfferManager
    {       
        public bool saveOffer(Offer offer)
        {
            using (WSTransaction transaction = new WSTransaction("SAVE_OFFER"))
            {
                if (offer.file != null)
                {
                    String fileExtension = System.IO.Path.GetExtension(offer.file.FileName);
                    String fileName = "BNS_" + DateTime.Now.ToString().Replace("/", "_").Replace(":", "_").Replace(" ", "_") + "_"; ;
                    offer.file.SaveAs(HttpContext.Current.Server.MapPath("~" + BNSConstants.OFFER_ICON_FILES_BASE_PATH) + fileName + fileExtension);
                    offer.imagePath = BNSConstants.OFFER_ICON_FILES_BASE_PATH + fileName + fileExtension;
                }
                String query = "INSERT INTO Offers(type, number, title, description, start_time, end_time, duration, image_path, account_id, created_on, updated_on, created_by, updated_by) OUTPUT Inserted.id VALUES(@type, @number, @title, @description, @startTime, @endTime, @duration, @imagePath, @account, @createdOn, @updatedOn, @createdBy, @updatedBy);";
                WSQuery Query = new WSQuery(query, offer.toDictionary(), transaction);
                offer.id = WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQueryReturnID(Query);
                if (offer.id != 0)
                {
                    foreach (Category category in offer.categories)
                    {
                        if(! this.saveOfferCategory(offer.id, category, transaction))
                        {
                            transaction.rollback();
                            return false;
                        }
                    }
                    foreach (Country country in offer.countries)
                    {
                        if(! this.saveOfferCountry(offer.id, country, transaction))
                        {
                            transaction.rollback();
                            return false;
                        }
                    }
                    transaction.commit();
                    return true;
                }
                else
                {
                    transaction.rollback();
                    return false;
                }
               
            }                    
        }

        public List<Category> getCategoriesForOffer(Int64 offerId)
        {
            String query = QueryConstants.CATEGORY_FOR_OFFER + offerId.ToString();
            WSQuery Query = new WSQuery(query);
            Query.parameters["offerId"] = offerId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Category> offerCategories = new List<Category>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offerCategories.Add(new Category(reader));
                }
            }
            Query.connection.Close();
            return offerCategories;
        }

        public List<Country> getCountriesForOffer(Int64 offerId)
        {
            String query = QueryConstants.OFFER_COUNTRIES + offerId.ToString();
            WSQuery Query = new WSQuery(query);
            Query.parameters["offerId"] = offerId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Country> offerCountries = new List<Country>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offerCountries.Add(new Country(reader));
                }
            }
            Query.connection.Close();
            return offerCountries;
        }

        public List<Offer> getAll(Account account)
        {
            String query = QueryConstants.ALL_OFFERS_BY_ACCOUNT + account.id.ToString() + " ORDER BY OFFER_CREATED_ON DESC";
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = account.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Offer> offersList = new List<Offer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offersList.Add(new Offer(reader));
                }
            }
            Query.connection.Close();
            foreach (Offer offer in offersList)
            {
                offer.categories = getCategoriesForOffer(offer.id);
            }
            return offersList;
        }

        public Offer getDetails(Offer offer)
        {
            String query = QueryConstants.OFFER_DETAILS + offer.id.ToString();
            WSQuery Query = new WSQuery(query);
            Query.parameters["offerId"] = offer.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);           
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offer = new Offer(reader);
                }              
            }
            Query.connection.Close();
            offer.categories = getCategoriesForOffer(offer.id);          
            offer.countries = getCountriesForOffer(offer.id);
            return offer;
        }

        public Offer getUserLatestOfferDetails(Offer offer)
        {
            String query = QueryConstants.OFFER_DETAILS + offer.id.ToString();
            WSQuery Query = new WSQuery(query);
            Query.parameters["offerId"] = offer.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offer = new Offer(reader);
                }
            }
            Query.connection.Close();
            return offer;
        }

        public List<Offer> getOffersForLandingPage(Account account)
        {
            String currentDateTime = DateTime.Now.ToString();
            String query = QueryConstants.LANDING_PAGE_OFFERS + account.id.ToString() + " AND OFFER.end_time >= '" + currentDateTime + "' ORDER BY OFFER_START_TIME DESC";
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = account.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Offer> offersList = new List<Offer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offersList.Add(new Offer(reader));
                }
            }
            Query.connection.Close();
            foreach (Offer offer in offersList)
            {
                offer.categories = getCategoriesForOffer(offer.id);
            }
            foreach (Offer offer in offersList)
            {
                offer.countries = getCountriesForOffer(offer.id);
            }
            
            return offersList;
        }


        public bool checkIfNumberExists(String offerNumber, Int64 account)
        {
            String query = "SELECT CASE WHEN EXISTS (Select * from Offers where number = @offerNumber and account_id = @accountId) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);           
            Query.parameters["offerNumber"] = offerNumber;
            Query.parameters["accountId"] = account;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }

        public bool saveOfferCountry(Int64 offer, Country country, WSTransaction transaction)
        {
            String query = "INSERT INTO Offers_Countries(offer_id, country_id) VALUES(@offerId, @countryId);";
            WSQuery Query = new WSQuery(query);
            Query.parameters["offerId"] = offer;
            Query.parameters["countryId"] = country.id;
            Query.transaction = transaction;

            return WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);
        }

        public bool saveOfferCategory(Int64 offer, Category category, WSTransaction transaction)
        {

            String query = "INSERT INTO Offers_Categories(offer_id, category_id, value) VALUES(@offerId, @categoryId, @value);";
            WSQuery Query = new WSQuery(query);
            Query.parameters["offerId"] = offer;
            Query.parameters["categoryId"] = category.id;
            Query.parameters["value"] = category.value;
            Query.transaction = transaction;

            return WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);
        }

        public bool saveKioskLuckyItemOffer(Int64 kioskId, Int64 luckyItemId, Offer offer) 
        {
            using (WSTransaction transaction = new WSTransaction("SET_KIOSK_LUCKY_ITEM_OFFER"))
            {              
                String query = "INSERT INTO Kiosks_Lucky_Items_Offers(kiosk_id, lucky_item_id, offer_id, is_default, created_on, updated_on, created_by, updated_by) VALUES(@kioskId, @luckyItemId, @offerId, @isDefault, @createdOn, @updatedOn, @createdBy, @updatedBy);";
                WSQuery Query = new WSQuery(query);
                Query.parameters["kioskId"] = kioskId;
                Query.parameters["luckyItemId"] = luckyItemId;
                Query.parameters["offerId"] = offer.id;
                Query.parameters["isDefault"] = 0;
                Query.parameters["createdOn"] = DateTime.Now.ToString();
                Query.parameters["updatedOn"] = DateTime.Now.ToString();
                Query.parameters["createdBy"] = offer.createdBy.id;
                Query.parameters["updatedBy"] = offer.updatedBy.id;
                Query.transaction = transaction;            
                if (! WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query))
                {
                    transaction.rollback();
                    return false;
                }
                else
                {
                    transaction.commit();
                    return true;
                }
            }              
        }      

        public bool changeLuckyItemDefaultOffer(Int64 kioskId, Int64 luckyItemId, Offer offer)
        {
            using (WSTransaction transaction = new WSTransaction("CHANGE_LUCKY_ITEM_DEFAULT_OFFER"))
            {
                if(this.updateLuckyItemOfferToNonDefault(kioskId, luckyItemId, transaction))
                {
                    String query = "INSERT INTO Kiosks_Lucky_Items_Offers(kiosk_id, lucky_item_id, offer_id, is_default, created_on, updated_on, created_by, updated_by) VALUES(@kioskId, @luckyItemId, @offerId, @isDefault, @createdOn, @updatedOn, @createdBy, @updatedBy);";
                    WSQuery Query = new WSQuery(query);
                    Query.parameters["kioskId"] = kioskId;
                    Query.parameters["luckyItemId"] = luckyItemId;
                    Query.parameters["offerId"] = offer.id;
                    Query.parameters["isDefault"] = 1;
                    Query.parameters["createdOn"] = DateTime.Now.ToString();
                    Query.parameters["updatedOn"] = DateTime.Now.ToString();
                    Query.parameters["createdBy"] = offer.createdBy.id;
                    Query.parameters["updatedBy"] = offer.updatedBy.id;
                    Query.transaction = transaction;
                    if (!WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query))
                    {
                        transaction.rollback();
                        return false;
                    }
                    else
                    {
                        transaction.commit();
                        return true;
                    }
                }
                else
                {
                    transaction.rollback();
                    return false;
                }
                
            }

        }
        
        public bool updateLuckyItemOfferToNonDefault(Int64 kioskId, Int64 luckyItemId, WSTransaction transaction)
        {
            String query = "UPDATE Kiosks_Lucky_Items_Offers SET is_default = 0 WHERE is_default = 1 AND kiosk_id = " + kioskId.ToString() + "AND lucky_item_id = " + luckyItemId.ToString();
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = kioskId;
            Query.parameters["luckyItemId"] = luckyItemId;
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeUpdateQuery(Query);      
        }

        public Offer getLuckyItemDefaultOffer(Int64 kioskId, Int64 luckyItemId)
        {
            String query = QueryConstants.LUCKY_ITEM_DEFAULT_OFFER + kioskId.ToString() + " AND KIOSKS_LICKY_ITEM_OFFER.lucky_item_id = " + luckyItemId.ToString();
            WSQuery Query = new WSQuery(query);      
            Query.parameters["kioskId"] = kioskId;
            Query.parameters["luckyItemId"] = luckyItemId;           
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            Offer offer = new Offer();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offer = new Offer(reader);
                }
            }
            Query.connection.Close();
            Offer currentActiveOffer = luckyItemCurrentActiveOffer(kioskId, luckyItemId);
            if(offer.id == currentActiveOffer.id)
            {
                offer.isCurrentlyActive = true;
            }
            else
            {
                offer.isCurrentlyActive = false;
            }
            offer.categories = getCategoriesForOffer(offer.id);
            offer.countries = getCountriesForOffer(offer.id);

            return offer;
        }

        public List<Offer> getLuckyItemNonDefaultOffers(Int64 kioskId, Int64 luckyItemId, Int64 accountId)
        {
            String query = QueryConstants.LUCKY_ITEM_NON_DEFAULT_OFFERS + kioskId.ToString() + " AND KIOSKS_LUCKY_ITEM_OFFER.lucky_item_id = " + luckyItemId.ToString() + ") AND OFFER.account_id =" + accountId.ToString();
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = kioskId;
            Query.parameters["luckyItemId"] = luckyItemId;
            Query.parameters["accountId"] = accountId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Offer> offersList = new List<Offer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offersList.Add(new Offer(reader));
                }
            }
            Query.connection.Close();
            foreach (Offer offer in offersList)
            {
                offer.categories = getCategoriesForOffer(offer.id);
            }
            return offersList;
        }
                                                 
        public Offer getLuckyItemOfferForKiosk(Int64 kioskId, Int64 luckyItemId)
        {
            String query = QueryConstants.KIOSK_LUCKY_ITEM_OFFER + "KIOSKS_LICKY_ITEM_OFFER.kiosk_id = " + kioskId.ToString() + " AND KIOSKS_LICKY_ITEM_OFFER.lucky_item_id = " + luckyItemId.ToString() + " AND KIOSKS_LICKY_ITEM_OFFER.is_default = 1";
            WSQuery Query = new WSQuery(query);    
            Query.parameters["kioskId"] = kioskId;
            Query.parameters["luckyItemId"] = luckyItemId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            Offer offer = new Offer();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offer = new Offer(reader);                                   
                }
            }
            Query.connection.Close();
            offer.categories = getCategoriesForOffer(offer.id);
            return offer;
        }

        public Offer luckyItemCurrentActiveOffer(Int64 kioskId, Int64 luckyItemId)
        {
            String currentDateTime = DateTime.Now.ToString();

            String query = QueryConstants.KIOSK_LUCKY_ITEM_CURRENT_OFFER + "KIOSKS_LICKY_ITEM_OFFER.kiosk_id = " + kioskId.ToString() + " AND KIOSKS_LICKY_ITEM_OFFER.lucky_item_id = " + luckyItemId.ToString() + " AND KIOSKS_LICKY_ITEM_OFFER.is_default = 0 AND OFFER.end_time >= '" + currentDateTime + "' ORDER BY OFFER.updated_on DESC";
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = kioskId;
            Query.parameters["luckyItemId"] = luckyItemId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            Offer offer = new Offer();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offer = new Offer(reader);
                }              
            }
            Query.connection.Close();            
            return offer;
        }

        public Offer getLuckyItemCurrentActiveOfferForKiosk(Int64 kioskId, Int64 luckyItemId)
        {
            String currentDateTime = DateTime.Now.ToString();
            
            String query = QueryConstants.KIOSK_LUCKY_ITEM_CURRENT_OFFER + "KIOSKS_LICKY_ITEM_OFFER.kiosk_id = " + kioskId.ToString() + " AND KIOSKS_LICKY_ITEM_OFFER.lucky_item_id = " + luckyItemId.ToString() + " AND KIOSKS_LICKY_ITEM_OFFER.is_default = 0 AND OFFER.end_time >= '" + currentDateTime + "' ORDER BY OFFER.updated_on DESC";
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = kioskId;
            Query.parameters["luckyItemId"] = luckyItemId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            Offer offer = new Offer();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offer = new Offer(reader);                  
                }
                Query.connection.Close();
                offer.categories = getCategoriesForOffer(offer.id);
                offer.countries = getCountriesForOffer(offer.id);
                return offer;
            }
           
            else
            {
               offer =  this.getLuckyItemDefaultOffer(kioskId, luckyItemId);
                return offer;
            }
        }

        public List<Offer> getLuckyItemActiveOffers(Int64 kiosk_id, Int64 lucky_item_id)
        {                         
            String currentDateTime = DateTime.Now.ToString();
            String query = QueryConstants.LUCKY_ITEM_ACTIVE_OFFERS + "KIOSKS_LICKY_ITEM_OFFER.kiosk_id = " + kiosk_id.ToString() + " AND KIOSKS_LICKY_ITEM_OFFER.lucky_item_id = " + lucky_item_id.ToString() + " AND KIOSKS_LICKY_ITEM_OFFER.is_default = 0 AND OFFER.end_time >= '" + currentDateTime + "'";
            WSQuery Query = new WSQuery(query);       
            Query.parameters["kioskId"] = kiosk_id;
            Query.parameters["luckyItemId"] = lucky_item_id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Offer> offersList = new List<Offer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {                 
                    offersList.Add(new Offer(reader));
                }
            }
            Query.connection.Close();
            Offer currentActiveOffer = luckyItemCurrentActiveOffer(kiosk_id, lucky_item_id);
            foreach (Offer offer in offersList)
            {
                offer.categories = getCategoriesForOffer(offer.id);
                if(offer.id == currentActiveOffer.id)
                {
                    offer.isCurrentlyActive = true;
                }
                else
                {
                    offer.isCurrentlyActive = false;
                }
            }
            return offersList;
        }

        public List<Offer> getLuckyItemOffersHistory(Int64 kiosk_id, Int64 lucky_item_id)
        {           
            String currentDateTime = DateTime.Now.ToString();
            String query = QueryConstants.LUCKY_ITEM_OFFERS_HISTORY + "KIOSKS_LICKY_ITEM_OFFER.kiosk_id = " + kiosk_id.ToString() + " AND KIOSKS_LICKY_ITEM_OFFER.lucky_item_id = " + lucky_item_id.ToString() + " AND KIOSKS_LICKY_ITEM_OFFER.is_default = 0 AND OFFER.end_time < '" + currentDateTime + "'";
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = kiosk_id;
            Query.parameters["luckyItemId"] = lucky_item_id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Offer> offersList = new List<Offer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offersList.Add(new Offer(reader));
                }
            }
            Query.connection.Close();
            foreach (Offer offer in offersList)
            {
                offer.categories = getCategoriesForOffer(offer.id);
            }
            return offersList;
        }

        public List<Offer> getLuckyItemInactiveOffers(Int64 kiosk_id, Int64 lucky_item_id, Int64 accountId)
        {
            String currentDateTime = DateTime.Now.ToString();
            String query = QueryConstants.LUCKY_ITEM_INACTIVE_OFFERS + " OFFER.end_time > '" + currentDateTime + "' AND OFFER.id NOT IN (SELECT KIOSKS_LUCKY_ITEM_OFFER.offer_id FROM Kiosks_Lucky_Items_Offers KIOSKS_LUCKY_ITEM_OFFER WHERE KIOSKS_LUCKY_ITEM_OFFER.kiosk_id = " + kiosk_id.ToString() + " AND KIOSKS_LUCKY_ITEM_OFFER.lucky_item_id = " + lucky_item_id.ToString() + " ) AND OFFER.account_id = " + accountId.ToString();
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = kiosk_id;
            Query.parameters["luckyItemId"] = lucky_item_id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Offer> offersList = new List<Offer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offersList.Add(new Offer(reader));
                }
            }
            Query.connection.Close();
            foreach (Offer offer in offersList)
            {
                offer.categories = getCategoriesForOffer(offer.id);
            }
            return offersList;
        }


        public bool saveBeaconOffer(Int64 beaconId, Offer offer)
        {
            using (WSTransaction transaction = new WSTransaction("SET_BEACON_OFFER"))
            {
                String query = "INSERT INTO Beacons_Offers(beacon_id, offer_id, is_default, created_on, updated_on, created_by, updated_by) VALUES(@beaconId, @offerId, @isDefault, @createdOn, @updatedOn, @createdBy, @updatedBy);";
                WSQuery Query = new WSQuery(query);
                Query.parameters["beaconId"] = beaconId;
                Query.parameters["offerId"] = offer.id;
                Query.parameters["isDefault"] = 0;
                Query.parameters["createdOn"] = DateTime.Now.ToString();
                Query.parameters["updatedOn"] = DateTime.Now.ToString();
                Query.parameters["createdBy"] = offer.createdBy.id;
                Query.parameters["updatedBy"] = offer.updatedBy.id;
                Query.transaction = transaction;               
                if( !WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query))
                {
                    transaction.rollback();
                    return false;
                }
                else
                {
                    transaction.commit();
                    return true;
                }
            }              
        }

        public bool changeBeaconDefaultOffer(Int64 beaconId, Offer offer)
        {
            using (WSTransaction transaction = new WSTransaction("CHANGE_BEACON_DEFAULT_OFFER"))
            {
                if(this.updateBeaconOfferToNonDefault(beaconId, transaction))
                {
                    String query = "INSERT INTO Beacons_Offers(beacon_id, offer_id, is_default, created_on, updated_on, created_by, updated_by) VALUES(@beaconId, @offerId, @isDefault, @createdOn, @updatedOn, @createdBy, @updatedBy);";
                    WSQuery Query = new WSQuery(query);
                    Query.parameters["beaconId"] = beaconId;
                    Query.parameters["offerId"] = offer.id;
                    Query.parameters["isDefault"] = 1;
                    Query.parameters["createdOn"] = DateTime.Now.ToString();
                    Query.parameters["updatedOn"] = DateTime.Now.ToString();
                    Query.parameters["createdBy"] = offer.createdBy.id;
                    Query.parameters["updatedBy"] = offer.updatedBy.id;
                    Query.transaction = transaction;
                    if (!WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query))
                    {
                        transaction.rollback();
                        return false;
                    }
                    else
                    {
                        transaction.commit();
                        return true;
                    }
                }
                else
                {
                    transaction.rollback();
                    return false;
                }
                
            }
        }

        public bool updateBeaconOfferToNonDefault(Int64 beaconId, WSTransaction transaction)
        {
            String query = "UPDATE Beacons_Offers SET is_default = 0 WHERE is_default = 1 AND beacon_id = " + beaconId.ToString();
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beaconId;
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeUpdateQuery(Query);
        }

        public Offer getBeaconDefaultOffer(Int64 beacon_id)
        {
            String query = QueryConstants.BEACON_DEFAULT_OFFER + beacon_id.ToString();
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beacon_id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            Offer offer = new Offer();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offer= new Offer(reader);
                }
            }
            Query.connection.Close();
            Offer currentActiveOffer = beaconCurrentActiveOffer(beacon_id);
            if (offer.id == currentActiveOffer.id)
            {
                offer.isCurrentlyActive = true;
            }
            else
            {
                offer.isCurrentlyActive = false;
            }
            offer.categories = getCategoriesForOffer(offer.id);
            offer.countries = getCountriesForOffer(offer.id);
            return offer;
        }

        public List<Offer> getBeaconNonDefaultOffers(Int64 beacon_id, Int64 accountId)
        {
            String query = QueryConstants.BEACON_NON_DEFAULT_OFFER + beacon_id.ToString() + ") AND OFFER.account_id = " + accountId.ToString();
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beacon_id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Offer> offersList = new List<Offer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offersList.Add(new Offer(reader));
                }
            }
            Query.connection.Close();
            foreach (Offer offer in offersList)
            {
                offer.categories = getCategoriesForOffer(offer.id);
            }

            return offersList;
        }

        public List<Offer> getBeaconActiveOffers(Int64 beacon_id)
        {
            String currentDateTime = DateTime.Now.ToString();
            String query = QueryConstants.BEACON_ACTIVE_OFFERS + beacon_id.ToString() + " AND BEACON_OFFER.is_default = 0 AND OFFER.end_time >= '" + currentDateTime + "'";
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beacon_id;            
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Offer> offersList = new List<Offer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offersList.Add(new Offer(reader));
                }
            }
            Query.connection.Close();
            Offer currentActiveOffer = beaconCurrentActiveOffer(beacon_id);
            foreach (Offer offer in offersList)
            {
                offer.categories = getCategoriesForOffer(offer.id);
                if (offer.id == currentActiveOffer.id)
                {
                    offer.isCurrentlyActive = true;
                }
                else
                {
                    offer.isCurrentlyActive = false;
                }
            }
            return offersList;
        }

        public List<Offer> getBeaconOffersHistory(Int64 beacon_id)
        {
            String currentDateTime = DateTime.Now.ToString();
            String query = QueryConstants.BEACON_OFFERS_HISTORY + beacon_id.ToString() + " AND BEACON_OFFER.is_default = 0 AND OFFER.end_time < '" + currentDateTime + "'";
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beacon_id;            
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Offer> offersList = new List<Offer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offersList.Add(new Offer(reader));
                }
            }
            Query.connection.Close();
            foreach (Offer offer in offersList)
            {
                offer.categories = getCategoriesForOffer(offer.id);
            }
            return offersList;
        }

        public List<Offer> getBeaconInactiveOffers(Int64 beacon_id, Int64 accountId)
        {
            String currentDateTime = DateTime.Now.ToString();
            String query = QueryConstants.BEACON_INACTIVE_OFFERS + " OFFER.end_time >= '" + currentDateTime + "' AND OFFER.id NOT IN (SELECT BEACON_OFFER.offer_id FROM Beacons_Offers BEACON_OFFER WHERE BEACON_OFFER.beacon_id = " + beacon_id.ToString() + " ) AND OFFER.account_id = " + accountId.ToString();
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beacon_id;           
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Offer> offersList = new List<Offer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offersList.Add(new Offer(reader));
                }               
            }
            Query.connection.Close();
            //DataSet dataSet = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQueryAndReturnDataSet(Query);

            //if(dataSet != null && dataSet.Tables.Count !=0)
            //{
            //    DataTable offerTable = dataSet.Tables[0];
            //    foreach(DataRow row in offerTable.Rows)
            //    {
            //        offersList.Add(new Offer(row));
            //    }
            //}

            foreach (Offer offer in offersList)
            {
                offer.categories = getCategoriesForOffer(offer.id);
            }
            return offersList;
        }


        public Offer beaconCurrentActiveOffer(Int64 beaconId)
        {
            String currentDateTime = DateTime.Now.ToString();
            String query = QueryConstants.BEACON_CURRENT_OFFER + " BEACON_OFFER.beacon_id = " + beaconId.ToString() + " AND BEACON_OFFER.is_default = 0 AND OFFER.end_time >= '" + currentDateTime + "' ORDER BY OFFER.updated_on DESC";
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beaconId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            Offer offer = new Offer();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offer = new Offer(reader);
                }               
            }
            Query.connection.Close();           
            return offer;
        }
        public Offer getCurrentActiveOfferForBeacon(String beaconGuid)
        {
            DALBeaconManager beaconManager = new DALBeaconManager();
            Int64 beaconId = beaconManager.getBeaconIdFromGuid(beaconGuid);
            String currentDateTime = DateTime.Now.ToString();
            String query = QueryConstants.BEACON_CURRENT_OFFER + " BEACON_OFFER.beacon_id = " + beaconId.ToString() + " AND BEACON_OFFER.is_default = 0 AND OFFER.end_time >= '" + currentDateTime + "' ORDER BY OFFER.updated_on DESC";
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beaconId;     
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            Offer offer = new Offer();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                     offer = new Offer(reader);                   
                }
                Query.connection.Close();
                offer.categories = getCategoriesForOffer(offer.id);
                offer.countries = getCountriesForOffer(offer.id);
                return offer;
            }           
            else
            {
                offer = this.getBeaconDefaultOffer(beaconId);
                return offer;
            }
            
        }
    }
}