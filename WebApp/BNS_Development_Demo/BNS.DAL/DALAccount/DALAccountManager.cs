﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using BNS.Models.Accounts;
using BNS.DL.WSDatabase;
using BNS.DAL.Constants;
using System.Data.SqlClient;
using WSDatabase;
using System.IO;
using BNS.DAL.DALUser;
using BNS.DAL.DALCategory;
using BNS.DAL.DALLuckyItem;


namespace BNS.DAL.DALAccount
{
    public class DALAccountManager
    {
        public DALCategoryManager categoryManager = new DALCategoryManager();
        public DALLuckyItemManager luckyItemManager = new DALLuckyItemManager();

        public Int64 addAccount(Account account)
        {
            using (WSTransaction transaction = new WSTransaction("SAVE_ACCOUNT"))
            {
                account.picUrl = getPicUrl(account);
                String query = "INSERT INTO Accounts(name, pic_url, website, created_on, updated_on) OUTPUT Inserted.id VALUES(@name, @picUrl, @website, @createdOn, @updatedOn)";
                WSQuery Query = new WSQuery(query, account.toDictionary(), transaction);
                account.id = WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQueryReturnID(Query);
                if (account.id != 0)
                {
                    account.admin.id = new DALUserManager().creatUser(account, transaction);
                    if (account.admin.id != 0 && categoryManager.setCategoriesForNewAccount(account, transaction) && luckyItemManager.setLuckyItemsForNewAccount(account, transaction))
                    {
                        transaction.commit();
                        return account.id;
                    }
                    else
                    {
                        transaction.rollback();
                        return 0;
                    }
                }
                else
                {
                    transaction.rollback();
                    return 0;
                }
            }
        }

        public String getPicUrl(Account account)
        {
            if (account.logo != null)
            {
                String fileExtension = System.IO.Path.GetExtension(account.logo.FileName);
                String fileName = account.name + "_" + DateTime.Now.ToString().Replace("/", "_").Replace(":", "_").Replace(" ", "_") + "_"; ;
                account.logo.SaveAs(HttpContext.Current.Server.MapPath("~" + BNSConstants.ACCOUNT_LOGO_FILES_BASE_PATH) + fileName + fileExtension);
                account.picUrl = BNSConstants.ACCOUNT_LOGO_FILES_BASE_PATH + fileName + fileExtension;
                return account.picUrl;
            }
            else
            {
                return null;
            }
        }

        public Account getDetails(Account account)
        {
            String query = QueryConstants.ACCOUNT_DETAILS + account.id.ToString();
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = account.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    account = new Account(reader);
                }
            }
            Query.connection.Close();
            return account;
        }

        public Int64 isValidUser(String username, String password)
        {
            Dictionary<String, Object> parameters = new Dictionary<String, Object>();
            parameters["username"] = username;
            parameters["password"] = password;

            if (username.Equals("novomatic@gmail.com") && password.Equals("pass@novomatic"))
            {
                return 101;
            }
            else
            {
                return 0;
            }

            //String query = "SELECT * FROM Users Where email = @username AND password= @password;";
            //System.Data.SqlClient.SqlDataReader reader = DatabaseManager.sharedDatabaseManagerinstance.getRecordsFromDatabase(query, parameters);
            //if (reader.HasRows)
            //{
            //    Int64 user = 0;
            //    while (reader.Read())
            //    {
            //        user = reader.GetInt64(reader.GetOrdinal("id"));
            //    }
            //    Session session = new Session();
            //    session.user = user;
            //    SessionManager.sharedInstance.save(session);
            //    return user;
            //}
            //else

            //{
            //    return 0;
            //}
        }

        public bool update(Account account)
        {
            using (WSTransaction transaction = new WSTransaction("UPDATE_ACCOUNT"))
            {
                String path = "C:/Projects/BNS_BASE/BNS/BNS_Development/BNS.WebAPI";
                Account oldAccount = getDetails(account);

                if (account.logo != null)
                {
                    account.picUrl = getPicUrl(account);
                }
                if (account.picUrl == null)
                {
                    account.picUrl = oldAccount.picUrl;
                }
                else
                {
                    if (File.Exists(path + oldAccount.picUrl))
                    {
                        File.Delete(path + oldAccount.picUrl);
                    }
                }
                account.updatedOn = DateTime.Now.ToString();

                String query = "UPDATE Accounts set name = '" + account.name + "', website = '" + account.website + "', pic_url = '" + account.picUrl + "', updated_on = '" + account.updatedOn + "' where id = " + account.id;
                WSQuery Query = new WSQuery(query);
                Query.transaction = transaction;
                if (!WSDatabaseManager.sharedDatabaseManagerinstance.executeUpdateQuery(Query))
                {
                    transaction.rollback();
                    return false;
                }
                transaction.commit();
                return true;
            }
        }

        public bool logout(HttpRequestMessage request)
        {
            //return SessionManager.sharedInstance.clearSession(request);
            return true;
        }

    }
}
        