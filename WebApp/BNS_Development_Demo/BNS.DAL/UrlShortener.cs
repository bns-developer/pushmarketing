﻿using Google.Apis.Services;
using Google.Apis.Urlshortener.v1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNS.DAL
{
    public class UrlShortener
    {
        private const String apiKey = "AIzaSyBcRCHTCTytwXuPQAihSU_7BgMk1bpQqAU";

        public static String shortenIt(String url)
        {
            UrlshortenerService service = new UrlshortenerService(new BaseClientService.Initializer()
            {
                ApiKey = apiKey,
                ApplicationName = "Best Network System",
            });

            var m = new Google.Apis.Urlshortener.v1.Data.Url();
            m.LongUrl = url;
            return service.Url.Insert(m).Execute().Id;
        }
    }
}