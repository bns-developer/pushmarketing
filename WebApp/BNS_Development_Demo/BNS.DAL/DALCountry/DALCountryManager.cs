﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.Countries;
using BNS.DL.WSDatabase;
using System.Data.SqlClient;
using WSDatabase;

namespace BNS.DAL.DALCountry
{
    public class DALCountryManager
    {
        public List<Country> getAll()
        {
            WSQuery Query = new WSQuery();
            Query.query = QueryConstants.ALL_COUNTRIES;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);

            List<Country> CountriesList = new List<Country>();
            if(reader.HasRows)
            {
                while(reader.Read())
                {
                    CountriesList.Add(new Country(reader));                      
                }               
            }
            Query.connection.Close();
            return CountriesList;
        }

        public List<Country> getCountryCodes()
        {
            WSQuery Query = new WSQuery();
            Query.query = QueryConstants.ALL_COUNTRY_CODE;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);

            List<Country> CountriesList = new List<Country>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    CountriesList.Add(new Country(reader));
                }            
            }
            Query.connection.Close();
            return CountriesList;
        }
    }
}