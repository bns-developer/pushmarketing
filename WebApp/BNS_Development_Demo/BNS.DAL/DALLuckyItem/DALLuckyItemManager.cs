﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.LuckyItems;
using BNS.Models.Accounts;
using BNS.DL.WSDatabase;
using System.Data.SqlClient;
using BNS.Models.Kiosks;
using BNS.Models.Stores;
using BNS.Models.Countries;
using WSDatabase;


namespace BNS.DAL.DALLuckyItem
{
    public class DALLuckyItemManager
    {
        public Kiosk getKioskLuckyItems(Kiosk kiosk)
        {
            String query = QueryConstants.All_LUCKY_ITEMS_BY_KIOSK + kiosk.id.ToString();
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = kiosk.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            kiosk.luckyItems = new List<LuckyItem>();
            if (reader.HasRows)
            {             
                while (reader.Read())
                {
                    kiosk.name = reader.GetString(reader.GetOrdinal("KIOSK_NAME"));
                    kiosk.luckyItems.Add(new LuckyItem(reader));
                }               
            }
            Query.connection.Close();
            return kiosk ;
        }
        
        public Kiosk getLuckyItem(Kiosk kiosk, Int64 luckyItemId)
        {
            String query = QueryConstants.KIOSK_LUCKY_ITEM_DETAILS1 + kiosk.id.ToString() + QueryConstants.KIOSK_LUCKY_ITEM_DETAILS2 + luckyItemId.ToString();
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = kiosk.id;
            Query.parameters["luckyItemId"] = luckyItemId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);           

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    kiosk = new Kiosk(reader);
                    kiosk.luckyItems.Add(new LuckyItem(reader));
                    kiosk.store = new Store(reader);
                    kiosk.store.country = new Country(reader);
                }                
            }
            Query.connection.Close();
            return kiosk;
        }

        public Kiosk getLuckyItemsForKiosk(Kiosk kiosk)
        {
            String query = QueryConstants.All_LUCKY_ITEMS_BY_KIOSK + kiosk.id.ToString();
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = kiosk.id;

            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            kiosk.luckyItems = new List<LuckyItem>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    kiosk.name = reader.GetString(reader.GetOrdinal("KIOSK_NAME"));
                    kiosk.luckyItems.Add(new LuckyItem(reader));
                }              
            }
            Query.connection.Close();
            return kiosk;
        }

        public bool setLuckyItemsForNewAccount(Account account, WSTransaction transaction)
        {
            bool result = false;
            List<LuckyItem> luckyItems = new List<LuckyItem>();

            LuckyItem item1 = new LuckyItem("Coin", account.id, "/Resources/Images/coin.png");
            luckyItems.Add(item1);
            LuckyItem item2 = new LuckyItem("4 Leaf Clover", account.id, "/Resources/Images/4-leaf-clover.png");
            luckyItems.Add(item2);
            LuckyItem item3 = new LuckyItem("Horse Shoe", account.id, "/Resources/Images/horse-shoe.png");
            luckyItems.Add(item3);
            LuckyItem item4 = new LuckyItem("Number", account.id, "/Resources/Images/number.png");
            luckyItems.Add(item4);
            LuckyItem item5 = new LuckyItem("Day", account.id, "/Resources/Images/day.png");
            luckyItems.Add(item5);
            LuckyItem item6 = new LuckyItem("Rainbow", account.id, "/ Resources / Images / rainbow.png");
            luckyItems.Add(item6);
            LuckyItem item7 = new LuckyItem("Surprise", account.id, "/Resources/Images/surprise.png");
            luckyItems.Add(item7);
            LuckyItem item8 = new LuckyItem("Rabbit Foot", account.id, "/Resources/Images/rabbit-foot.png");
            luckyItems.Add(item8);
            LuckyItem item9 = new LuckyItem("Pot of Gold", account.id, "/Resources/Images/pot-of-gold.png");
            luckyItems.Add(item9);
            LuckyItem item10 = new LuckyItem("Wheel of Fortune", account.id, "/Resources/Images/wheel-of-fortune.png");
            luckyItems.Add(item10);
            LuckyItem item11 = new LuckyItem("Money", account.id, "/Resources/Images/money.png");
            luckyItems.Add(item11);
            LuckyItem item12 = new LuckyItem("Charm", account.id, "/Resources/Images/charm.png");
            luckyItems.Add(item12);
            LuckyItem item13 = new LuckyItem("Color", account.id, "/Resources/Images/color.png");
            luckyItems.Add(item13);
            LuckyItem item14 = new LuckyItem("Birthday", account.id, "/Resources/Images/birthday.png");
            luckyItems.Add(item14);
            LuckyItem item15 = new LuckyItem("Dragon", account.id, "/Resources/Images/dragon.png");
            luckyItems.Add(item15);
            LuckyItem item16 = new LuckyItem("Pet", account.id, "/Resources/Images/pet.png");
            luckyItems.Add(item16);
            LuckyItem item17 = new LuckyItem("Dream", account.id, "/Resources/Images/dream.png");
            luckyItems.Add(item17);
            LuckyItem item18 = new LuckyItem("Winner", account.id, "/Resources/Images/winner.png");
            luckyItems.Add(item18);
            LuckyItem item19 = new LuckyItem("Pick One", account.id, "/Resources/Images/pick-one.png");
            luckyItems.Add(item19);
            LuckyItem item20 = new LuckyItem("Extra Toy", account.id, "/Resources/Images/extra-toy.png");
            luckyItems.Add(item20);

            foreach (LuckyItem luckyItem in luckyItems)
            {
                String query = "INSERT INTO Lucky_Items(name, account_id, icon) VALUES(@name, @accountId, @icon)";
                WSQuery Query = new WSQuery(query);
                Query.parameters["name"] = luckyItem.name;
                Query.parameters["accountId"] = luckyItem.account.id;
                Query.parameters["icon"] = luckyItem.icon;
                Query.transaction = transaction;
                result = WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);
            }
            return result;
        }
    }
}