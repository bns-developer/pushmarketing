﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNS.DAL
{
    public class QueryConstants
    {
        //STORES ALIAS
        public const String STORE_ID = "STORE_ID";
        public const String STORE_NUMBER = "STORE_NUMBER";
        public const String STORE_NAME = "STORE_NAME";
        public const String STORE_ACCOUNT_ID = "STORE_ACCOUNT_ID";
        public const String STORE_COUNTRY = "STORE_COUNTRY";
        public const String STORE_CREATED_ON = "STORE_CREATED_ON";
        public const String STORE_UPDATED_ON = "STORE_UPDATED_ON";
        public const String STORE_CREATED_BY = "STORE_CREATED_BY";
        public const String STORE_UPDATED_BY = "STORE_UPDATED_BY";
        public const String STORE_ALIAS = "STORE.id AS " + STORE_ID + ", STORE.number AS " + STORE_NUMBER + ", STORE.name AS " + STORE_NAME + ", STORE.country_id AS " + STORE_COUNTRY;
        public const String STORE_ALIAS_MINIMAL = "STORE.id AS " + STORE_ID + ", STORE.name AS " + STORE_NAME;
        public const String STORE_KIOSK_ALIAS = "STORE.id AS " + STORE_ID + ", STORE.name AS " + STORE_NAME + ", STORE.account_id AS " + STORE_ACCOUNT_ID;
        public const String STORE_BEACON_ALIAS = "STORE.id AS " + STORE_ID + ", STORE.name AS " + STORE_NAME + ", STORE.account_id AS " + STORE_ACCOUNT_ID;
        public const String ALL_STORES_BY_ACCOUNT = "SELECT " + STORE_ALIAS + ", " + COUNTRY_ALIAS + " FROM Stores STORE INNER JOIN Countries COUNTRY ON STORE.country_id = COUNTRY.id where";
        public const String ALL_STORES_FOR_DROPDOWN_BY_ACCOUNT = "SELECT " + STORE_ALIAS_MINIMAL + " FROM Stores STORE where";

        //COUNTRIES ALIAS
        public const String COUNTRY_ID = "COUNTRY_ID";
        public const String COUNTRY_NAME = "COUNTRY_NAME";
        public const String COUNTRY_FLAG_PATH = "COUNTRY_FLAG_PATH";
        public const String COUNTRY_CODE = "COUNTRY_CODE";
        public const String COUNTRY_CURRENCY = "COUNTRY_CURRENCY";
        public const String COUNTRY_ALIAS = "COUNTRY.id AS " + COUNTRY_ID + ", COUNTRY.name AS " + COUNTRY_NAME;
        public const String COUNTRY_ALIAS1 = "COUNTRY.id AS " + COUNTRY_ID + ", COUNTRY.country_code AS " + COUNTRY_CODE;
        public const String ALL_COUNTRIES = "SELECT COUNTRY.id AS " + COUNTRY_ID + ", COUNTRY.name AS " + COUNTRY_NAME + ", COUNTRY.currency AS " + COUNTRY_CURRENCY + ", COUNTRY.flag_path AS " + COUNTRY_FLAG_PATH + " FROM Countries COUNTRY ORDER BY COUNTRY_NAME ASC";
        public const String ALL_COUNTRY_CODE = "SELECT COUNTRY.id AS " + COUNTRY_ID + ", COUNTRY.name AS " + COUNTRY_NAME + ", COUNTRY.country_code AS " + COUNTRY_CODE + ", COUNTRY.flag_path AS " + COUNTRY_FLAG_PATH + " FROM Countries COUNTRY ORDER BY COUNTRY_NAME ASC";
        public const String OFFER_COUNTRIES = "SELECT COUNTRY.id AS " + COUNTRY_ID + ", COUNTRY.name AS " + COUNTRY_NAME + ", COUNTRY.currency AS " + COUNTRY_CURRENCY + " FROM Countries COUNTRY JOIN Offers_Countries OFFER_COUNTRY ON OFFER_COUNTRY.country_id = COUNTRY.id WHERE OFFER_COUNTRY.offer_id = ";
        //OFFERS ALIAS
        public const String OFFER_ID = "OFFER_ID";
        public const String OFFER_ACCOUNT = "OFFER_ACCOUNT";
        public const String OFFER_NUMBER = "OFFER_NUMBER";
        public const String OFFER_TYPE = "OFFER_TYPE";
        public const String OFFER_TITLE = "OFFER_TITLE";
        public const String OFFER_DESCRIPTION = "OFFER_DESCRIPTION";
        public const String OFFER_IMAGE_PATH = "OFFER_IMAGE_PATH";
        public const String OFFER_START_TIME = "OFFER_START_TIME";
        public const String OFFER_END_TIME = "OFFER_END_TIME";
        public const String OFFER_DURATION = "OFFER_DURATION";
        public const String OFFER_CREATED_ON = "OFFER_CREATED_ON";
        public const String OFFER_UPDATED_ON = "OFFER_UPDATED_ON";
        public const String OFFER_CREATED_BY = "OFFER_CREATED_BY";
        public const String OFFER_UPDATED_BY = "OFFER_UPDATED_BY";
        public const String REGISTERED_USERS_TODAY = "REGISTERED_USERS_TODAY";
        public const String REGISTERED_USERS_THIS_WEEK = "REGISTERED_USERS_THIS_WEEK";
        public const String REGISTERED_USERS_THIS_MONTH = "REGISTERED_USERS_THIS_MONTH";
        public const String TOTAL_REGISTERED_USERS = "TOTAL_REGISTERED_USERS";
        public const String BEACON_STATUS = "BEACON_STATUS";
        public const String KIOSK_STATUS = "KIOSK_STATUS";
        public const String OFFER_ALIAS = "OFFER.id AS " + OFFER_ID + ", OFFER.type AS " + OFFER_TYPE + ", OFFER.number AS " + OFFER_NUMBER + ", OFFER.image_path AS " + OFFER_IMAGE_PATH + ", OFFER.title AS " + OFFER_TITLE + ", OFFER.description AS " + OFFER_DESCRIPTION + ", OFFER.start_time AS " + OFFER_START_TIME + ", OFFER.end_time AS " + OFFER_END_TIME + ", OFFER.duration AS " + OFFER_DURATION + ", OFFER.created_on AS " + OFFER_CREATED_ON;
        public const String OFFER_ALIAS1 = "OFFER.id AS " + OFFER_ID + ", OFFER.number AS " + OFFER_NUMBER + ", OFFER.type AS " + OFFER_TYPE + ", OFFER.title AS " + OFFER_TITLE + ", OFFER.description AS " + OFFER_DESCRIPTION + ", OFFER.created_on AS " + OFFER_CREATED_ON + ", OFFER.account_id AS " + OFFER_ACCOUNT;
        public const String OFFER_ALIAS2 = "OFFER.id AS " + OFFER_ID + ", OFFER.title AS " + OFFER_TITLE + ", OFFER.description AS " + OFFER_DESCRIPTION + ", OFFER.created_by AS " + OFFER_CREATED_BY + ", OFFER.created_on AS " + OFFER_CREATED_ON;
        public const String OFFER_ALIAS3 = "OFFER.id AS " + OFFER_ID + ", OFFER.number AS " + OFFER_NUMBER + ", OFFER.title AS " + OFFER_TITLE + ", OFFER.description AS " + OFFER_DESCRIPTION;
        public const String OFFER_DETAILS = "SELECT " + OFFER_ALIAS + " FROM Offers OFFER WHERE OFFER.id = ";       
        public const String ALL_OFFERS_BY_ACCOUNT = "SELECT " + OFFER_ALIAS1 + OFFER_ADVANCED_DATA + ", " + BEACON_STATUS_ALIAS + " AS " + BEACON_STATUS + ", " + KIOSK_STATUS_ALIAS + " AS " + KIOSK_STATUS + " FROM Offers OFFER WHERE OFFER.account_id = ";
        public const String REGISTERED_USERS_TODAY_ALIAS = "(SELECT COUNT(USER_KIOSK.user_id) FROM users_kiosks USER_KIOSK WHERE USER_KIOSK.offer_id = OFFER.id AND USER_KIOSK.is_registered = 1 AND DATEPART(dd,USER_KIOSK.date) = DATEPART(dd,CURRENT_TIMESTAMP)) + (SELECT COUNT(USER_BEACON.user_id) FROM users_beacons USER_BEACON WHERE USER_BEACON.offer_id = OFFER.id AND USER_BEACON.is_registered = 1 AND DATEPART(dd,USER_BEACON.date) = DATEPART(dd,CURRENT_TIMESTAMP))";
        public const String REGISTERED_USERS_THIS_WEEK_ALIAS = "(SELECT COUNT(USER_KIOSK.user_id) FROM users_kiosks USER_KIOSK WHERE USER_KIOSK.offer_id = OFFER.id AND USER_KIOSK.is_registered = 1 AND DATEPART(wk, USER_KIOSK.date) = DATEPART(wk, CURRENT_TIMESTAMP)) + (SELECT COUNT(USER_BEACON.user_id) FROM users_beacons USER_BEACON WHERE USER_BEACON.offer_id = OFFER.id AND USER_BEACON.is_registered = 1 AND DATEPART(wk, USER_BEACON.date) = DATEPART(wk, CURRENT_TIMESTAMP))";
        public const String REGISTERED_USERS_THIS_MONTH_ALIAS = "(SELECT COUNT(USER_KIOSK.user_id) FROM users_kiosks USER_KIOSK WHERE USER_KIOSK.offer_id = OFFER.id AND USER_KIOSK.is_registered = 1 AND DATEPART(mm, USER_KIOSK.date) = DATEPART(mm, CURRENT_TIMESTAMP)) + (SELECT COUNT(USER_BEACON.user_id) FROM users_beacons USER_BEACON WHERE USER_BEACON.offer_id = OFFER.id AND USER_BEACON.is_registered = 1 AND DATEPART(mm, USER_BEACON.date) = DATEPART(mm, CURRENT_TIMESTAMP))";
        public const String TOTAL_REGISTERED_USERS_ALIAS = "(SELECT COUNT(USER_KIOSK.user_id) FROM users_kiosks USER_KIOSK WHERE USER_KIOSK.offer_id = OFFER.id AND USER_KIOSK.is_registered = 1) + (SELECT COUNT(USER_BEACON.user_id) FROM users_beacons USER_BEACON WHERE USER_BEACON.offer_id = OFFER.id AND USER_BEACON.is_registered = 1)";
        public const String BEACON_STATUS_ALIAS = "(SELECT COUNT(BEACON_OFFER.beacon_id) FROM Beacons_Offers BEACON_OFFER WHERE BEACON_OFFER.offer_id = OFFER.id AND OFFER.end_time >= CURRENT_TIMESTAMP)";
        public const String KIOSK_STATUS_ALIAS = "(SELECT  COUNT(KIOSK_LUCKY_ITEM_OFFER.kiosk_id) FROM Kiosks_Lucky_Items_Offers KIOSK_LUCKY_ITEM_OFFER WHERE KIOSK_LUCKY_ITEM_OFFER.offer_id = OFFER.id AND OFFER.end_time >= CURRENT_TIMESTAMP)";
        public const String OFFER_ADVANCED_DATA = ", " + REGISTERED_USERS_TODAY_ALIAS + " AS " + REGISTERED_USERS_TODAY + ", " + REGISTERED_USERS_THIS_WEEK_ALIAS + " AS " + REGISTERED_USERS_THIS_WEEK + ", " + REGISTERED_USERS_THIS_MONTH_ALIAS + " AS " + REGISTERED_USERS_THIS_MONTH + ", " + TOTAL_REGISTERED_USERS_ALIAS + " AS " + TOTAL_REGISTERED_USERS;
        public const String LANDING_PAGE_OFFERS = "SELECT " + OFFER_ALIAS + " FROM Offers OFFER WHERE OFFER.account_id = ";

        public const String LUCKY_ITEM_DEFAULT_OFFER = "SELECT " + OFFER_ALIAS + OFFER_ADVANCED_DATA + " FROM Offers OFFER JOIN Kiosks_Lucky_Items_Offers KIOSKS_LICKY_ITEM_OFFER ON KIOSKS_LICKY_ITEM_OFFER.offer_id = OFFER.id WHERE KIOSKS_LICKY_ITEM_OFFER.is_default = 1 AND KIOSKS_LICKY_ITEM_OFFER.kiosk_id = ";
        public const String LUCKY_ITEM_NON_DEFAULT_OFFERS = "SELECT " + OFFER_ALIAS + OFFER_ADVANCED_DATA + " FROM Offers OFFER WHERE OFFER.id NOT IN (SELECT KIOSKS_LUCKY_ITEM_OFFER.offer_id FROM Kiosks_Lucky_Items_Offers KIOSKS_LUCKY_ITEM_OFFER WHERE KIOSKS_LUCKY_ITEM_OFFER.is_default = 1 AND KIOSKS_LUCKY_ITEM_OFFER.kiosk_id = ";
        public const String LUCKY_ITEM_ACTIVE_OFFERS = "SELECT " + OFFER_ALIAS + OFFER_ADVANCED_DATA + " FROM Offers OFFER JOIN Kiosks_Lucky_Items_Offers KIOSKS_LICKY_ITEM_OFFER ON KIOSKS_LICKY_ITEM_OFFER.offer_id = OFFER.id WHERE ";
        public const String LUCKY_ITEM_OFFERS_HISTORY = "SELECT " + OFFER_ALIAS + OFFER_ADVANCED_DATA + " FROM Offers OFFER JOIN Kiosks_Lucky_Items_Offers KIOSKS_LICKY_ITEM_OFFER ON KIOSKS_LICKY_ITEM_OFFER.offer_id = OFFER.id WHERE ";
        public const String KIOSK_LUCKY_ITEM_OFFER = "SELECT " + OFFER_ALIAS + OFFER_ADVANCED_DATA + " FROM Offers OFFER JOIN Kiosks_Lucky_Items_Offers KIOSKS_LICKY_ITEM_OFFER ON KIOSKS_LICKY_ITEM_OFFER.offer_id = OFFER.id WHERE ";
        public const String KIOSK_LUCKY_ITEM_CURRENT_OFFER = "SELECT TOP 1 " + OFFER_ALIAS + OFFER_ADVANCED_DATA + " FROM Offers OFFER JOIN Kiosks_Lucky_Items_Offers KIOSKS_LICKY_ITEM_OFFER ON KIOSKS_LICKY_ITEM_OFFER.offer_id = OFFER.id WHERE ";
        public const String LUCKY_ITEM_INACTIVE_OFFERS = "SELECT " + OFFER_ALIAS + OFFER_ADVANCED_DATA + " FROM Offers OFFER WHERE ";

        public const String BEACON_ACTIVE_OFFERS = "SELECT " + OFFER_ALIAS + OFFER_ADVANCED_DATA + " FROM Offers OFFER JOIN Beacons_Offers BEACON_OFFER ON BEACON_OFFER.offer_id = OFFER.id WHERE BEACON_OFFER.beacon_id = ";
        public const String BEACON_OFFERS_HISTORY = "SELECT " + OFFER_ALIAS + OFFER_ADVANCED_DATA + " FROM Offers OFFER JOIN Beacons_Offers BEACON_OFFER ON BEACON_OFFER.offer_id = OFFER.id WHERE BEACON_OFFER.beacon_id = ";
        public const String BEACON_INACTIVE_OFFERS = "SELECT DISTINCT " + OFFER_ALIAS + OFFER_ADVANCED_DATA + " FROM Offers OFFER WHERE ";
        public const String BEACON_CURRENT_OFFER = "SELECT TOP 1 " + OFFER_ALIAS + " FROM Offers OFFER JOIN Beacons_Offers BEACON_OFFER ON BEACON_OFFER.offer_id = OFFER.id WHERE ";
        public const String BEACON_DEFAULT_OFFER = "SELECT " + OFFER_ALIAS + OFFER_ADVANCED_DATA + " FROM Offers OFFER JOIN Beacons_Offers BEACON_OFFER ON BEACON_OFFER.offer_id = OFFER.id WHERE BEACON_OFFER.is_default = 1 AND BEACON_OFFER.beacon_id = ";
        public const String BEACON_NON_DEFAULT_OFFER = "SELECT " + OFFER_ALIAS + OFFER_ADVANCED_DATA + " FROM Offers OFFER WHERE OFFER.id NOT IN (SELECT BEACON_OFFER.offer_id FROM Beacons_Offers BEACON_OFFER WHERE BEACON_OFFER.is_default = 1 AND BEACON_OFFER.beacon_id = ";

        //CATEGORIES ALIAS
        public const String CATEGORY_ID = "CATEGORY_ID";
        public const String CATEGORY_NAME = "CATEGORY_NAME";
        public const String CATEGORY_ACCOUNT = "CATEGORY_ACCOUNT";
        public const String CATEGORY_ALIAS = "CATEGORY.id AS " + CATEGORY_ID + ", CATEGORY.name AS " + CATEGORY_NAME;
        public const String CATEGORY_FOR_OFFER = "SELECT " + CATEGORY_ALIAS + ", OFFER_CATEGORY.value AS " + OFFER_CATEGORY_VALUE + " FROM Categories CATEGORY JOIN Offers_Categories OFFER_CATEGORY ON OFFER_CATEGORY.category_id = CATEGORY.id WHERE OFFER_CATEGORY.offer_id = ";
        

        //OFFERS_CATEGORIES ALIAS
        public const String OFFER_CATEGORY_OFFERID = "OFFER_CATEGORY_OFFERID";
        public const String OFFER_CATEGORY_CATEGORYID = "OFFER_CATEGORY_CATEGORYID";
        public const String OFFER_CATEGORY_VALUE = "OFFER_CATEGORY_VALUE";

        //KIOSKS ALIAS
        public const String KIOSK_ID = "KIOSK_ID";
        public const String KIOSK_NUMBER = "KIOSK_NUMBER";
        public const String KIOSK_NAME = "KIOSK_NAME";
        public const String KIOSK_PUSH_ID = "KIOSK_PUSH_ID";
        public const String KIOSK_STORE = "KIOSK_STORE";
        public const String KIOSK_CREATED_ON = "KIOSK_CREATED_ON";
        public const String KIOSK_UPDATED_ON = "KIOSK_UPDATED_ON";
        public const String KIOSK_CREATED_BY = "KIOSK_CREATED_BY";
        public const String KIOSK_UPDATED_BY = "KIOSK_UPDATED_BY";
        public const String KIOSK_ALIAS = "KIOSK.id AS " + KIOSK_ID + ", KIOSK.number AS " + KIOSK_NUMBER + ", KIOSK.name AS " + KIOSK_NAME + ", KIOSK.store_id AS " + KIOSK_STORE;
        public const String KIOSK_REGISTERED_USERS_TODAY = "(SELECT COUNT(USER_KIOSK.user_id) FROM users_kiosks USER_KIOSK WHERE USER_KIOSK.kiosk_id = KIOSK.id AND USER_KIOSK.is_registered = 1 AND DATEPART(dd,USER_KIOSK.date) = DATEPART(dd,CURRENT_TIMESTAMP))";
        public const String KIOSK_REGISTERED_USERS_THIS_WEEK = "(SELECT COUNT(USER_KIOSK.user_id) FROM users_kiosks USER_KIOSK WHERE USER_KIOSK.kiosk_id = KIOSK.id AND USER_KIOSK.is_registered = 1 AND DATEPART(wk,USER_KIOSK.date) = DATEPART(wk,CURRENT_TIMESTAMP))";
        public const String KIOSK_REGISTERED_USERS_THIS_MONTH = "(SELECT COUNT(USER_KIOSK.user_id) FROM users_kiosks USER_KIOSK WHERE USER_KIOSK.kiosk_id = KIOSK.id AND USER_KIOSK.is_registered = 1 AND DATEPART(mm,USER_KIOSK.date) = DATEPART(mm,CURRENT_TIMESTAMP))";
        public const String KIOSK_TOTAL_REGISTERED_USERS = "(SELECT COUNT(USER_KIOSK.user_id) FROM users_kiosks USER_KIOSK WHERE USER_KIOSK.kiosk_id = KIOSK.id AND USER_KIOSK.is_registered = 1)";
        public const String ALL_KIOSKS_BY_ACCOUNT = "SELECT " + KIOSK_ALIAS + ", " + STORE_KIOSK_ALIAS + ", " + KIOSK_REGISTERED_USERS_TODAY + " AS " + REGISTERED_USERS_TODAY + ", " + KIOSK_REGISTERED_USERS_THIS_WEEK + " AS " + REGISTERED_USERS_THIS_WEEK + ", " + KIOSK_REGISTERED_USERS_THIS_MONTH + " AS " +  REGISTERED_USERS_THIS_MONTH + ", " + KIOSK_TOTAL_REGISTERED_USERS + " AS " + TOTAL_REGISTERED_USERS + " FROM Kiosks KIOSK INNER JOIN Stores STORE ON KIOSK.store_id = STORE.id where STORE.account_id = ";
        public const String LUCKY = "SELECT KIOSK.name AS " + KIOSK_NAME + ", STORE.name AS " + STORE_NAME + ", COUNTRY.name AS " + COUNTRY_NAME + ", COUNTRY.id AS " + COUNTRY_ID + ", " + LUCKY_ITEM_ALIAS + " FROM Kiosks KIOSK INNER JOIN Stores STORE ON STORE.id = KIOSK.store_id INNER JOIN Countries COUNTRY ON COUNTRY.id = STORE.country_id INNER JOIN Lucky_Items LUCKY_ITEM ON LUCKY_ITEM.id = ";
        public const String LUCKY2 = " WHERE KIOSK.id = ";

        //LUCKY_ITEMS ALIAS
        public const String LUCKY_ITEM_ID = "LUCKY_ITEM_ID";
        public const String LUCKY_ITEM_NAME = "LUCKY_ITEM_NAME";
        public const String LUCKY_ITEM_ICON = "LUCKY_ITEM_ICON";
        public const String LUCKY_ITEM_ACCOUNT = "LUCKY_ITEM_ACCOUNT";
        public const String LUCKY_ITEM_ALIAS = "LUCKY_ITEM.id AS " + LUCKY_ITEM_ID + ", LUCKY_ITEM.name AS " + LUCKY_ITEM_NAME + ", LUCKY_ITEM.icon AS " + LUCKY_ITEM_ICON + ", LUCKY_ITEM.account_id AS " + LUCKY_ITEM_ACCOUNT;
        public const String All_LUCKY_ITEMS_BY_KIOSK = "SELECT KIOSK.name AS KIOSK_NAME, " + LUCKY_ITEM_ALIAS + ", KIOSK_LUCKY_ITEM.short_url AS " + KIOSK_LUCKY_ITEM_SHORT_URL + " FROM Lucky_Items LUCKY_ITEM JOIN Kiosks_lucky_items KIOSK_LUCKY_ITEM ON KIOSK_LUCKY_ITEM.lucky_item_id = LUCKY_ITEM.id INNER JOIN Kiosks KIOSK ON KIOSK.id = KIOSK_LUCKY_ITEM.kiosk_id WHERE KIOSK_LUCKY_ITEM.kiosk_id = ";
        public const String All_LUCKY_ITEMS_BY_ACCOUNT = "SELECT " + LUCKY_ITEM_ALIAS  + ", STORE.id AS STORE_ID, STORE.account_id AS STORE_ACCOUNT_ID FROM Lucky_Items LUCKY_ITEM JOIN Stores STORE ON STORE.id = ";

        //KIOSKS_LUCKY_ITEMS ALIAS
        public const String KIOSK_LUCKY_ITEM_KIOSKID = "KIOSK_LUCKY_ITEM_KIOSKID";
        public const String KIOSK_LUCKY_ITEM_LUCKYITEMID = "KIOSK_LUCKY_ITEM_LUCKYITEMID";
        public const String KIOSK_LUCKY_ITEM_SHORT_URL = "KIOSK_LUCKY_ITEM_SHORT_URL";
        public const String KIOSK_LUCKY_ITEM_DETAILS1 = "SELECT LUCKY_ITEM.id AS " + LUCKY_ITEM_ID + ", LUCKY_ITEM.name AS " + LUCKY_ITEM_NAME + ", LUCKY_ITEM.icon AS " + LUCKY_ITEM_ICON + ", KIOSK_LUCKY_ITEM.short_url AS " + KIOSK_LUCKY_ITEM_SHORT_URL + ", KIOSK.id AS " + KIOSK_ID + ", KIOSK.name AS " + KIOSK_NAME + ", STORE.id AS " + STORE_ID + ", STORE.name AS " + STORE_NAME + ", COUNTRY.id AS " + COUNTRY_ID + ", COUNTRY.name AS " + COUNTRY_NAME + " FROM Lucky_Items LUCKY_ITEM JOIN Kiosks KIOSK ON KIOSK.id = ";
        public const String KIOSK_LUCKY_ITEM_DETAILS2 = " JOIN Kiosks_lucky_items KIOSK_LUCKY_ITEM ON KIOSK_LUCKY_ITEM.kiosk_id = KIOSK.id AND KIOSK_LUCKY_ITEM.lucky_item_id = LUCKY_ITEM.id JOIN Stores STORE ON STORE.id = KIOSK.store_id JOIN Countries COUNTRY ON COUNTRY.id = STORE.country_id WHERE LUCKY_ITEM.id = ";

        //KIOSKS_LUCKY_ITEMS_OFFERS ALIAS
        public const String KIOSK_LUCKY_ITEM_OFFER_KIOSKID = "KIOSK_LUCKY_ITEM_OFFER_KIOSKID";
        public const String KIOSK_LUCKY_ITEM_OFFER_LUCKYITEMID = "KIOSK_LUCKY_ITEM_OFFER_LUCKYITEMID";
        public const String KIOSK_LUCKY_ITEM_OFFER_OFFERID = "KIOSK_LUCKY_ITEM_OFFER_OFFERID";
        public const String KIOSK_LUCKY_ITEM_OFFER_SHORTURL = "KIOSK_LUCKY_ITEM_OFFER_SHORTURL";
        public const String KIOSK_LUCKY_ITEM_OFFER_CREATEDON = "KIOSK_LUCKY_ITEM_OFFER_CREATEDON";
        public const String KIOSK_LUCKY_ITEM_OFFER_UPDATEDON = "KIOSK_LUCKY_ITEM_OFFER_UPDATEDON";
        public const String KIOSK_LUCKY_ITEM_OFFER_CREATEDBY = "KIOSK_LUCKY_ITEM_OFFER_CREATEDBY";
        public const String KIOSK_LUCKY_ITEM_OFFER_UPDATEDBY = "KIOSK_LUCKY_ITEM_OFFER_UPDATEDBY";
        public const String KIOSKS_LUCKY_ITEMS_OFFERS_ALIAS = "KIOSK_LUCKY_ITEM.kiosk_id AS " + KIOSK_LUCKY_ITEM_OFFER_KIOSKID + ", KIOSK_LUCKY_ITEM.lucky_item_id AS " + KIOSK_LUCKY_ITEM_OFFER_LUCKYITEMID;
        
        //BEACONS ALIAS
        public const String BEACON_ID = "BEACON_ID";
        public const String BEACON_NUMBER = "BEACON_NUMBER";
        public const String BEACON_NAME = "BEACON_NAME";
        public const String BEACON_GUID = "BEACON_GUID";
        public const String BEACON_STORE = "BEACON_STORE";
        public const String BEACON_SHORT_URL = "BEACON_SHORT_URL";
        public const String BEACON_CREATED_ON = "BEACON_CREATED_ON";
        public const String BEACON_UPDATED_ON = "BEACON_UPDATED_ON";
        public const String BEACON_CREATED_BY = "BEACON_CREATED_BY";
        public const String BEACON_UPDATED_BY = "BEACON_UPDATED_BY";
        public const String BEACON_REGISTERED_USERS_TODAY = "(SELECT COUNT(USER_BEACON.user_id) FROM users_beacons USER_BEACON WHERE USER_BEACON.beacon_id = BEACON.id AND USER_BEACON.is_registered = 1 AND DATEPART(dd,USER_BEACON.date) = DATEPART(dd,CURRENT_TIMESTAMP))";
        public const String BEACON_REGISTERED_USERS_THIS_WEEK = "(SELECT COUNT(USER_BEACON.user_id) FROM users_beacons USER_BEACON WHERE USER_BEACON.beacon_id = BEACON.id AND USER_BEACON.is_registered = 1 AND DATEPART(wk,USER_BEACON.date) = DATEPART(wk,CURRENT_TIMESTAMP))";
        public const String BEACON_REGISTERED_USERS_THIS_MONTH = "(SELECT COUNT(USER_BEACON.user_id) FROM users_beacons USER_BEACON WHERE USER_BEACON.beacon_id = BEACON.id AND USER_BEACON.is_registered = 1 AND DATEPART(mm,USER_BEACON.date) = DATEPART(mm,CURRENT_TIMESTAMP))";
        public const String BEACON_TOTAL_REGISTERED_USERS = "(SELECT COUNT(USER_BEACON.user_id) FROM users_beacons USER_BEACON WHERE USER_BEACON.beacon_id = BEACON.id AND USER_BEACON.is_registered = 1)";
        public const String BEACON_ADVANCE_DATA = BEACON_REGISTERED_USERS_TODAY + " AS " + REGISTERED_USERS_TODAY + ", " + BEACON_REGISTERED_USERS_THIS_WEEK + " AS " + REGISTERED_USERS_THIS_WEEK + ", " + BEACON_REGISTERED_USERS_THIS_MONTH + " AS " + REGISTERED_USERS_THIS_MONTH + ", " + BEACON_TOTAL_REGISTERED_USERS + " AS " + TOTAL_REGISTERED_USERS;
        public const String BEACON_ALIAS = "BEACON.id AS " + BEACON_ID + ", BEACON.number AS " + BEACON_NUMBER + ", BEACON.name AS " + BEACON_NAME + ", BEACON.store_id AS " + BEACON_STORE ;
        public const String ALL_BEACONS_BY_ACCOUNT = "SELECT " + BEACON_ALIAS + ", " + STORE_BEACON_ALIAS + ", " + BEACON_ADVANCE_DATA + " FROM Beacons BEACON INNER JOIN Stores STORE ON BEACON.store_id = STORE.id where STORE.account_id = ";
        public const String BEACON_DETAILS = "SELECT BEACON.id AS " + BEACON_ID + ", BEACON.name AS " + BEACON_NAME + ", BEACON.short_url AS " + BEACON_SHORT_URL + ", STORE.id AS " + STORE_ID + ", STORE.name AS " + STORE_NAME + ", COUNTRY.id AS " + COUNTRY_ID + ", COUNTRY.name AS " + COUNTRY_NAME + " FROM Beacons BEACON INNER JOIN Stores STORE ON STORE.id = BEACON.store_id INNER JOIN Countries COUNTRY ON COUNTRY.id = STORE.country_id WHERE BEACON.id = ";
        public const String BEACON_ID_FROM_GUID = "SELECT BEACON.id AS " + BEACON_ID + " FROM Beacons BEACON WHERE BEACON.beacon_unique_id = ";


        //BEACONS_OFFERS ALIAS
        public const String BEACON_OFFER_BEACONID = "BEACON_OFFER_BEACONID";
        public const String BEACON_OFFER_OFFERID = "BEACON_OFFER_OFFERID";
        public const String BEACON_OFFER_SHORT_URL = "BEACON_OFFER_SHORT_URL";
        public const String BEACON_OFFER_CREATED_ON = "BEACON_OFFER_CREATED_ON";
        public const String BEACON_OFFER_UPDATED_ON = "BEACON_OFFER_UPDATED_ON";
        public const String BEACON_OFFER_CREATED_BY = "BEACON_OFFER_CREATED_BY";
        public const String BEACON_OFFER_UPDATED_BY = "BEACON_OFFER_UPDATED_BY";

        //USERS ALIAS
        public const String USER_ID = "USER_ID";
        public const String USER_CONTACT = "USER_CONTACT";
        public const String USER_ACCOUNT = "USER_ACCOUNT";
        public const String USER_CREATED_ON = "USER_CREATED_ON";
        public const String USER_UPDATED_ON = "USER_UPDATED_ON";
        public const String USER_BEACON = "USER_BEACON";
        public const String USER_KIOSK = "USER_KIOSK";
        public const String USER_MEMBER_ID = "USER_MEMBER_ID";
        public const String USER_LATEST_OFFER = "USER_LATEST_OFFER";
        public const String USER_LAST_ACTIVITY = "USER_LAST_ACTIVITY";
        public const String USER_ALIAS = "USR.id AS " + USER_ID + ", USR.account_id AS " + USER_ACCOUNT + ", USR.created_on AS " + USER_CREATED_ON;
        public const String USER_DETAILS = "SELECT USR.id AS " + USER_ID + " FROM users USR WHERE USR.member_id = @memberId AND USR.account_id = @accountId";
        public const String USER_DETAILS1 = "SELECT USR.id AS " + USER_ID + ", USR.account_id AS " + ACCOUNT_ID + " FROM users USR Where username = @username AND password = @password;";
        public const String REGISTERED_USERS_FROM_BEACON = "SELECT " + USER_ALIAS + ", " + STORE_ALIAS_MINIMAL + ", " + USER_BEACON_ALIAS + ", " + OFFER_ALIAS3 + ", " + USER_LATEST_OFFER_ALIAS + " AS " + USER_LATEST_OFFER + ", " + USER_LAST_ACTIVITY_ALIAS + " AS " + USER_LAST_ACTIVITY + " FROM users USR JOIN users_beacons USER_BEACON ON USER_BEACON.user_id = USR.id JOIN Beacons BEACON ON BEACON.id = USER_BEACON.beacon_id JOIN Stores STORE ON STORE.id = BEACON.store_id AND USER_BEACON.beacon_id = BEACON_ID JOIN Offers OFFER ON OFFER.id = USER_BEACON.offer_id WHERE USER_BEACON.is_registered = 1 AND USR.account_id = ";
        public const String REGISTERED_USERS_FROM_KIOSK = "SELECT " + USER_ALIAS + ", " + STORE_ALIAS_MINIMAL + ", " + USER_KIOSK_ALIAS + ", " + OFFER_ALIAS3 + ", " + USER_LATEST_OFFER_ALIAS + " AS " + USER_LATEST_OFFER + ", " + USER_LAST_ACTIVITY_ALIAS + " AS " + USER_LAST_ACTIVITY + " FROM users USR JOIN users_kiosks USER_KIOSK ON USER_KIOSK.user_id = USR.id JOIN kiosks KIOSK ON KIOSK.id = USER_KIOSK.kiosk_id JOIN Stores STORE ON STORE.id = KIOSK.store_id AND USER_KIOSK.kiosk_id = KIOSK_ID JOIN Offers OFFER ON OFFER.id = USER_KIOSK.offer_id WHERE USER_KIOSK.is_registered = 1 AND USR.account_id = ";
        public const String USER_BEACON_ALIAS = "BEACON.id AS DEVICE_ID, '1' AS DEVICE_TYPE, BEACON.number AS DEVICE_NUMBER, BEACON.name AS DEVICE_NAME";
        public const String USER_KIOSK_ALIAS = "KIOSK.id AS DEVICE_ID, '2' AS DEVICE_TYPE, KIOSK.number AS DEVICE_NUMBER, KIOSK.name AS DEVICE_NAME";
        public const String USER_LATEST_OFFER_ALIAS = "(SELECT TOP 1 LATEST_OFFER_ID FROM (SELECT LATEST_OFFER.id AS LATEST_OFFER_ID, USER_KIOSK.date AS LATEST_OFFER_DATE FROM Offers LATEST_OFFER JOIN users_kiosks USER_KIOSK ON USER_KIOSK.user_id = USR.id WHERE LATEST_OFFER.id = USER_KIOSK.offer_id UNION SELECT LATEST_OFFER.id AS LATEST_OFFER_ID, USER_BEACON.date AS LATEST_OFFER_DATE FROM Offers LATEST_OFFER JOIN users_beacons USER_BEACON ON USER_BEACON.user_id = USR.id WHERE LATEST_OFFER.id = USER_BEACON.offer_id) AS LATEST_OFFER ORDER BY LATEST_OFFER_DATE DESC)";
        public const String USER_LAST_ACTIVITY_ALIAS = "(SELECT TOP 1 LATEST_OFFER_DATE FROM (SELECT LATEST_OFFER.id AS LATEST_OFFER_ID, USER_KIOSK.date AS LATEST_OFFER_DATE FROM Offers LATEST_OFFER JOIN users_kiosks USER_KIOSK ON USER_KIOSK.user_id = USR.id WHERE LATEST_OFFER.id = USER_KIOSK.offer_id UNION SELECT LATEST_OFFER.id AS LATEST_OFFER_ID, USER_BEACON.date AS LATEST_OFFER_DATE FROM Offers LATEST_OFFER JOIN users_beacons USER_BEACON ON USER_BEACON.user_id = USR.id WHERE LATEST_OFFER.id = USER_BEACON.offer_id) AS LATEST_OFFER ORDER BY LATEST_OFFER_DATE DESC)";

        //CONTACTS ALIAS
        public const String CONTACT_ID = "CONTACT_ID";
        public const String CONTACT_COUNTRY = "CONTACT_COUNTRY";
        public const String CONTACT_TYPE = "CONTACT_TYPE";
        public const String CONTACT_VALUE = "CONTACT_VALUE";
        public const String CONTACT_CREATED_ON = "CONTACT_CREATED_ON";
        public const String CONTACT_UPDATED_ON = "CONTACT_UPDATED_ON";
        public const String CONTACT_ALIAS = "CONTACT.id AS " + CONTACT_ID + ", CONTACT.type AS " + CONTACT_TYPE + ", CONTACT.value AS " + CONTACT_VALUE;
        public const String CONTACTS_FOR_USER = "SELECT " + CONTACT_ALIAS + ", " + COUNTRY_ALIAS1 + " FROM contacts CONTACT JOIN users_contacts USER_CONTACT ON USER_CONTACT.contact_id = CONTACT.id JOIN Countries COUNTRY ON COUNTRY.id = CONTACT.country_id WHERE USER_CONTACT.user_id = ";

        //ACCOUNT ALIAS
        public const String ACCOUNT_ID = "ACCOUNT_ID";
        public const String ACCOUNT_NAME = "ACCOUNT_NAME";
        public const String ACCOUNT_USER_NAME = "ACCOUNT_USER_NAME";
        public const String ACCOUNT_PASSWORD = "ACCOUNT_PASSWORD";
        public const String ACCOUNT_ADMIN = "ACCOUNT_ADMIN";
        public const String ACCOUNT_PIC_URL = "ACCOUNT_PIC_URL";
        public const String ACCOUNT_WEBSITE = "ACCOUNT_WEBSITE";
        public const String ACCOUNT_CREATED_ON = "ACCOUNT_CREATED_ON";
        public const String ACCOUNT_UPDATED_ON = "ACCOUNT_UPDATED_ON";
        public const String ACCOUNT_ALIAS = "ACCOUNT.id AS " + ACCOUNT_ID + ", ACCOUNT.name AS " + ACCOUNT_NAME + ", ACCOUNT.pic_url AS " + ACCOUNT_PIC_URL + ", ACCOUNT.website AS " + ACCOUNT_WEBSITE + ", ACCOUNT.created_on AS " + ACCOUNT_CREATED_ON + ", ACCOUNT.updated_on AS " + ACCOUNT_UPDATED_ON;
        public const String ACCOUNT_DETAILS = "SELECT " + ACCOUNT_ALIAS + " FROM Accounts ACCOUNT WHERE ACCOUNT.id = ";
    }
}