﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.Accounts;
using BNS.Models.Beacons;
using BNS.DL.WSDatabase;
using System.Data.SqlClient;
using BNS.Models.Stores;
using BNS.Models.Countries;
using WSDatabase;

namespace BNS.DAL.DALBeacon
{
    public class DALBeaconManager
    {       
        public bool saveBeacon(Beacon beacon)
        {
            using (WSTransaction transaction = new WSTransaction("SAVE_BEACON"))
            {
                String query1 = "INSERT INTO Beacons(beacon_unique_id, name, number, store_id, created_on, updated_on, created_by, updated_by) OUTPUT Inserted.id  VALUES(@beaconGuid, @name, @number, @storeId, @createdOn, @updatedOn, @createdBy, @updatedBy);";
                WSQuery Query = new WSQuery(query1, beacon.toDictionary(), transaction);
                beacon.id = WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQueryReturnID(Query);
                if (beacon.id != 0)
                {
                    String beaconUrl = this.getUrl(beacon.beacon_unique_id);
                    beacon.shortUrl = UrlShortener.shortenIt(beaconUrl);
                    if (beacon.shortUrl != null)
                    {
                        if (!this.updateBeaconShortUrl(beacon, transaction))
                        {
                            transaction.rollback();
                            return false;
                        }
                    }
                    if (!this.saveBeaconDefaultOffer(beacon, transaction))
                    {
                        transaction.rollback();
                        return false;
                    }
                }
                else
                {
                    transaction.rollback();
                    return false;
                }
                transaction.commit();
                return true;
            }        
        }

        public String getUrl(String beaconGuid)
        {
            UriBuilder uriBuilder = new UriBuilder();
            uriBuilder.Scheme = "http";
            uriBuilder.Host = "localhost";
            uriBuilder.Fragment = @"/beaconCurrentOffer?bid=" + beaconGuid;
            uriBuilder.Path = @"index.html";
            uriBuilder.Port = 32515;            
            Uri uri = uriBuilder.Uri;
            return uri.ToString();
        }

        public Int64 getBeaconIdFromGuid(String beaconGuid)
        {
            String query = QueryConstants.BEACON_ID_FROM_GUID + "'" + beaconGuid + "'";
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconGuid"] = beaconGuid;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            Int64 beaconId = 0;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if (WSDatabaseHelper.isValidField(reader, "BEACON_ID"))
                    {
                        beaconId = reader.GetInt64(reader.GetOrdinal("BEACON_ID"));
                    }
                }
            }
            Query.connection.Close();
            return beaconId;
        }

        public bool updateBeaconShortUrl(Beacon beacon, WSTransaction transaction)
        {
            String query = "UPDATE Beacons SET short_url = '" + beacon.shortUrl + "' WHERE id = " + beacon.id.ToString();
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beacon.id;
            Query.parameters["beaconUrl"] = beacon.shortUrl;
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeUpdateQuery(Query);
        }

        public bool saveBeaconDefaultOffer(Beacon beacon, WSTransaction transaction)
        {
            String query = "INSERT INTO Beacons_Offers(beacon_id, offer_id, is_default, created_on, updated_on, created_by, updated_by) VALUES(@beaconId, @offerId, @isDefault, @createdOn, @updatedOn, @createdBy, @updatedBy);";
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beacon.id;
            Query.parameters["offerId"] = 4;
            Query.parameters["isDefault"] = 1;
            Query.parameters["createdOn"] = DateTime.Now.ToString();
            Query.parameters["updatedOn"] = DateTime.Now.ToString();
            Query.parameters["createdBy"] = beacon.createdBy.id;
            Query.parameters["updatedBy"] = beacon.updatedBy.id;
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);
        }

        public List<Beacon> getAll(Account account)
        {
            
            String query = QueryConstants.ALL_BEACONS_BY_ACCOUNT + account.id.ToString() + " ORDER BY BEACON.updated_on DESC";
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = account.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Beacon> beaconsList = new List<Beacon>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    beaconsList.Add(new Beacon(reader));
                }               
            }
            Query.connection.Close();
            return beaconsList;
        }

        public bool checkIfNameExists(String beaconName, Int64 storeId)
        {          
            String query = "SELECT CASE WHEN EXISTS (Select * from Beacons where name = @beaconName and store_id = @storeId) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconName"] = beaconName;
            Query.parameters["storeId"] = storeId;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }

        public bool checkIfNumberExists(String beaconNumber)
        {           
            String query = "SELECT CASE WHEN EXISTS (Select * from Beacons where number = @beaconNumber) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconNumber"] = beaconNumber;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }

        public Beacon getDetails(Beacon beacon)
        {
            String query = QueryConstants.BEACON_DETAILS + beacon.id.ToString();
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beacon.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    beacon = new Beacon(reader);
                    beacon.store = new Store(reader);
                    beacon.store.country = new Country(reader);
                }             
            }
            Query.connection.Close();
            return beacon;
        }

        
    }
}