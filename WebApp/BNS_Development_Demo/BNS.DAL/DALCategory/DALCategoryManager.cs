﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.Categories;
using BNS.DL.WSDatabase;
using System.Data.SqlClient;
using BNS.Models.Accounts;
using WSDatabase;

namespace BNS.DAL.DALCategory
{
    public class DALCategoryManager
    {
        public bool setCategoriesForNewAccount(Account account, WSTransaction transaction)
        {
            bool result = false;
            List<Category> categories = new List<Category>();
            Category category1 = new Category("Cash", account.id);
            Category category2 = new Category("Points", account.id);
            Category category3 = new Category("Promotion", account.id);
            categories.Add(category1);
            categories.Add(category2);
            categories.Add(category3);
            foreach (Category category in categories)
            {
                String query = "INSERT INTO Categories(name, account_id) VALUES(@name, @accountId)";
                WSQuery Query = new WSQuery(query);
                Query.parameters["name"] = category.name;
                Query.parameters["accountId"] = account.id;
                Query.transaction = transaction;
                result = WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);
            }
            return result;
        }

        public List<Category> getAll(Account account)
        {
            WSQuery Query = new WSQuery();
            Query.query = "SELECT CATEGORY.id as CATEGORY_ID, CATEGORY.name as CATEGORY_NAME, CATEGORY.account_id as CATEGORY_ACCOUNT_ID from Categories CATEGORY WHERE CATEGORY.account_id = " + account.id.ToString();
            Query.parameters["accountId"] = account.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);

            List<Category> CategoriesList = new List<Category>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    CategoriesList.Add(new Category(reader));
                }            
            }
            Query.connection.Close();
            return CategoriesList;
        }       
    }
}