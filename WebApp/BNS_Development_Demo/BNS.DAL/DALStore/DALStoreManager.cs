﻿using BNS.DL.WSDatabase;
using BNS.Models.Stores;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using BNS.Models.BNSModels;
using BNS.Models.Accounts;
using WSDatabase;

namespace BNS.DAL.DALStore
{
    public class DALStoreManager
    {      
        public Int64 save(Store store)
        {
            using (WSTransaction transaction = new WSTransaction("SAVE_STORE"))
            {
                String query = "INSERT INTO Stores(name, number, country_id, account_id, created_on, updated_on, created_by, updated_by) OUTPUT Inserted.id  VALUES(@name, @number, @countryId, @accountId, @createdOn, @updatedOn, @createdBy, @updatedBy);";
                WSQuery Query = new WSQuery(query, store.toDictionary(),transaction);
                store.id = WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQueryReturnID(Query);
                if(store.id != 0 )
                {
                    transaction.commit();
                    return store.id;
                }
                else
                {
                    transaction.rollback();
                    return 0;
                }
            }
        }

        public List<Store> getAll(Account account)
        {
            String query = QueryConstants.ALL_STORES_BY_ACCOUNT + " STORE.account_id = " + account.id.ToString() + " ORDER BY STORE.updated_on DESC";
            WSQuery Query = new WSQuery(query);           
            Query.parameters["accountId"] = account.id;                    
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Store> storesList = new List<Store>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    storesList.Add(new Store(reader));
                }               
            }
            Query.connection.Close();
            return storesList;
        }

        public List<Store> getStoresForDropDown(Account account)
        {
            String query = QueryConstants.ALL_STORES_FOR_DROPDOWN_BY_ACCOUNT + " STORE.account_id = " + account.id.ToString() + " ORDER BY STORE.name ASC";
            WSQuery Query = new WSQuery(query);         
            Query.parameters["accountId"] = account.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Store> storesList = new List<Store>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    storesList.Add(new Store(reader));
                }
                Query.connection.Close();
            }
            return storesList;
        }

        public bool checkIfNameExists(String storeName, Int64 account)
        {
            String query = "SELECT CASE WHEN EXISTS (Select * from Stores where name = @storeName and account_id = @accountId) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["storeName"] = storeName;
            Query.parameters["accountId"] = account;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }

        public bool checkIfNumberExists(String storeNumber, Int64 account)
        {
            String query = "SELECT CASE WHEN EXISTS (Select * from Stores where number = @storeNumber and account_id = @accountId) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["storeNumber"] = storeNumber;
            Query.parameters["accountId"] = account;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }
    }
}