﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.DL.WSDatabase;
using System.Data.SqlClient;
using BNS.Models.Kiosks;
using BNS.Models.Accounts;
using BNS.Models.LuckyItems;
using BNS.DAL.DALLuckyItem;
using BNS.DAL;
using WSDatabase;

namespace BNS.DAL.DALKiosk
{
    public class DALKioskManager
    {
        public Int64 saveKiosk(Kiosk kiosk)
        {
            using (WSTransaction transaction = new WSTransaction("SAVE_KIOSK"))
            {
                String query = "INSERT INTO Kiosks(name, number, store_id, created_on, updated_on, created_by, updated_by) OUTPUT Inserted.id  VALUES(@name, @number, @storeId, @createdOn, @updatedOn, @createdBy, @updatedBy);";
                WSQuery Query = new WSQuery(query, kiosk.toDictionary(), transaction);
                kiosk.id = WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQueryReturnID(Query);

                kiosk.luckyItems = this.getAllLuckeyItemForKiosk(kiosk);
                if (kiosk.luckyItems.Count != 0)
                {
                    foreach (LuckyItem item in kiosk.luckyItems)
                    {
                        String luckyItemUrl = getUrl(kiosk.id, item.id);
                        item.shortUrl = UrlShortener.shortenIt(luckyItemUrl);
                        bool is_inserted = this.setLuckyItemsToKiosk(kiosk, item, transaction);
                        if (is_inserted)
                        {
                            bool is_offer_inserted = this.setDefaultOfferToKioskLuckyItem(kiosk, item, transaction);
                            if (!is_offer_inserted)
                            {
                                transaction.rollback();
                                return kiosk.id = 0;
                            }
                        }
                        else
                        {
                            transaction.rollback();
                            return kiosk.id = 0;
                        }
                    }
                    transaction.commit();
                }
                else
                {
                    transaction.rollback();
                    kiosk.id = 0;
                }
                return kiosk.id;
            }
        }

        private List<LuckyItem> getAllLuckeyItemForKiosk(Kiosk kiosk)
        {
            WSQuery Query = new WSQuery();
            Query.query = QueryConstants.All_LUCKY_ITEMS_BY_ACCOUNT + kiosk.store.id.ToString() + " AND STORE.account_id = LUCKY_ITEM.account_id;";
            Query.parameters["storeId"] = kiosk.store.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<LuckyItem> luckyItems = new List<LuckyItem>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    luckyItems.Add(new LuckyItem(reader));
                }              
            }
            Query.connection.Close();
            return luckyItems;
        }

        public String getUrl(Int64 kioskId, Int64 luckyItemId)
        {
            UriBuilder uriBuilder = new UriBuilder();
            uriBuilder.Scheme = "http";
            uriBuilder.Host = "localhost";
            uriBuilder.Fragment = @"/luckyItemCurrentOffer?kioskId=" + kioskId.ToString() + "&luckyItemId=" + luckyItemId.ToString();
            uriBuilder.Path = @"index.html";
            uriBuilder.Port = 32515;
            //uriBuilder.Query = "kioskId=" + kioskId.ToString() + "&luckyItemId=" + luckyItemId.ToString();
            Uri uri = uriBuilder.Uri;
            return uri.ToString();
        }
   

        public bool setLuckyItemsToKiosk(Kiosk kiosk, LuckyItem luckyItem, WSTransaction transaction)
        {
            //String query = "INSERT INTO Kiosks_lucky_items(kiosk_id, lucky_item_id, created_on, updated_on, created_by, updated_by) VALUES(@kioskId, @luckyItemId, @createdOn, @updatedOn, @createdBy, @updatedBy);";
            String query = "INSERT INTO Kiosks_lucky_items(kiosk_id, lucky_item_id, short_url, created_on, updated_on, created_by, updated_by) VALUES(@kioskId, @luckyItemId, @shortUrl, @createdOn, @updatedOn, @createdBy, @updatedBy);";
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = kiosk.id;
            Query.parameters["luckyItemId"] = luckyItem.id;
            Query.parameters["luckyItemId"] = luckyItem.id;
            Query.parameters["shortUrl"] = luckyItem.shortUrl;
            Query.parameters["createdOn"] = DateTime.Now.ToString();
            Query.parameters["updatedOn"] = DateTime.Now.ToString();
            Query.parameters["createdBy"] = kiosk.createdBy.id;
            Query.parameters["updatedBy"] = kiosk.updatedBy.id;
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);
        }

        public bool setDefaultOfferToKioskLuckyItem(Kiosk kiosk, LuckyItem luckyItem, WSTransaction transaction)
        {
            String query = "INSERT INTO Kiosks_Lucky_Items_Offers(kiosk_id, lucky_item_id, offer_id, is_default, created_on, updated_on, created_by, updated_by) VALUES(@kioskId, @luckyItemId, @offerId, @isDefault, @createdOn, @updatedOn, @createdBy, @updatedBy);";
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = kiosk.id;
            Query.parameters["luckyItemId"] = luckyItem.id;
            if (luckyItem.id % 2 == 0)
            {
                Query.parameters["offerId"] = 10046;
            }
            else
            {
                Query.parameters["offerId"] = 10040;
            }
            Query.parameters["isDefault"] = 1;
            Query.parameters["createdOn"] = DateTime.Now.ToString();
            Query.parameters["updatedOn"] = DateTime.Now.ToString();
            Query.parameters["createdBy"] = kiosk.createdBy.id;
            Query.parameters["updatedBy"] = kiosk.updatedBy.id;
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);
        }

        public List<Kiosk> getAll(Account account)
        {
            String query = QueryConstants.ALL_KIOSKS_BY_ACCOUNT + account.id.ToString() + " ORDER BY KIOSK.updated_on DESC";
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = account.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Kiosk> kiosksList = new List<Kiosk>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    kiosksList.Add(new Kiosk(reader));
                }              
            }
            Query.connection.Close();
            return kiosksList;
        }

        public bool checkIfNameExists(String kioskName, Int64 storeId)
        {                                               
            String query = "SELECT CASE WHEN EXISTS (Select * from Kiosks where name = @kioskName and store_id = @storeId) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskName"] = kioskName;
            Query.parameters["storeId"] = storeId;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }

        public bool checkIfNumberExists(String kioskNumber)
        {
            String query = "SELECT CASE WHEN EXISTS (Select * from Kiosks where number = @kioskNumber) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskNumber"] = kioskNumber;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }
    }
}