﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.DL.WSDatabase;
using System.Data.SqlClient;
using BNS.Models.Contacts;
using WSDatabase;

namespace BNS.DAL.DALContact
{
    public class DALContactManager
    {
        public bool checkIfEmailIdExists(String emailId, Int64 account)
        {
            String query = "Select CONTACT.id , CONTACT.value, NewUSER.id, NewUSER.contact_id, NewUSER.account_id from contacts CONTACT JOIN users NewUSER ON NewUSER.contact_id = CONTACT.id where CONTACT.value like '" + emailId + "' AND NewUSER.account_id = " + account.ToString();
            WSQuery Query = new WSQuery(query);
            Query.parameters["emailId"] = emailId;
            Query.parameters["accountId"] = account;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            return reader.HasRows;           
        }

        public Int64 saveContact(Contact contact, WSTransaction transaction)
        {
            String query = "INSERT INTO contacts(country_id, type, value, created_on, updated_on) OUTPUT Inserted.id  VALUES(@countryId, @type, @value, @createdOn, @updatedOn);";
            WSQuery Query = new WSQuery(query,contact.toDictionary());
            Query.transaction = transaction;
            contact.id = WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQueryReturnID(Query);           
            return contact.id;          
        }
    }
}