﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using BNS.Models.Users;
using BNS.Models.Contacts;
using BNS.DL.WSDatabase;
using BNS.DAL.DALContact;
using BNS.DAL.DALOffer;
using BNS.Models.Accounts;
using BNS.Models.Offers;
using BNS.DAL.DALAccount;
using System.Net;
using System.Net.Mail;
using WSDatabase;
using System.IO;
using BNS.Models.Categories;
using System.Dynamic;
using RazorEngine;
using BNS.DAL.Constants;

namespace BNS.DAL.DALUser
{
    public class DALUserManager
    {
        
        DALOfferManager offerManager = new DALOfferManager();
        DALAccountManager accountManager = new DALAccountManager();

        public List<User> getAll(Account account)
        {
            String query = QueryConstants.REGISTERED_USERS_FROM_BEACON + account.id.ToString() + " UNION " + QueryConstants.REGISTERED_USERS_FROM_KIOSK + account.id.ToString() + " ORDER BY USR.created_on DESC";
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = account.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<User> usersList = new List<User>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    usersList.Add(new User(reader));
                }                              
            }
            Query.connection.Close();
            foreach (User user in usersList)
            {
                user.contacts = getCantactsForUser(user.id);
                user.latestOffer = offerManager.getUserLatestOfferDetails(user.latestOffer);
            }
            return usersList;
        }

        public List<Contact> getCantactsForUser(Int64 userId)
        {
            String query = QueryConstants.CONTACTS_FOR_USER + userId.ToString();
            WSQuery Query = new WSQuery(query);
            Query.parameters["userId"] = userId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Contact> contactsList = new List<Contact>();
            if(reader.HasRows)
            {
                while(reader.Read())
                {
                    contactsList.Add(new Contact(reader));
                }               
            }
            Query.connection.Close();
            return contactsList;
        }

        public Int64 creatUser(Account account , WSTransaction transaction)
        {
            account.admin.memberId = getAlphaNumericCode(4);
            String query = "INSERT INTO users(account_id, member_id, username, password, created_on, updated_on) OUTPUT Inserted.id VALUES(@accountId, @memberId, @userName, @password, @createdOn, @updatedOn)";
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] =account.id;
            Query.parameters["memberId"] =account.admin.memberId;
            Query.parameters["userName"] =account.admin.username;
            Query.parameters["password"] =account.admin.password;
            Query.parameters["createdOn"] = DateTime.Now.ToString();
            Query.parameters["updatedOn"] = DateTime.Now.ToString();
            Query.transaction = transaction;
            account.admin.id = WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQueryReturnID(Query);
            return account.admin.id;
        }

        public Int64 saveUser(User user)

        {           
            using (WSTransaction transaction = new WSTransaction("SAVE_USER"))
            {
                
                foreach(Contact contact in user.contacts)
                {
                    contact.id = new DALContactManager().saveContact(contact, transaction);
                    if(contact.id != 0)
                    {                     
                        if (contact.ContactType == Contact.Contact_Type.EMAIL)
                        {
                            user.contact = contact;
                        }                       
                    }
                    else
                    {
                        transaction.rollback();
                        return user.id = 0;
                    }
                }
                user.memberId = getAlphaNumericCode(4);
                String query = "INSERT INTO users(contact_id, account_id, member_id, created_on, updated_on) OUTPUT Inserted.id  VALUES(@contactId, @accountId, @memberId, @createdOn, @updatedOn); ";
                WSQuery Query = new WSQuery(query, user.toDictionary(), transaction);
                user.id = WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQueryReturnID(Query);

                if(user.id != 0)
                {
                    foreach(Contact contact in user.contacts)
                    {
                        user.contact = contact;
                        if(!this.saveUserContacts(user,transaction))
                        {
                            transaction.rollback();
                            return user.id = 0;
                        }
                    }

                    if(user.beacon != null)
                    {
                        if(!this.saveUserBeacons(user, transaction))
                        {
                            transaction.rollback();
                            return user.id = 0;
                        }                        
                    }

                    if(user.kiosk != null)
                    {
                        if(!this.saveUserKiosks(user, transaction))
                        {
                            transaction.rollback();
                            return user.id = 0;
                        }
                    }

                    user.offer = offerManager.getDetails(user.offer);                
                    user.offer.code = this.getAlphaNumericCode(8);
                    if (!this.saveUsersOfferCodes(user, transaction))
                    {                      
                        transaction.rollback();
                        return user.id = 0;
                    }
                    user.account = accountManager.getDetails(user.account);               
                   this.sendOfferCodeToUser(user);
                    transaction.commit();
                    return user.id;
                }
                else
                {
                    transaction.rollback();
                    return user.id = 0;
                }
            }
        }

        public bool saveMemberOffer(User member)
        {
            using (WSTransaction transaction = new WSTransaction("SAVE_MEMBER_OFFER"))
            {
                member = getUserDetails(member);
                member.contacts = getCantactsForUser(member.id);
                member.is_registered = 0;
                if(member.id != 0)
                {
                    if (member.beacon != null)
                    {
                        if (!this.saveUserBeacons(member, transaction))
                        {
                            transaction.rollback();
                            return false;
                        }
                    }

                    if (member.kiosk != null)
                    {
                        if (!this.saveUserKiosks(member, transaction))
                        {
                            transaction.rollback();
                            return false;
                        }
                    }

                    member.offer = offerManager.getDetails(member.offer);
                    member.offer.code = this.getAlphaNumericCode(8);
                    if (!this.saveUsersOfferCodes(member, transaction))
                    {
                        transaction.rollback();
                        return false;
                    }

                    this.sendOfferCodeToUser(member);
                    transaction.commit();
                    return true;
                }
                else
                {
                    transaction.rollback();
                    return false;
                }
            }

        }

        public User getUserDetails(User member)
        {
            String query = QueryConstants.USER_DETAILS;
            WSQuery Query = new WSQuery(query);
            Query.parameters["memberId"] = member.memberId;
            Query.parameters["accountId"] = member.account.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            User user = new User();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    user = new User(reader);
                }               
            }
            Query.connection.Close();
            member.id = user.id;
            return member;
        }

        public String getAlphaNumericCode(int n)
        {
            int maxSize = n;
            char[] chars = new char[62];
            chars =
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
            byte[] data = new byte[1];
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetNonZeroBytes(data);
                data = new byte[maxSize];
                crypto.GetNonZeroBytes(data);
            }
            StringBuilder result = new StringBuilder(maxSize);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }

        public bool saveUsersOfferCodes(User user, WSTransaction transaction)
        {
            String query = "INSERT INTO users_offer_codes(offer_id, user_id, code) VALUES(@offerId, @userId, @code);";
            WSQuery Query = new WSQuery(query);
            Query.parameters["offerId"] = user.offer.id;
            Query.parameters["userId"] = user.id;
            Query.parameters["code"] = user.offer.code;
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);
        }
                 
        public bool saveUserContacts(User user, WSTransaction transaction)
        {
            String query = "INSERT INTO users_contacts(user_id, contact_id) VALUES(@userId, @contactId);";
            WSQuery Query = new WSQuery(query);
            Query.parameters["userId"] = user.id;
            Query.parameters["contactId"] = user.contact.id;
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);
        }     
        
        public bool saveUserBeacons(User user, WSTransaction transaction)
        {
            String query = "INSERT INTO users_beacons(user_id, beacon_id, offer_id, date, is_registered) VALUES(@userId, @beaconId, @offerId, @date, @isRegistered);";
            WSQuery Query = new WSQuery(query);
            Query.parameters["userId"] = user.id;
            Query.parameters["beaconId"] = user.beacon.id;
            Query.parameters["offerId"] = user.offer.id;
            Query.parameters["date"] = DateTime.Now.ToString();
            Query.parameters["isRegistered"] = user.is_registered;
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);
        }

        public bool saveUserKiosks(User user, WSTransaction transaction)
        {

            String query = "INSERT INTO users_kiosks(user_id, kiosk_id, offer_id, date, is_registered) VALUES(@userId, @kioskId, @offerId, @date, @isRegistered);";
            WSQuery Query = new WSQuery(query);
            Query.parameters["userId"] = user.id;
            Query.parameters["kioskId"] = user.kiosk.id;
            Query.parameters["offerId"] = user.offer.id;
            Query.parameters["date"] = DateTime.Now.ToString();
            Query.parameters["isRegistered"] = user.is_registered;
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);
        }

        private string PopulateBody(User user)
        {
            String categories = String.Empty;
            String categoryValue = String.Empty;
            int i = 0;
            foreach(Category category in user.offer.categories)
            {
                i++;
                if (user.offer.categories.Count == i)
                {
                    categories += category.name;
                    categoryValue += category.value + " " + category.name + ".";
                }
                else 
                {
                    categories += category.name + " and ";
                    categoryValue += category.value + " " + category.name + " and ";
                }               
            }           
            //categoryValue += ".";
            //string path = HttpContext.Current.Server.MapPath("~").Replace("\\", @"/");
            //path = path.Remove(path.Length-1);
            //path = path + user.account.picUrl;
            //dynamic model = new ExpandoObject();
            //model.picUrl = user.account.picUrl;
            //model.company_name = user.account.name;
            //model.UserName = emailContact.value;
            //model.title = user.offer.title;
            //model.description = user.offer.description;
            //model.category = offerCategory.name;
            //model.value = offerCategory.value;
            //model.code = user.offer.code;
            //model.memberId = user.memberId;
            //model.website = user.account.website;

            string body = string.Empty;
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/bnsMail.html")))
            {
                body = reader.ReadToEnd();
            }
            //body = Engine.Razor.RunCompile(body, new Random().Next(100, 10000).ToString(), null, (object)model); //rendering the template with our model
            //body = body.Replace("{logo_src}", "cid:" + BNSConstants.LOCAL_HOST + user.account.picUrl);                    
            body = body.Replace("{categories}", categories);
            body = body.Replace("{category_value}", categoryValue);
            body = body.Replace("{offer_code}", user.offer.code);
            body = body.Replace("{member_id}", user.memberId);
            body = body.Replace("{register_link}", "http://www.novomatic.com/en/user/register");
            body = body.Replace("{sender}", user.account.name);

            return body;
        }

        public void sendOfferCodeToUser(User user)
        {
            string smtpAddress = "smtp.gmail.com";
            int portNumber = 587;
            bool enableSSL = true;

            Contact emailContact = user.contacts.First<Contact>();
            Category offerCategory = user.offer.categories.First<Category>();
            string emailFrom = "bestnetworksystem@gmail.com";
            string password = "bestnetworksystems";
            string emailTo = emailContact.value;
            string subject = user.offer.title;                                                                        

            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress(emailFrom);
                mail.To.Add(emailTo);
                mail.Subject = subject;
                mail.Body = PopulateBody(user);
                mail.IsBodyHtml = true;               

                using (SmtpClient smtp = new SmtpClient(smtpAddress, portNumber))
                {
                    smtp.Credentials = new NetworkCredential(emailFrom, password);
                    smtp.EnableSsl = enableSSL;
                    smtp.Send(mail);
                }
            }
        } 

        public bool verifyMemberId (String memberId, Int64 accountId)
        {
            String query = "SELECT CASE WHEN EXISTS (Select * from users where member_id = @memberId and account_id = @accountId ) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["memberId"] = memberId;
            Query.parameters["accountId"] = accountId;          
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }

        public bool checkIfUserNameExists(String userName)
        {
            String query = "SELECT CASE WHEN EXISTS (Select * from users where username = @userName) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["userName"] = userName;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }
    }
}