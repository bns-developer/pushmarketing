﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WSDatabase;
using BNS.Models.Users;

namespace BNS.DAL.DALUser
{
    public class UserLogin 
    {
        public User isValidUser(String username, String password)
        {
            User user = new User();
            Dictionary<String, Object> parameters = new Dictionary<String, Object>();
            parameters["username"] = username;
            parameters["password"] = password;
            String query = QueryConstants.USER_DETAILS1;
            System.Data.SqlClient.SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.getRecordsFromDatabase(query, parameters);
            if (reader.HasRows)
            {               
                while (reader.Read())
                {
                    user = new User(reader);
                }                            
            }
            return user;
        }      
    }
}