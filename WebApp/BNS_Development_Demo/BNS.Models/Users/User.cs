﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.Accounts;
using BNS.Models.BNSModels;
using BNS.Models.Contacts;
using Newtonsoft.Json;
using BNS.Models.Beacons;
using BNS.Models.Kiosks;
using BNS.Models.Devices;
using BNS.Models.Offers;
using System.Data.SqlClient;
using BNS.DL.WSDatabase;
using WSDatabase;
using Microsoft.Owin;
using System.Security.Claims;
using Newtonsoft.Json.Converters;

namespace BNS.Models.Users
{
    public class User : BNSModel
    {

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Contact contact { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Account  account { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String memberId { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String username { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String password { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<Contact> contacts { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Beacon beacon { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Kiosk kiosk { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Device device { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Offer offer { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Offer latestOffer { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String lastAactivityDate { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int is_registered = 0;

        public enum User_Type
        {
            MEMBER = 1,
            NON_MEMBER = 2,           
        };
        private User_Type _type;

        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public User_Type UserType
        {
            get { return _type; }
            set { _type = value; }
        }

        public User(HttpContext current)
        {
            if (HttpContext.Current.Request.Form.AllKeys.Contains("username"))
            {
                username = Convert.ToString(HttpContext.Current.Request.Form.GetValues("username").FirstOrDefault());
            }
            if (HttpContext.Current.Request.Form.AllKeys.Contains("password"))
            {
                password = Convert.ToString(HttpContext.Current.Request.Form.GetValues("password").FirstOrDefault());
            }
        }


        public User()
        {
            contacts = new List<Contact>();
        }

        private void setType(int code)
        {
            switch (code)
            {
                case 1: this.UserType = User_Type.MEMBER; break;
                case 2: this.UserType = User_Type.NON_MEMBER; break;               
            }
        }

        public User(SqlDataReader reader)
        {
            if (WSDatabaseHelper.isValidField(reader, "USER_ID"))
            {
                this.id = reader.GetInt64(reader.GetOrdinal("USER_ID"));
            }

            if (WSDatabaseHelper.isValidField(reader, "ACCOUNT_ID"))
            {
                account = new Account(reader);
            }

            if (WSDatabaseHelper.isValidField(reader, "USER_MEMBER_ID"))
            {
                this.name = reader.GetString(reader.GetOrdinal("USER_MEMBER_ID"));
            }

            if (WSDatabaseHelper.isValidField(reader, "USER_CREATED_ON"))
            {
               this.createdOn = reader.GetDateTime(reader.GetOrdinal("USER_CREATED_ON")).ToString();                
            }

            if (WSDatabaseHelper.isValidField(reader, "CONTACT_ID"))
            {               
                contact = new Contact(reader);
            }

            if (WSDatabaseHelper.isValidField(reader, "DEVICE_ID"))
            {
                device = new Device(reader);
            }           

            if (WSDatabaseHelper.isValidField(reader, "OFFER_ID"))
            {
                offer = new Offer(reader);
            }

            if (WSDatabaseHelper.isValidField(reader, "USER_LATEST_OFFER"))
            {
                latestOffer = new Offer();
                latestOffer.id = reader.GetInt64(reader.GetOrdinal("USER_LATEST_OFFER"));
            }

            if (WSDatabaseHelper.isValidField(reader, "USER_LAST_ACTIVITY"))
            {
                lastAactivityDate = reader.GetDateTime(reader.GetOrdinal("USER_LAST_ACTIVITY")).ToString();
            }
        }

        public override void save()
        {
            throw new NotImplementedException();
        }

        public User getContextUser(IOwinContext context)
        {
            ClaimsPrincipal user = context.Authentication.User;
            this.id = Convert.ToInt32(user.Claims.FirstOrDefault(claim => claim.Type == "userId").Value);
            account = new Account();
            account.id = Convert.ToInt32(user.Claims.FirstOrDefault(claim => claim.Type == "accountId").Value);

            //this.Email = user.Claims.FirstOrDefault(claim => claim.Type == "EmailId").Value;
            //this.FirstName = user.Claims.FirstOrDefault(claim => claim.Type == "FirstName").Value;
            //this.LastName = user.Claims.FirstOrDefault(claim => claim.Type == "LastName").Value;
            //this.IsAreaManager = bool.Parse(user.Claims.FirstOrDefault(claim => claim.Type == "IsAreaManager").Value);
            return this;
         }

        public override Dictionary<string, object> toDictionary()
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            if (this.id != 0)
                parameters["id"] = this.id;
            if (contact != null)
                parameters["contactId"] = contact.id;
            if (account != null)
                parameters["accountId"] = account.id;
            if (memberId != null)
                parameters["memberId"] = memberId;
            parameters["createdOn"] = DateTime.Now.ToString();
            parameters["updatedOn"] = DateTime.Now.ToString();
           
            return parameters;
        }

        public override void update()
        {
            throw new NotImplementedException();
        }
    }
}