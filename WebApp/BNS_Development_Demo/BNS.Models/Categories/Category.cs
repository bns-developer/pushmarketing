﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.Accounts;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;
using BNS.DL.WSDatabase;
using WSDatabase;

namespace BNS.Models.Categories
{
    public class Category
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Int64 id { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String name { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Account account { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String value { get; set; }

        public Category(JObject categoryJson)
        {
            if (JSONHelper.isValidFieldForKey(categoryJson, "id"))
                id = (Int64)categoryJson["id"];

            if (JSONHelper.isValidFieldForKey(categoryJson, "name"))
                name = (String)categoryJson["name"];

            if (JSONHelper.isValidFieldForKey(categoryJson, "value"))
                value = (String)categoryJson["value"];
        }

        public Category(SqlDataReader reader)
        {
            if (WSDatabaseHelper.isValidField(reader, "CATEGORY_ID"))
            {
                id = reader.GetInt64(reader.GetOrdinal("CATEGORY_ID"));              
            }
            if (WSDatabaseHelper.isValidField(reader, "CATEGORY_NAME"))
            {               
                name = reader.GetString(reader.GetOrdinal("CATEGORY_NAME"));           
            }
            if (WSDatabaseHelper.isValidField(reader, "OFFER_CATEGORY_VALUE"))
            {                          
                value = reader.GetString(reader.GetOrdinal("OFFER_CATEGORY_VALUE"));
            }
        }

        public Category(String type, Int64 accountNo)
        {
            name = type;
            account = new Account();
            account.id = accountNo;
        }
        public Category()
        {
        }
    }
}