﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.BNSModels;
using BNS.Models.Stores;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;
using BNS.DL.WSDatabase;
using BNS.Models.LuckyItems;
using WSDatabase;

namespace BNS.Models.Kiosks
{
    public class Kiosk : BNSModel
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String pushId { get;  set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Store store { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<LuckyItem> luckyItems { get; set; }

        public Int64 registredUsersToday = 0;

        public Int64 registredUsersThisWeek = 0;

        public Int64 registredUsersThisMonth = 0;

        public Int64 totalRegistredUsers = 0;

        public Kiosk(JObject kioskJson)
        {
            if (JSONHelper.isValidFieldForKey(kioskJson, "id"))
                this.id = (Int64)kioskJson["id"];

            if (JSONHelper.isValidFieldForKey(kioskJson, "name"))
                this.name = (String)kioskJson["name"];

            if (JSONHelper.isValidFieldForKey(kioskJson, "number"))
                this.number = (String)kioskJson["number"];

            if (JSONHelper.isValidFieldForKey(kioskJson, "pushId"))
                pushId = (String)kioskJson["pushId"];
            else
                pushId = "";           

            if (JSONHelper.isValidFieldForKey(kioskJson, "store"))
            {
                JObject storeJObject = (JObject)kioskJson["store"];
                store = new Store(storeJObject);             
            }

            luckyItems = new List<LuckyItem>();
        }

        public Kiosk(SqlDataReader reader)
        {
            luckyItems = new List<LuckyItem>();
            if (WSDatabaseHelper.isValidField(reader, "KIOSK_ID"))
            {
                this.id = reader.GetInt64(reader.GetOrdinal("KIOSK_ID"));
            }

            if (WSDatabaseHelper.isValidField(reader, "KIOSK_NUMBER"))
            {
                this.number = reader.GetString(reader.GetOrdinal("KIOSK_NUMBER"));
            }

            if (WSDatabaseHelper.isValidField(reader, "KIOSK_NAME"))
            {
                this.name = reader.GetString(reader.GetOrdinal("KIOSK_NAME"));
            }

            if (WSDatabaseHelper.isValidField(reader, "KIOSK_STORE"))
            {
                store = new Store(reader);               
            }

            if (WSDatabaseHelper.isValidField(reader, "REGISTERED_USERS_TODAY"))
            {
                registredUsersToday = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("REGISTERED_USERS_TODAY")));
            }

            if (WSDatabaseHelper.isValidField(reader, "REGISTERED_USERS_THIS_WEEK"))
            {
                registredUsersThisWeek = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("REGISTERED_USERS_THIS_WEEK")));
            }

            if (WSDatabaseHelper.isValidField(reader, "REGISTERED_USERS_THIS_MONTH"))
            {
                registredUsersThisMonth = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("REGISTERED_USERS_THIS_MONTH")));
            }

            if (WSDatabaseHelper.isValidField(reader, "TOTAL_REGISTERED_USERS"))
            {
                totalRegistredUsers = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("TOTAL_REGISTERED_USERS")));
            }

            if (WSDatabaseHelper.isValidField(reader, "LUCKY_ITEMS"))
            {                
                luckyItems.Add(new LuckyItem(reader));
            }
        }

        public Kiosk()
        {
        }

        public override Dictionary<string, object> toDictionary()
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            if (this.id != 0)
                parameters["id"] = this.id;
            if (this.name != null)
                parameters["name"] = this.name;
            if (this.number != null)
                parameters["number"] = this.number;
            if (store != null)
                parameters["storeId"] = store.id;
             
            parameters["pushId"] = pushId;               
            parameters["createdBy"] = createdBy.id;
            parameters["updatedBy"] = updatedBy.id;
            parameters["updatedOn"] = DateTime.Now.ToString();
            parameters["createdOn"] = DateTime.Now.ToString();
            return parameters;
        }

        public override void save()
        {
            throw new NotImplementedException();
        }

        public override void update()
        {
            throw new NotImplementedException();
        }
    }
}