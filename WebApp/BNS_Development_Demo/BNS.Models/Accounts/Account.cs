﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.BNSModels;
using BNS.Models.Users;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WSDatabase;
using System.Data.SqlClient;

namespace BNS.Models.Accounts
{
    public class Account : BNSModel
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String website { get; set; }       

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public HttpPostedFile logo { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String picUrl { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public User admin { get; set; }

        public Account(JObject accountJson)
        {
            if (JSONHelper.isValidFieldForKey(accountJson, "id"))
                id = (Int64)accountJson["id"];

            if (JSONHelper.isValidFieldForKey(accountJson, "name"))
                name = (String)accountJson["name"];
        }

        public Account(HttpContext current)
        {
            if (HttpContext.Current.Request.Form.AllKeys.Contains("id"))
            {
                id = Convert.ToInt64(HttpContext.Current.Request.Form.GetValues("id").FirstOrDefault());
            }

            if (HttpContext.Current.Request.Form.AllKeys.Contains("name"))
            {
                name = Convert.ToString(HttpContext.Current.Request.Form.GetValues("name").FirstOrDefault());
            }
            
            if(HttpContext.Current.Request.Form.AllKeys.Contains("website"))
            {
                website = Convert.ToString(HttpContext.Current.Request.Form.GetValues("website").FirstOrDefault());
            }

            admin = new User(HttpContext.Current);
            HttpFileCollection files = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files : null;
            if (files != null)
            {
                if (files.Count == 0)
                {
                    picUrl = null;
                }
                else
                {
                    foreach (string fileName in files)
                    {
                        logo = files[fileName];
                    }
                }
            }
            else
            {
                picUrl = null;
            }
        }

        public Account()
        {
        }

        public Account(SqlDataReader reader)
        {
            if (WSDatabaseHelper.isValidField(reader, "ACCOUNT_ID"))
            {
                id = reader.GetInt64(reader.GetOrdinal("ACCOUNT_ID"));
            }

            if (WSDatabaseHelper.isValidField(reader, "ACCOUNT_NAME"))
            {
                name = reader.GetString(reader.GetOrdinal("ACCOUNT_NAME"));
            }            

            if (WSDatabaseHelper.isValidField(reader, "ACCOUNT_ADMIN"))
            {
                admin = new User();
                admin.id = reader.GetInt64(reader.GetOrdinal("ACCOUNT_ADMIN"));
            }           

            if (WSDatabaseHelper.isValidField(reader, "ACCOUNT_PIC_URL"))
            {
                picUrl = reader.GetString(reader.GetOrdinal("ACCOUNT_PIC_URL"));
            }

            if (WSDatabaseHelper.isValidField(reader, "ACCOUNT_WEBSITE"))
            {
                website = reader.GetString(reader.GetOrdinal("ACCOUNT_WEBSITE"));
            }


            if (WSDatabaseHelper.isValidField(reader, "ACCOUNT_CREATED_ON"))
            {
               createdOn = reader.GetDateTime(reader.GetOrdinal("ACCOUNT_CREATED_ON")).ToString();
            }

            if (WSDatabaseHelper.isValidField(reader, "ACCOUNT_UPDATED_ON"))
            {
                updatedOn = reader.GetDateTime(reader.GetOrdinal("ACCOUNT_UPDATED_ON")).ToString();
            }
        }

        public override void save()
        {
            throw new NotImplementedException();
        }

        public override void update()
        {
            throw new NotImplementedException();
        }

        public override Dictionary<string, object> toDictionary()
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            if (id != 0)
                parameters["id"] = id;
            if (name != null)
                parameters["name"] = name;
            if(website != null)
                parameters["website"] = website;

            parameters["picUrl"] = picUrl;        
            parameters["updatedOn"] = DateTime.Now.ToString();
            parameters["createdOn"] = DateTime.Now.ToString();
            return parameters;
        }
    }
}