﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.BNSModels;
using BNS.Models.Stores;
using BNS.Models.Offers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;
using BNS.DL.WSDatabase;
using WSDatabase;

namespace BNS.Models.Beacons
{
    public class Beacon : BNSModel
    {

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Store store { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<Offer> offers { get; set; }

        public String shortUrl { get; set; }
        public Int64 registredUsersToday = 0;
        public Int64 registredUsersThisWeek = 0;
        public Int64 registredUsersThisMonth = 0;
        public Int64 totalRegistredUsers = 0;       
        
        public String beacon_unique_id { get; set; }


        public Beacon(SqlDataReader reader)
        {
            if (WSDatabaseHelper.isValidField(reader, "BEACON_ID"))
            {
                this.id = reader.GetInt64(reader.GetOrdinal("BEACON_ID"));
            }

            if (WSDatabaseHelper.isValidField(reader, "BEACON_NUMBER"))
            {
                this.number = reader.GetString(reader.GetOrdinal("BEACON_NUMBER"));
            }

            if (WSDatabaseHelper.isValidField(reader, "BEACON_NAME"))
            {
                this.name = reader.GetString(reader.GetOrdinal("BEACON_NAME"));
            }

            if (WSDatabaseHelper.isValidField(reader, "BEACON_SHORT_URL"))
            {
                shortUrl = reader.GetString(reader.GetOrdinal("BEACON_SHORT_URL"));
            }
            else
            {
                shortUrl = null;
            }

            if (WSDatabaseHelper.isValidField(reader, "BEACON_STORE"))
            {
                store = new Store(reader);
            }

            if (WSDatabaseHelper.isValidField(reader, "REGISTERED_USERS_TODAY"))
            {
                registredUsersToday = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("REGISTERED_USERS_TODAY")));
            }

            if (WSDatabaseHelper.isValidField(reader, "REGISTERED_USERS_THIS_WEEK"))
            {
               registredUsersThisWeek  = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("REGISTERED_USERS_THIS_WEEK")));
            }

            if (WSDatabaseHelper.isValidField(reader, "REGISTERED_USERS_THIS_MONTH"))
            {
                registredUsersThisMonth = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("REGISTERED_USERS_THIS_MONTH")));
            }

            if (WSDatabaseHelper.isValidField(reader, "TOTAL_REGISTERED_USERS"))
            {
                totalRegistredUsers = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("TOTAL_REGISTERED_USERS")));
            }


            if (WSDatabaseHelper.isValidField(reader, "OFFER_ID"))
            {
                offers = new List<Offer>();
                offers.Add(new Offer(reader));
            }
        }

        public Beacon(JObject beaconJson)
        {
            if (JSONHelper.isValidFieldForKey(beaconJson, "id"))
                this.id = (Int64)beaconJson["id"];

            if (JSONHelper.isValidFieldForKey(beaconJson, "name"))
                this.name = (String)beaconJson["name"];

            if (JSONHelper.isValidFieldForKey(beaconJson, "number"))
                this.number = (String)beaconJson["number"];

            if (JSONHelper.isValidFieldForKey(beaconJson, "store"))
            {
                JObject storeJObject = (JObject)beaconJson["store"];
                store = new Store(storeJObject);
            }
            beacon_unique_id = Guid.NewGuid().ToString();

            offers = new List<Offer>();
        }
       
        public Beacon()
        {
        }

        public override Dictionary<string, object> toDictionary()
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            if (this.id != 0)
                parameters["id"] = this.id;
            if (this.name != null)
                parameters["name"] = this.name;
            if (this.number != null)
                parameters["number"] = this.number;
            if (store != null)
                parameters["storeId"] = store.id;
            if (beacon_unique_id != null)
                parameters["beaconGuid"] = beacon_unique_id;
            parameters["createdBy"] = createdBy.id;
            parameters["updatedBy"] = updatedBy.id;
            parameters["updatedOn"] = DateTime.Now.ToString();
            parameters["createdOn"] = DateTime.Now.ToString();
            return parameters;
        }
        public override void save()
        {
            throw new NotImplementedException();
        }  

        public override void update()
        {
            throw new NotImplementedException();
        }
    }
}