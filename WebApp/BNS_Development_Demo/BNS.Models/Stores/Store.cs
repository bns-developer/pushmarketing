﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.BNSModels;
using BNS.Models.Accounts;
using BNS.Models.Countries;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;
using BNS.DL.WSDatabase;
using WSDatabase;

namespace BNS.Models.Stores
{
    public class Store : BNSModel
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Country country { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Account account { get; set; }


        public Store(JObject storeJson)
        {
            if (JSONHelper.isValidFieldForKey(storeJson, "id"))
                this.id = (Int64)storeJson["id"];
                           
            if (JSONHelper.isValidFieldForKey(storeJson, "name"))
                this.name = (String)storeJson["name"];

            if (JSONHelper.isValidFieldForKey(storeJson, "number"))
                this.number = (String)storeJson["number"];

            if (JSONHelper.isValidFieldForKey(storeJson, "country"))
            {
                JObject countryJObject = (JObject)storeJson["country"];
                country = new Country(countryJObject);
            }
            
            if(JSONHelper.isValidFieldForKey(storeJson, "account"))
            {
                JObject accountJobject = (JObject)storeJson["account"];
                account = new Account(accountJobject);
            }           
        }

        public Store(SqlDataReader reader)
        {
            if (WSDatabaseHelper.isValidField(reader, "STORE_ID"))
            {
               this.id = reader.GetInt64(reader.GetOrdinal("STORE_ID"));
            }

            if (WSDatabaseHelper.isValidField(reader, "STORE_NUMBER"))
            {
                this.number = reader.GetString(reader.GetOrdinal("STORE_NUMBER"));
            }

            if (WSDatabaseHelper.isValidField(reader, "STORE_NAME"))
            {
                this.name = reader.GetString(reader.GetOrdinal("STORE_NAME"));
            }

            if (WSDatabaseHelper.isValidField(reader, "STORE_COUNTRY"))
            {
                country = new Country(reader);            
            }
        }
        

        public override Dictionary<string, object> toDictionary()
        {
            
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            if (this.id != 0)
                parameters["id"] = this.id;
            if (country != null)
                parameters["countryId"] = country.id;
            if (this.name != null)
                parameters["name"] = this.name;
            if (this.number != null)
                parameters["number"] = this.number;
            parameters["accountId"] = account.id;
            parameters["createdBy"] = createdBy.id;
            parameters["updatedBy"] = updatedBy.id;
            parameters["updatedOn"] = DateTime.Now.ToString();
            parameters["createdOn"] = DateTime.Now.ToString();
            return parameters;
        }

        public override void save()
        {
            throw new NotImplementedException();
        }

        public override void update()
        {
            throw new NotImplementedException();
        }
    }
}