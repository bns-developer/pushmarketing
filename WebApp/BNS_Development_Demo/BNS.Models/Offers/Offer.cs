﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.Offers;
using BNS.Models.BNSModels;
using BNS.Models.Accounts;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;
using BNS.DL.WSDatabase;
using BNS.Models.Countries;
using BNS.Models.Categories;
using System.Data;
using WSDatabase;
using Newtonsoft.Json.Converters;

namespace BNS.Models.Offers
{
    public class Offer : BNSModel
    {
        public enum Offer_Type
        {
            MEMBER = 1,
            NON_MEMBER = 2,
            BOTH = 3           
        };
        private Offer_Type _type;

        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Offer_Type OfferType
        {
            get { return _type; }
            set { _type = value; }
        }
  
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Account account { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String imagePath { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String title { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String description { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String startTime { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Int64 duration { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String endTime { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<Country> countries { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<Category> categories { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public HttpPostedFile file { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String code { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Int64 registredUsersToday { get; set;}

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Int64 registredUsersThisWeek { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Int64 registredUsersThisMonth { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Int64 totalRegistredUsers { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Int64 beaconStatus { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Int64 kioskStatus { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool isCurrentlyActive { get; set; }


        public Offer(JObject offerJson)
        {
            if (WSDatabase.JSONHelper.isValidFieldForKey(offerJson, "id"))
                this.id = (Int64)offerJson["id"];

            if (WSDatabase.JSONHelper.isValidFieldForKey(offerJson, "number"))
                this.number = (String)offerJson["number"];

            if (JSONHelper.isValidFieldForKey(offerJson, "title"))
                title = (String)offerJson["title"];

            if (JSONHelper.isValidFieldForKey(offerJson, "description"))
                description = (String)offerJson["description"];

            if (JSONHelper.isValidFieldForKey(offerJson, "startTime"))
                startTime = (String)offerJson["startTime"];

            if (JSONHelper.isValidFieldForKey(offerJson, "endTime"))
                endTime = (String)offerJson["endTime"];            

            if (JSONHelper.isValidFieldForKey(offerJson, "account"))
            {
                JObject accountJobject = (JObject)offerJson["account"];
                account = new Account(accountJobject);
            }

            imagePath = "/Resources/images/Offer_Image.png";
            countries = new List<Country>();
            categories = new List<Category>();
        }


        public Offer(SqlDataReader reader)
        {
            if (WSDatabaseHelper.isValidField(reader, "OFFER_ID"))
            {
                this.id = reader.GetInt64(reader.GetOrdinal("OFFER_ID"));
            }

            if (WSDatabaseHelper.isValidField(reader, "OFFER_NUMBER"))
            {
                number = reader.GetString(reader.GetOrdinal("OFFER_NUMBER"));
            }

            if (WSDatabaseHelper.isValidField(reader, "OFFER_TYPE"))
            {
                Int64 i = reader.GetInt64(reader.GetOrdinal("OFFER_TYPE"));
                this.setType((int)i);
            }

            if (WSDatabaseHelper.isValidField(reader, "OFFER_IMAGE_PATH"))
            {
                imagePath = reader.GetString(reader.GetOrdinal("OFFER_IMAGE_PATH"));
            }
            else
            {
                imagePath = null;
            }

            if (WSDatabaseHelper.isValidField(reader, "OFFER_TITLE"))
            {
                title = reader.GetString(reader.GetOrdinal("OFFER_TITLE"));
            }

            if (WSDatabaseHelper.isValidField(reader, "OFFER_DESCRIPTION"))
            {
                description = reader.GetString(reader.GetOrdinal("OFFER_DESCRIPTION"));
            }

            if (WSDatabaseHelper.isValidField(reader, "OFFER_START_TIME"))
            {
                startTime = reader.GetDateTime(reader.GetOrdinal("OFFER_START_TIME")).ToString();
            }

            if (WSDatabaseHelper.isValidField(reader, "OFFER_END_TIME"))
            {
                endTime = reader.GetDateTime(reader.GetOrdinal("OFFER_END_TIME")).ToString();
            }

            if (WSDatabaseHelper.isValidField(reader, "OFFER_DURATION"))
            {
                duration = reader.GetInt64(reader.GetOrdinal("OFFER_DURATION"));
            }     

            if (WSDatabaseHelper.isValidField(reader, "OFFER_CREATED_ON"))
            {
                this.createdOn = reader.GetDateTime(reader.GetOrdinal("OFFER_CREATED_ON")).ToString();
            }

            if (WSDatabaseHelper.isValidField(reader, "OFFER_UPDATED_ON"))
            {
                this.updatedOn = reader.GetDateTime(reader.GetOrdinal("OFFER_UPDATED_ON")).ToString();
            }

            if (WSDatabaseHelper.isValidField(reader, "REGISTERED_USERS_TODAY"))
            {
                registredUsersToday = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("REGISTERED_USERS_TODAY")));
            }

            if (WSDatabaseHelper.isValidField(reader, "REGISTERED_USERS_THIS_WEEK"))
            {
                registredUsersThisWeek = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("REGISTERED_USERS_THIS_WEEK")));
            }

            if (WSDatabaseHelper.isValidField(reader, "REGISTERED_USERS_THIS_MONTH"))
            {
                registredUsersThisMonth = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("REGISTERED_USERS_THIS_MONTH")));
            }

            if (WSDatabaseHelper.isValidField(reader, "TOTAL_REGISTERED_USERS"))
            {
                totalRegistredUsers = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("TOTAL_REGISTERED_USERS")));
            }

            if (WSDatabaseHelper.isValidField(reader, "BEACON_STATUS"))
            {
                beaconStatus = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("BEACON_STATUS")));
            }

            if (WSDatabaseHelper.isValidField(reader, "KIOSK_STATUS"))
            {
                kioskStatus = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("KIOSK_STATUS")));
            }

            if (WSDatabaseHelper.isValidField(reader, "CATEGORY_ID"))
            {
                categories = new List<Category>();
                categories.Add(new Category(reader));
            }

            if (WSDatabaseHelper.isValidField(reader, "COUNTRY_ID"))
            {
                countries = new List<Country>();
                countries.Add(new Country(reader));
            }


        }


        public Offer(HttpContext current)
        {
            //id = Convert.ToInt64(HttpContext.Current.Request.Form.GetValues("id").FirstOrDefault());
            number = Convert.ToString(HttpContext.Current.Request.Form.GetValues("number").FirstOrDefault());
            Int16 type = Convert.ToInt16(HttpContext.Current.Request.Form.GetValues("offerType").FirstOrDefault());
            setType(type);
            title = Convert.ToString(HttpContext.Current.Request.Form.GetValues("title").FirstOrDefault());
            description = Convert.ToString(HttpContext.Current.Request.Form.GetValues("description").FirstOrDefault());
            startTime = Convert.ToString(HttpContext.Current.Request.Form.GetValues("startTime").FirstOrDefault());
            endTime = Convert.ToString(HttpContext.Current.Request.Form.GetValues("endTime").FirstOrDefault());
            duration  = Convert.ToInt64(HttpContext.Current.Request.Form.GetValues("duration").FirstOrDefault());
            HttpFileCollection files = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files : null;
            if(files !=  null)
            {
                if(files.Count == 0 )
                {
                    imagePath = "No Image";
                }
                else
                {
                    foreach (string fileName in files)
                    {
                        file = files[fileName];
                    }
                }
            }
            else
            {
                imagePath = "No Image";
            }
                          
            countries = new List<Country>();
            categories = new List<Category>();
        }

        public Offer()
        {
        }
        private void setType(int code)
        {
            switch (code)
            {
                case 1: this.OfferType = Offer_Type.MEMBER ; break;
                case 2: this.OfferType = Offer_Type.NON_MEMBER ; break;
                case 3: this.OfferType = Offer_Type.BOTH; break;             
            }
        }

        public Offer(DataRow row)
        {
        //    if (WSDatabaseHelper.isValidField(row, "OFFER_ID"))
        //    {
        //        this.id = reader.GetInt64(reader.GetOrdinal("OFFER_ID"));
        //    }

        //    if (WSDatabaseHelper.isValidField(row, "OFFER_NUMBER"))
        //    {
        //        number = Convert.ToString(row[ "OFFER_NUMBER"]);
        //    }

        //    if (WSDatabaseHelper.isValidField(row, "OFFER_TITLE"))
        //    {
        //        title = reader.GetString(row.GetOrdinal("OFFER_TITLE"));
        //    }

        //    if (WSDatabaseHelper.isValidField(reader, "OFFER_DESCRIPTION"))
        //    {
        //        description = reader.GetString(reader.GetOrdinal("OFFER_DESCRIPTION"));
        //    }

        //    if (WSDatabaseHelper.isValidField(reader, "OFFER_START_TIME"))
        //    {
        //        startTime = reader.GetDateTime(reader.GetOrdinal("OFFER_START_TIME")).ToString();
        //    }

        //    if (WSDatabaseHelper.isValidField(reader, "OFFER_END_TIME"))
        //    {
        //        endTime = reader.GetDateTime(reader.GetOrdinal("OFFER_END_TIME")).ToString();
        //    }

        //    if (WSDatabaseHelper.isValidField(reader, "OFFER_CREATED_ON"))
        //    {
        //        this.createdOn = reader.GetDateTime(reader.GetOrdinal("OFFER_CREATED_ON")).ToString();
        //    }

        //    if (WSDatabaseHelper.isValidField(reader, "OFFER_UPDATED_ON"))
        //    {
        //        this.updatedOn = reader.GetDateTime(reader.GetOrdinal("OFFER_UPDATED_ON")).ToString();
        //    }

        //    if (WSDatabaseHelper.isValidField(reader, "REGISTERED_USERS_TODAY"))
        //    {
        //        registredUsersToday = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("REGISTERED_USERS_TODAY")));
        //    }

        //    if (WSDatabaseHelper.isValidField(reader, "REGISTERED_USERS_THIS_WEEK"))
        //    {
        //        registredUsersThisWeek = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("REGISTERED_USERS_THIS_WEEK")));
        //    }

        //    if (WSDatabaseHelper.isValidField(reader, "REGISTERED_USERS_THIS_MONTH"))
        //    {
        //        registredUsersThisMonth = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("REGISTERED_USERS_THIS_MONTH")));
        //    }

        //    if (WSDatabaseHelper.isValidField(reader, "TOTAL_REGISTERED_USERS"))
        //    {
        //        totalRegistredUsers = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("TOTAL_REGISTERED_USERS")));
        //    }

        //    if (WSDatabaseHelper.isValidField(reader, "BEACON_STATUS"))
        //    {
        //        beaconStatus = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("BEACON_STATUS")));
        //    }

        //    if (WSDatabaseHelper.isValidField(reader, "KIOSK_STATUS"))
        //    {
        //        kioskStatus = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("KIOSK_STATUS")));
        //    }

        //    if (WSDatabaseHelper.isValidField(reader, "CATEGORY_ID"))
        //    {
        //        categories = new List<Category>();
        //        categories.Add(new Category(reader));
        //    }

        //    if (WSDatabaseHelper.isValidField(reader, "COUNTRY_ID"))
        //    {
        //        countries = new List<Country>();
        //        countries.Add(new Country(reader));
        //    }
        }

        public override Dictionary<string, object> toDictionary()
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            if (this.id != 0)
                parameters["id"] = this.id;
            if(OfferType == Offer_Type.MEMBER)
                parameters["type"] = 1;
            else if(OfferType == Offer_Type.NON_MEMBER)
                parameters["type"] = 2;
            else if (OfferType == Offer_Type.BOTH)
                parameters["type"] = 3;
            if (title != null)
                parameters["title"] = title;
            if (this.number != null)
                parameters["number"] = this.number;
            if (imagePath != null)
                parameters["imagePath"] = imagePath;
            if (description != null)
                parameters["description"] = description;
            if (startTime != null)
                parameters["startTime"] = startTime;
            if (endTime != null)
                parameters["endTime"] = endTime;
            if (duration != 0)
                parameters["duration"] = duration;
            parameters["account"] = account.id;
            parameters["createdBy"] = createdBy.id;
            parameters["updatedBy"] = updatedBy.id;
            parameters["updatedOn"] = DateTime.Now.ToString();
            parameters["createdOn"] = DateTime.Now.ToString();
            return parameters;
        }

        public override void save()
        {
            throw new NotImplementedException();
        }

        public override void update()
        {
            throw new NotImplementedException();
        }
    }
}