﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WSDatabase
{
    public class WSStoredProcedure
    {      

        public WSStoredProcedure(string name, Dictionary<string, WSStoredProcedureParameter> parameters)
        {
            this.name = name;
            this.parameters = parameters;
        }

        public String name { get; set; }
       
        public Dictionary<String, WSStoredProcedureParameter> parameters { get; set; }
    }
}