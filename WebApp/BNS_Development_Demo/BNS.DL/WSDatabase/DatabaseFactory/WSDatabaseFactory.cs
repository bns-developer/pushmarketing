﻿
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace WSDatabase
{
    public class WSDatabaseFactory
    {
        static readonly DbProviderFactory dbProviderFactory = DbProviderFactories.GetFactory("System.Data.SqlClient");

        public static Database CreateDatabase(string connectionString)
        {
            return new GenericDatabase(connectionString, dbProviderFactory);
        }
    }
}