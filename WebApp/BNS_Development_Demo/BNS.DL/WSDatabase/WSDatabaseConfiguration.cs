﻿using System.Configuration;

namespace WSDatabase
{
    public static class WSDatabaseConfiguration
    {
        public const string CONNECTION_NAME = "DefaultConnection";
        public static string CONNECTION_STRING = ConfigurationManager.ConnectionStrings[WSDatabaseConfiguration.CONNECTION_NAME].ToString();
        public const int DEFAULT_NUMBER_OF_RECORDS = 10;
        public const bool DEFAULT_SORT_ORDER = true; //  true = ASCENDING, false =DESCENDING
        public const string DEFAULT_SORT_FIELD = "createdOn"; //  true = ASCENDING, false =DESCENDING
        public const int DEFAULT_OFFEST = 0;
        public const int DEFAULT_PAGE = 1;
    }
}