﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace WSDatabase
{
    public class WSSort
    {
        private JObject sort;
        public string Direction { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public WSRecordsFilter filter { get; set; }
       

        public WSSort()
        {
        }

        public WSSort(JObject sort)
        {
            this.sort = sort;
        }

        void setDefault()
        {
            Direction = "ASC";
            Name = "CreatedOn";
        }
        public Dictionary<string, object> toDictionary()
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            this.setDefault();
            parameters["Direction"] = this.Direction;
            parameters["Name"] = this.Name;
            return parameters;
        }
        public string toQuery()
        {
            this.setDefault();
            String query = "";
            if (!String.IsNullOrEmpty(this.Alias))
                query =  "ORDER BY "+ this.Alias +"." + this.Name + " " + this.Direction;
            else
                query = "ORDER BY "+this.filter.Alias + "." + this.Name + " " + this.Direction;
            return query;
        }
    }
}