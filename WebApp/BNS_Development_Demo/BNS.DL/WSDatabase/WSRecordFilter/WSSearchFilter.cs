﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace WSDatabase
{
    public class WSSearchFilter
    {        
        public List<string> Fields { get; set; }
        public string Key { get; set; }
        public string Operator { get; set; }
        public WSRecordsFilter filter { get;  set; }

       

        public WSSearchFilter(JObject search)
        {
            if (JSONHelper.isValidFieldForKey(search, "Fields"))
            {
                this.Fields = new List<string>();
                JArray fields = (JArray)search["Fields"];
                foreach(JObject field in fields)
                {
                    this.Fields.Add((String)field);
                }
            }
            

            if (JSONHelper.isValidFieldForKey(search, "Key"))
                this.Key = (string)search["Key"];

            if (JSONHelper.isValidFieldForKey(search, "Operator"))
                this.Operator =(string) search["Operator"];
        }

        void setDefault()
        {

        }
        public Dictionary<string, object> toDictionary()
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            this.setDefault();
            return parameters;
        }
        public string toQuery()
        {
            this.setDefault();
            String query = "";
            return query;
        }
    }
}