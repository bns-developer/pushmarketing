﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace WSDatabase
{
    public static class NumberFilterOperators
    {
        public static string greaterThan { get { return " > "; } }
        public static string greaterThanOrEqual { get { return " >= "; } }
        public static string lessThan { get { return " < "; } }
        public static string lessThanOrEqual { get { return " >= "; } }
        public static string equal { get { return " = "; } }
    }
    public static class StringFilterOperators
    {
        public static string startsWith { get { return " LIKE @keyword% "; } }
        public static string endsWith { get { return " LIKE %@keyword "; } }
        public static string contains { get { return " LIKE '%' + @keyword + '%'"; } }
        public static string equal { get { return "="; } }
        public static string notEqual { get { return "!="; } }
    }
    public static class BooleanFilterOperators
    {
        public static int equal { get { return 1; } }
        public static int notEqual { get { return 0; } }
    }
    public static class DateFilterOperators
    {
        public static string greaterThan { get { return " > "; } }
        public static string greaterThanOrEqual { get { return " >= "; } }
        public static string lessThan { get { return " < "; } }
        public static string lessThanOrEqual { get { return " >= "; } }
        public static string equal { get { return " = "; } }
    }

    public class WSWhereFilter
    {

        public string Alias { get; set; }
        public string Condition { get; set; }
        public string Field { get; set; }
        public Boolean? IgnoreCase { get; set; }
        public Boolean? IsNumber { get; set; }
        public Boolean? IsComplex { get; set; }
        public string Operator { get; set; }
        public List<WSWhereFilter> predicates { get; set; }
        public object value { get; set; }
        public WSRecordsFilter filter { get; set; }
      

        public WSWhereFilter(JObject requestJSON)
        {
            if (JSONHelper.isValidFieldForKey(requestJSON, "value"))            
                this.value = (String)requestJSON["value"];

            if (JSONHelper.isValidFieldForKey(requestJSON, "Field"))
                this.Field = (String) requestJSON["Field"];

            if (JSONHelper.isValidFieldForKey(requestJSON, "Operator"))
                this.Operator = (String)requestJSON["Operator"];

            if (JSONHelper.isValidFieldForKey(requestJSON, "IgnoreCase"))
                this.IgnoreCase = (Boolean)requestJSON["IgnoreCase"];

            if (JSONHelper.isValidFieldForKey(requestJSON, "Condition"))
                this.Condition = (String)requestJSON["Condition"];
        }

        void setDefault()
        {
            if (String.IsNullOrEmpty(this.Field))
                this.Field = "";

            if (String.IsNullOrEmpty(this.Operator))
                this.Operator = "=";

            if (this.value ==null)
                this.value = "";

            if (String.IsNullOrEmpty(this.Field))
                this.IgnoreCase = false;

            if (String.IsNullOrEmpty(this.Field))
                this.Condition = "";

            if (this.IsNumber == null)
                this.IsNumber = false;

            if (this.IsComplex == null)
                this.IsComplex = false;
        }
        public Dictionary<string, object> toDictionary()
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            this.setDefault();
            return parameters;
        }
        public string toQuery()
        {            
            this.setDefault();
            if (String.IsNullOrEmpty(this.Field) && this.value ==null)
            {
                return "";
            }
            else
            {
                if((bool)this.IsNumber)
                     return this.Alias +"."+ this.Field + " " + this.Operator + " " + this.value + " " + this.IgnoreCase;
                else
                    return this.Alias + "." + this.Field + " " + this.Operator + " '" + this.value + "' " + this.IgnoreCase;

            }
                      
        }
    }
}