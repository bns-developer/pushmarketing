﻿
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Threading.Tasks;


namespace WSDatabase
{
    public class WSDatabaseManager
    {

        private static WSDatabaseManager instance;
        public static SqlConnection Connection { get; set; }

        string ConString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
        private WSDatabaseManager()
        {
            //  connectToDatabase();
        }



        public static WSDatabaseManager sharedDatabaseManagerinstance
        {
            get
            {
                if (instance == null)
                {
                    instance = new WSDatabaseManager();
                }
                return instance;
            }
        }

        private void connectToDatabase()
        {

            Connection = new SqlConnection(ConString);
            Connection.Open();
        }

        public Boolean login(Dictionary<String, Object> queryParameter)
        {
            WSQuery query = new WSQuery("SELECT CASE WHEN EXISTS (SELECT* FROM Users WHERE email = @email AND password = @password) THEN 1 ELSE 0 END AS Result");
            return Convert.ToBoolean(this.executeScalar(query));

        }
        public Boolean insertRecordIntoDatabase(String query, Dictionary<String, Object> queryParameter)
        {
            SqlCommand command = new SqlCommand(query, Connection);

            foreach (String key in queryParameter.Keys)
            {
                object obj = (queryParameter[key]);
                if (this.isPrimptive(obj))
                    command.Parameters.AddWithValue(("@" + key), obj);
            }
            //logger.log(query + "  Fired on Database.");
            int rows_affected = command.ExecuteNonQuery();
            if (rows_affected == 0)
                return false;
            else
                return true;
        }

        public Int64 insertRecordIntoDatabaseIfNotExistAndReturnID(String query, Dictionary<String, Object> queryParameter)
        {
            //String query = "IF EXISTS (SELECT * FROM Session WHERE sessionUser = @sessionUser) UPDATE Session SET expiryTime = @expiryTime WHERE sessionUser = @sessionUser  ELSE INSERT INTO Session (sessionUser,sessionKey,expiryTime) VALUES (@sessionUser,@sessionKey,@expiryTime)  ";
            using (SqlConnection connection = new SqlConnection(ConString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);

                foreach (String key in queryParameter.Keys)
                {
                    object obj = (queryParameter[key]);
                    if (this.isPrimptive(obj))
                        command.Parameters.AddWithValue(("@" + key), obj);
                }
                //logger.log(query + "  Fired on Database.");
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        return reader.GetInt64(reader.GetOrdinal("ID"));
                    }
                    return 0;
                }
                else
                {
                    return 0;
                }
            }


        }



        public Int64 insertRecordIntoDatabaseAndReturnID(String query, Dictionary<String, Object> queryParameter)
        {
            using (SqlConnection connection = new SqlConnection(ConString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);

                //logger.log(query + "  Fired on Database.");
                foreach (String key in queryParameter.Keys)
                {
                    object obj = (queryParameter[key]);
                    if (this.isPrimptive(obj))
                        command.Parameters.AddWithValue(("@" + key), obj);
                }
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        return reader.GetInt64(reader.GetOrdinal("ID"));
                    }
                    return 0;
                }
                else
                {
                    return 0;
                }
            }

        }



        public Boolean updateRecordsInDatabase(String query, Dictionary<String, Object> queryParameter)
        {
            using (SqlConnection connection = new SqlConnection(ConString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                //command.Transaction = transaction;
                foreach (String key in queryParameter.Keys)
                {
                    object obj = (queryParameter[key]);
                    if (this.isPrimptive(obj))
                        command.Parameters.AddWithValue(("@" + key), obj);
                }

                //logger.log(query + "  Fired on Database.");
                int rows_affected = command.ExecuteNonQuery();
                if (rows_affected == 0)
                    return false;
                else
                    return true;
            }

        }
        public Boolean deleteRecordsFromDatabase(String query, Dictionary<String, Object> queryParameter)
        {
            using (SqlConnection connection = new SqlConnection(ConString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                foreach (String key in queryParameter.Keys)
                {
                    object obj = (queryParameter[key]);
                    if (this.isPrimptive(obj))
                        command.Parameters.AddWithValue(("@" + key), obj);
                }


                try
                {
                    int rows_affected = command.ExecuteNonQuery();
                    if (rows_affected == 0)
                        return false;
                    else
                        return true;
                }
                catch (Exception e)
                {
                    Console.Write(e);
                    return false;

                }
            }


        }
        public SqlDataReader getRecordsFromDatabase(String query, Dictionary<String, Object> queryParameter, SqlConnection connection)
        {
            connection.Open();
            SqlCommand command = new SqlCommand(query, connection);

            foreach (String key in queryParameter.Keys)
            {
                object obj = (queryParameter[key]);
                if (this.isPrimptive(obj))
                    command.Parameters.AddWithValue(("@" + key), obj);
            }
            //logger.log(query + "  Fired on Database.");
            SqlDataReader reader = command.ExecuteReader();
            return reader;
        }
        public SqlDataReader getRecordsFromDatabase(String query, Dictionary<String, Object> queryParameter)
        {
            if (Connection == null)
            {
                Connection = new SqlConnection(connectionString: ConString);
                Connection.Open();
            }

            SqlCommand command = new SqlCommand(query, Connection);

            foreach (String key in queryParameter.Keys)
            {
                object obj = (queryParameter[key]);
                if (this.isPrimptive(obj))
                    command.Parameters.AddWithValue(("@" + key), obj);
            }

            SqlDataReader reader = command.ExecuteReader();
            return reader;
        }
        

        public Object getValueFromDatabase(String query, Dictionary<String, Object> queryParameter)
        {
            SqlCommand command = new SqlCommand(query, Connection);
            foreach (String key in queryParameter.Keys)
            {
                String paramkey = "@" + key;
                command.Parameters.AddWithValue(paramkey, queryParameter[key]);
            }
            //logger.log(query + "  Fired on Database.");
            Object result = command.ExecuteScalar();
            return result;
        }
        public bool isExistInDatabase(String query, Dictionary<String, Object> queryParameter)
        {
            using (SqlConnection connection = new SqlConnection(ConString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                foreach (String key in queryParameter.Keys)
                {
                    String paramkey = "@" + key;
                    command.Parameters.AddWithValue(paramkey, queryParameter[key]);
                }
                //logger.log(query + "  Fired on Database.");
                Object result = command.ExecuteScalar();
                return (bool)result;
            }


        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        private bool isPrimptive(object obj)
        {
            if (this.IsList(obj) || this.IsDictionary(obj))
                return false;

            return true;

        }

        public bool IsList(object o)
        {
            if (o == null) return false;
            return o is IList &&
                   o.GetType().IsGenericType &&
                   o.GetType().GetGenericTypeDefinition().IsAssignableFrom(typeof(List<>));
        }

        public bool IsDictionary(object o)
        {
            if (o == null) return false;
            return o is IDictionary &&
                   o.GetType().IsGenericType &&
                   o.GetType().GetGenericTypeDefinition().IsAssignableFrom(typeof(Dictionary<,>));
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////                                       NEW METHODS                                             ////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public SqlDataReader executeSelectQuery(WSQuery query)
        {
            query.connection = new SqlConnection(ConString);
            query.connection.Open();
            SqlCommand command = new SqlCommand(query.query, query.connection);
            foreach (String key in query.parameters.Keys)
            {
                String paramkey = "@" + key;
                command.Parameters.AddWithValue(paramkey, query.parameters[key]);
            }
            SqlDataReader reader = command.ExecuteReader();
            return reader;
        }
        public DataSet executeSelectQueryAndReturnDataSet(WSQuery query)
        {
            using (SqlConnection connection = new SqlConnection(ConString))
            {
                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = query.query;
                foreach (String key in query.parameters.Keys)
                {
                    String paramkey = "@" + key;
                    command.Parameters.AddWithValue(paramkey, query.parameters[key]);
                }

                dataAdapter.SelectCommand = command;
                DataSet dataSet = new DataSet();
                connection.Open();
                dataAdapter.Fill(dataSet);
                connection.Close();
                return dataSet;
            }
        }
        public object executeScalar(WSQuery query)
        {
            SqlCommand command;
            if (query.transaction != null)
            {
                command = new SqlCommand(query.query, query.transaction.connection, query.transaction.transaction);
            }
            else
            {
                query.connection = new SqlConnection(ConString);
                if (query.connection.State != ConnectionState.Open)
                    query.connection.Open();
                command = new SqlCommand(query.query, query.connection);
            }
            foreach (String key in query.parameters.Keys)
            {
                String paramkey = "@" + key;
                command.Parameters.AddWithValue(paramkey, query.parameters[key]);
            }
            object response = command.ExecuteScalar();
            command.Connection.Close();
            return response;
        }
        public bool executeUpdateQuery(WSQuery query)
        {
            if (query.transaction != null)
            {
                SqlCommand command = new SqlCommand(query.query, query.transaction.connection, query.transaction.transaction);
                command.Transaction = query.transaction.transaction;
                foreach (String key in query.parameters.Keys)
                {
                    String paramkey = "@" + key;
                    command.Parameters.AddWithValue(paramkey, query.parameters[key]);
                }
                return command.ExecuteNonQuery() != 0;
            }
            else
            {
                SqlConnection tempConnection = new SqlConnection(ConString);
                tempConnection.Open();
                SqlCommand command = new SqlCommand(query.query, tempConnection);
                foreach (String key in query.parameters.Keys)
                {
                    String paramkey = "@" + key;
                    command.Parameters.AddWithValue(paramkey, query.parameters[key]);
                }
                int rows_affected = command.ExecuteNonQuery();
                tempConnection.Close();
                return rows_affected != 0;
            }
        }
        public bool executeDeleteQuery(WSQuery query)
        {
            if (query.transaction != null)
            {
                SqlCommand command = new SqlCommand(query.query, query.transaction.connection, query.transaction.transaction);
                command.Transaction = query.transaction.transaction;
                foreach (String key in query.parameters.Keys)
                {
                    String paramkey = "@" + key;
                    command.Parameters.AddWithValue(paramkey, query.parameters[key]);
                }
                return command.ExecuteNonQuery() != 0;
            }
            else
            {
                SqlConnection tempConnection = new SqlConnection(ConString);
                tempConnection.Open();
                SqlCommand command = new SqlCommand(query.query, tempConnection);
                foreach (String key in query.parameters.Keys)
                {
                    String paramkey = "@" + key;
                    command.Parameters.AddWithValue(paramkey, query.parameters[key]);
                }
                int rows_affected = command.ExecuteNonQuery();
                tempConnection.Close();
                return rows_affected != 0;
            }
        }
        public Boolean executeInsertQuery(WSQuery query)
        {
            if (query.transaction != null)
            {
                SqlCommand command = new SqlCommand(query.query, query.transaction.connection, query.transaction.transaction);
                command.Transaction = query.transaction.transaction;
                foreach (String key in query.parameters.Keys)
                {
                    String paramkey = "@" + key;
                    command.Parameters.AddWithValue(paramkey, query.parameters[key]);
                }
                return command.ExecuteNonQuery() != 0;
            }
            else
            {
                SqlConnection tempConnection = new SqlConnection(ConString);
                tempConnection.Open();
                SqlCommand command = new SqlCommand(query.query, tempConnection);
                foreach (String key in query.parameters.Keys)
                {
                    String paramkey = "@" + key;
                    command.Parameters.AddWithValue(paramkey, query.parameters[key]);
                }
                int rows_affected = command.ExecuteNonQuery();
                tempConnection.Close();
                return rows_affected != 0;
            }
        }
        public Int64 executeInsertQueryReturnID(WSQuery query)
        {
            Int64 response = 0;
            if (query.transaction != null)
            {

                SqlCommand command = new SqlCommand(query.query, query.transaction.connection, query.transaction.transaction);
                foreach (String key in query.parameters.Keys)
                {
                    String paramkey = "@" + key;
                    command.Parameters.AddWithValue(paramkey, query.parameters[key]);
                }
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        response = reader.GetInt64(reader.GetOrdinal("ID"));
                    }
                    reader.Close();
                }
                return response;
            }
            else
            {
                SqlConnection tempConnection = new SqlConnection(ConString);
                tempConnection.Open();
                SqlCommand command = new SqlCommand(query.query, tempConnection);
                foreach (String key in query.parameters.Keys)
                {
                    String paramkey = "@" + key;
                    command.Parameters.AddWithValue(paramkey, query.parameters[key]);
                }
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        response = reader.GetInt64(reader.GetOrdinal("ID"));
                        tempConnection.Close();
                        return response;
                    }
                    return response;
                }
                else
                {
                    return response;
                }
            }
        }
        public Int64 executeInsertOrUpdateQueryReturnID(WSQuery query)
        {
            SqlCommand command = null;
            if (query.transaction != null)
            {
                command = new SqlCommand(query.query, query.transaction.connection, query.transaction.transaction);
            }
            else
            {
                SqlConnection tempConnection = new SqlConnection(ConString);
                tempConnection.Open();
                command = new SqlCommand(query.query, tempConnection);
            }
            foreach (String key in query.parameters.Keys)
            {
                String paramkey = "@" + key;
                command.Parameters.AddWithValue(paramkey, query.parameters[key]);
            }
            return command.ExecuteNonQuery();
        }
        public DataSet executeSelectStoredProcedure(WSStoredProcedure procedure)
        {
            Database database = WSDatabaseFactory.CreateDatabase(ConString);
            using (DbCommand command = database.GetStoredProcCommand(procedure.name))
            {
                foreach (String key in procedure.parameters.Keys)
                {
                    String paramkey = "@" + key;
                    WSStoredProcedureParameter param = procedure.parameters[key];
                    database.AddInParameter(command, paramkey, param.dbType, param.value);
                }
                return database.ExecuteDataSet(command);
            }
        }
    }
}