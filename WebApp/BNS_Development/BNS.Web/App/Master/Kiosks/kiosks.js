angular.module('bns.master.kiosks', [])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.kiosks', {
        url: '/kiosks',
        views: {
            "kiosks": {
                templateUrl: 'App/Master/Kiosks/kiosks.html',
                controller: 'kiosksController'
            }
        }
    });
}])
.controller("kiosksController", ['$scope', '$http', '$window', 'API_URL', 'LOADING_MESSAGE', 'ngDialog', 'API_SERVICE', '$state', '$timeout', 'DTOptionsBuilder', 'DTColumnDefBuilder',
    function ($scope, $http, $window, API_URL, LOADING_MESSAGE, ngDialog, API_SERVICE, $state, $timeout, DTOptionsBuilder, DTColumnDefBuilder) {
        $scope.deleteButtonName = "Yes";
        $scope.updateButtonName = "Update";
        $scope.loadingMessage = LOADING_MESSAGE;
        $scope.changeBreadcrums([{ "name": "Kiosks", "state": "master.kiosks" }]);

        $scope.editKioskModel = {
            "id": "",
            "name": "",
            "number": "",
            "store": { "id": "" }
        };
        $scope.deleteKioskModel = {
            "id": "",
            "name": "",
            "number": "",
            "store": { "id": "" }
        };
        $scope.dtOptionsKiosks = DTOptionsBuilder.newOptions()
       .withDOM('lrftip')
       .withPaginationType('full_numbers')
       .withOption('responsive', true)
       .withOption('scrollX', true)
       //.withOption('bAutoWidth', false)
       //.withOption('scrollCollapse', 'true')
       .withOption('order', [])
       .withButtons([
           {
               extend: 'csvHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Kiosks ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3, 4, 5, 6]
               }
           },
           {
               extend: 'pdfHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Kiosks ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3, 4, 5, 6]
               }
           },
           {
               extend: 'excelHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Kiosks ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3, 4, 5, 6]
               }
           },
           'print']);
        $scope.dtColumnDefsKiosks = [DTColumnDefBuilder.newColumnDef(7).notSortable()];

        //------------------------ Update beacon modal --------------------------//
        $scope.updateKiosk = function (kiosk) {
            console.log(kiosk);
            $scope.editKioskModel.id = kiosk.id;
            $scope.editKioskModel.name = kiosk.name;
            $scope.editKioskModel.number = kiosk.number;
            $scope.editKioskModel.store.id = kiosk.store.id;
            ngDialog.open({
                template: 'updateKioskDialog',
                className: 'ngdialog-theme-default modal-large beacon-dialog draggable',
                scope: $scope,
                closeByDocument: false,
                preCloseCallback: function () { $scope.clearUpdateModal() }

            });
            $timeout(function () {
                $(".draggable .ngdialog-content").draggable();
            }, 500);
        }

        $scope.closeModal = function () {
            ngDialog.close({
                template: 'updateKioskDialog',
                scope: $scope
            });
            $scope.clearUpdateModal();
        }

        $scope.clearUpdateModal = function () {
            $scope.submitted = false;
            $scope.successAlert = false;
            $scope.faliureAlert = false;
            $scope.editKioskModel = {
                "id": "",
                "name": "",
                "number": "",
                "store": { "id": "" }
            };
        }

        //----------------------- Delete beacon modal -------------------------//

        $scope.deleteKiosk = function (kiosk) {
            console.log(kiosk);
            $scope.deleteKioskModel.id = kiosk.id
            $scope.deleteKioskModel.name = kiosk.name;
            $scope.deleteKioskModel.number = kiosk.number;
            $scope.deleteKioskModel.store.id = kiosk.store.id;
           
            ngDialog.open({
                template: 'deleteKioskDialog',
                className: 'ngdialog-theme-default modal-large store-dialog draggable',
                scope: $scope,
                closeByDocument: false,
                preCloseCallback: function () { $scope.clearDeleteModal() }

            });
            $timeout(function () {
                $(".draggable .ngdialog-content").draggable();
            }, 500);
        }

        $scope.clearDeleteModal = function () {
            $scope.submitted = false;
            $scope.successAlert = false;
            $scope.faliureAlert = false;
            $scope.deleteKioskModel = {
                "id": "",
                "name": "",
                "number": "",
                "store": { "id": "" }
            };
        }

        $scope.closeDeleteModal = function () {
            ngDialog.close({
                template: 'deleteKioskDialog',
                scope: $scope
            });
            $scope.clearDeleteModal();
        }
    //-------------------------------------------------------------------//
        
       
        $scope.deleteKioskAPI = function () {
            console.log("testing");
            $scope.deleteButtonName = "Deleting";
            $scope.submitted = true;
            if ($scope.deleteKioskModel.id == "" || $scope.deleteKioskModel.id == undefined ) {
                $scope.deleteButtonName = "Yes";
            }
            else {
                function success(data, status, headers, config) {
                    getAllKiosksList();
                    $scope.notify("Kiosk with name \"" + $scope.deleteKioskModel.name + "\", successfully deleted from the list.", 1);
                    $scope.submitted = false;
                    $scope.successAlert = true;
                    $scope.faliureAlert = false;
                    $scope.closeModal();
                    $scope.deleteKioskModel = {
                        "id": "",
                        "name": "",
                        "number": "",
                        "store": { "id": "" }
                    };
                    $scope.loading = false;
                    $scope.deleteButtonName = "Yes";
                }
                function failure(data, status, headers, config) {
                    $scope.loading = false;
                    $scope.successAlert = false;
                    $scope.faliureAlert = true;
                    $scope.deleteButtonName = "Yes";
                    if (status == 409) {
                        $scope.errorMessage = data;
                    }
                    else if (status == 500) {
                        $scope.errorMessage = data;
                    }
                    else {
                        $scope.errorMessage = "Could not delete kiosk."
                    }
                }
                var url = "/kiosks/delete";
                API_SERVICE.postData($scope, $http, $scope.deleteKioskModel, url, success, failure);
            }
        }

        $scope.updateKioskAPI = function () {
            $scope.updateButtonName = "Updating";
            $scope.submitted = true;
            if ($scope.editKioskModel.id == "" || $scope.editKioskModel.id == undefined || $scope.editKioskModel.name == "" || $scope.editKioskModel.name == undefined || $scope.editKioskModel.number == "" || $scope.editKioskModel.number == undefined || $scope.editKioskModel.store.id == "" || $scope.editKioskModel.store.id == undefined) {
                $scope.updateButtonName = "Update";
            }
            else {
                function success(data, status, headers, config) {
                    getAllKiosksList();

                    $scope.notify("Kiosk with name \"" + $scope.editKioskModel.name + "\" updated successfully.", 1);
                    $scope.submitted = false;
                    $scope.successAlert = true;
                    $scope.faliureAlert = false;
                    $scope.closeModal();
                    $scope.editKioskModel = {
                        "id": "",
                        "name": "",
                        "number": "",
                        "store": { "id": "" }
                    };
                    $scope.loading = false;
                    $scope.updateButtonName = "Update";
                }
                function failure(data, status, headers, config) {
                    $scope.loading = false;
                    $scope.successAlert = false;
                    $scope.faliureAlert = true;
                    $scope.updateButtonName = "Update";
                    if (status == 409) {
                        $scope.errorMessage = data;
                    }
                    else {
                        $scope.errorMessage = "Could not update store. Please check all the fields."
                    }
                }
                var url = "/kiosks/update";
                API_SERVICE.postData($scope, $http, $scope.editKioskModel, url, success, failure);
            }
        }

        function getAllKiosksList() {
            $scope.loading = true;
            $scope.dataLoader = 0;
            function success(data, status, headers, config) {
                $scope.kiosks = data;
                console.log($scope.stores);
                $scope.loading = false;
                $scope.dataLoader = 1;
            }
            function failure(data, status, headers, config) {
                $scope.loading = false;
                $scope.dataLoader = 2;
            }
            var url = "/kiosks/all";
            API_SERVICE.getData($scope, $http, url, success, failure);
        }
        getAllKiosksList();

        function getAllStoresList() {
            $scope.loading = true;
            function success(data, status, headers, config) {
                $scope.stores = data;
                console.log($scope.stores);
                $scope.loading = false;
            }
            function failure(data, status, headers, config) {
                $scope.loading = false;
            }
            var url = "/stores/all";
            API_SERVICE.getData($scope, $http, url, success, failure);
        }
        getAllStoresList();

        $scope.goLuckyCharacters = function (kioskId) {
            $state.go("master.luckyCharacters", { "kioskId": kioskId });
        }

        $scope.goKioskDetails = function (kioskId) {
            $state.go('master.kioskDetails', { 'kioskId': kioskId});
        }

        $scope.reloadKiosksList = function () {
            getAllKiosksList();
        }

    }]);