﻿angular.module('bns.master.offersCategories', [])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.offersCategories', {
        url: '/offerCategories',
        views: {
            "offersCategories": {
                templateUrl: 'App/Master/OffersCategories/OffersCategories.html',
                controller: 'offersCategoriesController'
            }
        }
    });
}])
.controller("offersCategoriesController", ['$scope', '$http', '$window', 'API_URL', 'LOADING_MESSAGE', 'API_SERVICE', '$state', '$timeout', 'ngDialog', 'DTOptionsBuilder', 'DTColumnDefBuilder',
function ($scope, $http, $window, API_URL, LOADING_MESSAGE, API_SERVICE, $state, $timeout, ngDialog, DTOptionsBuilder, DTColumnDefBuilder) {

        $scope.changeBreadcrums([{ "name": "Offers Categories", "state": "master.offersCategories" }]);
        $scope.saveButtonName = 'Add';
        $scope.submitted = false;
        $scope.editButtonName = 'Update';
        $scope.notDeleted = true;

        $scope.dtOptionsOffersCategories = DTOptionsBuilder.newOptions()
       .withDOM('lrftip')
       .withPaginationType('full_numbers')
       .withOption('responsive', true)
       .withOption('scrollX', true)
       //.withOption('bAutoWidth', false)
       //.withOption('scrollCollapse', 'true')
       .withOption('order', [])
       .withButtons([
           {
               extend: 'csvHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Offers Categories ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3, 4]
               }
           },
           {
               extend: 'pdfHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Offers Categories ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3, 4]
               }
           },
           {
               extend: 'excelHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Offers Categories ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3, 4]
               }
           },
           'print']);
        $scope.dtColumnDefsOfferCategories = [DTColumnDefBuilder.newColumnDef(5).notSortable()];

        $scope.newCategory = {
            'name': '',
            'type': ''
        };

        $scope.categoryToEdit = {
            'name': '',
            'type': ''
        };

        $scope.categoryToView = {
            'name': '',
            'type': ''
        };

        $scope.categoryToDelete = {
            'name': '',
            'type': ''
        };

        $scope.categories = [
            //{
            //    "id": 1,
            //    "name": "Cash",
            //    "is_currency": 1,
            //    "type": 1,
            //    "activeOn": "3 Offers"
            //},
            //{
            //    "id": 2,
            //    "name": "Promotions",
            //    "is_currency": 2,
            //    "type": 2,
            //    "activeOn": "2 Offers"
            //},
            //{
            //    "id": 3,
            //    "name": "Point",
            //    "is_currency": 2,
            //    "type": 1,
            //    "activeOn": "6 Offers"
            //},
            //{
            //    "id": 4,
            //    "name": "Cash",
            //    "is_currency": 1,
            //    "type": 1,
            //    "activeOn": "4 Offers"
            //},
            //{
            //    "id": 5,
            //    "name": "Dinner",
            //    "is_currency": 2,
            //    "type": 2,
            //    "activeOn": "1 Offers"
            //},
            //{
            //    "id": 6,
            //    "name": "Chips",
            //    "is_currency": 2,
            //    "type": 1,
            //    "activeOn": "5 Offers"
            //}
        ];

        $scope.getCategoriesList = function () {
            function success(data, status, headers, config) {
                console.log(data);
                if (data.length == 0) {
                    $scope.dataLoader = 2;
                } else {
                    $scope.categories = data;
                    $scope.dataLoader = 1;
                }
            }
            function failure(data, status, headers, config) {
                console.log("Failed");
                $scope.dataLoader = 3;
            }
            var url = "/Categories/list";
            API_SERVICE.getData($scope, $http, url, success, failure);
        };
        $scope.getCategoriesList();

        $scope.addCategories = function () {
            ngDialog.open({
                template: 'addCategoryDialog',
                className: 'ngdialog-theme-default modal-large store-dialog draggable',
                scope: $scope,
                preCloseCallback: function () { $scope.clearModal() }
            });
            $timeout(function () {
                $(".draggable .ngdialog-content").draggable();
            }, 500);
        };

        $scope.saveNewCategory = function (isValid) {
            $scope.submitted = true;
            if (isValid) {
                if ($scope.newCategory.is_currency == true)
                {
                    $scope.newCategory.is_currency = 1;
                } else {
                    $scope.newCategory.is_currency = 2;
                }
                console.log($scope.newCategory);
                $scope.saveButtonName = 'Adding';

                function success(data, status, headers, config) {
                    $scope.notify("New category with name \"" + $scope.newCategory.name + "\", successfully added to the list.", 1);
                    $scope.submitted = false;
                    $scope.successAlert = true;
                    $scope.faliureAlert = false;
                    $scope.newCategory = {
                        'name': '',
                        'type': ''
                    };
                    $scope.loading = false;
                    $scope.saveButtonName = 'Add';
                    $scope.getCategoriesList();
                    $scope.closeModal();
                }
                function failure(data, status, headers, config) {
                    $scope.loading = false;
                    $scope.saveButtonName = 'Add';
                    $scope.successAlert = false;
                    $scope.faliureAlert = true;
                    if (status == 409) {
                        $scope.errorMessage = data;
                    }
                    else {
                        $scope.errorMessage = "Could not add term/condition to the list. Please check all the fields."
                        $scope.saveButtonName = 'Add';
                    }
                }
                var url = "/Categories/save";
                API_SERVICE.postData($scope, $http, $scope.newCategory, url, success, failure);

            }
        };

        $scope.clearModal = function () {
            $scope.submitted = false;
            $scope.newCategory = {
                'name': '',
                'type': ''
            };
            $scope.successAlert = false;
            $scope.faliureAlert = false;
            $scope.saveButtonName = 'Add';
        };

        $scope.closeModal = function () {
            ngDialog.close({
                template: 'addCategoryDialog',
                scope: $scope
            });
            $scope.clearModal();
        };

        $scope.viewCategory = function (category) {
            $scope.categoryToView.name = category.name;
            if (category.CategoryUnit == 'CURRENCY') {
                $scope.categoryToView.is_currency = 1;
            } else {
                $scope.categoryToView.is_currency = 2;
            }

            if (category.CategoryType == 'NUMBER') {
                $scope.categoryToView.type = 1;
            } else {
                $scope.categoryToView.type = 2;
            }

            $scope.categoryToView.activeOn = category.activeOn;

            ngDialog.open({
                template: 'viewCategoryDialog',
                className: 'ngdialog-theme-default modal-large beacon-dialog draggable',
                scope: $scope,
            });
            $timeout(function () {
                $(".draggable .ngdialog-content").draggable();
            }, 500);
        };

        $scope.editCategory = function (category) {
            $scope.categoryToEdit.name = category.name;
            $scope.categoryToEdit.type = category.type;
            if (category.CategoryType == 'NUMBER')
            {
                $scope.categoryToEdit.type = 1;
            } else {
                $scope.categoryToEdit.type = 2;
            }
            if (category.CategoryUnit == 'OTHER')
            {
                $scope.categoryToEdit.is_currency = false;
            } else {
                $scope.categoryToEdit.is_currency = true;
            }
            $scope.categoryToEdit.id = category.id;
            ngDialog.open({
                template: 'editCategoryDialog',
                className: 'ngdialog-theme-default modal-large beacon-dialog draggable',
                scope: $scope,
                preCloseCallback: function () { $scope.clearModal() }
            });
            $timeout(function () {
                $(".draggable .ngdialog-content").draggable();
            }, 500);
        };

        $scope.saveEditedCategory = function (isValid) {
            $scope.submitted = true;
            if (isValid) {
                if ($scope.categoryToEdit.is_currency == true) {
                    $scope.categoryToEdit.is_currency = 1;
                } else {
                    $scope.categoryToEdit.is_currency = 2;
                }

                function success(data, status, headers, config) {
                   
                    $scope.submitted = false;
                    $scope.successAlert = true;
                    $scope.faliureAlert = false;
                    $scope.newCategory = {
                        'name': '',
                        'type': ''
                    };
                    $scope.loading = false;
                    $scope.editButtonName = 'Update';
                    $scope.getCategoriesList();
                    $scope.notify("Category updated successfully.", 1);
                    $scope.closeModal();
                }
                function failure(data, status, headers, config) {
                    $scope.loading = false;
                    $scope.editButtonName = 'Update';
                    $scope.successAlert = false;
                    $scope.faliureAlert = true;
                    if (status == 409) {
                        $scope.errorMessage = data;
                    }
                    else {
                        $scope.errorMessage = "Could not edit category."
                        $scope.editButtonName = 'Update';
                    }
                }
                var url = "/Categories/edit";
                API_SERVICE.postData($scope, $http, $scope.categoryToEdit, url, success, failure);

            }
            $scope.editButtonName = 'Updating';
        };

        $scope.deleteCategory = function (index) {
            $scope.categoryToDelete = $scope.categories[index];
            if($scope.categories[index].activeOn != 0)
            {
                ngDialog.open({
                    template: 'deleteNotAllowed',
                    className: 'ngdialog-theme-default modal-large store-dialog draggable',
                    scope: $scope
                });
                $timeout(function () {
                    $(".draggable .ngdialog-content").draggable();
                }, 500);
            } else {
                ngDialog.open({
                    template: 'deleteCategoryDialog',
                    className: 'ngdialog-theme-default modal-large store-dialog draggable',
                    scope: $scope,
                    preCloseCallback: function () { $scope.clearDeleteModal() }
                });
                $timeout(function () {
                    $(".draggable .ngdialog-content").draggable();
                }, 500);
            }
        }

        $scope.clearDeleteModal = function () {
            $scope.notDeleted = true;
        }

        $scope.confirmDelete = function () {
            function success(data, status, headers, config) {
                $scope.notDeleted = false;
                $scope.deleteSuccess = true;
                $scope.getCategoriesList();
                $scope.notify("Category deleted from the list.", 1);
                $scope.closeModal();
            }
            function failure(data, status, headers, config) {
                $sccope.deleteSuccess = false;
                if (status == 409) {
                    $scope.errorMessage = data;
                }
                else {
                    $scope.errorMessage = "Could not delete category."
                }
            }
            var url = "/Categories/delete";
            API_SERVICE.postData($scope, $http, $scope.categoryToDelete, url, success, failure);
            $scope.notDeleted = true;
            $scope.deleteSuccess = false;
        }

        $scope.closeConfirmModal = function () {
            ngDialog.close({
                template: 'deleteCategoryDialog',
                scope: $scope
            });
            $scope.notDeleted = true;
            $scope.deleteSuccess = true;
        }

    }])