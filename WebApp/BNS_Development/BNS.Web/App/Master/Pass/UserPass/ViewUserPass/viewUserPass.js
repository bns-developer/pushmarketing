﻿angular.module('bns.master.viewUserPass', [])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.viewUserPass', {
        url: '/pass/viewUserPass?passID',
        views: {
            "viewUserPass": {
                templateUrl: 'App/Master/Pass/UserPass/ViewUserPass/viewUserPass.html',
                controller: 'viewUserPassController'
            }
        }
    });
}])

.controller("viewUserPassController", ['$scope', '$state', '$http', '$stateParams', 'API_URL', 'ngDialog', 'API_SERVICE', 'MEDIA_URL', '$timeout', function ($scope, $state, $http, $stateParams, API_URL, ngDialog, API_SERVICE, MEDIA_URL, $timeout) {
    $scope.changeBreadcrums([{ "name": "Pass", "state": "bns.master.pass" }, { "name": "User Pass", "state": "bns.master.pass" }]);
    $scope.passId = $stateParams.passID;
}])