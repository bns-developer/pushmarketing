﻿angular.module('bns.master.editTemplate', [])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.editTemplate', {
        url: '/pass/templates/editTemplate?templateID',
        views: {
            "editTemplate": {
                templateUrl: 'App/Master/Pass/Templates/EditTemplate/editTemplate.html',
                controller: 'editTemplateController'
            }
        }
    });
}])

.controller("editTemplateController", ['$scope', '$state', '$http', '$stateParams', 'API_URL', 'ngDialog', 'API_SERVICE', 'MEDIA_URL', '$timeout', function ($scope, $state, $http, $stateParams, API_URL, ngDialog, API_SERVICE, MEDIA_URL, $timeout) {
    $scope.changeBreadcrums([{ "name": "Pass", "state": "bns.master.pass" }]);
    $scope.templateID = $stateParams.templateID;
    /*  ---------------------- Navigation Function --------------------------  */
    $scope.goBack = function()
    {
        $state.go("master.pass");
    }
    /*  ----------------------  Save Function  ------------------------------- */
    $scope.savePassTemplate = function () {
        $scope.notify("Save Pass Template Module is under progress", 2);
    }
}])