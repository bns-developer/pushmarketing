﻿angular.module('bns.master.viewTemplate', [])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.viewTemplate', {
        url: '/pass/templates/viewTemplate?templateID',
        views: {
            "viewTemplate": {
                templateUrl: 'App/Master/Pass/Templates/ViewTemplate/viewTemplate.html',
                controller: 'viewTemplateController'
            }
        }
    });
}])

.controller("viewTemplateController", ['$scope', '$state', '$http', '$stateParams', 'API_URL', 'ngDialog', 'API_SERVICE', 'MEDIA_URL', '$timeout', function ($scope, $state, $http, $stateParams, API_URL, ngDialog, API_SERVICE, MEDIA_URL, $timeout) {
    $scope.changeBreadcrums([{ "name": "Pass", "state": "bns.master.pass" }]);
    $scope.templateID = $stateParams.templateID;
    /*  ---------------------- Navigation Function --------------------------  */
    $scope.goBack = function()
    {
        $state.go("master.pass");
    }
    $scope.goToEditTemplate = function(templateID)
    {
        $state.go('master.editTemplate', { 'templateID': templateID });
    }
}])