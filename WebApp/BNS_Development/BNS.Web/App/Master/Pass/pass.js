﻿angular.module('bns.master.pass',['oc.lazyLoad'])
    
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.pass', {
        url: '/pass',
        views: {
            "pass": {
                templateUrl: 'App/Master/Pass/pass.html',
                controller: 'passController'
            }
        }
    });
}])
.controller("passController", ['$scope', '$state', '$http', '$window', 'API_URL', 'ngDialog', 'API_SERVICE', 'MEDIA_URL', '$timeout', 'DTOptionsBuilder', 'DTColumnDefBuilder', function ($scope, $state, $http, $window, API_URL, ngDialog, API_SERVICE, MEDIA_URL, $timeout, DTOptionsBuilder, DTColumnDefBuilder) {
    $scope.dataLoader = 1;
    $scope.changeBreadcrums([{ "name": "Pass", "state": "bns.master.pass" }]);

    $scope.dtOptionsUserPassList = DTOptionsBuilder.newOptions()
       .withDOM('lrftip')
       .withPaginationType('full_numbers')
       .withOption('responsive', true)
       .withOption('scrollX', true)
       //.withOption('bAutoWidth', false)
       //.withOption('scrollCollapse', 'true')
       .withOption('order', [])
       .withButtons([
           {
               extend: 'csvHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Users_Member ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3, 4, 5, 6]
               }
           },
           {
               extend: 'pdfHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Users_Member ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3, 4, 5, 6]
               }
           },
           {
               extend: 'excelHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Users_Member ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3, 4, 5, 6]
               }
           },
           'print']);

    $scope.dtOptionsTemplateList = DTOptionsBuilder.newOptions()
       .withDOM('lrftip')
       .withPaginationType('full_numbers')
       .withOption('responsive', true)
       .withOption('scrollX', true)
       //.withOption('bAutoWidth', false)
       //.withOption('scrollCollapse', 'true')
       .withOption('order', [])
       .withButtons([
           {
               extend: 'csvHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Users_Member ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3]
               }
           },
           {
               extend: 'pdfHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Users_Member ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3]
               }
           },
           {
               extend: 'excelHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Users_Member ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3]
               }
           },
           'print']);

    $scope.dtOptionsSystemPassList = DTOptionsBuilder.newOptions()
       .withDOM('lrftip')
       .withPaginationType('full_numbers')
       .withOption('responsive', true)
       .withOption('scrollX', true)
       //.withOption('bAutoWidth', false)
       //.withOption('scrollCollapse', 'true')
       .withOption('order', [])
       .withButtons([
           {
               extend: 'csvHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Users_Member ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3, 4]
               }
           },
           {
               extend: 'pdfHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Users_Member ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3, 4]
               }
           },
           {
               extend: 'excelHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Users_Member ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3, 4]
               }
           },
           'print']);

    $scope.dtColumnDefsUserPassList = [DTColumnDefBuilder.newColumnDef(7).notSortable()];
    $scope.dtColumnDefsTemplateList = [DTColumnDefBuilder.newColumnDef(4).notSortable()];
    $scope.dtColumnDefsSystemPassList = [DTColumnDefBuilder.newColumnDef(5).notSortable()];

    /*  ------------------------------- Add templates and passes ------------------------------  */

    $scope.addSystemPass = function()
    {
        $state.go('master.addSystemPass');
    }

    $scope.addTemplate = function () {
        $state.go('master.addTemplate');
    }

    /*  ------------------------------- Navigation to ------------------------------------------ */
    $scope.userPassDetails = function (userPassID) {
        $state.go('master.viewUserPass', { 'passID': userPassID });
    }
   
    $scope.viewTemplate = function (templateID) {
        $state.go('master.viewTemplate', { 'templateID': templateID });
    }

    $scope.editTemplate = function (templateID) {
        $state.go('master.editTemplate', { 'templateID': templateID });
    }

    $scope.viewSystemPass = function (passID)
    {
        $state.go('master.viewSystemPass', { 'passID': passID });
    }

    $scope.editSystemPass = function (passID) {
        $state.go('master.editSystemPass', { 'passID': passID });
    }

    /*  ------------------------------- Dummy Data user pass list ----------------------------- */
    $scope.userPasses = [
        { "passID": "75HF94HF84", "memberID": "DJVI", "email": "jules@bestnetworksystem.com", "phone": "6735634343", "issuedDate": "10-3-2018", "expiryDate": "24-4-2018" },
        { "passID": "DGE3672D81", "memberID": "DHEI", "email": "harry.p@hw.com", "phone": "8915574656", "issuedDate": "10-3-2018", "expiryDate": "24-4-2018" },
        { "passID": "JGFRT464DH", "memberID": "JDIW", "email": "jsnow@blackcastlelimited.com", "phone": "6735634343", "issuedDate": "10-3-2018", "expiryDate": "24-4-2018" },
        { "passID": "CKLR4Y3646", "memberID": "NVHY", "email": "peter@parkers.com", "phone": "6735634343", "issuedDate": "10-3-2018", "expiryDate": "24-4-2018" },
        { "passID": "WHEOEFI498", "memberID": "QN83", "email": "robert@gmail.com", "phone": "6735634343", "issuedDate": "10-3-2018", "expiryDate": "24-4-2018" },
        { "passID": "94HF8429F5", "memberID": "I3DK", "email": "jsamuels@gmail.com", "phone": "6735634343", "issuedDate": "10-3-2018", "expiryDate": "24-4-2018" },
        { "passID": "ODNVU39573", "memberID": "JD93", "email": "davidtheshaw@ff.com", "phone": "6735634343", "issuedDate": "10-3-2018", "expiryDate": "24-4-2018" },
        { "passID": "0484HF84FR", "memberID": "JDN3", "email": "jules@gmail.com", "phone": "6735634343", "issuedDate": "10-3-2018", "expiryDate": "24-4-2018" },
        { "passID": "85HD4749EY", "memberID": "OIQW", "email": "eisenberg@bestnetworksystem.com", "phone": "6735634343", "issuedDate": "10-3-2018", "expiryDate": "24-4-2018" },
        { "passID": "8473829FBS", "memberID": "A48L", "email": "aryastark@winterfell.com", "phone": "6735634343", "issuedDate": "10-3-2018", "expiryDate": "24-4-2018" },
        { "passID": "HJRBFIWIR7", "memberID": "HR3J", "email": "jules@bestnetworksystem.com", "phone": "6735634343", "issuedDate": "10-3-2018", "expiryDate": "24-4-2018" }

    ];
    /*  ------------------------------- Dummy Data template list ----------------------------- */
    $scope.templates = [
        { "templateID": "DJ74WK", "name": "Carnival Template", "type": "coupon" },
        { "templateID": "JKDF93", "name": "Hurricane Alert Template", "type": "coupon" },
        { "templateID": "PWE983", "name": "Bar Beacon Template", "type": "coupon" },
        { "templateID": "8EJ393", "name": "High Tide Alert Template", "type": "coupon" },
        { "templateID": "NBGSS8", "name": "Novomatic Template", "type": "coupon" }, 
    ];
    /*  ------------------------------- Dummy Data system pass list ----------------------------- */
    $scope.systemPasses = [
        { "passID": "UED9", "name": "Carnival Cruise Pass", "type": "Coupon", "totalPassIssued": "67" },
        { "passID": "DJ49", "name": "Hurricane Alert Pass", "type": "Coupon", "totalPassIssued": "354" },
        { "passID": "PE84", "name": "Bar Beacon Pass", "type": "Coupon", "totalPassIssued": "45" },
        { "passID": "3MC9", "name": "High Tide Alert Pass", "type": "Coupon", "totalPassIssued": "457" },
        { "passID": "D8DE", "name": "Novomatic Pass", "type": "Coupon", "totalPassIssued": "234" }
    ];
}]);