﻿angular.module('bns.master.anonymousUserDetails', [])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.anonymousUserDetails', {
        url: '/users/anonymousUserDetails?userId',
        views: {
            "anonymousUserDetails": {
                templateUrl: 'App/Master/Users/AnonymousUserDetails/anonymousUserDetails.html',
                controller: 'anonymousUserDetailsController'
            }
        }
    });
}])
.controller("anonymousUserDetailsController", ['$scope', '$http', '$window', 'API_URL', 'LOADING_MESSAGE', 'ngDialog', 'API_SERVICE', 'LOADING_MESSAGE', '$timeout', '$stateParams', 'DTOptionsBuilder', 'DTColumnDefBuilder', function ($scope, $http, $window, API_URL, LOADING_MESSAGE, ngDialog, API_SERVICE, LOADING_MESSAGE, $timeout, $stateParams, DTOptionsBuilder, DTColumnDefBuilder) {
   
    $scope.changeBreadcrums([{ "name": "Users", "state": "master.users" }, { "name": "Anonymous User", "state": "master.anonymousUserDetails" }]);
    $scope.loadingMessage = LOADING_MESSAGE;
    function getUserActivities() {
        $scope.tableDataLoader = 0;
        function success(data, status, headers, config) {
            $scope.userActivities = data;
            $scope.tableDataLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.tableDataLoader = 2;
        }
        var url = '/users/getAnnonymousActivities?userId=' + $stateParams.userId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getUserActivities();
    $scope.dtColumnDefsNonMemberUserList = [DTColumnDefBuilder.newColumnDef(5).notSortable()];

    $scope.dtOptionsNonMemberDetails = DTOptionsBuilder.newOptions()
      .withDOM('lrftip')
      .withPaginationType('full_numbers')
      .withOption('responsive', true)
      .withOption('scrollX', true)
      //.withOption('bAutoWidth', false)
      //.withOption('scrollCollapse', 'true')
      .withOption('order', [])
      .withButtons([
          {
              extend: 'csvHtml5',
              orientation: 'landscape',
              title: $window.document.title + ' :- Non-Member Activities ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
              pageSize: 'LEGAL',
              exportOptions: {
                  columns: [0, 1, 2, 3, 4, 5]
              }
          },
          {
              extend: 'pdfHtml5',
              orientation: 'landscape',
              title: $window.document.title + ' :- Non-Member Activities ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
              pageSize: 'LEGAL',
              exportOptions: {
                  columns: [0, 1, 2, 3, 4, 5]
              }
          },
          {
              extend: 'excelHtml5',
              orientation: 'landscape',
              title: $window.document.title + ' :- Non-Member Activities ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
              pageSize: 'LEGAL',
              exportOptions: {
                  columns: [0, 1, 2, 3, 4, 5]
              }
          },
          'print']);

   
    $scope.reloadUserActivities = function () {
        getUserActivities();
    }

    
}]);