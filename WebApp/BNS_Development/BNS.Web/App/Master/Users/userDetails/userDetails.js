angular.module('bns.master.userDetails', [])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.userDetails', {
        url: '/users/userDetails?userId',
        views: {
            "userDetails": {
                templateUrl: 'App/Master/Users/userDetails/userDetails.html',
                controller: 'userDetailsController'
            }
        }
    });
}])
.controller("userDetailsController", ['$scope', '$http', '$window', 'API_URL', 'LOADING_MESSAGE', 'ngDialog', 'API_SERVICE', 'LOADING_MESSAGE', '$timeout', '$stateParams', 'DTOptionsBuilder', 'DTColumnDefBuilder', function ($scope, $http, $window, API_URL, LOADING_MESSAGE, ngDialog, API_SERVICE, LOADING_MESSAGE, $timeout, $stateParams, DTOptionsBuilder, DTColumnDefBuilder) {
    $scope.loadingMessage = LOADING_MESSAGE;

    $scope.dtColumnDefsMemberDetails = [DTColumnDefBuilder.newColumnDef(5).notSortable()];

    function initDtOptions(){
        $scope.dtOptionsMemberDetails = DTOptionsBuilder.newOptions()
       .withDOM('lrftip')
       .withPaginationType('full_numbers')
       .withOption('responsive', true)
       .withOption('scrollX', true)
       //.withOption('bAutoWidth', false)
       //.withOption('scrollCollapse', 'true')
       .withOption('order', [])
       .withButtons([
           {
               extend: 'csvHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Member Activities ' + $scope.userDetail.memberId + " " + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3, 4, 5]
               }
           },
           {
               extend: 'pdfHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Member Activities ' + $scope.userDetail.memberId + " " + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3, 4, 5]
               }
           },
           {
               extend: 'excelHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Member Activities ' + $scope.userDetail.memberId + " " + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3, 4, 5]
               }
           },
           'print']);
    }
    
    function getUserActivities() {     
        $scope.tableDataLoader = 0;
        function success(data, status, headers, config) {          
            $scope.userActivities = data;
            $scope.tableDataLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.tableDataLoader = 2;
        }
        var url = '/users/getUserActivities?userId=' + $stateParams.userId;

        var header = {
            "access-token": "264982hwqudy298344982yrsagad773283e"
        };


        API_SERVICE.getData($scope, $http, url, success, failure, header);
    }
    getUserActivities();

    $scope.reloadUserActivities = function () {
        getUserActivities();
    }

    function getUserDetails() {
        $scope.dataLoader = 0;
        function success(data, status, headers, config) {           
            $scope.userDetail = data;
            initDtOptions();
            $scope.changeBreadcrums([{ "name": "Users", "state": "master.users" }, { name: data.memberId, "state": "master.userDetails" }]);
            $scope.dataLoader = 1;
        }
        function failure(data, status, headers, config) {          
            $scope.dataLoader = 2;
        }
        var url = '/users/getUserDetailsById?userId=' + $stateParams.userId;
        var header = {
            "access-token": "264982hwqudy298344982yrsagad773283e"
        };
        API_SERVICE.getData($scope, $http, url, success, failure, header);
    }
    getUserDetails();

    $scope.reloadUserDetails = function(){
        getUserDetails();
    }

}]);