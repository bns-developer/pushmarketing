angular.module('bns.master.users', [])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.users', {
        url: '/users',
        views: {
            "users": {
                templateUrl: 'App/Master/Users/users.html',
                controller: 'usersController'
            }
        }
    });
}])
.controller("usersController", ['$scope', '$http', '$window', 'API_URL', 'LOADING_MESSAGE', 'API_SERVICE', '$state', 'DTOptionsBuilder', 'DTColumnDefBuilder',
    function ($scope, $http, $window, API_URL, LOADING_MESSAGE, API_SERVICE, $state, DTOptionsBuilder, DTColumnDefBuilder) {

        $scope.loadingMessage = LOADING_MESSAGE;

        $scope.changeBreadcrums([{ "name": "Users", "state": "'master.users" }]);
        $scope.dtOptionsUsersMember = DTOptionsBuilder.newOptions()
       .withDOM('lrftip')
       .withPaginationType('full_numbers')
       .withOption('responsive', true)
       .withOption('scrollX', true)
       //.withOption('bAutoWidth', false)
       //.withOption('scrollCollapse', 'true')
       .withOption('order', [])
       .withButtons([
           {
               extend: 'csvHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Users_Member ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
               }
           },
           {
               extend: 'pdfHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Users_Member ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
               }
           },
           {
               extend: 'excelHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Users_Member ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
               }
           },
           'print']);
        $scope.dtOptionsUsersNonMember = DTOptionsBuilder.newOptions()
       .withDOM('lrftip')
       .withPaginationType('full_numbers')
       .withOption('responsive', true)
       .withOption('scrollX', true)
       //.withOption('bAutoWidth', false)
       //.withOption('scrollCollapse', 'true')
       .withOption('order', [])
       .withButtons([
           {
               extend: 'csvHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Users_NonMember ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3, 4, 5, 6]
               }
           },
           {
               extend: 'pdfHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Users_NonMember ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3, 4, 5, 6]
               }
           },
           {
               extend: 'excelHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Users_NonMember ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3, 4, 5, 6]
               }
           },
           'print']);
        $scope.dtColumnDefsUsers = [DTColumnDefBuilder.newColumnDef(11).notSortable()];
        $scope.dtColumnDefsNonMember = [DTColumnDefBuilder.newColumnDef(7).notSortable()];

        function processData(data) {
            var prevId = -1;
            var prevUserData = {};
            var users = [];
            var userData = {};
            for (var i = 0; i < data.length; i++)
            {
               
                if (prevId == userData.id)
                {
                    if (data[i].contact.ContactType === "PHONE")
                    {
                        userData.phone=(data[i].contact.value);
                    }
                    else if (data[i].contact.ContactType === "EMAIL")
                    {
                        userData.email = (data[i].contact.value);
                    }
                    
                }
                else
                {
                    userData = data[i];

                    //if (data[i].contact.ContactType === "PHONE") {
                    //    userData.phone = (data[i].contact.value);
                    //}
                    //else if (data[i].contact.ContactType === "EMAIL") {
                    //    userData.email = (data[i].contact.value);
                    //}

                    prevId = data[i].id;
                }
                if((i+1)<data.length)
                {
                    if(data[i].id !== data[i+1].id)
                    {
                        users.push(userData);
                        userData = {};
                    }
                }
                else if (i == data.length - 1) {
                    users.push(userData);
                }

               
            }

            return users;
        }

        function getAllUsersList() {
            $scope.dataLoader = 0;
            function success(data, status, headers, config) {
                $scope.users = data;
                $scope.dataLoader = 1;
            }
            function failure(data, status, headers, config) {                
                $scope.dataLoader = 2;
            }
            var url = "/users/all";
            API_SERVICE.getData($scope, $http, url, success, failure);
           
        }
        getAllUsersList();

        function getAllAnonymousUsersList() {
            $scope.dataAnonymousLoader = 0;
            function success(data, status, headers, config) {
                $scope.anonymousUsers = data;
                $scope.dataAnonymousLoader = 1;
            }
            function failure(data, status, headers, config) {              
                $scope.dataAnonymousLoader = 2;
            }
            var url = "/users/getAnnonymous";
            API_SERVICE.getData($scope, $http, url, success, failure);
        }
        getAllAnonymousUsersList();

        $scope.reloadUsersList = function () {
            getAllUsersList();
        }
        $scope.reloadAnonymousUsersList = function () {
            getAllAnonymousUsersList();
        }

        $scope.userDetails = function (userId) {           
            $state.go('master.userDetails', { 'userId': userId });
        }
        $scope.anonymousUserDetails = function (userId) {
            $state.go('master.anonymousUserDetails', { 'userId': userId });
        }

    }])