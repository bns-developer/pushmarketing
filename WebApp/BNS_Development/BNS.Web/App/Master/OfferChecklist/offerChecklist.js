﻿angular.module('bns.master.offerChecklist', ['oc.lazyLoad'])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.offerChecklist', {
        url: '/offerChecklist',
        views: {
            "offerChecklist": {
                templateUrl: 'App/Master/OfferChecklist/offerChecklist.html',
                controller: 'offerChecklistController'
            }
        }
    });
}])
.controller("offerChecklistController", ['$scope', '$http', '$timeout', '$window', 'API_URL', 'API_SERVICE', 'MEDIA_URL', 'ngDialog', 'LOADING_MESSAGE', 'DTOptionsBuilder', 'DTColumnDefBuilder', function ($scope, $http, $timeout, $window, API_URL, API_SERVICE, MEDIA_URL, ngDialog, LOADING_MESSAGE, DTOptionsBuilder, DTColumnDefBuilder) {
    $scope.flagCouponButtonName = "Flag";
    $scope.redeemCouponButtonName = "Redeem";
    $scope.dataLoader = 1;
    $scope.loadingMessage = LOADING_MESSAGE;
    $scope.submitted = false;
    $scope.successAlert = false;
    $scope.faliureAlert = false;
    $scope.isFlagged = false;
    $scope.isRedeemed = false;
    $scope.isRedeemedAlert = false;
    $scope.offerCode = {
        "code" : ""
    };

    $scope.dtOptionsCouponChecklist = DTOptionsBuilder.newOptions()
       .withDOM('lrftip')
       .withPaginationType('full_numbers')
       .withOption('responsive', true)
       .withOption('scrollX', true)
       //.withOption('bAutoWidth', false)
       //.withOption('scrollCollapse', 'true')
       .withOption('order', [])
       .withButtons([
           {
               extend: 'csvHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Coupon Checklist ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3]
               }
           },
           {
               extend: 'pdfHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Coupon Checklist ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3]
               }
           },
           {
               extend: 'excelHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Coupon Checklist ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3]
               }
           },
           'print']);

    $scope.dtColumnDefsOfferChecklist = [DTColumnDefBuilder.newColumnDef(4).notSortable()];

    $scope.changeBreadcrums([{ "name": "Coupon Checklist", "state": "bns.master.offerChecklist" }]);

    //------------  viewReddeemedOfferDialog  -------------------------------//
    $scope.openViewRedeemedCouponDialog = function (offerRedeemedCouponRecord) {
        $scope.viewRedeemedCouponData = offerRedeemedCouponRecord;
        $scope.offerRedeemedCouponImg = MEDIA_URL + offerRedeemedCouponRecord.offer.imagePath;
        ngDialog.open({
            template: 'viewRedeemedCouponData',
            className: 'ngdialog-theme-default modal-large coupon-code-checklist draggable',
            scope: $scope,
            preCloseCallback: function () { $scope.clearViewRedeemedCouponDialog() }
        });
        $timeout(function () {
            $(".draggable .ngdialog-content").draggable();
        }, 500);
    }
    $scope.clearViewRedeemedCouponDialog = function () {
        $scope.submitted = false;
        $scope.successAlert = false;
        $scope.faliureAlert = false;
        $scope.viewRedeemedCouponData = {};
           
    }

    $scope.closeViewRedeemedCouponDialog = function () {
        ngDialog.close({
            template: 'viewRedeemedCouponData',
            scope: $scope
        });
        $scope.clearViewRedeemedCouponDialog();
    }
    //------------  viewRedFlaggedCouponDialog  -------------------------------//
    $scope.openViewRedFlaggedCouponDialog = function (viewRedFlaggedCouponData) {
        $scope.viewRedFlaggedCouponData = viewRedFlaggedCouponData;
        $scope.offerRedFlaggedCouponImg = MEDIA_URL + viewRedFlaggedCouponData.offer.imagePath;
        ngDialog.open({
            template: 'viewRedFlaggedCouponData',
            className: 'ngdialog-theme-default modal-large coupon-code-checklist draggable',
            scope: $scope,
            preCloseCallback: function () { $scope.clearViewRedFlaggedCouponDialog() }
        });
        $timeout(function () {
            $(".draggable .ngdialog-content").draggable();
        }, 500);
    }
    $scope.clearViewRedFlaggedCouponDialog = function () {
        $scope.submitted = false;
        $scope.successAlert = false;
        $scope.faliureAlert = false;
        $scope.viewRedFlaggedCouponData = {};

    }

    $scope.closeViewRedFlaggedCouponDialog = function () {
        ngDialog.close({
            template: 'viewRedFlaggedCouponData',
            scope: $scope
        });
        $scope.clearViewRedeemedCouponDialog();
    }

    $scope.submitForm = function (isValid, form) {
        $scope.submitted = true;
        if (isValid) {
            $scope.getUserOfferCodeDetails();
        }
        else {
            return false;
        }
    }

    //--------------- Redeem coupon-----------------------//
    $scope.openRedeemCouponDialog = function () {

        ngDialog.open({
            template: 'redeemCouponDialog',
            className: 'ngdialog-theme-default modal-large store-dialog draggable',
            scope: $scope,
            closeByDocument: false,
            preCloseCallback: function () { $scope.clearRedeemCouponModal() }

        });
        $timeout(function () {
            $(".draggable .ngdialog-content").draggable();
        }, 500);
    }
    $scope.clearRedeemCouponModal = function () {
        $scope.submitted = false;
        $scope.successAlert = false;
        $scope.faliureAlert = false;
    }
    $scope.closeRedeemCouponModal = function () {
        ngDialog.close({
            template: 'redeemCouponDialog',
            scope: $scope
        });
        $scope.clearRedeemCouponModal();
    }

   
    //----------------- flag coupon ----------------------//
    $scope.openFlagCouponDialog = function () {

        ngDialog.open({
            template: 'flagCouponDialog',
            className: 'ngdialog-theme-default modal-large store-dialog draggable',
            scope: $scope,
            closeByDocument: false,
            preCloseCallback: function () { $scope.clearFlagCouponModal() }

        });
        $timeout(function () {
            $(".draggable .ngdialog-content").draggable();
        }, 500);
    }
    $scope.clearFlagCouponModal = function () {
        $scope.submitted = false;
        $scope.successAlert = false;
        $scope.faliureAlert = false;
    }
    $scope.closeFlagCouponModal = function () {
        ngDialog.close({
            template: 'flagCouponDialog',
            scope: $scope
        });
        $scope.clearFlagCouponModal();
    }

    $scope.flagCoupon = function () {
        function success_res(data, status, headers, config) {
            $scope.isFlagged = true;
            $scope.isRedeemed = false;
            $scope.isRedeemedAlert = false;
            $scope.closeFlagCouponModal();
            $scope.notify("Coupon red flagged sucessfully", 1);
        }
        function failure_res(data, status, headers, config) {
            $scope.notify("Coupon redeered flagging failed, please try again", 4);
        }
        changeStatusOfCoupon(2, success_res, failure_res);
    }
    $scope.redeemCoupon = function () {
        function success_res(data, status, headers, config) {
            $scope.isRedeemed = true;
            $scope.isRedeemedAlert = false;
            $scope.isFlagged = false;
            $scope.closeRedeemCouponModal();
            $scope.notify("Coupon redeemed sucessfully", 1);
        }
        function failure_res(data, status, headers, config) {
            $scope.notify("Coupon redeemed failed, please try again", 4);
        }
        changeStatusOfCoupon(1, success_res, failure_res);
    }

    function changeStatusOfCoupon(status, sucess_res, failure_res)
    {
        var statusUpdate = {
            "user_id": $scope.userCouponCodeData.userOfferDetails.id,
            "offer_id": $scope.userCouponCodeData.userOfferDetails.offer.id,
            "code": $scope.userCouponCodeData.userOfferDetails.offer.code,
            "status": status
        }
        function success(data, status, headers, config) {
            sucess_res(data, status, headers, config);
        }
        function failure(data, status, headers, config) {
            failure_res(data, status, headers, config);
        }
        var url = "/users/setUserOfferCodeStatus";
        API_SERVICE.postData($scope, $http, statusUpdate, url, success, failure);
    }

    $scope.getUserOfferCodeDetails = function () {
        $scope.dataLoader = 0;
        function success(data, status, headers, config) {
            $scope.dataLoader = 2;
            $scope.userCouponCodeData = data;
            $scope.offerImg = MEDIA_URL + data.userOfferDetails.offer.imagePath;
            if (data.userOfferDetails.offer.OfferStatus == "FLAG")
            {
                $scope.isFlagged = true;
                $scope.isRedeemed = false;
                $scope.isRedeemedAlert = false;
            }
            else if (data.userOfferDetails.offer.OfferStatus == "REDEEMED")
            {
                $scope.isRedeemed = true;
                $scope.isRedeemedAlert = true;
                $scope.isFlagged = false;
            }
            else {
                $scope.isFlagged = false;
                $scope.isRedeemed = false;
                $scope.isRedeemedAlert = false;
            }
            
            $scope.submitted = false;
            $scope.successAlert = false;
            $scope.faliureAlert = false;
            $scope.offerCode = {
                "code": ""
            };
        }
        function failure(data, status, headers, config) {
            $scope.dataLoader = 1;
            $scope.submitted = false;
            $scope.successAlert = false;
            $scope.faliureAlert = true;
            $scope.errorMessage = data;      
        }
        var url = "/users/getUserOfferCodeDetails";
        API_SERVICE.postData($scope, $http, $scope.offerCode, url, success, failure);
    }

    

    $scope.backToCouponCodeHome = function()
    {
        $scope.flagCouponButtonName = "Flag";
        $scope.redeemCouponButtonName = "Redeem";
        $scope.dataLoader = 1;
        $scope.userCouponCodeData = null;
        $scope.offerImg = null;
        $scope.submitted = false;
        $scope.successAlert = false;
        $scope.faliureAlert = false;
        $scope.isFlagged = false;
        $scope.offerCode = {
            "code": ""
        };
    }

    getAllUsersOfferCodeRecords = function()
    {
        
        $scope.redeemedOfferLoader = 0;
        function success(data, status, headers, config)
        {
            $scope.offerCouponRecords = data;
            $scope.redeemedOfferLoader = 1;
        }
        function failure(data, status, headers, config)
        {
            $scope.errorMessage = data;
            $scope.redeemedOfferLoader = 2;
        }
        var url = "/users/getAllUsersOfferCode";
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getAllUsersOfferCodeRecords();

    getAllRedFlaggedCouponsRecords = function () {

        $scope.redFlaggedCouponsLoader = 0;
        function success(data, status, headers, config) {
            $scope.redFlaggedCouponsRecords = data;
            $scope.redFlaggedCouponsLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.errorMessage = data;
            $scope.redFlaggedCouponsLoader = 2;
        }
        var url = "/users/getFlagedUserOfferCode";
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getAllRedFlaggedCouponsRecords();
    $scope.relaodAllRedFlaggedCouponsRecords = function () {
        getAllRedFlaggedCouponsRecords();
    }

    $scope.relaodOfferCouponCodeData = function()
    {
        getAllUsersOfferCodeRecords();
    }
}]);