﻿angular.module('bns.master.terms', [])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.terms', {
        url: '/terms',
        views: {
            "terms": {
                templateUrl: 'App/Master/TermsAndConditions/termsAndConditions.html',
                controller: 'termsController'
            }
        }
    });
}])
.controller("termsController", ['$scope', '$http', '$window', 'API_URL', 'LOADING_MESSAGE', 'API_SERVICE', '$state', 'ngDialog', '$timeout', 'DTOptionsBuilder', 'DTColumnDefBuilder',
    function ($scope, $http, $window, API_URL, LOADING_MESSAGE, API_SERVICE, $state, ngDialog, $timeout, DTOptionsBuilder, DTColumnDefBuilder) {

        $scope.loadingMessage = LOADING_MESSAGE;
        $scope.saveButtonName = 'Add';
        $scope.editButtonName = 'Update';
        $scope.uploadButtonName = 'Upload';
        $scope.newTerm = {
            title: '',
            description: ''
        };
        $scope.TC = {
            termsAndConditionsSheet : ''
        };
        $scope.uploaded = false;
        $scope.notDeleted = true;

        $scope.dtOptionsTermsAndConditions = DTOptionsBuilder.newOptions()
       .withDOM('lrftip')
       .withPaginationType('full_numbers')
       .withOption('responsive', true)
       .withOption('scrollX', true)
       //.withOption('bAutoWidth', false)
       //.withOption('scrollCollapse', 'true')
       .withOption('order', [])
       .withButtons([
           {
               extend: 'csvHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Terms And Conditions ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3]
               }
           },
           {
               extend: 'pdfHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Terms And Conditions ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3]
               }
           },
           {
               extend: 'excelHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Terms And Conditions ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3]
               }
           },
           'print']);
        $scope.dtColumnDefsTermsAndCondtions = [DTColumnDefBuilder.newColumnDef(4).notSortable()];

        $scope.termsAndConditionsSheet = '';
        $scope.fileSelected = false;

        $scope.changeBreadcrums([{ "name": "Terms And Conditions", "state": "master.terms" }]);

        $scope.addTerms = function () {
            ngDialog.open({
                template: 'addTermsDialog',
                className: 'ngdialog-theme-default modal-large beacon-dialog draggable',
                scope: $scope,
                preCloseCallback: function () { $scope.clearModal() }

            });
            $timeout(function () {
                $(".draggable .ngdialog-content").draggable();
            }, 500);
        };

        $scope.getAllTermsAndConditions = function () {
            $scope.dataLoader = 0;  
            function success(data, status, headers, config) {
                console.log(data);
                if (data.length == 0)
                {
                    $scope.dataLoader = 2;
                } else {
                    $scope.termsAndConditions = data;
                    $scope.dataLoader = 1;
                }
            }
            function failure(data, status, headers, config) {
                console.log("Failed");
                $scope.dataLoader = 3;
            }
            var url = "/TermsAndConditions/get";
            API_SERVICE.getData($scope, $http, url, success, failure);
        }
        $scope.getAllTermsAndConditions();

        $scope.clearModal = function () {
            $scope.submitted = false;
            $scope.successAlert = false;
            $scope.faliureAlert = false;
            $scope.newTerm = {
                title: '',
                description: ''
            };
        }

        $scope.closeModal = function () {
            ngDialog.close({
                template: 'addTermsDialog',
                scope: $scope
            });
            $scope.clearModal();
        }

        $scope.saveNewTerm = function (isValid) {
            $scope.saveButtonName = 'Adding';
            $scope.submitted = true;
            $scope.successAlert = false;
            $scope.faliureAlert = false;
            if (isValid) {
                console.log($scope.newTerm);

                function success(data, status, headers, config) {
                    $scope.notify("New TermsAndCondition with name " + $scope.newTerm.title + ", successfully added to the list.", 1);
                    $scope.submitted = false;
                    $scope.successAlert = true;
                    $scope.faliureAlert = false;
                    $scope.newTerm = {
                        title: '',
                        description: ''
                    };
                    $scope.loading = false;
                    $scope.saveButtonName = 'Add';
                    $scope.getAllTermsAndConditions();
                    $scope.closeModal();
                }
                function failure(data, status, headers, config) {
                    $scope.loading = false;
                    $scope.saveButtonName = 'Add';
                    $scope.successAlert = false;
                    $scope.faliureAlert = true;
                    if (status == 409) {
                        $scope.errorMessage = data;
                    }
                    else {
                        $scope.errorMessage = "Could not add term/condition to the list. Please check all the fields."
                        $scope.saveButtonName = 'Add';
                    }
                }
                var url = "/TermsAndConditions/add";
                API_SERVICE.postData($scope, $http, $scope.newTerm, url, success, failure);

            } else {
                $scope.successAlert = false;
                $scope.faliureAlert = false;
                $scope.saveButtonName = 'Add';
                console.log('Unable to add new terms');
            }
        }

        $scope.deleteTerm = function (index) {
            $scope.idToDelete = $scope.termsAndConditions[index].id
            if ($scope.termsAndConditions[index].validOn != 0)
            {
                ngDialog.open({
                    template: 'deleteNotAllowed',
                    className: 'ngdialog-theme-default modal-large beacon-dialog draggable',
                    scope: $scope
                });
                $timeout(function () {
                    $(".draggable .ngdialog-content").draggable();
                }, 500);
            } else {
                ngDialog.open({
                    template: 'deleteTermsDialog',
                    className: 'ngdialog-theme-default modal-large beacon-dialog draggable',
                    scope: $scope,
                    preCloseCallback: function () { $scope.clearConfirDeleteModal() }

                });
                $timeout(function () {
                    $(".draggable .ngdialog-content").draggable();
                }, 500);
            }
        }

        $scope.closeConfirmModal = function () {
            ngDialog.close({
                template: 'deleteTermsDialog',
                scope: $scope
            });
            
        }

        $scope.clearConfirDeleteModal = function () {
            $scope.notDeleted = true;
        }

        $scope.confirmDelete = function () {

            function success(data, status, headers, config) {
                $scope.notDeleted = false;
                $scope.deleteSuccess = true;
                $scope.notify("TermsAndCondtition deleted from the list.", 1);
                $scope.getAllTermsAndConditions();
                $scope.closeModal();
            }
            function failure(data, status, headers, config) {
                $scope.deleteSuccess = false;
            }
            var url = "/TermsAndConditions/delete?id="+$scope.idToDelete;
            API_SERVICE.deleteData($scope, $http, url, success, failure);

        }

        $scope.viewTerms = function (termAndCondition) {
            $scope.viewTerm = termAndCondition;
            ngDialog.open({
                template: 'viewTermsDialog',
                className: 'ngdialog-theme-default modal-large beacon-dialog draggable',
                scope: $scope,
                preCloseCallback: function () { $scope.clearConfirDeleteModal() }
            });
            $timeout(function () {
                $(".draggable .ngdialog-content").draggable();
            }, 500);

        }

        $scope.editTerms = function (termAndCondition) {
            $scope.editTerm = termAndCondition;
            ngDialog.open({
                template: 'editTermsDialog',
                className: 'ngdialog-theme-default modal-large beacon-dialog draggable',
                scope: $scope,
                preCloseCallback: function () { $scope.clearEditModal() }
            });
            $timeout(function () {
                $(".draggable .ngdialog-content").draggable();
            }, 500);

        }

        $scope.clearEditModal = function () {
            $scope.successAlert = false;
            $scope.faliureAlert = false;
        }

        $scope.updateTerm = function (isValid) {

            $scope.editButtonName = 'Updating';
            $scope.submitted = true;

            if (isValid) {
                console.log($scope.editTerm);

                function success(data, status, headers, config) {
                    $scope.notify("TermsAndContion updated successfully.", 1);
                    $scope.submitted = false;
                    $scope.successAlert = true;
                    $scope.faliureAlert = false;
                    $scope.loading = false;
                    $scope.editButtonName = 'Update';
                    $scope.getAllTermsAndConditions();
                    $scope.closeModal();
                }
                function failure(data, status, headers, config) {
                    $scope.loading = false;
                    $scope.editButtonName = 'Update';
                    $scope.successAlert = false;
                    $scope.faliureAlert = true;
                    if (status == 409) {
                        $scope.errorMessage = data;
                    }
                    else {
                        $scope.errorMessage = "Could not add term/condition to the list. Please check all the fields."
                        $scope.editButtonName = 'Update';
                    }
                }
                var url = "/TermsAndConditions/edit";
                API_SERVICE.postData($scope, $http, $scope.editTerm, url, success, failure);

            } else {
            }
        }

        $scope.import = function () {
            if ($scope.termsAndConditionsSheet == '' || $scope.termsAndConditionsSheet == undefined || $scope.termsAndConditionsSheet == null) {
                $scope.alertMode = 'danger';
                $scope.uploadTitle = 'Failure';
                $scope.uploadMessage = '"Please select an excel file before uploading."';
                $scope.uploaded = true;
            } else {
                $scope.uploadButtonName = 'Uploading';
                $scope.uploaded = false;
                console.log($scope.termsAndConditionsSheet);
                $scope.TC.termsAndConditionsSheet = $scope.termsAndConditionsSheet;
                function success(data, status, headers, config) {
                    console.log(data.data);
                    $scope.alertMode = 'success';
                    $scope.uploadTitle = 'Success';
                    $scope.uploadMessage = data.data;
                    $scope.uploadButtonName = 'Upload';
                    $scope.uploaded = true;
                    $scope.getAllTermsAndConditions();
                }
                function failure(data, status, headers, config) {
                    $scope.alertMode = 'danger';
                    $scope.uploadTitle = 'Failure';
                    $scope.uploadMessage = data.data;
                    $scope.uploaded = true;
                    $scope.uploadButtonName = 'Upload';
                }
                var url = "/TermsAndConditions/upload";
                API_SERVICE.postMultiPartData($scope, $http, $scope.TC, url, success, failure);
            }
        };

        $scope.fileAction = function () {
            alert('Action performed');
        };

        $scope.fileNameChanged = function (files) {
            if (files.files.length == 0) {
                $scope.fileSelected = false;
                $scope.termsAndConditionsSheet = '';
            } else {
                $scope.fileSelected = true;
                $scope.termsAndConditionsSheet = files.files[0];
            }
        };
        
        $scope.uploadTermsConditions = function () {
            $scope.uploaded = false;
            $scope.uploadButtonName = 'Upload';
            ngDialog.open({
                template: 'uploadTermsAndConditions',
                className: 'ngdialog-theme-default modal-large beacon-dialog draggable',
                scope: $scope,
                preCloseCallback: function () { $scope.clearUploadModal() }
            });
            $timeout(function () {
                $(".draggable .ngdialog-content").draggable();
            }, 500);
        };

        $scope.closeUploadModal = function () {
            ngDialog.close({
                template: 'uploadTermsAndConditions',
                scope: $scope
            });
            $scope.clearUploadModal();
        };

        $scope.clearUploadModal = function () {
            $scope.termsAndConditionsSheet = '';
        };

        $scope.showSample = function () {
            $scope.closeModal();
            $state.go('master.termsSample');
        };

        $scope.download = function () {
            window.open("../../../libs/assets/T&CUpload.xlsx", "_blank");
        };

    }])