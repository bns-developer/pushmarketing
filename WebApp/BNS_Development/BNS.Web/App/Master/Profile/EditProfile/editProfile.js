﻿angular.module('bns.master.editProfile', [])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.editProfile', {
        url: '/editProfile',
        views: {
            "editProfile": {
                templateUrl: 'App/Master/Profile/EditProfile/editProfile.html',
                controller: 'editProfileController'
            }
        }
    });
}])
.controller("editProfileController", ['$scope','$state', '$http', 'API_URL', 'API_SERVICE', 'MEDIA_URL', function ($scope, $state, $http, API_URL, API_SERVICE, MEDIA_URL) {

    $scope.updateButtonName = "Update";
    $scope.submitted = false;
    $scope.successAlert = false;
    $scope.failureAlert = false;
    $scope.image = "";

    $scope.editProfileModel = {
        "name" : "",
        "website" : "",
        "image": "",
        "picUrl": "",
        "twitter": "",
        "facebook": ""

    };

    $scope.changeBreadcrums([{ "name": "Profile", "state": "master.profile" }, { "name": "Edit Account Profile", "state": "master.editProfile" }]);

    function formatContacts(accountContacts)
    {
        for(var i=0; i<accountContacts.length; i++)
        {
            var accountContact = accountContacts[i];
            if (accountContact.ContactType == "WEBSITE")
                $scope.editProfileModel.website = accountContact.value;
            else if (accountContact.ContactType == "TWITTER")
                $scope.editProfileModel.twitter = accountContact.value;
            else if (accountContact.ContactType == "FACEBOOK")
                $scope.editProfileModel.facebook = accountContact.value;
        }
    }
    //--------------------- account details api ------------------------------------------//
    function getAccountDetails() {
        $scope.loading = true;
        $scope.dataLoading = 0;
        function success(data, status, headers, config) {
            $scope.editProfileModel = data;
            formatContacts(data.accountContacts)
            $scope.imgSrc = MEDIA_URL + $scope.editProfileModel.picUrl;
            $scope.editProfileModel.file = MEDIA_URL + $scope.editProfileModel.picUrl;
            $scope.loading = false;
            $scope.reloadAccountDetails();
            $scope.dataLoading = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.dataLoading = 2;
        }
        var url = "/accounts/details?account_id=" + 5;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getAccountDetails();

    $scope.reloadData = function () {
        getAccountDetails();
    }

    //--------------------- update profile form validation and API -----------------------//
    $scope.submitForm = function (isValid, form) {
        $scope.submitted = true;
        if (isValid) {
            $scope.updateProfileAPI();
        }
        else {
            return false;
        }
    }
    $scope.updateProfileAPI = function () {
        $scope.updateButtonName = "Updating";
        $scope.editProfileModel.picUrl = $scope.image[0];
       
        $scope.dateError = false;
        function success(data, status, headers, config) {
            $scope.submitted = false;
            $scope.notify("Profile updated successfully.", 1);
            $scope.successAlert = true;
            $scope.faliureAlert = false;
            $scope.editProfileModel = {
                "name": "",
                "website": "",
                "image": "",
                "picUrl": ""
            };
            getAccountDetails();
            $scope.updateButtonName = "Update";
            $state.go('master.profile');

        }
        function failure(data, status, headers, config) {
            $scope.updateButtonName = "Update";
            $scope.successAlert = false;
            $scope.faliureAlert = true;
            if (data.status == 409) {
                $scope.errorMessage = data.data;
            }
            else {
                $scope.errorMessage = "Could not update. Please check all the fields."
            }
        }
        var url = "/accounts/update";
        API_SERVICE.postMultiPartData($scope, $http, $scope.editProfileModel, url, success, failure);
    }

}]);