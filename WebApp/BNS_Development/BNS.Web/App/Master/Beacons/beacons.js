angular.module('bns.master.beacons', [])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.beacons', {
        url: '/beacons',
        views: {
            "beacons": {
                templateUrl: 'App/Master/Beacons/beacons.html',
                controller: 'beaconsController'
            }
        }
    });

    $stateProvider.state('master.beaconDetails', {
        url: '/beaconDetails?beaconId',
        views: {
            "beaconDetails": {
                templateUrl: 'App/Master/Beacons/BeaconDetails/BeaconDetails.html',
                controller: 'beaconDetailsController'
            }
        }
    });

}])
.controller("beaconsController", ['$scope', '$http', '$window', 'API_URL', 'LOADING_MESSAGE', 'ngDialog', 'API_SERVICE', '$state', '$timeout', 'DTOptionsBuilder', 'DTColumnDefBuilder', function ($scope, $http, $window, API_URL, LOADING_MESSAGE, ngDialog, API_SERVICE, $state, $timeout, DTOptionsBuilder, DTColumnDefBuilder) {

    $scope.saveButtonName = "Save";
    $scope.deleteButtonName = "Yes";
    $scope.updateButtonName = "Update";
    $scope.loadingMessage = LOADING_MESSAGE;
    $scope.submitted = false;
    $scope.successAlert = false;
    $scope.failureAlert = false;
    $scope.changeBreadcrums([{ "name": "Beacons", "state": "master.beacons" }]);

    $scope.dtOptionsBeacons = DTOptionsBuilder.newOptions()
       .withDOM('lrftip')
       .withPaginationType('full_numbers')
       .withOption('responsive', true)
       .withOption('scrollX', true)
       //.withOption('bAutoWidth', false)
       //.withOption('scrollCollapse', 'true')
       .withOption('order', [])
       .withButtons([
           {
               extend: 'csvHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Beacons ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3, 4, 5, 6]
               }
           },
           {
               extend: 'pdfHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Beacons ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3, 4, 5, 6]
               }
           },
           {
               extend: 'excelHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Beacons ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3, 4, 5, 6]
               }
           },
           'print']);
    $scope.dtColumnDefsBeacons = [DTColumnDefBuilder.newColumnDef(7).notSortable()];

    function getAllBeaconList() {
        $scope.loading = true;
        $scope.dataLoader = 0;
        function success(data, status, headers, config) {
            $scope.beacons = data;
            $scope.loading = false;
            $scope.dataLoader = 1;
      
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.dataLoader = 2;
        }
        var url = "/beacons/all";
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getAllBeaconList();

    $scope.goBeaconDetails = function (beaconId) {
        $state.go("master.beaconDetails", { "beaconId": beaconId });
    }
    $scope.reloadBeaconList = function () {
        getAllBeaconList();
    }


    //-------------------------------------------------------------------------------- add beacon dialog

    $scope.newBeacon = {
        "name": "",
        "number": "",
        "beacon_uuid":"",
        "store": { "id": "" },
        "address": "",
        "notification": "",
        "lat": "",
        "lng": ""
    };
    $scope.editBeaconModel = {
        "name": "",
        "number": "",
        "id": "",
        "store": { "id": "" }
    };
    $scope.deleteBeaconModel = {
        "name": "",
        "number": "",
        "id": "",
        "store": { "id": "" }
    };
    $scope.mapModel = {
        "address": "",
        "lat": "",
        "lng": "",
        "loc": "",
    };

    $scope.addBeacon = function () {
        ngDialog.open({
            template: 'addBeaconDialog',
            className: 'ngdialog-theme-default modal-large beacon-dialog draggable',
            scope: $scope,
            closeByDocument: false,
            preCloseCallback: function () { $scope.clearModal() }

        });
        $timeout(function () {
            $(".draggable .ngdialog-content").draggable();
        }, 500);
    }
    var map_dialog;
    $scope.findAddress = function () {
        map_dialog = ngDialog.open({
            template: 'mapDialog',
            className: 'ngdialog-theme-default modal-large map-dialog',
            scope: $scope,
            closeByDocument: true,
            preCloseCallback: function () { $scope.clearMapModal() }
        });
        $timeout(function () {
            //$(".ngdialog-content").draggable();
            $scope.initMap();
        }, 500);
    };
    
   $scope.initMap = function() {
        var Map = new google.maps.Map(document.getElementById('map'), {
            center: { lat: 0, lng: 0 },
            zoom: 1
        });
       
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(0, 0),            
            map: Map
        });

        Map.addListener('drag', function () {
            var lat = document.getElementById('lat');
            var lng = document.getElementById('lng');
            var loc = Map.getCenter();
            lat.innerHTML = loc.lat();           
            lng.innerHTML = loc.lng();
            marker.setPosition(loc);
        });

        Map.addListener('zoom_changed', function () {
            var loc = Map.getCenter();
            marker.setPosition(loc);
        });

        Map.addListener('center_changed', function () {
            var lat = document.getElementById('lat');
            var lng = document.getElementById('lng');
            var loc = Map.getCenter();
            lat.innerHTML = loc.lat();
            lng.innerHTML = loc.lng();
            marker.setPosition(loc);           
        });

        var geocoder = new google.maps.Geocoder();

        document.getElementById('go').addEventListener('click', function () {
            goSearch(geocoder, Map);
        });

        document.getElementById('useLocation').addEventListener('click', function () {
            geocodeLatLng(geocoder, Map);
        });
   }

   function goSearch(geocoder, resultsMap) {
       var address = document.getElementById('address').value;
       geocoder.geocode({ 'address': address }, function (results, status) {
           if (status === 'OK') {
               resultsMap.setCenter(results[0].geometry.location);
               resultsMap.setZoom(11);
           } else {
               alert('Geocode was not successful for the following reason: ' + status);
           }
       });
   }

   function geocodeLatLng(geocoder, map) {
       var locLat = document.getElementById('lat').innerText;
       var locLng = document.getElementById('lng').innerText;
       var latlng = { lat: parseFloat(locLat), lng: parseFloat(locLng) };
       geocoder.geocode({ 'location': latlng }, function (results, status) {
           if (status === 'OK') {
               if (results[0]) {
                   var Address = results[0].formatted_address;
                   $scope.newBeacon.address = Address;
                   $scope.newBeacon.lat = locLat;
                   $scope.newBeacon.lng = locLng;
                   map_dialog.close();
               } else {
                   window.alert('No results found');
               }
           } else {             
               window.alert('Please select the location');
           }
       });
   }
  
    $scope.closeModal = function () {
        ngDialog.close({
            template: 'addStoreDialog',
            scope: $scope
        });
        $scope.clearModal();
    }

    $scope.clearModal = function () {
        $scope.submitted = false;
        $scope.successAlert = false;
        $scope.faliureAlert = false;
        $scope.newBeacon = {
            "name": "",
            "number": "",
            "beacon_uuid": "",
            "store": { "id": "" },
            "address": "",
            "notification": "",
            "lat": "",
            "lng": ""
        };
    }

    $scope.clearMapModal = function () {
        $scope.submitted = false;
        $scope.successAlert = false;
        $scope.faliureAlert = false;
        $scope.mapModel = {
            "address": "",
            "lat": "",
            "lng": "",
            "loc": "",
        };
    }
    //------------------------ Update beacon modal --------------------------//
    $scope.updateBeacon = function (beacon) {
        $scope.editBeaconModel.name = beacon.name;
        $scope.editBeaconModel.number = beacon.number;
        $scope.editBeaconModel.store.id = beacon.store.id;
        $scope.editBeaconModel.id = beacon.id;
        ngDialog.open({
            template: 'updateBeaconDialog',
            className: 'ngdialog-theme-default modal-large beacon-dialog draggable',
            scope: $scope,
            closeByDocument: false,
            preCloseCallback: function () { $scope.clearUpdateModal() }

        });
        $timeout(function () {
            $(".draggable .ngdialog-content").draggable();
        }, 500);
    }

    $scope.closeModal = function () {
        ngDialog.close({
            template: 'updateBeaconDialog',
            scope: $scope
        });
        $scope.clearUpdateModal();
    }

    $scope.clearUpdateModal = function () {
        $scope.submitted = false;
        $scope.successAlert = false;
        $scope.faliureAlert = false;
        $scope.editBeaconModel = {
            "name": "",
            "number": "",
            "id": "",
            "store": { "id": "" }
        };
    }

     //----------------------- Delete beacon modal -------------------------//

        $scope.deleteBeacon = function (beacon) {
            $scope.deleteBeaconModel.name = beacon.name;
            $scope.deleteBeaconModel.number = beacon.number;
            $scope.deleteBeaconModel.store.id = beacon.store.id;
            $scope.deleteBeaconModel.id = beacon.id;
            ngDialog.open({
                template: 'deleteBeaconDialog',
                className: 'ngdialog-theme-default modal-large store-dialog draggable',
                scope: $scope,
                closeByDocument: false,
                preCloseCallback: function () { $scope.clearDeleteModal() }

            });
            $timeout(function () {
                $(".draggable .ngdialog-content").draggable();
            }, 500);
        }

        $scope.clearDeleteModal = function () {
            $scope.submitted = false;
            $scope.successAlert = false;
            $scope.faliureAlert = false;
            $scope.deleteBeaconModel = {
                "name": "",
                "number": "",
                "id": "",
                "store": { "id": "" }
            };
        }

        $scope.closeDeleteModal = function () {
            ngDialog.close({
                template: 'deleteBeaconDialog',
                scope: $scope
            });
            $scope.clearDeleteModal();
        }
    //-------------------------------------------------------------------//
       
        $scope.deleteBeaconAPI = function () {
            $scope.deleteButtonName = "Deleting";
            $scope.submitted = true;
            if ($scope.deleteBeaconModel.id == "" || $scope.deleteBeaconModel.id == undefined) {
                $scope.deleteButtonName = "Yes";
            }
            else {
                function success(data, status, headers, config) {
                    getAllBeaconList();
                    $scope.notify("Beacon with name \"" + $scope.deleteBeaconModel.name + "\", successfully deleted from the list.", 1);
                    $scope.submitted = false;
                    $scope.successAlert = true;
                    $scope.faliureAlert = false;
                    $scope.closeDeleteModal();
                    $scope.deleteBeaconModel = {
                        "name": "",
                        "number": "",
                        "id": "",
                        "store": { "id": "" }
                    };
                    $scope.loading = false;
                    $scope.deleteButtonName = "Yes";
                }
                function failure(data, status, headers, config) {
                    $scope.loading = false;
                    $scope.successAlert = false;
                    $scope.faliureAlert = true;
                    $scope.deleteButtonName = "Yes";
                    if (status == 409) {
                        $scope.errorMessage = data;
                    }
                    else if (status == 500) {
                        $scope.errorMessage = data;
                    }
                    else {
                        $scope.errorMessage = "Could not delete beacon."
                    }
                }
                var url = "/beacons/delete";
                API_SERVICE.postData($scope, $http, $scope.deleteBeaconModel, url, success, failure);
            }
        }

        $scope.updateBeaconAPI = function () {
            $scope.updateButtonName = "Updating";
            $scope.submitted = true;
            if ($scope.editBeaconModel.id == "" || $scope.editBeaconModel.id == undefined || $scope.editBeaconModel.name == "" || $scope.editBeaconModel.name == undefined || $scope.editBeaconModel.number == "" || $scope.editBeaconModel.number == undefined || $scope.editBeaconModel.store.id == "" || $scope.editBeaconModel.store.id == undefined) {
                $scope.updateButtonName = "Update";
            }
            else {
                function success(data, status, headers, config) {
                    getAllBeaconList();
                    $scope.notify("Beacon with name \"" + $scope.editBeaconModel.name + "\" updated successfully.", 1);
                    $scope.submitted = false;
                    $scope.successAlert = true;
                    $scope.faliureAlert = false;
                    $scope.closeModal();
                    $scope.editBeaconModel = {
                        "name": "",
                        "number": "",
                        "id": "",
                        "store": { "id": "" }
                    };
                    $scope.loading = false;
                    $scope.updateButtonName = "Update";
                }
                function failure(data, status, headers, config) {
                    $scope.loading = false;
                    $scope.successAlert = false;
                    $scope.faliureAlert = true;
                    $scope.updateButtonName = "Update";
                    if (status == 409) {
                        $scope.errorMessage = data;
                    }
                    else {
                        $scope.errorMessage = "Could not update store. Please check all the fields."
                    }
                }
                var url = "/beacons/update";
                API_SERVICE.postData($scope, $http, $scope.editBeaconModel, url, success, failure);
            }
        }

       

    function getAllStoresList() {
        $scope.loading = true;
        function success(data, status, headers, config) {
            $scope.stores = data;
            console.log($scope.stores);
            $scope.loading = false;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
        }
        var url = "/stores/all";
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getAllStoresList();

    $scope.saveNewBeacon = function () {     
        $scope.submitted = true;
        $scope.saveButtonName = "Saving";
        if ($scope.newBeacon.name == "" || $scope.newBeacon == undefined || $scope.newBeacon.store.id == "" || $scope.newBeacon.store.id == undefined || $scope.newBeacon.beacon_uuid == undefined || (($scope.newBeacon.beacon_uuid.length > 0 || $scope.newBeacon.address.length > 0) && ($scope.newBeacon.notification == "" || $scope.newBeacon.notification == undefined))) {
            $scope.saveButtonName = "Save";
        }
        else {
           
            function success(data, status, headers, config) {
                getAllBeaconList();
                $scope.notify("Beacon with name \"" + $scope.newBeacon.name + "\", successfully added to the list.", 1);
                $scope.submitted = false;
                $scope.successAlert = true;
                $scope.faliureAlert = false;
                $scope.closeModal();
                $scope.newBeacon = {
                    "name": "",
                    "number": "",
                    "beacon_uuid": "",
                    "store": { "id": "" },
                    "address": "",
                    "notification": "",
                    "lat": "",
                    "lng": ""
                };
                //check if we can replace above code with clear modal
                $scope.loading = false;
                $scope.saveButtonName = "Save";
            }
            function failure(data, status, headers, config) {
                $scope.loading = false;
                $scope.successAlert = false;
                $scope.faliureAlert = true;
                $scope.saveButtonName = "Save";
                if (status == 409 || status == 403) {
                    $scope.errorMessage = data;
                }
                else {
                    $scope.errorMessage = "Could not add beacon. Please check all the fields."
                }
            }
            var url = "/beacons/add";
            API_SERVICE.postData($scope, $http, $scope.newBeacon, url, success, failure);
        }
        console.log($scope.newBeacon);
    }

    $scope.beaconDetails = function (beaconId) {
        $state.go('master.viewBeacon', { 'beaconId': beaconId })
    }

}])