﻿angular.module('bns.master.viewBeacon', ['bns.master.viewBeacon.beaconDetail', 'monospaced.qrcode'])


.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.viewBeacon', {
        url: '/beacons/viewBeacon?beaconId',
        views: {
            "viewBeacon": {
                templateUrl: 'App/Master/Beacons/viewBeacon/viewBeacon.html',
                controller: 'viewBeaconController'
            }
        }
    });
}])
.controller("viewBeaconController", ['$scope', '$http', 'API_URL', 'ngDialog', '$stateParams', 'API_SERVICE', 'LOADING_MESSAGE', 'MEDIA_URL', 'LOADING_MESSAGE', '$timeout', '$state', function ($scope, $http, API_URL, ngDialog, $stateParams, API_SERVICE, LOADING_MESSAGE, MEDIA_URL, LOADING_MESSAGE, $timeout, $state) {
    $scope.loadingMessage = LOADING_MESSAGE;
    $scope.beaconDetail = $stateParams.beaconDetail;
    $scope.copyButtonText = "Copy Beacon URL";
    $scope.copyPassUrlButtonText = "Copy Pass URL";
    $scope.setNonWorkableButtonName = "Set";
    $scope.beaconWorkable = true;
    $scope.beaconMonthlyUserData = [];
    $scope.beaconCashMonthlyUserData = [];
    $scope.beaconPointsMonthlyUserData = [];
    $scope.beaconPromotionMonthlyUserData = [];
    
    setWorkableButtonName = function () {
        if ($scope.beaconDetail.is_workable) {
            $scope.workableButtonName = "Set As Not Workable";
            $scope.beaconWorkable = true;
        }
        else {
            $scope.workableButtonName = "Set As Workable";
            $scope.beaconWorkable = false;
        }
    }

    function getBeaconInfo() {
        $scope.loading = true;
        $scope.dataLoading = 0;
        function success(data, status, headers, config) {
            $scope.beaconDetail = data;
            setWorkableButtonName();
            $scope.loading = false;
            $scope.changeBreadcrums([{ "name": "Beacons", "state": "master.beacons" }, { "name": $scope.beaconDetail.name, "state": "master.viewBeacon" }]);
            $scope.dataLoading = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.dataLoading = 2;
        }
        var url = "/beacons/details?beaconId=" + $stateParams.beaconId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getBeaconInfo();

    $scope.reloadBeaconData = function () {
        getBeaconInfo();
    }

    $scope.showOffers = function () {
        $state.go("master.viewBeacon.beaconDetail", { "beaconDetail": $scope.beaconDetail });
    }
    $scope.setWorkableAPI = function () {
        var beaconState = "";
        $scope.workableButtonName = "Wait...";
        function success(data, status, headers, config) {
            if ($scope.beaconDetail.is_workable)
            {
                $scope.closeNotWorkableModal();
                beaconState = "Not Workable";
            }
            else
                beaconState = "Workable";
            getBeaconInfo();
            getBeaconDashboardInfo();
            $scope.notify("Beacon state is changed to " + beaconState + " successfully!", 1);
        }
        function failure(data, status, headers, config) {
            setWorkableButtonName();
        }
        var url = "/beacons/changeBeaconWorkableState?beaconId=" + $stateParams.beaconId;
        API_SERVICE.postData($scope, $http, "", url, success, failure);
    }
    //------------- getBeacon Dashboard data -----------------------------//
    function drawCharts() {
        Highcharts.setOptions({
            lang: {
                noData: 'No data present.'
            }
        });
        Highcharts.chart('TimeVsRegUsers', {
            series: [{
                name: 'Registered User',
                data: $scope.beaconMonthlyUserData
                //data: [[1483228800000, 567], [1485907200000, 685], [1488326400000, 1025], [1493596800000, 986], [1496275200000, 2358]]
            }],
            title: {
                text: 'Registered users in last 5 months'
            },
            credits: {
                enabled: false
            },
            yAxis: {
                maxPadding: 0.10,
                title: {
                    text: 'Number of users registered'
                }
            },
            xAxis: {
                maxPadding: 0.10,
                title: {
                    text: 'Month'
                },
                startOnTick: true,
                type: 'datetime',
                dateTimeLabelFormats: {
                    month: '%b %Y'
                }
            }
        });

        Highcharts.chart('CategoriesVsRegUsers', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Offer Categories VS Users'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                maxPadding: 0.10,
                title: {
                    text: 'Month'
                },
                startOnTick: true,
                type: 'datetime',
                dateTimeLabelFormats: {
                    month: '%b'
                }
            },
            yAxis: {
                maxPadding: 0.10,
                min: 0,
                title: {
                    text: 'Number of users availing offer'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Cash',
                data: $scope.beaconCashMonthlyUserData
                //data: [125, 658, 214, 587, 164]

            }, {
                name: 'Points',
                data: $scope.beaconPointsMonthlyUserData
                //data: [658, 421, 56, 596, 256]

            }, {
                name: 'Promotion',
                data: $scope.beaconPromotionMonthlyUserData
                //data: [64, 854, 142, 741, 963]

            }]
        });     
    }

    function formatMonthlyUserdata(rawMonthlyUserData) {
        if (!rawMonthlyUserData)
        {
            rawMonthlyUserData = [];
        }
        var monthlyUserData = [];
        switch (rawMonthlyUserData.length) {
            case 5: break;
            case 0: var date = new Date(); date.setDate(1); date.setHours(-1);
                rawMonthlyUserData[0] = { "userDate": date, "userCount": 0 };
            case 1: rawMonthlyUserData.splice(1, 0, { "userDate": new Date(moment(rawMonthlyUserData[0].userDate).subtract(1, 'months')), "userCount": 0 });
            case 2: rawMonthlyUserData.splice(2, 0, { "userDate": new Date(moment(rawMonthlyUserData[0].userDate).subtract(2, 'months')), "userCount": 0 });
            case 3: rawMonthlyUserData.splice(3, 0, { "userDate": new Date(moment(rawMonthlyUserData[0].userDate).subtract(3, 'months')), "userCount": 0 });
            case 4: rawMonthlyUserData.splice(4, 0, { "userDate": new Date(moment(rawMonthlyUserData[0].userDate).subtract(4, 'months')), "userCount": 0 });
                break;
        }
        for (i = 0; i < rawMonthlyUserData.length ; i++) {
            var tempDate = new Date(rawMonthlyUserData[i].userDate)
            //monthlyUserData.push([Date.UTC(tempDate.getFullYear(), tempDate.getMonth(), tempDate.getDay()), rawMonthlyUserData[i].userCount]);
            monthlyUserData.push([tempDate.getTime(), rawMonthlyUserData[i].userCount]);
        }
        return monthlyUserData;
    }
    function formatCategorywiseMonthlyUserData(rawData) {
        for (j = 0; j < rawData.length; j++) {
            if (rawData[j].name == "Cash") {
                $scope.beaconCashMonthlyUserData = formatMonthlyUserdata(rawData[j].data);
            }
            else if (rawData[j].name == "Points") {
                $scope.beaconPointsMonthlyUserData = formatMonthlyUserdata(rawData[j].data);
            }
            else if (rawData[j].name == "Promotion") {
                $scope.beaconPromotionMonthlyUserData = formatMonthlyUserdata(rawData[j].data);
            }
        }
         
    }

    function getBeaconDashboardInfo() {
        $scope.beaconDashbordData = 0;
        function success(data, status, headers, config) {
            $scope.beaconDashboardData = data;
            $scope.beaconMonthlyUserData = formatMonthlyUserdata(data.monthlyUserCount);
            formatCategorywiseMonthlyUserData(data.categoryList);
            $scope.beaconDashbordData = 1;
            drawCharts();
        }
        function failure(data, status, headers, config) {
            $scope.beaconDashbordData = 2;         
        }
        var url = "/dashboard/getBeaconDashboardData?beacon_id=" + $stateParams.beaconId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getBeaconDashboardInfo();
    $scope.reloadBeaconDashboardData = function () {
        getBeaconDashboardInfo();
    }
    //--------- Go to popular offer ------------------------------//
    $scope.goToPopularOffer = function (offerId) {
        if (offerId != '')
            $state.go('master.offerDetails', { "offerId": offerId });
        else
            $scope.notify("Currently there is no popular offer", 2);
    }
    //----------------------- set beacon as non workable modal -------------------------//
    $scope.setWorkable = function () {
        if ($scope.beaconDetail.is_workable) {
            ngDialog.open({
                template: 'setBeaconNonWorkableDialog',
                className: 'ngdialog-theme-default modal-large store-dialog draggable',
                scope: $scope,
                closeByDocument: false,
                preCloseCallback: function () { $scope.clearNotWorkableModal() }

            });
            $timeout(function () {
                $(".draggable .ngdialog-content").draggable();
            }, 500); 
        }
        else {
            $scope.setWorkableAPI();
        }

    }

    $scope.clearNotWorkableModal = function () {
        $scope.submitted = false;
        $scope.successAlert = false;
        $scope.faliureAlert = false;
    }

    $scope.closeNotWorkableModal = function () {
        ngDialog.close({
            template: 'setBeaconNonWorkableDialog',
            scope: $scope
        });
        $scope.clearNotWorkableModal();
    }
    //-------------------------------------------------------------------//
    $scope.copyUrlToClipboard = function (text, urlType) {
        if (window.clipboardData && window.clipboardData.setData) {
            switch(urlType)
            {
                case 1:
                    $scope.copyButtonText = "Copied";
                    $timeout(function () {
                        $scope.copyButtonText = "Copy Beacon URL";
                    }, 1000);
                    break;
                case 2:
                    $scope.copyPassUrlButtonText = "Copied";
                    $timeout(function () {
                        $scope.copyPassUrlButtonText = "Copy Pass URL";
                    }, 1000);
                    break;
                default:
                    break;
            }
            return clipboardData.setData("Text", text);

        } else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
            var textarea = document.createElement("textarea");
            textarea.textContent = text;
            switch(urlType)
            {
                case 1:
                    $scope.copyButtonText = "Copied";
                    $timeout(function () {
                        $scope.copyButtonText = "Copy Beacon URL";
                    }, 1000);
                    break;
                case 2:
                    $scope.copyPassUrlButtonText = "Copied";
                    $timeout(function () {
                        $scope.copyPassUrlButtonText = "Copy Pass URL";
                    }, 1000);
                    break;
                default:
                    break;
            }
           
            textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in MS Edge.
            document.body.appendChild(textarea);
            textarea.select();
            try {

                return document.execCommand("copy");  // Security exception may be thrown by some browsers.
            } catch (ex) {
                console.warn("Copy to clipboard failed.", ex);
                return false;
            } finally {
                document.body.removeChild(textarea);
            }
        }
    }
    
}])