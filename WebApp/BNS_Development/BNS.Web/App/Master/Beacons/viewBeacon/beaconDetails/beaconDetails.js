﻿angular.module('bns.master.viewBeacon.beaconDetail', ['angularjs-dropdown-multiselect'])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.viewBeacon.beaconDetail', {
        url: '/beaconDetail',
        views: {
            "beaconDetail": {
                templateUrl: 'App/Master/Beacons/viewBeacon/beaconDetails/beaconDetails.html',
                controller: 'beaconDetailController'
            }
        }
    });
}])

.controller("beaconDetailController", ['$scope', '$http', 'API_URL', 'ngDialog', '$stateParams', 'API_SERVICE', 'MEDIA_URL', 'LOADING_MESSAGE', '$timeout', function ($scope, $http, API_URL, ngDialog, $stateParams, API_SERVICE, MEDIA_URL, LOADING_MESSAGE, $timeout) {
    $scope.successAlert = false;
    $scope.showPopUp = false;
    $scope.faliureAlert = false;
    $scope.deactivateButtonName = "Yes";
    $scope.loadingMessage = LOADING_MESSAGE;
    //$scope.changeBreadcrums([{ "name": "Beacons", "state": "master.beacons" }, { "name": "Beacon Details", "state": "master.beaconDetails", "params": { "beaconId": $stateParams.beaconDetail.id } }]);
    var table = null;
    var defaultOfferTable = null;
    $scope.getLuckyItemPic = function (icon) {
        $scope.imagePath = MEDIA_URL + icon;
        return $scope.imagePath;
    }
    //----------------------- Deasctivate active offer for beacon modal -------------------------//
    $scope.deactivateOffer = function (offerId) {
        $scope.deactivateOfferModel = {"offerId": offerId };
        ngDialog.open({
            template: 'deactivateOfferDialog',
            className: 'ngdialog-theme-default modal-large store-dialog draggable',
            scope: $scope,
            closeByDocument: false,
            preCloseCallback: function () { $scope.cleardeactivateModal() }

        });
        $timeout(function () {
            $(".draggable .ngdialog-content").draggable();
        }, 500);
    }

    $scope.cleardeactivateModal = function () {
        $scope.submitted = false;
        $scope.successAlert = false;
        $scope.faliureAlert = false;
        
    }

    $scope.closeDeactivateModal = function () {
        ngDialog.close({
            template: 'deactivateOfferDialog',
            scope: $scope
        });
        $scope.cleardeactivateModal();
    }
    //-------------------------------------------------------------------//

    $scope.copyUrlToClipboard = function (text) {
        if (window.clipboardData && window.clipboardData.setData) {
            $scope.copyButtonText = "Copied";
            $timeout(function () {
                $scope.copyButtonText = "Copy";
            }, 1000);
            return clipboardData.setData("Text", text);

        } else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
            var textarea = document.createElement("textarea");
            textarea.textContent = text;
            $scope.copyButtonText = "Copied";
            $timeout(function () {
                $scope.copyButtonText = "Copy";
            }, 1000);
            textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in MS Edge.
            document.body.appendChild(textarea);
            textarea.select();
            try {

                return document.execCommand("copy");  // Security exception may be thrown by some browsers.
            } catch (ex) {
                console.warn("Copy to clipboard failed.", ex);
                return false;
            } finally {
                document.body.removeChild(textarea);
            }
        }
    }

    $scope.setDefaultOfferMember = function () {
        ngDialog.open({
            template: 'setDefaultOfferMemberDialog',
            className: 'ngdialog-theme-default modal-large resizeable draggable',
            scope: $scope,
            closeByDocument: false,
            preCloseCallback: function () { $scope.clearSetDefaultOfferMember() }
        });
        $timeout(function () {
            $(".resizeable .ngdialog-content").resizable({
                alsoResize: "#also-resize"
            });
            $(".draggable .ngdialog-content").draggable();
        }, 500);
    }
    $scope.closeSetDefaultOfferMember = function () {
        ngDialog.close({
            template: 'setDefaultOfferMemberDialog',
            scope: $scope,
        });
        $scope.clearSetDefaultOfferMember();
    }
    $scope.clearSetDefaultOfferMember = function () {
        $scope.successAlert = false;
        $scope.faliureAlert = false;
    }

    $scope.setDefaultOfferNonMember = function () {
        ngDialog.open({
            template: 'setDefaultOfferNonMemberDialog',
            className: 'ngdialog-theme-default modal-large resizeable draggable',
            scope: $scope,
            closeByDocument: false,
            preCloseCallback: function () { $scope.clearSetDefaultOfferNonMember() }
        });
        $timeout(function () {
            $(".resizeable .ngdialog-content").resizable({
                alsoResize: "#also-resize"
            });
            $(".draggable .ngdialog-content").draggable();
        }, 500);
    }
    $scope.closeSetDefaultOfferNonMember = function () {
        ngDialog.close({
            template: 'setDefaultOfferNonMemberDialog',
            scope: $scope,
        });
        $scope.clearSetDefaultOfferNonMember();
    }
    $scope.clearSetDefaultOfferNonMember = function () {
        $scope.successAlert = false;
        $scope.faliureAlert = false;
    }



    $scope.setOffer = function () {
        ngDialog.open({
            template: 'setOfferDialog',
            className: 'ngdialog-theme-default modal-large resizeable draggable',
            scope: $scope,
            closeByDocument: false,
            preCloseCallback: function () { $scope.clearModal() }
        });
        $timeout(function () {
            $(".resizeable .ngdialog-content").resizable({ alsoResize: "#also-resize" });
            $(".draggable .ngdialog-content").draggable();
        }, 500);
    }
    
    $scope.closeModal = function () {
        ngDialog.close({
            template: 'setOfferDialog',
            scope: $scope,
        });
        $scope.clearModal();
    }

    $scope.clearModal = function () {
        $scope.successAlert = false;
        $scope.faliureAlert = false;
    }

    function getDefaultOffer(offerType, success_res, failure_res) {
        $scope.loading = true;
        $scope.defaultOfferLoader = 0;
        function success(data, status, headers, config) {
            for (i = 0; i < data.length; i++)
            {
                if(data[i].OfferType=="MEMBER")
                {
                    $scope.defaultOfferMember = data[i].offer;
                }
                else if (data[i].OfferType == "NON_MEMBER")
                {
                    $scope.defaultOfferNonMember = data[i].offer;
                }
            }
            $scope.loading = false;
            $scope.defaultOfferLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.defaultOfferLoader = 2;
        }
        var url = "/Offers/getBeaconDfaultOffer?beacon_id=" + $stateParams.beaconId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getDefaultOffer();
    
    function getBeaconInfo() {
        $scope.loading = true;
        $scope.dataLoading = 0;
        function success(data, status, headers, config) {
            $scope.beaconInfo = data;
            console.log($scope.offer);
            $scope.loading = false;
            $scope.dataLoading = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.dataLoading = 2;
        }
        var url = "/beacons/details?beaconId=" + $stateParams.beaconId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getBeaconInfo();


    function getNonDefaultOffers(offerType, success_res, failure_res) {
        function success(data, status, headers, config) {
            success_res(data, status, headers, config);
        }
        function failure(data, status, headers, config) {
            failure_res(data, status, headers, config);
        }
        var url = "/Offers/getBeaconNonDefaultOffers?beacon_id=" + $stateParams.beaconId + "&&offer_type=" + offerType;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    function getNonDefaultOffersMember() {
        $scope.loading = true;
        $scope.nonDefaultOffersLoaderMember = 0;
        function success(data, status, headers, config) {
            $scope.nonDefaultOffersMember = data;
            $scope.loading = false;
            $scope.nonDefaultOffersLoaderMember = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.nonDefaultOffersLoaderMember = 2;
        }
        getNonDefaultOffers(1, success, failure);
    }
    getNonDefaultOffersMember();
    function getNonDefaultOffersNonMember() {
        $scope.loading = true;
        $scope.nonDefaultOffersLoaderNonMember = 0;
        function success(data, status, headers, config) {
            $scope.nonDefaultOffersNonMember = data;
            $scope.loading = false;
            $scope.nonDefaultOffersLoaderNonMember = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.nonDefaultOffersLoaderNonMember = 2;
        }
        getNonDefaultOffers(2, success, failure);
    }
    getNonDefaultOffersNonMember();
    function getInactiveOffers() {
        $scope.loading = true;
        $scope.inactiveOffersLoader = 0;
        function success(data, status, headers, config) {
            $scope.inactiveOffers = data;
            console.log($scope.inactiveOffers);
            $scope.loading = false;
            $scope.inactiveOffersLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.inactiveOffersLoader = 2;
        }
        var url = "/Offers/getBeaconInactiveOffers?beacon_id=" + $stateParams.beaconId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getInactiveOffers();

    function getActiveOffers() {
        $scope.loading = true;
        $scope.activeOfferLoader = 0;
        function success(data, status, headers, config) {
            $scope.activeOffers = data;
            console.log($scope.activeOffers);
            $scope.loading = false;
            $scope.activeOfferLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.activeOfferLoader = 2;
        }
        var url = "/Offers/getBeaconActiveOffers?beacon_id=" + $stateParams.beaconId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getActiveOffers();

    function getOffersHistory() {
        $scope.loading = true;
        $scope.offerHistoryLoader = 0;
        function success(data, status, headers, config) {
            $scope.offersHistory = data;
            console.log($scope.activeOffers);
            $scope.loading = false;
            $scope.offerHistoryLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.offerHistoryLoader = 2;
        }
        var url = "/Offers/getBeaconOffersHistory?beacon_id=" + $stateParams.beaconId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getOffersHistory();

    setNonDefaultActive = function (offerId, offerType, success_res, failure_res) {
        function success(data, status, headers, config) {
            getDefaultOffer(); 
            success_res(data, status, headers, config);    
        }
        function failure(data, status, headers, config) {
            failure_res(data, status, headers, config);   
        }
        var url = "/Offers/changeBeaconDefaultOffer?beacon_id=" + $stateParams.beaconId + "&&offer_id=" + offerId + "&&offer_type=" + offerType;
        API_SERVICE.postData($scope, $http, "", url, success, failure);
    }
    $scope.setNonDefaultActiveMember = function (offerId) {
        function success(data, status, headers, config) {
            getNonDefaultOffersMember();
            $scope.closeSetDefaultOfferMember();
            $scope.notify("Successfully changed default offer for member.", 1);
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.successAlert = false;
            $scope.faliureAlert = true;
        }
        setNonDefaultActive(offerId, 1, success, failure);
    }
    $scope.setNonDefaultActiveNonMember = function (offerId) {
        function success(data, status, headers, config) {
            getNonDefaultOffersNonMember();
            $scope.closeSetDefaultOfferNonMember();
            $scope.notify("Successfully changed default offer for non-member.", 1);
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.successAlert = false;
            $scope.faliureAlert = true;
        }
        setNonDefaultActive(offerId, 2, success, failure);
    }

    $scope.isOfferActive = function (isOfferActive) {
        if (isOfferActive)
            isOfferActive = "Active";
        else
            isOfferActive = "-";
        return isOfferActive;
    }

    //------------reload-------------------------//
    $scope.reloadData = function () {
        getBeaconInfo();
    }

    $scope.reloadDefaultOffersList = function () {
        getDefaultOffer();
    }

    $scope.reloadActiveOffersList = function () {
        getActiveOffers();
    }

    $scope.reloadOffersHistory = function () {
        getOffersHistory();
    }

    $scope.reloadNonDefaultOffersListMember = function () {
        getNonDefaultOffersMember();
    }
    $scope.reloadNonDefaultOffersListNonMember = function () {
        getNonDefaultOffersNonMember();
    }

    $scope.reloadInactiveOffersList = function () {
        getInactiveOffers();
    }
    //-------------------- Deactivate offer ------------------------//
    $scope.deactivateOfferAPI = function (offerId) {
        $scope.deactivateButtonName = "Deactivating";
        function success(data, status, headers, config) {
            getActiveOffers();
            $scope.notify("Offer deactivated successfully.", 1);
            getInactiveOffers();
            $scope.successAlert = true;
            $scope.faliureAlert = false;
            $scope.closeDeactivateModal();
            $scope.deactivateOfferModel = {};
            $scope.deactivateButtonName = "Yes";
        }
        function failure(data, status, headers, config) {
            $scope.successAlert = false;
            $scope.faliureAlert = true;
            $scope.deactivateOfferModel = {};
            $scope.deactivateButtonName = "Yes";
        }
        var url = "/Offers/deactivateBeaconOffer?beaconId=" + $stateParams.beaconId + "&&offerId=" + offerId;
        API_SERVICE.postData($scope, $http, "", url, success, failure);
    }

    $scope.setActive = function (offerId) {
        function success(data, status, headers, config) {
            getActiveOffers();
            getInactiveOffers();
            $scope.successAlert = true;
            $scope.faliureAlert = false;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.successAlert = false;
            $scope.faliureAlert = true;
        }
        var url = "/Offers/setOfferForBeacon?beacon_id=" + $stateParams.beaconId + "&&offer_id=" + offerId;
        API_SERVICE.postData($scope, $http, "", url, success, failure);
    }

}])