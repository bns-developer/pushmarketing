﻿angular.module('bns.master.socialLogin', [])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.socialLogin', {
        url: '/socialLogin',
        views: {
            "socialLogin": {
                templateUrl: 'App/Master/SocialLogin/socialLogin.html',
                controller: 'socialLoginController'
            }
        }
    });
}])
.controller("socialLoginController", ['$scope', '$state', '$http', '$timeout', 'API_URL', 'API_SERVICE', 'MEDIA_URL', 'ngDialog', 'MEDIA_URL', function ($scope, $state, $http, $timeout, API_URL, API_SERVICE, MEDIA_URL, ngDialog, MEDIA_URL) {

    $scope.changeBreadcrums([{ "name": "Social Login", "state": "bns.master.socialLogin" }]);
    $scope.deleteButtonName = "Yes";
    $scope.confirmButtonName = "Confirm";
    $scope.showScocailLogin = false;
    $scope.mediaUrl = MEDIA_URL;
    $scope.socialLoginProvider = {
        "serviceId": ""
    };
    $scope.addedSocialLoginProviders = [];

    //$scope.listOfProvider = "facebook,twitter,googleplus,linkedin,yahoo,microsoft";
    $scope.listOfProvider = "";
    $scope.socailLoginProviders = [
        { "name": "Facebook", "logoPath": "/Resources/socialLogin/facebook.png", "isEnabled": false, "img_url": "https://cdns.gigya.com/gs/i/HTMLLogin/FullLogoColored/facebook_120.png" },
        { "name": "Twitter", "logoPath": "/Resources/socialLogin/twitter.png", "isEnabled": false, "img_url": "https://cdns.gigya.com/gs/i/HTMLLogin/FullLogoColored/twitter_120.png" },
        { "name": "Google", "logoPath": "/Resources/socialLogin/GooglePlus.png", "isEnabled": false, "img_url": "https://cdns.gigya.com/gs/i/HTMLLogin/FullLogoColored/google_120.png" },
        { "name": "LinkedIn", "logoPath": "/Resources/socialLogin/linkedin.png", "isEnabled": false, "img_url": "https://cdns.gigya.com/gs/i/HTMLLogin/FullLogoColored/linkedIn_120.png" },
        { "name": "Yahoo", "logoPath": "/Resources/socialLogin/yahoo.png", "isEnabled": false, "img_url": "https://cdns.gigya.com/gs/i/HTMLLogin/FullLogoColored/yahoo_120.png" },
        { "name": "Microsoft", "logoPath": "/Resources/socialLogin/messenger.png", "isEnabled": false, "img_url": "https://cdns.gigya.com/gs/i/HTMLLogin/FullLogoColored/microsoft_120.png" },
        { "name": "Foursquare", "logoPath": "/Resources/socialLogin/foursquare.png", "isEnabled": false, "img_url": "https://cdns.gigya.com/gs/i/HTMLLogin/FullLogoColored/fourSquare_120.png" },
        { "name": "Renren", "logoPath": "/Resources/socialLogin/renren.png", "isEnabled": false, "img_url": "https://cdns.gigya.com/gs/i/HTMLLogin/FullLogoColored/renren_120.png" },
        { "name": "QQ", "logoPath": "/Resources/socialLogin/qq.png", "isEnabled": false, "img_url": "https://cdns.gigya.com/gs/i/HTMLLogin/FullLogoColored/qq_120.png" },
        { "name": "Sina", "logoPath": "/Resources/socialLogin/sina.png", "isEnabled": false, "img_url": "https://cdns.gigya.com/gs/i/HTMLLogin/FullLogoColored/sina_120.png" },
        { "name": "VKontakte", "logoPath": "/Resources/socialLogin/vkontakte.png", "isEnabled": false, "img_url": "https://cdns.gigya.com/gs/i/HTMLLogin/FullLogoColored/vkontakte_120.png" },
        { "name": "Mixi", "logoPath": "/Resources/socialLogin/mixi.png", "isEnabled": false, "img_url": "https://cdns.gigya.com/gs/i/HTMLLogin/FullLogoColored/mixi_120.png" },
        { "name": "Yahoo Japan", "logoPath": "/Resources/socialLogin/YahooJapan.png", "isEnabled": false, "img_url": "https://cdns.gigya.com/gs/i/HTMLLogin/FullLogoColored/YahooJapan_120.png" },
        { "name": "Spiceworks", "logoPath": "/Resources/socialLogin/spiceworks.png", "isEnabled": false, "img_url": "https://cdns.gigya.com/gs/i/HTMLLogin/FullLogoColored/spiceworks_120.png" },
        { "name": "Instagram", "logoPath": "/Resources/socialLogin/instagram.png", "isEnabled": false, "img_url": "https://cdns.gigya.com/gs/i/HTMLLogin/FullLogoColored/instagram_120.png" },
        { "name": "Odnoklassniki", "logoPath": "/Resources/socialLogin/odnoklassniki.png", "isEnabled": false, "img_url": "https://cdns.gigya.com/gs/i/HTMLLogin/FullLogoColored/odnoklassniki_120.png" },
        { "name": "Amazon", "logoPath": "/Resources/socialLogin/amazon.png", "isEnabled": false, "img_url": "https://cdns.gigya.com/gs/i/HTMLLogin/FullLogoColored/amazon_120.png" },
        { "name": "Xing", "logoPath": "/Resources/socialLogin/xing.png", "isEnabled": false, "img_url": "https://cdns.gigya.com/gs/i/HTMLLogin/FullLogoColored/xing_120.png" },
        { "name": "WeChat", "logoPath": "/Resources/socialLogin/wechat.png", "isEnabled": false, "img_url": "https://cdns.gigya.com/gs/i/HTMLLogin/FullLogoColored/wechat_120.png" },
        { "name": "Wordpress", "logoPath": "/Resources/socialLogin/wordpress.png", "isEnabled": false, "img_url": "https://cdns.gigya.com/gs/i/HTMLLogin/FullLogoColored/wordpress_120.png" },
        { "name": "Blogger", "logoPath": "/Resources/socialLogin/blogger.png", "isEnabled": false, "img_url": "https://cdns.gigya.com/gs/i/HTMLLogin/FullLogoColored/blogger_120.png" },
        { "name": "PayPal", "logoPath": "/Resources/socialLogin/paypal.png", "isEnabled": false, "img_url":"https://cdns.gigya.com/gs/i/HTMLLogin/FullLogoColored/paypal_120.png" },
        { "name": "PayPalOAuth", "logoPath": "/Resources/socialLogin/Paypaloauth.png", "isEnabled": false, "img_url": "https://cdns.gigya.com/gs/i/HTMLLogin/FullLogoColored/Paypaloauth_120.png" },
        { "name": "Netlog", "logoPath": "/Resources/socialLogin/netlog.png", "isEnabled": false, "img_url": "https://cdns.gigya.com/gs/i/HTMLLogin/FullLogoColored/netlog_120.png" },
        { "name": "line", "logoPath": "/Resources/socialLogin/line.png", "isEnabled": false, "img_url": "https://cdns.gigya.com/gs/i/HTMLLogin/FullLogoColored/line_120.png" },
        { "name": "Livedoor", "logoPath": "/Resources/socialLogin/livedoor.png", "isEnabled": false, "img_url": "https://cdns.gigya.com/gs/i/HTMLLogin/FullLogoColored/livedoor_120.png" },
        { "name": "AOL", "logoPath": "/Resources/socialLogin/aol.png", "isEnabled": false, "img_url": "https://cdns.gigya.com/gs/i/HTMLLogin/FullLogoColored/aol_120.png" },
        { "name": "OrangeFrance", "logoPath": "/Resources/socialLogin/OrangeFrance.png", "isEnabled": false, "img_url": "https://cdns.gigya.com/gs/i/HTMLLogin/FullLogoColored/OrangeFrance_120.png" }
    ];

    //----------------------- Add Social login modal -------------------------//
    $scope.openAddSocialLoginModal = function () {
        ngDialog.open({
            template: 'addSocialLogin',
            className: 'ngdialog-theme-default modal-large coupon-code-checklist draggable',
            scope: $scope,
            closeByDocument: false,
            preCloseCallback: function () { $scope.clearAddSocialLoginModal() }

        });
        $timeout(function () {
            $(".draggable .ngdialog-content").draggable();
        }, 500);
    }

    $scope.clearAddSocialLoginModal = function () {
        $scope.submitted = false;
        $scope.successAlert = false;
        $scope.faliureAlert = false;
        $scope.index = null;
    }

    $scope.closeAddSocialLoginModal = function () {
        ngDialog.close({
            template: 'addSocialLogin',
            scope: $scope
        });
        $scope.clearAddSocialLoginModal();
    }

    //----------------------- Confirm Add Social login modal -------------------------//
    $scope.openConfirmAddSocialLoginModal = function (socialLoginProvider) {
        $scope.socialLoginProvider.serviceId = socialLoginProvider.id;
        $scope.selectedSocialLoginProivder = socialLoginProvider;
        ngDialog.open({
            template: 'confirmAddSocialLogin',
            className: 'ngdialog-theme-default modal-large store-dialog draggable',
            scope: $scope,
            closeByDocument: false,
            preCloseCallback: function () { $scope.clearConfirmAddSocialLoginModal() }

        });
        $timeout(function () {
            $(".draggable .ngdialog-content").draggable();
        }, 500);
    }

    $scope.clearConfirmAddSocialLoginModal = function () {
        $scope.socialLoginProvider = {
            "serviceId": ""
        };
        $scope.submitted = false;
        $scope.successAlert = false;
        $scope.faliureAlert = false;
        $scope.index = null;
    }

    $scope.closeConfirmAddSocialLoginModal = function () {
        ngDialog.close({
            template: 'confirmAddSocialLogin',
            scope: $scope
        });
        $scope.clearConfirmAddSocialLoginModal();
    }
    //--------------------logout from gigya socail login -------------------------------------//
    $scope.logoutFromGS = function () {
        gigya.socialize.logout(); // logout from Gigya platform
    }
    //----------------------------------------- Gigya socail login------------------------------------------------------------//

    var onLoginHandler = function (eventObj) {
        alert(eventObj.context.str + ' ' + eventObj.eventName + ' to ' + eventObj.provider
            + '!\n' + eventObj.provider + ' user ID: ' + eventObj.user.identities[eventObj.provider].providerUID);
        // verify the signature ...
        verifyTheSignature(eventObj.UID, eventObj.timestamp, eventObj.signature);
        // Check whether the user is new by searching if eventObj.UID exists in your database
        var newUser = true; // lets assume the user is new

        if (newUser) {
            // 1. Register user
            // 2. Store new user in DB
            // 3. link site account to social network identity
            // 3.1 first construct the linkAccounts parameters
            var dateStr = Math.round(new Date().getTime() / 1000.0); // Current time in Unix format
            //(i.e. the number of seconds since Jan. 1st 1970)

            var siteUID = 'uTtCGqDTEtcZMGL08w'; // siteUID should be taken from the new user record
            // you have stored in your DB in the previous step
            var yourSig = createSignature(siteUID, dateStr);
            var params = {
                siteUID: siteUID,
                timestamp: dateStr,
                cid: '',
                signature: yourSig
            };

            //   3.1 call linkAccounts method:
            gigya.socialize.notifyRegistration(params);
        }

        document.getElementById('status').style.color = "green";
        document.getElementById('status').innerHTML = "Status: You are now signed in";
    }

    var onLogoutHandler = function(eventObj) {
        document.getElementById('status').style.color = "red";
        document.getElementById('status').innerHTML = "Status: You are now signed out";
    }

    var onLoadFunction = function () {
            // register for login event
            gigya.socialize.addEventHandlers({
                    context: { str: 'congrats on your' }
                    , onLogin: onLoginHandler
                    , onLogout: onLogoutHandler
                    });
    }

    var createSignature = function(UID, timestamp) {
        encodedUID = encodeURIComponent(UID); // encode the UID parameter before sending it to the server.
        // On server side use decodeURIComponent() function to decode an encoded UID
        return '';
    }

    // Note: the actual signature calculation implementation should be on server side
    function verifyTheSignature(UID, timestamp, signature) {
        encodedUID = encodeURIComponent(UID); // encode the UID parameter before sending it to the server.
        // On server side use decodeURIComponent() function to decode an encoded UID
        alert('Your UID: ' + UID + '\n timestamp: ' + timestamp + '\n signature: ' + signature + '\n Your UID encoded: ' + encodedUID);
    }

    function initializeGigya() {

        $scope.gigyaParams = {
            version: 2,
            showTermsLink: false, // 'terms' link is hidden
            //headerText: "Please Login using one of the following providers:", // adding header text
            height: 145, // changing default add-on size
            width: '100%',  // changing default add-on size
            cid: '',
            extraFields: 'phones',
            deviceType: 'mobile',
            lastLoginIndication: 'border',
            enabledProviders: $scope.listOfProvider,
            containerID: "loginDiv2", // The add-on will embed itself inside the "loginDiv" DIV (will not be a popup)
            // Changes to the default design of the add-on's design 
            //     Background color is changed to purple, text color to gray and button size is set to 40 pixels:   
            UIConfig: '<config><body><texts color="#DFDFDF"></texts><controls><snbuttons buttonsize="40"></snbuttons></controls><background background-color="#51286D"></background></body></config>',
            // Change the buttons design style to the 'fullLogo' style:
            onLoad: onLoadFunction,
            pagingButtonStyle: 'floating'
            // After successful login - the user will be redirected to "https://www.MySite.com/welcome.html" :  
        };
        $scope.socialUI = gigya.socialize.showLoginUI($scope.gigyaParams);

    }
    //initializeGigya();

    $scope.addSocialLogin = function (index) {
        if (index > -1)
        {
            $scope.socailLoginProviders[index].isEnabled = true;
            $scope.closeAddSocialLoginModal();
            if ($scope.listOfProvider.length > 0)
                $scope.listOfProvider = $scope.listOfProvider + "," + $scope.socailLoginProviders[index].name;
            else
                $scope.listOfProvider = $scope.socailLoginProviders[index].name;
            //---------------   add item to list of social login -----------------------------//
            $scope.addedSocialLoginProviders.push($scope.socailLoginProviders[index]);
            $scope.socailLoginProviders.splice(index,1);
            //initializeGigya();
        }  
    }

    function getSocialLoginProviders()
    {
        $scope.addedSocialLoginProviderLoader = 1;
        $scope.socialLoginProviderLoader = 1;
    }
    getSocialLoginProviders();
    $scope.reloadSocialLoginProviderData = function()
    {
        console.log("Reload function under progress");
    }

    //************************   Social Login API's *********************************//
    getAllSocialLoginProviders = function () {
        $scope.allSocialLoginPorivdersLoader = 0;
        function success(data, status, headers, config) {
            $scope.allSocialLoginProviders = data;
            $scope.allSocialLoginPorivdersLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.errorMessage = data;
            $scope.allSocialLoginPorivdersLoader = 2;
        }
        var url = "/socialNetworkingServices/getAll";
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getAllSocialLoginProviders();

    $scope.reloadAllSocialLoginProviders = function()
    {
        getAllSocialLoginProviders();
    }

    getSelectedSocialLoginProviders = function () {
        $scope.selectedSocialLoginProvidersLoader = 0;
        function success(data, status, headers, config) {
            $scope.selectedSocialLoginProviders = data;
            $scope.selectedSocialLoginProvidersLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.errorMessage = data;
            $scope.selectedSocialLoginProvidersLoader = 2;
        }
        var url = "/socialNetworkingServices/getSelectedServices";
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getSelectedSocialLoginProviders();

    $scope.reloadSelectedSocialLoginProviders = function()
    {
        getSelectedSocialLoginProviders();
    }

    getUnSelectedSocialLoginProviders = function () {
        $scope.unSelectedSocialLoginProvidersLoader = 0;
        function success(data, status, headers, config) {
            $scope.unSelectedSocialLoginProviders = data;
            $scope.unSelectedSocialLoginProvidersLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.errorMessage = data;
            $scope.unSelectedSocialLoginProvidersLoader = 2;
        }
        var url = "/socialNetworkingServices/getUnselectedServices";
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getUnSelectedSocialLoginProviders();

    $scope.ReloadUnSelectedSocialLoginProviders = function () {
        getUnSelectedSocialLoginProviders();
    }

    $scope.addSocialLoginProvider = function () {
        $scope.confirmButtonName = "Processing...";
        function success(data, status, headers, config) {
            getUnSelectedSocialLoginProviders();
            getAllSocialLoginProviders();
            getSelectedSocialLoginProviders();
            $scope.notify("Social login provider added successfully to the selected list", 1);
            $scope.successAlert = true;
            $scope.faliureAlert = false;
            $scope.loading = false;
            $scope.confirmButtonName = "Confirm";
            $scope.closeConfirmAddSocialLoginModal();

        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.successAlert = false;
            $scope.faliureAlert = true;
            $scope.confirmButtonName = "Confirm";
            if (status == 409) {
                $scope.errorMessage = data;
                //$scope.notify(data, 4);
            }
            else {
                $scope.errorMessage = "Failed to add Social Login provider.";
                //$scope.notify($scope.errorMessage, 4);
            }
            $scope.closeConfirmAddSocialLoginModal();
        }
        var url = "/socialNetworkingServices/add";
        API_SERVICE.postData($scope, $http, $scope.socialLoginProvider, url, success, failure);
    }


}]);