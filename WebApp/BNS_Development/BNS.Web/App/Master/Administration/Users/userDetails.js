﻿/// <reference path="userDetails.html" />
angular.module('bns.master.administrationUserDetails', [])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.administrationUserDetails', {
        url: '/administration/userDetails',
        views: {
            "administrationUserDetails": {
                templateUrl: 'App/Master/Administration/Users/userDetails.html',
                controller: 'administrationUserDetailsController'
            }
        }
    });
}])
.controller("administrationUserDetailsController", ['$scope', '$http', 'API_URL', 'LOADING_MESSAGE', 'ngDialog', 'API_SERVICE', '$state', '$timeout',
    function ($scope, $http, API_URL, LOADING_MESSAGE, ngDialog, API_SERVICE, $state, $timeout) {
        $scope.changeBreadcrums([{ "name": "Administration", "state": "master.administration" }, { "name": "User Details", "state": "master.administration.userDetails" }]);
        $scope.rolesUpdateDropDownSetting = { disable: true, smartButtonMaxItems: 5, smartButtonTextConverter: function (itemText, originalItem) { return itemText; } };
        $scope.rolesUpdateButtonText = { buttonDefaultText: 'Choose Countries...', checkAll: 'Select All', uncheckAll: 'Deselect All' };
    }]);