﻿angular.module('bns.master.administration', ['ui.bootstrap'])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.administration', {
        url: '/administration',
        views: {
            "administration": {
                templateUrl: 'App/Master/Administration/administration.html',
                controller: 'administrationController'
            }
        }
    });
}])
.controller("administrationController", ['$scope', '$http', '$window', 'API_URL', 'LOADING_MESSAGE', 'ngDialog', 'API_SERVICE', '$state', '$timeout', 'DTOptionsBuilder', 'DTColumnDefBuilder',
    function ($scope, $http, $window, API_URL, LOADING_MESSAGE, ngDialog, API_SERVICE, $state, $timeout, DTOptionsBuilder, DTColumnDefBuilder) {
        $scope.changeBreadcrums([{ "name": "Administration", "state": "master.administration" }]);
        $scope.roleId = 1;
        $scope.rolePermissions = {
            "login": { "loginPermission": true },
            "dashboard": { "viewDashboard": true },
            "store": { "viewStoreList": true, "viewStoreDetails": true, "addStore": true, "editOrUpdateStore": true, "deleteStore": true }
        };
        $scope.saveButtonName = "Save";
        $scope.deleteButtonName = "Yes";
        $scope.updateButtonName = "Update";
        $scope.updatePermissionsButtonName = "Update";
        $scope.addRoleButtonName = "+ Role";
        $scope.dataLoader = 1;
        $scope.rolesButtonText = { buttonDefaultText: 'Choose Roles...', checkAll: 'Select All', uncheckAll: 'Deselect All' };
        $scope.rolesUpdateButtonText = { buttonDefaultText: 'Choose Roles...', checkAll: 'Select All', uncheckAll: 'Deselect All' };
        $scope.rolesDropDownSetting = { smartButtonMaxItems: 5, smartButtonTextConverter: function (itemText, originalItem) { return itemText; } };
        $scope.rolesUpdateDropDownSetting = { smartButtonMaxItems: 5, smartButtonTextConverter: function (itemText, originalItem) { return itemText; } };
        $scope.roles = [{ "id": "1", "label": "Admin" }, { "id": "2", "label": "User" }, { "id": "3", "label": "Store Keeper" }];

        $scope.dtOptionsAdministration = DTOptionsBuilder.newOptions()
       .withDOM('lrftip')
       .withPaginationType('full_numbers')
       .withOption('responsive', true)
       .withOption('scrollX', true)
       //.withOption('bAutoWidth', false)
       //.withOption('scrollCollapse', 'true')
       .withOption('order', [])
       .withButtons([
           {
               extend: 'csvHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Administration List of users ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3, 4, 5]
               }
           },
           {
               extend: 'pdfHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Administration List of users ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3, 4, 5]
               }
           },
           {
               extend: 'excelHtml5',
               orientation: 'landscape',
               title: $window.document.title + ' :- Administration List of users ' + moment().format('MMMM Do YYYY, h:mm:ss a'),
               pageSize: 'LEGAL',
               exportOptions: {
                   columns: [0, 1, 2, 3, 4, 5]
               }
           },
           'print']);
        $scope.dtColumnDefsAdministrationUserList = [DTColumnDefBuilder.newColumnDef(6).notSortable()];
            
        
        $scope.newUser = {
            "firstName": "",
            "lastName": "",
            "userName": "",
            "roles": [],
            "email": "",
            "contact": "",
            "isActive": false,
            "notes":""
        };

        $scope.updateUser = {
            "firstName": "John",
            "lastName": "Smith",
            "userName": "JSmith",
            "roles": [],
            "email": "j@smiths.com",
            "contact": "4038472647",
            "isActive": true,
            "notes": "It is for test purpose"
        };

        $scope.viewUser = {
            "firstName": "John",
            "lastName": "Smith",
            "userName": "JSmith",
            "roles": [],
            "email": "j@smiths.com",
            "contact": "4038472647",
            "isActive": true,
            "notes": "It is for test purpose"
        };
        //------------------------ selectRole ----------------------------------------------//
        function getPermissionForRole(roleId) {
            var permissions = {};
            switch(roleId)
            {
                case "1":
                    permissions =
                        {
                            "login": {"loginPermission":true},
                            "dashboard": {"viewDashboard":true},
                            "store": {"viewStoreList": true, "viewStoreDetails": true, "addStore": true, "editOrUpdateStore": true, "deleteStore": true}
                        };
                    break;
                case "2":
                     permissions =
                        {
                            "login": {"loginPermission":true},
                            "dashboard": {"viewDashboard":true},
                            "store": {"viewStoreList": true, "viewStoreDetails": true, "addStore": true, "editOrUpdateStore": true, "deleteStore": false}
                        };
                    break;
                case "3":
                    permissions =
                       {
                           "login": { "loginPermission": true },
                           "dashboard": { "viewDashboard": true },
                           "store": { "viewStoreList": true, "viewStoreDetails": true, "addStore": false, "editOrUpdateStore": false, "deleteStore": false }
                       };
                    break;
                default:
                    permissions =
                      { "errMsg": "failure" };
                    break;
            }

            return permissions;

        }
        $scope.selectRole = function()
        {
            $scope.rolePermissions = getPermissionForRole($scope.roleId);
        }
        $scope.selectRole();
       
        //------------------------   Add User Modal ----------------------------------------//
        $scope.openAddUserModal = function () {
            ngDialog.open({
                template: 'addUserDialog',
                className: 'ngdialog-theme-default modal-large draggable',
                scope: $scope,
                closeByDocument: false,
                preCloseCallback: function () { $scope.clearAddUserModal() }

            });
            $timeout(function () {
                $(".draggable .ngdialog-content").draggable();
            }, 500);
        }

        $scope.closeAddUserModal = function () {
            ngDialog.close({
                template: 'addUserDialog',
                scope: $scope
            });
            $scope.clearModal();
        }

        $scope.clearAddUserModal = function () {
            $scope.submitted = false;
            $scope.successAlert = false;
            $scope.faliureAlert = false;
            $scope.newUser = {
                "firstName": "",
                "lastName": "",
                "userName": "",
                "roles": "",
                "email": "",
                "contact": "",
                "isActive": false,
                "contact": "",
                "notes": ""
            };
        }

        //------------------------   Update User Modal -------------------------------------//
        $scope.openUpdateUserModal = function () {

            $scope.updateUser = {
                "firstName": "John",
                "lastName": "Smith",
                "userName": "JSmith",
                "roles": [{ "id": "1" }],
                "email": "j@smiths.com",
                "contact": "4038472647",
                "isActive": true,
                "notes": "It is for test purpose"
            };
            ngDialog.open({
                template: 'updateUserDialog',
                className: 'ngdialog-theme-default modal-large draggable',
                scope: $scope,
                closeByDocument: false,
                preCloseCallback: function () { $scope.clearUpdateUserModal() }

            });
            $timeout(function () {
                $(".draggable .ngdialog-content").draggable();
            }, 500);
        }

        $scope.closeUpdateUserModal = function () {
            ngDialog.close({
                template: 'updateUserDialog',
                scope: $scope
            });
            $scope.clearModal();
        }

        $scope.clearUpdateUserModal = function () {
            $scope.submitted = false;
            $scope.successAlert = false;
            $scope.faliureAlert = false;
            $scope.updateUser = {
                "firstName": "",
                "lastName": "",
                "userName": "",
                "roles": "",
                "email": "",
                "contact": "",
                "isActive": false,
                "contact": "",
                "notes": ""
            };
        }

        //------------------------    Delete User Modal ------------------------------------//
        $scope.openDeleteUserModal = function () {
            $scope.deleteUserModel = { "userName": "John" };
            ngDialog.open({
                template: 'deleteUserDialog',
                className: 'ngdialog-theme-default modal-large store-dialog draggable',
                scope: $scope,
                closeByDocument: false,
                preCloseCallback: function () { $scope.clearDeleteModal() }

            });
            $timeout(function () {
                $(".draggable .ngdialog-content").draggable();
            }, 500);
        }

        $scope.clearDeleteModal = function () {
            $scope.submitted = false;
            $scope.successAlert = false;
            $scope.faliureAlert = false;
            $scope.deleteUserModel = { "userName": "" };
        }

        $scope.closeDeleteModal = function () {
            ngDialog.close({
                template: 'deleteUserDialog',
                scope: $scope
            });
            $scope.clearDeleteModal();
        }

        $scope.saveNewUser = function () {
            console.log($scope.newUser);
            console.log("save user API under progress!");
        }

        $scope.updateUserAPI = function () {
            console.log($scope.updateUser);
            console.log("Update user API under progress!");
        }

        $scope.deleteUserAPI = function () {
            console.log("Delete user API under progress!");
        }

        $scope.updatePermissions = function () {
            console.log("Update API for roles & permissions are under progress");
        }

        $scope.addRole = function () {
            console.log("Add role API under progress");
        }

        //-------------------  some accordian code ---------------------------------------//
}]);