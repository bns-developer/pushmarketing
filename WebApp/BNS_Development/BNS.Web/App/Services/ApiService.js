﻿angular.module('bns.API_SERVICE', ['bns.constants', 'ngFileUpload'])

.factory('API_SERVICE', function ($http, API_URL, TOKEN_SERVICE, Upload, $state) {

    function patchData($scope, $http, data, url, success_callback, error_callback, header) {
        var _header = ((header == null || header == undefined) ? { 'Content-Type': 'application/json' } : header);
        _header['Timezone'] = new Date().getTimezoneOffset();
        if (TOKEN_SERVICE.RestoreState() != undefined)
            _header['Authorization'] = TOKEN_SERVICE.RestoreState()['access_token'];
        $http({ method: 'patch', url: API_URL + url, data: data, headers: _header })
        .success(function (data, status, headers, config) {
            console.log("SUCCESS:" + status);
            success_callback(data, status, headers, config);
        }).
        error(function (data, status, headers, config) {
            console.log("ERROR:" + status);
            
            error_callback(data, status, headers, config);
        });
    };

    function getData($scope, $http, url, success_callback, error_callback, header) {
        var _header = ((header == null || header == undefined) ? { 'Content-Type': 'application/json' } : header);
        _header['Timezone'] = new Date().getTimezoneOffset();
        if (TOKEN_SERVICE.RestoreState() != undefined)
            _header['Authorization'] = TOKEN_SERVICE.RestoreState()['access_token'];

        $http({ method: 'get', url: API_URL + url, headers: _header })
            .success(function (data, status, headers, config) {
                console.log("SUCCESS:" + status);
                success_callback(data, status, headers, config);
            }).
        error(function (data, status, headers, config) {
            console.log("ERROR:" + status);
            if (status == 401) {
                TOKEN_SERVICE.Delete();
                $state.go('login');
            }
            else
              error_callback(data, status, headers, config);
        });
        //    .success(function (data, status, headers, config) {
        //    console.log("SUCCESS:"+status);
        //    success_callback(data, status, headers, config);
        //}).
        //error(function(data, status, headers, config) {
        //    console.log("ERROR:"+status);
        //    error_callback(data, status, headers, config);
        //});
    };

    function postData($scope, $http, data, url, success_callback, error_callback, header) {
        var _header = ((header == null || header == undefined) ? { 'Content-Type': 'application/json' } : header);
        _header['Timezone'] = new Date().getTimezoneOffset();
        if (TOKEN_SERVICE.RestoreState() != undefined)
            _header['Authorization'] = TOKEN_SERVICE.RestoreState()['access_token'];

        $http({ method: 'post', url: API_URL + url, data: data, headers: _header })
             .then(function successCallback(response) {
                 console.log("SUCCESS:");
                 success_callback(response.data, response.status, response.config.hearder, response.config);
             }, function errorCallback(response) {
                 console.log("ERROR:");
                 if (response.status == 401) {
                     TOKEN_SERVICE.Delete();
                     $state.go('login');
                 }
                 else
                    error_callback(response.data, response.status, response.config.hearder, response.config);
             })
    };

    function postLoginData($scope, $http, data, url, success_callback, error_callback, header) {
        var _header = ((header == null || header == undefined) ? { 'Content-Type': 'application/json' } : header);
        //_header['Timezone'] = new Date().getTimezoneOffset();
        $http({ method: 'post', url: API_URL + url, data: data, headers: _header })
             .then(function successCallback(response) {
                 console.log("SUCCESS:");
                 success_callback(response.data, response.status, response.config.hearder, response.config);
             }, function errorCallback(response) {
                 console.log("ERROR:");
                 if (response.status == 401) {
                     TOKEN_SERVICE.Delete();
                     $state.go('login');
                 }
                 else
                    error_callback(response.data, response.status, response.config.hearder, response.config);
             })
    };



    function putData($scope, $http, data, url, success_callback, error_callback, header) {
        var _header = ((header == null || header == undefined) ? { 'Content-Type': 'application/json' } : header);
        _header['Timezone'] = new Date().getTimezoneOffset();
        if (TOKEN_SERVICE.RestoreState() != undefined)
            _header['Authorization'] = TOKEN_SERVICE.RestoreState()['access_token'];

        $http({ method: 'put', url: API_URL + url, data: data, headers: _header })
        .success(function (data, status, headers, config) {
            console.log("SUCCESS:" + status);
            success_callback(data, status, headers, config);
        }).
        error(function (data, status, headers, config) {
            console.log("ERROR:" + status);
           
            error_callback(data, status, headers, config);
        });
    };

    function patchData($scope, $http, data, url, success_callback, error_callback, header) {
        var _header = ((header == null || header == undefined) ? { 'Content-Type': 'application/json' } : header);
        _header['Timezone'] = new Date().getTimezoneOffset();
        if (TOKEN_SERVICE.RestoreState() != undefined)
            _header['Authorization'] = TOKEN_SERVICE.RestoreState()['access_token'];

        $http({ method: 'patch', url: API_URL + url, data: data, headers: _header })
        .success(function (data, status, headers, config) {
            console.log("SUCCESS:" + status);
            success_callback(data, status, headers, config);
        }).
        error(function (data, status, headers, config) {
            console.log("ERROR:" + status);

            error_callback(data, status, headers, config);
        });
    };

    function postMultiPartData($scope, $http, data, url, success_callback, error_callback, header) {
        var _header = ((header == null || header == undefined) ? { 'Content-Type': 'application/json' } : header);
        _header['Timezone'] = new Date().getTimezoneOffset();
        if (TOKEN_SERVICE.RestoreState() != undefined)
            _header['Authorization'] = TOKEN_SERVICE.RestoreState()['access_token'];
        Upload.upload({
            url: API_URL+url,
            data: data,
            method: "POST",
            headers: _header
        }).then(function (data, status, headers, config) {
            success_callback(data, status, headers, config);
        }, function (data, status, headers, config) {
            error_callback(data, status, headers, config);
        }, function (evt) {
            console.log(evt);
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
        });
    };

    //function postMultipartData($scope, $http, data, url, success_callback, error_callback, progress_callback, header) {
    //    var _header = ((header == null || header == undefined) ? {"Content-Type": "multipart/form-data" } : header);
    //    Upload.upload({
    //        url: API_URL + url,
    //        data: data,
    //        headers: _header,
    //    })
    //    .success(function (data, status, headers, config) {
    //        success_callback(data, status, headers, config);
    //    })
    //    .error(function (data, status, headers, config) {
    //        if (status == 401) {
    //            var url = $location.url();
    //            if (url != "" && url.indexOf('/login') == -1) {
    //                var login = '<strong> Please login and continue.</strong>'
    //                Flash.create('danger', ERROR401 + login, 5000, { container: 'flash-fixed-login' });
    //            }
    //            if ($state.current.abstract != true) {
    //                $state.reload();
    //            }
    //            $state.go("login");
    //        }
    //        if (status == 500) {
    //            Flash.create('danger', ERROR500, 0, { container: 'flash-fixed' });
    //        }
    //        if (status == 503) {
    //            Flash.create('danger', ERROR503, 0, { container: 'flash-fixed' });
    //        }
    //        if (status == 404) {
    //            Flash.create('danger', ERROR404, 0, { container: 'flash-fixed' });
    //        }
    //        if (status == 408) {
    //            Flash.create('danger', ERROR408, 0, { container: 'flash-fixed' });
    //        }
    //        error_callback(data, status, headers, config);
    //    })
    //    .progress(function (evt) {
    //        $scope.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
    //        progress_callback($scope.progress, evt);
    //    })
    //};

    //function patchMultiPartData($scope,$http,data,url,success_callback,error_callback,header) {
    //    Upload.upload({
    //        url: API_URL+url,
    //        data: data,
    //        method: "patch",
    //    }).then(function (data, status, headers, config) {
    //        success_callback(data, status, headers, config);
    //    }, function (data, status, headers, config) {
    //        error_callback(data, status, headers, config);
    //    }, function (evt) {
    //        console.log(evt);
    //        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
    //    });
    //};

    function deleteData($scope, $http, url, success_callback, error_callback, header) {
        var _header = ((header == null || header == undefined) ? { 'Content-Type': 'application/json' } : header);
        _header['Timezone'] = new Date().getTimezoneOffset();
        if (TOKEN_SERVICE.RestoreState() != undefined)
            _header['Authorization'] = TOKEN_SERVICE.RestoreState()['access_token'];

        $http({ method: 'delete', url: API_URL + url, headers: _header })
        .success(function (data, status, headers, config) {
            console.log("SUCCESS:" + status);
            success_callback(data, status, headers, config);
        }).
        error(function (data, status, headers, config) {
            console.log("ERROR:" + status);
            error_callback(data, status, headers, config);
        });
    };


    return {
        patchData: patchData,
        getData: getData,
        postData: postData,
        putData: putData,
        postLoginData: postLoginData,
        //postMultipartData: postMultipartData,
        postMultiPartData : postMultiPartData,
        //patchMultiPartData : patchMultiPartData,
        deleteData: deleteData,

    };

});