﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.Accounts;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;
using BNS.DL.WSDatabase;
using WSDatabase;
using Newtonsoft.Json.Converters;
using BNS.Models.BNSModels;

namespace BNS.Models.Categories
{
    public class Category : BNSModel
    {       
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Account account { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String value { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Int64 activeOn { get; set; }

        int categoryType;
        int isCurrency;
        public List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();


        public enum Category_Type
        {
            NUMBER = 1,
            TEXT = 2,
        };
        private Category_Type _type;

        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Category_Type CategoryType
        {
            get { return _type; }
            set { _type = value; }
        }

        private void setType(int code)
        {
            switch (code)
            {
                case 1: this.CategoryType = Category_Type.NUMBER; break;
                case 2: this.CategoryType = Category_Type.TEXT; break;
            }
        }

        public enum Category_Unit
        {          
            CURRENCY = 1,
            OTHER = 2,
        };
        private Category_Unit _unit;

        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Category_Unit CategoryUnit
        {
            get { return _unit; }
            set { _unit = value; }
        }

        private void setUnit(int code)
        {
            switch (code)
            {
                case 1: this.CategoryUnit = Category_Unit.CURRENCY; break;
                case 2: this.CategoryUnit = Category_Unit.OTHER; break;
            }
        }

        public Category(JObject categoryJson)
        {
            if (JSONHelper.isValidFieldForKey(categoryJson, "id"))
                id = (Int64)categoryJson["id"];

            if (JSONHelper.isValidFieldForKey(categoryJson, "name"))
                name = (String)categoryJson["name"];

            if (JSONHelper.isValidFieldForKey(categoryJson, "value"))
                value = (String)categoryJson["value"];

            if (JSONHelper.isValidFieldForKey(categoryJson, "type"))
                categoryType = (int)categoryJson["type"];
            setType(categoryType);

            if (JSONHelper.isValidFieldForKey(categoryJson, "is_currency"))
                isCurrency = (int)categoryJson["is_currency"];
            setUnit(isCurrency);
                
        }

        public Category(SqlDataReader reader)
        {
            if (WSDatabaseHelper.isValidField(reader, "CATEGORY_ID"))
            {
                id = reader.GetInt64(reader.GetOrdinal("CATEGORY_ID"));              
            }
            if (WSDatabaseHelper.isValidField(reader, "CATEGORY_NAME"))
            {               
                name = reader.GetString(reader.GetOrdinal("CATEGORY_NAME"));           
            }
            if (WSDatabaseHelper.isValidField(reader, "OFFER_CATEGORY_VALUE"))
            {                          
                value = reader.GetString(reader.GetOrdinal("OFFER_CATEGORY_VALUE"));
            }

            if (WSDatabaseHelper.isValidField(reader, "CATEGORY_TYPE"))
            {                
                int i = (int)reader.GetValue(reader.GetOrdinal("CATEGORY_TYPE"));
                this.setType(i);
            }

            if (WSDatabaseHelper.isValidField(reader, "CATEGORY_UNIT"))
            {               
                int i = (int)reader.GetValue(reader.GetOrdinal("CATEGORY_UNIT"));
                this.setUnit(i);
            }

            if (WSDatabaseHelper.isValidField(reader, "CATEGORY_ACTIVE_ON"))
            {
               activeOn =  Convert.ToInt64(reader.GetValue(reader.GetOrdinal("CATEGORY_ACTIVE_ON")));            
            }
        }

        public Category(String categoryName, int unit, Account newaccount, int type)
        {
            name = categoryName;
            account = new Account();
            account = newaccount;
            setType(type);
            setUnit(unit);
            createdBy = account.admin;
            updatedBy = account.admin;            
        }
        public Category()
        {
        }

        public override void save()
        {
            throw new NotImplementedException();
        }

        public override void update()
        {
            throw new NotImplementedException();
        }

        public override Dictionary<string, object> toDictionary()
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            if (this.id != 0)
                parameters["id"] = this.id;
            if (CategoryType == Category_Type.NUMBER)
                parameters["type"] = 1;
            else if (CategoryType == Category_Type.TEXT)
                parameters["type"] = 2;
            if (name != null)
                parameters["name"] = name;
            if (CategoryUnit == Category_Unit.CURRENCY)
                parameters["unit"] = 1;
            else if (CategoryUnit == Category_Unit.OTHER)
                parameters["unit"] = 2;
            parameters["accountId"] = account.id;
            parameters["createdBy"] = createdBy.id;
            parameters["updatedBy"] = updatedBy.id;
            parameters["updatedOn"] = DateTime.UtcNow.ToString();
            parameters["createdOn"] = DateTime.UtcNow.ToString();
            return parameters;
        }
    }
}