﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.BNSModels;
using BNS.Models.Stores;
using BNS.Models.Offers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;
using BNS.DL.WSDatabase;
using Newtonsoft.Json.Converters;
using WSDatabase;

namespace BNS.Models.Devices
{
    public class Device : BNSModel
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Store store { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Offer offer { get; set; }       

        int deviceType;

        public enum Device_Type
        {
            BEACON = 1,
            KIOSK = 2,          
        };
        private Device_Type _type;

        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Device_Type DeviceType
        {
            get { return _type; }
            set { _type = value; }
        }

        private void setType(int code)
        {
            switch (code)
            {
                case 1: this.DeviceType = Device_Type.BEACON; break;
                case 2: this.DeviceType = Device_Type.KIOSK; break;               
            }
        }

        public Device(SqlDataReader reader)
        {
            if (WSDatabaseHelper.isValidField(reader, "DEVICE_ID"))
            {
                this.id = reader.GetInt64(reader.GetOrdinal("DEVICE_ID"));
            }

            if (WSDatabaseHelper.isValidField(reader, "DEVICE_TYPE"))
            {
                String i = reader.GetString(reader.GetOrdinal("DEVICE_TYPE"));
                this.setType(Convert.ToInt16(i));
            }

            if (WSDatabaseHelper.isValidField(reader, "DEVICE_NUMBER"))
            {
                this.number = reader.GetString(reader.GetOrdinal("DEVICE_NUMBER"));
            }

            if (WSDatabaseHelper.isValidField(reader, "DEVICE_NAME"))
            {
                this.name = reader.GetString(reader.GetOrdinal("DEVICE_NAME"));
            }

            if (WSDatabaseHelper.isValidField(reader, "STORE_ID"))
            {
                store = new Store(reader);
            }

            if (WSDatabaseHelper.isValidField(reader, "OFFER_ID"))
            {
                offer = new Offer(reader);
            }           
        }

        public Device(JObject deviceJson)
        {
            if (JSONHelper.isValidFieldForKey(deviceJson, "id"))
                this.id = (Int64)deviceJson["id"];

            if (JSONHelper.isValidFieldForKey(deviceJson, "name"))
                this.name = (String)deviceJson["name"];

            if (JSONHelper.isValidFieldForKey(deviceJson, "number"))
                this.number = (String)deviceJson["number"];

            if (JSONHelper.isValidFieldForKey(deviceJson, "deviceType"))
                deviceType = (int)deviceJson["deviceType"];
            setType(deviceType);

        }

        public override void save()
        {
            throw new NotImplementedException();
        }

        public override Dictionary<string, object> toDictionary()
        {
            throw new NotImplementedException();
        }

        public override void update()
        {
            throw new NotImplementedException();
        }
    }
}