﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.BNSModels;
using BNS.Models.Users;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WSDatabase;
using System.Data.SqlClient;
using BNS.Models.Beacons;
using BNS.Models.Kiosks;
using BNS.Models.Offers;
using BNS.Models.Contacts;

namespace BNS.Models.Accounts
{
    public class Account : BNSModel
    {       
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public HttpPostedFile logo { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String picUrl { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public User admin { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<Contact> accountContacts { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Contact contact { get; set; }

        public Account(JObject accountJson)
        {
            if (JSONHelper.isValidFieldForKey(accountJson, "id"))
                id = (Int64)accountJson["id"];

            if (JSONHelper.isValidFieldForKey(accountJson, "name"))
                name = (String)accountJson["name"];
        }

        public Account(HttpContext current)
        {
            contact = new Contact();
            accountContacts = new List<Contact>();

            if (HttpContext.Current.Request.Form.AllKeys.Contains("id"))
            {
                id = Convert.ToInt64(HttpContext.Current.Request.Form.GetValues("id").FirstOrDefault());
            }

            if (HttpContext.Current.Request.Form.AllKeys.Contains("name"))
            {
                name = Convert.ToString(HttpContext.Current.Request.Form.GetValues("name").FirstOrDefault());
            }

            if (HttpContext.Current.Request.Form.AllKeys.Contains("website"))
            {
                Contact websiteContact = new Contact();
                websiteContact.value = Convert.ToString(HttpContext.Current.Request.Form.GetValues("website").FirstOrDefault());
                websiteContact.ContactType = Contact.Contact_Type.WEBSITE;
                accountContacts.Add(websiteContact);     
                contact.value = Convert.ToString(HttpContext.Current.Request.Form.GetValues("website").FirstOrDefault());
                contact.ContactType = Contact.Contact_Type.WEBSITE;
            }

            if (HttpContext.Current.Request.Form.AllKeys.Contains("facebook"))
            {
                Contact facebookContact = new Contact();
                facebookContact.value = Convert.ToString(HttpContext.Current.Request.Form.GetValues("facebook").FirstOrDefault());
                facebookContact.ContactType = Contact.Contact_Type.FACEBOOK;
                accountContacts.Add(facebookContact);
            }

            if (HttpContext.Current.Request.Form.AllKeys.Contains("twitter"))
            {
                Contact twitterContact = new Contact();
                twitterContact.value = Convert.ToString(HttpContext.Current.Request.Form.GetValues("twitter").FirstOrDefault());
                twitterContact.ContactType = Contact.Contact_Type.TWITTER;
                accountContacts.Add(twitterContact);
            }

            admin = new User(HttpContext.Current);
            
            HttpFileCollection files = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files : null;
            if (files != null)
            {
                if (files.Count == 0)
                {
                    picUrl = null;
                }
                else
                {
                    foreach (string fileName in files)
                    {
                        logo = files[fileName];
                    }
                }
            }
            else
            {
                picUrl = null;
            }
        }

        public Account()
        {
         
        }

        public Account(SqlDataReader reader)
        {
            
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());            

            if (WSDatabaseHelper.isValidField(reader, "ACCOUNT_ID"))
            {
                id = reader.GetInt64(reader.GetOrdinal("ACCOUNT_ID"));
            }

            if (WSDatabaseHelper.isValidField(reader, "ACCOUNT_NAME"))
            {
                name = reader.GetString(reader.GetOrdinal("ACCOUNT_NAME"));
            }            

            if (WSDatabaseHelper.isValidField(reader, "ACCOUNT_ADMIN"))
            {
                admin = new User();     
                admin.id = reader.GetInt64(reader.GetOrdinal("ACCOUNT_ADMIN"));
            }

            if (WSDatabaseHelper.isValidField(reader, "ACCOUNT_ADMIN_CONTACT"))
            {
                contact = new Contact(reader);  
            }


            if (WSDatabaseHelper.isValidField(reader, "ACCOUNT_PIC_URL"))
            {
                picUrl = reader.GetString(reader.GetOrdinal("ACCOUNT_PIC_URL"));
            }            

            if (WSDatabaseHelper.isValidField(reader, "ACCOUNT_CREATED_ON"))
            {
                DateTime date = reader.GetDateTime(reader.GetOrdinal("ACCOUNT_CREATED_ON"));
                date = date.AddMinutes(-offSet);
                createdOn = date.ToString();        
            }

            if (WSDatabaseHelper.isValidField(reader, "ACCOUNT_UPDATED_ON"))
            {
                DateTime date = reader.GetDateTime(reader.GetOrdinal("ACCOUNT_UPDATED_ON"));
                date = date.AddMinutes(-offSet);
                updatedOn = date.ToString();
            }
        }

        public override void save()
        {
            throw new NotImplementedException();
        }

        public override void update()
        {
            throw new NotImplementedException();
        }

        public override Dictionary<string, object> toDictionary()
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            if (id != 0)
                parameters["id"] = id;
            if (name != null)
                parameters["name"] = name;            

            parameters["picUrl"] = picUrl;        
            parameters["updatedOn"] = DateTime.UtcNow.ToString();
            parameters["createdOn"] = DateTime.UtcNow.ToString();
            return parameters;
        }
    }
}