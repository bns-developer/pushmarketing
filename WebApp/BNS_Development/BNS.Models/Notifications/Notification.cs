﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.Users;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using WSDatabase;
using BNS.Models.Accounts;

namespace BNS.Models.Notifications
{
    public class Notification
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String subject { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public String message { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<User> receivers { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Account sender { get; set; }

        int notificationType;

        public enum Notification_Type
        {
            EMAIL = 1,
            TEXT = 2,
            PUSH = 3
        };
        private Notification_Type _type;

        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Notification_Type NotificationType
        {
            get { return _type; }
            set { _type = value; }
        }
        private void setType(int code)
        {
            switch (code)
            {
                case 1: this.NotificationType = Notification_Type.EMAIL; break;
                case 2: this.NotificationType = Notification_Type.TEXT; break;
                case 3: this.NotificationType = Notification_Type.PUSH; break;
            }
        }

        public Notification()
        {
            receivers = new List<User>();
            sender = new Account();
        }

        public Notification(JObject notificationJson)
        {
            if (JSONHelper.isValidFieldForKey(notificationJson, "subject"))
                subject = (String)notificationJson["subject"];

            if (JSONHelper.isValidFieldForKey(notificationJson, "message"))
                message = (String)notificationJson["message"];

            if (JSONHelper.isValidFieldForKey(notificationJson, "type"))
                notificationType = (int)notificationJson["type"];
                setType(notificationType);

            receivers = new List<User>();
            JArray userJsonArray = (JArray)notificationJson.SelectToken("users");
            foreach (JObject item in userJsonArray)
            {
                User user = new User(item);
                receivers.Add(user);
            }
        }




    }
}