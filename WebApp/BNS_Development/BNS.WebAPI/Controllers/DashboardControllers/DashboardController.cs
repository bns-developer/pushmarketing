﻿using BNS.Models.Users;
using BNS.Wrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using BNS.DAL.DALDashboard;
using BNS.DAL.DALBeacon;
using BNS.DAL.DALKiosk;
using BNS.DAL.DALOffer;
using BNS.DAL.DALStore;


namespace BNS.WebAPI.Controllers.DashboardControllers
{
    [RoutePrefix("api/v1/dashboard")]
    public class DashboardController : ApiController
    {
        ResponseWrapper responseWrapper = new ResponseWrapper();
        DALDashboardManager dashboardManager = new DALDashboardManager();
        DALBeaconDashboard beaconDashboard = new DALBeaconDashboard();
        DALKioskDashboard kioskDashboard = new DALKioskDashboard();
        DALOfferDashboard offerDashboard = new DALOfferDashboard();
        DALStoreDashboard storeDashboard = new DALStoreDashboard();     

        [Route("getData")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage getDashboardData()
        {
            User requestUser = new User();
            Dictionary<string, object> responseObject = new Dictionary<string, object>();

            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            responseObject = dashboardManager.getDashboardDetails(requestUser.account);

            var response = Request.CreateResponse(HttpStatusCode.OK, responseObject);
            return response;
        }

        [Route("getStoreDashboardData")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage getStoreDashboardData([FromUri] Int64 store_id)
        {
            User requestUser = new User();
            Dictionary<string, object> responseObject = new Dictionary<string, object>();

            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            responseObject = storeDashboard.getStoreDashboardData(store_id, requestUser.account.id);

            var response = Request.CreateResponse(HttpStatusCode.OK, responseObject);
            return response;
        }


        [Route("getBeaconDashboardData")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage getBeaconDashboardData([FromUri] Int64 beacon_id)
        {
            User requestUser = new User();
            Dictionary<string, object> responseObject = new Dictionary<string, object>();

            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            responseObject = beaconDashboard.getBeaconDashboardDetails(beacon_id, requestUser.account.id);

            var response = Request.CreateResponse(HttpStatusCode.OK, responseObject);
            return response;
        }

        [Route("getKioskDashboardData")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage getKioskDashboardData([FromUri] Int64 kiosk_id)
        {
            User requestUser = new User();
            Dictionary<string, object> responseObject = new Dictionary<string, object>();

            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            responseObject = kioskDashboard.getKioskDashboardDetails(kiosk_id, requestUser.account.id);

            var response = Request.CreateResponse(HttpStatusCode.OK, responseObject);
            return response;
        }

        [Route("getOfferDashboardData")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage getOfferDashboardData([FromUri] Int64 offer_id)
        {
            User requestUser = new User();
            Dictionary<string, object> responseObject = new Dictionary<string, object>();

            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            responseObject = offerDashboard.getOfferDashboardDetails(offer_id);

            var response = Request.CreateResponse(HttpStatusCode.OK, responseObject);
            return response;
        }
    }
}
