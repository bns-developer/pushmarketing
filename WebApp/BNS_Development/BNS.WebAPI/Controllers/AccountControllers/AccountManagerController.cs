﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using BNS.Wrapper;
using BNS.DAL.DALAccount;


namespace BNS.API.Controllers.AccountControllers
{
    [RoutePrefix("api/v1/accounts")]
    public class AccountManagerController : ApiController
    {
        DALAccountManager account = new DALAccountManager();       
        ResponseWrapper responseWrapper = new ResponseWrapper();
        //SessionManager manager = new SessionManager();

        [Route("login")]
        [HttpPost]
        public HttpResponseMessage Login([FromBody]JObject credentials)
        {
            String username = (string)credentials["email"];
            String password = (string)credentials["password"];
            Int64 userID = account.isValidUser(username, password);
            if (userID != 0)
            {              
                var res = Request.CreateResponse(HttpStatusCode.OK, "Authorized");
                return res;
            }
            else
            {

                var res = Request.CreateResponse(HttpStatusCode.Unauthorized, "Unauthorized");
                return res;

            }

        }


        [Route("logout")]
        [HttpPost]      
        public HttpResponseMessage Logout()
        {

            if (account.logout(Request))
                return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Sucessfully logout.");
            else
                return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Error in Logout.");

        }
    }
}
