﻿using BNS.Wrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Script.Serialization;
using BNS.WebAPI.Controllers.BNSControllers;
using Newtonsoft.Json.Linq;
using BNS.Models.Accounts;
using BNS.Models.Users;
using BNS.Models.Offers;
using BNS.BL.BLOffer;
using BNS.DAL.DALOffer;
using BNS.Models.Countries;
using BNS.Models.Categories;
using System.Web;
using BNS.DAL.DALAccount;
using System.Diagnostics;
using BNS.DAL.DALUser;
using BNS.DAL.DALStore;
using BNS.RE;
using BNS.DAL.DALBeacon;
using BNS.Models.UsersBeacons;
using BNS.Models.TermsAndConditions;
using BNS.DAL.DALTermsAndConditions;
using BNS.DAL.DALKiosk;
using BNS.DAL.Constants;
using System.Data;
using lcpi.data.oledb;
using BNS.DAL.DALCategory;
using static BNS.Models.Offers.Offer;
using BNS.DAL.DALCountry;
using BNS.Models.UsersKiosks;

namespace BNS.API.Controllers.OfferControllers
{
    [RoutePrefix("api/v1/Offers")]
    public class OfferController : BNSController
    {
        OfferBL BLmanager = new OfferBL();
        DALOfferManager DALmanager = new DALOfferManager();
        DALUserManager DALUserManager = new DALUserManager();
        ResponseWrapper responseWrapper = new ResponseWrapper();
        DALStoreManager storeManager = new DALStoreManager();
        DALBeaconManager beaconManager = new DALBeaconManager();
        DALTermsAndConditions termsAndConditionsManager = new DALTermsAndConditions();
        DALCategoryManager categoryManager = new DALCategoryManager();
        DALCountryManager countryManager = new DALCountryManager();
        RuleEngine ruleEngine = new RuleEngine();
        KioskRuleEngine kioakRuleEngine = new KioskRuleEngine();


        [Route("save")]
        [HttpPost]
        [Authorize]
        //Save an offer multipart data
        public HttpResponseMessage save()
        {
            HttpFileCollection files = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files : null;
            String offerNumber = Convert.ToString(HttpContext.Current.Request.Form.GetValues("number").FirstOrDefault());

            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());

            List<string[]> countries = new List<string[]>();
            List<string[]> categories = new List<string[]>();
            List<string[]> terms = new List<string[]>();

            List<TermsAndCondition> termsAndConditionsArray = new List<TermsAndCondition>();

            if (DALmanager.checkIfNumberExists(offerNumber, requestUser.account.id))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Offer Number Already Exist");
            }
            else
            {
                Offer newOffer = new Offer(HttpContext.Current);
                newOffer.createdBy = requestUser;
                newOffer.updatedBy = requestUser;
                newOffer.account = requestUser.account;


                int length = HttpContext.Current.Request.Form.Count;
                while (length >= 0)
                {
                    if (HttpContext.Current.Request.Form.GetValues("countries[" + length + "][id]") != null)
                    {
                        countries.Add(HttpContext.Current.Request.Form.GetValues("countries[" + length + "][id]"));
                    }

                    if (HttpContext.Current.Request.Form.GetValues("terms[" + length + "]") != null)
                    {
                        terms.Add(HttpContext.Current.Request.Form.GetValues("terms[" + length + "]"));
                    }
                     
                    if (HttpContext.Current.Request.Form.GetValues("categories[" + length + "][is_selected]") != null)
                    {
                        string[] selected = HttpContext.Current.Request.Form.GetValues("categories[" + length + "][is_selected]");
                        if(selected[0].Equals("true"))
                        {
                            categories.Add(HttpContext.Current.Request.Form.GetValues("categories[" + length + "][id]"));
                            categories.Add(HttpContext.Current.Request.Form.GetValues("categories[" + length + "][value]"));
                        }
                        selected = null;                   
                    }
                    length = length - 1;
                }

               foreach(string[] item in countries)
                {
                    Country country = new Country();
                    country.id = Convert.ToInt64(item[0]);
                    newOffer.countries.Add(country);
                }

               foreach(string[] item in terms)
                {
                    TermsAndCondition termsAndConditions = new TermsAndCondition();
                    termsAndConditions.id = Convert.ToInt64(item[0]);
                    newOffer.termsAndConditions.Add(termsAndConditions);                   
                }

               for(int i = 0; i <= categories.Count - 1; i++)
                {
                    Category category = new Category();
                    category.id = Convert.ToInt64(categories[i][0]);
                    i++;
                    category.value = categories[i][0].ToString();
                    newOffer.categories.Add(category);
                }              

                if (BLmanager.save(newOffer))
                {                  
                        return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Offer  Saved");                                        
                }
                else
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Offer Not Saved");
                }
            }

        }

        [Route("update")]
        [HttpPost]
        [Authorize]
        //Update an offer
        public  HttpResponseMessage update()
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());

            List<string[]> terms = new List<string[]>();
            List<TermsAndCondition> termsAndConditionsArray = new List<TermsAndCondition>();

            Offer updateOffer = new Offer(HttpContext.Current);
            updateOffer.updatedBy = requestUser;
            updateOffer.createdBy = requestUser;
            updateOffer.account = requestUser.account;

            int length = HttpContext.Current.Request.Form.Count;
            while (length >= 0)
            {
                if (HttpContext.Current.Request.Form.GetValues("termsAndConditions[" + length + "][id]") != null)
                {
                    terms.Add(HttpContext.Current.Request.Form.GetValues("termsAndConditions[" + length + "][id]"));
                }
                length = length - 1;
            }

            foreach (string[] item in terms)
            {
                TermsAndCondition termsAndConditions = new TermsAndCondition();
                termsAndConditions.id = Convert.ToInt64(item[0]);
                updateOffer.termsAndConditions.Add(termsAndConditions);
            }
            if (DALmanager.update(updateOffer))
                return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Offer Updated");
            else
                return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Offer Not Updated");
        }

        public override HttpResponseMessage update([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }

        [Route("offerDetailsForBeacon")]
        [HttpPost]        
        //Offer details for beacon
        public HttpResponseMessage GetOfferInfo([FromBody]JObject info)
        {
            Dictionary<string, object> responseObject = new Dictionary<string, object>();            
            User user = new User();
            UsersBeacons userBeacon = new UsersBeacons();

            user.offer.id = Convert.ToInt64(info.GetValue("offerId"));
            user.beacon.id = Convert.ToInt64(info.GetValue("beaconId"));
            user.memberId = Convert.ToString(info.GetValue("memberId"));   
                   
            user = DALUserManager.getUserDetailsByMemberId(user);
            user.offer = DALmanager.getDetails(user.offer);
            user.account = new DALAccountManager().getDetails(user.account);
            //account.id = storeManager.getAccountIdFromBeaconId(beaconId);
                      
            userBeacon = beaconManager.getUserBeacon(user.id, user.beacon.id, user.offer.id);
            if (userBeacon.user_id == 0)
            {
                bool add = beaconManager.addUserBeacons(user.id, user.beacon.id, user.offer.id);
            }
            else
            {
                beaconManager.updateUserBeacon(userBeacon);
            }
            responseObject.Add("offer", user.offer);
            responseObject.Add("type", user.type);
            responseObject.Add("account", user.account);
            responseObject.Add("beaconId", user.beacon.id);
            responseObject.Add("userId", user.id);
            var response = Request.CreateResponse(HttpStatusCode.OK, responseObject);
            return response;
                   
            
        }

        [Route("details")]
        [HttpGet]
        //Offer details
        public override HttpResponseMessage GetInfo([FromUri]Int64 offerId)
        {

            Offer offer = new Offer();
            offer.id = offerId;
            offer = DALmanager.getDetails(offer);           
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return response;
        }

        [Route("all")]
        [HttpGet]
        [Authorize]
        //All offers by accountId
        public override HttpResponseMessage GetAll()
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            List<Offer> offer = DALmanager.getAll(requestUser.account);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
        }

        [Route("delete")]
        [HttpPost]
        [Authorize]
        public override HttpResponseMessage delete([FromBody] JObject requestJSON)
        {
            Int64 offerId = (Int64)requestJSON["id"];
            if (DALmanager.isDefaultOffer(offerId) || DALmanager.isOfferSetAsDefault(offerId) || DALmanager.isOfferSeenByUser(offerId))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Unable to delete the offer as there is a data associated with this offer. ");
            }
            else
            {
                if (DALmanager.delete(offerId))
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Offer Deleted Successfully.");
                else
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Offer Not Deleted.");
            }
        }


        [Route("setOfferForLuckyItem")]
        [HttpPost]
        [Authorize]
        // Set an offer for kiosk's lucky item
        public HttpResponseMessage saveKioskLuckyItemOffer([FromUri] Int64 kiosk_id, [FromUri] Int64 lucky_item_id, [FromUri] Int64 offer_id)
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            Offer offer = new Offer();
            offer.id = offer_id;
            offer.createdBy = requestUser;
            offer.updatedBy = requestUser;

            if (BLmanager.saveOfferForLuckyItem(kiosk_id, lucky_item_id, offer))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Offer Activated Successfully");
            }
            else
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Offer Not Activated");
            }

        }

        [Route("changeLuckyItemDefaultOffer")]
        [HttpPost]
        [Authorize]
        // Change an default offer of kiosk's lucky item
        public HttpResponseMessage changeLuckyItemDefaultOffer([FromUri] Int64 kiosk_id, [FromUri] Int64 lucky_item_id, [FromUri] Int64 offer_id, [FromUri] int offer_type)
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());

            Offer offer = new Offer();
            offer.id = offer_id;
            offer.createdBy = requestUser;
            offer.updatedBy = requestUser;

            if (DALmanager.changeLuckyItemDefaultOffer(kiosk_id, lucky_item_id, offer, offer_type))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Offer Changed Successfully");
            }
            else
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Offer Not Changed");
            }

        }

        [Route("getLuckyItemDefaultOffer")]
        [HttpGet]
        [Authorize]
        //Default offer for kiosk's lucky item
        public  HttpResponseMessage getDefaultOffer([FromUri] Int64 kiosk_id, [FromUri] Int64 lucky_item_id)
        {
            List<DefaultOffer> defaultOffers = DALmanager.getLuckyItemDefaultOffers(kiosk_id, lucky_item_id);
            var response = Request.CreateResponse(HttpStatusCode.OK, defaultOffers);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, defaultOffers);       
        }

        [Route("getLuckyItemNonDefaultOffers")]
        [HttpGet]
        [Authorize]
        //Default offer for kiosk's lucky item
        public HttpResponseMessage getLuckyItemNonDefaultOffers([FromUri] Int64 kiosk_id, [FromUri] Int64 lucky_item_id, [FromUri] int offer_type)
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            List<Offer> offer = DALmanager.getLuckyItemNonDefaultOffers(kiosk_id, lucky_item_id, requestUser.account.id, offer_type);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
        }

        [Route("getLuckyItemActiveOffers")]
        [HttpGet]
        [Authorize]
        // Active offers for kiosk's lucky item
        public HttpResponseMessage getActiveOffers([FromUri] Int64 kiosk_id, [FromUri] Int64 lucky_item_id)
        {
            List<Offer> offer = DALmanager.getLuckyItemActiveOffers(kiosk_id, lucky_item_id);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
        }

        [Route("getLuckyItemOffersHistory")]
        [HttpGet]
        [Authorize]
        // Offers history of kiosk's lucky item
        public HttpResponseMessage getOffersHistory([FromUri] Int64 kiosk_id, [FromUri] Int64 lucky_item_id)
        {
            List<Offer> offer = DALmanager.getLuckyItemOffersHistory(kiosk_id, lucky_item_id);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
        }

        [Route("getLuckyItemInactiveOffers")]
        [HttpGet]
        [Authorize]
        // Inactive offers kiosk's lucky item
        public HttpResponseMessage getInactiveOffers([FromUri] Int64 kiosk_id, [FromUri] Int64 lucky_item_id)
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            List<Offer> offer = DALmanager.getLuckyItemInactiveOffers(kiosk_id, lucky_item_id, requestUser.account.id);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
        }

        [Route("deactivateKioskLuckyItemOffer")]
        [HttpPost]
        [Authorize]
        public HttpResponseMessage deactivateKioskLuckyItemOffer([FromUri] Int64 kioskId, [FromUri] Int64 luckyItemId, [FromUri] Int64 offerId)
        {
            if (DALmanager.deactivateKioskLuckyItemOffer(kioskId, luckyItemId, offerId))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Offer deactivated successfully");
            }
            else
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Offer not deactivated");
            }
        }


        [Route("setOfferForBeacon")]
        [HttpPost]
        [Authorize]
        // Set an offer for beacon
        public HttpResponseMessage saveBeaconOffer([FromUri] Int64 beacon_id, [FromUri] Int64 offer_id)
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());

            Offer offer = new Offer();
            offer.id = offer_id;
            offer.createdBy = requestUser;
            offer.updatedBy = requestUser;

            if (BLmanager.saveOfferForBeacon(beacon_id, offer))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Offer Activated Successfully");
            }
            else
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Offer Not Activated");
            }

        }

        [Route("changeBeaconDefaultOffer")]
        [HttpPost]
        [Authorize]
        // Change an default offer of beacon
        public HttpResponseMessage changeBeaconDefaultOffer([FromUri] Int64 beacon_id, [FromUri] Int64 offer_id, [FromUri] int offer_type)
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());

            Offer offer = new Offer();
            offer.id = offer_id;
            offer.createdBy = requestUser;
            offer.updatedBy = requestUser;

            if (DALmanager.changeBeaconDefaultOffer(beacon_id, offer, offer_type))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Offer Changed Successfully");
            }
            else
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Offer Not Changed");
            }

        }       


        [Route("getBeaconDfaultOffer")]
        [HttpGet]
        [Authorize]
        //Default offer for beacon
        public HttpResponseMessage getDefaultOffers([FromUri] Int64 beacon_id)
        {
            List<DefaultOffer> defaultOffers = DALmanager.getBeaconDefaultOffers(beacon_id);
            var response = Request.CreateResponse(HttpStatusCode.OK, defaultOffers);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, defaultOffers);
        }

        [Route("getBeaconNonDefaultOffers")]
        [HttpGet]
        [Authorize]
        //All offer for beacon excluding the default one
        public HttpResponseMessage getBeaconNonDefaultOffers([FromUri] Int64 beacon_id, [FromUri] int offer_type)
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            List<Offer> nonDefaultOffers = DALmanager.getBeaconNonDefaultOffers(beacon_id, requestUser.account.id, offer_type);
            var response = Request.CreateResponse(HttpStatusCode.OK, nonDefaultOffers);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, nonDefaultOffers);
        }

        [Route("getBeaconActiveOffers")]
        [HttpGet]
        [Authorize]
        // Active offers for beacon
        public HttpResponseMessage getActiveOffers([FromUri] Int64 beacon_id)
        {
            List<Offer> offer = DALmanager.getBeaconActiveOffers(beacon_id);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
        }

        [Route("getBeaconOffersHistory")]
        [HttpGet]
        [Authorize]
        // Offers history of kiosk's lucky item
        public HttpResponseMessage getOffersHistory([FromUri] Int64 beacon_id)
        {
            List<Offer> offer = DALmanager.getBeaconOffersHistory(beacon_id);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
        }

        [Route("getBeaconInactiveOffers")]
        [HttpGet]
        [Authorize]
        // Inactive offers kiosk's lucky item
        public HttpResponseMessage getInactiveOffers([FromUri] Int64 beacon_id)
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            List<Offer> offer = DALmanager.getBeaconInactiveOffers(beacon_id, requestUser.account.id);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
        }

        [Route("getNonDefaultOffersByType")]
        [HttpGet]
        [Authorize]
        //Non default offer by offer type
        public HttpResponseMessage getNonDefaultOffersByType([FromUri]int offerType, [FromUri]int deviceType )
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            List<Offer> offers = DALmanager.getNonDefaultOffersByType(offerType, deviceType, requestUser.account.id);
            var response = Request.CreateResponse(HttpStatusCode.OK, offers);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offers);
        }

        [Route("changeDeviceDefaultOffer")]
        [HttpPost]
        [Authorize]
        //Set default offer for newly added device
        public HttpResponseMessage changeDeviceDefaultOffer([FromUri]int deviceType, [FromUri]int offerType,  [FromUri] Int64 offerId)
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());           

            if (DALmanager.changeDeviceDefaultOffer(deviceType, offerType, offerId, requestUser))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Default Offer Changed Successfully");
            }
            else
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Default Offer Not Changed");
            }

        }

        [Route("getDefaultOffersByAccount")]
        [HttpGet]
        [Authorize]
        //Default offer by account
        public HttpResponseMessage getDefaultOffersByAccount()
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            List<DefaultOffer> defaultOffers = DALmanager.getDefaultOffersByAccount(requestUser.account.id);
            var response = Request.CreateResponse(HttpStatusCode.OK, defaultOffers);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, defaultOffers);
        }


        [Route("deactivateBeaconOffer")]
        [HttpPost]
        [Authorize]
        public HttpResponseMessage deactivateBeaconOffer(Int64 beaconId, Int64 offerId)
        {           
            if (DALmanager.deactivateBeaconOffer(beaconId, offerId))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Offer deactivated successfully");
            }
            else
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Offer not deactivated");
            }
        }


        //[Route("getCurrentActiveOfferForBeacon")]
        //[HttpGet]
        //[Authorized]
        ////Current active offer for beacon(Mobile App)
        //public HttpResponseMessage getCurrentActiveOfferForBeacon([FromUri] Int64 beacon_id)
        //{
        //    Offer offer = DALmanager.getCurrentActiveOfferForBeacon(beacon_id);
        //    var response = Request.CreateResponse(HttpStatusCode.OK, offer);
        //    return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
        //}

        [Route("viewBeaconOffer")]
        [HttpPost]
        public HttpResponseMessage viewBeaconOffer([FromBody] JObject detailsArray)
        {

            var response = Request.CreateResponse(HttpStatusCode.OK, detailsArray);           
            
            //Get values from the body
            JToken details = detailsArray.GetValue("details");
            String beaconGuid = Convert.ToString(detailsArray.GetValue("beaconGuid"));

            //Initialize required variables
            Dictionary<String, Object> responseObject = new Dictionary<string, object>();
            List<Dictionary<String, Object>> memberDetails = new List<Dictionary<string, object>>();
            Dictionary<String, Object> memberDetail = new Dictionary<String, Object>();
            Int64 type = 0;
            String memberId = "";
            User user = new User();
            Account account = new Account();
            List<Int64> termsAndConditionsIdArray = new List<Int64>();

            //Get beacon_id from beaconGuid
            Int64 beacon_id = beaconManager.getBeaconIdFromGuid(beaconGuid);
            //Get account_id from beacon_id
            Int64 account_id = storeManager.getAccountIdFromBeaconId(beacon_id);

            //Get account details
            account.id = account_id;
            account = new DALAccountManager().getDetails(account);

            //Check if detailsArray is empty
            if (details != null)
            {
                //Find user for the account_id and memberId in the deatailsArray
                foreach(JObject detail in details)
                {
                    Dictionary<String, Object> tempDetail = new Dictionary<String, Object>();
                    String tempMemberId = Convert.ToString(detail["memberId"]);
                    user.memberId = tempMemberId;
                    user.account.id = account_id;
                    user = DALUserManager.getUserDetails(user);

                    //Push the whole array for response object
                    tempDetail["memberId"] = user.memberId;
                    tempDetail["accountId"] = account_id;

                    memberDetails.Add(tempDetail);

                    if (user.id != 0)
                    {
                        memberId = tempMemberId;
                    }
                }

                //If memberId == "" User for the available memberIds and account_id is not present
                if(memberId == "")
                {
                    user = DALUserManager.saveAnonymousUser(account_id);
                    type = 2;
                    Dictionary<String, Object> tempDetail = new Dictionary<String, Object>();
                    //Set member details array
                    tempDetail["memberId"] = user.memberId;
                    tempDetail["accountId"] = user.account.id;
                    tempDetail["type"] = type;
                    tempDetail["beaconId"] = beacon_id;
                    tempDetail["userId"] = user.id;
                    memberDetails.Add(tempDetail);

                    //Set currently in use details
                    responseObject["currentlyInUse"] = tempDetail;

                } else
                {
                    user.memberId = memberId;
                    user = DALUserManager.getUserDetailsByMemberId(user);
                    if(user.type == 1)
                    {
                        type = 1;
                    }else
                    {
                        type = 2;
                    }

                    Dictionary<String, Object> tempDetail = new Dictionary<String, Object>();
                    //Set member details array
                    tempDetail["memberId"] = user.memberId;
                    tempDetail["accountId"] = user.account.id;
                    tempDetail["type"] = type;
                    tempDetail["beaconId"] = beacon_id;
                    tempDetail["userId"] = user.id;
                    //Set currently in use details
                    responseObject["currentlyInUse"] = tempDetail;

                }
            } else
            {
                //Save anonymous user
                user = DALUserManager.saveAnonymousUser(account_id);
                type = 2;
                Dictionary<String, Object> tempDetail = new Dictionary<String, Object>();
                //Set member details array
                tempDetail["memberId"] = user.memberId;
                tempDetail["accountId"] = user.account.id;
                tempDetail["type"] = type;
                tempDetail["beaconId"] = beacon_id;
                tempDetail["userId"] = user.id;
                memberDetails.Add(tempDetail);

                //Set currently in use details
                responseObject["currentlyInUse"] = tempDetail;
            }

            //Get appropriate offer 
            Offer offer = ruleEngine.ruleEngine(user, beacon_id, type, account_id, beaconGuid);
            
            //Get Terms And Conditions For The Offer
            termsAndConditionsIdArray = termsAndConditionsManager.getTermsAndConditionsFromOfferId(offer.id);

            List<TermsAndCondition> termsAndConditions = new List<TermsAndCondition>();
            foreach(var id in termsAndConditionsIdArray)
            {
                TermsAndCondition termAndCondition = new TermsAndCondition();

                termAndCondition = termsAndConditionsManager.getSingleTermAndConditionById(id);

                termsAndConditions.Add(termAndCondition);
            }


            responseObject["details"] = memberDetails;
            responseObject["beaconGuid"] = beaconGuid;
            responseObject["offer"] = offer;
            responseObject["account"] = account;
            responseObject["termsAndConditions"] = termsAndConditions;

            response = Request.CreateResponse(HttpStatusCode.OK, responseObject);

            return response;
        }

        [Route("getUserOfferForBeacon")]
        [HttpPost]
        //Current active offer for beacon(Mobile App)
        public HttpResponseMessage getUserOfferForBeacon([FromBody] JObject details)
        {          
            Dictionary<string, object> responseObject = new Dictionary<string, object>();

            String beaconGuid = Convert.ToString(details.GetValue("beaconGuid"));
            Int64 beacon_id = beaconManager.getBeaconIdFromGuid(beaconGuid);

            Int64 type = 0;

            User user = new User();
            user.memberId = Convert.ToString(details.GetValue("memberId"));

            Int64 account_id = storeManager.getAccountIdFromBeaconId(beacon_id);
            Account account = new Account();
            account.id = account_id;
            account = new DALAccountManager().getDetails(account);

            if (user.memberId == null || user.memberId == "")
            {
                user = DALUserManager.saveAnonymousUser(account_id);
                type = 2;
            }
            else
            {
                user = DALUserManager.getUserDetailsByMemberId(user);
                if(user.type == 1)
                {
                    type = 1;
                } else
                {
                    type = 2;
                }
            }

            Offer offer = ruleEngine.ruleEngine(user, beacon_id, type, account_id, beaconGuid);

            responseObject.Add("memberId", user.memberId);
            responseObject.Add("userId", user.id);
            responseObject.Add("offer", offer);
            responseObject.Add("account", account);
            responseObject.Add("accountId", account_id);
            responseObject.Add("typeId", type);
            responseObject.Add("beaconId", beacon_id);

            var response = Request.CreateResponse(HttpStatusCode.OK, responseObject);

            return response;
        }

        [Route("getOffersForLandingPage")]
        [HttpGet]        
        //All unexpired offers for landing page(Mobile App)
        public HttpResponseMessage getOffersForLandingPage([FromUri] String memberId)
        {
            User user = new User();
            user.memberId = memberId;
            user = DALUserManager.getUserDetailsByMemberId(user);
            List<Offer> offer = DALmanager.getOffersForLandingPage(user);
            var response = Request.CreateResponse(HttpStatusCode.OK, offer);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);            
        }      

        public override HttpResponseMessage deleteMultiple([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }              
            

        //[Route("getOffers")]
        //[HttpGet]
        //public JsonResult<ResponseWrapper> GetItems()
        //{
        //    String json = "[{\"id\":1,\"offer_id\":\"C101\",\"offer_category\":\"Cash\",\"value\":\"500\",\"title\":\"Let's make your day interesting \",\"description\":\"I will give you $500 to play\",\"image_path\":\"~/Resources/Images/offer_image.jpg\",\"start_time\":\"15-03-2017\",\"end_time\":\"15-04-2017\"}]";
        //    JavaScriptSerializer serializer = new JavaScriptSerializer();
        //    var jsonObject = serializer.Deserialize<dynamic>(json);
        //    return Json(new ResponseWrapper(status: 200, desc: "Ok", obj: jsonObject));
        //}

        public override HttpResponseMessage save([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }

        [Route("getLuckyItemOfferForKiosk")]
        [HttpGet]
        [Authorize]
        //Default offer for kiosk's lucky item
        public HttpResponseMessage getLuckyItemOfferForKiosk([FromUri] Int64 kiosk_id, [FromUri] Int64 lucky_item_id)
        {
            if(new DALKioskManager().isWokable(kiosk_id))
            {
                Offer offer = DALmanager.getLuckyItemOfferForKiosk(kiosk_id, lucky_item_id);
                var response = Request.CreateResponse(HttpStatusCode.OK, offer);
                return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
            }
            else
            {
                var response = Request.CreateResponse(HttpStatusCode.Forbidden, "Please  contact to admin.");
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Forbidden, "Please  contact to admin.");
            }
        }

        [Route("getLuckyItemCurrentActiveOfferForKiosk")]
        [HttpGet]        
        //Current active offer for kiosk's lucky item
        public HttpResponseMessage getLuckyItemCurrentActiveOfferForKiosk([FromUri] Int64 kiosk_id, [FromUri] Int64 lucky_item_id, [FromUri] String member_id)
        {
            Offer offer = new Offer();
                  
            if (new DALKioskManager().isWokable(kiosk_id))
            {
                UserKiosk userKiosk = new UserKiosk();
                userKiosk.kiosk_id = kiosk_id;
                userKiosk.lucky_item_id = lucky_item_id;               
                if (member_id != null)
                {
                    userKiosk.offer_type = 1;
                    userKiosk.member_id = member_id;
                }
                else
                {
                    userKiosk.offer_type = 2;
                }               
                new DALKioskManager().updateKioskLuckyItemCount(kiosk_id, lucky_item_id);

                if(userKiosk.member_id != null)
                {
                    userKiosk.user_id = DALUserManager.getUserIdByMemberId(userKiosk.member_id);
                    offer = kioakRuleEngine.RuleEngine(userKiosk);
                }
                else
                {
                    offer = DALmanager.getLuckyItemCurrentActiveOfferForKiosk(userKiosk);
                }

                var response = Request.CreateResponse(HttpStatusCode.OK, offer);
                return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, offer);
            }                                
            else
            {
                var response = Request.CreateResponse(HttpStatusCode.Forbidden, "Please  contact to admin.");
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Forbidden, "Please  contact to admin.");
            }
        }

        //Start bulk upload of offers
        [Route("upload")]
        [HttpPost]
        public HttpResponseMessage uploadBulkOffers()
        {
            var response = Request.CreateResponse(HttpStatusCode.OK, "Success");
            String responseString = "";
            int recordsInserted = 0;
            int recordsFailed = 0;
            string responseStringFailed = "";
            string responseStringInserted = "";

            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());

            Account account = new Account();
            account = requestUser.account;

            HttpFileCollection files = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files : null;
            Dictionary<string, int> result = BLmanager.uploadOffers(files, requestUser);

            if(result["Status"] == -2)
            {
                responseString = "Please make sure the file that you are uploading is of xls or xlsx format.";
                response = Request.CreateResponse(HttpStatusCode.NotAcceptable, responseString);
                return response;
            }
            else if(result["Status"] == -1) 
            {
                responseString = "Please make sure the file contains mandetory columns.";
                response = Request.CreateResponse(HttpStatusCode.NotAcceptable, responseString);
                return response;
            } else
            {
                if (result["Failed"] > 0)
                {
                    responseStringFailed = "Fail to insert " + result["Failed"] + " record/s";
                }
                if (result["Inserted"] > 0)
                {
                    responseStringInserted = result["Inserted"] + " record/s inserted sucessfully.";
                }
                responseString = responseStringInserted + " " + responseStringFailed;
                response = Request.CreateResponse(HttpStatusCode.OK, responseString);
                return response;
            }
        }
        //End bulk upload of offers

    }
}