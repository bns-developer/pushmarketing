﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BNS.Wrapper;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;
using System.Web.Http.Results;
using BNS.WebAPI.Controllers.BNSControllers;
using BNS.BL.BLKiosk;
using BNS.DAL.DALKiosk;
using BNS.Models.Accounts;
using BNS.Models.Users;
using BNS.Models.Kiosks;
using BNS.Models.Stores;
using Newtonsoft.Json;
using System.Web;

namespace BNS.API.Controllers.KioskControllers
{
    [RoutePrefix("api/v1/kiosks")]
    public class KioskController : BNSController
    {
        KioskBL BLmanager = new KioskBL();
        DALKioskManager DALmanager = new DALKioskManager();
        ResponseWrapper responseWrapper = new ResponseWrapper();

        [Route("all")]
        [HttpGet]
        [Authorize]
        //All kiosks by accountId
        public override HttpResponseMessage GetAll()
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            List<Kiosk> kiosks = DALmanager.getAll(requestUser.account);
            var response = Request.CreateResponse(HttpStatusCode.OK, kiosks);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, kiosks);
        }

        [Route("getKiosksByStoreId")]
        [HttpGet]
        [Authorize]
        //All kiosks by storeId
        public HttpResponseMessage getKiosksByStoreId([FromUri] Int64 storeId)
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            List<Kiosk> kiosks = DALmanager.getKiosksByStoreId(storeId);
            var response = Request.CreateResponse(HttpStatusCode.OK, kiosks);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, kiosks);
        }

        [Route("update")]
        [HttpPost]
        [Authorize]
        public override HttpResponseMessage update([FromBody] JObject requestJSON)
        {
            Int64 kioskId = (Int64)requestJSON["id"];
            if(DALmanager.isWokable(kioskId))
            {
                User requestUser = new User();
                requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
                Kiosk updateKiosk = new Kiosk(requestJSON);
                updateKiosk.createdBy = requestUser;
                updateKiosk.updatedBy = requestUser;
                if (DALmanager.update(updateKiosk))
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Kiosk Updated");
                else
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Kiosk Not Updated");
            }
            else
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Forbidden, "This kiosk is not workable, set it as workable to update.");
            }         
        }

        [Route("updateFromDevice")]
        [HttpPost]
        [Authorize]
        public  HttpResponseMessage updateFromDevice([FromBody] JObject requestJSON)
        {
            Int64 kioskId = (Int64)requestJSON["id"];
            if (DALmanager.isWokable(kioskId))
            {
                User requestUser = new User();
                requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
                Kiosk updateKiosk = new Kiosk(requestJSON);
                updateKiosk.createdBy = requestUser;
                updateKiosk.updatedBy = requestUser;
                if (DALmanager.updateFromDevice(updateKiosk))
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Kiosk Updated");
                else
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Kiosk Not Updated");
            }
            else
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Forbidden, "This kiosk is not workable");
            }
        }

        [Route("delete")]
        [HttpPost]
        [Authorize]
        public override HttpResponseMessage delete([FromBody] JObject requestJSON)
        {
            Int64 kioskId = (Int64)requestJSON["id"];
            if(DALmanager.isWokable(kioskId))
            {
                if (DALmanager.isKioskUsedForRegistration(kioskId))
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Unable to delete the kiosk as there is a data associated with this kiosk. ");
                }
                else
                {
                    if (DALmanager.delete(kioskId))
                        return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Kiosk Deleted Successfully.");
                    else
                        return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Kiosk Not Deleted.");
                }
            }
            else
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Forbidden, "This kiosk is not workable, set it as workable to delete.");
            }
            
        }

        [Route("changeKioskWorkableState")]
        [HttpPost]
        [Authorize]
        public HttpResponseMessage changeKioskWorkableState([FromUri] Int64 kioskId)
        {
            if (DALmanager.changeKioskWorkableState(kioskId))
                return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Kiosk state updated successfuly.");
            else
                return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Kiosk state not updated");
        }

        [Route("settingAuthentication")]
        [HttpPost]       
        public HttpResponseMessage settingAuthentication([FromBody] JObject requestJSON)
        {
            User user = new User();
            user.kiosk.id = (Int64)requestJSON["kioskId"];
            user.username = (String)requestJSON["username"];
            user.password = (String)requestJSON["password"];
            if (DALmanager.settingAuthentication(user))
                return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Authenticate User");
            else
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Unauthorized, "Authentication Failed");
        }

        public override HttpResponseMessage deleteMultiple([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }

        public override HttpResponseMessage GetInfo([FromUri] long id)
        {
            throw new NotImplementedException();
        }       


        //[Route("addKiosk")]
        //[HttpPost]
        //public JsonResult<ResponseWrapper> addKiosk([FromBody]JObject kiosk) 
        //{
        //    try
        //    {
        //        Int64 storename = (Int64)kiosk["store_id"];
        //        String kioskname = (string)kiosk["kiosk_name"];

        //        if (!storename.Equals(null) && !kioskname.Equals(null))
        //        {
        //            String json = "{\"id\":125}";
        //            JavaScriptSerializer serializer = new JavaScriptSerializer();
        //            var jsonObject = serializer.Deserialize<dynamic>(json);
        //            return Json(new ResponseWrapper(status: 200, desc: "Successfully Created", obj: jsonObject));                               
        //        }
        //        else
        //        {
        //            String json = "{\"id\":0}";
        //            JavaScriptSerializer serializer = new JavaScriptSerializer();
        //            var jsonObject = serializer.Deserialize<dynamic>(json);
        //            return Json(new ResponseWrapper(status: 400, desc: "Missing data", obj: jsonObject));                   
        //        }
        //    }
        //    catch(NullReferenceException Ex)
        //    {
        //        String json = "{\"id\":0}";
        //        JavaScriptSerializer serializer = new JavaScriptSerializer();
        //        var jsonObject = serializer.Deserialize<dynamic>(json);
        //        return Json(new ResponseWrapper(status: 400, desc: "Missing data", obj: jsonObject));
        //    }           

        //}

     
        [Route("add")]
        [HttpPost]
        [Authorize]
        //kiosk will call this API to add itself
        public override HttpResponseMessage save([FromBody] JObject requestJSON)
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());

            JObject storeJObject = (JObject)requestJSON["store"];
            Store store = new Store(storeJObject);
            Int64 storeId = store.id;

            String kioskName = (String)requestJSON["name"];
            String kioskNumber = (String)requestJSON["number"];
            Kiosk newKiosk = new Kiosk(requestJSON);

            if (!DALmanager.checkDefaultOffer(requestUser.account.id))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Forbidden, "Please set default offers for Kiosk to proceed.");
            }

            else if (DALmanager.checkIfNameExists(kioskName, storeId))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Kiosk name already exist for this store.");
            }

            else if (DALmanager.checkIfNumberExists(kioskNumber, storeId))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Kiosk number already exist for this store.");
            }
            else
            {
                newKiosk.createdBy = requestUser;
                newKiosk.updatedBy = requestUser;
                newKiosk.id = BLmanager.save(newKiosk);
                if (newKiosk.id != 0)
                {
                    JObject kioskId = new JObject();
                    kioskId["id"] = newKiosk.id;
                        
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, kioskId);
                }
                else
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Kiosk Not Saved");
                }
            }
        }

        [Route("getKioskDetails")]
        [HttpGet]
        [Authorize]
        //kiosk details from id
        public HttpResponseMessage getKioskDetails([FromUri] Int64 kioskId)
        {
            Kiosk kiosk = new Kiosk();
            kiosk.id = kioskId;
            kiosk = DALmanager.getKioskDetails(kiosk);
            var response = Request.CreateResponse(HttpStatusCode.OK, kiosk);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, kiosk);

        }



    }
}
