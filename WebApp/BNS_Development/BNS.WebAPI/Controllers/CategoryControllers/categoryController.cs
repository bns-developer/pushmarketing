﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BNS.DAL.DALCategory;
using BNS.Wrapper;
using BNS.Models.Accounts;
using BNS.Models.Categories;
using System.Web;
using BNS.Models.Users;
using BNS.WebAPI.Controllers.BNSControllers;
using Newtonsoft.Json.Linq;

namespace BNS.WebAPI.Controllers.CategoryControllers
{

    [RoutePrefix("api/v1/Categories")]
    public class CategoryController : BNSController
    {
        DALCategoryManager manager = new DALCategoryManager();
        ResponseWrapper responseWrapper = new ResponseWrapper();

        [Route("save")]
        [HttpPost]
        [Authorize]
        //Save category
        public override HttpResponseMessage save([FromBody] JObject requestJSON)
        {

            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());

            String categoryName = (String)requestJSON["name"];
            Category newCategory = new Category(requestJSON);

            if (manager.checkIfNameExists(categoryName, requestUser.account.id))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.Conflict, "Category Name Already Exist.");

            }
            else
            {
                newCategory.createdBy = requestUser;
                newCategory.updatedBy = requestUser;
                newCategory.account = requestUser.account;
                if (manager.save(newCategory) != 0)
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Category  Saved");
                }
                else
                {
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Category Not Saved");
                }
            }

        }

        [Route("all")]
        [HttpGet]
        [Authorize]
        //All categories for drop-down by account   
        public override HttpResponseMessage GetAll()
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            List<Category> CategoriesList = manager.getAll(requestUser.account.id);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, CategoriesList);
        }

        [Route("list")]
        [HttpGet]
        [Authorize]
        //All categories for list by account
        public HttpResponseMessage All()
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            List<Category> CategoriesList = manager.All(requestUser.account);
            return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, CategoriesList);
        }

        public override HttpResponseMessage GetInfo([FromUri] long id)
        {
            throw new NotImplementedException();
        }

        [Route("edit")]
        [HttpPost]
        [Authorize]
        public override HttpResponseMessage update([FromBody] JObject requestJSON)
        {
            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());
            Category updateCategory = new Category(requestJSON);
            updateCategory.updatedBy = requestUser;
            updateCategory.createdBy = requestUser;
            updateCategory.account = requestUser.account;
            if (manager.update(updateCategory))
                return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Category Updated");
            else
                return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Category Not Updated");
        }

        [Route("delete")]
        [HttpPost]
        [Authorize]
        public override HttpResponseMessage delete([FromBody] JObject requestJSON)
        {
            Int64 categoryId = (Int64)requestJSON["id"];
            if (manager.isCategoryUsedForOffer(categoryId))
            {
                return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Unable to delete the category as it is associated with the offer in the database.");
            }
            else
            {
                if (manager.delete(categoryId))
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.OK, "Category Deleted Successfully.");
                else
                    return responseWrapper.CreateResponse(Request, HttpStatusCode.InternalServerError, "Category Not Deleted.");
            }
        }

        public override HttpResponseMessage deleteMultiple([FromBody] JObject requestJSON)
        {
            throw new NotImplementedException();
        }
    }
}
