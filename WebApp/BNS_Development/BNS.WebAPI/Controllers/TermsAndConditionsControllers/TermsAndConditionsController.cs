﻿using BNS.DAL.DALTermsAndConditions;
using BNS.Models.Accounts;
using BNS.Models.TermsAndConditions;
using BNS.Models.Users;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace BNS.WebAPI.Controllers.TermsAndConditionsControllers
{
    [RoutePrefix("api/v1/TermsAndConditions")]
    public class TermsAndConditionsController : ApiController
    {

        DALTermsAndConditions termsAndConditionsManager = new DALTermsAndConditions();

        [Route("add")]
        [HttpPost]
        [Authorize]
        public HttpResponseMessage addTermsAndConditions([FromBody]JObject details)
        {
            var response = Request.CreateResponse(HttpStatusCode.OK, "Success");

            TermsAndCondition termsAndConditions = new TermsAndCondition();

            termsAndConditions.title = Convert.ToString(details.GetValue("title"));
            termsAndConditions.description = Convert.ToString(details.GetValue("description"));

            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());

            Account account = new Account();
            termsAndConditions.account = requestUser.account;
            termsAndConditions.account.id = requestUser.account.id;

            bool ifTermExists = termsAndConditionsManager.checkTermsAndConditions(termsAndConditions);

            if (ifTermExists)
            {
                response = Request.CreateResponse(HttpStatusCode.Conflict, "Term/Condition already exists");
            } else
            {
                termsAndConditions = termsAndConditionsManager.addTermsAndConditions(termsAndConditions);
                response = Request.CreateResponse(HttpStatusCode.OK, "Successfully added term/condition");
            }

            return response;
        }

        [Route("get")]
        [HttpGet]
        [Authorize]
        public HttpResponseMessage getTermsAndConditions()
        {
            var response = Request.CreateResponse(HttpStatusCode.OK, "Success");

            List<Dictionary<String, object>> responseObject = new List<Dictionary<string, object>>();

            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());

            Account account = new Account();
            account = requestUser.account;

            List<TermsAndCondition> termsAndConditions = new List<TermsAndCondition>();
            termsAndConditions = termsAndConditionsManager.getAllTermsAndConditions(account);
            response = Request.CreateResponse(HttpStatusCode.OK, termsAndConditions);

            return response;
        }

        [Route("delete")]
        [HttpDelete]
        [Authorize]
        public HttpResponseMessage deleteTerm([FromUri]Int64 id)
        {
            var response = Request.CreateResponse(HttpStatusCode.OK, "Success");

            bool confirm = termsAndConditionsManager.deleteTermsAndConditions(id);
            if (confirm)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, "Successfully deleted term/condition");
            } else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound, "Failed to delete term/condition");
            }

            return response;
        }

        [Route("edit")]
        [HttpPost]
        [Authorize]
        public HttpResponseMessage editTermsAndConditions([FromBody]TermsAndCondition termsAndConditionsDetails)
        {
            var response = Request.CreateResponse(HttpStatusCode.OK, "Success");

            bool result = termsAndConditionsManager.editTermsAndConditions(termsAndConditionsDetails);
            if (result)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, "Successfully updated term/condition");
            } else
            {
                response = Request.CreateResponse(HttpStatusCode.NotModified, "Failed to updated term/condition");
            }

            return response;
        }

        [Route("upload")]
        [HttpPost]
        [Authorize]
        public HttpResponseMessage uploadTermsAndConditions()
        {
            var response = Request.CreateResponse(HttpStatusCode.OK, "Success");
            string responseStringFailed = "";
            string responseStringInserted = "";
            string responseString = "";

            User requestUser = new User();
            requestUser = requestUser.getContextUser(HttpContext.Current.GetOwinContext());

            Account account = new Account();
            account = requestUser.account;

            HttpFileCollection files = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files : null;

            Dictionary<string, int> result = termsAndConditionsManager.uploadTermsAndConditions(files, account);

            if(result["Inserted"] == -1)
            {
                response = Request.CreateResponse(HttpStatusCode.NotAcceptable, "Please make sure the file contains mandetory column.");
            }
            else if(result["Inserted"] == -2)
            {
                response = Request.CreateResponse(HttpStatusCode.NotAcceptable, "Please make sure the file that you are uploading is of xls or xlsx format.");
            } else
            {
                if(result["Failed"] != 0)
                {
                    responseStringFailed = "Failed to insert " + result["Failed"] + " record/s due to conflicting title.";
                }
                if(result["Inserted"] != 0)
                {
                    responseStringInserted = result["Inserted"] + " record/s inserted sucessfully.";
                }
                responseString = responseStringInserted + " " + responseStringFailed;
                response = Request.CreateResponse(HttpStatusCode.OK, responseString);
            }

            return response;
        }

    }
}
