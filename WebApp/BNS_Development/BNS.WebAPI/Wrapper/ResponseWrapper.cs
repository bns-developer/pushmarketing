﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;



namespace BNS.Wrapper
{
    public class ResponseWrapper
    {
        private HttpStatusCode status;
        private string desc;
        private string obj;

        public Int64 statusCode { get; set; }
        public String description { get; set; }
        public Object data { get; set; }

        public ResponseWrapper(Int64 status, String desc, object obj = null)
        {
            statusCode = status;
            description = desc;
            data = obj;
        }

        public ResponseWrapper()
        {
        }

        public ResponseWrapper(HttpStatusCode status, string desc, string obj)
        {
            this.status = status;
            this.desc = desc;
            this.obj = obj;
        }

        public HttpResponseMessage CreateResponse<T>(HttpRequestMessage request, HttpStatusCode code, T value)
        {
            String token = "";
            var response = request.CreateResponse(code, value);
            foreach (KeyValuePair<string, IEnumerable<string>> header in request.Headers)
            {
                if (header.Key.Equals("access-token"))
                {
                    string[] stringstokens = (string[])header.Value;
                    token = stringstokens[0];
                }
            }
            //if (token.Equals(""))
            //{
            //    response = request.CreateResponse(code, "Session Expired.");
            //    response.StatusCode = HttpStatusCode.Forbidden;
            //    return response;
            //}
            //else
            //{
                response.Headers.Add("access-token", token);
                JsonSerializer ser = new JsonSerializer();
                string jsonResponse = JsonConvert.SerializeObject(value);
                response.Content = new StringContent(jsonResponse, System.Text.Encoding.UTF8, "application/json");
                return response;
            //}


        }
    }
}