﻿using BNS.WebAPI.CommunicationService.Email;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http.Filters;

namespace BNS.WebAPI.ExceptionHandling
{
    public class ExceptionHandlingFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context) {
            //using (TextWriter tw = new StreamWriter("c:\\Logs\\BNSLogs.txt", true))
            //{
            //    tw.WriteLine("\r\n-------------------------------------" + DateTime.Today.ToString() + "------------------------------------------");
            //    tw.WriteLine("\r\n Exception: " + context.Exception.Message + "\r\r\n Inner Exception:" + context.Exception.InnerException + "\r\n Stack Trace: " + context.Exception.StackTrace);
            //    tw.WriteLine("\r\n----------------------------------------------------------------------------------------------------------------");
            //    tw.Close();
            //}

            using (BnsEmailService service = new BnsEmailService())
            {
                string[] receiver = { "kalpesh@whitesnow.com" };
                service.sendException(receiver, context.Exception);
            }
        }
    }
}