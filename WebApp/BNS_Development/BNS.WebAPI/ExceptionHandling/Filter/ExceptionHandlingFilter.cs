﻿using BNS.WebAPI.CommunicationService.Email;
using System.Web.Http.Filters;

namespace WSExceptionHandling
{
    public class ExceptionHandlingFilter : ExceptionFilterAttribute
    {
        
        public override void OnException(HttpActionExecutedContext context)
        {                          
           // WSFileBasedLogger.sharedInstance.log(context.Exception);
            using (BnsEmailService service = new BnsEmailService())
            {
                string[] receiver = {"kalpesh@whitesnow.com"};
                service.sendException(receiver, context.Exception);
            }         
        }
    }
}