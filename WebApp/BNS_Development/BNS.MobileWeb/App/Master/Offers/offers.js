angular.module('bns.master.offers', ['oc.lazyLoad'])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.offers', {
        url: '/offers',
        views: {
            "offers": {
                templateUrl: 'App/Master/Offers/offers.html',
                controller: 'offersController'
            }
        }

    });
}])
    .directive('fileInput', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attributes) {
                element.bind('change', function () {
                    $parse(attributes.fileInput)
                    .assign(scope, element[0].files)
                    scope.$apply()
                });
            }
        };
    }])


    .directive('currency', function ($filter, $locale) {
 var decimalSep = $locale.NUMBER_FORMATS.DECIMAL_SEP;
 var toNumberRegex = new RegExp('[^0-9\\' + decimalSep + ']', 'g');
 var trailingZerosRegex = new RegExp('\\' + decimalSep + '0+$');
 var filterFunc = function (value) {
   return $filter('currency')(value,"");
 };

 function getCaretPosition(input){
   if (!input) return 0;
   if (input.selectionStart !== undefined) {
     return input.selectionStart;
   } else if (document.selection) {
           // Curse you IE
           input.focus();
           var selection = document.selection.createRange();
           selection.moveStart('character', input.value ? -input.value.length : 0);
           return selection.text.length;
         }
         return 0;
       }

       function setCaretPosition(input, pos){
         if (!input) return 0;
         if (input.offsetWidth === 0 || input.offsetHeight === 0) {
           return; // Input's hidden
         }
         if (input.setSelectionRange) {
           input.focus();
           input.setSelectionRange(pos, pos);
         }
         else if (input.createTextRange) {
           // Curse you IE
           var range = input.createTextRange();
           range.collapse(true);
           range.moveEnd('character', pos);
           range.moveStart('character', pos);
           range.select();
         }
       }

       function toNumber(currencyStr) {
         return parseFloat(currencyStr.replace(toNumberRegex, ''), 10);
       }

       return {
         restrict: 'A',
         require: 'ngModel',
         link: function postLink(scope, elem, attrs, modelCtrl) {    
           modelCtrl.$formatters.push(filterFunc);
           modelCtrl.$parsers.push(function (newViewValue) {
             var oldModelValue = modelCtrl.$modelValue;
             var newModelValue = toNumber(newViewValue);
             modelCtrl.$viewValue = filterFunc(newModelValue);
             var pos = getCaretPosition(elem[0]);
             elem.val(modelCtrl.$viewValue);
             var newPos = pos + modelCtrl.$viewValue.length -
             newViewValue.length;
             if ((oldModelValue === undefined) || isNaN(oldModelValue)) {
               newPos -= 3;
             }
             setCaretPosition(elem[0], pos);
             return newModelValue;
           });
         }
       };
     })


.controller("offersController", ['$scope', '$http', 'API_URL', 'ngDialog', 'API_SERVICE', 'LOADING_MESSAGE','$timeout', function ($scope, $http, API_URL, ngDialog, API_SERVICE, LOADING_MESSAGE, $timeout) {
    $scope.saveButtonName = "Save";
    $scope.submitted = false;
    $scope.dateError = false;
    $scope.successAlert = false;
    $scope.faliureAlert = false;
    $scope.image = "";
    $scope.loadingMessage = LOADING_MESSAGE;
    var today = new Date();
    today.setHours(0, 0, 0, 0);

    $('#offerDatetime').daterangepicker({

        timePicker: true,
        timePickerIncrement: 1,
        format: 'MM/DD/YYYY h:mm:ss A',
        minDate: today,
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear'
        },
        "startDate": new Date(),
        "endDate": new Date()
    });

    $scope.offerTypes = [
        {type : "Member", value : 1},
        {type: "Non-member", value: 2 },
        {type: "Both", value: 3 }
    ];

    $scope.newOffer = {
        "categories": "",
        "countries": "",
        "title": "",
        "description": "",
        "startTime": "",
        "endTime": "",
        "number": "",
        "imagePath": "",
        "value": "",
        "offerType":""
    };

    //function getTodaysDate()
    //{
    //    var today = new Date();
    //    var dd = today.getDate();
    //    var mm = today.getMonth() + 1; //January is 0! 
    //    var yyyy = today.getFullYear();
    //    if (dd < 10) { dd = '0' + dd }
    //    if (mm < 10) { mm = '0' + mm }
    //    var today = dd + '/' + mm + '/' + yyyy;
    //    return today;
    //}

    //------------------ here we will create category object-----------//
    $scope.category = {
        "id": "",
        "value": ""
    };

    var table_advanced = function () {

        var handleDatatable = function () {
            var spinner = $(".spinner").spinner();
            var table = $('#table_id').dataTable({
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "order": [],
                "aoColumnDefs": [{
                    'bSortable': false,
                    'aTargets': [0]
                },
                {
                    'bSortable': false,
                    'aTargets': [9]
                }],
                "columns": [
                        null,
                        null,
                        null,
                        null,
                        { "className": "alignRight" },
                        { "className": "alignRight" },
                        { "className": "alignRight" },
                        { "className": "alignRight" },
                        null,
                        null                       
                        ]
            });

            var tableTools = new $.fn.dataTable.TableTools(table, {
                "sSwfPath": "libs/assets/vendors/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                "buttons": [
                    "copy",
                    "csv",
                    "xls",
                    "pdf",
                    { "type": "print", "buttonText": "Print me!" }
                ]
            });
            $(".DTTT_container").css("float", "right");

        };
        return {
            init: function () {
                handleDatatable();
            }
        };
    }(jQuery);

    setTimeout(function () {
        //$('.selectpicker').selectpicker({
        //    iconBase: 'fa',
        //    tickIcon: 'fa-check'
        //});
        table_advanced.init();
        $('input[name="daterangepicker-date-time"]').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' });

    }, 2000);

    $scope.addOffer = function () {
        ngDialog.open({
            template: 'addOfferDialog',
            scope: $scope,
            preCloseCallback: function () { $scope.clearModal() }
        });
    }

    $scope.closeModal = function () {
        alert("Cancel Clicked...!!!");
        ngDialog.close({
            template: 'addStoreDialog',
            scope: $scope
        });
        $scope.clearModal();
    }

    $scope.clearModal = function () {
        $scope.submitted = false;
        $scope.successAlert = false;
        $scope.faliureAlert = false;
        $scope.date = "";
        $scope.newOffer = {
            "categories": [],
            "countries": [],
            "title": "",
            "description": "",
            "startTime": "",
            "endTime": "",
            "number": "",
            "imagePath": "",
            "value": "",
            "offerType": ""
        };
        $('#offerDatetime').val('');
        $scope.dateError = false;
        getAllCategories();
    }

    $scope.checkDate = function () {
        $scope.newOffer.startTime = ($('#offerDatetime').val()).split('-')[0];
        $scope.newOffer.endTime = ($('#offerDatetime').val()).split('-')[1];
        if ($scope.newOffer.startTime == "" || $scope.newOffer.endTime == "" || $scope.newOffer.startTime == undefined || $scope.newOffer.endTime == undefined) {
            $scope.dateError = true;
        }
        else {
            $scope.dateError = false;
        }
    }

    $scope.saveNewOffer = function (form) {
        console.log($('#offerDatetime').val());
        console.log($scope.image.file);
        $scope.submitted = true;

        //var tempArray = [];
        //var countries = $scope.countries;
        //for (var i = 0; i < countries.length ; i++)
        //{
        //    var temp = {"id": countries[i]};

        //    tempArray.push(temp);
        //}
        //$scope.countries = tempArray;
        $scope.newOffer.startTime = ($('#offerDatetime').val()).split('-')[0];
        $scope.newOffer.endTime = ($('#offerDatetime').val()).split('-')[1];
        $scope.newOffer.imagePath = $scope.image[0];
        if (form.$valid == false || $scope.InvalidCountryGroupSelected || $scope.newOffer.categories == "" || $scope.newOffer.categories == undefined || $scope.newOffer.countries == "" || $scope.newOffer.countries == undefined || $scope.newOffer.title == ""
            || $scope.newOffer.title == undefined || $scope.newOffer.description == "" || $scope.newOffer.description == undefined || $scope.newOffer.startTime == "" || $scope.newOffer.startTime == undefined
            || $scope.newOffer.endTime == "" || $scope.newOffer.endTime == undefined || $scope.newOffer.duration == "" || $scope.newOffer.duration == undefined) {
            if ($scope.newOffer.startTime == "" || $scope.newOffer.startTime == undefined
            || $scope.newOffer.endTime == "" || $scope.newOffer.endTime == undefined) {
                $scope.dateError = true;
            }
            else {
                $scope.dateError = false;
            }
            $scope.saveButtonName = "Save";
        }
        else {
            $scope.saveButtonName = "Saving";
            $scope.dateError = false;
            function success(data, status, headers, config) {
                getAllOffersList();
                $scope.submitted = false;
                $scope.successAlert = true;
                $scope.faliureAlert = false;
                $scope.image = [];
                $scope.newOffer = {
                    "category": [],
                    "country": [],
                    "title": "",
                    "description": "",
                    "startTime": "",
                    "endTime": "",
                    "number": "",
                    "imagePath": "",
                    "value": "",
                    "offerType": ""
                };
                $('#offerDatetime').val('');
                $scope.loading = false;
                $scope.saveButtonName = "Save";
            }
            function failure(data, status, headers, config) {
                $scope.loading = false;
                $scope.saveButtonName = "Save";
                $scope.successAlert = false;
                $scope.faliureAlert = true;
                if (status == 409) {
                    $scope.errorMessage = data;
                }
                else {
                    $scope.errorMessage = "Could not add offer to the list. Please check all the fields."
                }
            }
            var url = "/Offers/save";
            API_SERVICE.postMultiPartData($scope, $http, $scope.newOffer, url, success, failure);
            //API_SERVICE.postData($scope, $http, $scope.newOffer, url, success, failure);
        }
    }

    function getAllOffersList() {
        $scope.loading = true;
        $scope.dataLoader = 0;
        function success(data, status, headers, config) {
            $scope.offers = data;
            console.log($scope.offers);
            $scope.loading = false;
            $scope.dataLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.dataLoader = 2;
        }
        var url = "/Offers/all";
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getAllOffersList();

    function getAllCountries() {
        $scope.loading = true;
        function success(data, status, headers, config) {
            for(var i in data){
                data[i].name = data[i].name + " ("+ data[i].currency +")";
            }
            $scope.countries = data;
            console.log($scope.stores);
            $scope.loading = false;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
        }
        var url = "/countries/all";
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getAllCountries();

    function getAllCategories() {
        $scope.loading = true;
        function success(data, status, headers, config) {
            //$scope.categories = data;
            //setTimeout(function () {
                $scope.newOffer.categories = data;
            //}, 0);
            console.log($scope.categories);
            $scope.loading = false;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
        }
        var url = "/Categories/all";
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getAllCategories();

    $scope.reloadOfferList = function () {
        getAllOffersList();
    }


    $scope.reloadSelectPicker = function () {
        $('.selectpicker').selectpicker('refresh');
    }
    

    $scope.onSelectListChange = function () {
        if ($scope.selectedCategories == undefined){
            for (var index in $scope.newOffer.categories) {
                $scope.newOffer.categories[index]["is_selected"] = false;
            }
            $scope.newOffer.is_country_disabled = true;
            $scope.newOffer.countries = [];
            return;
        }
        for (var index in $scope.newOffer.categories) {
            if (isSelectedChecked($scope.newOffer.categories[index].id))
                $scope.newOffer.categories[index].is_selected = true;
            else
                $scope.newOffer.categories[index].is_selected = false;
            $scope.newOffer.is_country_disabled = false;
        }
    }

    $scope.$watch('newOffer.is_country_disabled',function(newValue, oldValue){
        $timeout(function(){
                $("#countryDropdown").selectpicker('refresh');
            },100);
    });

    function isSelectedChecked(id) {
        for (var selectedCategory in $scope.selectedCategories) {
            if (id == $scope.selectedCategories[selectedCategory]) {
                return true;
            }
        }
        return false;
    }

    $scope.submit = function (newOfferForm) {
        var x = newOfferForm;
    }

    $scope.changeSelectedCountryCurrency = function(){
        if($scope.newOffer.countries){
            $scope.selectedCountryCurrency = $scope.newOffer.countries[0].currency;
            var selectedCurrencyIcon = "";
            for(var i in $scope.newOffer.countries){
                if(selectedCurrencyIcon != "" && selectedCurrencyIcon != $scope.newOffer.countries[i].currency){
                    $scope.InvalidCountryGroupSelected = true;
                    return;
                }
                else{
                    selectedCurrencyIcon = $scope.newOffer.countries[i].currency;
                    $scope.InvalidCountryGroupSelected = false;
                }
            }
        }
        else{
            $scope.selectedCountryCurrency = "";
        }    
    }
    

}]);