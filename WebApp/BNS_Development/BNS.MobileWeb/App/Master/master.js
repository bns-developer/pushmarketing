angular.module('bns.master', [
    'bns.master.dashboard',
    'bns.master.stores',
    'bns.master.kiosks',
    'bns.master.luckyCharacters',
    'bns.master.setKioskOffer',
    'bns.master.beacons',
    'bns.master.offers',
    'bns.master.users',
    'bns.master.profile'
    ])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
    .state('master', {
        url: "",
        abstract: true,
        views: {
            "master": {
                templateUrl: "App/Master/master.html",
                controller: 'masterCtrl'
            },
        },
    });
}])
.controller('masterCtrl', ['$state', '$scope', '$window', '$http', 'API_SERVICE', 'TOKEN_SERVICE', 'MEDIA_URL', '$rootScope', function ($state, $scope, $window, $http, API_SERVICE, TOKEN_SERVICE, MEDIA_URL, $rootScope) {
    if (TOKEN_SERVICE.RestoreState()['access_token'] == null || TOKEN_SERVICE.RestoreState()['access_token'] == '' || TOKEN_SERVICE.RestoreState()['access_token'] == undefined) {
       $state.go('login');
   }

    function getAccountDetails() {
        $scope.loading = true;
        $scope.dataLoading = 0;
        function success(data, status, headers, config) {
            $scope.accountDetails = data;
            $window.document.title = data.name;
            $scope.imgSrc = MEDIA_URL + data.picUrl;
            //Here we are changing icon in header
            var link = document.querySelector("link[rel*='icon']") || document.createElement('link');
            link.type = 'image/x-icon';
            link.rel = 'shortcut icon';
            link.href = $scope.imgSrc;
            document.getElementsByTagName('head')[0].appendChild(link);


            console.log($scope.accountDetails);
            $scope.loading = false;
            $scope.dataLoading = 1;
        }
        function failure(data, status, headers, config) {
            $window.document.title = "BNS";
           
            $scope.loading = false;
            $scope.dataLoading = 2;
        }
        var url = "/accounts/details";
        API_SERVICE.getData($scope, $http, url, success, failure);

    }
    getAccountDetails();

    $scope.logout = function () {
        TOKEN_SERVICE.Delete();
        $state.go('login');
    }

    $scope.currentDate = new Date();

    }])
