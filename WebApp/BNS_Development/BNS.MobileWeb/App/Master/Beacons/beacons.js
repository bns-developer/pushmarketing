angular.module('bns.master.beacons', [])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.beacons', {
        url: '/beacons',
        views: {
            "beacons": {
                templateUrl: 'App/Master/Beacons/beacons.html',
                controller: 'beaconsController'
            }
        }
    });

    $stateProvider.state('master.beaconDetails', {
        url: '/beaconDetails?beaconId',
        views: {
            "beaconDetails": {
                templateUrl: 'App/Master/Beacons/BeaconDetails/BeaconDetails.html',
                controller: 'beaconDetailsController'
            }
        }
    });
}])
.controller("beaconsController", ['$scope', '$http', 'API_URL', 'LOADING_MESSAGE', 'ngDialog', 'API_SERVICE', '$state', function ($scope, $http, API_URL, LOADING_MESSAGE, ngDialog, API_SERVICE, $state) {

    $scope.saveButtonName = "Save";
    $scope.loadingMessage = LOADING_MESSAGE;
    $scope.submitted = false;
    $scope.successAlert = false;
    $scope.failureAlert = false;

    var table_advanced = function () {

        var handleDatatable = function () {
            var spinner = $(".spinner").spinner();
            var table = $('#table_id').dataTable({
                
                "bInfo": false,
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "order": [[1, "asc"]],
                "aoColumnDefs": [{
                    'bSortable': false,
                    'aTargets': [0]
                },
                {
                    'bSortable': false,
                    'aTargets': [7]
                },],
                "columns": [
                        null,
                        null,
                        null,                       
                        { "className": "alignRight" },
                        { "className": "alignRight" },
                        { "className": "alignRight" },
                        { "className": "alignRight" },
                        null                                           
                        ]
            }).columns.adjust()
    .responsive.recalc();

            var tableTools = new $.fn.dataTable.TableTools(table, {
                "sSwfPath": "libs/assets/vendors/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                "buttons": [
                    "copy",
                    "csv",
                    "xls",
                    "pdf",
                    { "type": "print", "buttonText": "Print me!" }
                ]
            });
            $(".DTTT_container").css("float", "right");
            table.responsive.recalc();
        };
        return {
            init: function () {
                handleDatatable();
            }
        };
    }(jQuery);

    setTimeout(function () {
        table_advanced.init();
    }, 2000);

    function getAllBeaconList() {
        $scope.loading = true;
        $scope.dataLoader = 0;
        function success(data, status, headers, config) {
            $scope.beacons = data;
            $scope.loading = false;
            $scope.dataLoader = 1;
      
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.dataLoader = 2;
        }
        var url = "/beacons/all";
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getAllBeaconList();

    $scope.goBeaconDetails = function (beaconId) {
        $state.go("master.beaconDetails", { "beaconId": beaconId });
    }
    $scope.reloadBeaconList = function () {
        getAllBeaconList();
    }


    //-------------------------------------------------------------------------------- add beacon dialog

    $scope.newBeacon = {
        "name": "",
        "number": "",
        "store": { "id": "" }
    };

    $scope.addBeacon = function () {
        ngDialog.open({
            template: 'addBeaconDialog',
            scope: $scope,
            preCloseCallback: function () { $scope.clearModal() }

        });
    }

    $scope.closeModal = function () {
        ngDialog.close({
            template: 'addStoreDialog',
            scope: $scope
        });
        $scope.clearModal();
    }

    $scope.clearModal = function () {
        $scope.submitted = false;
        $scope.successAlert = false;
        $scope.faliureAlert = false;
        $scope.newBeacon = {
            "name": "",
            "number": "",
            "store": { "id": "" }
        };
    }


    function getAllStoresList() {
        $scope.loading = true;
        function success(data, status, headers, config) {
            $scope.stores = data;
            console.log($scope.stores);
            $scope.loading = false;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
        }
        var url = "/stores/all";
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getAllStoresList();

    $scope.saveNewBeacon = function () {
        $scope.submitted = true;
        $scope.saveButtonName = "Saving";
        if ($scope.newBeacon.name == "" || $scope.newBeacon == undefined || $scope.newBeacon.store.id == "" || $scope.newBeacon.store.id == undefined) {
            $scope.saveButtonName = "Save";
        }
        else {
            function success(data, status, headers, config) {
                getAllBeaconList();
                $scope.submitted = false;
                $scope.successAlert = true;
                $scope.faliureAlert = false;
                //$scope.closeModal();
                $scope.newBeacon = {
                    "name": "",
                    "number": "",
                    "store": { "id": "" }
                };
                //check if we can replave above code with clear modal
                $scope.loading = false;
                $scope.saveButtonName = "Save";
            }
            function failure(data, status, headers, config) {
                $scope.loading = false;
                $scope.successAlert = false;
                $scope.faliureAlert = true;
                $scope.saveButtonName = "Save";
                if (status == 409) {
                    $scope.errorMessage = data;
                }
                else {
                    $scope.errorMessage = "Could not add beacon. Please check all the fields."
                }
            }
            var url = "/beacons/add";
            API_SERVICE.postData($scope, $http, $scope.newBeacon, url, success, failure);
        }
        console.log($scope.newBeacon);
    }

}])