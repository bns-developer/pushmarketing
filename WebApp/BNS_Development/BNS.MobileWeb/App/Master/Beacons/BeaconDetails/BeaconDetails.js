angular.module('bns.master.beacons')
.controller("beaconDetailsController", ['$scope', '$http', 'API_URL', 'ngDialog', '$stateParams', 'API_SERVICE', 'MEDIA_URL', 'LOADING_MESSAGE', '$timeout', function ($scope, $http, API_URL, ngDialog, $stateParams, API_SERVICE, MEDIA_URL, LOADING_MESSAGE, $timeout) {

    $scope.successAlert = false;
    $scope.showPopUp = false;
    $scope.faliureAlert = false;
    $scope.copyButtonText = "Copy";
    $scope.loadingMessage = LOADING_MESSAGE;
    var table = null;
    var defaultOfferTable = null;
    $scope.getLuckyItemPic = function (icon) {
        $scope.imagePath = MEDIA_URL + icon;
        return $scope.imagePath;
    }


    $timeout(function () {
        defaultOfferTable = $('#defaultOffer-table').dataTable({
            //"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "bPaginate": false,
            "order": [[1, "asc"]],
            "aoColumnDefs": [{
                'bSortable': false,
                'aTargets': [0]
            }]
        });
    }, 500);

    //var table_advanced = function () {

    //    var handleDatatable = function () {
    //        var spinner = $(".spinner").spinner();
    //        table = $('.table-advanced').dataTable({
    //            //"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
    //            "bPaginate": false,
    //            "order": [[1, "asc"]],
    //            "aoColumnDefs": [{
    //                'bSortable': false,
    //                'aTargets': [0]
    //            }]
    //        });

            
    //    };
    //    return {
    //        init: function () {
    //            handleDatatable();
    //        }
    //    };
    //}(jQuery);

    $timeout(function () {
        table = $('.table-advanced').dataTable({
            //"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "bFilter": true,
            "bPaginate": false,
            "order": [[1, "asc"]],
            "aoColumnDefs": [{
                'bSortable': false,
                'aTargets': [0]
            }]
        });
        var tableTools = new $.fn.dataTable.TableTools(table, {
            "sSwfPath": "libs/assets/vendors/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
            "buttons": [
                "copy",
                "csv",
                "xls",
                "pdf",
                { "type": "print", "buttonText": "Print me!" }
            ]
        });
        $(".DTTT_container").css("float", "right");
    }, 500);

    $scope.copyUrlToClipboard= function (text) {
        if (window.clipboardData && window.clipboardData.setData) {
            $scope.copyButtonText = "Copied";
            $timeout(function () {
                $scope.copyButtonText = "Copy";
            }, 1000);
            return clipboardData.setData("Text", text);

        } else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
            var textarea = document.createElement("textarea");
            textarea.textContent = text;
            $scope.copyButtonText = "Copied";
            $timeout(function () {
                $scope.copyButtonText = "Copy";
            }, 1000);
            textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in MS Edge.
            document.body.appendChild(textarea);
            textarea.select();
            try {
               
                return document.execCommand("copy");  // Security exception may be thrown by some browsers.
            } catch (ex) {
                console.warn("Copy to clipboard failed.", ex);
                return false;
            } finally {
                document.body.removeChild(textarea);
            }
        }
    }

    $scope.setDefaultOffer = function () {
        ngDialog.open({
            template: 'setDefaultOfferDialog',
            className: 'ngdialog-theme-default modal-large',
            scope: $scope,
            preCloseCallback: function () { $scope.clearModal() }
        });
    }

    $scope.setOffer = function () {
        ngDialog.open({
            template: 'setOfferDialog',
            className: 'ngdialog-theme-default modal-large',
            scope: $scope,
            preCloseCallback: function () { $scope.clearModal() }
        });
    }

    $scope.closeModal = function () {
        ngDialog.close({
            template: 'setOfferDialog',
            scope: $scope,
        });
        $scope.clearModal();
    }

    $scope.clearModal = function () {
        $scope.successAlert = false;
        $scope.faliureAlert = false;
    }

    function getDefaultOffer() {
        $scope.loading = true;
        $scope.defaultOfferLoader = 0;
        function success(data, status, headers, config) {
            $scope.offer = data;
            console.log($scope.offer);
            //$scope.offer.date = $scope.offer[0].createdOn.split(' ')[0];
            //table.ajax.reload();
            $scope.loading = false;
            $scope.defaultOfferLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.defaultOfferLoader = 2;
        }
        var url = "/Offers/getBeaconDfaultOffer?beacon_id=" + $stateParams.beaconId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getDefaultOffer();

    function getBeaconInfo() {
        $scope.loading = true;
        $scope.dataLoading = 0;
        function success(data, status, headers, config) {
            $scope.beaconInfo = data;
            console.log($scope.offer);
            $scope.loading = false;
            $scope.dataLoading = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.dataLoading = 2;
        }
        var url = "/beacons/details?beaconId=" + $stateParams.beaconId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getBeaconInfo();


    function getNonDefaultOffers() {
        $scope.loading = true;
        $scope.nonDefaultOffersLoader = 0;
        function success(data, status, headers, config) {
            $scope.nonDefaultOffers = data;
            console.log($scope.nonDefaultOffers);
            $scope.loading = false;
            $scope.nonDefaultOffersLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.nonDefaultOffersLoader = 2;
        }
        var url = "/Offers/getBeaconNonDefaultOffers?beacon_id=" + $stateParams.beaconId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getNonDefaultOffers();

    function getInactiveOffers() {
        $scope.loading = true;
        $scope.inactiveOffersLoader = 0;
        function success(data, status, headers, config) {
            $scope.inactiveOffers = data;
            console.log($scope.inactiveOffers);
            $scope.loading = false;
            $scope.inactiveOffersLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.inactiveOffersLoader = 2;
        }
        var url = "/Offers/getBeaconInactiveOffers?beacon_id=" + $stateParams.beaconId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getInactiveOffers();

    function getActiveOffers() {
        $scope.loading = true;
        $scope.activeOfferLoader = 0;
        function success(data, status, headers, config) {
            $scope.activeOffers = data;
            console.log($scope.activeOffers);
            $scope.loading = false;
            $scope.activeOfferLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.activeOfferLoader = 2;
        }
        var url = "/Offers/getBeaconActiveOffers?beacon_id=" + $stateParams.beaconId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getActiveOffers();

    function getOffersHistory() {
        $scope.loading = true;
        $scope.offerHistoryLoader = 0;
        function success(data, status, headers, config) {
            $scope.offersHistory = data;
            console.log($scope.activeOffers);
            $scope.loading = false;
            $scope.offerHistoryLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.offerHistoryLoader = 2;
        }
        var url = "/Offers/getBeaconOffersHistory?beacon_id=" + $stateParams.beaconId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getOffersHistory();

    $scope.setNonDefaultActive = function (offerId) {
        function success(data, status, headers, config) {
            getDefaultOffer();
            getNonDefaultOffers();
            defaultOfferTable.fnDraw();
            $scope.successAlert = true;
            $scope.faliureAlert = false;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.successAlert = false;
            $scope.faliureAlert = true;
        }
        var url = "/Offers/changeBeaconDefaultOffer?beacon_id=" + $stateParams.beaconId + "&&offer_id=" + offerId;
        API_SERVICE.postData($scope, $http, "", url, success, failure);
    }

    $scope.isOfferActive = function (isOfferActive) {
        if (isOfferActive)
            isOfferActive = "Active";
        else
            isOfferActive = "-";
        return isOfferActive;
    }

    //------------reload-------------------------//
    $scope.reloadData = function () {
        getBeaconInfo();
    }

    $scope.reloadDefaultOffersList = function () {
        getDefaultOffer();
    }

    $scope.reloadActiveOffersList = function () {
        getActiveOffers();
    }

    $scope.reloadOffersHistory = function () {
        getOffersHistory();
    }

    $scope.reloadNonDefaultOffersList = function () {
        getNonDefaultOffers();
    }

    $scope.reloadInactiveOffersList = function () {
        getInactiveOffers();
    }

    $scope.setActive = function (offerId) {
        function success(data, status, headers, config) {
            getActiveOffers();
            getInactiveOffers();
            $scope.successAlert = true;
            $scope.faliureAlert = false;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.successAlert = false;
            $scope.faliureAlert = true;
        }
        var url = "/Offers/setOfferForBeacon?beacon_id=" + $stateParams.beaconId + "&&offer_id=" + offerId;
        API_SERVICE.postData($scope, $http, "", url, success, failure);
    }

}])