angular.module('bns.mobile.getMemberID', [])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
    .state('mobile.getMemberID', {
        url: "/beacon/memberDetails",
        views: {
            "getMemberID": {
                templateUrl: "App/Mobile/getMemberID/getMemberID.html",
                controller: 'getMemberIDCtrl'
            },
        },
        params: {
            userOffer: {
            }
        }
    });
}])
.controller('getMemberIDCtrl', ['$state', '$scope', '$rootScope', '$stateParams', '$timeout', 'API_SERVICE', '$http', 'IMAGE_URL', 'TimerService', 'ngDialog', function ($state, $scope, $rootScope, $stateParams, $timeout, API_SERVICE, $http, IMAGE_URL, TimerService, ngDialog) {

    $scope.memberId = undefined;
    $scope.submitButtonName = "Avail this offer now!";
    $scope.submitEmailButtonName = 'Submit';
    $scope.accountImg = Cookies.get("accountImg");
    $scope.emailAddress = undefined;
    $scope.invalidEmail = false;
    $scope.wrongMemberId = false;
    $scope.emptyEmail = false;
    $scope.showResponse = false;
    $scope.memberDetails = {
        'accountId': undefined,
        'email': undefined,
        'member_id': undefined
    };
    $scope.memberSocialDetails = {
        'accountId': undefined,
        'offerId': 0,
        'beaconId': undefined,
        'memberId': undefined,
        'unique_id': undefined
    };

    $('#confirm').modal('hide');

    if ($stateParams.userOffer == undefined) {
        $state.go("mobile.landing");
        return;
    }

    $scope.userDetails = {
        memberId: undefined,
        offerId: 0,
        beaconId: $stateParams.userOffer.beaconId,
        accountId: $stateParams.userOffer.accountId
    };

    console.log($stateParams);

    function init() {
        $scope.userOffer = { categories: "", offerId: "", imagePath: "", endTime: "" };
        $scope.userOffer.categories = $stateParams.userOffer.categories;
        $scope.userOffer.offerId = $stateParams.userOffer.offerId;
        $scope.userOffer.imagePath = $stateParams.userOffer.imagePath;
        $scope.userOffer.endTime = $stateParams.userOffer.endTime;
        $scope.cookieValue = JSON.parse(Cookies.get("details"));
        //$scope.userDetails.memberId = $scope.cookieValue.currentlyInUse.memberId;
        if ($scope.userOffer.hasOwnProperty('imagePath')) {
            $scope.imgsrc = $scope.userOffer.imagePath;
        }
        else {
            if ($scope.userOffer.categories[0].name == "Cash") {
                $scope.imgsrc = "/Resources/offerImages/Cash.jpg";
            } else if ($scope.userOffer.categories[0].name == "Points") {
                $scope.imgsrc = "/Resources/offerImages/Points.jpg";
            } else if ($scope.userOffer.categories[0].name == "Promotions") {
                $scope.imgsrc = "/Resources/offerImages/Promotions.jpg";
            } else {
                $scope.imgsrc = '/Resources/offerImages/defaultNoCategory.jpg';
            }
        }
        
    }
    init();

    $scope.getOffer = function (offerId) {
        function success(data, status, headers, config) {
            //console.log(data);
            $scope.offer = data;
            $scope.userOffer.countries = data.countries
            $scope.userDetails.offerId = data.id;
        }
        function failure(data, status, headers, config) {
            alert(data);
        }
        var url = "/Offers/details?offerId=" + offerId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    $scope.getOffer($scope.userOffer.offerId);

    $scope.submitForm = function () {
        $scope.submitButtonName = "Please wait ...";
        $scope.cookieValue = JSON.parse(Cookies.get("details"));
        if (/*$scope.userDetails.memberId == $scope.cookieValue.currentlyInUse.memberId*/true) {
            function success(data, status, headers, config) {
                $scope.wrongMemberId = false;
                $scope.submitButtonName = "Avail this offer now!";
                console.log(data);
                $scope.offer = data;
                //title: "", description: "", couponCode:"", memberId:""
                $scope.userOffer.title = data.offer.title;
                $scope.userOffer.description = data.offer.description;
                $scope.userOffer.couponCode = data.offer.code ;
                $scope.userOffer.memberId = data.memberId;
                $scope.userOffer.userOfferStatus = data.userOfferStatus;
                $scope.cookieValue.currentlyInUse.memberId = $scope.userDetails.memberId;
                $scope.cookieValue.currentlyInUse.type = 1;
                Cookies.set("details", JSON.stringify($scope.cookieValue), { expires: 365 });
                TimerService.stopTimer();
                $state.go('mobile.congoNewUser', { 'userOffer': $scope.userOffer });
            }
            function failure(data, status, headers, config) {
                $scope.submitButtonName = "Avail this offer now!";
                if (status == 403) {
                    ngDialog.open({
                        template: 'alreadyAvialed',
                        className: 'ngdialog-theme-default modal-large beacon-dialog draggable',
                        scope: $scope

                    });
                }
                else if (status == 409) {
                    $scope.wrongMemberId = true;
                }
            }
            var url = "/users/member/acceptOffer";
            API_SERVICE.postData($scope, $http, $scope.userDetails, url, success, failure);
        }
        else {
            $scope.submitButtonName = "Avail this offer now!";
            $scope.wrongMemberId = true;
        }
    }

    $scope.gotoLanding = function () {
        $('.modal-backdrop').hide();
        ngDialog.close({
            template: 'alreadyAvialed',
            scope: $scope
        });
        $state.go('mobile.landing')
    }

    $scope.submitEmail = function () {
        $scope.submitted = true;
        if ($scope.emailAddress == undefined || $scope.emailAddress == '')
        {
            $scope.emptyEmail = true;
        } else {
            $scope.emptyEmail = false;
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (re.test($scope.emailAddress)) {
                $scope.invalidEmail = false;
                $scope.memberDetails.accountId = $scope.userDetails.accountId;
                $scope.memberDetails.email = $scope.emailAddress;
                $scope.submitEmailButtonName = 'Please wait...';
                console.log($scope.memberDetails);

                function success(data, status, headers, config) {
                    $scope.retrievedId = data.memberId;
                    $scope.showResponse = true;
                    $scope.successAlert = true;
                    $scope.failureAlert = false;
                    $scope.submitEmailButtonName = 'Submit';
                }
                function failure(data, status, headers, config) {
                    $scope.errorMsg = data;
                    $scope.showResponse = true;
                    $scope.successAlert = false;
                    $scope.failureAlert = true;
                    $scope.submitEmailButtonName = 'Submit';
                }
                var url = "/users/getMemberId";
                API_SERVICE.postData($scope, $http, $scope.memberDetails, url, success, failure);


            } else {
                $scope.invalidEmail = true;
            }
        }
    };

    $scope.clearEmail = function () {
        $scope.submitEmailButtonName = 'Submit';
        $scope.invalidEmail = false;
        $scope.submitted = false;
        $scope.emptyEmail = false;
        $scope.showResponse = false;
        $scope.successAlert = false;
        $scope.failureAlert = false;
        $scope.emailAddress = undefined;
    };


    //------- oN social Login call this function for data ---------------------------//
    function socialLoginData() {
        $scope.submitButtonName = "Please wait ...";
        $scope.cookieValue = JSON.parse(Cookies.get("details"));
        $scope.memberSocialDetails.memberId = $scope.cookieValue.currentlyInUse.memberId;
        function success(data, status, headers, config) {
            $scope.submitButtonName = "Avail this offer now!";
            $scope.offer = data;
            $scope.userOffer.title = data.offer.title;
            $scope.userOffer.description = data.offer.description;
            $scope.userOffer.couponCode = data.offer.code;
            $scope.userOffer.memberId = data.memberId;
            $scope.userOffer.userOfferStatus = data.userOfferStatus;
            $scope.cookieValue.currentlyInUse.memberId = $scope.userDetails.memberId;
            $scope.cookieValue.currentlyInUse.type = 1;
            Cookies.set("details", JSON.stringify($scope.cookieValue), { expires: 365 });
            TimerService.stopTimer();
            $state.go('mobile.congoNewUser', { 'userOffer': $scope.userOffer });
        }
        function failure(data, status, headers, config) {
            $scope.submitButtonName = "Avail this offer now!";
            if (status == 403) {
                ngDialog.open({
                    template: 'alreadyAvialed',
                    className: 'ngdialog-theme-default modal-large beacon-dialog draggable',
                    scope: $scope

                });
            }
        }
        var url = "/users/member/acceptOffer";
        API_SERVICE.postData($scope, $http, $scope.memberSocialDetails, url, success, failure);
    }

    //--------------------logout from gigya socail login -------------------------------------//
    $scope.logoutFromGS = function () {
        gigya.socialize.logout(); // logout from Gigya platform
    }
    //----------------------------------------- Gigya socail login------------------------------------------------------------//

    var onLoginHandler = function (eventObj) {
        // verify the signature ...
        verifyTheSignature(eventObj.UID, eventObj.timestamp, eventObj.signature);
        // Check whether the user is new by searching if eventObj.UID exists in your database
        var newUser = true; // lets assume the user is new

        if (newUser) {
            // 1. Register user
            // 2. Store new user in DB
            // 3. link site account to social network identity
            // 3.1 first construct the linkAccounts parameters
            var dateStr = Math.round(new Date().getTime() / 1000.0); // Current time in Unix format
            //(i.e. the number of seconds since Jan. 1st 1970)

            var siteUID = 'uTtCGqDTEtcZMGL08w'; // siteUID should be taken from the new user record
            // you have stored in your DB in the previous step
            var yourSig = createSignature(siteUID, dateStr);
            var params = {
                siteUID: siteUID,
                timestamp: dateStr,
                cid: '',
                signature: yourSig
            };

            //   3.1 call linkAccounts method:
            gigya.socialize.notifyRegistration(params);
        }
        // here will be code for api calling 
        if (eventObj.hasOwnProperty("provider"))
            $scope.memberSocialDetails.social_service = eventObj.provider;
        if (eventObj.hasOwnProperty("UID"))
            $scope.memberSocialDetails.unique_id = eventObj.UID;
        if (eventObj.user.hasOwnProperty("email")) {
            $scope.memberSocialDetails.email_id = eventObj.user.email;
        }
        $scope.memberSocialDetails.offerId = $scope.userDetails.offerId;
        $scope.memberSocialDetails.beaconId = $scope.userDetails.beaconId;
        $scope.memberSocialDetails.accountId = $scope.userDetails.accountId;

        socialLoginData();
        //document.getElementById('status').style.color = "green";
        //document.getElementById('status').innerHTML = "Status: You are now signed in";
    }

    var onLogoutHandler = function (eventObj) {
        document.getElementById('status').style.color = "red";
        document.getElementById('status').innerHTML = "Status: You are now signed out";
    }

    var onLoadFunction = function () {
        // register for login event

        gigya.socialize.addEventHandlers({
            context: { str: 'congrats on your' }
                , onLogin: onLoginHandler
                , onLogout: onLogoutHandler
        });
    }

    var createSignature = function (UID, timestamp) {
        encodedUID = encodeURIComponent(UID); // encode the UID parameter before sending it to the server.
        // On server side use decodeURIComponent() function to decode an encoded UID
        return '';
    }

    // Note: the actual signature calculation implementation should be on server side
    function verifyTheSignature(UID, timestamp, signature) {
        encodedUID = encodeURIComponent(UID); // encode the UID parameter before sending it to the server.
        // On server side use decodeURIComponent() function to decode an encoded UID
    }

    function initializeGigya() {
        var gigyaParams = {
            version: 2,
            showTermsLink: false, // 'terms' link is hidden
            //headerText: "Please Login using one of the following providers:", // adding header text
            height: 50, // changing default add-on size
            width: '100%',  // changing default add-on size
            cid: '',
            extraFields: 'phones',
            deviceType: 'mobile',
            lastLoginIndication: 'border',
            enabledProviders: $scope.selectedSocialnetworks,
            containerID: 'socialLogin', // The add-on will embed itself inside the "loginDiv" DIV (will not be a popup)
            // Changes to the default design of the add-on's design 
            //     Background color is changed to purple, text color to gray and button size is set to 30 pixels:   
            //UIConfig: '<config><body><texts color="#DFDFDF"></texts><controls><snbuttons buttonsize="30"></snbuttons></controls><background background-color="#51286D"></background></body></config>',
            // Change the buttons design style to the 'fullLogo' style:
            onLoad: onLoadFunction,
            pagingButtonStyle: 'arrows'
            // After successful login - the user will be redirected to "https://www.MySite.com/welcome.html" :  
        };
        $scope.socialUI = gigya.socialize.showLoginUI(gigyaParams);

    }
   
    //-------------  Fetch Social Networks ----------------------------------//
    function refromatSocialData(data) {
        var socialNetworks = "";
        for (var i = 0; i < data.length; i++) {
            if (i > 0)
                socialNetworks = socialNetworks + ", " + (data[i].name);
            else
                socialNetworks = socialNetworks + "" + (data[i].name);
        }
        return socialNetworks;
    }

    function getSelectedSocialNetworks() {
        $scope.socialDataLoader = 0;
        $scope.socialDataLoaderFlag = false;
        function success(data, status, headers, config) {
            $scope.selectedSocialnetworks = refromatSocialData(data);
            if ($scope.selectedSocialnetworks.length > 0) {
                $scope.socialDataLoaderFlag = true;
                $timeout(function () {
                    initializeGigya();
                }, 300);

            }
            else {
                $scope.socialDataLoaderFlag = false;
            }

            $scope.socialDataLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.socialDataLoader = 2;
            $scope.socialDataLoaderFlag = false;
        }
        var url = "/socialNetworkingServices/getSelectedServicesForMobile?accountId=" + (JSON.parse(Cookies.get("details"))).account.id;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getSelectedSocialNetworks();

}])
