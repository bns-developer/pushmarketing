angular.module('bns.mobile.landing', [])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
    .state('mobile.landing', {
        url: "/beacon/landing",
        views: {
            "landing": {
                templateUrl: "App/Mobile/landing/landing.html",
                controller: 'landingCtrl'
            },
        }
    });
}])
.controller('landingCtrl', ['$state', '$scope', '$stateParams','$rootScope', 'API_URL', 'IMAGE_URL', 'API_SERVICE', '$http','$window', function ($state, $scope, $stateParams, $rootScope, API_URL, IMAGE_URL, API_SERVICE, $http, $window) {

    
    //$scope.beaconId = $rootScope.beaconId;
    //$scope.memberId = $.cookie('memberId');
    //$scope.beaconId = $.cookie('beaconId');
    $scope.dataLoader = 1;
    $scope.cookieValue = JSON.parse(Cookies.get("details"));
    $scope.beaconId = $scope.cookieValue.currentlyInUse.beaconId;
    $scope.memberId = $scope.cookieValue.currentlyInUse.memberId;
    $scope.accountName = $scope.cookieValue.account.name;
    
    $window.document.title = $scope.accountName;
    $window.document.head.querySelector("[name=author]").content = $scope.accountName;
    $scope.getAllOffers = function () {
        $scope.dataLoader = 0;
        function success(data, status, headers, config) {
            $scope.offers = data;
            if (data.length == 0) {
                $scope.dataLoader = 2;
            } else {
                $scope.dataLoader = 1;
            }
        }
        function failure(data, status, headers, config) {
            $scope.dataLoader = 2;
            $scope.loading = false;
        }
        var url = "/Offers/getOffersForLandingPage?memberId=" + $scope.memberId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    $scope.getAllOffers();

    $scope.gotoViewOffer = function (offerId, categories, imagePath, endTime) {
        $scope.offer = { categories: "", offerId: "", imagePath: "", endTime: "" };
        $scope.offer.offerId = offerId;
        $scope.offer.categories = categories;
        $scope.offer.imagePath = imagePath;
        $scope.offer.endTime = endTime;
        $state.go("mobile.viewOffer", { "offer": $scope.offer });
    }

    $scope.getBackgroundPic = function (offer) {
        if (offer.hasOwnProperty('imagePath') && (offer.imagePath != "No Image")) {
            return(IMAGE_URL + (offer.imagePath));
        }
        else
        {
            if (offer.categories[0].name == "Cash") {
                return (IMAGE_URL + "/Resources/offerImages/Cash.jpg");
            } else if (offer.categories[0].name == "Points") {
                return (IMAGE_URL + "/Resources/offerImages/Points.jpg");
            } else if (offer.categories[0].name == "Promotion") {
                return (IMAGE_URL + "/Resources/offerImages/Promotions.jpg");
            } else {
              return (IMAGE_URL + '/Resources/offerImages/defaultNoCategory.jpg');
            }
        }
        
    }

    $scope.getTimeLeft = function (endTime) {
        endTime = new Date(endTime);
        return moment(endTime).fromNow();
    }

    $scope.reloadOfferList = function () {
        $scope.getAllOffers();
    }

    $scope.currentDate = new Date();

}])
