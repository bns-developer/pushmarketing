angular.module('bns.mobile.viewOffer', [])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
    .state('mobile.viewOffer', {
        url: "/beacon/view-offer",
        views: {
            "viewOffer": {
                templateUrl: "App/Mobile/viewOffer/viewOffer.html",
                controller: 'viewOfferCtrl',
            },
        },
        params: {
            offer: { offerId: "", category: "", imagePath: "", endTime: "" }
        }
    });
}])
.controller('viewOfferCtrl', ['$state', '$scope', '$rootScope', '$stateParams', 'API_URL', 'LOADING_MESSAGE', 'IMAGE_URL', 'API_SERVICE', '$http', 'TimerService', '$timeout', '$window', function ($state, $scope, $rootScope, $stateParams, API_URL, LOADING_MESSAGE, IMAGE_URL, API_SERVICE, $http, TimerService, $timeout, $window) {
    $scope.loadingMessage = LOADING_MESSAGE;
    $scope.dataLoader = 1;
    $scope.acceptButtonName = "Accept";

    $('#confirm').modal('hide');

    $scope.cookieValue = JSON.parse(Cookies.get("details"));
    $scope.beaconId = $scope.cookieValue.currentlyInUse.beaconId;
    $scope.memberId = $scope.cookieValue.currentlyInUse.memberId;
    $scope.accountImg = Cookies.get("accountImg");
    if($stateParams.offer == undefined){
            $state.go("mobile.landing");
            return;
    }
    $scope.info = {
        memberId: undefined,
        offerId: undefined,
        beaconId: undefined
    };
    $scope.userDetails = {
        memberId: undefined,
        offerId: 0,
        beaconId: undefined,
        accountId: undefined
    };

    function init()
    {
        $scope.cookieValue = JSON.parse(Cookies.get("details"));
        $scope.beaconId = $scope.cookieValue.currentlyInUse.beaconId;
        $scope.memberId = $scope.cookieValue.currentlyInUse.memberId;
        $scope.accountImg = Cookies.get("accountImg");
    }
    init();

    function imgInit() {

        if ($stateParams.offer.hasOwnProperty('imagePath') && ($stateParams.offer.imagePath != "No Image"))
           {
            $scope.imgsrc = IMAGE_URL + $stateParams.offer.imagePath;
          }
        else {
            if ($stateParams.offer.categories[0].name == "Cash") {
                $scope.imgsrc = IMAGE_URL + "/Resources/offerImages/Cash.jpg";
            } else if ($stateParams.offer.categories[0].name == "Points") {
                $scope.imgsrc = (IMAGE_URL + "/Resources/offerImages/Points.jpg");
            } else if ($stateParams.offer.categories[0].name == "Promotion") {
                $scope.imgsrc = (IMAGE_URL +  "/Resources/offerImages/Promotions.jpg");
            } else {
                $scope.imgsrc = IMAGE_URL + '/Resources/offerImages/defaultNoCategory.jpg';
            }
        }
    }
    imgInit();


    function getOffer() {
        $scope.loading = true;
        $scope.dataLoader = 0;
        $scope.info.memberId = $scope.memberId;
        $scope.info.beaconId = $scope.beaconId;
        $scope.info.offerId = $stateParams.offer.offerId;
        function success(data, status, headers, config) {
            $scope.offerToDisplay = data.offer;
            $scope.type = $scope.cookieValue.currentlyInUse.type;
            $scope.accountId = data.account.id;
            $scope.beaconId = data.beaconId;
            $scope.userId = data.userId;
            $window.document.title = $scope.offerToDisplay.title;

            $scope.termsAndConditionsList = data.offer.termsAndConditions;
            if ($scope.termsAndConditionsList.length == 0 || $scope.termsAndConditionsList.length == undefined) {
                $scope.showTerms = false;
            } else {
                $scope.showTerms = true;
            }
            $scope.dataLoader = 1;
            TimerService.initializeTimer(data.offer.duration);
        }
        function failure(data, status, headers, config) {
            $scope.dataLoader = 2;
            $scope.loading = false;
        }
        var language = $window.navigator.language || $window.navigator.userLanguage;
        var header = { 'Content-Type': 'application/json', 'language': language }
        var url = "/Offers/offerDetailsForBeacon";
        API_SERVICE.postData($scope, $http,$scope.info, url, success, failure, header);
    }
    getOffer();

    $scope.reloadOffer = function () {
        $scope.getOffer();
    }


    function acceptOffer()
    {
        $scope.acceptButtonName == "Please wait ...";
        $scope.userDetails = {
            memberId: $scope.memberId,
            offerId: $scope.userOffer.offerId,
            beaconId: $scope.userOffer.beaconId,
            accountId: $scope.userOffer.accountId
        };
        function success(data, status, headers, config) {
            $scope.offer = data;
            //title: "", description: "", couponCode:"", memberId:""
            $scope.userOffer.title = data.offer.title;
            $scope.userOffer.description = data.offer.description;
            $scope.userOffer.couponCode = data.offer.code;
            $scope.userOffer.memberId = data.memberId;
            $scope.userOffer.userOfferStatus = data.userOfferStatus;
            $scope.cookieValue.currentlyInUse.memberId = $scope.userDetails.memberId;
            $scope.cookieValue.currentlyInUse.type = 1;
            Cookies.set("details", JSON.stringify($scope.cookieValue), { expires: 365 });
            TimerService.stopTimer();
            $scope.acceptButtonName = "Accept";
            $state.go('mobile.congoNewUser', { 'userOffer': $scope.userOffer });
        }
        function failure(data, status, headers, config) {
            $scope.acceptButtonName = "Accept";
            if (status == 403) {
                ngDialog.open({
                    template: 'alreadyAvialed',
                    className: 'ngdialog-theme-default modal-large beacon-dialog draggable',
                    scope: $scope

                });
            }
            else if (status == 409) {
                /********************  handle here wrong memberID for novomatic *******************************/
                $scope.wrongMemberId = true;
            }
        }
        var url = "/users/member/acceptOffer";
        API_SERVICE.postData($scope, $http, $scope.userDetails, url, success, failure);
    }

    $scope.gotoUserDetails = function () {
        $scope.userOffer = { categories: "", offerId: "", imagePath: "", endTime: "" };
        $scope.userOffer.categories = $scope.offerToDisplay.categories;
        $scope.userOffer.offerId = $scope.offerToDisplay.id;
        $scope.userOffer.imagePath = $scope.imgsrc;
        $scope.userOffer.endTime = $scope.offerToDisplay.endTime;
        $scope.userOffer.accountId = $scope.accountId;
        $scope.userOffer.beaconId = $scope.beaconId;
        $scope.userOffer.userId = $scope.userId;
        var commonCurrencySymbol = "$";
        if($scope.offerToDisplay.countries.length > 0)
            commonCurrencySymbol = $scope.offerToDisplay.countries[0].currency;
        for(var i in $scope.userOffer.categories){
            if($scope.userOffer.categories[i].name == 'Cash')
                $scope.userOffer.categories[i]["currencySymbol"] = commonCurrencySymbol;
        }
        if ($scope.type == 1)
        {
            /*********  Normal User ***********/
            $state.go('mobile.getMemberID', { 'userOffer': $scope.userOffer });

            /****************************************************   Novomatic User ************************************************************/
            //acceptOffer();
        } else {
            $state.go('mobile.getUserDetail', { 'userOffer': $scope.userOffer });
        }
    }

    $scope.getTimeLeft = function (endTime) {
        
        return moment(endTime).fromNow();
    }

    $scope.gotoLanding = function () {
        $('.modal-backdrop').hide();
        $state.go('mobile.landing')
    }

    $scope.currentDate = new Date();

    //$(window).bind("beforeunload",function(event) {
    //    return "Offer will be discarded. Are you sure you want to continue?";
    //});

    //$scope.$on('$destroy', function() {
    //    delete window.onbeforeunload;
    //});

    }])
