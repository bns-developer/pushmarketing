angular.module('bns.mobile.getUserDetail', [])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
    .state('mobile.getUserDetail', {
        url: "/beacon/get-user-details?userOffer",
        views: {
            "getUserDetail": {
                templateUrl: "App/Mobile/getUserDetail/getUserDetail.html",
                controller: 'getUserDetailCtrl'
            },
        }
    });
}])
.controller('getUserDetailCtrl', ['$state', '$scope', '$rootScope', '$stateParams', '$state', 'API_URL', 'IMAGE_URL', 'LOADING_MESSAGE', 'API_SERVICE', '$http', 'TimerService', '$timeout', function ($state, $scope, $rootScope, $stateParams, $state, API_URL, IMAGE_URL, LOADING_MESSAGE, API_SERVICE, $http, TimerService, $timeout) {
    $scope.loadingMessage = LOADING_MESSAGE;
    $scope.dataLoader = 1;
    $scope.submitButtonName = "Avail this offer now!";
    $scope.errorAccepting = false;
    $scope.socialDataLoaderFlag = false;
    $scope.cookieValue = JSON.parse(Cookies.get("details"));

    $scope.userdetail = {
        "email_id": "",
        "phone": "",
        "country": {
            "id":""
        },
        "registered_from": "",
        "beacon": {
            "id":""
        },
        "offer": {
            "id": ""
        },
        "userId": "",
        "accountId": "",
        "memberId": ""
    }

    //------- oN social Login call this function for data ---------------------------//
    function socialLoginData() {

        function success(data, status, headers, config) {
            $scope.submitButtonName = "Avail this offer now!";
            $scope.submitted = false;
            $scope.errorMessage = false;
            $scope.cookieValue = JSON.parse(Cookies.get("details"));
            $scope.userOffer.title = data.offer.title;
            $scope.userOffer.description = data.offer.description;
            $scope.userOffer.couponCode = data.offer.code;
            $scope.userOffer.memberId = data.memberId;
            $scope.userOffer.userOfferStatus = data.userOfferStatus;
            $scope.cookieValue.currentlyInUse.type = 1;
            Cookies.set("details", JSON.stringify($scope.cookieValue), { expires: 365 });
            TimerService.stopTimer();
            $state.go('mobile.congoNewUser', { 'userOffer': $scope.userOffer, 'userId': data.id });
        }
        function failure(data, status, headers, config) {
            $scope.submitButtonName = "Avail this offer now!";
            $scope.errorMessage = true;
            if (status == 409) {
                $scope.errorResponse = data;
                $scope.errorAccepting = true;
            }
            else
                $scope.errorResponse = "Something went wrong. Please try again."
            $scope.submitted = false;
        }
        var url = "/users/registrationFromBeacon";
        API_SERVICE.postData($scope, $http, $scope.userdetail, url, success, failure);
    }

     //--------------------logout from gigya socail login -------------------------------------//
    $scope.logoutFromGS = function () {
        gigya.socialize.logout(); // logout from Gigya platform
    }
    //----------------------------------------- Gigya socail login------------------------------------------------------------//

    var onLoginHandler = function (eventObj) {
        // verify the signature ...
        verifyTheSignature(eventObj.UID, eventObj.timestamp, eventObj.signature);
        // Check whether the user is new by searching if eventObj.UID exists in your database
        var newUser = true; // lets assume the user is new

        if (newUser) {
            // 1. Register user
            // 2. Store new user in DB
            // 3. link site account to social network identity
            // 3.1 first construct the linkAccounts parameters
            var dateStr = Math.round(new Date().getTime() / 1000.0); // Current time in Unix format
            //(i.e. the number of seconds since Jan. 1st 1970)

            var siteUID = 'uTtCGqDTEtcZMGL08w'; // siteUID should be taken from the new user record
            // you have stored in your DB in the previous step
            var yourSig = createSignature(siteUID, dateStr);
            var params = {
                siteUID: siteUID,
                timestamp: dateStr,
                cid: '',
                signature: yourSig
            };

            //   3.1 call linkAccounts method:
            gigya.socialize.notifyRegistration(params);
        }
        // here will be code for api calling
        
        if (eventObj.hasOwnProperty("provider"))
            $scope.userdetail.social_service = eventObj.provider;
        if (eventObj.hasOwnProperty("UID"))
            $scope.userdetail.unique_id = eventObj.UID;
        if(eventObj.user.hasOwnProperty("email"))
        {
            $scope.userdetail.email_id = eventObj.user.email;
        }
        socialLoginData();
        //document.getElementById('status').style.color = "green";
        //document.getElementById('status').innerHTML = "Status: You are now signed in";
    }

    var onLogoutHandler = function(eventObj) {
        document.getElementById('status').style.color = "red";
        document.getElementById('status').innerHTML = "Status: You are now signed out";
    }

    var onLoadFunction = function () {
        // register for login event
        
            gigya.socialize.addEventHandlers({
                    context: { str: 'congrats on your' }
                    , onLogin: onLoginHandler
                    , onLogout: onLogoutHandler
                    });
    }

    var createSignature = function(UID, timestamp) {
        encodedUID = encodeURIComponent(UID); // encode the UID parameter before sending it to the server.
        // On server side use decodeURIComponent() function to decode an encoded UID
        return '';
    }

    // Note: the actual signature calculation implementation should be on server side
    function verifyTheSignature(UID, timestamp, signature) {
        encodedUID = encodeURIComponent(UID); // encode the UID parameter before sending it to the server.
        // On server side use decodeURIComponent() function to decode an encoded UID
    }

    function initializeGigya() {
        var gigyaParams = {
            version: 2,
            showTermsLink: false, // 'terms' link is hidden
            //headerText: "Please Login using one of the following providers:", // adding header text
            height: 50, // changing default add-on size
            width: '100%',  // changing default add-on size
            cid: '',
            extraFields: 'phones',
            deviceType: 'mobile',
            lastLoginIndication: 'border',
            enabledProviders: $scope.selectedSocialnetworks,
            containerID: 'socialLogin', // The add-on will embed itself inside the "loginDiv" DIV (will not be a popup)
            // Changes to the default design of the add-on's design 
            //     Background color is changed to purple, text color to gray and button size is set to 30 pixels:   
            //UIConfig: '<config><body><texts color="#DFDFDF"></texts><controls><snbuttons buttonsize="30"></snbuttons></controls><background background-color="#51286D"></background></body></config>',
            // Change the buttons design style to the 'fullLogo' style:
            onLoad: onLoadFunction,
            pagingButtonStyle: 'arrows'
            // After successful login - the user will be redirected to "https://www.MySite.com/welcome.html" :  
        };
        $scope.socialUI = gigya.socialize.showLoginUI(gigyaParams);
       
    }
    
   


    if($stateParams.userOffer.offerId == undefined){
        $state.go("mobile.landing");
        return;
    }

    $scope.accountImg = Cookies.get("accountImg");

    $scope.country = { selector: "" };

    $scope.userdetail = {
        email_id: "",
        phone: "",
        country:{
            id: ""
        },
        registered_from: 1,
        beacon:{
            id: $stateParams.userOffer.beaconId
        },
        offer:{
            id: $stateParams.userOffer.offerId
        },
        userId: $scope.cookieValue.currentlyInUse.userId,
        accountId: $scope.cookieValue.currentlyInUse.accountId,
        memberId: $scope.cookieValue.currentlyInUse.memberId
    }

    $scope.submitted = false;
    $scope.errorMessage = false;
    $scope.flagBasePath = IMAGE_URL;
    
    function init() {
        
        $scope.userOffer = { categories: "", offerId: "", imagePath: "", endTime: "" };
        $scope.userOffer.categories = $stateParams.userOffer.categories;
        $scope.userOffer.offerId = $stateParams.userOffer.offerId;
        $scope.userOffer.imagePath = $stateParams.userOffer.imagePath;
        $scope.userOffer.endTime = $stateParams.userOffer.endTime;
        $scope.imgsrc = $scope.userOffer.imagePath;
       
    }
    init();

    //$(window).on("scroll", function () {
    //     $('#background').addClass("imageHeight", $(this).scrollTop() > $(window).height());
    //});

    function getCounrtyCodes() {
        $scope.dataLoader = 0;
        function success(data, status, headers, config) {
            $scope.countryCodes = data;
            $scope.dataLoader = 1;
            console.log(data);
        }
        function failure(data, status, headers, config) {
            $scope.dataLoader = 2;
            console.log(data);
        }
        var url = "/countries/getCountryCodes";
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getCounrtyCodes();

    $scope.getTimeLeft = function (endTime) {
        endTime = new Date(endTime);
        return moment(endTime).fromNow();
    }

    $scope.gotoLanding = function () {
        $('.modal-backdrop').hide();
        $state.go('mobile.landing');
    }

    $scope.submitForm = function (isValid) {
        $scope.submitted = true;
        if (isValid) {
            $scope.submitButtonName = "Please wait ...";
            $scope.userdetail.country.id = $scope.country.selecter.id;
            function success(data, status, headers, config) {
                $scope.submitButtonName = "Avail this offer now!";
                $scope.submitted = false;
                $scope.errorMessage = false;
                $scope.cookieValue = JSON.parse(Cookies.get("details"));
                $scope.userOffer.title = data.offer.title;
                $scope.userOffer.description = data.offer.description;
                $scope.userOffer.couponCode = data.offer.code;
                $scope.userOffer.memberId = data.memberId;
                $scope.userOffer.userOfferStatus = data.userOfferStatus;
                $scope.cookieValue.currentlyInUse.type = 1;
                Cookies.set("details", JSON.stringify($scope.cookieValue), { expires: 365 });
                TimerService.stopTimer();
                $state.go('mobile.congoNewUser', { 'userOffer': $scope.userOffer, 'userId': data.id });
            }
            function failure(data, status, headers, config) {
                $scope.submitButtonName = "Avail this offer now!";
                $scope.errorMessage = true;
                if (status == 409) {
                    $scope.errorResponse = data;
                    $scope.errorAccepting = true;
                }
                else
                    $scope.errorResponse = "Something went wrong. Please try again."
                $scope.submitted = false;
            }
            var url = "/users/registrationFromBeacon";
            API_SERVICE.postData($scope, $http, $scope.userdetail, url, success, failure);
        }
    }


    $scope.currentDate = new Date();

    $(window).bind("beforeunload",function(event) {
        return "Offer will be discarded. Are you sure you want to continue?";
    });

    $scope.$on('$destroy', function() {
        delete window.onbeforeunload;
    });

    $scope.reloadCountries = function()
    {
        getCounrtyCodes();
    }

    //$scope.gotoGetMemberId = function () {
    //    $state.go('mobile.getMemberID', { 'userOffer': $stateParams.userOffer });
    //};

    function refromatSocialData(data) {
        var socialNetworks ="";
        for (var i = 0; i < data.length; i++)
        {
            if(i>0)
                socialNetworks = socialNetworks + ", " + (data[i].name);
            else
                socialNetworks = socialNetworks + "" + (data[i].name);
        }
        return socialNetworks;
    }

    function getSelectedSocialNetworks() {
        $scope.socialDataLoader = 0;
        $scope.socialDataLoaderFlag = false;
        function success(data, status, headers, config) {
            $scope.selectedSocialnetworks = refromatSocialData(data);
            if ($scope.selectedSocialnetworks.length > 0)
            {
                $scope.socialDataLoaderFlag = true;
                $timeout(function () {
                    initializeGigya();
                }, 300);
               
            }
            else
            {
                $scope.socialDataLoaderFlag = false;
            }
            
            $scope.socialDataLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.socialDataLoader = 2;
            $scope.socialDataLoaderFlag = false;
        }
        var url = "/socialNetworkingServices/getSelectedServicesForMobile?accountId=" + $scope.cookieValue.account.id;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getSelectedSocialNetworks();
    }])
