﻿angular.module('bns.mobile.viewBeaconOffer', ['ngCookies'])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
    .state('mobile.viewBeaconOffer', {
        url: "/beaconCurrentOffer/:bid",
        views: {
            "viewBeaconOffer": {
                templateUrl: "App/Mobile/viewBeaconOffer/viewBeaconOffer.html",
                controller: 'viewBeaconOfferCtrl',
            },
        }
    });
}])
.controller('viewBeaconOfferCtrl', ['$state', '$scope', '$rootScope', '$stateParams', '$window', 'API_URL', 'IMAGE_URL', 'API_SERVICE', 'LOADING_MESSAGE', '$http', 'TimerService', '$window', 'ngDialog', function ($state, $scope, $rootScope, $stateParams, $window, API_URL, IMAGE_URL, API_SERVICE, LOADING_MESSAGE, $http, TimerService, $window, ngDialog) {
    $scope.loadingMessage = LOADING_MESSAGE;
    $scope.dataLoader = 1;
    $scope.acceptButtonName = "Accept";
    $('#confirm').modal('hide');
    Cookies.set("beaconId", $rootScope.beaconId, { expires: 120 });
    $scope.details = {
        'memberId': undefined,
        'beaconGuid': $stateParams.bid,
        'accountId' : undefined
    }
    $scope.userDetails = {
        memberId: undefined,
        offerId: 0,
        beaconId: undefined,
        accountId: undefined
    };
    $scope.userOffer = { categories: "", offerId: "", imagePath: "", endTime: "" };

    new Fingerprint2().get(function (result, components) {
      console.log(result); //a hash, representing your device fingerprint
      //alert(components); // an array of FP components
  });

    $scope.typeId = 0;

    $scope.termsAndConditionsList = [];
    $scope.showTerms = true;
    $scope.cookieValue = Cookies.get('isVisited');
    if ($scope.cookieValue == undefined)
    {
        ngDialog.open({
            template: 'cookieWarningDialog',
            className: 'ngdialog-theme-default modal-large',
            scope: $scope,
            preCloseCallback: function  () {
                Cookies.set('isVisited', true, { expires: 365 });
            }
        });
    }

    $scope.closePage = function () {
        $window.close();
    };

    $scope.closeCookieDialog = function () {
        ngDialog.close({
            template: 'cookieWarningDialog',
        });
        Cookies.set('isVisited', true, { expires: 365 });
    }
   
    $scope.init = function () {

        //JSON stored in cookie and again parsed to JSON
        //$scope.jsonObject = [{ memberId: "VED19", bGuid: "XYZ" }, { memberId: "VED99", bGuid: "XYZ" }];
        $scope.cookieValue = Cookies.get("details");
        if($scope.cookieValue == null)
        {
            $scope.jsonObject = {
                "beaconGuid": $stateParams.bid
            };
            Cookies.set("details", JSON.stringify($scope.jsonObject), { expires: 365 });
        }
        $scope.cookieValue = Cookies.get("details");
        $scope.cookieValue = JSON.parse($scope.cookieValue);
        $scope.cookieValue.beaconGuid = $stateParams.bid;
        //Sending cookieValue in API request
        $scope.getBeaconOffer = function () {
            $scope.dataLoader = 0;
            function success(data, status, headers, config) {
                Cookies.set("details", JSON.stringify(data), { expires: 365 });
                var infoData = {
                    "account": data.account,
                    "currentlyInUse": data.currentlyInUse,
                    "details": data.details,
                    "beaconGuid": $stateParams.bid
                };
                Cookies.set("details", JSON.stringify(infoData), { expires: 365 });
                //$scope.cookieValue = JSON.parse(Cookies.get("details"));
                //$scope.accountName = $scope.cookieValue.account.name;
                //$window.document.head.querySelector("[name=author]").content = $scope.accountName;
                console.log(JSON.parse(Cookies.get('details')));
                $scope.cookieData = JSON.parse(Cookies.get('details'));
                $scope.beaconOffer = data.offer;
                $scope.typeId = data.currentlyInUse.type;
                $scope.beaconId = data.currentlyInUse.beaconId;
                $scope.userOffer.accountId = data.currentlyInUse.accountId;
                $scope.userOffer.userId = data.currentlyInUse.userId;
                if ($scope.cookieData.account.id != $scope.cookieData.currentlyInUse.accountId)
                {
                    for(var i=0; i<$scope.cookieData.details.length; i++)
                    {
                        if($scope.cookieData.details[i].accountId == $scope.cookieData.account.id)
                        {
                            $scope.cookieData.currentlyInUse.accountId = $scope.cookieData.account.id;
                            $scope.cookieData.currentlyInUse.memberId = $scope.cookieData.details[i].memberId;
                            break;
                        }
                    }
                }

                TimerService.initializeTimer(data.offer.duration);
                $window.document.title = data.offer.title;
                var link = document.querySelector("link[rel*='icon']") || document.createElement('link');
                link.type = 'image/x-icon';
                link.rel = 'shortcut icon';
                link.href = IMAGE_URL + data.account.picUrl;
                document.getElementsByTagName('head')[0].appendChild(link);
                $scope.accountImg = IMAGE_URL + data.account.picUrl;   // data.account.picUrl  add account image here.
                Cookies.set("accountImg", $scope.accountImg, { expires: 365 });

                $scope.termsAndConditionsList = data.offer.termsAndConditions;
                if (data.offer.termsAndConditions != undefined)
                {
                    if ($scope.termsAndConditionsList.length == 0 || $scope.termsAndConditionsList.length == undefined) {
                        $scope.showTerms = false;
                    } else {
                        $scope.showTerms = true;
                    }
                } else {
                    $scope.showTerms = false;
                }

                if (data.offer == undefined) {
                    $scope.dataLoader = 2;
                } else {
                    $scope.dataLoader = 1;
                }

                if (data.offer.hasOwnProperty('imagePath') && (data.offer.imagePath != "No Image")) {
                    $scope.imgsrc = IMAGE_URL + data.offer.imagePath;
                }
                else {
                    if (data.offer.categories[0].name == "Cash") {
                        $scope.imgsrc = IMAGE_URL + "/Resources/offerImages/Cash.jpg";
                    } else if (data.offer.categories[0].name == "Points") {
                        $scope.imgsrc = IMAGE_URL + "/Resources/offerImages/Points.jpg";
                    } else if (data.offer.categories[0].name == "Promotion") {
                        $scope.imgsrc = IMAGE_URL + "/Resources/offerImages/Promotions.jpg";
                    } else {
                        $scope.imgsrc = IMAGE_URL + '/Resources/offerImages/defaultNoCategory.jpg';
                    }
                }
                console.log(data);
            }
            function failure(data, status, headers, config) {
                alert("FAILED");
                $scope.dataLoader = 3;
            }
            var url = "/Offers/viewBeaconOffer";
            var language = $window.navigator.language || $window.navigator.userLanguage;
            var header = { 'Content-Type': 'application/json', 'language': language }
            API_SERVICE.postData($scope, $http, $scope.cookieValue, url, success, failure, header);
        }
        $scope.getBeaconOffer();
    }
    $scope.init();

    //$scope.getUserOffer = function () {
    //    $scope.dataLoader = 0;
    //    function success(data, status, headers, config) {
    //        console.log(data);
    //        $.cookie("memberId", data.memberId, { expires: 365 });
    //        $.cookie("beaconId", data.beaconId, { expires: 365 });
    //        $.cookie("userType", data.typeId, { expires: 365 });
    //        $scope.beaconOffer = data.offer;
    //        $scope.typeId = data.typeId;
    //        $scope.beaconId = data.beaconId;
    //        $rootScope.beaconId = data.beaconId;
    //        $scope.userOffer.accountId = data.accountId;
    //        $scope.userOffer.userId = data.userId;
    //        TimerService.initializeTimer(data.offer.duration);
    //        $window.document.title = data.offer.title;
    //        var link = document.querySelector("link[rel*='icon']") || document.createElement('link');
    //        link.type = 'image/x-icon';
    //        link.rel = 'shortcut icon';
    //        link.href = IMAGE_URL + data.account.picUrl;
    //        document.getElementsByTagName('head')[0].appendChild(link);
    //        $scope.accountImg = IMAGE_URL + data.account.picUrl;
    //        $.cookie("accountImg", $scope.accountImg, { expires: 365 });
    //        if (data.offer.hasOwnProperty('imagePath') && (data.offer.imagePath != "No Image")) {
    //            $scope.imgsrc = IMAGE_URL + data.offer.imagePath;
    //        }
    //        else {
    //            if (data.offer.categories[0].name == "Cash") {
    //                $scope.imgsrc = IMAGE_URL + "/Resources/offerImages/Cash.jpg";
    //            } else if (data.offer.categories[0].name == "Points") {
    //                $scope.imgsrc = IMAGE_URL + "/Resources/offerImages/Points.jpg";
    //            } else if (data.offer.categories[0].name == "Promotions") {
    //                $scope.imgsrc = IMAGE_URL + "/Resources/offerImages/Promotions.jpg";
    //            }
    //        }
    //        $scope.dataLoader = 1;
    //    }
    //    function failure(data, status, headers, config) {
    //        alert(data);
    //        $scope.dataLoader = 2;
    //    }
    //    var url = "/Offers/getUserOfferForBeacon";
    //    API_SERVICE.postData($scope, $http, $scope.details, url, success, failure);
    //}
    //$scope.getUserOffer();

    function acceptOffer() {
        $scope.acceptButtonName == "Please wait ...";
        $scope.userDetails = {
            memberId: $scope.cookieData.currentlyInUse.memberId,
            offerId: $scope.userOffer.offerId,
            beaconId: $scope.userOffer.beaconId,
            accountId: $scope.userOffer.accountId
        };

        function success(data, status, headers, config) {
            $scope.offer = data;
            //title: "", description: "", couponCode:"", memberId:""
            $scope.userOffer.title = data.offer.title;
            $scope.userOffer.description = data.offer.description;
            $scope.userOffer.couponCode = data.offer.code;
            $scope.userOffer.memberId = data.memberId;
            $scope.userOffer.userOfferStatus = data.userOfferStatus;
            $scope.cookieValue.currentlyInUse.memberId = $scope.userDetails.memberId;
            $scope.cookieValue.currentlyInUse.type = 1;
            Cookies.set("details", JSON.stringify($scope.cookieValue), { expires: 365 });
            TimerService.stopTimer();
            $scope.acceptButtonName = "Accept";
            $state.go('mobile.congoNewUser', { 'userOffer': $scope.userOffer });
        }
        function failure(data, status, headers, config) {
            $scope.acceptButtonName = "Accept";
            if (status == 403) {
                ngDialog.open({
                    template: 'alreadyAvialed',
                    className: 'ngdialog-theme-default modal-large beacon-dialog draggable',
                    scope: $scope

                });
            }
            else if (status == 409) {
                /********************  handle here wrong memberID for novomatic *******************************/
                $scope.wrongMemberId = true;
            }
        }
        var url = "/users/member/acceptOffer";
        API_SERVICE.postData($scope, $http, $scope.userDetails, url, success, failure);
    }

    $scope.gotoNextPage = function () {
        $scope.userOffer.categories = $scope.beaconOffer.categories;
        $scope.userOffer.offerId = $scope.beaconOffer.id;
        $scope.userOffer.imagePath = $scope.imgsrc;
        $scope.userOffer.endTime = $scope.beaconOffer.endTime;
        $scope.userOffer.beaconId = $scope.beaconId;

        var commonCurrencySymbol = "$";
        if ($scope.beaconOffer.countries.length > 0)
            commonCurrencySymbol = $scope.beaconOffer.countries[0].currency;
        for(var i in $scope.userOffer.categories){
            if($scope.userOffer.categories[i].name == 'Cash')
                $scope.userOffer.categories[i]["currencySymbol"] = commonCurrencySymbol;
        }
        if ($scope.typeId == 2)
        {
            $state.go('mobile.getUserDetail', { 'userOffer': $scope.userOffer });
        } else 
        {
            /**********************************for general use *****************************/
            $state.go('mobile.getMemberID', { 'userOffer': $scope.userOffer });

            /**********************************for NOVOMATIC *****************************/
           //acceptOffer();
        }
    }
    
    $scope.gotoLanding = function()
    {
        $('.modal-backdrop').hide();
        $state.go('mobile.landing');
    }

    $scope.getTimeLeft = function (endTime) {
        endTime = new Date(endTime);
        return moment(endTime).fromNow();
    }
    
    //$(window).bind("beforeunload",function(event) {
    //    return "Offer will be discarded. Are you sure you want to continue?";
    //});
    //$('#confirm').modal('show');
    //$scope.$on('$destroy', function() {
    //    delete window.onbeforeunload;
    //});

    $scope.reloadBeconOffer = function()
    {
        $scope.getUserOffer();
    }

    $scope.viewTermsAndConditions = function () {
       
    }

}])
