angular.module('bns.mobile.congoUser', [])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
    .state('mobile.congoUser', {
        url: "/beacon/congo-user",
        views: {
            "congoUser": {
                templateUrl: "App/Mobile/congoUser/congoUser.html",
                controller: 'congoUserCtrl'
            },
        },
    });
}])
.controller('congoUserCtrl', ['$state', '$scope', '$rootScope', function ($state, $scope, $rootScope) {

    $scope.accountImg = Cookies.get("accountImg");

    $scope.currentDate = new Date();

    }])
