angular.module('bns.mobile.congoNewUser', [])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
    .state('mobile.congoNewUser', {
        url: "/beacon/congo-new-user",
        views: {
            "congoNewUser": {
                templateUrl: "App/Mobile/congoNewUser/congoNewUser.html",
                controller: 'congoNewUserCtrl'
            },
        },
        params: {
            userOffer: { title: "", description: "", couponCode: "", memberId: "", userOfferStatus:"", categories: "", offerId: "", imagePath: "", endTime: "" },
            userId:""
        }
    });
}])
.controller('congoNewUserCtrl', ['$http', '$state', '$scope', '$rootScope', '$stateParams', 'API_URL', 'API_SERVICE', 'TimerService', 'ngDialog', function ($http, $state, $scope, $rootScope, $stateParams, API_URL, API_SERVICE, TimerService, ngDialog) {
    //var userOffer= $state.params.userOffer;
    //$scope.userOffer = $state.params.userOffer;
    $scope.userPushNotificationData =
    {
        "id": "",
        "userDevice": {
            "userDeviceType": "",
            "push_id": ""
        }
    };

    var currentLocation;
    var geoOptions = {
        enableHighAccuracy: true
    };

    function init() {
        TimerService.stopTimer();
       
        $scope.accountImg = Cookies.get("accountImg");
        $scope.userOffer = $state.params.userOffer;8 
        $scope.categories = $state.params.userOffer.categories;
        $scope.countries = $stateParams.userOffer.countries
        
        $scope.imgsrc = $state.params.userOffer.imagePath;
        
        //console.log("User offers ----- " + $stateParams.categories[0].value);
        $scope.currentDate = new Date();
    }
    init();

    function getBrowserId() {
        var
            aKeys = ["MSIE", "Firefox", "Safari", "Chrome", "Opera"],
            sUsrAg = navigator.userAgent, nIdx = aKeys.length - 1;

        for (nIdx; nIdx > -1 && sUsrAg.indexOf(aKeys[nIdx]) === -1; nIdx--);

        return nIdx
    }


        //*********************** WEB PUSH NOTIFICATIONS START ***********************//

    var config = {
        apiKey: "AIzaSyCYePqaAIoLDvjcCjSifpFbpePGNOzBb_w",
        authDomain: "best-network-systems.firebaseapp.com",
        databaseURL: "https://best-network-systems.firebaseio.com",
        projectId: "best-network-systems",
        storageBucket: "best-network-systems.appspot.com",
        messagingSenderId: "509813089928"
    };

    if (!firebase.apps.length) {
        switch(getBrowserId())
        {
            case 0:
                break;
            case 1:
                firebase.initializeApp(config);
                break;
            case 2:
                break;
            case 3:
                firebase.initializeApp(config);
                break;
            case 4:
                break;
            default:
                break;
        }
    }
    const messaging = firebase.messaging();

    $scope.notNow = function () {
        ngDialog.close({
            template: 'pushNotificationsDialog',
            scope: $scope
        });
    };

    $scope.accept = function () {
        ngDialog.close({
            template: 'pushNotificationsDialog',
            scope: $scope
        });
        $scope.getPermission();
    }

    //*********************** API FOR PUSH NOTIFIACTION TOKEN ********************//
    function saveUserPushId() {

        function success(data, status, headers, config) {
            $scope.userPushNotificationData =
            {
                "id": "",
                "userDevice": {
                    "userDeviceType": "",
                    "push_id": ""
                }
            };
            console.log("Push id added successfuly.");
           
        }
        function failure(data, status, headers, config) {
            $scope.userPushNotificationData =
            {
                "id": "",
                "userDevice": {
                    "userDeviceType": "",
                    "push_id": ""
                }
            };
            console.log("Failed to add push id.");

        }
        var url = "/users/saveUserPushId";
        API_SERVICE.postData($scope, $http, $scope.userPushNotificationData, url, success, failure);

    }
    //*********************** ASK FOR NOTIFICATION PERMISSION START ***********************//
    $scope.getPermission = function () {
        messaging.requestPermission()
        .then(function () {
            console.log("Request Granted");
            return messaging.getToken()
            .then(function (token) {
                debugger
                console.log(token);
                $scope.userPushNotificationData.id = $stateParams.userId;
                $scope.userPushNotificationData.userDevice.push_id = token;
                $scope.userPushNotificationData.userDevice.userDeviceType = 3;
                saveUserPushId();
            });
        })
        .catch(function (err) {
            console.log("Request Not Granted :", err);
        })
        //*********************** ASK FOR NOTIFICATION PERMISSION END ***********************//
    };

    //*********************** CHECK IF PUSH NOTIFICATION IS GRANTED START ***********************//
    if (Notification.permission != 'granted' && Notification.permission != 'denied') {
        

        ngDialog.open({
            template: 'pushNotificationsDialog',
            className: 'ngdialog-theme-default modal-large',
            scope: $scope
        });
    }
    //*********************** CHECK IF PUSH NOTIFICATION IS GRANTED END ***********************//

    messaging.onMessage(function (payload) {
        console.log('On message', payload);
    });

    //*********************** WEB PUSH NOTIFICATIONS END ***********************//
    /******************************GEOLOCATION STARTS**************************************/
    function getUserGeoLocation()
    {
        var geoSuccess = function (position) {
            currentLocation = position;
            console.log("The user location is => ", position);
        };
        var geoError = function (error) {
            switch (error.code) {
                /* here we need to handle on failure to access geolocation */
                case error.PERMISSION_DENIED:
                    console.log("User denied the request for Geolocation."); break;
                case error.POSITION_UNAVAILABLE:
                    console.log("Location information is unavailable."); break;
                case error.TIMEOUT:
                    console.log("The request to get user location timed out."); break;
                case error.UNKNOWN_ERROR:
                    console.log("An unknown error occurred."); break;
            }
        }
        navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
    }
    /******************************GEOLOCATION ENDS**************************************/
}])
