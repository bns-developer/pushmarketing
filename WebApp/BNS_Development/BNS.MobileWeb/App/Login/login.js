angular.module('bns.login', [])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
    .state('login', {
        url: "/login",
        views: {
            "login": {
                templateUrl: "App/Login/login.html",
                controller: 'loginCtrl'
            },
        },
    });
}])
.controller('loginCtrl', ['$state', '$window', '$scope', '$http', 'API_URL', '$rootScope', 'API_SERVICE', 'TOKEN_SERVICE', 'LOADING_MESSAGE', function ($state, $window, $scope, $http, API_URL, $rootScope, API_SERVICE, TOKEN_SERVICE, LOADING_MESSAGE) {

    /*Go to Main dashboard screen after successful Login*/
    if (TOKEN_SERVICE.RestoreState()['access_token'] != null && TOKEN_SERVICE.RestoreState()['access_token'] != '' && TOKEN_SERVICE.RestoreState()['access_token'] != undefined) {
        $state.go('master.dashboard');
    }
    $scope.submitted = false;
    $scope.userInvalid = false;
    $scope.faliureAlert = false;
    $scope.loginButtonName = "Login";
    

    //$scope.User = {
    //    "grant_type": "password"
    //};


    //$scope.errorMessage = "";
    /*Login function*/
    $scope.forgotPassword = function () {
        alert("This module is under developement.");
    }

    $scope.login = function (isvalid) {
        $scope.submitted = true;
        if (isvalid) {
            $scope.loginButtonName = "Logging in ...";
            $scope.loading = true;
            function success(data, status, headers, config) {
                TOKEN_SERVICE.SaveState(data);
                $scope.loginButtonName = "Login";
                $scope.faliureAlert = false;
                $scope.userInvalid = false;
                $window.location.reload();
                $state.go("master.dashboard");
            }
            function failure(data, status, headers, config) {
                $scope.loginButtonName = "Login";
    
                $scope.faliureAlert = true;
                if (status == 400)
                    $scope.errorMessage = data.error_description;
                else
                    $scope.errorMessage = "Invalid Username & Password";
                $scope.userInvalid = true;
                $scope.loading = false;
            }
            var url = "/login";
            var data = "grant_type=password&username=" + $scope.User.username + "&password=" + $scope.User.password;
            API_SERVICE.postLoginData($scope, $http, data, url, success, failure, { "Content-Type": "application/x-www-form-urlencoded" });
        }
    }
    }])
