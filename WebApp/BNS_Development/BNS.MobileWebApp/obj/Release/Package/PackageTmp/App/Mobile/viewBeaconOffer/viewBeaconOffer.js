﻿angular.module('bns.mobile.viewBeaconOffer', [])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
    .state('mobile.viewBeaconOffer', {
        url: "/beaconCurrentOffer?beaconId",
        views: {
            "viewBeaconOffer": {
                templateUrl: "App/Mobile/viewBeaconOffer/viewBeaconOffer.html",
                controller: 'viewBeaconOfferCtrl',
            },
        }
    });
}])
.controller('viewBeaconOfferCtrl', ['$state', '$scope', '$rootScope', '$stateParams', 'API_URL', 'API_SERVICE', '$http', function ($state, $scope, $rootScope, $stateParams, API_URL, API_SERVICE, $http) {
    $rootScope.beaconId = $stateParams.beaconId;
    function getBeaconOffer() {
        function success(data, status, headers, config) {
            $scope.beaconOffer = data;
            console.log(data);
            if (data.categories[0].name == "Cash") {
                $scope.imgsrc = "http://localhost:5040/Resources/offerImages/Cash.jpg";
            } else if (data.categories[0].name == "Points") {
                $scope.imgsrc = "http://localhost:5040/Resources/offerImages/Points.jpg";
            } else if (data.categories[0].name == "Promotions") {
                $scope.imgsrc = "http://localhost:5040/Resources/offerImages/Promotions.jpg";
            }
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
        }
        var url = "/Offers/getCurrentActiveOfferForBeacon?beacon_id=" + $rootScope.beaconId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getBeaconOffer();

    $scope.gotoUserDetails = function () {
        $scope.userOffer = { value: "", category: "",offerId:""};
        $scope.userOffer.value = $scope.beaconOffer.categories[0].value;
        $scope.userOffer.category = $scope.beaconOffer.categories[0].name;
        $scope.userOffer.offerId = $scope.beaconOffer.id;
        $state.go('mobile.getUserDetail', { 'userOffer': $scope.userOffer });
    }
    
    $scope.gotoLanding = function()
    {
        $state.go('mobile.landing')
    }

}])
