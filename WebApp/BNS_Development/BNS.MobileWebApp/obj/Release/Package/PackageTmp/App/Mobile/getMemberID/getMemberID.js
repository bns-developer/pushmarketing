angular.module('bns.mobile.getMemberID', [])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
    .state('mobile.getMemberID', {
        url: "/beacon/get-member-details",
        views: {
            "getMemberID": {
                templateUrl: "App/Mobile/getMemberID/getMemberID.html",
                controller: 'getMemberIDCtrl'
            },
        },
    });
}])
.controller('getMemberIDCtrl', ['$state', '$scope', '$rootScope', function ($state, $scope, $rootScope) {

    $scope.currentDate = new Date();

    }])
