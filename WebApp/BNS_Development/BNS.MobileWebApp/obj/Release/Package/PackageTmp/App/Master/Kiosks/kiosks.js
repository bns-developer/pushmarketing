angular.module('bns.master.kiosks', [])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.kiosks', {
        url: '/kiosks',
        views: {
            "kiosks": {
                templateUrl: 'App/Master/Kiosks/kiosks.html',
                controller: 'kiosksController'
            }
        }
    });
}])
.controller("kiosksController", ['$scope', '$http', 'API_URL', 'API_SERVICE', '$state',
    function ($scope, $http, API_URL, API_SERVICE, $state) {
        var table_advanced = function () {

            var handleDatatable = function () {
                var spinner = $(".spinner").spinner();
                var table = $('#table_id').dataTable({
                    "bFilter": true,
                    "bInfo": false,
                    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                    "order": [[1, "asc"]],
                    "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0]
                    },
                    {
                        'bSortable': false,
                        'aTargets': [8]
                    }]
                });

                var tableTools = new $.fn.dataTable.TableTools(table, {
                    "sSwfPath": "libs/assets/vendors/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                    "buttons": [
                        "copy",
                        "csv",
                        "xls",
                        "pdf",
                        { "type": "print", "buttonText": "Print me!" }
                    ]
                });
                $(".DTTT_container").css("float", "right");
            };
            return {
                init: function () {
                    handleDatatable();
                }
            };
        }(jQuery);

        setTimeout(function () {
            table_advanced.init();
        }, 2000);

        function getAllKiosksList() {
            $scope.loading = true;
            $scope.dataLoader = 0;
            function success(data, status, headers, config) {
                $scope.kiosks = data;
                console.log($scope.stores);
                $scope.loading = false;
                $scope.dataLoader = 1;
            }
            function failure(data, status, headers, config) {
                $scope.loading = false;
                $scope.dataLoader = 2;
            }
            var url = "/kiosks/all";
            API_SERVICE.getData($scope, $http, url, success, failure);
        }
        getAllKiosksList();

        $scope.goLuckyCharacters = function (kioskId) {
            $state.go("master.luckyCharacters", { "kioskId": kioskId });
        }

        $scope.reloadKiosksList = function () {
            getAllKiosksList();
        }

    }]);