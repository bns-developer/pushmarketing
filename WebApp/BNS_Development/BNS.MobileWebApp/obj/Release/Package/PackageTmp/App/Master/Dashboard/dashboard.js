angular.module('bns.master.dashboard', ['oc.lazyLoad'])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.dashboard', {
        url: '/dashboard',
        views: {
            "dashboard": {
                templateUrl: 'App/Master/Dashboard/dashboard.html',
                controller: 'dashboardController'
            }
        }        
});
}])
.controller("dashboardController", ['$scope','$http', 'API_URL','API_SERVICE',function ($scope,$http, API_URL,API_SERVICE) {

}]);