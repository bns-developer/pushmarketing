angular.module('bns.mobile.viewOffer', [])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
    .state('mobile.viewOffer', {
        url: "/beacon/view-offer",
        views: {
            "viewOffer": {
                templateUrl: "App/Mobile/viewOffer/viewOffer.html",
                controller: 'viewOfferCtrl',
            },
        },
        params: {
            offer: {offerId:"",category:""}
        }
    });
}])
.controller('viewOfferCtrl', ['$state', '$scope', '$rootScope', '$stateParams', 'API_URL', 'API_SERVICE', '$http', function ($state, $scope, $rootScope, $stateParams, API_URL, API_SERVICE, $http) {

    function imgInit() {
        if ($stateParams.offer.category == "Cash") {
            $scope.imgsrc = "http://localhost:5040/Resources/offerImages/Cash.jpg";
        } else if ($stateParams.offer.category == "Points") {
            $scope.imgsrc = ("http://localhost:5040/Resources/offerImages/Points.jpg");
        } else if ($stateParams.offer.category == "Promotions") {
            $scope.imgsrc = ("http://localhost:5040/Resources/offerImages/Promotions.jpg");
        }
    }
    imgInit();

    function getOffer() {
        $scope.loading = true;
        function success(data, status, headers, config) {
            $scope.offerToDisplay = data;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
        }
        var url = "/Offers/details?offerId=" + $stateParams.offer.offerId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getOffer();

    $scope.gotoUserDetails = function () {
        $scope.userOffer = { value: "", category: "",offerId:""};
        $scope.userOffer.value = $scope.offerToDisplay.categories[0].value;
        $scope.userOffer.category = $stateParams.offer.category;
        $scope.userOffer.offerId = $stateParams.offer.offerId;
        $state.go('mobile.getUserDetail', { 'userOffer': $scope.userOffer });
    }

    $scope.currentDate = new Date();

    }])
