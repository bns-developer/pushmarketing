angular.module('bns.master', [
    'bns.master.dashboard',
    'bns.master.stores',
    'bns.master.kiosks',
    'bns.master.luckyCharacters',
    'bns.master.setKioskOffer',
    'bns.master.beacons',
    'bns.master.offers',
    'bns.master.users',
    ])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
    .state('master', {
        url: "",
        abstract: true,
        views: {
            "master": {
                templateUrl: "App/Master/master.html",
                controller: 'masterCtrl'
            },
        },
    });
}])
.controller('masterCtrl', ['$state', '$scope', '$rootScope', function ($state, $scope, $rootScope) {

    $scope.currentDate = new Date();

    }])
