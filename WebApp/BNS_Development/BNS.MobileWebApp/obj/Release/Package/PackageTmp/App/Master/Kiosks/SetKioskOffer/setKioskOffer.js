angular.module('bns.master.setKioskOffer', [])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.setKioskOffer', {
        url: '/kiosks/setKioskOffer?kioskId?luckyItemId',
        views: {
            "setKioskOffer": {
                templateUrl: 'App/Master/Kiosks/SetKioskOffer/setKioskOffer.html',
                controller: 'setKioskOfferController'
            }
        }
    });
}])
.controller("setKioskOfferController", ['$scope', '$http', 'API_URL', 'ngDialog', '$stateParams', 'API_SERVICE', 'MEDIA_URL', function ($scope, $http, API_URL, ngDialog, $stateParams, API_SERVICE, MEDIA_URL) {

    $scope.getLuckyItemPic = function (icon) {
        $scope.imagePath = MEDIA_URL + icon;
        return $scope.imagePath;
    }

    $scope.successAlert = false;
    $scope.faliureAlert = false;

    var table_advanced = function () {
        var handleDatatable = function(){
            var spinner = $( ".spinner" ).spinner();
            var table = $('.table-advanced').dataTable( {
                //"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "bPaginate": false,
                "order": [[ 1, "asc" ]],
                "aoColumnDefs" : [ {
                    'bSortable' : false,
                    'aTargets' : [ 0 ]
                } ]
            } );

            var tableTools = new $.fn.dataTable.TableTools( table, {
                "sSwfPath": "libs/assets/vendors/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                "buttons": [
                    "copy",
                    "csv",
                    "xls",
                    "pdf",
                    { "type": "print", "buttonText": "Print me!" }
                ]
            } );
            $(".DTTT_container").css("float","right");
        };
        return{
            init: function () {
                handleDatatable();
            }
        };
    }(jQuery);
    
    setTimeout(function(){
        table_advanced.init();
    },5000);

    $scope.setOffer = function(){
        ngDialog.open({
                template: 'setOfferDialog',
                className: 'ngdialog-theme-default modal-large',
                scope: $scope,
                preCloseCallback: function () { $scope.clearModal() }
            });
    }

    $scope.setDefaultOffer = function () {
        ngDialog.open({
            template: 'setDefaultOfferDialog',
            className: 'ngdialog-theme-default modal-large',
            scope: $scope,
            preCloseCallback: function () { $scope.clearModal() }
        });
    }

    $scope.closeModal = function () {
        ngDialog.close({
            template: 'setOfferDialog',
            scope: $scope,
        });
        $scope.clearModal();
    }

    $scope.clearModal = function () {
        $scope.successAlert = false;
        $scope.faliureAlert = false;
    }

    function getDefaultOffer() {
        $scope.loading = true;
        $scope.defaultOfferLoader = 0;
        function success(data, status, headers, config) {
            $scope.offer = data;
            console.log($scope.offer);
            //$scope.offer.date = $scope.offer[0].createdOn.split(' ')[0];
            $scope.loading = false;
            $scope.defaultOfferLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.defaultOfferLoader = 2;
        }
        var url = "/Offers/getLuckyItemDefaultOffer?kiosk_id=" + $stateParams.kioskId + "&&lucky_item_id=" + $stateParams.luckyItemId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getDefaultOffer();
    
    function getLuckyItemInfo() {
        $scope.loading = true;
        $scope.dataLoading = 0;
        function success(data, status, headers, config) {
            $scope.luckyItemInfo = data;
            console.log($scope.offer);
            $scope.loading = false;
            $scope.dataLoading = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.dataLoading = 2;
        }
        var url = "/luckyItems/byluckyItemId?luckyItemId=" + $stateParams.luckyItemId + "&&kioskId=" + $stateParams.kioskId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getLuckyItemInfo();

    function getInactiveOffers() {
        $scope.loading = true;
        $scope.inactiveOffersLoader = 0;
        function success(data, status, headers, config) {
            $scope.inactiveOffers = data;
            console.log($scope.inactiveOffers);
            $scope.loading = false;
            $scope.inactiveOffersLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.inactiveOffersLoader = 2;
        }
        var url = "/Offers/getLuckyItemInactiveOffers?kiosk_id=" + $stateParams.kioskId + "&&lucky_item_id=" + $stateParams.luckyItemId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getInactiveOffers();


    

    function getNonDefaultOffers() {
        $scope.loading = true;
        $scope.nonDefaultLoader = 0;
        function success(data, status, headers, config) {
            $scope.nonDefaultOffers = data;
            console.log($scope.inactiveOffers);
            $scope.loading = false;
            $scope.nonDefaultLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.nonDefaultLoader = 2;
        }
        var url = "/Offers/getLuckyItemNonDefaultOffers?kiosk_id=" + $stateParams.kioskId + "&&lucky_item_id=" + $stateParams.luckyItemId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getNonDefaultOffers();


    function getActiveOffers() {
        $scope.loading = true;
        $scope.activeOfferLoader = 0;
        function success(data, status, headers, config) {
            $scope.activeOffers = data;
            console.log($scope.activeOffers);
            $scope.loading = false;
            $scope.activeOfferLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.activeOfferLoader = 2;
        }
        var url = "/Offers/getLuckyItemActiveOffers?kiosk_id=" + $stateParams.kioskId + "&&lucky_item_id=" + $stateParams.luckyItemId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getActiveOffers();

    function getOffersHistory() {
        $scope.loading = true;
        $scope.offerHistoryLoader = 0;
        function success(data, status, headers, config) {
            $scope.offersHistory = data;
            console.log($scope.activeOffers);
            $scope.loading = false;
            $scope.offerHistoryLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.offerHistoryLoader = 2;
        }
        var url = "/Offers/getLuckyItemOffersHistory?kiosk_id=" + $stateParams.kioskId + "&&lucky_item_id=" + $stateParams.luckyItemId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getOffersHistory();

    $scope.setDefaultOfferActive = function (offerId) {
        function success(data, status, headers, config) {
            getDefaultOffer();
            getNonDefaultOffers();
            $scope.successAlert = true;
            $scope.faliureAlert = false;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.successAlert = false;
            $scope.faliureAlert = true;
        }
        var url = "/Offers/changeLuckyItemDefaultOffer?kiosk_id=" + $stateParams.kioskId + "&&lucky_item_id=" + $stateParams.luckyItemId + "&&offer_id=" + offerId;
        API_SERVICE.postData($scope, $http, "", url, success, failure);
    }

    //-----------------reload data---------------------//
    $scope.reloadData = function () {
        getLuckyItemInfo();
    }

    $scope.reloadDefaultOffersList = function () {
        getDefaultOffer();
    }

    $scope.reloadActiveOffersList = function () {
        getActiveOffers();
    }

    $scope.reloadOffersHistory = function () {
        getOffersHistory();
    }

    $scope.reloadNonDefaultOffersList = function () {
        getNonDefaultOffers();
    }

    $scope.reloadInactiveOffersList = function () {
        getInactiveOffers();
    }

    $scope.setActive = function (offerId) {
        function success(data, status, headers, config) {
            getActiveOffers();
            getInactiveOffers();
            $scope.successAlert = true;
            $scope.faliureAlert = false;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.successAlert = false;
            $scope.faliureAlert = true;
        }
        var url = "/Offers/setOfferForLuckyItem?kiosk_id=" + $stateParams.kioskId + "&&lucky_item_id=" + $stateParams.luckyItemId + "&&offer_id="+offerId;
        API_SERVICE.postData($scope, $http, "", url, success, failure);
    }

}])