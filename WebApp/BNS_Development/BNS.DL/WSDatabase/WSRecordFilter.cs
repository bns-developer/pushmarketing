﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace BNS.DL.WSDatabase
{
    public class WSSort
    {
        public string order { get; set; }
        public string field { get; set; }
    }

    public class WSSearchFilter
    {        
        public List<string> Fields { get; set; }
        public string Key { get; set; }
        public string Operator { get; set; }
    }
    public class WSAggregate
    {       
        public string Field { get; set; }
        public string Type { get; set; }
    }
    public class WSRecordGroup
    {
        public String field { get; set; }

        public JValue value { get; set; }
    }
    public class WSFilter
    {
        public string Condition { get; set; }
        public string Field { get; set; }
        public bool IgnoreCase { get; set; }
        public bool IsComplex { get; set; }
        public string Operator { get; set; }
        public List<WSFilter> predicates { get; set; }
        public object value { get; set; }
    }

    public class WSRecordsFilter
    {

        public List<WSAggregate> Aggregates { get; set; }
        public List<string> Expand { get; set; }
        public List<string> Group { get; set; }
      
        public bool RequiresCounts { get; set; }
        public List<WSSearchFilter> Search { get; set; }
        public List<string> Select { get; set; }
       
        public int Skip { get; set; }
        public List<WSSort> Sort { get; set; }
        public string Table { get; set; }
        
        public int Take { get; set; }
        public List<WSFilter> Where { get; set; }
        //public WSRecordsFilter(DataManager datamanager)
        //{
          

        //   // this.filterBy = datamanager.Where;

        //}

        public WSRecordsFilter()
        {
        }

        void setDefault()
        {

            //if (this.sortBy == null)
            //    this.sortBy = Constant.DEFAULT_SORT_FIELD;

            //if (this.sortByOrder == null)
            //    this.sortByOrder = Constant.DEFAULT_SORT_ORDER; ;

            //if (this.numberOfRecords == 0)
            //    this.numberOfRecords = Constant.DEFAULT_NUMBER_OF_RECORDS;

            //if (this.page == 0)
            //    this.page = Constant.DEFAULT_PAGE;

            //if (this.offset == 0)
            //    this.offset = Constant.DEFAULT_OFFEST;

        }

        public Dictionary<string, object> toDictionary()
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            this.setDefault();


            //if (this.sortByOrder.Value)
            //    parameters["sortByOrder"] = "ASC";
            //else
            //    parameters["sortByOrder"] = "DESC";

            //this.sortBy = "ORDER BY " + this.sortBy + " " + this.sortByOrder;

            //parameters["sortBy"] = this.sortBy;

            //parameters["page"] = this.page;



            //if (this.filter != null)
            //    parameters["filter"] = this.filter;

            //parameters["offset"] = this.offset;
            //parameters["numberOfRecords"] = "TOP(" + this.numberOfRecords + ")";
            //parameters["sortBy"] = this.sortBy;
            return parameters;
        }
        public string toQuery()
        {
            this.setDefault();
            String query = "";

            return query;
        }
    }
}