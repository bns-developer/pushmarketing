﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.DAL.DALKiosk;
using BNS.Models.Kiosks;

namespace BNS.BL.BLKiosk
{
    public class KioskBL
    {
        DALKioskManager manager = new DALKioskManager();
        public Int64 save(Kiosk kiosk)
        {
            return manager.saveKiosk(kiosk);
        }

    }
}