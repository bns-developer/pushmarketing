﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.Offers;
using System.Data.SqlClient;
using BNS.DAL.DALOffer;
using WSDatabase;
using BNS.Models.Categories;
using BNS.DAL.DALCategory;
using BNS.Models.LuckyItems;

namespace BNS.DAL.DALKiosk
{
    public class DALKioskDashboard
    {
        public Dictionary<string, object> getKioskDashboardDetails(Int64 kioskId, Int64 accountId)
        {
            Dictionary<string, object> responseObject = new Dictionary<string, object>();

            Int64 registeredUsers = 0;            
            Offer popularOffer = new Offer();
            LuckyItem popularLuckyItem = new LuckyItem();
            List<Dictionary<string, object>> monthlyUserCount = new List<Dictionary<string, object>>();
            List<Category> categoryList = new List<Category>();

            registeredUsers = getRegisteredUsersCount(kioskId);           
            popularOffer = getPopularOffer(kioskId);
            popularLuckyItem = getPopularLuckyItem(kioskId);
            monthlyUserCount = getRegisteredUsersOfLast5Months(kioskId);
            categoryList = getOfferCategoriesVsUsersOfLast5Months(kioskId, accountId);

            responseObject.Add("registeredUsers", registeredUsers);          
            responseObject.Add("popularOffer", popularOffer);
            responseObject.Add("popularLuckyItem", popularLuckyItem);
            responseObject.Add("monthlyUserCount", monthlyUserCount);
            responseObject.Add("categoryList", categoryList);

            return responseObject;
        }

        public Int64 getRegisteredUsersCount(Int64 id)
        {
            Int64 count = 0;
            string query = QueryConstants.KIOSK_REGISTERED_USERS;
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    count = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("REGISTERED_USERS")));
                }
            }
            Query.connection.Close();
            return count;
        }

        public LuckyItem getPopularLuckyItem(Int64 id)
        {
            String query = QueryConstants.POPULAR_LUCKY_ITEM;
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            LuckyItem luckyItem = new LuckyItem();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    luckyItem = new LuckyItem(reader);
                }
            }
            Query.connection.Close();
            return luckyItem;
        }

        public Offer getPopularOffer(Int64 id)
        {
            Offer offer = new Offer();
            string query = QueryConstants.KIOSK_POPULAR_OFFER;
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offer.id = reader.GetInt64(reader.GetOrdinal("OFFER_ID"));
                }
            }
            Query.connection.Close();
            offer = new DALOfferManager().getDetails(offer);
            return offer;
        }

        public List<Dictionary<string, object>> getRegisteredUsersOfLast5Months(Int64 id)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);

            List<Dictionary<string, object>> monthlyUserCount = new List<Dictionary<string, object>>();
            
            Int64 count = 0;
            int month = 0;
            int year = 0;
            DateTime userDate;
            DateTime _START = currentDateTime.AddMonths(-5).AddDays(1 - currentDateTime.Day).AddHours(-currentDateTime.Hour).AddMinutes(-currentDateTime.Minute).AddSeconds(-currentDateTime.Second);
            DateTime _END = currentDateTime.AddDays(-currentDateTime.Day + 1).AddHours(-currentDateTime.Hour).AddMinutes(-currentDateTime.Minute).AddSeconds(-currentDateTime.Second - 1);

            string query = QueryConstants.REGISTERED_USERS_FROM_KIOSK_BY_MONTH;
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = id;
            Query.parameters["_START"] = _START;
            Query.parameters["_END"] = _END;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Dictionary<string, object> obj = new Dictionary<string, object>();
                    count = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("USER_COUNT")));
                    month = Convert.ToInt16(reader.GetValue(reader.GetOrdinal("_MONTH")));
                    year = Convert.ToInt16(reader.GetValue(reader.GetOrdinal("_YEAR")));
                    userDate = new DateTime((int)year, (int)month, 1);
                    obj.Add("userCount", count);
                    obj.Add("userDate", userDate);
                    monthlyUserCount.Add(obj);
                }
            }
            Query.connection.Close();
            return monthlyUserCount;
        }

        public List<Category> getOfferCategoriesVsUsersOfLast5Months(Int64 kioskId, Int64 accountId)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);

            List<Category> categories = new List<Category>();
            categories = new DALCategoryManager().getAll(accountId);
            Int64 count = 0;
            Int64 categoryId;
            int month = 0;
            int year = 0;

            DateTime userDate;
            DateTime _START = currentDateTime.AddMonths(-5).AddDays(1 - currentDateTime.Day).AddHours(-currentDateTime.Hour).AddMinutes(-currentDateTime.Minute).AddSeconds(-currentDateTime.Second);
            DateTime _END = currentDateTime.AddDays(-currentDateTime.Day + 1).AddHours(-currentDateTime.Hour).AddMinutes(-currentDateTime.Minute).AddSeconds(-currentDateTime.Second - 1);

            string query = QueryConstants.USER_COUNT_BY_OFFER_CATEGORIES_FROM_KIOSK;
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = kioskId;
            Query.parameters["_START"] = _START;
            Query.parameters["_END"] = _END;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Dictionary<string, object> obj = new Dictionary<string, object>();
                    count = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("USER_COUNT")));
                    month = Convert.ToInt16(reader.GetValue(reader.GetOrdinal("_MONTH")));
                    year = Convert.ToInt16(reader.GetValue(reader.GetOrdinal("_YEAR")));
                    categoryId = reader.GetInt64(reader.GetOrdinal("CATEGORY_ID"));
                    userDate = new DateTime((int)year, (int)month, 1);
                    obj.Add("userCount", count);
                    obj.Add("userDate", userDate);
                    foreach (Category category in categories)
                    {
                        if (category.id == categoryId)
                        {
                            category.data.Add(obj);
                        }
                    }
                }
            }
            Query.connection.Close();
            return categories;
        }

        
    }
}