﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.Categories;
using BNS.DL.WSDatabase;
using System.Data.SqlClient;
using BNS.Models.Accounts;
using WSDatabase;

namespace BNS.DAL.DALCategory
{
    public class DALCategoryManager
    {
        public bool setCategoriesForNewAccount(Account account, WSTransaction transaction)
        {
            bool result = false;
            List<Category> categories = new List<Category>();
            Category category1 = new Category("Cash", 1, account, 1);
            Category category2 = new Category("Points", 2, account, 1);
            Category category3 = new Category("Promotion", 2, account, 2);
            categories.Add(category1);
            categories.Add(category2);
            categories.Add(category3);
            foreach (Category category in categories)
            {
                String query = "INSERT INTO Categories(name, account_id, unit, type, created_on, updated_on, created_by, updated_by) OUTPUT Inserted.id  VALUES(@name, @accountId, @unit, @type, @createdOn, @updatedOn, @createdBy, @updatedBy);";
                WSQuery Query = new WSQuery(query, category.toDictionary(), transaction);
                result = WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);
            }
            return result;
        }

        public List<Category> getAll(Int64 accountId)
        {
            WSQuery Query = new WSQuery();
            Query.query = QueryConstants.ALL_CATEGORIES;
            Query.parameters["accountId"] = accountId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);

            List<Category> CategoriesList = new List<Category>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    CategoriesList.Add(new Category(reader));
                }            
            }
            Query.connection.Close();
            return CategoriesList;
        }

        public List<Category> All(Account account)
        {
            WSQuery Query = new WSQuery();
            Query.query = QueryConstants.CATEGORY_FOR_LIST;
            Query.parameters["accountId"] = account.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);

            List<Category> CategoriesList = new List<Category>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    CategoriesList.Add(new Category(reader));
                }
            }
            Query.connection.Close();
            return CategoriesList;
        }

        public bool checkIfNameExists(String categoryName, Int64 account)
        {
            String query = "SELECT CASE WHEN EXISTS (Select * from Categories where name = @categoryName and account_id = @accountId) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["categoryName"] = categoryName;
            Query.parameters["accountId"] = account;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }

        public Int64 save(Category category)
        {
            using (WSTransaction transaction = new WSTransaction("SAVE_CATEGORY"))
            {
                String query = "INSERT INTO Categories(name, account_id, unit, type, created_on, updated_on, created_by, updated_by) OUTPUT Inserted.id  VALUES(@name, @accountId, @unit, @type, @createdOn, @updatedOn, @createdBy, @updatedBy);";
                WSQuery Query = new WSQuery(query, category.toDictionary(), transaction);
                category.id = WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQueryReturnID(Query);
                if (category.id != 0)
                {
                    transaction.commit();
                    return category.id;
                }
                else
                {
                    transaction.rollback();
                    return 0;
                }
            }
        }

        public bool update(Category category)
        {
            using (WSTransaction transaction = new WSTransaction("UPDATE_CATEGORY"))
            {
                String query = "UPDATE Categories SET name = @name, unit = @unit, type = @type, updated_on =  @updatedOn, updated_by = @updatedBy WHERE id = @id";
                //String query = "UPDATE Categories SET " + (category.name != null ? "name = @name" : " ") + ", " + (category.CategoryType != null ? (category.CategoryType == Category.Category_Type.TEXT ? "type = 1" : "type = 2") : " ") + ", " + ;
                WSQuery Query = new WSQuery(query, category.toDictionary(), transaction);
                if (!WSDatabaseManager.sharedDatabaseManagerinstance.executeUpdateQuery(Query))
                {
                    transaction.rollback();
                    return false;
                }
                transaction.commit();
                return true;
            }              
        }

        public bool isCategoryUsedForOffer(Int64 categoryId)
        {          
                WSQuery Query = new WSQuery();
                Query.query = "SELECT * FROM Offers_Categories WHERE category_id = @categoryId";
                Query.parameters["categoryId"] = categoryId;
                SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
                return reader.HasRows;   
        }

        public bool delete (Int64 categoryId)
        {
            using (WSTransaction transaction = new WSTransaction("DELETE_CATEGORY"))
            {
                WSQuery Query = new WSQuery();
                Query.query = "DELETE FROM Categories WHERE id = @categoryId";
                Query.parameters["categoryId"] = categoryId;
                Query.transaction = transaction;
                if (!WSDatabaseManager.sharedDatabaseManagerinstance.executeDeleteQuery(Query))
                {
                    transaction.rollback();
                    return false;
                }
                transaction.commit();
                return true;
            }           
        }

        public Int64 getIdFromName(string categoryName, Account account)
        {
            WSQuery Query = new WSQuery();
            Query.query = "SELECT id AS CATEGORY_ID FROM Categories WHERE Categories.name = '" + categoryName + "' AND Categories.account_id = " + account.id + ";";
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            Category category = new Category();
            if(reader.HasRows)
            {
                while(reader.Read())
                {
                    category = new Category(reader);
                }
            }
            return category.id;
        }

    }
}