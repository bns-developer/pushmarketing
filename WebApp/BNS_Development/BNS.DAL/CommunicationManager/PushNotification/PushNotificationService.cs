﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using BNS.Models.Notifications;
using System.Text;
using BNS.Models.Users;
using BNS.Models.UserDevices;

namespace BNS.DAL.CommunicationManager.PushNotification
{
    public class PushNotificationService
    {
        public static bool SendNotificationFromFirebaseCloud(String deviceId, Notification notification1)
        {                      
            try
            {
                WebRequest tRequest = WebRequest.Create(PushNotificationConfig.FIREBASE_APPLICATION_URL);
                tRequest.Method = "post";
                tRequest.ContentType = PushNotificationConfig.FIREBASE_APPLICATION_ENCODING;
                var data = new
                {
                    to = deviceId,
                    data = new
                    {
                        title = "Best Networks Systems",
                        subtitle = "Notification",
                        body = notification1.message,
                        click_action = "https://www.google.co.in",
                        sound = "Enabled",
                        icon = "https://image.ibb.co/fHXuxb/images.jpg",
                        color = "#53c4bc"
                    }
                };
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", PushNotificationConfig.FIREBASE_APPLICATION_ID));
                tRequest.Headers.Add(string.Format("Sender: id={0}", PushNotificationConfig.FIREBASE_APPLICATION_SENDER_ID));
                tRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                string str = sResponseFromServer;
                                return true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        
    }
}