﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Twilio;
using Twilio.Exceptions;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace BNS.DAL.CommunicationManager.Message
{
    public class MessageService
    {
        public static bool SendSms(String To, String Message)
        {
            try
            {
                TwilioClient.Init(MessageConfig.accountSid, MessageConfig.authToken);

                var message = MessageResource.Create(
                    to: new PhoneNumber(To),
                    from: new PhoneNumber("+13055207026"),
                    body: Message);
                return true;
            }
            catch(ApiException Ex)
            {
                Console.WriteLine(Ex.ToString());
            }
            return false;
        }

    }
}