﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.BNSModels;
using BNS.Models.Users;

namespace BNS.DAL
{
    public abstract class BNSManager
    {
        //public ConversionHelper conversionHelper = new ConversionHelper();
        public abstract List<BNSModel> getAll();
        public abstract bool save(BNSModel model);
        public abstract BNSModel getInfo(Int64 id);
        public abstract bool update(BNSModel model);
        public abstract bool delete(BNSModel model);
        public abstract bool deleteMultiple(List<Int64> list);
    }
}