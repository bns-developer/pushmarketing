﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Mail;
using BNS.Models.Notifications;
using BNS.DAL.DALAccount;
using System.Dynamic;
using System.Web.Configuration;
using System.IO;
using RazorEngine;
using RazorEngine.Templating;
using BNS.Models.Users;
using BNS.Models.Contacts;
using BNS.Models.UserDevices;
using BNS.DAL.CommunicationManager.Email;
using BNS.DAL.CommunicationManager.Message;
using BNS.DAL.CommunicationManager.PushNotification;

namespace BNS.DAL.DALNotification
{
    public class DALNotificationManager
    {
        DALAccountManager accountManager = new DALAccountManager();

        public bool sendNotifications(Notification notification)
        {
            bool result = false;
            if (notification.NotificationType == Notification.Notification_Type.EMAIL)
                result = sendEmailNotification(notification);
            if (notification.NotificationType == Notification.Notification_Type.TEXT)
                result = sendTextNotification(notification);
            if (notification.NotificationType == Notification.Notification_Type.PUSH)
                result = sendPushNotification(notification);
            return result;
        }

        public bool sendEmailNotification(Notification notification)
        {
            IEnumerable<User> Receivers = notification.receivers.Where(receiver => receiver.contacts != null);
                   
            bool enableSSL = false;            
       
            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress(EmailConfig.emailFromSupport, EmailConfig.SupportDisplayName);
                mail.Subject = notification.subject;
                //mail.Body = PopulateBodyForNotificationEmail(notification);
                mail.Body = notification.message;
                mail.IsBodyHtml = true;

                foreach (User receiver in Receivers)
                {
                    foreach(Contact contact in receiver.contacts)
                    {
                        if(contact.ContactType == Contact.Contact_Type.EMAIL)
                        {
                            mail.To.Add(contact.value);
                            using (SmtpClient smtp = new SmtpClient(EmailConfig.smtpAddress, EmailConfig.portNumber))
                            {
                                smtp.Credentials = new NetworkCredential(EmailConfig.emailFromSupport, EmailConfig.supportPassword);
                                smtp.EnableSsl = enableSSL;
                                smtp.Send(mail);
                            }
                            mail.To.Clear();
                        }

                    }                   
                }                                            
            }
            return true;
        }

        private string PopulateBodyForNotificationEmail(Notification notification)
        {
            notification.sender = accountManager.getDetails(notification.sender);
            dynamic model = new ExpandoObject();
            model.picUrl = WebConfigurationManager.AppSettings["IMAGE_PATH"].ToString() + notification.sender.picUrl;
            model.sender = notification.sender.name;

            string body = string.Empty;
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("")))
            {
                body = reader.ReadToEnd();
            }
            body = Engine.Razor.RunCompile(body, new Random().Next(100, 10000).ToString(), null, (object)model); //rendering the template with our model

            return body;
        }

        public bool sendTextNotification(Notification notification)
        {
            IEnumerable<User> Receivers = notification.receivers.Where(receiver => receiver.contacts != null);
            foreach (User receiver in Receivers)
            {
                foreach(Contact contact in receiver.contacts)
                {
                    if(contact.ContactType == Contact.Contact_Type.PHONE)
                    {
                        String To = String.Concat(contact.country.countryCode, contact.value);
                        MessageService.SendSms(To, notification.message);
                    }
                }              
            }
            return true;
        }

        public bool sendPushNotification(Notification notification)
        {
            IEnumerable<User> Receivers = notification.receivers.Where(receiver => receiver.userDevices != null);
            foreach(User receiver in Receivers)
            {
                foreach(UserDevice device in receiver.userDevices)
                {
                    String deviceId = device.push_id;
                    PushNotificationService.SendNotificationFromFirebaseCloud(deviceId, notification);
                }
            }
            return true;
        }

    }
}