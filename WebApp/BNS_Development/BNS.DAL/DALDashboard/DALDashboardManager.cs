﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.Accounts;
using BNS.Models.Beacons;
using BNS.Models.Kiosks;
using BNS.Models.Offers;
using WSDatabase;
using System.Data.SqlClient;
using BNS.DAL.DALBeacon;
using BNS.DAL.DALKiosk;
using BNS.DAL.DALOffer;
using BNS.Models.Categories;
using BNS.DAL.DALCategory;

namespace BNS.DAL.DALDashboard
{
    public class DALDashboardManager
    {
        public Dictionary<string, object> getDashboardDetails(Account account)
        {
            Dictionary<string, object> responseObject = new Dictionary<string, object>();
            

            Int64 totalMembers = 0;
            Int64 activeMembers = 0;
            Int64 anonymousUsers = 0;
            Int64 availedOfferCount = 0;
            Int64 ignoredOfferCount = 0;
            Int64 popularOfferHits = 0;
            Beacon popularBeacon = new Beacon();
            Kiosk popularKiosk = new Kiosk();
            Offer popularOffer = new Offer();
            List<Dictionary<string, object>> monthlyUserCount = new List<Dictionary<string, object>>();
            List<Dictionary<string, object>> usersCountByOfferCategory = new List<Dictionary<string, object>>();
            List<Category> categoryList = new List<Category>();

            totalMembers = getTotalMembersCount(account.id);
            activeMembers = getActiveMembersCount(account.id);
            anonymousUsers = getAnonymousUsersCount(account.id);
            availedOfferCount = getAvailedOfferCount(account.id);
            ignoredOfferCount = getIgnoredOfferCount(account.id);
            popularOfferHits = getPopularOfferHits(account.id);
            popularBeacon = getPopularBeacon(account.id);
            popularKiosk = getPopularkiosk(account.id);
            popularOffer = getPopularOffer(account.id);
            monthlyUserCount = getRegisteredUsersOfLast5Months(account.id);
            categoryList = getOfferCategoriesVsUsersOfLast5Months(account.id);

            responseObject.Add("totalMembers", totalMembers);
            responseObject.Add("activeMembers", activeMembers);
            responseObject.Add("popularBeacon", popularBeacon);
            responseObject.Add("popularKiosk", popularKiosk);
            responseObject.Add("popularOffer", popularOffer);
            responseObject.Add("availedOfferCount", availedOfferCount);
            responseObject.Add("anonymousUsers", anonymousUsers);
            responseObject.Add("ignoredOfferCount", ignoredOfferCount);
            responseObject.Add("popularOfferHits", popularOfferHits);
            responseObject.Add("monthlyUserCount", monthlyUserCount);
            responseObject.Add("categoryList", categoryList);
            return responseObject;
        }

        public List<Dictionary<string, object>> getRegisteredUsersOfLast5Months(Int64 id)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);

            List<Dictionary<string, object>> monthlyUserCount = new List<Dictionary<string, object>>();
            Int64 count = 0;
            int month = 0;
            int year = 0;         
            DateTime userDate;
            DateTime _START = currentDateTime.AddMonths(-5).AddDays(1 - currentDateTime.Day).AddHours(-currentDateTime.Hour).AddMinutes(-currentDateTime.Minute).AddSeconds(-currentDateTime.Second);
            DateTime _END = currentDateTime.AddDays(-currentDateTime.Day + 1).AddHours(-currentDateTime.Hour).AddMinutes(-currentDateTime.Minute).AddSeconds(-currentDateTime.Second - 1);

            string query = QueryConstants.TOTAL_REGISTERED_USERS_BY_MONTH;
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = id;
            Query.parameters["_START"] = _START;
            Query.parameters["_END"] = _END;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Dictionary<string, object> obj = new Dictionary<string, object>();
                    count = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("USER_COUNT")));
                    month = Convert.ToInt16(reader.GetValue(reader.GetOrdinal("_MONTH")));            
                    year = Convert.ToInt16(reader.GetValue(reader.GetOrdinal("_YEAR")));
                    userDate = new DateTime((int)year, (int)month, 1);
                    
                    obj.Add("userCount", count);
                    //obj.Add("userDate", userDate.ToString("MMM yyyy"));
                    obj.Add("userDate", userDate);
                    monthlyUserCount.Add(obj);
                }
            }
            Query.connection.Close();
            return monthlyUserCount;
        }

        public List<Category> getOfferCategoriesVsUsersOfLast5Months(Int64 id)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);

            List<Category> categories = new List<Category>();
            categories = new DALCategoryManager().getAll(id);
            Int64 count = 0;
            Int64 categoryId;
            int month = 0;
            int year = 0;
            DateTime userDate;
            DateTime _START = currentDateTime.AddMonths(-5).AddDays(1 - currentDateTime.Day).AddHours(-currentDateTime.Hour).AddMinutes(-currentDateTime.Minute).AddSeconds(-currentDateTime.Second);
            DateTime _END = currentDateTime.AddDays(-currentDateTime.Day + 1).AddHours(-currentDateTime.Hour).AddMinutes(-currentDateTime.Minute).AddSeconds(-currentDateTime.Second - 1);

            string query = QueryConstants.TOTAL_USER_OFFER_CATEGORIES_BY_MONTH;
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = id;
            Query.parameters["_START"] = _START;
            Query.parameters["_END"] = _END;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Dictionary<string, object> obj = new Dictionary<string, object>();
                    count = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("USER_COUNT")));
                    month = Convert.ToInt16(reader.GetValue(reader.GetOrdinal("_MONTH")));
                    year = Convert.ToInt16(reader.GetValue(reader.GetOrdinal("_YEAR")));
                    categoryId = reader.GetInt64(reader.GetOrdinal("CATEGORY_ID"));
                    userDate = new DateTime((int)year, (int)month, 1);
                    obj.Add("userCount", count);
                    obj.Add("userDate", userDate);
                    foreach (Category category in categories)
                    {
                        if(category.id == categoryId)
                        {
                            category.data.Add(obj);
                        }
                    }                                   
                }
            }
            Query.connection.Close();
            return categories;
        }

        public Int64 getTotalMembersCount(Int64 id)
        {
            Int64 count = 0;
            string query = QueryConstants.TOTAL_MEMBERS_COUNT;
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    count = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("USER_COUNT")));
                }
            }
            Query.connection.Close();
            return count;
        }

        public Int64 getActiveMembersCount(Int64 id)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);
            
            Int64 count = 0;
            string query = QueryConstants.LAST_MONTH_ACTIVE_MEMBERS_COUNT;
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = id;
            Query.parameters["currentDateTime"] = currentDateTime;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    count = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("USER_COUNT")));
                }
            }
            Query.connection.Close();
            return count;
        }

        public Beacon getPopularBeacon(Int64 id)
        {
            Beacon beacon = new Beacon();
            string query = QueryConstants.POPULAR_BEACON;
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    beacon.id = reader.GetInt64(reader.GetOrdinal("BEACON_ID"));
                }
            }
            Query.connection.Close();
            beacon = new DALBeaconManager().getDetails(beacon);
            return beacon;
        }

        public Kiosk getPopularkiosk(Int64 id)
        {
            Kiosk kiosk = new Kiosk();
            string query = QueryConstants.POPULAR_KIOSK;
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    kiosk.id = reader.GetInt64(reader.GetOrdinal("KIOSK_ID"));
                }
            }
            Query.connection.Close();
            kiosk = new DALKioskManager().getDetails(kiosk);
            return kiosk;
        }

        public Offer getPopularOffer(Int64 id)
        {
            Offer offer = new Offer();
            string query = QueryConstants.POPULER_OFFER;
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offer.id = reader.GetInt64(reader.GetOrdinal("OFFER_ID"));
                }
            }
            Query.connection.Close();
            offer = new DALOfferManager().getDetails(offer);
            return offer;
        }

        public Int64 getPopularOfferHits(Int64 id)
        {
            Int64 hits = 0;
            string query = QueryConstants.POPULER_OFFER;
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    hits = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("OFFER_COUNT")));
                }
            }
            Query.connection.Close();
            return hits;
        }

        public Int64 getAnonymousUsersCount(Int64 id)
        {
            Int64 count = 0;
            string query = QueryConstants.ANONYMOUS_USERS_COUNT;
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    count = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("USER_COUNT")));
                }
            }
            Query.connection.Close();
            return count;
        }

        public Int64 getAvailedOfferCount(Int64 id)
        {
            Int64 count = 0;
            string query = QueryConstants.AVAILED_OFFERS_COUNT;
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    count = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("AVAILED_OFFER_COUNT")));
                }
            }
            Query.connection.Close();
            return count;
        }

        public Int64 getIgnoredOfferCount(Int64 id)
        {
            Int64 count = 0;
            string query = QueryConstants.IGNORED_OFFERS_COUNT;
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    count = Convert.ToInt64(reader.GetValue(reader.GetOrdinal("IGNORED_OFFER_COUNT")));
                }
            }
            Query.connection.Close();
            return count;
        } 
   
    }
}