﻿using System;
using System.Collections.Generic;
using System.Web;
using BNS.Models.Offers;
using BNS.DL.WSDatabase;
using BNS.Models.Countries;
using BNS.Models.Categories;
using BNS.Models.Accounts;
using System.Data.SqlClient;
using BNS.DAL.Constants;
using BNS.Models.Kiosks;
using System.Linq;
using WSDatabase;
using System.Data;
using System.IO;
using BNS.DAL.DALBeacon;
using BNS.Models.Users;
using System.Configuration;
using BNS.Models.TermsAndConditions;
using System.Data.OleDb;
using BNS.Models.UsersKiosks;
using BNS.DAL.DALCategory;
using BNS.DAL.DALUser;

namespace BNS.DAL.DALOffer
{
    public class DALOfferManager
    {
        
        public bool saveOffer(Offer offer)
        {
            using (WSTransaction transaction = new WSTransaction("SAVE_OFFER"))
            {
                if (offer.file != null)
                {
                    String fileExtension = System.IO.Path.GetExtension(offer.file.FileName);
                    String fileName = "BNS_" + DateTime.UtcNow.ToString().Replace("/", "_").Replace(":", "_").Replace(" ", "_") + "_"; ;
                    offer.file.SaveAs(HttpContext.Current.Server.MapPath("~" + BNSConstants.OFFER_ICON_FILES_BASE_PATH) + fileName + fileExtension);
                    offer.imagePath = BNSConstants.OFFER_ICON_FILES_BASE_PATH + fileName + fileExtension;
                }
                String query = "INSERT INTO Offers(type, number, title, description, start_time, end_time, duration, image_path, account_id, created_on, updated_on, created_by, updated_by) OUTPUT Inserted.id VALUES(@type, @number, @title, @description, @startTime, @endTime, @duration, @imagePath, @account, @createdOn, @updatedOn, @createdBy, @updatedBy);";
                WSQuery Query = new WSQuery(query, offer.toDictionary(), transaction);
                offer.id = WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQueryReturnID(Query);
                if (offer.id != 0)
                {
                    foreach (Category category in offer.categories)
                    {
                        if(! this.saveOfferCategory(offer.id, category, transaction))
                        {
                            transaction.rollback();
                            return false;
                        }
                    }
                    foreach (Country country in offer.countries)
                    {
                        if(! this.saveOfferCountry(offer.id, country, transaction))
                        {
                            transaction.rollback();
                            return false;
                        }
                    }
                    foreach(TermsAndCondition term in offer.termsAndConditions)
                    {
                        if(! this.saveOfferTermsAndConditions(offer.id, term, transaction))
                        {
                            transaction.rollback();
                            return false;
                        }
                    }
                    transaction.commit();
                    return true;
                }
                else
                {
                    transaction.rollback();
                    return false;
                }
               
            }                    
        }

        public bool update(Offer offer)
        {
            using (WSTransaction transaction = new WSTransaction("UPDATE_OFFER"))
            {
                Offer oldOffer = new Offer();
                oldOffer.id = offer.id;
                oldOffer = this.getDetails(oldOffer);
                if (offer.file != null)
                {
                    String fileExtension = Path.GetExtension(offer.file.FileName);
                    String fileName = "BNS_" + DateTime.UtcNow.ToString().Replace("/", "_").Replace(":", "_").Replace(" ", "_") + "_"; ;
                    offer.file.SaveAs(HttpContext.Current.Server.MapPath("~" + BNSConstants.OFFER_ICON_FILES_BASE_PATH) + fileName + fileExtension);
                    offer.imagePath = BNSConstants.OFFER_ICON_FILES_BASE_PATH + fileName + fileExtension;
                }
                if (offer.imagePath == null || offer.imagePath == "No Image")
                {
                    offer.imagePath = oldOffer.imagePath;
                }
                else
                {
                    if (File.Exists(BNSConstants.OFFER_ICON_FILES_BASE_PATH + oldOffer.imagePath))
                    {
                        File.Delete(BNSConstants.OFFER_ICON_FILES_BASE_PATH + oldOffer.imagePath);
                    }
                }

                String query = "UPDATE Offers SET title = @title, description = @description, start_time = @startTime, end_time = @endTime, duration = @duration, image_path = @imagePath, updated_on =  @updatedOn, updated_by = @updatedBy WHERE id = @id";                               
                WSQuery Query = new WSQuery(query, offer.toDictionary(), transaction);
                if (WSDatabaseManager.sharedDatabaseManagerinstance.executeUpdateQuery(Query))
                {
                    this.deleteOfferTermsAndConditions(offer.id, transaction);                    
                    foreach (TermsAndCondition term in offer.termsAndConditions)
                    {
                        if (!this.saveOfferTermsAndConditions(offer.id, term, transaction))
                        {
                            transaction.rollback();
                            return false;
                        }
                    }
                    transaction.commit();
                    return true;                
                }
                else
                {
                    transaction.rollback();
                    return false;
                }
            }
        }

        public bool delete(Int64 offerId)
        {
            using (WSTransaction transaction = new WSTransaction("DELETE_OFFER"))
            {
                this.deleteBeaconOffers(offerId, transaction);
                this.deleteKioskLuckyItemOffers(offerId, transaction);
                this.deleteOfferTermsAndConditions(offerId, transaction);
                this.deleteOfferCountries(offerId, transaction);
                this.deleteOfferCategories(offerId, transaction);                
                WSQuery Query = new WSQuery();
                Query.query = "DELETE FROM Offers WHERE id = @offerId";
                Query.parameters["offerId"] = offerId;
                Query.transaction = transaction;
                if (!WSDatabaseManager.sharedDatabaseManagerinstance.executeDeleteQuery(Query))
                {
                    transaction.rollback();
                    return false;
                }
                transaction.commit();
                return true;
            }
        }

        public bool isDefaultOffer(Int64 offerId)
        {
            WSQuery Query = new WSQuery();
            Query.query = "SELECT * FROM default_offers WHERE offer_id = @offerId";
            Query.parameters["offerId"] = offerId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            return reader.HasRows;
        }

        public bool isOfferSetAsDefault(Int64 offerId)
        {
            WSQuery Query = new WSQuery();
            Query.query = "SELECT kiosk_id AS DEVICE_ID FROM Kiosks_Lucky_Items_Offers WHERE is_default = 1 AND offer_id = @offerId UNION SELECT beacon_id AS DEVICE_ID FROM Beacons_Offers WHERE is_default = 1 AND offer_id = @offerId";
            Query.parameters["offerId"] = offerId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            return reader.HasRows;
        }

        public bool isOfferSeenByUser(Int64 offerId)
        {
            WSQuery Query = new WSQuery();
            Query.query = "SELECT BEACON_USER.user_id AS USER_ID FROM users_beacons BEACON_USER WHERE BEACON_USER.offer_id = @offerId UNION SELECT KIOSK_USER.user_id AS USER_ID FROM users_kiosks KIOSK_USER WHERE KIOSK_USER.offer_id = @offerId";
            Query.parameters["offerId"] = offerId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            return reader.HasRows;
        }       

        public bool deleteBeaconOffers(Int64 offerId, WSTransaction transaction)
        {
            WSQuery Query = new WSQuery();
            Query.query = "IF EXISTS (SELECT * FROM Beacons_Offers WHERE offer_id = @offerId) BEGIN DELETE FROM Beacons_Offers WHERE offer_id = @offerId END";
            Query.parameters["offerId"] = offerId;
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeDeleteQuery(Query);
        }

        public bool deleteKioskLuckyItemOffers(Int64 offerId, WSTransaction transaction)
        {
            WSQuery Query = new WSQuery();
            Query.query = "IF EXISTS (SELECT * FROM Kiosks_Lucky_Items_Offers WHERE offer_id = @offerId) BEGIN DELETE FROM Kiosks_Lucky_Items_Offers WHERE offer_id = @offerId END";
            Query.parameters["offerId"] = offerId;
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeDeleteQuery(Query);
        }

        public bool deleteOfferTermsAndConditions(Int64 offerId, WSTransaction transaction)
        {
            WSQuery Query = new WSQuery();
            Query.query = "IF EXISTS (SELECT * FROM offers_terms_and_conditions WHERE offer_id = @offerId) BEGIN DELETE FROM offers_terms_and_conditions WHERE offer_id = @offerId END";
            Query.parameters["offerId"] = offerId;
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeDeleteQuery(Query);
        }

        public bool deleteOfferCountries(Int64 offerId, WSTransaction transaction)
        {
            WSQuery Query = new WSQuery();
            Query.query = "IF EXISTS (SELECT * FROM Offers_Countries WHERE offer_id = @offerId) BEGIN DELETE FROM Offers_Countries WHERE offer_id = @offerId END";
            Query.parameters["offerId"] = offerId;
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeDeleteQuery(Query);
        }

        public bool deleteOfferCategories(Int64 offerId, WSTransaction transaction)
        {
            WSQuery Query = new WSQuery();
            Query.query = "IF EXISTS (SELECT * FROM Offers_Categories WHERE offer_id = @offerId) BEGIN DELETE FROM Offers_Categories WHERE offer_id = @offerId END";
            Query.parameters["offerId"] = offerId;
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeDeleteQuery(Query);
        }

        public List<Category> getCategoriesForOffer(Int64 offerId)
        {
            String query = QueryConstants.CATEGORY_FOR_OFFER + offerId.ToString();
            WSQuery Query = new WSQuery(query);
            Query.parameters["offerId"] = offerId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Category> offerCategories = new List<Category>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offerCategories.Add(new Category(reader));
                }
            }
            Query.connection.Close();
            return offerCategories;
        }

        public List<Country> getCountriesForOffer(Int64 offerId)
        {
            String query = QueryConstants.OFFER_COUNTRIES + offerId.ToString();
            WSQuery Query = new WSQuery(query);
            Query.parameters["offerId"] = offerId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Country> offerCountries = new List<Country>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offerCountries.Add(new Country(reader));
                }
            }
            Query.connection.Close();
            return offerCountries;
        }

        public List<TermsAndCondition> getTermsAndConditionsForOffer(Int64 offerId)
        {
            String query = QueryConstants.TERM_AND_CONDITION_FOR_OFFER;
            WSQuery Query = new WSQuery(query);
            Query.parameters["offerId"] = offerId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<TermsAndCondition> termsAndCondition = new List<TermsAndCondition>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    termsAndCondition.Add(new TermsAndCondition(reader));
                }
            }
            Query.connection.Close();
            return termsAndCondition;
        }

        public List<Offer> getAll(Account account)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);

            String query = QueryConstants.ALL_OFFERS_BY_ACCOUNT;
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = account.id;
            Query.parameters["currentDateTime"] = currentDateTime;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Offer> offersList = new List<Offer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offersList.Add(new Offer(reader));
                }
            }
            Query.connection.Close();
            foreach (Offer offer in offersList)
            {
                offer.categories = getCategoriesForOffer(offer.id);
            }
            return offersList;
        }

        public Offer getDetails(Offer offer)
        {
            String query = QueryConstants.OFFER_DETAILS + offer.id.ToString();
            WSQuery Query = new WSQuery(query);
            Query.parameters["offerId"] = offer.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);           
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offer = new Offer(reader);
                }              
            }
            Query.connection.Close();
            offer.categories = getCategoriesForOffer(offer.id);          
            offer.countries = getCountriesForOffer(offer.id);
            offer.termsAndConditions = getTermsAndConditionsForOffer(offer.id);
            return offer;
        }      
        
        public List<Offer> getActiveBeaconOffers(Int64 typeId,Int64 beaconId,Int64 accountId)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            List<Offer> offersList = new List<Offer>();
            //String query = "SELECT * FROM Offers WHERE Offers.type = @typeID OR Offers.type = 3";
            String currentDateTime = DateTime.UtcNow.AddMinutes(-offSet).ToString();
            String query = QueryConstants.BEACON_ACTIVE_OFFERS_TYPE + " AND OFFER.end_time > '" + currentDateTime + "' AND OFFER.start_time < '" + currentDateTime + "' AND ( OFFER.type = @typeId OR OFFER.type = 3 )";
            WSQuery Query = new WSQuery(query);
            Query.parameters["typeId"] = typeId;
            Query.parameters["beaconId"] = beaconId;
            Query.parameters["accountId"] = accountId;

            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if(reader.HasRows)
            {
                while(reader.Read())
                {
                    offersList.Add(new Offer(reader));
                }
            }       
            return offersList;
        }

        public Offer getOffersForBeacon(Int64 beaconId, Int64 typeId, Int64 userId, Int64 accountId)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            Offer offer = new Offer();
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);
            string ConString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            SqlConnection conn = null;
            conn = new SqlConnection(ConString);
            conn.Open();

            String view1Name = "VIEW1" + Convert.ToString(beaconId);
            String view2Name = "VIEW2" + Convert.ToString(beaconId);

            String query = "CREATE VIEW " + view1Name + " AS SELECT Offers.* FROM Offers JOIN Beacons_Offers ON Beacons_Offers.offer_id = Offers.id WHERE Beacons_Offers.beacon_id = " + Convert.ToString(beaconId) + " AND (Offers.type = "+typeId+" OR Offers.type = 3) AND Offers.start_time < '" + Convert.ToString(currentDateTime) + "' AND Offers.end_time > '" + Convert.ToString(currentDateTime) + "' AND Offers.account_id = " + Convert.ToString(accountId) + " AND Beacons_Offers.is_default = 0 ";
            SqlCommand command = new SqlCommand(query, conn);
            string reutrnValue = (string)command.ExecuteScalar();

            query = "CREATE VIEW " + view2Name + " AS SELECT * FROM users_beacons WHERE users_beacons.user_id = " + Convert.ToString(userId) + " AND users_beacons.beacon_id = " + Convert.ToString(beaconId);
            command = new SqlCommand(query, conn);
            reutrnValue = (string)command.ExecuteScalar();

            query = "SELECT TOP 1 " + view1Name + ".id AS OFFER_ID, "+ view1Name + ".type AS OFFER_TYPE, "+ view1Name + ".duration AS OFFER_DURATION, " + view1Name + ".number AS OFFER_NUMBER, "+ view1Name + ".image_path AS OFFER_IMAGE_PATH, " + view1Name + ".title AS OFFER_TITLE, "+ view1Name + ".description AS OFFER_DESCRIPTION, " + view1Name + ".start_time AS OFFER_START_TIME, "+ view1Name + ".end_time AS OFFER_END_TIME FROM " + view1Name + " LEFT JOIN " + view2Name + " ON " + view2Name +".offer_id = " + view1Name + ".id WHERE " + view2Name + ".is_availed is NULL OR " + view2Name + ".is_availed = 0  ORDER BY " + view2Name+".count";
            WSQuery Query = new WSQuery(query);
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offer = new Offer(reader);
                }
            }
            Query.connection.Close();

            offer.categories = getCategoriesForOffer(offer.id);
            offer.countries = getCountriesForOffer(offer.id);
            offer.termsAndConditions = getTermsAndConditionsForOffer(offer.id);

            query = "DROP VIEW " + view1Name;
            command = new SqlCommand(query, conn);
            reutrnValue = (string)command.ExecuteScalar();

            query = "DROP VIEW " + view2Name;
            command = new SqlCommand(query, conn);
            reutrnValue = (string)command.ExecuteScalar();
            conn.Close();
            return offer;
        }

        public Offer getUserLatestOfferDetails(Offer offer)
        {
            String query = QueryConstants.OFFER_DETAILS + offer.id.ToString();
            WSQuery Query = new WSQuery(query);
            Query.parameters["offerId"] = offer.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offer = new Offer(reader);
                }
            }
            Query.connection.Close();
            return offer;
        }

        public List<Offer> getOffersForLandingPage(User user)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());           
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);
            
            //String query = QueryConstants.LANDING_PAGE_OFFERS + user.account.id.ToString() + " AND OFFER.end_time >= '" + currentDateTime + "' ORDER BY USER_BEACON.count";
            String query = "SELECT OFFER.id AS OFFER_ID, OFFER.type AS OFFER_TYPE, OFFER.number AS OFFER_NUMBER, OFFER.image_path AS OFFER_IMAGE_PATH, OFFER.title AS OFFER_TITLE, OFFER.description AS OFFER_DESCRIPTION, OFFER.start_time AS OFFER_START_TIME, OFFER.end_time AS OFFER_END_TIME, OFFER.duration AS OFFER_DURATION, OFFER.created_on AS OFFER_CREATED_ON FROM (SELECT * FROM Offers WHERE Offers.account_id = @accountId AND Offers.type in (@userType, 3)) AS OFFER LEFT JOIN (SELECT * FROM users_beacons WHERE users_beacons.user_id = @userId AND users_beacons.is_availed = 0) AS USR_BEA ON USR_BEA.offer_id = OFFER.id WHERE OFFER.start_time <= '"+ currentDateTime + "' AND OFFER.end_time >='" + currentDateTime + "' AND OFFER.id NOT IN(SELECT users_beacons.offer_id from users_beacons WHERE users_beacons.is_availed = 1 AND users_beacons.user_id = @userId) ORDER BY USR_BEA.count";
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = user.account.id;
            Query.parameters["userId"] = user.id;
            Query.parameters["userType"] = user.type;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Offer> offersList = new List<Offer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offersList.Add(new Offer(reader));
                }
            }
            Query.connection.Close();
            foreach (Offer offer in offersList)
            {
                offer.categories = getCategoriesForOffer(offer.id);
            }
            foreach (Offer offer in offersList)
            {
                offer.countries = getCountriesForOffer(offer.id);
            }
            
            return offersList;
        }

        public bool checkIfNumberExists(String offerNumber, Int64 account)
        {
            String query = "SELECT CASE WHEN EXISTS (Select * from Offers where number = @offerNumber and account_id = @accountId) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);           
            Query.parameters["offerNumber"] = offerNumber;
            Query.parameters["accountId"] = account;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }

        public bool saveOfferCountry(Int64 offer, Country country, WSTransaction transaction)
        {
            String query = "INSERT INTO Offers_Countries(offer_id, country_id) VALUES(@offerId, @countryId);";
            WSQuery Query = new WSQuery(query);
            Query.parameters["offerId"] = offer;
            Query.parameters["countryId"] = country.id;
            Query.transaction = transaction;

            return WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);
        }

        public bool saveOfferTermsAndConditions(Int64 offer, TermsAndCondition termAndCondition, WSTransaction transaction)
        {
            String query = "INSERT INTO offers_terms_and_conditions(offer_id, terms_and_conditions_id, created_at, updated_at) VALUES (@offerId, @termAndConditionId, @createdAt, @createdAt);";
            WSQuery Query = new WSQuery(query);
            Query.parameters["offerId"] = offer;
            Query.parameters["termAndConditionId"] = termAndCondition.id;
            Query.parameters["createdAt"] = DateTime.UtcNow.ToString();
            Query.parameters["updatedAt"] = DateTime.UtcNow.ToString();
            Query.transaction = transaction;

            return WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);

        }

        public bool saveOfferCategory(Int64 offer, Category category, WSTransaction transaction)
        {

            String query = "INSERT INTO Offers_Categories(offer_id, category_id, value) VALUES(@offerId, @categoryId, @value);";
            WSQuery Query = new WSQuery(query);
            Query.parameters["offerId"] = offer;
            Query.parameters["categoryId"] = category.id;
            Query.parameters["value"] = category.value;
            Query.transaction = transaction;

            return WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);
        }

        ///////////////////////////////////////// Kiosk Offer APIs ///////////////////////////////////////////////////////////

        public Offer getKioskOfferForUser(UserKiosk userKiosk)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            Offer offer = new Offer();
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);
            string ConString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            SqlConnection conn = null;
            conn = new SqlConnection(ConString);
            conn.Open();

            String view1Name = "VIEW1_" + Convert.ToString(userKiosk.kiosk_id);
            String view2Name = "VIEW2_" + Convert.ToString(userKiosk.kiosk_id);

            String query = "CREATE VIEW " + view1Name + " AS SELECT OFFER.* FROM Offers OFFER JOIN Kiosks_Lucky_Items_Offers KIOSKS_LICKY_ITEM_OFFER ON KIOSKS_LICKY_ITEM_OFFER.offer_id = OFFER.id WHERE KIOSKS_LICKY_ITEM_OFFER.kiosk_id = " + userKiosk.kiosk_id + " AND KIOSKS_LICKY_ITEM_OFFER.lucky_item_id = " + userKiosk.lucky_item_id + " AND KIOSKS_LICKY_ITEM_OFFER.is_default = 0 AND (OFFER.type = " + userKiosk.offer_type + " OR OFFER.type = 3) AND OFFER.start_time <= '" + currentDateTime +"' AND OFFER.end_time >= '" + currentDateTime + "'";
            SqlCommand command = new SqlCommand(query, conn);
            string reutrnValue = (string)command.ExecuteScalar();

            query = "CREATE VIEW " + view2Name + " AS SELECT * FROM users_kiosks WHERE users_kiosks.user_id = " + userKiosk.user_id + " AND users_kiosks.kiosk_id = " + userKiosk.kiosk_id;
            command = new SqlCommand(query, conn);
            reutrnValue = (string)command.ExecuteScalar();

            query = "SELECT TOP 1 " + view1Name + ".id AS OFFER_ID, " + view1Name + ".type AS OFFER_TYPE, " + view1Name + ".duration AS OFFER_DURATION, " + view1Name + ".number AS OFFER_NUMBER, " + view1Name + ".image_path AS OFFER_IMAGE_PATH, " + view1Name + ".title AS OFFER_TITLE, " + view1Name + ".description AS OFFER_DESCRIPTION, " + view1Name + ".start_time AS OFFER_START_TIME, " + view1Name + ".end_time AS OFFER_END_TIME FROM  " + view1Name + " LEFT JOIN  " + view2Name + " ON  " + view2Name + ".offer_id = " + view1Name + ".id WHERE " + view2Name + ".is_availed is NULL OR " + view2Name + ".is_availed = 0  ORDER BY " + view2Name + ".count";
            WSQuery Query = new WSQuery(query);
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offer = new Offer(reader);
                }
            }
            Query.connection.Close();

            offer.categories = getCategoriesForOffer(offer.id);
            offer.countries = getCountriesForOffer(offer.id);
            offer.termsAndConditions = getTermsAndConditionsForOffer(offer.id);

            query = "DROP VIEW " + view1Name;
            command = new SqlCommand(query, conn);
            reutrnValue = (string)command.ExecuteScalar();

            query = "DROP VIEW " + view2Name;
            command = new SqlCommand(query, conn);
            reutrnValue = (string)command.ExecuteScalar();
            conn.Close();
            return offer;
        }

        public bool saveKioskLuckyItemOffer(Int64 kioskId, Int64 luckyItemId, Offer offer) 
        {
            using (WSTransaction transaction = new WSTransaction("SET_KIOSK_LUCKY_ITEM_OFFER"))
            {              
                String query = "INSERT INTO Kiosks_Lucky_Items_Offers(kiosk_id, lucky_item_id, offer_id, is_default, created_on, updated_on, created_by, updated_by) VALUES(@kioskId, @luckyItemId, @offerId, @isDefault, @createdOn, @updatedOn, @createdBy, @updatedBy);";
                WSQuery Query = new WSQuery(query);
                Query.parameters["kioskId"] = kioskId;
                Query.parameters["luckyItemId"] = luckyItemId;
                Query.parameters["offerId"] = offer.id;
                Query.parameters["isDefault"] = 0;
                Query.parameters["createdOn"] = DateTime.UtcNow.ToString();
                Query.parameters["updatedOn"] = DateTime.UtcNow.ToString();
                Query.parameters["createdBy"] = offer.createdBy.id;
                Query.parameters["updatedBy"] = offer.updatedBy.id;
                Query.transaction = transaction;            
                if (! WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query))
                {
                    transaction.rollback();
                    return false;
                }
                else
                {
                    transaction.commit();
                    return true;
                }
            }              
        }      

        public bool changeLuckyItemDefaultOffer(Int64 kioskId, Int64 luckyItemId, Offer offer, int offerType)
        {
            using (WSTransaction transaction = new WSTransaction("CHANGE_LUCKY_ITEM_DEFAULT_OFFER"))
            {
                if(this.deleteLuckyItemDefaultOffer(kioskId, luckyItemId, offerType, transaction))
                {
                    String query = "INSERT INTO Kiosks_Lucky_Items_Offers(kiosk_id, lucky_item_id, offer_id, is_default, default_type, created_on, updated_on, created_by, updated_by) VALUES(@kioskId, @luckyItemId, @offerId, @isDefault, @offerType, @createdOn, @updatedOn, @createdBy, @updatedBy);";
                    WSQuery Query = new WSQuery(query);
                    Query.parameters["kioskId"] = kioskId;
                    Query.parameters["luckyItemId"] = luckyItemId;
                    Query.parameters["offerId"] = offer.id;
                    Query.parameters["isDefault"] = 1;
                    Query.parameters["offerType"] = offerType;
                    Query.parameters["createdOn"] = DateTime.UtcNow.ToString();
                    Query.parameters["updatedOn"] = DateTime.UtcNow.ToString();
                    Query.parameters["createdBy"] = offer.createdBy.id;
                    Query.parameters["updatedBy"] = offer.updatedBy.id;
                    Query.transaction = transaction;
                    if (!WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query))
                    {
                        transaction.rollback();
                        return false;
                    }
                    else
                    {
                        transaction.commit();
                        return true;
                    }
                }
                else
                {
                    transaction.rollback();
                    return false;
                }
                
            }

        }
        
        public bool deleteLuckyItemDefaultOffer(Int64 kioskId, Int64 luckyItemId, int offerType, WSTransaction transaction)
        {
            String query = "DELETE FROM Kiosks_Lucky_Items_Offers WHERE is_default = 1 AND kiosk_id = @kioskId AND lucky_item_id = @luckyItemId AND default_type = @offerType";
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = kioskId;
            Query.parameters["luckyItemId"] = luckyItemId;
            Query.parameters["offerType"] = offerType;
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeDeleteQuery(Query);      
        }

        public Int64 luckyItemCurrentActiveOffer(Int64 kioskId, Int64 luckyItemId, int offerType)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);

            String query = QueryConstants.LUCKY_ITEM_CURRENT_OFFER_ID;
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = kioskId;
            Query.parameters["luckyItemId"] = luckyItemId;
            Query.parameters["offerType"] = offerType;
            Query.parameters["currentDateTime"] = currentDateTime;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            Int64 currentActiveOfferId = 0;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    currentActiveOfferId = reader.GetInt64(reader.GetOrdinal("OFFER_ID"));
                }
            }
            Query.connection.Close();
            return currentActiveOfferId;
        }

        public List<DefaultOffer> getLuckyItemDefaultOffers(Int64 kioskId, Int64 luckyItemId)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);

            String query = QueryConstants.LUCKY_ITEM_DEFAULT_OFFERS;
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = kioskId;
            Query.parameters["luckyItemId"] = luckyItemId;
            Query.parameters["currentDateTime"] = currentDateTime;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<DefaultOffer> defaultOffers = new List<DefaultOffer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    defaultOffers.Add(new DefaultOffer(reader));
                }
            }
            Query.connection.Close();
            foreach (DefaultOffer defaultOffer in defaultOffers)
            {
                if (defaultOffer.OfferType == DefaultOffer.Offer_Type.MEMBER && luckyItemCurrentActiveOffer(kioskId, luckyItemId, 1) == 0)
                {
                    defaultOffer.offer.isCurrentlyActive = true;
                }
                else if (defaultOffer.OfferType == DefaultOffer.Offer_Type.NON_MEMBER && luckyItemCurrentActiveOffer(kioskId, luckyItemId, 2) == 0)
                {
                    defaultOffer.offer.isCurrentlyActive = true;
                }
                else
                {
                    defaultOffer.offer.isCurrentlyActive = false;
                }
                defaultOffer.offer.categories = getCategoriesForOffer(defaultOffer.offer.id);
                defaultOffer.offer.countries = getCountriesForOffer(defaultOffer.offer.id);
                defaultOffer.offer.termsAndConditions = getTermsAndConditionsForOffer(defaultOffer.offer.id);
            }

            return defaultOffers;
        }        

        public List<Offer> getLuckyItemNonDefaultOffers(Int64 kioskId, Int64 luckyItemId, Int64 accountId, int offerType)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);
            
            String query = QueryConstants.LUCKY_ITEM_NON_DEFAULT_OFFERS;
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = kioskId;
            Query.parameters["luckyItemId"] = luckyItemId;
            Query.parameters["accountId"] = accountId;
            Query.parameters["offerType"] = offerType;
            Query.parameters["currentDateTime"] = currentDateTime;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Offer> offersList = new List<Offer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offersList.Add(new Offer(reader));
                }
            }
            Query.connection.Close();
            foreach (Offer offer in offersList)
            {
                offer.categories = getCategoriesForOffer(offer.id);
            }
            return offersList;
        }
                                                 
        public Offer getLuckyItemOfferForKiosk(Int64 kioskId, Int64 luckyItemId)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);
            
            String query = QueryConstants.KIOSK_LUCKY_ITEM_OFFER + "KIOSKS_LICKY_ITEM_OFFER.kiosk_id = " + kioskId.ToString() + " AND KIOSKS_LICKY_ITEM_OFFER.lucky_item_id = " + luckyItemId.ToString() + " AND KIOSKS_LICKY_ITEM_OFFER.is_default = 1";
            WSQuery Query = new WSQuery(query);    
            Query.parameters["kioskId"] = kioskId;
            Query.parameters["luckyItemId"] = luckyItemId;
            Query.parameters["currentDateTime"] = currentDateTime;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            Offer offer = new Offer();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offer = new Offer(reader);                                   
                }
            }
            Query.connection.Close();
            offer.categories = getCategoriesForOffer(offer.id);
            return offer;
        }

        public Offer getLuckyItemDefaultOffer(UserKiosk userKiosk)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);

            String query = QueryConstants.LUCKY_ITEM_DEFAULT_OFFER_BY_TYPE;
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = userKiosk.kiosk_id;
            Query.parameters["luckyItemId"] = userKiosk.lucky_item_id;
            Query.parameters["offerType"] = userKiosk.offer_type;
            Query.parameters["currentDateTime"] = currentDateTime;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            Offer offer = new Offer();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offer = new Offer(reader);
                }
            }
            Query.connection.Close();
            offer.categories = getCategoriesForOffer(offer.id);
            offer.countries = getCountriesForOffer(offer.id);
            offer.termsAndConditions = getTermsAndConditionsForOffer(offer.id);
            return offer;
        }

        public Offer getLuckyItemCurrentActiveOfferForKiosk(UserKiosk userKiosk)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);

            String query = QueryConstants.KIOSK_LUCKY_ITEM_CURRENT_OFFER;
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = userKiosk.kiosk_id;
            Query.parameters["luckyItemId"] = userKiosk.lucky_item_id;
            Query.parameters["offerType"] = userKiosk.offer_type;
            Query.parameters["currentDateTime"] = currentDateTime;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            Offer offer = new Offer();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offer = new Offer(reader);
                }
                Query.connection.Close();
                offer.categories = getCategoriesForOffer(offer.id);
                offer.countries = getCountriesForOffer(offer.id);
                offer.termsAndConditions = getTermsAndConditionsForOffer(offer.id);
                return offer;
            }

            else
            {
                offer = this.getLuckyItemDefaultOffer(userKiosk);
                return offer;
            }
        }

        public List<Offer> getLuckyItemActiveOffers(Int64 kiosk_id, Int64 lucky_item_id)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);
                        
            String query = QueryConstants.LUCKY_ITEM_ACTIVE_OFFERS + "KIOSKS_LICKY_ITEM_OFFER.kiosk_id = " + kiosk_id.ToString() + " AND KIOSKS_LICKY_ITEM_OFFER.lucky_item_id = " + lucky_item_id.ToString() + " AND KIOSKS_LICKY_ITEM_OFFER.is_default = 0 AND OFFER.end_time >= '" + currentDateTime + "'";
            WSQuery Query = new WSQuery(query);       
            Query.parameters["kioskId"] = kiosk_id;
            Query.parameters["luckyItemId"] = lucky_item_id;
            Query.parameters["currentDateTime"] = currentDateTime;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Offer> offersList = new List<Offer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {                 
                    offersList.Add(new Offer(reader));
                }
            }
            Query.connection.Close();           
            foreach (Offer offer in offersList)
            {
                if ((offer.OfferType == Offer.Offer_Type.MEMBER || offer.OfferType == Offer.Offer_Type.BOTH) && offer.id == luckyItemCurrentActiveOffer(kiosk_id, lucky_item_id, 1))
                {
                    offer.isCurrentlyActive = true;
                }
                else if ((offer.OfferType == Offer.Offer_Type.NON_MEMBER || offer.OfferType == Offer.Offer_Type.BOTH) && offer.id == luckyItemCurrentActiveOffer(kiosk_id, lucky_item_id, 2))
                {
                    offer.isCurrentlyActive = true;
                }
                else
                {
                    offer.isCurrentlyActive = false;
                }
                offer.categories = getCategoriesForOffer(offer.id);
            }           
            return offersList;
        }

        public List<Offer> getLuckyItemOffersHistory(Int64 kiosk_id, Int64 lucky_item_id)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);
            
            String query = QueryConstants.LUCKY_ITEM_OFFERS_HISTORY + "KIOSKS_LICKY_ITEM_OFFER.kiosk_id = " + kiosk_id.ToString() + " AND KIOSKS_LICKY_ITEM_OFFER.lucky_item_id = " + lucky_item_id.ToString() + " AND KIOSKS_LICKY_ITEM_OFFER.is_default = 0 AND OFFER.end_time < '" + currentDateTime + "'";
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = kiosk_id;
            Query.parameters["luckyItemId"] = lucky_item_id;
            Query.parameters["currentDateTime"] = currentDateTime;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Offer> offersList = new List<Offer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offersList.Add(new Offer(reader));
                }
            }
            Query.connection.Close();
            foreach (Offer offer in offersList)
            {
                offer.categories = getCategoriesForOffer(offer.id);
            }
            return offersList;
        }

        public List<Offer> getLuckyItemInactiveOffers(Int64 kiosk_id, Int64 lucky_item_id, Int64 accountId)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);
            
            String query = QueryConstants.LUCKY_ITEM_INACTIVE_OFFERS + " OFFER.end_time > '" + currentDateTime + "' AND OFFER.id NOT IN (SELECT KIOSKS_LUCKY_ITEM_OFFER.offer_id FROM Kiosks_Lucky_Items_Offers KIOSKS_LUCKY_ITEM_OFFER WHERE KIOSKS_LUCKY_ITEM_OFFER.kiosk_id = " + kiosk_id.ToString() + " AND KIOSKS_LUCKY_ITEM_OFFER.lucky_item_id = " + lucky_item_id.ToString() + " ) AND OFFER.account_id = " + accountId.ToString();
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = kiosk_id;
            Query.parameters["luckyItemId"] = lucky_item_id;
            Query.parameters["currentDateTime"] = currentDateTime;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Offer> offersList = new List<Offer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offersList.Add(new Offer(reader));
                }
            }
            Query.connection.Close();
            foreach (Offer offer in offersList)
            {
                offer.categories = getCategoriesForOffer(offer.id);
            }
            return offersList;
        }

        public bool deactivateKioskLuckyItemOffer(Int64 kioskId, Int64 luckyItemId, Int64 offerId)
        {
            String query = "DELETE FROM Kiosks_Lucky_Items_Offers WHERE kiosk_id = @kioskId AND lucky_item_id = @luckyItemId AND offer_id = @offerId";
            WSQuery Query = new WSQuery(query);
            Query.parameters["kioskId"] = kioskId;
            Query.parameters["luckyItemId"] = luckyItemId;
            Query.parameters["offerId"] = offerId;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeDeleteQuery(Query);
        }


        ///////////////////////////////////////// Beacon Offer APIs ///////////////////////////////////////////////////////////

        public bool saveBeaconOffer(Int64 beaconId, Offer offer)
        {
            using (WSTransaction transaction = new WSTransaction("SET_BEACON_OFFER"))
            {
                String query = "INSERT INTO Beacons_Offers(beacon_id, offer_id, is_default, created_on, updated_on, created_by, updated_by) VALUES(@beaconId, @offerId, @isDefault, @createdOn, @updatedOn, @createdBy, @updatedBy);";
                WSQuery Query = new WSQuery(query);
                Query.parameters["beaconId"] = beaconId;
                Query.parameters["offerId"] = offer.id;
                Query.parameters["isDefault"] = 0;
                Query.parameters["createdOn"] = DateTime.UtcNow.ToString();
                Query.parameters["updatedOn"] = DateTime.UtcNow.ToString();
                Query.parameters["createdBy"] = offer.createdBy.id;
                Query.parameters["updatedBy"] = offer.updatedBy.id;
                Query.transaction = transaction;               
                if( !WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query))
                {
                    transaction.rollback();
                    return false;
                }
                else
                {
                    transaction.commit();
                    return true;
                }
            }              
        }

        public bool changeBeaconDefaultOffer(Int64 beaconId, Offer offer, int offerType)
        {
            using (WSTransaction transaction = new WSTransaction("CHANGE_BEACON_DEFAULT_OFFER"))
            {
                if(this.deleteBeaconDefaultOffer(beaconId, offerType, transaction))
                {
                    String query = "INSERT INTO Beacons_Offers(beacon_id, offer_id, is_default, default_type, created_on, updated_on, created_by, updated_by) VALUES(@beaconId, @offerId, @isDefault, @offerType, @createdOn, @updatedOn, @createdBy, @updatedBy);";
                    WSQuery Query = new WSQuery(query);
                    Query.parameters["beaconId"] = beaconId;
                    Query.parameters["offerId"] = offer.id;
                    Query.parameters["isDefault"] = 1;
                    Query.parameters["offerType"] = offerType;
                    Query.parameters["createdOn"] = DateTime.UtcNow.ToString();
                    Query.parameters["updatedOn"] = DateTime.UtcNow.ToString();
                    Query.parameters["createdBy"] = offer.createdBy.id;
                    Query.parameters["updatedBy"] = offer.updatedBy.id;
                    Query.transaction = transaction;
                    if (!WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query))
                    {
                        transaction.rollback();
                        return false;
                    }
                    else
                    {
                        transaction.commit();
                        return true;
                    }
                }
                else
                {
                    transaction.rollback();
                    return false;
                }
                
            }
        }

        public bool deleteBeaconDefaultOffer(Int64 beaconId, int offerType, WSTransaction transaction)
        {
            String query = "DELETE FROM Beacons_Offers WHERE is_default = 1 AND beacon_id = @beaconId AND default_type = @offerType";
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beaconId;
            Query.parameters["offerType"] = offerType;
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeDeleteQuery(Query);
        }

        public List<DefaultOffer> getBeaconDefaultOffers(Int64 beacon_id)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);
            
            String query = QueryConstants.BEACON_DEFAULT_OFFERS;
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beacon_id;
            Query.parameters["currentDateTime"] = currentDateTime;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<DefaultOffer> defaultOffers = new List<DefaultOffer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {                  
                    defaultOffers.Add(new DefaultOffer(reader));
                }
            }
            Query.connection.Close();           
            foreach(DefaultOffer defaultOffer in defaultOffers)
            {
                if(defaultOffer.OfferType == DefaultOffer.Offer_Type.MEMBER && beaconCurrentActiveOffer(beacon_id, 1) == 0)
                {
                    defaultOffer.offer.isCurrentlyActive = true;
                }
                else if (defaultOffer.OfferType == DefaultOffer.Offer_Type.NON_MEMBER && beaconCurrentActiveOffer(beacon_id, 2) == 0)
                {
                    defaultOffer.offer.isCurrentlyActive = true;
                }
                else
                {
                    defaultOffer.offer.isCurrentlyActive = false;
                }
                defaultOffer.offer.categories = getCategoriesForOffer(defaultOffer.offer.id);
                defaultOffer.offer.countries = getCountriesForOffer(defaultOffer.offer.id);
                defaultOffer.offer.termsAndConditions = getTermsAndConditionsForOffer(defaultOffer.offer.id);
            }
            
            return defaultOffers;
        }

        public Offer getBeaconDefaultOffer(Int64 beacon_id, int offer_type)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);
            
            String query = QueryConstants.BEACON_DEFAULT_OFFER_BY_TYPE;
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beacon_id;
            Query.parameters["offerType"] = offer_type;
            Query.parameters["currentDateTime"] = currentDateTime;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            Offer offer = new Offer();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offer = new Offer(reader);
                }
            }
            Query.connection.Close();            
            offer.categories = getCategoriesForOffer(offer.id);
            offer.countries = getCountriesForOffer(offer.id);
            offer.termsAndConditions = getTermsAndConditionsForOffer(offer.id);
            return offer;
        }

        public List<Offer> getBeaconNonDefaultOffers(Int64 beacon_id, Int64 accountId, int offerType)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);
            
            String query = QueryConstants.BEACON_NON_DEFAULT_OFFER;
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beacon_id;
            Query.parameters["accountId"] = accountId;
            Query.parameters["offerType"] = offerType;
            Query.parameters["currentDateTime"] = currentDateTime;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Offer> offersList = new List<Offer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offersList.Add(new Offer(reader));
                }
            }
            Query.connection.Close();
            foreach (Offer offer in offersList)
            {
                offer.categories = getCategoriesForOffer(offer.id);
            }

            return offersList;
        }

        public List<Offer> getBeaconActiveOffers(Int64 beacon_id)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);
           
            String query = QueryConstants.BEACON_ACTIVE_OFFERS + beacon_id.ToString() + " AND BEACON_OFFER.is_default = 0 AND OFFER.end_time >= '" + currentDateTime + "'";
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beacon_id;
            Query.parameters["currentDateTime"] = currentDateTime;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Offer> offersList = new List<Offer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offersList.Add(new Offer(reader));
                }
            }
            Query.connection.Close();          
            foreach (Offer offer in offersList)
            {
                if ((offer.OfferType == Offer.Offer_Type.MEMBER || offer.OfferType == Offer.Offer_Type.BOTH) && offer.id == beaconCurrentActiveOffer(beacon_id, 1))
                {
                    offer.isCurrentlyActive = true;
                }
                else if ((offer.OfferType == Offer.Offer_Type.NON_MEMBER || offer.OfferType == Offer.Offer_Type.BOTH) && offer.id == beaconCurrentActiveOffer(beacon_id, 2))
                {
                    offer.isCurrentlyActive = true;
                }
                else
                {
                    offer.isCurrentlyActive = false;
                }
                offer.categories = getCategoriesForOffer(offer.id);                
            }
            return offersList;
        }

        public List<Offer> getBeaconOffersHistory(Int64 beacon_id)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);
            
            String query = QueryConstants.BEACON_OFFERS_HISTORY + beacon_id.ToString() + " AND BEACON_OFFER.is_default = 0 AND OFFER.end_time < '" + currentDateTime + "'";
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beacon_id;
            Query.parameters["currentDateTime"] = currentDateTime;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Offer> offersList = new List<Offer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offersList.Add(new Offer(reader));
                }
            }
            Query.connection.Close();
            foreach (Offer offer in offersList)
            {
                offer.categories = getCategoriesForOffer(offer.id);
            }
            return offersList;
        }

        public List<Offer> getBeaconInactiveOffers(Int64 beacon_id, Int64 accountId)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);
            
            String query = QueryConstants.BEACON_INACTIVE_OFFERS + " OFFER.end_time >= '" + currentDateTime + "' AND OFFER.id NOT IN (SELECT BEACON_OFFER.offer_id FROM Beacons_Offers BEACON_OFFER WHERE BEACON_OFFER.beacon_id = " + beacon_id.ToString() + " ) AND OFFER.account_id = " + accountId.ToString();
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beacon_id;
            Query.parameters["currentDateTime"] = currentDateTime;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Offer> offersList = new List<Offer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offersList.Add(new Offer(reader));
                }               
            }
            Query.connection.Close();
            //DataSet dataSet = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQueryAndReturnDataSet(Query);

            //if(dataSet != null && dataSet.Tables.Count !=0)
            //{
            //    DataTable offerTable = dataSet.Tables[0];
            //    foreach(DataRow row in offerTable.Rows)
            //    {
            //        offersList.Add(new Offer(row));
            //    }
            //}

            foreach (Offer offer in offersList)
            {
                offer.categories = getCategoriesForOffer(offer.id);
            }
            return offersList;
        }

        public List<Offer> getNonDefaultOffersByType(int offerType, int deviceType, Int64 accountId)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);
            
            String query = QueryConstants.NON_DEFAULT_OFFERS_BY_OFFER_TYPE;
            WSQuery Query = new WSQuery(query);
            Query.parameters["offerType"] = offerType;
            Query.parameters["deviceType"] = deviceType;
            Query.parameters["accountId"] = accountId;
            Query.parameters["currentDateTime"] = currentDateTime;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Offer> offersList = new List<Offer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offersList.Add(new Offer(reader));
                }
            }
            Query.connection.Close();
            foreach (Offer offer in offersList)
            {
                offer.categories = getCategoriesForOffer(offer.id);
            }
            return offersList;
        }

        public List<DefaultOffer> getDefaultOffersByAccount(Int64 accountId)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);
            
            String query = QueryConstants.DEFAULT_OFFERS_BY_ACCOUNT;
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = accountId;
            Query.parameters["currentDateTime"] = currentDateTime;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<DefaultOffer> defaultOffersList = new List<DefaultOffer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    defaultOffersList.Add(new DefaultOffer(reader));
                }
            }
            Query.connection.Close();
            foreach (DefaultOffer defaultOffer in defaultOffersList)
            {
                defaultOffer.offer.categories = getCategoriesForOffer(defaultOffer.offer.id);
            }
            return defaultOffersList;
        }      

        public List<DefaultOffer> getDefaultOffersByDeviceType(int deviceType, Int64 accountId)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);
            
            String query = QueryConstants.DEFAULT_OFFERS_BY_DEVICE_TYPE;
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = accountId;
            Query.parameters["deviceType"] = deviceType;
            Query.parameters["currentDateTime"] = currentDateTime;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<DefaultOffer> defaultOffersList = new List<DefaultOffer>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    defaultOffersList.Add(new DefaultOffer(reader));
                }
            }
            Query.connection.Close();
            foreach (DefaultOffer defaultOffer in defaultOffersList)
            {
                defaultOffer.offer.categories = getCategoriesForOffer(defaultOffer.offer.id);
            }
            return defaultOffersList;
        }

        public bool changeDeviceDefaultOffer(int deviceType, int offerType, Int64 offerId, User user)
        {
            using (WSTransaction transaction = new WSTransaction("CHANGE_DEVICE_DEFAULT_OFFER"))
            {
                String query = "IF EXISTS(SELECT * FROM default_offers WHERE device_type = @deviceType AND offer_type = @offerType AND account_id = @accountId) UPDATE default_offers SET offer_id = @offerId, updated_on = @updatedOn, updated_by = @updatedBy WHERE device_type = @deviceType AND offer_type = @offerType AND account_id = @accountId ELSE INSERT INTO default_offers (device_type, offer_type, account_id, offer_id, created_on, updated_on, created_by, updated_by) VALUES (@deviceType, @offerType, @accountId, @offerId, @createdOn, @updatedOn, @createdBy, @updatedBy)";
                    WSQuery Query = new WSQuery(query);
                    Query.parameters["deviceType"] = deviceType;
                    Query.parameters["offerId"] = offerId;
                    Query.parameters["offerType"] = offerType;
                    Query.parameters["accountId"] = user.account.id;
                    Query.parameters["createdOn"] = DateTime.UtcNow.ToString();
                    Query.parameters["updatedOn"] = DateTime.UtcNow.ToString();
                    Query.parameters["createdBy"] = user.id;
                    Query.parameters["updatedBy"] = user.id;
                    Query.transaction = transaction;
                    if (!WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query))
                    {
                        transaction.rollback();
                        return false;
                    }
                    else
                    {
                        transaction.commit();
                        return true;
                    }                              
            }
        }

        public bool deactivateBeaconOffer(Int64 beaconId, Int64 offerId)
        {
            String query = "DELETE FROM Beacons_Offers WHERE beacon_id = @beaconId  AND  offer_id = @offerId";
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beaconId;
            Query.parameters["offerId"] = offerId;           
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeDeleteQuery(Query);
        }

        public bool checkOfferCode(Offer offer)
        {
            String query = "SELECT CASE WHEN EXISTS (SELECT * FROM users_offer_codes WHERE users_offer_codes.code = @code) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["code"] = offer.code;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }

        public bool checkOfferCodeForAccount(Offer offer)
        {
            String query = "SELECT CASE WHEN EXISTS (SELECT * FROM users_offer_codes JOIN users ON users_offer_codes.user_id = users.id WHERE users.account_id = @accountId AND users_offer_codes.code = @code) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = offer.account.id;
            Query.parameters["code"] = offer.code;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }

        public Int64 beaconCurrentActiveOffer(Int64 beaconId, int offerType)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);
                        
            String query = QueryConstants.BEACON_CURRENT_OFFER_ID;
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beaconId;
            Query.parameters["offerType"] = offerType;
            Query.parameters["currentDateTime"] = currentDateTime;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            Int64 currentActiveOfferId = 0;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    currentActiveOfferId = reader.GetInt64(reader.GetOrdinal("OFFER_ID"));
                }               
            }
            Query.connection.Close();           
            return currentActiveOfferId;
        }

        public Offer getCurrentActiveOfferForBeacon(String beaconGuid, Int64 typeId, Int64 accountId)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);
         
            DALBeaconManager beaconManager = new DALBeaconManager();
            Int64 beaconId = beaconManager.getBeaconIdFromGuid(beaconGuid);
            
            String query = QueryConstants.BEACON_CURRENT_OFFER;
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beaconId;
            Query.parameters["offerType"] = typeId;
            Query.parameters["currentDateTime"] = currentDateTime;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            Offer offer = new Offer();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                     offer = new Offer(reader);                   
                }
                Query.connection.Close();
                offer.categories = getCategoriesForOffer(offer.id);
                offer.countries = getCountriesForOffer(offer.id);
                offer.termsAndConditions = getTermsAndConditionsForOffer(offer.id);
                return offer;
            }           
            else
            {

                offer = this.getBeaconDefaultOffer(beaconId, (int)typeId);
                return offer;
            }
            
        }

        public Dictionary<string, int> uploadOffers(HttpFileCollection files, Account account)
        {
            int recordsInserted = 0;
            int recordsFailed = 0;
            Dictionary<string, int> result = new Dictionary<string, int>();

            if(files != null)
            {
                foreach(string fileName in files)
                {
                    HttpPostedFile excelFile = files[fileName];
                    String fileExtension = System.IO.Path.GetExtension(excelFile.FileName);
                    String uploadFileName = "ExcelToRead" + DateTime.UtcNow.ToString().Replace("/", "_").Replace(":", "_").Replace(" ", "_");
                    excelFile.SaveAs(HttpContext.Current.Server.MapPath("~" + BNSConstants.OFFER_ICON_FILES_BASE_PATH) + uploadFileName + fileExtension);
                    String FilePath = HttpContext.Current.Server.MapPath("~" + BNSConstants.OFFER_ICON_FILES_BASE_PATH) + uploadFileName + fileExtension;
                    string excelConnString;

                    if (fileExtension != ".xls" && fileExtension != ".xlsx")
                    {
                        result["Status"] = -2;
                        return result;
                    }

                    if (fileExtension == ".xls")
                    {
                        excelConnString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR=yes'", FilePath);
                    }
                    else
                    {
                        excelConnString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0;HDR=yes'", FilePath);
                    }

                    DataSet dataset = new DataSet();
                    DataTable dataTable = new DataTable();

                    using (OleDbConnection connection = new OleDbConnection(excelConnString))
                    {
                        OleDbCommand oleDbcommand = new OleDbCommand("Select * FROM[sheet1$]", connection);

                        OleDbDataAdapter oleDbDataAdapter = new OleDbDataAdapter();
                        oleDbDataAdapter.SelectCommand = oleDbcommand;
                        oleDbDataAdapter.Fill(dataTable);

                        foreach (DataColumn column in dataTable.Columns)
                            column.ColumnName = column.ColumnName.Replace(" ", "").ToLower();

                        int typeIndex = dataTable.Columns.IndexOf("OfferType");
                        if(typeIndex == -1)
                        {
                            result["Status"] = -1;
                            return result;
                        }

                        int countriesIndex = dataTable.Columns.IndexOf("Countries");
                        if(countriesIndex == -1)
                        {
                            result["Status"] = -1;
                            return result;
                        }

                        int categoriesIndex = dataTable.Columns.IndexOf("Categories");
                        if(categoriesIndex == -1)
                        {
                            result["Status"] = -1;
                            return result;
                        }

                        int offerNumberIndex = dataTable.Columns.IndexOf("OfferNumber");
                        if(offerNumberIndex == -1)
                        {
                            result["Status"] = -1;
                            return result;
                        }

                        int titleIndex = dataTable.Columns.IndexOf("Title");
                        if(titleIndex == -1)
                        {
                            result["Status"] = -1;
                            return result;
                        }

                        int descriptionIndex = dataTable.Columns.IndexOf("Description");
                        if(descriptionIndex == -1)
                        {
                            result["Status"] = -1;
                            return result;
                        }

                        int fromDateIndex = dataTable.Columns.IndexOf("FromDate");
                        if(fromDateIndex == -1)
                        {
                            result["Status"] = -1;
                            return result;
                        }

                        int toDateIndex = dataTable.Columns.IndexOf("ToDate");
                        if(toDateIndex == -1)
                        {
                            result["Status"] = -1;
                            return result;
                        }

                        int offerDurationIndex = dataTable.Columns.IndexOf("OfferDuration");
                        if(offerDurationIndex == -1)
                        {
                            result["Status"] = -1;
                            return result;
                        }

                        int termsAndConditionsIndex = dataTable.Columns.IndexOf("TermsAndConditions");
                        if(termsAndConditionsIndex == -1)
                        {
                            result["Status"] = -1;
                            return result;
                        }

                        int valueIndex = dataTable.Columns.IndexOf("Value");
                        if(valueIndex == -1)
                        {
                            result["Status"] = -1;
                            return result;
                        }

                        foreach (DataRow row in dataTable.Rows)
                        {
                            Offer offer = new Offer();
                            offer.account = account;

                            List<Category> categories = new List<Category>();
                            String categoriesString = row[categoriesIndex].ToString();
                            char[] delimiter = {','};
                            string[] categoriesName = categoriesString.Split(delimiter);
                            foreach(string categoryName in categoriesName)
                            {
                                Category category = new Category();
                                //category.id = categoryManager.getIdFromName(categoryName, account);
                            }
                            //TermsAndCondition termAndCondition = new TermsAndCondition();
                            //termAndCondition.title = row[nameIndex].ToString();
                            //termAndCondition.description = row[descriptionIndex].ToString();
                            //termAndCondition.account = account;
                        }
                        result["Failed"] = recordsFailed;
                        result["Inserted"] = recordsInserted;
                    }

                    System.IO.File.Delete(HttpContext.Current.Server.MapPath("~" + BNSConstants.OFFER_ICON_FILES_BASE_PATH) + uploadFileName + fileExtension);
                }
            }

            return result;
        }

    }
}