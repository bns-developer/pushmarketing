﻿using BNS.DAL.Constants;
using BNS.Models.Accounts;
using BNS.Models.TermsAndConditions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using WSDatabase;

namespace BNS.DAL.DALTermsAndConditions
{
    public class DALTermsAndConditions
    {
        public TermsAndCondition addTermsAndConditions(TermsAndCondition terms)
        {
            using (WSTransaction transaction = new WSTransaction("SAVE_TERMS_AND_CONDITIONS"))
            {

                String query = QueryConstants.ADD_TERM_CONDITION;

                WSQuery Query = new WSQuery(query);
                Query.parameters["accountId"] = terms.account.id;
                Query.parameters["title"] = terms.title;
                Query.parameters["description"] = terms.description;
                Query.parameters["created_at"] = DateTime.UtcNow;
                Query.parameters["updated_at"] = DateTime.UtcNow;

                terms.id = WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQueryReturnID(Query);

                if(terms.id != 0)
                {
                    transaction.commit();
                } else
                {
                    transaction.rollback();
                }
            }

            return terms;
        }

        public bool checkTermsAndConditions(TermsAndCondition terms)
        {
            String query = "SELECT CASE WHEN EXISTS (SELECT * FROM terms_and_conditions WHERE terms_and_conditions.title = @title and terms_and_conditions.account_id = @accountId) THEN 1 ELSE 0 END AS Result";

            WSQuery Query = new WSQuery(query);
            Query.parameters["title"] = terms.title;
            Query.parameters["accountId"] = terms.account.id;

            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }

        public List<TermsAndCondition> getAllTermsAndConditions(Account account)
        {
            List<TermsAndCondition> termsAndConditions = new List<TermsAndCondition>();

            String query = QueryConstants.GET_ALL_TERMS_AND_CONDITIONS;

            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = account.id;

            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if(reader.HasRows)
            {
                while(reader.Read())
                {
                    termsAndConditions.Add(new TermsAndCondition(reader));
                }
            }

            termsAndConditions = getTermsAndConditionCount(termsAndConditions);

            return termsAndConditions;
        }

        public List<TermsAndCondition> getTermsAndConditionCount(List<TermsAndCondition> termsAndConditions)
        {
            List<TermsAndCondition> termsAndConditionsWithCount = new List<TermsAndCondition>();

            foreach(TermsAndCondition term in termsAndConditions)
            {
                String query = "SELECT CAST(Count(*) as bigint )AS COUNT FROM offers_terms_and_conditions WHERE terms_and_conditions_id = @termsAndConditionsId";
                WSQuery Query = new WSQuery(query);
                Query.parameters["termsAndConditionsId"] = term.id;
                term.validOn = Convert.ToInt64(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
                termsAndConditionsWithCount.Add(term);
            }

            return termsAndConditionsWithCount;
        }

        public bool deleteTermsAndConditions(Int64 id)
        {
            String query = "DELETE FROM terms_and_conditions WHERE terms_and_conditions.id = @id";

            WSQuery Query = new WSQuery(query);
            Query.parameters["id"] = id;

            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeDeleteQuery(Query));

        }        

        public List<Int64> getTermsAndConditionsFromOfferId(Int64 offerId)
        {
            List<Int64> termsAndConditionsIdArray = new List<Int64>();

            String query = "SELECT OFFER_TERMS_AND_CONDITIONS.terms_and_conditions_id AS TERMS_AND_CONDITIONS_ID FROM offers_terms_and_conditions OFFER_TERMS_AND_CONDITIONS WHERE OFFER_TERMS_AND_CONDITIONS.offer_id = @offerId";
            WSQuery Query = new WSQuery(query);

            Query.parameters["offerId"] = offerId;

            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);

            if(reader.HasRows)
            {
                while (reader.Read())
                {
                    termsAndConditionsIdArray.Add(reader.GetInt64(reader.GetOrdinal("TERMS_AND_CONDITIONS_ID")));
                }
            }

            return termsAndConditionsIdArray;
        }

        public TermsAndCondition getSingleTermAndConditionById(Int64 id)
        {
            TermsAndCondition termAndCondition = new TermsAndCondition();

            String query = QueryConstants.GET_SINGLE_TERM_AND_CONDITION_BY_ID;

            WSQuery Query = new WSQuery(query);
            Query.parameters["id"] = id;

            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if(reader.HasRows)
            {
                while(reader.Read())
                {
                    termAndCondition = new TermsAndCondition(reader);
                }
            }

            return termAndCondition;
        }

        public bool editTermsAndConditions(TermsAndCondition termsAndConditionsDetails)
        {

            String query = "UPDATE terms_and_conditions SET title = @title, description = @description, updated_at = @updatedAt WHERE id = @id";
            WSQuery Query = new WSQuery(query);
            Query.parameters["title"] = termsAndConditionsDetails.title;
            Query.parameters["description"] = termsAndConditionsDetails.description;
            Query.parameters["updatedAt"] = DateTime.UtcNow;
            Query.parameters["id"] = termsAndConditionsDetails.id;


            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeUpdateQuery(Query));
        }

        public Dictionary<string, int> uploadTermsAndConditions (HttpFileCollection files,Account account)
        {
            int recordsInserted = 0;
            int recordsFailed = 0;
            Dictionary<string, int> result = new Dictionary<string, int>();

            if(files != null)
            {
                foreach(string fileName in files)
                {
                    HttpPostedFile excelFile = files[fileName];
                    String fileExtension = System.IO.Path.GetExtension(excelFile.FileName);
                    String uploadFileName = "ExcelToRead" + DateTime.UtcNow.ToString().Replace("/", "_").Replace(":", "_").Replace(" ", "_");
                    excelFile.SaveAs(HttpContext.Current.Server.MapPath("~" + BNSConstants.OFFER_ICON_FILES_BASE_PATH) + uploadFileName + fileExtension);
                    String FilePath = HttpContext.Current.Server.MapPath("~" + BNSConstants.OFFER_ICON_FILES_BASE_PATH) + uploadFileName + fileExtension;
                    string excelConnString;

                    if(fileExtension != ".xls" && fileExtension != ".xlsx")
                    {
                        result["Inserted"] = -2;
                        return result;
                    }

                    if (fileExtension == ".xls")
                    {
                        excelConnString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties='Excel 8.0;HDR=yes'", FilePath);
                    } else
                    {
                        excelConnString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0;HDR=yes'", FilePath);
                    }

                    DataSet dataset = new DataSet();
                    DataTable dataTable = new DataTable();

                    using (OleDbConnection connection = new OleDbConnection(excelConnString))
                    {
                        OleDbCommand oleDbcommand = new OleDbCommand("Select * FROM[sheet1$]", connection);

                        OleDbDataAdapter oleDbDataAdapter = new OleDbDataAdapter();
                        oleDbDataAdapter.SelectCommand = oleDbcommand;
                        oleDbDataAdapter.Fill(dataTable);

                        foreach (DataColumn column in dataTable.Columns)
                            column.ColumnName = column.ColumnName.Replace(" ", "").ToLower();

                        int nameIndex = dataTable.Columns.IndexOf("Title");
                        if(nameIndex == -1)
                        {
                            result["Inserted"] = -1;
                            return result;
                        }

                        int descriptionIndex = dataTable.Columns.IndexOf("Description");
                        if(descriptionIndex == -1)
                        {
                            result["Inserted"] = -1;
                            return result;
                        }

                        foreach(DataRow row in dataTable.Rows)
                        {
                            TermsAndCondition termAndCondition = new TermsAndCondition();
                            termAndCondition.title = row[nameIndex].ToString();
                            termAndCondition.description = row[descriptionIndex].ToString();
                            termAndCondition.account = account;
                            bool ifTermExists = checkTermsAndConditions(termAndCondition);
                            if (ifTermExists)
                            {
                                recordsFailed = recordsFailed + 1;
                            }
                            else
                            {
                                termAndCondition = addTermsAndConditions(termAndCondition);
                                if(termAndCondition.id != 0)
                                {
                                    recordsInserted = recordsInserted + 1;
                                }
                            }
                        }
                        result["Failed"] = recordsFailed;
                        result["Inserted"] = recordsInserted;
                    }

                    System.IO.File.Delete(HttpContext.Current.Server.MapPath("~" + BNSConstants.OFFER_ICON_FILES_BASE_PATH) + uploadFileName + fileExtension);
                }
            }

            return result;
        }

        public Int64 getIdFromName(string name, Account account)
        {
            Int64 id = 0;
            TermsAndCondition termsAndConditions = new TermsAndCondition();

            WSQuery Query = new WSQuery();
            Query.query = "SELECT terms_and_conditions.id AS TERMS_AND_CONDITIONS_ID FROM terms_and_conditions WHERE terms_and_conditions.title = '" + name + "' AND terms_and_conditions.account_id = '" + account.id + "';";
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);

            if(reader.HasRows)
            {
                while(reader.Read())
                {
                    termsAndConditions = new TermsAndCondition(reader);
                }
            }
            return termsAndConditions.id;
        }

    }
}