﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNS.DAL
{
    public class QueryConstants
    {
        //STORES ALIAS
        public const String STORE_ID = "STORE_ID";
        public const String STORE_NUMBER = "STORE_NUMBER";
        public const String STORE_NAME = "STORE_NAME";
        public const String STORE_ACCOUNT_ID = "STORE_ACCOUNT_ID";
        public const String STORE_COUNTRY = "STORE_COUNTRY";
        public const String STORE_CREATED_ON = "STORE_CREATED_ON";
        public const String STORE_UPDATED_ON = "STORE_UPDATED_ON";
        public const String STORE_CREATED_BY = "STORE_CREATED_BY";
        public const String STORE_UPDATED_BY = "STORE_UPDATED_BY";
        public const String STORE_ALIAS = "STORE.id AS " + STORE_ID + ", STORE.number AS " + STORE_NUMBER + ", STORE.name AS " + STORE_NAME + ", STORE.country_id AS " + STORE_COUNTRY;
        public const String STORE_ALIAS_MINIMAL = "STORE.id AS " + STORE_ID + ", STORE.name AS " + STORE_NAME;
        public const String STORE_DETAILS = "SELECT " + STORE_ALIAS + ", " + COUNTRY_ALIAS + " FROM Stores STORE INNER JOIN Countries COUNTRY ON STORE.country_id = COUNTRY.id WHERE STORE.id = @storeId";
        public const String STORE_KIOSK_ALIAS = "STORE.id AS " + STORE_ID + ", STORE.name AS " + STORE_NAME + ", STORE.account_id AS " + STORE_ACCOUNT_ID;
        public const String STORE_BEACON_ALIAS = "STORE.id AS " + STORE_ID + ", STORE.name AS " + STORE_NAME + ", STORE.account_id AS " + STORE_ACCOUNT_ID + ", STORE.country_id AS " + STORE_COUNTRY;
        public const String ALL_STORES_BY_ACCOUNT = "SELECT " + STORE_ALIAS + ", " + COUNTRY_ALIAS + " FROM Stores STORE INNER JOIN Countries COUNTRY ON STORE.country_id = COUNTRY.id where";
        public const String ALL_STORES_FOR_DROPDOWN_BY_ACCOUNT = "SELECT " + STORE_ALIAS_MINIMAL + " FROM Stores STORE where";

        //COUNTRIES ALIAS
        public const String COUNTRY_ID = "COUNTRY_ID";
        public const String COUNTRY_NAME = "COUNTRY_NAME";
        public const String COUNTRY_FLAG_PATH = "COUNTRY_FLAG_PATH";
        public const String COUNTRY_CODE = "COUNTRY_CODE";
        public const String COUNTRY_CURRENCY = "COUNTRY_CURRENCY";
        public const String COUNTRY_ALIAS = "COUNTRY.id AS " + COUNTRY_ID + ", COUNTRY.name AS " + COUNTRY_NAME;
        public const String COUNTRY_ALIAS1 = "COUNTRY.id AS " + COUNTRY_ID + ", COUNTRY.country_code AS " + COUNTRY_CODE;
        public const String ALL_COUNTRIES = "SELECT COUNTRY.id AS " + COUNTRY_ID + ", COUNTRY.name AS " + COUNTRY_NAME + ", COUNTRY.currency AS " + COUNTRY_CURRENCY + ", COUNTRY.flag_path AS " + COUNTRY_FLAG_PATH + " FROM Countries COUNTRY ORDER BY COUNTRY_NAME ASC";
        public const String ALL_COUNTRY_CODE = "SELECT COUNTRY.id AS " + COUNTRY_ID + ", COUNTRY.name AS " + COUNTRY_NAME + ", COUNTRY.country_code AS " + COUNTRY_CODE + ", COUNTRY.flag_path AS " + COUNTRY_FLAG_PATH + " FROM Countries COUNTRY ORDER BY COUNTRY_NAME ASC";
        public const String OFFER_COUNTRIES = "SELECT COUNTRY.id AS " + COUNTRY_ID + ", COUNTRY.name AS " + COUNTRY_NAME + ", COUNTRY.currency AS " + COUNTRY_CURRENCY + " FROM Countries COUNTRY JOIN Offers_Countries OFFER_COUNTRY ON OFFER_COUNTRY.country_id = COUNTRY.id WHERE OFFER_COUNTRY.offer_id = ";
        //OFFERS ALIAS
        public const String OFFER_ID = "OFFER_ID";
        public const String OFFER_ACCOUNT = "OFFER_ACCOUNT";
        public const String OFFER_NUMBER = "OFFER_NUMBER";
        public const String OFFER_TYPE = "OFFER_TYPE";
        public const String OFFER_TITLE = "OFFER_TITLE";
        public const String OFFER_DESCRIPTION = "OFFER_DESCRIPTION";
        public const String OFFER_IMAGE_PATH = "OFFER_IMAGE_PATH";
        public const String OFFER_START_TIME = "OFFER_START_TIME";
        public const String OFFER_END_TIME = "OFFER_END_TIME";
        public const String OFFER_DURATION = "OFFER_DURATION";
        public const String OFFER_CREATED_ON = "OFFER_CREATED_ON";
        public const String OFFER_UPDATED_ON = "OFFER_UPDATED_ON";
        public const String OFFER_CREATED_BY = "OFFER_CREATED_BY";
        public const String OFFER_UPDATED_BY = "OFFER_UPDATED_BY";
        public const String REGISTERED_USERS_TODAY = "REGISTERED_USERS_TODAY";
        public const String REGISTERED_USERS_THIS_WEEK = "REGISTERED_USERS_THIS_WEEK";
        public const String REGISTERED_USERS_THIS_MONTH = "REGISTERED_USERS_THIS_MONTH";
        public const String TOTAL_REGISTERED_USERS = "TOTAL_REGISTERED_USERS";
        public const String BEACON_STATUS = "BEACON_STATUS";
        public const String KIOSK_STATUS = "KIOSK_STATUS";
        public const String OFFER_ALIAS = "OFFER.id AS " + OFFER_ID + ", OFFER.type AS " + OFFER_TYPE + ", OFFER.number AS " + OFFER_NUMBER + ", OFFER.image_path AS " + OFFER_IMAGE_PATH + ", OFFER.title AS " + OFFER_TITLE + ", OFFER.description AS " + OFFER_DESCRIPTION + ", OFFER.start_time AS " + OFFER_START_TIME + ", OFFER.end_time AS " + OFFER_END_TIME + ", OFFER.duration AS " + OFFER_DURATION + ", OFFER.created_on AS " + OFFER_CREATED_ON;
        public const String OFFER_ALIAS1 = "OFFER.id AS " + OFFER_ID + ", OFFER.number AS " + OFFER_NUMBER + ", OFFER.type AS " + OFFER_TYPE + ", OFFER.title AS " + OFFER_TITLE + ", OFFER.description AS " + OFFER_DESCRIPTION + ", OFFER.created_on AS " + OFFER_CREATED_ON + ", OFFER.account_id AS " + OFFER_ACCOUNT;
        public const String OFFER_ALIAS2 = "OFFER.id AS " + OFFER_ID + ", OFFER.title AS " + OFFER_TITLE + ", OFFER.description AS " + OFFER_DESCRIPTION + ", OFFER.created_by AS " + OFFER_CREATED_BY + ", OFFER.created_on AS " + OFFER_CREATED_ON;
        public const String OFFER_ALIAS3 = "OFFER.id AS " + OFFER_ID + ", OFFER.number AS " + OFFER_NUMBER + ", OFFER.title AS " + OFFER_TITLE + ", OFFER.description AS " + OFFER_DESCRIPTION;
        public const String OFFER_DETAILS = "SELECT " + OFFER_ALIAS + " FROM Offers OFFER WHERE OFFER.id = ";       
        public const String ALL_OFFERS_BY_ACCOUNT = "SELECT " + OFFER_ALIAS1 + OFFER_ADVANCED_DATA + ", " + BEACON_STATUS_ALIAS + " AS " + BEACON_STATUS + ", " + KIOSK_STATUS_ALIAS + " AS " + KIOSK_STATUS + ", " + OFFER_AVAILED_COUNT_FOR_LIST + " AS OFFER_AVAILED_COUNT FROM Offers OFFER WHERE OFFER.account_id = @accountId ORDER BY OFFER.updated_on DESC";
        public const String REGISTERED_USERS_TODAY_ALIAS = "(SELECT COUNT(USER_KIOSK.user_id) FROM users_kiosks USER_KIOSK WHERE USER_KIOSK.offer_id = OFFER.id AND USER_KIOSK.is_registered = 1 AND DATEPART(dd,USER_KIOSK.date) = DATEPART(dd, @currentDateTime)) + (SELECT COUNT(USER_BEACON.user_id) FROM users_beacons USER_BEACON WHERE USER_BEACON.offer_id = OFFER.id AND USER_BEACON.is_registered = 1 AND DATEPART(dd, USER_BEACON.date) = DATEPART(dd, @currentDateTime))";
        public const String REGISTERED_USERS_THIS_WEEK_ALIAS = "(SELECT COUNT(USER_KIOSK.user_id) FROM users_kiosks USER_KIOSK WHERE USER_KIOSK.offer_id = OFFER.id AND USER_KIOSK.is_registered = 1 AND DATEPART(wk, USER_KIOSK.date) = DATEPART(wk, @currentDateTime)) + (SELECT COUNT(USER_BEACON.user_id) FROM users_beacons USER_BEACON WHERE USER_BEACON.offer_id = OFFER.id AND USER_BEACON.is_registered = 1 AND DATEPART(wk, USER_BEACON.date) = DATEPART(wk, @currentDateTime))";
        public const String REGISTERED_USERS_THIS_MONTH_ALIAS = "(SELECT COUNT(USER_KIOSK.user_id) FROM users_kiosks USER_KIOSK WHERE USER_KIOSK.offer_id = OFFER.id AND USER_KIOSK.is_registered = 1 AND DATEPART(mm, USER_KIOSK.date) = DATEPART(mm, @currentDateTime)) + (SELECT COUNT(USER_BEACON.user_id) FROM users_beacons USER_BEACON WHERE USER_BEACON.offer_id = OFFER.id AND USER_BEACON.is_registered = 1 AND DATEPART(mm, USER_BEACON.date) = DATEPART(mm, @currentDateTime))";
        public const String TOTAL_REGISTERED_USERS_ALIAS = "(SELECT COUNT(USER_KIOSK.user_id) FROM users_kiosks USER_KIOSK WHERE USER_KIOSK.offer_id = OFFER.id AND USER_KIOSK.is_registered = 1) + (SELECT COUNT(USER_BEACON.user_id) FROM users_beacons USER_BEACON WHERE USER_BEACON.offer_id = OFFER.id AND USER_BEACON.is_registered = 1)";
        public const String BEACON_STATUS_ALIAS = "(SELECT COUNT(BEACON_OFFER.beacon_id) FROM Beacons_Offers BEACON_OFFER JOIN Beacons BEACON ON BEACON_OFFER.beacon_id = BEACON.id WHERE BEACON_OFFER.offer_id = OFFER.id AND OFFER.end_time >= @currentDateTime AND BEACON.is_workable = 1)";
        public const String KIOSK_STATUS_ALIAS = "(SELECT  COUNT(DISTINCT KIOSK_LUCKY_ITEM_OFFER.kiosk_id) FROM Kiosks_Lucky_Items_Offers KIOSK_LUCKY_ITEM_OFFER JOIN Kiosks KIOSK ON KIOSK_LUCKY_ITEM_OFFER.kiosk_id = KIOSK.id JOIN Stores STORE ON STORE.id = KIOSK.store_id WHERE KIOSK_LUCKY_ITEM_OFFER.offer_id = OFFER.id AND OFFER.end_time >= @currentDateTime AND KIOSK.is_workable = 1 AND STORE.account_id = @accountId)";
        public const String OFFER_ADVANCED_DATA = ", " + REGISTERED_USERS_TODAY_ALIAS + " AS " + REGISTERED_USERS_TODAY + ", " + REGISTERED_USERS_THIS_WEEK_ALIAS + " AS " + REGISTERED_USERS_THIS_WEEK + ", " + REGISTERED_USERS_THIS_MONTH_ALIAS + " AS " + REGISTERED_USERS_THIS_MONTH + ", " + TOTAL_REGISTERED_USERS_ALIAS + " AS " + TOTAL_REGISTERED_USERS;
        public const String OFFER_AVAILED_COUNT_FOR_LIST = "(SELECT ((SELECT COUNT(USER_KIOSK.user_id) FROM users_kiosks USER_KIOSK WHERE USER_KIOSK.offer_id = OFFER.id AND USER_KIOSK.is_availed = 1) + (SELECT COUNT(USER_BEACON.user_id) FROM users_beacons USER_BEACON WHERE USER_BEACON.offer_id = OFFER.id AND USER_BEACON.is_availed = 1)))";
        public const String LANDING_PAGE_OFFERS = "SELECT " + OFFER_ALIAS + " FROM Offers OFFER LEFT JOIN users_beacons USER_BEACON on USER_BEACON.offer_id = OFFER.id AND USER_BEACON.user_id = @userId WHERE OFFER.account_id = ";

        public const String LUCKY_ITEM_DEFAULT_OFFERS = "SELECT " + OFFER_ALIAS + ", KIOSKS_LICKY_ITEM_OFFER.default_type AS " + DEFAULT_OFFER_OFFER_TYPE + ", KIOSKS_LICKY_ITEM_OFFER.offer_id AS " + DEFAULT_OFFER_OFFER_ID + OFFER_ADVANCED_DATA + ", " + OFFER_AVAILED_COUNT_FOR_LIST + " AS OFFER_AVAILED_COUNT FROM Offers OFFER JOIN Kiosks_Lucky_Items_Offers KIOSKS_LICKY_ITEM_OFFER ON KIOSKS_LICKY_ITEM_OFFER.offer_id = OFFER.id WHERE KIOSKS_LICKY_ITEM_OFFER.is_default = 1 AND KIOSKS_LICKY_ITEM_OFFER.kiosk_id = @kioskId AND KIOSKS_LICKY_ITEM_OFFER.lucky_item_id = @luckyItemId";
        public const String LUCKY_ITEM_DEFAULT_OFFER_BY_TYPE = "SELECT " + OFFER_ALIAS + OFFER_ADVANCED_DATA + " FROM Offers OFFER JOIN Kiosks_Lucky_Items_Offers KIOSKS_LICKY_ITEM_OFFER ON KIOSKS_LICKY_ITEM_OFFER.offer_id = OFFER.id WHERE KIOSKS_LICKY_ITEM_OFFER.is_default = 1 AND KIOSKS_LICKY_ITEM_OFFER.kiosk_id = @kioskId AND KIOSKS_LICKY_ITEM_OFFER.lucky_item_id = @luckyItemId AND KIOSKS_LICKY_ITEM_OFFER.default_type = @offerType";
        public const String LUCKY_ITEM_NON_DEFAULT_OFFERS = "SELECT " + OFFER_ALIAS + OFFER_ADVANCED_DATA + ", " + OFFER_AVAILED_COUNT_FOR_LIST + " AS OFFER_AVAILED_COUNT FROM Offers OFFER WHERE OFFER.id NOT IN (SELECT KIOSKS_LUCKY_ITEM_OFFER.offer_id FROM Kiosks_Lucky_Items_Offers KIOSKS_LUCKY_ITEM_OFFER WHERE KIOSKS_LUCKY_ITEM_OFFER.is_default = 1 AND KIOSKS_LUCKY_ITEM_OFFER.kiosk_id = @kioskId AND KIOSKS_LUCKY_ITEM_OFFER.lucky_item_id = @luckyItemId AND KIOSKS_LUCKY_ITEM_OFFER.default_type = @offerType) AND OFFER.account_id = @accountId AND (OFFER.type = @offerType OR OFFER.type = 3)";
        public const String LUCKY_ITEM_ACTIVE_OFFERS = "SELECT " + OFFER_ALIAS + OFFER_ADVANCED_DATA + ", " + OFFER_AVAILED_COUNT_FOR_LIST + " AS OFFER_AVAILED_COUNT FROM Offers OFFER JOIN Kiosks_Lucky_Items_Offers KIOSKS_LICKY_ITEM_OFFER ON KIOSKS_LICKY_ITEM_OFFER.offer_id = OFFER.id WHERE ";
        public const String LUCKY_ITEM_OFFERS_HISTORY = "SELECT " + OFFER_ALIAS + OFFER_ADVANCED_DATA + ", " + OFFER_AVAILED_COUNT_FOR_LIST + " AS OFFER_AVAILED_COUNT FROM Offers OFFER JOIN Kiosks_Lucky_Items_Offers KIOSKS_LICKY_ITEM_OFFER ON KIOSKS_LICKY_ITEM_OFFER.offer_id = OFFER.id WHERE ";
        public const String KIOSK_LUCKY_ITEM_OFFER = "SELECT " + OFFER_ALIAS + OFFER_ADVANCED_DATA + " FROM Offers OFFER JOIN Kiosks_Lucky_Items_Offers KIOSKS_LICKY_ITEM_OFFER ON KIOSKS_LICKY_ITEM_OFFER.offer_id = OFFER.id WHERE ";
        public const String KIOSK_LUCKY_ITEM_CURRENT_OFFER = "SELECT TOP 1 " + OFFER_ALIAS + ", (SELECT SUM (USER_KIOSK.count) FROM users_kiosks USER_KIOSK WHERE USER_KIOSK.offer_id = OFFER.id) AS OFFER_SEEN_COUNT FROM Offers OFFER JOIN Kiosks_Lucky_Items_Offers KIOSKS_LICKY_ITEM_OFFER ON KIOSKS_LICKY_ITEM_OFFER.offer_id = OFFER.id WHERE KIOSKS_LICKY_ITEM_OFFER.kiosk_id = @kioskId AND KIOSKS_LICKY_ITEM_OFFER.lucky_item_id = @luckyItemId AND (OFFER.type = @offerType OR OFFER.type = 3) AND KIOSKS_LICKY_ITEM_OFFER.is_default = 0 AND OFFER.end_time >= @currentDateTime ORDER BY OFFER_SEEN_COUNT ASC";
        public const String LUCKY_ITEM_CURRENT_OFFER_ID = "SELECT TOP 1  OFFER.id AS OFFER_ID, (SELECT SUM (USER_KIOSK.count) FROM users_kiosks USER_KIOSK WHERE USER_KIOSK.offer_id = OFFER.id) AS OFFER_SEEN_COUNT FROM Offers OFFER JOIN Kiosks_Lucky_Items_Offers KIOSKS_LICKY_ITEM_OFFER ON KIOSKS_LICKY_ITEM_OFFER.offer_id = OFFER.id WHERE KIOSKS_LICKY_ITEM_OFFER.kiosk_id = @kioskId AND KIOSKS_LICKY_ITEM_OFFER.lucky_item_id = @luckyItemId AND (OFFER.type = @offerType OR OFFER.type = 3 ) AND KIOSKS_LICKY_ITEM_OFFER.is_default = 0 AND OFFER.end_time >= @currentDateTime ORDER BY OFFER_SEEN_COUNT ASC";
        public const String LUCKY_ITEM_INACTIVE_OFFERS = "SELECT " + OFFER_ALIAS + OFFER_ADVANCED_DATA + ", " + OFFER_AVAILED_COUNT_FOR_LIST + " AS OFFER_AVAILED_COUNT FROM Offers OFFER WHERE ";

        public const String BEACON_ACTIVE_OFFERS = "SELECT " + OFFER_ALIAS + OFFER_ADVANCED_DATA + ", " + OFFER_AVAILED_COUNT_FOR_LIST + " AS OFFER_AVAILED_COUNT FROM Offers OFFER JOIN Beacons_Offers BEACON_OFFER ON BEACON_OFFER.offer_id = OFFER.id WHERE BEACON_OFFER.beacon_id = ";
        public const String BEACON_OFFERS_HISTORY = "SELECT " + OFFER_ALIAS + OFFER_ADVANCED_DATA + ", " + OFFER_AVAILED_COUNT_FOR_LIST + " AS OFFER_AVAILED_COUNT FROM Offers OFFER JOIN Beacons_Offers BEACON_OFFER ON BEACON_OFFER.offer_id = OFFER.id WHERE BEACON_OFFER.beacon_id = ";
        public const String BEACON_INACTIVE_OFFERS = "SELECT DISTINCT " + OFFER_ALIAS + OFFER_ADVANCED_DATA + ", " + OFFER_AVAILED_COUNT_FOR_LIST + " AS OFFER_AVAILED_COUNT FROM Offers OFFER WHERE ";
        public const String BEACON_CURRENT_OFFER = "SELECT TOP 1 " + OFFER_ALIAS + " FROM Offers OFFER JOIN Beacons_Offers BEACON_OFFER ON BEACON_OFFER.offer_id = OFFER.id WHERE BEACON_OFFER.beacon_id = @beaconId AND (OFFER.type = @offerType OR OFFER.type = 3 ) AND BEACON_OFFER.is_default = 0 AND OFFER.start_time <= @currentDateTime AND OFFER.end_time >= @currentDateTime ORDER BY OFFER.end_time ASC, DATEDIFF(MINUTE,OFFER.start_time, OFFER.end_time) DESC ";
        public const String BEACON_CURRENT_OFFER_ID = "SELECT TOP 1  OFFER.id AS OFFER_ID FROM Offers OFFER JOIN Beacons_Offers BEACON_OFFER ON BEACON_OFFER.offer_id = OFFER.id WHERE BEACON_OFFER.beacon_id = @beaconId AND (OFFER.type = @offerType OR OFFER.type = 3 ) AND BEACON_OFFER.is_default = 0 AND OFFER.start_time <= @currentDateTime AND OFFER.end_time >= @currentDateTime ORDER BY OFFER.end_time ASC, DATEDIFF(MINUTE,OFFER.start_time, OFFER.end_time) DESC";
        public const String BEACON_DEFAULT_OFFERS = "SELECT " + OFFER_ALIAS + ", BEACON_OFFER.default_type AS " + DEFAULT_OFFER_OFFER_TYPE + ", BEACON_OFFER.offer_id AS " + DEFAULT_OFFER_OFFER_ID + OFFER_ADVANCED_DATA + ", " + OFFER_AVAILED_COUNT_FOR_LIST + " AS OFFER_AVAILED_COUNT FROM Offers OFFER JOIN Beacons_Offers BEACON_OFFER ON BEACON_OFFER.offer_id = OFFER.id WHERE BEACON_OFFER.is_default = 1 AND BEACON_OFFER.beacon_id = @beaconId";
        public const String BEACON_DEFAULT_OFFER_BY_TYPE = "SELECT " + OFFER_ALIAS + OFFER_ADVANCED_DATA + " FROM Offers OFFER JOIN Beacons_Offers BEACON_OFFER ON BEACON_OFFER.offer_id = OFFER.id WHERE BEACON_OFFER.is_default = 1 AND BEACON_OFFER.default_type = @offerType AND BEACON_OFFER.beacon_id = @beaconId";
        public const String BEACON_NON_DEFAULT_OFFER = "SELECT " + OFFER_ALIAS + OFFER_ADVANCED_DATA + ", " + OFFER_AVAILED_COUNT_FOR_LIST + " AS OFFER_AVAILED_COUNT FROM Offers OFFER WHERE OFFER.id NOT IN (SELECT BEACON_OFFER.offer_id FROM Beacons_Offers BEACON_OFFER WHERE BEACON_OFFER.is_default = 1 AND BEACON_OFFER.beacon_id = @beaconId AND BEACON_OFFER.default_type = @offerType) AND OFFER.account_id = @accountId AND (OFFER.type = @offerType OR OFFER.type = 3)";
        public const String GET_ACCOUNT_ID = "SELECT Stores.account_id AS ACCOUNT_ID FROM Stores INNER JOIN Beacons ON Stores.id = Beacons.store_id WHERE Beacons.id = @beaconId";
        public const String BEACON_ACTIVE_OFFERS_TYPE = "SELECT " + OFFER_ALIAS + " FROM Offers OFFER JOIN Beacons_Offers BEACON_OFFER ON BEACON_OFFER.offer_id = OFFER.id WHERE BEACON_OFFER.is_default = 0 AND BEACON_OFFER.beacon_id = @beaconId AND OFFER.account_id = @accountId";

        public const String NON_DEFAULT_OFFERS_BY_OFFER_TYPE = "SELECT " + OFFER_ALIAS + OFFER_ADVANCED_DATA + ", " + OFFER_AVAILED_COUNT_FOR_LIST + " AS OFFER_AVAILED_COUNT FROM Offers OFFER WHERE OFFER.id NOT IN (SELECT DEFAULT_OFFER.offer_id FROM default_offers DEFAULT_OFFER WHERE DEFAULT_OFFER.offer_type = @offerType AND DEFAULT_OFFER.account_id = @accountId AND DEFAULT_OFFER.device_type = @deviceType) AND (OFFER.type = @offerType OR OFFER.type = 3) AND OFFER.account_id = @accountId";
        public const String DEFAULT_OFFERS_BY_ACCOUNT = "SELECT " + DEFAULT_OFFER_ALIAS + ", " + OFFER_ALIAS + OFFER_ADVANCED_DATA + ", " + OFFER_AVAILED_COUNT_FOR_LIST + " AS OFFER_AVAILED_COUNT FROM  default_offers DEFAULT_OFFER JOIN Offers OFFER  ON OFFER.id = DEFAULT_OFFER.offer_id WHERE DEFAULT_OFFER.account_id = @accountId";
        public const String DEFAULT_OFFERS_BY_DEVICE_TYPE = "SELECT " + DEFAULT_OFFER_ALIAS + ", " + OFFER_ALIAS + OFFER_ADVANCED_DATA + ", " + OFFER_AVAILED_COUNT_FOR_LIST + " AS OFFER_AVAILED_COUNT FROM default_offers DEFAULT_OFFER JOIN Offers OFFER  ON OFFER.id = DEFAULT_OFFER.offer_id WHERE DEFAULT_OFFER.account_id = @accountId AND DEFAULT_OFFER.device_type = @deviceType";

        //DEFAULT OFFER ALIAS
        public const String DEFAULT_OFFER_ID = "DEFAULT_OFFER_ID";
        public const String DEFAULT_OFFER_DEVICE_TYPE = "DEFAULT_OFFER_DEVICE_TYPE";
        public const String DEFAULT_OFFER_OFFER_TYPE = "DEFAULT_OFFER_OFFER_TYPE";
        public const String DEFAULT_OFFER_OFFER_ID = "DEFAULT_OFFER_OFFER_ID";
        public const String DEFAULT_OFFER_ACCOUNT_ID = "DEFAULT_OFFER_ACCOUNT_ID";
        public const String DEFAULT_OFFER_CREATED_ON = "DEFAULT_OFFER_CREATED_ON";
        public const String DEFAULT_OFFER_UPDATED_ON = "DEFAULT_OFFER_UPDATED_ON";
        public const String DEFAULT_OFFER_CREATED_BY = "DEFAULT_OFFER_CREATED_BY";
        public const String DEFAULT_OFFER_UPDATED_BY = "DEFAULT_OFFER_UPDATED_BY";
        public const String DEFAULT_OFFER_ALIAS = "DEFAULT_OFFER.id AS " + DEFAULT_OFFER_ID + ", DEFAULT_OFFER.device_type AS " + DEFAULT_OFFER_DEVICE_TYPE + ", DEFAULT_OFFER.offer_type AS " + DEFAULT_OFFER_OFFER_TYPE + ", DEFAULT_OFFER.offer_id AS " + DEFAULT_OFFER_OFFER_ID + ", DEFAULT_OFFER.account_id AS " + DEFAULT_OFFER_ACCOUNT_ID + ", DEFAULT_OFFER.created_on AS " +  DEFAULT_OFFER_CREATED_ON + ", DEFAULT_OFFER.created_by AS " + DEFAULT_OFFER_CREATED_BY;  
        

        //CATEGORIES ALIAS
        public const String CATEGORY_ID = "CATEGORY_ID";
        public const String CATEGORY_NAME = "CATEGORY_NAME";
        public const String CATEGORY_ACCOUNT = "CATEGORY_ACCOUNT";
        public const String CATEGORY_TYPE = "CATEGORY_TYPE";
        public const String CATEGORY_UNIT = "CATEGORY_UNIT";
        public const String CATEGORY_ACTIVE_ON = "CATEGORY_ACTIVE_ON";
        public const String CATEGORY_ALIAS = "CATEGORY.id AS " + CATEGORY_ID + ", CATEGORY.name AS " + CATEGORY_NAME + ", CAST(CATEGORY.type AS int) AS " + CATEGORY_TYPE;
        public const String CATEGORY_ALIAS1 = CATEGORY_ALIAS + ", CAST(CATEGORY.unit AS int) AS " + CATEGORY_UNIT;
        public const String ALL_CATEGORIES = "SELECT " + CATEGORY_ALIAS1 + " FROM Categories CATEGORY WHERE CATEGORY.account_id = @accountId";
        public const String CATEGORY_FOR_OFFER = "SELECT " + CATEGORY_ALIAS1 + ", OFFER_CATEGORY.value AS " + OFFER_CATEGORY_VALUE + " FROM Categories CATEGORY JOIN Offers_Categories OFFER_CATEGORY ON OFFER_CATEGORY.category_id = CATEGORY.id WHERE OFFER_CATEGORY.offer_id = ";
        public const String CATEGORY_FOR_LIST = "SELECT " + CATEGORY_ALIAS1 + ", " + CATEGORY_ACTIVE_ON_COUNT + " AS " + CATEGORY_ACTIVE_ON  + " FROM Categories CATEGORY WHERE CATEGORY.account_id = @accountId ORDER BY CATEGORY.updated_on DESC";
        public const String CATEGORY_ACTIVE_ON_COUNT = "(SELECT COUNT(OFFER.id) FROM Offers OFFER JOIN Offers_Categories OFFER_CATEGORY ON OFFER_CATEGORY.offer_id = OFFER.id AND OFFER_CATEGORY.category_id = CATEGORY.id WHERE OFFER.account_id = @accountId)";


        //OFFERS_CATEGORIES ALIAS
        public const String OFFER_CATEGORY_OFFERID = "OFFER_CATEGORY_OFFERID";
        public const String OFFER_CATEGORY_CATEGORYID = "OFFER_CATEGORY_CATEGORYID";
        public const String OFFER_CATEGORY_VALUE = "OFFER_CATEGORY_VALUE";

        //KIOSKS ALIAS
        public const String KIOSK_ID = "KIOSK_ID";
        public const String KIOSK_NUMBER = "KIOSK_NUMBER";
        public const String KIOSK_NAME = "KIOSK_NAME";
        public const String KIOSK_PUSH_ID = "KIOSK_PUSH_ID";
        public const String KIOSK_STORE = "KIOSK_STORE";
        public const String KIOSK_IS_WORKABLE = "KIOSK_IS_WORKABLE";
        public const String KIOSK_CREATED_ON = "KIOSK_CREATED_ON";
        public const String KIOSK_UPDATED_ON = "KIOSK_UPDATED_ON";
        public const String KIOSK_CREATED_BY = "KIOSK_CREATED_BY";
        public const String KIOSK_UPDATED_BY = "KIOSK_UPDATED_BY";
        public const String KIOSK_ALIAS = "KIOSK.id AS " + KIOSK_ID + ", KIOSK.number AS " + KIOSK_NUMBER + ", KIOSK.name AS " + KIOSK_NAME + ", KIOSK.is_workable AS " + KIOSK_IS_WORKABLE + ", KIOSK.store_id AS " + KIOSK_STORE;
        public const String KIOSK_DETAILS = "SELECT KIOSK.id AS " + KIOSK_ID + ", KIOSK.name AS " + KIOSK_NAME + ", KIOSK.is_workable AS " + KIOSK_IS_WORKABLE + ", STORE.id AS " + STORE_ID + ", STORE.name AS " + STORE_NAME + ", COUNTRY.id AS " + COUNTRY_ID + ", COUNTRY.name AS " + COUNTRY_NAME + " FROM Kiosks KIOSK INNER JOIN Stores STORE ON STORE.id = KIOSK.store_id INNER JOIN Countries COUNTRY ON COUNTRY.id = STORE.country_id WHERE KIOSK.id = @kioskId";
        public const String KIOSK_DETAILS_FOR_DEVICE = "SELECT " + KIOSK_ALIAS + ", " + STORE_KIOSK_ALIAS + " FROM Kiosks KIOSK JOIN Stores STORE ON KIOSK.store_id = STORE.id WHERE KIOSK.id = @kioskId";
        public const String KIOSK_REGISTERED_USERS_TODAY = "(SELECT COUNT(USER_KIOSK.user_id) FROM users_kiosks USER_KIOSK WHERE USER_KIOSK.kiosk_id = KIOSK.id AND USER_KIOSK.is_registered = 1 AND DATEPART(dd, USER_KIOSK.date) = DATEPART(dd, @currentDateTime))";
        public const String KIOSK_REGISTERED_USERS_THIS_WEEK = "(SELECT COUNT(USER_KIOSK.user_id) FROM users_kiosks USER_KIOSK WHERE USER_KIOSK.kiosk_id = KIOSK.id AND USER_KIOSK.is_registered = 1 AND DATEPART(wk, USER_KIOSK.date) = DATEPART(wk, @currentDateTime))";
        public const String KIOSK_REGISTERED_USERS_THIS_MONTH = "(SELECT COUNT(USER_KIOSK.user_id) FROM users_kiosks USER_KIOSK WHERE USER_KIOSK.kiosk_id = KIOSK.id AND USER_KIOSK.is_registered = 1 AND DATEPART(mm, USER_KIOSK.date) = DATEPART(mm, @currentDateTime))";
        public const String KIOSK_TOTAL_REGISTERED_USERS = "(SELECT COUNT(USER_KIOSK.user_id) FROM users_kiosks USER_KIOSK WHERE USER_KIOSK.kiosk_id = KIOSK.id AND USER_KIOSK.is_registered = 1)";
        public const String ALL_KIOSKS_BY_ACCOUNT = "SELECT " + KIOSK_ALIAS + ", " + STORE_KIOSK_ALIAS + ", " + KIOSK_REGISTERED_USERS_TODAY + " AS " + REGISTERED_USERS_TODAY + ", " + KIOSK_REGISTERED_USERS_THIS_WEEK + " AS " + REGISTERED_USERS_THIS_WEEK + ", " + KIOSK_REGISTERED_USERS_THIS_MONTH + " AS " +  REGISTERED_USERS_THIS_MONTH + ", " + KIOSK_TOTAL_REGISTERED_USERS + " AS " + TOTAL_REGISTERED_USERS + " FROM Kiosks KIOSK INNER JOIN Stores STORE ON KIOSK.store_id = STORE.id where STORE.account_id = ";
        public const String ALL_KIOSKS_BY_STORE = "SELECT " + KIOSK_ALIAS + " FROM Kiosks KIOSK WHERE KIOSK.store_id = @storeId";
        public const String LUCKY = "SELECT KIOSK.name AS " + KIOSK_NAME + ", STORE.name AS " + STORE_NAME + ", COUNTRY.name AS " + COUNTRY_NAME + ", COUNTRY.id AS " + COUNTRY_ID + ", " + LUCKY_ITEM_ALIAS + " FROM Kiosks KIOSK INNER JOIN Stores STORE ON STORE.id = KIOSK.store_id INNER JOIN Countries COUNTRY ON COUNTRY.id = STORE.country_id INNER JOIN Lucky_Items LUCKY_ITEM ON LUCKY_ITEM.id = ";
        public const String LUCKY2 = " WHERE KIOSK.id = ";

        //LUCKY_ITEMS ALIAS
        public const String LUCKY_ITEM_ID = "LUCKY_ITEM_ID";
        public const String LUCKY_ITEM_NAME = "LUCKY_ITEM_NAME";
        public const String LUCKY_ITEM_ICON = "LUCKY_ITEM_ICON";
        public const String LUCKY_ITEM_ACCOUNT = "LUCKY_ITEM_ACCOUNT";
        public const String LUCKY_ITEM_COUNT = "LUCKY_ITEM_COUNT";
        public const String LUCKY_ITEM_ALIAS = "LUCKY_ITEM.id AS " + LUCKY_ITEM_ID + ", LUCKY_ITEM.name AS " + LUCKY_ITEM_NAME + ", LUCKY_ITEM.icon AS " + LUCKY_ITEM_ICON + ", LUCKY_ITEM.account_id AS " + LUCKY_ITEM_ACCOUNT;
        public const String All_LUCKY_ITEMS_BY_KIOSK = "SELECT KIOSK.name AS KIOSK_NAME, " + LUCKY_ITEM_ALIAS + ", KIOSK_LUCKY_ITEM.short_url AS " + KIOSK_LUCKY_ITEM_SHORT_URL + " FROM Lucky_Items LUCKY_ITEM JOIN Kiosks_lucky_items KIOSK_LUCKY_ITEM ON KIOSK_LUCKY_ITEM.lucky_item_id = LUCKY_ITEM.id INNER JOIN Kiosks KIOSK ON KIOSK.id = KIOSK_LUCKY_ITEM.kiosk_id WHERE KIOSK_LUCKY_ITEM.kiosk_id = ";
        public const String All_LUCKY_ITEMS_BY_ACCOUNT = "SELECT " + LUCKY_ITEM_ALIAS  + ", STORE.id AS STORE_ID, STORE.account_id AS STORE_ACCOUNT_ID FROM Lucky_Items LUCKY_ITEM JOIN Stores STORE ON STORE.id = ";
        public const String POPULAR_LUCKY_ITEM = "SELECT TOP 1 LUCKY_ITEM.id AS " + LUCKY_ITEM_ID + ", LUCKY_ITEM.name AS " + LUCKY_ITEM_NAME + ", LUCKY_ITEM.icon AS " + LUCKY_ITEM_ICON + ", KIOSK_LUCKY_ITEM.short_url AS " + KIOSK_LUCKY_ITEM_SHORT_URL + ", KIOSK_LUCKY_ITEM.count AS " + LUCKY_ITEM_COUNT + " FROM Lucky_Items LUCKY_ITEM JOIN Kiosks_lucky_items KIOSK_LUCKY_ITEM ON KIOSK_LUCKY_ITEM.lucky_item_id = LUCKY_ITEM.id INNER JOIN Kiosks KIOSK ON KIOSK.id = KIOSK_LUCKY_ITEM.kiosk_id WHERE KIOSK_LUCKY_ITEM.kiosk_id = @kioskId AND KIOSK_LUCKY_ITEM.count > 0 ORDER BY KIOSK_LUCKY_ITEM.count DESC";

        //KIOSKS_LUCKY_ITEMS ALIAS
        public const String KIOSK_LUCKY_ITEM_KIOSKID = "KIOSK_LUCKY_ITEM_KIOSKID";
        public const String KIOSK_LUCKY_ITEM_LUCKYITEMID = "KIOSK_LUCKY_ITEM_LUCKYITEMID";
        public const String KIOSK_LUCKY_ITEM_SHORT_URL = "KIOSK_LUCKY_ITEM_SHORT_URL";
        public const String KIOSK_LUCKY_ITEM_DETAILS1 = "SELECT LUCKY_ITEM.id AS " + LUCKY_ITEM_ID + ", LUCKY_ITEM.name AS " + LUCKY_ITEM_NAME + ", LUCKY_ITEM.icon AS " + LUCKY_ITEM_ICON + ", KIOSK_LUCKY_ITEM.short_url AS " + KIOSK_LUCKY_ITEM_SHORT_URL + ", KIOSK.id AS " + KIOSK_ID + ", KIOSK.name AS " + KIOSK_NAME + ", STORE.id AS " + STORE_ID + ", STORE.name AS " + STORE_NAME + ", COUNTRY.id AS " + COUNTRY_ID + ", COUNTRY.name AS " + COUNTRY_NAME + " FROM Lucky_Items LUCKY_ITEM JOIN Kiosks KIOSK ON KIOSK.id = ";
        public const String KIOSK_LUCKY_ITEM_DETAILS2 = " JOIN Kiosks_lucky_items KIOSK_LUCKY_ITEM ON KIOSK_LUCKY_ITEM.kiosk_id = KIOSK.id AND KIOSK_LUCKY_ITEM.lucky_item_id = LUCKY_ITEM.id JOIN Stores STORE ON STORE.id = KIOSK.store_id JOIN Countries COUNTRY ON COUNTRY.id = STORE.country_id WHERE LUCKY_ITEM.id = ";

        //KIOSKS_LUCKY_ITEMS_OFFERS ALIAS
        public const String KIOSK_LUCKY_ITEM_OFFER_KIOSKID = "KIOSK_LUCKY_ITEM_OFFER_KIOSKID";
        public const String KIOSK_LUCKY_ITEM_OFFER_LUCKYITEMID = "KIOSK_LUCKY_ITEM_OFFER_LUCKYITEMID";
        public const String KIOSK_LUCKY_ITEM_OFFER_OFFERID = "KIOSK_LUCKY_ITEM_OFFER_OFFERID";
        public const String KIOSK_LUCKY_ITEM_OFFER_SHORTURL = "KIOSK_LUCKY_ITEM_OFFER_SHORTURL";
        public const String KIOSK_LUCKY_ITEM_OFFER_CREATEDON = "KIOSK_LUCKY_ITEM_OFFER_CREATEDON";
        public const String KIOSK_LUCKY_ITEM_OFFER_UPDATEDON = "KIOSK_LUCKY_ITEM_OFFER_UPDATEDON";
        public const String KIOSK_LUCKY_ITEM_OFFER_CREATEDBY = "KIOSK_LUCKY_ITEM_OFFER_CREATEDBY";
        public const String KIOSK_LUCKY_ITEM_OFFER_UPDATEDBY = "KIOSK_LUCKY_ITEM_OFFER_UPDATEDBY";
        public const String KIOSKS_LUCKY_ITEMS_OFFERS_ALIAS = "KIOSK_LUCKY_ITEM.kiosk_id AS " + KIOSK_LUCKY_ITEM_OFFER_KIOSKID + ", KIOSK_LUCKY_ITEM.lucky_item_id AS " + KIOSK_LUCKY_ITEM_OFFER_LUCKYITEMID;
        
        //BEACONS ALIAS
        public const String BEACON_ID = "BEACON_ID";
        public const String BEACON_NUMBER = "BEACON_NUMBER";
        public const String BEACON_NAME = "BEACON_NAME";
        public const String BEACON_GUID = "BEACON_GUID";
        public const String BEACON_UUID = "BEACON_UUID";
        public const String BEACON_ADDRESS = "BEACON_ADDRESS";
        public const String BEACON_NOTIFICATION = "BEACON_NOTIFICATION";
        public const String BEACON_LATITUDE = "BEACON_LATITUDE";
        public const String BEACON_LONGITUDE = "BEACON_LONGITUDE";
        public const String BEACON_STORE = "BEACON_STORE";
        public const String BEACON_IS_WORKABLE = "BEACON_IS_WORKABLE"; 
        public const String BEACON_SHORT_URL = "BEACON_SHORT_URL";
        public const String BEACON_PASS_URL = "BEACON_PASS_URL"; 
        public const String BEACON_CREATED_ON = "BEACON_CREATED_ON";
        public const String BEACON_UPDATED_ON = "BEACON_UPDATED_ON";
        public const String BEACON_CREATED_BY = "BEACON_CREATED_BY";
        public const String BEACON_UPDATED_BY = "BEACON_UPDATED_BY";
        public const String BEACON_REGISTERED_USERS_TODAY = "(SELECT COUNT(USER_BEACON.user_id) FROM users_beacons USER_BEACON WHERE USER_BEACON.beacon_id = BEACON.id AND USER_BEACON.is_registered = 1 AND DATEPART(dd, USER_BEACON.date) = DATEPART(dd, @currentDateTime))";
        public const String BEACON_REGISTERED_USERS_THIS_WEEK = "(SELECT COUNT(USER_BEACON.user_id) FROM users_beacons USER_BEACON WHERE USER_BEACON.beacon_id = BEACON.id AND USER_BEACON.is_registered = 1 AND DATEPART(wk, USER_BEACON.date) = DATEPART(wk, @currentDateTime))";
        public const String BEACON_REGISTERED_USERS_THIS_MONTH = "(SELECT COUNT(USER_BEACON.user_id) FROM users_beacons USER_BEACON WHERE USER_BEACON.beacon_id = BEACON.id AND USER_BEACON.is_registered = 1 AND DATEPART(mm, USER_BEACON.date) = DATEPART(mm, @currentDateTime))";
        public const String BEACON_TOTAL_REGISTERED_USERS = "(SELECT COUNT(USER_BEACON.user_id) FROM users_beacons USER_BEACON WHERE USER_BEACON.beacon_id = BEACON.id AND USER_BEACON.is_registered = 1)";
        public const String BEACON_ADVANCE_DATA = BEACON_REGISTERED_USERS_TODAY + " AS " + REGISTERED_USERS_TODAY + ", " + BEACON_REGISTERED_USERS_THIS_WEEK + " AS " + REGISTERED_USERS_THIS_WEEK + ", " + BEACON_REGISTERED_USERS_THIS_MONTH + " AS " + REGISTERED_USERS_THIS_MONTH + ", " + BEACON_TOTAL_REGISTERED_USERS + " AS " + TOTAL_REGISTERED_USERS;
        public const String BEACON_ALIAS = "BEACON.id AS " + BEACON_ID + ", BEACON.number AS " + BEACON_NUMBER + ", BEACON.name AS " + BEACON_NAME + ", BEACON.is_workable AS " + BEACON_IS_WORKABLE + ", BEACON.store_id AS " + BEACON_STORE ;
        public const String ALL_BEACONS_BY_ACCOUNT = "SELECT " + BEACON_ALIAS + ", " + STORE_BEACON_ALIAS + ", " + COUNTRY_ALIAS + ", " + BEACON_ADVANCE_DATA + " FROM Beacons BEACON INNER JOIN Stores STORE ON BEACON.store_id = STORE.id JOIN Countries COUNTRY ON STORE.country_id = COUNTRY.id where STORE.account_id = ";
        public const String BEACON_DETAILS = "SELECT BEACON.id AS " + BEACON_ID + ", BEACON.name AS " + BEACON_NAME + ", BEACON.short_url AS " + BEACON_SHORT_URL + ", BEACON.is_workable AS " + BEACON_IS_WORKABLE + ", BEACON.pass_url AS " + BEACON_PASS_URL + ", STORE.id AS " + STORE_ID + ", STORE.name AS " + STORE_NAME + ", COUNTRY.id AS " + COUNTRY_ID + ", COUNTRY.name AS " + COUNTRY_NAME + " FROM Beacons BEACON INNER JOIN Stores STORE ON STORE.id = BEACON.store_id INNER JOIN Countries COUNTRY ON COUNTRY.id = STORE.country_id WHERE BEACON.id = ";
        public const String BEACON_ID_FROM_GUID = "SELECT BEACON.id AS " + BEACON_ID + " FROM Beacons BEACON WHERE BEACON.beacon_unique_id = ";


        //BEACONS_OFFERS ALIAS
        public const String BEACON_OFFER_BEACONID = "BEACON_OFFER_BEACONID";
        public const String BEACON_OFFER_OFFERID = "BEACON_OFFER_OFFERID";
        public const String BEACON_OFFER_SHORT_URL = "BEACON_OFFER_SHORT_URL";
        public const String BEACON_OFFER_CREATED_ON = "BEACON_OFFER_CREATED_ON";
        public const String BEACON_OFFER_UPDATED_ON = "BEACON_OFFER_UPDATED_ON";
        public const String BEACON_OFFER_CREATED_BY = "BEACON_OFFER_CREATED_BY";
        public const String BEACON_OFFER_UPDATED_BY = "BEACON_OFFER_UPDATED_BY";

        //USERS ALIAS
        public const String USER_ID = "USER_ID";
        public const String USER_CONTACT = "USER_CONTACT";
        public const String USER_ACCOUNT = "USER_ACCOUNT";
        public const String USER_CREATED_ON = "USER_CREATED_ON";
        public const String USER_UPDATED_ON = "USER_UPDATED_ON";  
        public const String USER_BEACON = "USER_BEACON";
        public const String USER_KIOSK = "USER_KIOSK";
        public const String USER_MEMBER_ID = "USER_MEMBER_ID";
        public const String USER_DATE = "USER_DATE";
        public const String USER_SOCIAL_SERVICE = "USER_SOCIAL_SERVICE";
        public const String IS_OFFER_AVAILED = "IS_OFFER_AVAILED";
        public const String USER_LATEST_OFFER = "USER_LATEST_OFFER";
        public const String USER_LAST_ACTIVITY = "USER_LAST_ACTIVITY";
        public const String USER_ALIAS_MINIMAL = "USR.id AS " + USER_ID + ", USR.member_id AS " + USER_MEMBER_ID;
        public const String USER_REGISTRATION_DATE = "SELECT USER_BEACON.date AS REGISTRATION_DATE FROM users_beacons USER_BEACON WHERE USER_BEACON.user_id = USR.id AND USER_BEACON.is_registered = 1 UNION SELECT USER_KIOSK.date AS REGISTRATION_DATE FROM users_kiosks USER_KIOSK WHERE USER_KIOSK.user_id = USR.id AND USER_KIOSK.is_registered = 1";
        public const String USER_ALIAS = "USR.id AS " + USER_ID + ", USR.account_id AS " + USER_ACCOUNT + ", (" + USER_REGISTRATION_DATE + ") AS " + USER_CREATED_ON + ", USR.member_id AS " + USER_MEMBER_ID + ", USR.type AS TYPE";
        public const String USER_DETAILS = "SELECT USR.id AS " + USER_ID + ", USR.type AS TYPE FROM users USR WHERE USR.member_id = @memberId AND USR.account_id = @accountId";
        public const String USER_SOCIAL_LOGIN_DETAILS = "SELECT USR.id AS USER_ID , USR_SOCI_LOG.social_service AS USER_SOCIAL_SERVICE, USR.type AS TYPE FROM users USR JOIN users_social_login USR_SOCI_LOG ON USR_SOCI_LOG.user_id = USR.id WHERE USR.member_id = @memberId AND USR.account_id = @accountId";
        public const String USER_DETAILS_BY_MEMBER_ID = "SELECT USR.id AS " + USER_ID + ", USR.type AS TYPE, USR.account_id AS ACCOUNT_ID, USR.contact_id AS CONTACT_ID FROM users USR WHERE USR.member_id = @memberId";
        public const String USER_DETAILS1 = "SELECT USR.id AS " + USER_ID + ", USR.account_id AS " + ACCOUNT_ID + " FROM users USR Where username = @username AND password = @password;";
        public const String REGISTERED_USERS_FROM_BEACON = "SELECT " + USER_ALIAS + ", " + STORE_ALIAS_MINIMAL + ", " + USER_BEACON_ALIAS + ", " + OFFER_ALIAS3 + ", " + USER_LATEST_OFFER_ALIAS + " AS " + USER_LATEST_OFFER + ", " + USER_LAST_ACTIVITY_ALIAS + " AS " + USER_LAST_ACTIVITY + " FROM users USR JOIN users_beacons USER_BEACON ON USER_BEACON.user_id = USR.id JOIN Beacons BEACON ON BEACON.id = USER_BEACON.beacon_id JOIN Stores STORE ON STORE.id = BEACON.store_id AND USER_BEACON.beacon_id = BEACON_ID JOIN Offers OFFER ON OFFER.id = USER_BEACON.offer_id WHERE USER_BEACON.is_registered = 1 AND USR.account_id = @accountId AND USR.type = 1";
        public const String REGISTERED_USERS_FROM_KIOSK = "SELECT " + USER_ALIAS + ", " + STORE_ALIAS_MINIMAL + ", " + USER_KIOSK_ALIAS + ", " + OFFER_ALIAS3 + ", " + USER_LATEST_OFFER_ALIAS + " AS " + USER_LATEST_OFFER + ", " + USER_LAST_ACTIVITY_ALIAS + " AS " + USER_LAST_ACTIVITY + " FROM users USR JOIN users_kiosks USER_KIOSK ON USER_KIOSK.user_id = USR.id JOIN kiosks KIOSK ON KIOSK.id = USER_KIOSK.kiosk_id JOIN Stores STORE ON STORE.id = KIOSK.store_id AND USER_KIOSK.kiosk_id = KIOSK_ID JOIN Offers OFFER ON OFFER.id = USER_KIOSK.offer_id WHERE USER_KIOSK.is_registered = 1 AND USR.account_id = @accountId AND USR.type = 1";
        public const String TOTAL_REGISTERED_USERS_FROM_ACCOUNT = REGISTERED_USERS_FROM_BEACON + "  UNION " + REGISTERED_USERS_FROM_KIOSK + " ORDER BY USER_LAST_ACTIVITY DESC";
        public const String ANNONYMOUS_USERS = "SELECT " + USER_ALIAS + ", " + STORE_ALIAS_MINIMAL + ", " + USER_BEACON_ALIAS + ", " + OFFER_ALIAS3 + " FROM users USR JOIN users_beacons USER_BEACON ON USER_BEACON.user_id = USR.id JOIN Beacons BEACON ON BEACON.id = USER_BEACON.beacon_id JOIN Stores STORE ON STORE.id = BEACON.store_id AND USER_BEACON.beacon_id = BEACON_ID JOIN Offers OFFER ON OFFER.id = USER_BEACON.offer_id WHERE USR.type = 2 AND OFFER.id = (SELECT TOP 1 LATEST_OFFER_ID FROM (SELECT LATEST_OFFER.id AS LATEST_OFFER_ID, USER_BEACON.date AS LATEST_OFFER_DATE FROM Offers LATEST_OFFER JOIN users_beacons USER_BEACON ON USER_BEACON.user_id = USR.id WHERE LATEST_OFFER.id = USER_BEACON.offer_id) AS LATEST_OFFER ORDER BY LATEST_OFFER_DATE DESC) AND USER_BEACON.date = (SELECT TOP 1 LATEST_OFFER_DATE FROM (SELECT LATEST_OFFER.id AS LATEST_OFFER_ID, USER_BEACON.date AS LATEST_OFFER_DATE FROM Offers LATEST_OFFER JOIN users_beacons USER_BEACON ON USER_BEACON.user_id = USR.id WHERE LATEST_OFFER.id = USER_BEACON.offer_id) AS LATEST_OFFER ORDER BY LATEST_OFFER_DATE DESC) AND USR.account_id = @accountId ORDER BY USER_DATE DESC";
        public const String TOTAL_ANNONYMOUS_USERS = "SELECT " + USER_ALIAS + ", " + STORE_ALIAS_MINIMAL + ", " + USER_BEACON_ALIAS + ", " + OFFER_ALIAS3 + ", " + USER_LATEST_OFFER_ALIAS + " AS " + USER_LATEST_OFFER + ", " + USER_LAST_ACTIVITY_ALIAS + " AS " + USER_LAST_ACTIVITY + " FROM users USR JOIN users_beacons USER_BEACON ON USER_BEACON.user_id = USR.id JOIN Beacons BEACON ON BEACON.id = USER_BEACON.beacon_id JOIN Stores STORE ON STORE.id = BEACON.store_id AND USER_BEACON.beacon_id = BEACON_ID JOIN Offers OFFER ON OFFER.id = USER_BEACON.offer_id WHERE USR.type = 2 AND USR.account_id = @accountId AND USER_BEACON.date = (SELECT MIN(users_beacons.date) from users_beacons where users_beacons.user_id = USR.id)";
        public const String USER_BEACON_ALIAS = "BEACON.id AS DEVICE_ID, '1' AS DEVICE_TYPE, BEACON.number AS DEVICE_NUMBER, BEACON.name AS DEVICE_NAME, USER_BEACON.date AS USER_DATE";
        public const String USER_KIOSK_ALIAS = "KIOSK.id AS DEVICE_ID, '2' AS DEVICE_TYPE, KIOSK.number AS DEVICE_NUMBER, KIOSK.name AS DEVICE_NAME, USER_KIOSK.date AS USER_DATE";
        public const String USER_LATEST_OFFER_ALIAS = "(SELECT TOP 1 LATEST_OFFER_ID FROM (SELECT LATEST_OFFER.id AS LATEST_OFFER_ID, USER_KIOSK.date AS LATEST_OFFER_DATE FROM Offers LATEST_OFFER JOIN users_kiosks USER_KIOSK ON USER_KIOSK.user_id = USR.id WHERE LATEST_OFFER.id = USER_KIOSK.offer_id UNION SELECT LATEST_OFFER.id AS LATEST_OFFER_ID, USER_BEACON.date AS LATEST_OFFER_DATE FROM Offers LATEST_OFFER JOIN users_beacons USER_BEACON ON USER_BEACON.user_id = USR.id WHERE LATEST_OFFER.id = USER_BEACON.offer_id) AS LATEST_OFFER ORDER BY LATEST_OFFER_DATE DESC)";
        public const String USER_LAST_ACTIVITY_ALIAS = "(SELECT TOP 1 LATEST_OFFER_DATE FROM (SELECT LATEST_OFFER.id AS LATEST_OFFER_ID, USER_KIOSK.date AS LATEST_OFFER_DATE FROM Offers LATEST_OFFER JOIN users_kiosks USER_KIOSK ON USER_KIOSK.user_id = USR.id WHERE LATEST_OFFER.id = USER_KIOSK.offer_id UNION SELECT LATEST_OFFER.id AS LATEST_OFFER_ID, USER_BEACON.date AS LATEST_OFFER_DATE FROM Offers LATEST_OFFER JOIN users_beacons USER_BEACON ON USER_BEACON.user_id = USR.id WHERE LATEST_OFFER.id = USER_BEACON.offer_id) AS LATEST_OFFER ORDER BY LATEST_OFFER_DATE DESC)";
        public const String USER_ACTIVITIES = "SELECT USR.id AS USER_ID, USER_BEACON.date AS USER_DATE, USER_BEACON.is_availed AS IS_OFFER_AVAILED,  STORE.id AS STORE_ID, STORE.name AS STORE_NAME, BEACON.id AS DEVICE_ID, '1' AS DEVICE_TYPE, BEACON.number AS DEVICE_NUMBER, BEACON.name AS DEVICE_NAME, OFFER.id AS OFFER_ID, OFFER.number AS OFFER_NUMBER, OFFER.title AS OFFER_TITLE, OFFER.description AS OFFER_DESCRIPTION FROM users USR JOIN users_beacons USER_BEACON ON USER_BEACON.user_id = USR.id JOIN Beacons BEACON ON BEACON.id = USER_BEACON.beacon_id JOIN Stores STORE ON STORE.id = BEACON.store_id AND USER_BEACON.beacon_id = BEACON_ID JOIN Offers OFFER ON OFFER.id = USER_BEACON.offer_id WHERE USR.id = @userId UNION SELECT USR.id AS USER_ID, USER_KIOSK.date AS USER_DATE, USER_KIOSK.is_availed AS IS_OFFER_AVAILED, STORE.id AS STORE_ID, STORE.name AS STORE_NAME, KIOSK.id AS DEVICE_ID, '2' AS DEVICE_TYPE, KIOSK.number AS DEVICE_NUMBER, KIOSK.name AS DEVICE_NAME, OFFER.id AS OFFER_ID, OFFER.number AS OFFER_NUMBER, OFFER.title AS OFFER_TITLE, OFFER.description AS OFFER_DESCRIPTION FROM users USR JOIN users_kiosks USER_KIOSK ON USER_KIOSK.user_id = USR.id JOIN kiosks KIOSK ON KIOSK.id = USER_KIOSK.kiosk_id JOIN Stores STORE ON STORE.id = KIOSK.store_id AND USER_KIOSK.kiosk_id = KIOSK_ID JOIN Offers OFFER ON OFFER.id = USER_KIOSK.offer_id WHERE USR.id = @userId ORDER BY USER_DATE DESC";
        public const String ANNONYMOUS_ACTIVITIES = "SELECT USR.id AS USER_ID, USER_BEACON.date AS USER_DATE, USER_BEACON.is_availed AS IS_OFFER_AVAILED,  STORE.id AS STORE_ID, STORE.name AS STORE_NAME, BEACON.id AS DEVICE_ID, '1' AS DEVICE_TYPE, BEACON.number AS DEVICE_NUMBER, BEACON.name AS DEVICE_NAME, OFFER.id AS OFFER_ID, OFFER.number AS OFFER_NUMBER, OFFER.title AS OFFER_TITLE, OFFER.description AS OFFER_DESCRIPTION FROM users USR JOIN users_beacons USER_BEACON ON USER_BEACON.user_id = USR.id JOIN Beacons BEACON ON BEACON.id = USER_BEACON.beacon_id JOIN Stores STORE ON STORE.id = BEACON.store_id AND USER_BEACON.beacon_id = BEACON_ID JOIN Offers OFFER ON OFFER.id = USER_BEACON.offer_id WHERE USR.id = @userId ORDER BY USER_DATE DESC";
        public const String BEACON_USER_DETAILS_BY_ID = "SELECT " + USER_ALIAS + ", " + STORE_ALIAS_MINIMAL + ", " + USER_BEACON_ALIAS + ", " + OFFER_ALIAS3 + " FROM users USR JOIN users_beacons USER_BEACON ON USER_BEACON.user_id = USR.id JOIN Beacons BEACON ON BEACON.id = USER_BEACON.beacon_id JOIN Stores STORE ON STORE.id = BEACON.store_id AND USER_BEACON.beacon_id = BEACON_ID JOIN Offers OFFER ON OFFER.id = USER_BEACON.offer_id WHERE USER_BEACON.is_registered = 1 AND USR.id = @userId";
        public const String KIOSK_USER_DETAILS_BY_ID = "SELECT " + USER_ALIAS + ", " + STORE_ALIAS_MINIMAL + ", " + USER_KIOSK_ALIAS + ", " + OFFER_ALIAS3 + " FROM users USR JOIN users_kiosks USER_KIOSK ON USER_KIOSK.user_id = USR.id JOIN kiosks KIOSK ON KIOSK.id = USER_KIOSK.kiosk_id JOIN Stores STORE ON STORE.id = KIOSK.store_id AND USER_KIOSK.kiosk_id = KIOSK_ID JOIN Offers OFFER ON OFFER.id = USER_KIOSK.offer_id WHERE USER_KIOSK.is_registered = 1 AND USR.id = @userId";
        public const String USER_DETAILS_BY_ID = "IF EXISTS (SELECT * FROM users_beacons WHERE is_registered = 1 AND user_id = @userId) " + BEACON_USER_DETAILS_BY_ID + " ELSE " + KIOSK_USER_DETAILS_BY_ID;
        public const String USER_DETAILS_BY_UNIQUE_ID = "SELECT  USR_SOC_LOG.user_id AS USER_ID, USR_SOC_LOG.social_service AS USER_SOCIAL_SERVICE FROM users_social_login USR_SOC_LOG JOIN users USR  ON USR.id = USR_SOC_LOG.user_id WHERE USR_SOC_LOG.unique_id = @uniqueId AND USR.account_id = @accountId";
        public const String USER_BEACON_OFFER_DETAILS = "SELECT " + USER_ALIAS + ", " + STORE_ALIAS_MINIMAL + ", " + USER_BEACON_ALIAS + ", " + OFFER_ALIAS + ", " + USER_OFFER_CODE_ALIAS + " FROM users USR JOIN users_beacons USER_BEACON ON USER_BEACON.user_id = USR.id JOIN Beacons BEACON ON BEACON.id = USER_BEACON.beacon_id JOIN Stores STORE ON STORE.id = BEACON.store_id AND USER_BEACON.beacon_id = BEACON_ID JOIN Offers OFFER ON OFFER.id = USER_BEACON.offer_id JOIN users_offer_codes USER_OFFER_CODE ON USER_OFFER_CODE.offer_id = USER_BEACON.offer_id AND USER_OFFER_CODE.user_id = USER_BEACON.user_id WHERE USR.account_id = @accountId AND USR.type = 1 AND USER_OFFER_CODE.code = @code";
        public const String USER_KIOSK_OFFER_DETAILS = "SELECT " + USER_ALIAS + ", " + STORE_ALIAS_MINIMAL + ", " + USER_KIOSK_ALIAS + ", " + OFFER_ALIAS + ", " + USER_OFFER_CODE_ALIAS + " FROM users USR JOIN users_kiosks USER_KIOSK ON USER_KIOSK.user_id = USR.id JOIN kiosks KIOSK ON KIOSK.id = USER_KIOSK.kiosk_id JOIN Stores STORE ON STORE.id = KIOSK.store_id AND USER_KIOSK.kiosk_id = KIOSK_ID JOIN Offers OFFER ON OFFER.id = USER_KIOSK.offer_id JOIN users_offer_codes USER_OFFER_CODE ON USER_OFFER_CODE.offer_id = USER_KIOSK.offer_id AND USER_OFFER_CODE.user_id = USER_KIOSK.user_id WHERE USR.account_id = @accountId AND USR.type = 1 AND USER_OFFER_CODE.code = @code";
        public const String USER_OFFER_DETAILS = USER_BEACON_OFFER_DETAILS + " UNION " + USER_KIOSK_OFFER_DETAILS;
        public const String ALL_USERS_BEACON_OFFER_DETAILS = "SELECT " + USER_ALIAS + ", " + STORE_ALIAS_MINIMAL + ", " + USER_BEACON_ALIAS + ", " + OFFER_ALIAS + ", " + USER_OFFER_CODE_ALIAS + " FROM users USR JOIN users_beacons USER_BEACON ON USER_BEACON.user_id = USR.id JOIN Beacons BEACON ON BEACON.id = USER_BEACON.beacon_id JOIN Stores STORE ON STORE.id = BEACON.store_id AND USER_BEACON.beacon_id = BEACON_ID JOIN Offers OFFER ON OFFER.id = USER_BEACON.offer_id JOIN users_offer_codes USER_OFFER_CODE ON USER_OFFER_CODE.offer_id = USER_BEACON.offer_id AND USER_OFFER_CODE.user_id = USER_BEACON.user_id WHERE USR.type = 1 AND USER_OFFER_CODE.status = @status AND USER_BEACON.is_availed = 1 AND USR.account_id = @accountId";
        public const String ALL_USERS_KIOSK_OFFER_DETAILS = "SELECT " + USER_ALIAS + ", " + STORE_ALIAS_MINIMAL + ", " + USER_KIOSK_ALIAS + ", " + OFFER_ALIAS + ", " + USER_OFFER_CODE_ALIAS + " FROM users USR JOIN users_kiosks USER_KIOSK ON USER_KIOSK.user_id = USR.id JOIN kiosks KIOSK ON KIOSK.id = USER_KIOSK.kiosk_id JOIN Stores STORE ON STORE.id = KIOSK.store_id AND USER_KIOSK.kiosk_id = KIOSK_ID JOIN Offers OFFER ON OFFER.id = USER_KIOSK.offer_id JOIN users_offer_codes USER_OFFER_CODE ON USER_OFFER_CODE.offer_id = USER_KIOSK.offer_id AND USER_OFFER_CODE.user_id = USER_KIOSK.user_id WHERE USR.type = 1 AND USER_OFFER_CODE.status = @status AND USR.account_id = @accountId";
        public const String ALL_USERS_OFFER_DETAILS = ALL_USERS_BEACON_OFFER_DETAILS + " UNION " + ALL_USERS_KIOSK_OFFER_DETAILS;
        public const String ALL_USERS_FOR_NOTIFICATION = REGISTERED_USERS_FROM_BEACON + "  UNION " + REGISTERED_USERS_FROM_KIOSK + " UNION " + TOTAL_ANNONYMOUS_USERS + " ORDER BY USER_LAST_ACTIVITY DESC";

        //USER_DEVICE ALIAS
        public const String USER_DEVICE_ID = "USER_DEVICE_ID";
        public const String USER_DEVICE_PUSH_ID = "USER_DEVICE_PUSH_ID";
        public const String USER_DEVICE_TYPE = "USER_DEVICE_TYPE";
        public const String USER_DEVICE_CREATED_ON = "USER_DEVICE_CREATED_ON";
        public const String USER_DEVICE_UPDATED_ON = "USER_DEVICE_UPDATED_ON";
        public const String USER_DEVICE_ALIAS = "DEVICE.id AS " + USER_DEVICE_ID + ", DEVICE.type AS " + USER_DEVICE_TYPE + ", DEVICE.push_id AS " + USER_DEVICE_PUSH_ID + ", DEVICE.created_on AS " + USER_DEVICE_CREATED_ON + ", DEVICE.updated_on AS " + USER_DEVICE_UPDATED_ON;
        public const String DEVICES_FOR_USER = "SELECT " + USER_DEVICE_ALIAS + " FROM devices DEVICE WHERE DEVICE.user_id = @userId";


        //USER_OFFER_CODE ALIAS
        public const String CODE_OFFERID = "CODE_OFFERID";
        public const String CODE_USERID= "CODE_USERID";
        public const String OFFER_CODE = "OFFER_CODE";
        public const String OFFER_STATUS = "OFFER_STATUS";
        public const String OFFER_STATUS_DATE = "OFFER_STATUS_DATE";
        public const String USER_OFFER_CODE_ALIAS = "USER_OFFER_CODE.code AS " + OFFER_CODE + ", USER_OFFER_CODE.status AS " + OFFER_STATUS + ", USER_OFFER_CODE.date AS " + OFFER_STATUS_DATE;

        //CONTACTS ALIAS
        public const String CONTACT_ID = "CONTACT_ID";
        public const String CONTACT_COUNTRY = "CONTACT_COUNTRY";
        public const String CONTACT_TYPE = "CONTACT_TYPE";
        public const String CONTACT_VALUE = "CONTACT_VALUE";
        public const String CONTACT_CREATED_ON = "CONTACT_CREATED_ON";
        public const String CONTACT_UPDATED_ON = "CONTACT_UPDATED_ON";
        public const String CONTACT_ALIAS = "CONTACT.id AS " + CONTACT_ID + ", CONTACT.type AS " + CONTACT_TYPE + ", CONTACT.value AS " + CONTACT_VALUE;
        public const String CONTACTS_FOR_USER = "IF EXISTS (SELECT * FROM contacts CONTACT JOIN users_contacts USER_CONTACT ON USER_CONTACT.contact_id = CONTACT.id WHERE  USER_CONTACT.user_id = @userId AND CONTACT.country_id IS NOT NULL)" + CONTACTS_WITH_COUNTRY + " ELSE " + CONTACTS_WITHOUT_COUNTRY;
        public const String CONTACTS_WITH_COUNTRY = "SELECT " + CONTACT_ALIAS + ", " + COUNTRY_ALIAS1 + " FROM contacts CONTACT JOIN users_contacts USER_CONTACT ON USER_CONTACT.contact_id = CONTACT.id JOIN Countries COUNTRY ON COUNTRY.id = CONTACT.country_id WHERE USER_CONTACT.user_id = @userId";
        public const String CONTACTS_WITHOUT_COUNTRY = "SELECT " + CONTACT_ALIAS + " FROM contacts CONTACT JOIN users_contacts USER_CONTACT ON USER_CONTACT.contact_id = CONTACT.id WHERE USER_CONTACT.user_id = @userId";
        public const String CONTACTS_FOR_ACCOUNT = "SELECT " + CONTACT_ALIAS + " FROM contacts CONTACT JOIN accounts_contacts ACCOUNT_CONTACT ON ACCOUNT_CONTACT.contact_id = CONTACT.id WHERE ACCOUNT_CONTACT.account_id = @accountId";
        //ACCOUNT ALIAS
        public const String ACCOUNT_ID = "ACCOUNT_ID";
        public const String ACCOUNT_NAME = "ACCOUNT_NAME";
        public const String ACCOUNT_USER_NAME = "ACCOUNT_USER_NAME";
        public const String ACCOUNT_PASSWORD = "ACCOUNT_PASSWORD";
        public const String ACCOUNT_ADMIN = "ACCOUNT_ADMIN";
        public const String ACCOUNT_PIC_URL = "ACCOUNT_PIC_URL";
        public const String ACCOUNT_WEBSITE = "ACCOUNT_WEBSITE";
        public const String ACCOUNT_CREATED_ON = "ACCOUNT_CREATED_ON";
        public const String ACCOUNT_UPDATED_ON = "ACCOUNT_UPDATED_ON";
        public const String ACCOUNT_ALIAS = "ACCOUNT.id AS " + ACCOUNT_ID + ", ACCOUNT.name AS " + ACCOUNT_NAME + ", ACCOUNT.pic_url AS " + ACCOUNT_PIC_URL + ", ACCOUNT.created_on AS " + ACCOUNT_CREATED_ON + ", ACCOUNT.updated_on AS " + ACCOUNT_UPDATED_ON;
        public const String ACCOUNT_DETAILS = "SELECT " + ACCOUNT_ALIAS + ", USR.contact_id AS ACCOUNT_ADMIN_CONTACT, USR.contact_id AS CONTACT_ID, CONTACT.type AS CONTACT_TYPE, CONTACT.value AS CONTACT_VALUE FROM users USR JOIN Accounts ACCOUNT ON ACCOUNT.id = USR.account_id JOIN contacts CONTACT ON CONTACT.id = USR.contact_id   WHERE USR.username IS NOT NULL AND USR.password IS NOT NULL AND USR.account_id = @accountId";
        public const String GET_ACCOUNT_PASSWORD = "SELECT USR.username AS USER_NAME, USR.password AS PASSWORD FROM contacts CONTACT join users USR ON USR.contact_id = CONTACT.id WHERE CONTACT.value = @emailId AND USR.username IS NOT NULL AND USR.password IS NOT NULL";
        public const String ACCOUNT_UPDATE = "UPDATE Accounts set name = @name, pic_url = @picUrl, updated_on = @updatedOn where id = @accountId";                 


        // MASTER DASHBOARD
        public const String TOTAL_MEMBERS_COUNT = "SELECT COUNT(USR.id) AS USER_COUNT FROM users USR WHERE USR.type = 1 and USR.account_id = @accountId";
        public const String ANONYMOUS_USERS_COUNT = "SELECT COUNT (USR.id) AS USER_COUNT FROM users USR WHERE USR.account_id = @accountId AND USR.type = 2";
        public const String LAST_MONTH_ACTIVE_MEMBERS_COUNT = "SELECT COUNT(*) AS USER_COUNT FROM (SELECT DISTINCT USR.id AS USER_ID FROM users USR JOIN users_beacons USER_BEACON ON USER_BEACON.user_id = USR.id WHERE USR.account_id = @accountId AND DATEPART(mm, USER_BEACON.date) = DATEPART(mm, DATEADD(mm, -1, @currentDateTime)) AND DATEPART(yy, USER_BEACON.date) = DATEPART(yy, DATEADD(mm, -1, @currentDateTime)) AND USR.type = 1 UNION SELECT DISTINCT USR.id AS USER_ID FROM users USR JOIN users_kiosks USER_KIOSK ON USER_KIOSK.user_id = USR.id WHERE USR.account_id = @accountId AND DATEPART(mm, USER_KIOSK.date) = DATEPART(mm, DATEADD(mm, -1, @currentDateTime)) AND DATEPART(yy, USER_KIOSK.date) = DATEPART(yy, DATEADD(mm, -1, @currentDateTime)) AND USR.type = 1) AS USER_COUNT";
        public const String POPULER_OFFER = "SELECT TOP 1 USER_OFFER_CODE.offer_id AS OFFER_ID, COUNT (USER_OFFER_CODE.offer_id) AS OFFER_COUNT FROM users_offer_codes USER_OFFER_CODE JOIN Offers OFFER ON OFFER.id = USER_OFFER_CODE.offer_id WHERE OFFER.account_id = @accountId GROUP BY USER_OFFER_CODE.offer_id ORDER BY OFFER_COUNT DESC";
        public const String POPULAR_BEACON = "SELECT TOP 1 USER_BEACON.beacon_id as BEACON_ID, SUM (USER_BEACON.count) AS VIEW_COUNT FROM users_beacons USER_BEACON JOIN Beacons BEACON ON BEACON.id = USER_BEACON.beacon_id JOIN Stores STORE ON STORE.id = BEACON.store_id WHERE STORE.account_id = @accountId GROUP BY USER_BEACON.beacon_id ORDER BY VIEW_COUNT DESC ";
        public const String POPULAR_KIOSK = "SELECT TOP 1 USER_KIOSK.kiosk_id AS KIOSK_ID, SUM (USER_KIOSK.count) AS VIEW_COUNT FROM users_kiosks USER_KIOSK JOIN Kiosks KIOSK ON KIOSK.id = USER_KIOSK.kiosk_id JOIN Stores STORE ON STORE.id = KIOSK.store_id WHERE STORE.account_id = @accountId GROUP BY USER_KIOSK.kiosk_id ORDER BY VIEW_COUNT DESC";        
        public const String AVAILED_OFFERS_COUNT = "SELECT COUNT(DISTINCT OFFER.id) AS AVAILED_OFFER_COUNT FROM Offers OFFER JOIN users_offer_codes USER_OFFER_CODE ON USER_OFFER_CODE.offer_id = OFFER.id WHERE OFFER.account_id = @accountId";
        public const String IGNORED_OFFERS_COUNT = "SELECT COUNT(*) AS IGNORED_OFFER_COUNT FROM (SELECT DISTINCT OFFER.id AS OFFER_ID FROM Offers OFFER JOIN users_beacons USER_BEACON ON USER_BEACON.offer_id = OFFER.id WHERE USER_BEACON.is_availed = 0 AND OFFER.account_id = @accountId UNION SELECT DISTINCT OFFER.id AS OFFER_ID FROM Offers OFFER JOIN users_kiosks USER_KIOSK ON USER_KIOSK.offer_id = OFFER.id WHERE USER_KIOSK.is_availed = 0 AND OFFER.account_id = @accountId) AS IGNORED_OFFER_COUNT";
        public const String REGISTERED_USERS_FROM_BEACONS_BY_MONTH = "SELECT users_beacons.user_id, users_beacons.date AS _DATE, DATEPART(mm,users_beacons.date) AS _MONTH, DATEPART(yy,users_beacons.date) AS _YEAR FROM users JOIN users_beacons ON users_beacons.user_id = users.id WHERE users_beacons.is_registered = 1 AND users.account_id = @accountId AND users.type = 1";
        public const String REGISTERED_USERS_FROM_KIOSKS_BY_MONTH = "SELECT users_kiosks.user_id, users_kiosks.date AS _DATE, DATEPART(mm,users_kiosks.date) AS _MONTH, DATEPART(yy,users_kiosks.date) AS _YEAR FROM users JOIN users_kiosks ON users_kiosks.user_id = users.id WHERE users_kiosks.is_registered = 1  AND users.account_id = @accountId AND users.type = 1";
        public const String TOTAL_REGISTERED_USERS_BY_MONTH = "SELECT Count(user_id) AS USER_COUNT, _MONTH, _YEAR FROM (" + REGISTERED_USERS_FROM_BEACONS_BY_MONTH + " UNION " + REGISTERED_USERS_FROM_KIOSKS_BY_MONTH + ") AS USER_COUNT WHERE _DATE BETWEEN @_START AND @_END GROUP BY _YEAR, _MONTH ORDER BY _YEAR DESC, _MONTH DESC";
        public const String USER_OFFER_CATEGORIES_FROM_BEACONS_BY_MONTH = "SELECT users_beacons.user_id, users_beacons.date AS _DATE, DATEPART(mm,users_beacons.date) AS _MONTH, DATEPART(yy,users_beacons.date) AS _YEAR, Categories.id AS CATEGORY_ID FROM users JOIN users_beacons ON users_beacons.user_id = users.id JOIN Offers_Categories ON Offers_Categories.offer_id = users_beacons.offer_id JOIN Categories ON Categories.id = Offers_Categories.category_id WHERE users_beacons.is_availed = 1 AND users.account_id = @accountId AND users.type = 1";
        public const String USER_OFFER_CATEGORIES_FROM_KIOSKS_BY_MONTH = "SELECT users_kiosks.user_id, users_kiosks.date AS _DATE, DATEPART(mm,users_kiosks.date) AS _MONTH, DATEPART(yy,users_kiosks.date) AS _YEAR, Categories.id AS CATEGORY_ID FROM users JOIN users_kiosks ON users_kiosks.user_id = users.id JOIN Offers_Categories ON Offers_Categories.offer_id = users_kiosks.offer_id JOIN Categories ON Categories.id = Offers_Categories.category_id WHERE users_kiosks.is_availed = 1 AND users.account_id = @accountId AND users.type = 1";
        public const String TOTAL_USER_OFFER_CATEGORIES_BY_MONTH = "SELECT Count(user_id) AS USER_COUNT, _MONTH, _YEAR, CATEGORY_ID FROM (" + USER_OFFER_CATEGORIES_FROM_BEACONS_BY_MONTH + " UNION " + USER_OFFER_CATEGORIES_FROM_KIOSKS_BY_MONTH + ") AS USER_COUNT WHERE _DATE BETWEEN @_START AND @_END GROUP BY _MONTH, _YEAR, CATEGORY_ID ORDER BY _MONTH DESC, _YEAR DESC";

        //STORE DASHBOARD
        public const String STORE_BEACON_COUNT = "SELECT COUNT (BEACON.id) AS BEACON_COUNT FROM Beacons BEACON JOIN Stores STORE ON BEACON.store_id = STORE.id WHERE STORE.id = @storeId";
        public const String STORE_KIOSK_COUNT = "SELECT COUNT (KIOSK.id) AS KIOSK_COUNT FROM Kiosks KIOSK JOIN Stores STORE ON KIOSK.store_id = STORE.id WHERE STORE.id  = @storeId";
        public const String STORE_BEACON_MEMBERS = "SELECT COUNT (USR.id) AS MEMBER_COUNT FROM users USR JOIN users_beacons USER_BEACON ON USER_BEACON.user_id = USR.id JOIN Beacons BEACON ON BEACON.id = USER_BEACON.beacon_id JOIN Stores STORE ON STORE.id = BEACON.store_id WHERE USR.type = 1 AND USER_BEACON.is_registered = 1 AND STORE.id = @storeId";
        public const String STORE_KIOSK_MEMBERS = "SELECT COUNT (USR.id) AS MEMBER_COUNT FROM users USR JOIN users_kiosks USER_KIOSK ON USER_KIOSK.user_id = USR.id JOIN Kiosks KIOSK ON KIOSK.id = USER_KIOSK.kiosk_id JOIN Stores STORE ON STORE.id = KIOSK.store_id WHERE USR.type = 1 AND USER_KIOSK.is_registered = 1 AND STORE.id = @storeId";
        public const String TOTAL_STORE_MEMBERS = "SELECT (" + STORE_BEACON_MEMBERS + ") + (" + STORE_KIOSK_MEMBERS + ") AS MEMBER_COUNT";
        public const String TOTAL_NON_MEMBERS = "SELECT COUNT (DISTINCT USR.id) AS NON_MEMBER_COUNT FROM users USR JOIN users_beacons USER_BEACON ON USER_BEACON.user_id = USR.id JOIN Beacons BEACON ON BEACON.id = USER_BEACON.beacon_id JOIN Stores STORE ON STORE.id = BEACON.store_id WHERE USR.type = 2 AND STORE.id = @storeId";
        public const String POPULAR_STORE_BEACON = "SELECT TOP 1 USER_BEACON.beacon_id as BEACON_ID, SUM (USER_BEACON.count) AS VIEW_COUNT FROM users_beacons USER_BEACON JOIN Beacons BEACON ON BEACON.id = USER_BEACON.beacon_id JOIN Stores STORE ON STORE.id = BEACON.store_id WHERE STORE.id = @storeId GROUP BY USER_BEACON.beacon_id ORDER BY VIEW_COUNT DESC";
        public const String POPULAR_STORE_KIOSK = "SELECT TOP 1 USER_KIOSK.kiosk_id AS KIOSK_ID, SUM (USER_KIOSK.count) AS VIEW_COUNT FROM users_kiosks USER_KIOSK JOIN Kiosks KIOSK ON KIOSK.id = USER_KIOSK.kiosk_id JOIN Stores STORE ON STORE.id = KIOSK.store_id WHERE STORE.id = @storeId GROUP BY USER_KIOSK.kiosk_id ORDER BY VIEW_COUNT DESC";
        public const String POPULAR_BEACON_OFFER = "SELECT USER_BEACON.user_id, USER_BEACON.beacon_id AS DEVICE_ID, 1 AS DEVICE_TYPE, USER_BEACON.offer_id AS OFFER_ID FROM users_beacons USER_BEACON JOIN Beacons BEACON ON BEACON.id = USER_BEACON.beacon_id JOIN Stores STORE ON STORE.id = BEACON.store_id WHERE USER_BEACON.is_availed = 1 AND STORE.id = @storeId";
        public const String POPULAR_KIOSK_OFFER = "SELECT USER_KIOSK.user_id, USER_KIOSK.kiosk_id AS DEVICE_ID, 2 AS DEVICE_TYPE, USER_KIOSK.offer_id AS OFFER_ID FROM users_kiosks USER_KIOSK JOIN Kiosks KIOSK ON KIOSK.id = USER_KIOSK.kiosk_id JOIN Stores STORE ON STORE.id = KIOSK.store_id WHERE USER_KIOSK.is_availed = 1 AND STORE.id = @storeId";
        public const String POPULAR_STORE_OFFER = "SELECT TOP 1 COUNT (user_id) AS USER_COUNT, OFFER_ID FROM (" + POPULAR_BEACON_OFFER + " UNION " + POPULAR_KIOSK_OFFER + ") AS USER_COUNT GROUP BY OFFER_ID ORDER BY USER_COUNT DESC";
        public const String REGISTERED_USERS_FROM_STORE_BEACONS_BY_MONTH = "SELECT users_beacons.user_id, users_beacons.date AS _DATE, DATEPART(mm,users_beacons.date) AS _MONTH, DATEPART(yy,users_beacons.date) AS _YEAR FROM users_beacons JOIN Beacons ON Beacons.id = users_beacons.beacon_id JOIN Stores ON Stores.id = Beacons.store_id WHERE users_beacons.is_registered = 1 and Stores.id = @storeId";
        public const String REGISTERED_USERS_FROM_STORE_KIOSKS_BY_MONTH = "SELECT users_kiosks.user_id, users_kiosks.date AS _DATE, DATEPART(mm,users_kiosks.date) AS _MONTH, DATEPART(yy,users_kiosks.date) AS _YEAR FROM users_kiosks JOIN Kiosks ON Kiosks.id = users_kiosks.kiosk_id JOIN Stores ON Stores.id = Kiosks.store_id WHERE users_kiosks.is_registered = 1 and Stores.id = @storeId";
        public const String TOTAL_REGISTERED_USERS_FROM_STORE_BY_MONTH = "SELECT Count(user_id) AS USER_COUNT, _MONTH, _YEAR FROM (" + REGISTERED_USERS_FROM_STORE_BEACONS_BY_MONTH + " UNION " + REGISTERED_USERS_FROM_STORE_KIOSKS_BY_MONTH + ") AS USER_COUNT WHERE _DATE BETWEEN @_START AND @_END GROUP BY _MONTH, _YEAR ORDER BY _MONTH DESC, _YEAR DESC";
        public const String USER_OFFER_CATEGORIES_FROM_STORE_BEACONS_BY_MONTH = "SELECT users_beacons.user_id, users_beacons.date AS _DATE, DATEPART(mm,users_beacons.date) AS _MONTH, DATEPART(yy,users_beacons.date) AS _YEAR, Categories.id AS CATEGORY_ID FROM users_beacons JOIN Beacons ON Beacons.id = users_beacons.beacon_id JOIN Stores ON Stores.id = Beacons.store_id JOIN Offers_Categories ON Offers_Categories.offer_id = users_beacons.offer_id JOIN Categories ON Categories.id = Offers_Categories.category_id WHERE users_beacons.is_availed = 1 AND Stores.id = @storeId";
        public const String USER_OFFER_CATEGORIES_FROM_STORE_KIOSKS_BY_MONTH = "SELECT users_kiosks.user_id, users_kiosks.date AS _DATE, DATEPART(mm,users_kiosks.date) AS _MONTH, DATEPART(yy,users_kiosks.date) AS _YEAR, Categories.id AS CATEGORY_ID FROM users_kiosks JOIN Kiosks ON Kiosks.id = users_kiosks.kiosk_id JOIN Stores on Stores.id = Kiosks.store_id JOIN Offers_Categories ON Offers_Categories.offer_id = users_kiosks.offer_id JOIN Categories ON Categories.id = Offers_Categories.category_id WHERE users_kiosks.is_availed = 1 AND Stores.id = @storeId";
        public const String TOTAL_USER_OFFER_CATEGORIES_FROM_STORE_BY_MONTH = "SELECT Count(user_id) AS USER_COUNT, _MONTH, _YEAR, CATEGORY_ID FROM (" + USER_OFFER_CATEGORIES_FROM_STORE_BEACONS_BY_MONTH + " UNION " + USER_OFFER_CATEGORIES_FROM_STORE_KIOSKS_BY_MONTH + ") AS USER_COUNT WHERE _DATE BETWEEN @_START AND @_END GROUP BY _MONTH, _YEAR, CATEGORY_ID ORDER BY _MONTH DESC, _YEAR DESC";

        //BEACON DASHBOARD
        public const String BEACON_TOTAL_USERS = "SELECT COUNT (DISTINCT USERS.id) AS TOTAL_USERS FROM users USERS JOIN users_beacons USER_BEACON  ON USER_BEACON.user_id = USERS.id WHERE USER_BEACON.beacon_id = @beaconId";
        public const String BEACON_MEMBERS = "SELECT COUNT (DISTINCT USERS.id) AS MEMBERS FROM users USERS JOIN users_beacons USER_BEACON  ON USER_BEACON.user_id = USERS.id WHERE USERS.type = 1 AND USER_BEACON.beacon_id = @beaconId";
        public const String BEACON_NON_MEMBERS = "SELECT COUNT (DISTINCT USERS.id) AS NON_MEMBERS FROM users USERS JOIN users_beacons USER_BEACON  ON USER_BEACON.user_id = USERS.id WHERE USERS.type = 2 AND USER_BEACON.beacon_id = @beaconId";
        public const String BEACON_POPULAR_OFFER = "SELECT TOP 1 USER_BEACON.offer_id AS OFFER_ID, COUNT (USER_BEACON.offer_id) AS OFFER_COUNT FROM users_beacons USER_BEACON WHERE USER_BEACON.is_availed = 1 AND USER_BEACON.beacon_id = @beaconId GROUP BY USER_BEACON.offer_id ORDER BY OFFER_COUNT DESC";
        public const String REGISTERED_USERS_FROM_BEACON_BY_MONTH = "SELECT Count(user_id) AS USER_COUNT, _MONTH, _YEAR FROM (SELECT users_beacons.user_id, users_beacons.date AS _DATE, DATEPART(mm,users_beacons.date) AS _MONTH, DATEPART(yy,users_beacons.date) AS _YEAR FROM users JOIN users_beacons ON users_beacons.user_id = users.id WHERE users_beacons.is_registered = 1 AND users_beacons.beacon_id = @beaconId AND users.type = 1) AS USER_COUNT WHERE _DATE BETWEEN @_START AND @_END GROUP BY _YEAR, _MONTH ORDER BY _YEAR DESC, _MONTH DESC";
        public const String USER_COUNT_BY_OFFER_CATEGORIES_FROM_BEACON = "SELECT Count(user_id) AS USER_COUNT, _MONTH, _YEAR, CATEGORY_ID FROM (SELECT users_beacons.user_id, users_beacons.date AS _DATE, DATEPART(mm,users_beacons.date) AS _MONTH, DATEPART(yy,users_beacons.date) AS _YEAR, Categories.id AS CATEGORY_ID FROM users JOIN users_beacons ON users_beacons.user_id = users.id JOIN Offers_Categories ON Offers_Categories.offer_id = users_beacons.offer_id JOIN Categories ON Categories.id = Offers_Categories.category_id WHERE users_beacons.is_availed = 1 AND users_beacons.beacon_id = @beaconId AND users.type = 1) AS USER_COUNT WHERE _DATE BETWEEN @_START AND @_END GROUP BY _MONTH, _YEAR, CATEGORY_ID ORDER BY _MONTH DESC, _YEAR DESC";

        //KIOSK DASHBOARD
        public const String KIOSK_REGISTERED_USERS = "SELECT COUNT (USER_KIOSK.user_id) AS REGISTERED_USERS FROM users_kiosks USER_KIOSK WHERE USER_KIOSK.is_registered = 1 AND USER_KIOSK.kiosk_id = @kioskId";
        public const String KIOSK_POPULAR_OFFER = "SELECT TOP 1 USER_KIOSK.offer_id AS OFFER_ID, COUNT (USER_KIOSK.offer_id) AS OFFER_COUNT FROM users_kiosks USER_KIOSK WHERE USER_KIOSK.kiosk_id = @kioskId AND USER_KIOSK.is_availed = 1 GROUP BY USER_KIOSK.offer_id ORDER BY OFFER_COUNT DESC";
        public const String REGISTERED_USERS_FROM_KIOSK_BY_MONTH = "SELECT Count(user_id) AS USER_COUNT, _MONTH, _YEAR FROM (SELECT users_kiosks.user_id, users_kiosks.date AS _DATE, DATEPART(mm,users_kiosks.date) AS _MONTH, DATEPART(yy,users_kiosks.date) AS _YEAR FROM users JOIN users_kiosks ON users_kiosks.user_id = users.id WHERE users_kiosks.is_registered = 1  AND users_kiosks.kiosk_id = @kioskId AND users.type = 1) AS USER_COUNT WHERE _DATE BETWEEN @_START AND @_END GROUP BY _MONTH, _YEAR ORDER BY _MONTH DESC, _YEAR DESC";
        public const String USER_COUNT_BY_OFFER_CATEGORIES_FROM_KIOSK = "SELECT Count(user_id) AS USER_COUNT, _MONTH, _YEAR, CATEGORY_ID FROM (SELECT users_kiosks.user_id, users_kiosks.date AS _DATE, DATEPART(mm,users_kiosks.date) AS _MONTH, DATEPART(yy,users_kiosks.date) AS _YEAR, Categories.id AS CATEGORY_ID FROM users JOIN users_kiosks ON users_kiosks.user_id = users.id JOIN Offers_Categories ON Offers_Categories.offer_id = users_kiosks.offer_id JOIN Categories ON Categories.id = Offers_Categories.category_id WHERE users_kiosks.is_availed = 1 AND users_kiosks.kiosk_id = @kioskId AND users.type = 1) AS USER_COUNT WHERE _DATE BETWEEN @_START AND @_END GROUP BY _MONTH, _YEAR, CATEGORY_ID ORDER BY _MONTH DESC, _YEAR DESC";

        //OFFER DASHBOARD
        public const String REGISTERED_USERS = "SELECT ((SELECT COUNT(USER_KIOSK.user_id) FROM users_kiosks USER_KIOSK WHERE USER_KIOSK.offer_id = @offerId AND USER_KIOSK.is_registered = 1) + (SELECT COUNT(USER_BEACON.user_id) FROM users_beacons USER_BEACON WHERE USER_BEACON.offer_id = @offerId AND USER_BEACON.is_registered = 1)) AS REGISTERED_USERS";
        public const String OFFER_SEEN1 = "SELECT IsNull(SUM(USER_KIOSK.count),0) FROM users_kiosks USER_KIOSK WHERE USER_KIOSK.offer_id = @offerId";
        public const String OFFER_SEEN2 = "SELECT ISNULL(SUM(USER_BEACON.count),0) FROM users_beacons USER_BEACON WHERE USER_BEACON.offer_id = @offerId";
        public const String OFFER_SEEN = "SELECT (" + OFFER_SEEN1 + ") + (" + OFFER_SEEN2 + ") AS OFFER_SEEN";
        public const String OFFER_AVAILED = "SELECT COUNT(USER_OFFER_CODE.user_id) AS OFFER_AVAILED FROM users_offer_codes USER_OFFER_CODE WHERE USER_OFFER_CODE.offer_id = @offerId";
        public const String BEACON_COUNT = "SELECT COUNT( DISTINCT BEACON_OFFER.beacon_id) AS BEACON_COUNT FROM Beacons_Offers BEACON_OFFER JOIN Beacons BEACON ON BEACON_OFFER.beacon_id = BEACON.id JOIN Offers OFFER ON BEACON_OFFER.offer_id = OFFER.id WHERE OFFER.end_time >= @currentDateTime AND BEACON.is_workable = 1 AND OFFER.id = @offerId";
        public const String KIOSK_COUNT = "SELECT COUNT( DISTINCT KIOSK_LUCKY_ITEM_OFFER.kiosk_id) AS KIOSK_COUNT FROM Kiosks_Lucky_Items_Offers KIOSK_LUCKY_ITEM_OFFER JOIN Kiosks KIOSK ON KIOSK_LUCKY_ITEM_OFFER.kiosk_id = KIOSK.id JOIN Offers OFFER ON KIOSK_LUCKY_ITEM_OFFER.offer_id = OFFER.id WHERE OFFER.end_time >= @currentDateTime AND KIOSK.is_workable = 1 AND OFFER.id = @offerId";
        public const String BEACON_OFFER_AVAILED_BY = "SELECT " + USER_ALIAS_MINIMAL + ", " + STORE_ALIAS_MINIMAL + ", " + USER_BEACON_ALIAS + " FROM users USR JOIN users_beacons USER_BEACON ON USER_BEACON.user_id = USR.id JOIN Beacons BEACON ON BEACON.id = USER_BEACON.beacon_id JOIN Stores STORE ON STORE.id = BEACON.store_id AND USER_BEACON.beacon_id = BEACON_ID WHERE USR.type = 1 AND USER_BEACON.offer_id = @offerId AND USER_BEACON.is_availed = 1";
        public const String KIOSK_OFFER_AVAILED_BY = "SELECT " + USER_ALIAS_MINIMAL + ", " + STORE_ALIAS_MINIMAL + ", " + USER_KIOSK_ALIAS + " FROM users USR JOIN users_kiosks USER_KIOSK ON USER_KIOSK.user_id = USR.id JOIN kiosks KIOSK ON KIOSK.id = USER_KIOSK.kiosk_id JOIN Stores STORE ON STORE.id = KIOSK.store_id AND USER_KIOSK.kiosk_id = KIOSK_ID WHERE USR.type = 1 AND USER_KIOSK.offer_id = @offerId AND USER_KIOSK.is_availed = 1";
        public const String OFFER_AVAILED_BY = BEACON_OFFER_AVAILED_BY + " UNION " + KIOSK_OFFER_AVAILED_BY + " ORDER BY USER_DATE DESC";
        public const String OFFER_AVAILED_COUNT = "SELECT COUNT(USER_OFFER_CODE.user_id) AS OFFER_AVAILED_COUNT FROM users_offer_codes USER_OFFER_CODE WHERE USER_OFFER_CODE.offer_id = @offerId";
        public const String OFFER_IGNORED_FROM_BEACON1 = "(SELECT ISNULL(SUM(USER_BEACON.count),0) FROM Offers OFFER JOIN users_beacons USER_BEACON ON USER_BEACON.offer_id = OFFER.id WHERE USER_BEACON.is_availed = 0 AND OFFER.id = @offerId)";
        public const String OFFER_IGNORED_FROM_BEACON2 = "(SELECT ISNULL(SUM(USER_BEACON.count - 1),0) FROM Offers OFFER JOIN users_beacons USER_BEACON ON USER_BEACON.offer_id = OFFER.id WHERE USER_BEACON.is_availed = 1 AND OFFER.id = @offerId)";
        public const String OFFER_IGNORED_FROM_KIOSK1 = "(SELECT ISNULL(SUM(USER_KIOSK.count),0) FROM Offers OFFER JOIN users_kiosks USER_KIOSK ON USER_KIOSK.offer_id = OFFER.id WHERE USER_KIOSK.is_availed = 0 AND OFFER.id = @offerId)";
        public const String OFFER_IGNORED_FROM_KIOSK2 = "(SELECT ISNULL(SUM(USER_KIOSK.count - 1),0) FROM Offers OFFER JOIN users_kiosks USER_KIOSK ON USER_KIOSK.offer_id = OFFER.id WHERE USER_KIOSK.is_availed = 1 AND OFFER.id = @offerId)";
        public const String OFFER_IGNORED_COUNT = "SELECT " + OFFER_IGNORED_FROM_BEACON1 + " + " + OFFER_IGNORED_FROM_BEACON2 + " + " + OFFER_IGNORED_FROM_KIOSK1 + " + " + OFFER_IGNORED_FROM_KIOSK2 + " AS OFFER_IGNORED_COUNT";
        public const String REGISTERED_USERS_FROM_BEACONS_OFFERS_BY_MONTH = "SELECT users_beacons.user_id, users_beacons.date AS _DATE, DATEPART(mm,users_beacons.date) AS _MONTH, DATEPART(yy,users_beacons.date) AS _YEAR FROM users JOIN users_beacons ON users_beacons.user_id = users.id WHERE users_beacons.is_registered = 1 AND users_beacons.offer_id = @offerId  AND users.type = 1";
        public const String REGISTERED_USERS_FROM_KIOSKS_OFFERS_BY_MONTH = "SELECT users_kiosks.user_id, users_kiosks.date AS _DATE, DATEPART(mm,users_kiosks.date) AS _MONTH, DATEPART(yy,users_kiosks.date) AS _YEAR FROM users JOIN users_kiosks ON users_kiosks.user_id = users.id WHERE users_kiosks.is_registered = 1  AND users_kiosks.offer_id = @offerId AND users.type = 1";
        public const String TOTAL_REGISTERED_USERS_OFFERS_BY_MONTH = "SELECT Count(user_id) AS USER_COUNT, _MONTH, _YEAR FROM (" + REGISTERED_USERS_FROM_BEACONS_OFFERS_BY_MONTH + " UNION " + REGISTERED_USERS_FROM_KIOSKS_OFFERS_BY_MONTH + ") AS USER_COUNT WHERE _DATE BETWEEN @_START AND @_END GROUP BY _MONTH, _YEAR ORDER BY _MONTH DESC, _YEAR DESC";


        //TERMS AND CONDITIONS
        public const String TERMS_AND_CONDITIONS = "TERMS_AND_CONDITIONS";
        public const String TERMS_AND_CONDITIONS_ID = "TERMS_AND_CONDITIONS_ID";
        public const String TERMS_AND_CONDITIONS_ACCOUNT_ID = "TERMS_AND_CONDITIONS_ACCOUNT_ID";
        public const String TERMS_AND_CONDITIONS_TITLE = "TERMS_AND_CONDITIONS_TITLE";
        public const String TERMS_AND_CONDITIONS_DESCRIPTION = "TERMS_AND_CONDITIONS_DESCRIPTION";
        public const String TERMS_AND_CONDITIONS_CREATED_AT = "TERMS_AND_CONDITIONS_CREATED_AT";
        public const String TERMS_AND_CONDITIONS_UPDATED_AT = "TERMS_AND_CONDITIONS_UPDATED_AT";
        public const String TERMS_AND_CONDITIONS_ALIAS = TERMS_AND_CONDITIONS + ".id AS " + TERMS_AND_CONDITIONS_ID + ", " + TERMS_AND_CONDITIONS + ".account_id AS " + TERMS_AND_CONDITIONS_ACCOUNT_ID + ", " + TERMS_AND_CONDITIONS + ".title AS " + TERMS_AND_CONDITIONS_TITLE + ", " + TERMS_AND_CONDITIONS + ".description AS " + TERMS_AND_CONDITIONS_DESCRIPTION;
        public const String ADD_TERM_CONDITION = "INSERT INTO terms_and_conditions (account_id, title, description, created_at, updated_at) OUTPUT Inserted.id VALUES (@accountId, @title, @description, @created_at, @updated_at)";
        public const String GET_ALL_TERMS_AND_CONDITIONS = "SELECT " + TERMS_AND_CONDITIONS_ALIAS + " FROM terms_and_conditions " + TERMS_AND_CONDITIONS + " WHERE " + TERMS_AND_CONDITIONS + ".account_id = @accountId ORDER BY " + TERMS_AND_CONDITIONS + ".updated_at DESC";
        public const String GET_SINGLE_TERM_AND_CONDITION_BY_ID = "SELECT " + TERMS_AND_CONDITIONS_ALIAS + " FROM terms_and_conditions " + TERMS_AND_CONDITIONS + " WHERE " + TERMS_AND_CONDITIONS + ".id = @id;";
        public const String TERM_AND_CONDITION_FOR_OFFER = "SELECT " + TERMS_AND_CONDITIONS_ALIAS + " FROM terms_and_conditions TERMS_AND_CONDITIONS JOIN offers_terms_and_conditions OFFER_TERM_AND_CONDITION ON OFFER_TERM_AND_CONDITION.terms_and_conditions_id = TERMS_AND_CONDITIONS.id WHERE OFFER_TERM_AND_CONDITION.offer_id = @offerId";

        //SOCIAL NETWORKING SERVICE
        public const String SERVICE_ID = "SERVICE_ID";
        public const String SERVICE_NAME = "SERVICE_NAME";
        public const String SERVICE_IMAGE_URL = "SERVICE_IMAGE_URL";
        public const String SERVICE_ALIAS = "SERVICE.id AS " + SERVICE_ID + ", SERVICE.name AS " + SERVICE_NAME + ", SERVICE.image_url AS " + SERVICE_IMAGE_URL;
        public const String ALL_SERVICES = "SELECT " + SERVICE_ALIAS + " FROM social_networking_services SERVICE ORDER BY SERVICE_ID ASC";
        public const String SELECTED_SERVICES = "SELECT " + SERVICE_ALIAS + " FROM social_networking_services SERVICE JOIN accounts_social_networking_services ACCOUNT_SERVICE ON ACCOUNT_SERVICE.social_networking_service_id = SERVICE.id WHERE ACCOUNT_SERVICE.account_id = @accountId ORDER BY SERVICE_ID ASC";
        public const String UNSELECTED_SERVICES = "SELECT " + SERVICE_ALIAS + " FROM social_networking_services SERVICE WHERE SERVICE.id NOT IN (SELECT ACCOUNT_SERVICE.social_networking_service_id FROM accounts_social_networking_services ACCOUNT_SERVICE WHERE ACCOUNT_SERVICE.account_id = @accountId) ORDER BY SERVICE_ID ASC";
    }
}