﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.Accounts;
using BNS.Models.Beacons;
using BNS.DL.WSDatabase;
using System.Data.SqlClient;
using BNS.Models.Stores;
using BNS.Models.Countries;
using WSDatabase;
using BNS.Models.UsersBeacons;
using System.Web.Configuration;
using BNS.Models.Users;
using BNS.Models.Offers;
using BNS.DAL.CommunicationManager.Passbook;
using BNS.DAL.DALAccount;

namespace BNS.DAL.DALBeacon
{
    public class DALBeaconManager
    {       
        public bool saveBeacon(Beacon beacon)
        {
            using (WSTransaction transaction = new WSTransaction("SAVE_BEACON"))
            {
                String query;

                if (beacon.beacon_uuid != null && (beacon.address !=null && beacon.notification != null && beacon.latitude != 0 && beacon.longitude != 0))
                {
                    query = "INSERT INTO Beacons(beacon_unique_id, name, number, store_id, is_workable, uuid, address, notification, latitude, longitude, created_on, updated_on, created_by, updated_by) OUTPUT Inserted.id  VALUES(@beaconGuid, @name, @number, @storeId, @isWorkable, @beaconUuid, @address, @notification, @latitude, @longitude, @createdOn, @updatedOn, @createdBy, @updatedBy)";
                }
                else if (beacon.beacon_uuid != null && (beacon.address == null || beacon.notification == null || beacon.latitude == 0 || beacon.longitude == 0))
                {
                    query = "INSERT INTO Beacons(beacon_unique_id, name, number, store_id, is_workable, uuid, created_on, updated_on, created_by, updated_by) OUTPUT Inserted.id  VALUES(@beaconGuid, @name, @number, @storeId, @isWorkable, @beaconUuid, @createdOn, @updatedOn, @createdBy, @updatedBy)";
                }
                else if(beacon.beacon_uuid == null && (beacon.address != null && beacon.notification != null && beacon.latitude != 0 && beacon.longitude != 0))
                {
                    query = "INSERT INTO Beacons(beacon_unique_id, name, number, store_id, is_workable, address, notification, latitude, longitude, created_on, updated_on, created_by, updated_by) OUTPUT Inserted.id  VALUES(@beaconGuid, @name, @number, @storeId, @isWorkable, @address, @notification, @latitude, @longitude, @createdOn, @updatedOn, @createdBy, @updatedBy)";
                }
                else
                {
                    query = "INSERT INTO Beacons(beacon_unique_id, name, number, store_id, is_workable, created_on, updated_on, created_by, updated_by) OUTPUT Inserted.id  VALUES(@beaconGuid, @name, @number, @storeId, @isWorkable, @createdOn, @updatedOn, @createdBy, @updatedBy);";
                }
                                
                WSQuery Query = new WSQuery(query, beacon.toDictionary(), transaction);
                beacon.id = WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQueryReturnID(Query);
                if (beacon.id != 0)
                {
                    String beaconUrl = this.getUrl(beacon.beacon_unique_id);
                    beacon.shortUrl = UrlShortener.shortenIt(beaconUrl);
                    if (beacon.shortUrl != null)
                    {
                        if (!this.updateBeaconShortUrl(beacon, transaction))
                        {
                            transaction.rollback();
                            return false;
                        }
                    }

                    if(beacon.beacon_uuid != null || (beacon.address != null && beacon.notification != null && beacon.latitude != 0 && beacon.longitude != 0))
                    {
                        beacon.createdBy.account = new DALAccountManager().getDetails(beacon.createdBy.account);
                        passbookService pass = new passbookService();
                        beacon.passUrl = pass.getPassForBeacon(beacon);
                        if(beacon.passUrl != null)
                        {
                            if (!this.updateBeaconPassUrl(beacon, transaction))
                            {
                                transaction.rollback();
                                return false;
                            }
                        }
                    }
                    List<DefaultOffer> defaultOffers = getDefaultOffersForNewBeacon(beacon.createdBy.account.id);
                    foreach(DefaultOffer offer in defaultOffers)
                    {
                        if(!this.saveBeaconDefaultOffer(beacon, offer, transaction))
                        {
                            transaction.rollback();
                            return false;
                        }
                        
                    }                  
                }
                else
                {
                    transaction.rollback();
                    return false;
                }
                transaction.commit();
                return true;
            }        
        }

        public String getUrl(String beaconGuid)
        {
            UriBuilder uriBuilder = new UriBuilder();
            uriBuilder.Scheme = "http";
            uriBuilder.Host = WebConfigurationManager.AppSettings["hostName"].ToString();
            uriBuilder.Path = "index.html";
            uriBuilder.Fragment = @"/beaconCurrentOffer/" + beaconGuid;
            uriBuilder.Port = 48302;
            Uri uri = uriBuilder.Uri;
            return uri.ToString();
        }

        public Int64 getBeaconIdFromGuid(String beaconGuid)
        {
            String query = QueryConstants.BEACON_ID_FROM_GUID + "'" + beaconGuid + "'";
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconGuid"] = beaconGuid;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            Int64 beaconId = 0;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if (WSDatabaseHelper.isValidField(reader, "BEACON_ID"))
                    {
                        beaconId = reader.GetInt64(reader.GetOrdinal("BEACON_ID"));
                    }
                }
            }
            Query.connection.Close();
            return beaconId;
        }

        public bool updateBeaconShortUrl(Beacon beacon, WSTransaction transaction)
        {
            String query = "UPDATE Beacons SET short_url = '" + beacon.shortUrl + "' WHERE id = " + beacon.id.ToString();
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beacon.id;
            Query.parameters["beaconUrl"] = beacon.shortUrl;
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeUpdateQuery(Query);
        }

        public bool updateBeaconPassUrl(Beacon beacon, WSTransaction transaction)
        {
            String query = "UPDATE Beacons SET pass_url = '" + beacon.passUrl + "' WHERE id = " + beacon.id.ToString();
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beacon.id;
            Query.parameters["passUrl"] = beacon.passUrl;
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeUpdateQuery(Query);
        }

        public List<DefaultOffer> getDefaultOffersForNewBeacon(Int64 accountId)
        {            
            List<DefaultOffer> offers = new List<DefaultOffer>();
            String query = "SELECT offer_id AS DEFAULT_OFFER_ID, offer_type AS DEFAULT_OFFER_OFFER_TYPE FROM default_offers WHERE device_type = 1 and account_id = @accountId";
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = accountId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    offers.Add(new DefaultOffer(reader));
                }
            }
            Query.connection.Close();
            return offers;
        }

        public bool saveBeaconDefaultOffer(Beacon beacon, DefaultOffer offer, WSTransaction transaction)
        {
            String query = "INSERT INTO Beacons_Offers(beacon_id, offer_id, is_default, default_type, created_on, updated_on, created_by, updated_by) VALUES(@beaconId, @offerId, @isDefault, @defaultType, @createdOn, @updatedOn, @createdBy, @updatedBy);";
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beacon.id;
            Query.parameters["offerId"] = offer.id;
            Query.parameters["isDefault"] = 1;
            if(offer.OfferType == DefaultOffer.Offer_Type.MEMBER)
            {
                Query.parameters["defaultType"] = 1;
            }
            if (offer.OfferType == DefaultOffer.Offer_Type.NON_MEMBER)
            {
                Query.parameters["defaultType"] = 2;
            }
            Query.parameters["createdOn"] = DateTime.UtcNow.ToString();
            Query.parameters["updatedOn"] = DateTime.UtcNow.ToString();
            Query.parameters["createdBy"] = beacon.createdBy.id;
            Query.parameters["updatedBy"] = beacon.updatedBy.id;
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);
        }

        public List<Beacon> getAll(Account account)
        {
            Int64 offSet = Convert.ToInt64(HttpContext.Current.Request.Headers.GetValues("Timezone").First());
            DateTime currentDateTime = DateTime.UtcNow.AddMinutes(-offSet);
            
            String query = QueryConstants.ALL_BEACONS_BY_ACCOUNT + account.id.ToString() + " ORDER BY BEACON.updated_on DESC";
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = account.id;
            Query.parameters["currentDateTime"] = currentDateTime;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Beacon> beaconsList = new List<Beacon>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    beaconsList.Add(new Beacon(reader));
                }               
            }
            Query.connection.Close();
            return beaconsList;
        }

        private void throwException()
        {
            throw new NotImplementedException();
        }

        public bool checkIfNameExists(String beaconName, Int64 storeId)
        {          
            String query = "SELECT CASE WHEN EXISTS (Select * from Beacons where name = @beaconName and store_id = @storeId) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconName"] = beaconName;
            Query.parameters["storeId"] = storeId;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }

        public bool checkIfNumberExists(String beaconNumber, Int64 storeId)
        {           
            String query = "SELECT CASE WHEN EXISTS (Select * from Beacons where number = @beaconNumber and store_id = @storeId) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconNumber"] = beaconNumber;
            Query.parameters["storeId"] = storeId;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }

        public bool checkDefaultOffer(Int64 accountId)
        {
            bool memberDefault = isDefaultOffersSetForBeacon(accountId, 1);
            bool nonMemberDefault = isDefaultOffersSetForBeacon(accountId, 2);
            if (memberDefault && nonMemberDefault)           
                return true;           
            else
                return false;
        }

        public bool isDefaultOffersSetForBeacon(Int64 accountId, int offerType)
        {
            String query = "SELECT CASE WHEN EXISTS (Select * from default_offers where device_type = 1 and offer_type = @offerType and account_id = @accountId) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = accountId;
            Query.parameters["offerType"] = offerType;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }

        public Beacon getDetails(Beacon beacon)
        {
            String query = QueryConstants.BEACON_DETAILS + beacon.id.ToString();
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beacon.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    beacon = new Beacon(reader);
                    beacon.store = new Store(reader);
                    beacon.store.country = new Country(reader);
                }             
            }
            Query.connection.Close();
            return beacon;
        }

        public bool addUserBeacons(Int64 userId, Int64 beaconId, Int64 offerId)
        {
            UsersBeacons userBeacon = new UsersBeacons();

            String query = "INSERT INTO users_beacons (user_id, beacon_id, offer_id, date, is_registered, count, is_availed) VALUES (@userId, @beaconId, @offerId, @date, @isRegistered, @count, @is_availed)";
            WSQuery Query = new WSQuery(query);
            Query.parameters["userId"] = userId;
            Query.parameters["beaconId"] = beaconId;
            Query.parameters["offerId"] = offerId;
            Query.parameters["date"] = DateTime.UtcNow.ToString();
            Query.parameters["isRegistered"] = 0;
            Query.parameters["count"] = 1;
            Query.parameters["is_availed"] = 0;

            bool reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);

            return reader;
        }

        public UsersBeacons getUserBeacon(Int64 userId, Int64 beaconId, Int64 offerId)
        {
            UsersBeacons userBeacon = new UsersBeacons();

            String query = "SELECT user_id AS USER_ID, beacon_id AS BEACON_ID, offer_id AS OFFER_ID, date AS DATE, is_registered AS IS_REGISTERED, count AS COUNT, is_availed AS IS_AVAILED  FROM users_beacons WHERE user_id = @userId AND beacon_id = @beaconId AND offer_id = @offerId";
            WSQuery Query = new WSQuery(query);
            Query.parameters["userId"] = userId;
            Query.parameters["beaconId"] = beaconId;
            Query.parameters["offerId"] = offerId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    userBeacon = new UsersBeacons(reader);
                }
            }
            Query.connection.Close();

            return userBeacon;
        }

        public UsersBeacons updateUserBeacon(UsersBeacons userBeacon)
        {
            UsersBeacons updateUserBeacon = new UsersBeacons();

            //String query = "INSERT INTO users_beacons (user_id, beacon_id, offer_id, date, is_registered, count, is_availed) VALUES (@userId, @beaconId, @offerId, @date, @isRegistered, @count, @is_availed)";
            String query = "UPDATE users_beacons SET count = @count, date = @date WHERE user_id = @userId AND beacon_id = @beaconId AND offer_id = @offerId";
            WSQuery Query = new WSQuery(query);
            Query.parameters["userId"] = userBeacon.user_id;
            Query.parameters["beaconId"] = userBeacon.beacon_id;
            Query.parameters["offerId"] = userBeacon.offer_id;
            Query.parameters["date"] = DateTime.UtcNow;
            Query.parameters["isRegistered"] = userBeacon.is_registered;
            Query.parameters["count"] = userBeacon.count + 1;
            Query.parameters["is_availed"] = userBeacon.is_availed;

            bool reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeUpdateQuery(Query);

            return updateUserBeacon;
        }

        public bool update(Beacon beacon)
        {
            using (WSTransaction transaction = new WSTransaction("UPDATE_BEACON"))
            {
                String query = "UPDATE Beacons SET name = @name, number = @number, store_id = @storeId, updated_on =  @updatedOn, updated_by = @updatedBy WHERE id = @id";
                WSQuery Query = new WSQuery(query, beacon.toDictionary(), transaction);
                Query.transaction = transaction;
                if (!WSDatabaseManager.sharedDatabaseManagerinstance.executeUpdateQuery(Query))
                {
                    transaction.rollback();
                    return false;
                }
                transaction.commit();
                return true;
            }
        }

        public bool isBeaconUsedForRegistration(Int64 beaconId)
        {
            WSQuery Query = new WSQuery();
            Query.query = "SELECT * FROM users_beacons WHERE beacon_id = @beaconId";
            Query.parameters["beaconId"] = beaconId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            return reader.HasRows;
        }

        public bool delete(Int64 beaconId)
        {
            using (WSTransaction transaction = new WSTransaction("DELETE_BEACON"))
            {
                this.deleteBeaconOffer(beaconId, transaction);
                WSQuery Query = new WSQuery();
                Query.query = "DELETE FROM Beacons WHERE id = @beaconId";
                Query.parameters["beaconId"] = beaconId;
                Query.transaction = transaction;
                if (!WSDatabaseManager.sharedDatabaseManagerinstance.executeDeleteQuery(Query))
                {
                    transaction.rollback();
                    return false;
                }
                transaction.commit();
                return true;
            }
        }

        public bool deleteBeaconOffer(Int64 beaconId, WSTransaction transaction)
        {
            WSQuery Query = new WSQuery();
            Query.query = "IF EXISTS (SELECT * FROM Beacons_Offers WHERE beacon_id = @beaconId) BEGIN DELETE FROM Beacons_Offers WHERE beacon_id = @beaconId END";
            Query.parameters["beaconId"] = beaconId;
            Query.transaction = transaction;
           return WSDatabaseManager.sharedDatabaseManagerinstance.executeDeleteQuery(Query);     
        }

        public bool changeBeaconWorkableState(Int64 beaconId)
        {
            String query = "IF EXISTS(SELECT * FROM Beacons where id = @beaconId and is_workable = 1) UPDATE Beacons SET is_workable = 0, updated_on = @updatedOn where id= @beaconId ELSE UPDATE Beacons SET is_workable = 1, updated_on = @updatedOn where id= @beaconId";
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beaconId;
            Query.parameters["updatedOn"] = DateTime.UtcNow.ToString();

            return  WSDatabaseManager.sharedDatabaseManagerinstance.executeUpdateQuery(Query);
        }

        public bool isWokable(Int64 beaconId)
        {
            String query = "SELECT CASE WHEN EXISTS (Select * from Beacons where is_workable = 1 and id = @beaconId) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["beaconId"] = beaconId;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }





    }
}