﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using BNS.Models.Users;
using BNS.Models.Contacts;
using BNS.DL.WSDatabase;
using BNS.DAL.DALContact;
using BNS.DAL.DALOffer;
using BNS.Models.Accounts;
using BNS.Models.Offers;
using BNS.DAL.DALAccount;
using System.Net;
using System.Net.Mail;
using WSDatabase;
using System.IO;
using BNS.Models.Categories;
using System.Dynamic;
using RazorEngine;
using BNS.DAL.Constants;
using BNS.DAL.DALStore;
using BNS.Models.Countries;
using RazorEngine.Templating;
using System.Web.Configuration;
using BNS.DAL.CommunicationManager.Email;
using BNS.Models.UserDevices;
using BNS.DAL.CommunicationManager.Message;
using BNS.DAL.DALCountry;
using Twilio.Exceptions;

namespace BNS.DAL.DALUser
{
    public class DALUserManager
    {
        
        DALOfferManager offerManager = new DALOfferManager();
        DALAccountManager accountManager = new DALAccountManager();

        public List<User> getAll(Account account)
        {
            String query = QueryConstants.TOTAL_REGISTERED_USERS_FROM_ACCOUNT;
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = account.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<User> usersList = new List<User>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    usersList.Add(new User(reader));
                }                              
            }
            Query.connection.Close();
            foreach (User user in usersList)
            {
                user.contacts = getCantactsForUser(user.id);
                user.latestOffer = offerManager.getUserLatestOfferDetails(user.latestOffer);
            }
            return usersList;
        }

        public List<User> getAllUsersToSendNotification(Account account)
        {
            String query = QueryConstants.ALL_USERS_FOR_NOTIFICATION;
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = account.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<User> usersList = new List<User>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    usersList.Add(new User(reader));
                }
            }
            Query.connection.Close();
            foreach (User user in usersList)
            {
                if(user.UserType == User.User_Type.MEMBER)
                {
                    user.contacts = getCantactsForUser(user.id);                    
                }                
                user.latestOffer = offerManager.getUserLatestOfferDetails(user.latestOffer);
                user.userDevices = getUserDevice(user.id);
            }
            return usersList;
        }   
        
        public List<UserDevice> getUserDevice(Int64 userId)
        {
            String query = QueryConstants.DEVICES_FOR_USER;
            WSQuery Query = new WSQuery(query);
            Query.parameters["userId"] = userId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<UserDevice> userDevicesList = new List<UserDevice>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    userDevicesList.Add(new UserDevice(reader));
                }
            }
            Query.connection.Close();
            return userDevicesList;
        }     

        public List<User> getAnnonymous(Account account)
        {
            String query = QueryConstants.ANNONYMOUS_USERS;
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = account.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<User> usersList = new List<User>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    usersList.Add(new User(reader));
                }
            }
            Query.connection.Close();            
            return usersList;
        }

        public List<User> getUserActivities(Int64 userId)
        {
            String query = QueryConstants.USER_ACTIVITIES;
            WSQuery Query = new WSQuery(query);
            Query.parameters["userId"] = userId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<User> usersList = new List<User>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    usersList.Add(new User(reader));
                }
            }
            Query.connection.Close();

            return usersList;
        }

        public List<User> getAnnonymousActivities(Int64 userId)
        {
            String query = QueryConstants.ANNONYMOUS_ACTIVITIES;
            WSQuery Query = new WSQuery(query);
            Query.parameters["userId"] = userId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<User> usersList = new List<User>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    usersList.Add(new User(reader));
                }
            }
            Query.connection.Close();

            return usersList;
        }

        private string PopulateBodyForMemberIdEmail(User user)
        {
            user.account = accountManager.getDetails(user.account);
            dynamic model = new ExpandoObject();
            model.picUrl = WebConfigurationManager.AppSettings["IMAGE_PATH"].ToString() + user.account.picUrl;
            model.sender = user.account.name;           

            string body = string.Empty;
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("")))
            {
                body = reader.ReadToEnd();
            }
            body = Engine.Razor.RunCompile(body, new Random().Next(100, 10000).ToString(), null, (object)model); //rendering the template with our model
                        
            return body;
        }

        public bool sendMemberIdMail(User user)
        {                       
            bool enableSSL = false;                       
            string emailTo = user.contact.value;
            string subject = "Member Id";
            try
            {
                using (MailMessage mail = new MailMessage())
                {

                    mail.From = new MailAddress(EmailConfig.emailFromSupport, EmailConfig.SupportDisplayName);
                    mail.To.Add(emailTo);
                    mail.Subject = subject;
                    //mail.Body = PopulateBodyForMemberIdEmail(user);
                    mail.Body = "User Member Id is : " + user.memberId;
                    mail.IsBodyHtml = true;

                    using (SmtpClient smtp = new SmtpClient(EmailConfig.smtpAddress, EmailConfig.portNumber))
                    {
                        smtp.Credentials = new NetworkCredential(EmailConfig.emailFromSupport, EmailConfig.supportPassword);
                        smtp.EnableSsl = enableSSL;
                        smtp.Send(mail);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                return false;
            }       
        }

        public User getMemberId(User user)
        {
            String query = "SELECT member_id AS MEMBER_ID FROM users USR JOIN contacts CONTACT ON USR.contact_id = CONTACT.id WHERE CONTACT.value = @email AND CONTACT.type = 1 AND USR.account_id = @accountId";
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = user.account.id;
            Query.parameters["email"] = user.contact.value;       
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);            
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if (WSDatabaseHelper.isValidField(reader, "MEMBER_ID"))
                    {
                        user.memberId = reader.GetString(reader.GetOrdinal("MEMBER_ID"));
                    }                                     
                }
            }
            Query.connection.Close();
            return user;
        }

        public List<Contact> getCantactsForUser(Int64 userId)
        {
            String query = QueryConstants.CONTACTS_FOR_USER;
            WSQuery Query = new WSQuery(query);
            Query.parameters["userId"] = userId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<Contact> contactsList = new List<Contact>();
            if(reader.HasRows)
            {
                while(reader.Read())
                {
                    contactsList.Add(new Contact(reader));
                }               
            }
            Query.connection.Close();
            return contactsList;
        }

        public Int64 creatUser(Account account , WSTransaction transaction)
        {            
            String query = "INSERT INTO users(contact_id, account_id, username, password, created_on, updated_on) OUTPUT Inserted.id VALUES(@contactId, @accountId, @userName, @password, @createdOn, @updatedOn)";
            WSQuery Query = new WSQuery(query);
            Query.parameters["contactId"] = account.admin.contact.id;
            Query.parameters["accountId"] = account.id;          
            Query.parameters["userName"] = account.admin.username;
            Query.parameters["password"] = account.admin.password;
            Query.parameters["createdOn"] = DateTime.UtcNow.ToString();
            Query.parameters["updatedOn"] = DateTime.UtcNow.ToString();
            Query.transaction = transaction;
            account.admin.id = WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQueryReturnID(Query);
            return account.admin.id;
        }

        public Int64 saveUserPushId(User user)
        {
            String query = "INSERT INTO devices(user_id, type, push_id, created_on, updated_on) OUTPUT Inserted.id VALUES(@userId, @type, @pushId, @createdOn, @updatedOn)";
            WSQuery Query = new WSQuery(query);
            Query.parameters["userId"] = user.id;
            Query.parameters["type"] = user.userDevice.userDeviceType;
            Query.parameters["pushId"] = user.userDevice.push_id;
            Query.parameters["createdOn"] = DateTime.UtcNow.ToString();
            Query.parameters["updatedOn"] = DateTime.UtcNow.ToString();
            user.userDevice.id = WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQueryReturnID(Query);
            return user.userDevice.id;
        }

        public User saveUserFromBeacon(User user)

        {           
            using (WSTransaction transaction = new WSTransaction("SAVE_USER_FROM_BEACON"))

            {
                bool isPhoneExists = false;
                bool isEmailExists = false;
                bool isTextSent = false;
                bool isEmailSent = false;
                if(user.contacts.Count != 0)
                {
                    foreach (Contact contact in user.contacts)
                    {
                        contact.id = new DALContactManager().saveContact(contact, transaction);
                        if (contact.id != 0)
                        {
                            if (contact.ContactType == Contact.Contact_Type.EMAIL)
                            {
                                isEmailExists = true;
                                user.contact = contact;
                            }
                            if(contact.ContactType == Contact.Contact_Type.PHONE)
                            {
                                isPhoneExists = true;
                            }
                        }
                        else
                        {
                            transaction.rollback();
                            user.id = 0;
                            return user;
                        }
                    }
                }
                
                String query = String.Empty;
                WSQuery Query = new WSQuery();
                                                 
                if(user.contact.id != 0)
                {
                    query = "UPDATE users SET contact_id = @contactId, type = 1, updated_on = @updatedOn WHERE users.id = @userId";
                    Query.parameters["contactId"] = user.contact.id;
                }

                if(user.contact.id == 0)
                {
                    query = "UPDATE users SET type = 1, updated_on = @updatedOn WHERE users.id = @userId";                   
                }
                Query.query = query;                                      
                Query.parameters["updatedOn"] = DateTime.UtcNow.ToString();
                Query.parameters["userId"] = user.id;              
                Query.transaction = transaction;
                bool result =  WSDatabaseManager.sharedDatabaseManagerinstance.executeUpdateQuery(Query);

                if(result)
                {
                    if(user.contacts.Count != 0)
                    {
                        foreach (Contact contact in user.contacts)
                        {
                            user.contact = contact;
                            if (!this.saveUserContacts(user, transaction))
                            {
                                transaction.rollback();
                                user.id = 0;
                                return user;
                            }
                        }
                    }
                    
                    if(user.socialService != null && user.uniqueId != null )
                    {
                        if(!this.saveUserSocialLogin(user, transaction))
                        {
                            transaction.rollback();
                            user.id = 0;
                            return user;
                        }
                    }           

                    if(user.beacon != null)
                    {
                        if(!this.saveUserBeacons(user, transaction))
                        {
                            transaction.rollback();
                            user.id = 0;
                            return user;
                        }                        
                    }                   

                    user.offer = offerManager.getDetails(user.offer);                
                    user.offer.code = this.getAlphaNumericCode(8);
                    if (!this.saveUsersOfferCodes(user, transaction))
                    {                      
                        transaction.rollback();
                        user.id = 0;
                        return user;
                    }                                       
                    transaction.commit();
                    user.account = accountManager.getDetails(user.account);
                    if (isEmailExists)
                    {
                        isEmailSent = this.sendOfferEmail(user);
                    }
                    if (isPhoneExists)
                    {
                        foreach (Contact contact in user.contacts)
                        {
                            if (contact.ContactType == Contact.Contact_Type.PHONE)
                            {
                                contact.country.countryCode = new DALCountryManager().getCountryCode(contact.country.id);
                                try
                                {                                   
                                    String message = user.offer.description + ". Your offer code is:" + user.offer.code + ". Your member ID is:" + user.memberId + ".";
                                    String To = String.Concat(contact.country.countryCode, contact.value);
                                    isTextSent = MessageService.SendSms(To, message);
                                }
                                catch (ApiException Ex)
                                {
                                    Console.WriteLine(Ex.ToString());
                                }
                            }
                        }
                    }
                    if (isEmailSent)
                        user.userOfferStatus = 1;
                    if (isTextSent)
                        user.userOfferStatus = 2;
                    if (isEmailSent && isTextSent)
                        user.userOfferStatus = 3;
                    if (!isEmailSent && !isTextSent)
                        user.userOfferStatus = 4;
                    return user;
                }
                else
                {
                    transaction.rollback();
                    user.id = 0;
                    return user;
                }
            }
        }



        public bool checkIfUniqueIdExixts(String uniqueId, Int64 accountId)
        {
            String query = "SELECT CASE WHEN EXISTS (SELECT * FROM users USR JOIN users_social_login USR_SOC_LOG ON  USR.id = USR_SOC_LOG.user_id WHERE USR_SOC_LOG.unique_id = @uniqueId and USR.account_id = @accountId) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["uniqueId"] = uniqueId;
            Query.parameters["accountId"] = accountId;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }       

        public String getMemberIdFromUserId(Int64 userId, WSTransaction transaction)
        {
            String MemberId = "";
            String query = "SELECT USR.member_id AS MEMBER_ID FROM users USR WHERE USR.id=@userId;";
            WSQuery Query = new WSQuery(query);
            Query.parameters["userId"] = userId;
            Query.transaction = transaction;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            User user = new User();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    MemberId = Convert.ToString(reader.GetOrdinal("MEMBER_ID"));
                }
            }
            Query.connection.Close();

            return MemberId;
        }

        public User saveUser(User user)
        {
            using (WSTransaction transaction = new WSTransaction("SAVE_USER_FROM_KIOSK"))
            {
                bool isTextSent = false;
                bool isEmailSent = false;

                foreach (Contact contact in user.contacts)
                {
                    contact.id = new DALContactManager().saveContact(contact, transaction);
                    if (contact.id == 0)
                    {
                        transaction.rollback();
                        user.id = 0;
                        return user;
                    }                    
                }
                user.memberId = getAlphaNumericCode(4);
                String query = "INSERT INTO users(contact_id, account_id, member_id, type, created_on, updated_on) OUTPUT Inserted.id  VALUES(@contactId, @accountId, @memberId, @type, @createdOn, @updatedOn); ";
                WSQuery Query = new WSQuery(query, user.toDictionary(), transaction);
                user.id = WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQueryReturnID(Query);

                if (user.id != 0)
                {
                    foreach (Contact contact in user.contacts)
                    {
                        user.contact = contact;
                        if (!this.saveUserContacts(user, transaction))
                        {
                            transaction.rollback();
                            user.id = 0;
                            return user;
                        }
                    }                   

                    if (user.kiosk != null)
                    {
                        if (!this.saveUserKiosks(user, transaction))
                        {
                            transaction.rollback();
                            user.id = 0;
                            return user;
                        }
                    }

                    user.offer = offerManager.getDetails(user.offer);
                    user.offer.code = this.getAlphaNumericCode(8);
                    if (!this.saveUsersOfferCodes(user, transaction))
                    {
                        transaction.rollback();
                        user.id = 0;
                        return user;
                    }                    
                    transaction.commit();

                    user.account = accountManager.getDetails(user.account);
                    foreach (Contact contact in user.contacts)
                    {
                        if(contact.ContactType == Contact.Contact_Type.EMAIL)
                        {
                           isEmailSent = this.sendOfferEmail(user);
                        }
                        if(contact.ContactType == Contact.Contact_Type.PHONE)
                        {
                            contact.country.countryCode = new DALCountryManager().getCountryCode(contact.country.id);
                            try
                            {                               
                                String message = user.offer.description + ". Your offer code is:" + user.offer.code + ". Your member ID is:" + user.memberId + ".";
                                String To = String.Concat(contact.country.countryCode, contact.value);
                                isTextSent = MessageService.SendSms(To, message);
                            }
                            catch (ApiException Ex)
                            {
                                Console.WriteLine(Ex.ToString());
                            }                        
                        }
                    }

                    if (isEmailSent)
                        user.userOfferStatus = 1;
                    if (isTextSent)
                        user.userOfferStatus = 2;
                    if (isEmailSent && isTextSent)
                        user.userOfferStatus = 3;
                    if (!isEmailSent && !isTextSent)
                        user.userOfferStatus = 4;
                    return user;                   
                }
                else
                {
                    transaction.rollback();
                    user.id = 0;
                    return user;
                }
            }
        }

        public User saveAnonymousUser(Int64 account_id)
        {
            User user = new User();
            user.memberId = getAlphaNumericCode(4);
            user.account = new Account();
            user.account.id = account_id;
            user.type = 2;

            using (WSTransaction transaction = new WSTransaction("SAVE_ANONYMOUS_USER"))
            {
                String query = "INSERT INTO users(account_id, member_id, created_on, updated_on, type) OUTPUT Inserted.id  VALUES(@accountId, @memberId, @createdOn, @updatedOn, @type); ";
                WSQuery Query = new WSQuery(query, user.toDictionary(), transaction);
                user.id = WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQueryReturnID(Query);
                if(user.id !=0)
                {
                    transaction.commit();
                }
                else
                {
                    transaction.rollback();
                }

            }

            return user;
        }

        public User saveMemberOffer(User member)
        {
            using (WSTransaction transaction = new WSTransaction("SAVE_MEMBER_OFFER"))
            {
                bool isTextSent = false;
                bool isEmailSent = false;
                member = getUserSocialLoginDetails(member);
                member.contacts = getCantactsForUser(member.id);
                member.is_registered = 0;
                if(member.id != 0)
                {                

                    if (member.kiosk != null)
                    {
                        if (!this.updateUserKiosks(member, transaction))
                        {
                            transaction.rollback();
                            member.id = 0;
                            return member;
                        }
                    }

                    member.offer = offerManager.getDetails(member.offer);
                    member.offer.code = this.getAlphaNumericCode(8);
                    if (!this.saveUsersOfferCodes(member, transaction))
                    {
                        transaction.rollback();
                        member.id = 0;
                        return member;
                    }
                    transaction.commit();

                    member.account = accountManager.getDetails(member.account);
                    foreach (Contact contact in member.contacts)
                    {
                        if (contact.ContactType == Contact.Contact_Type.EMAIL)
                        {
                            isEmailSent = this.sendOfferEmail(member);
                        }
                        if (contact.ContactType == Contact.Contact_Type.PHONE)
                        {
                            contact.country.countryCode = new DALCountryManager().getCountryCode(contact.country.id);
                            try
                            {
                                String message = member.offer.description + ". Your offer code is:" + member.offer.code + ". Your member ID is:" + member.memberId + ".";
                                String To = String.Concat(contact.country.countryCode, contact.value);
                                isTextSent = MessageService.SendSms(To, message);
                            }
                            catch (ApiException Ex)
                            {
                                Console.WriteLine(Ex.ToString());
                            }                                                      
                        }
                    }

                    if (isEmailSent)
                        member.userOfferStatus = 1;
                    if (isTextSent)
                        member.userOfferStatus = 2;
                    if (isEmailSent && isTextSent)
                        member.userOfferStatus = 3;
                    if (!isEmailSent && !isTextSent)
                        member.userOfferStatus = 4;
                    return member;                  
                }
                else
                {
                    transaction.rollback();
                    member.id = 0;
                    return member;
                }
            }

        }

        public User getUserDetails(User member)
        {
            String query = QueryConstants.USER_DETAILS;
            WSQuery Query = new WSQuery(query);
            Query.parameters["memberId"] = member.memberId;
            Query.parameters["accountId"] = member.account.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            User user = new User();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    user = new User(reader);
                }               
            }
            Query.connection.Close();
            member.id = user.id;
            member.UserType = user.UserType;
            return member;
        }

        public User getUserSocialLoginDetails(User member)
        {
            String query = QueryConstants.USER_SOCIAL_LOGIN_DETAILS;
            WSQuery Query = new WSQuery(query);
            Query.parameters["memberId"] = member.memberId;
            Query.parameters["accountId"] = member.account.id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            User user = new User();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    user = new User(reader);
                }
            }
            Query.connection.Close();
            if(user.id !=0)
            {
                member.id = user.id;
                member.UserType = user.UserType;
                member.socialService = user.socialService;
            }
            return member;
        }

        public Int64 getUserIdByMemberId(String member_id)
        {
            Int64 userId = 0;
            String query = "SELECT USR.id AS USER_ID FROM users USR where USR.member_id = @memberId";
            WSQuery Query = new WSQuery(query);
            Query.parameters["memberId"] = member_id;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            User userDetails = new User();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if (WSDatabaseHelper.isValidField(reader, "USER_ID"))
                    {
                        userId = reader.GetInt64(reader.GetOrdinal("USER_ID"));
                    }
                }
            }
            Query.connection.Close();           
            return userId;
        }

        public User getUserDetailsByMemberId(User user)
        {
            String query = QueryConstants.USER_DETAILS_BY_MEMBER_ID;
            WSQuery Query = new WSQuery(query);
            Query.parameters["memberId"] = user.memberId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            User userDetails = new User();
            if(reader.HasRows)
            {
                while(reader.Read())
                {
                    userDetails = new User(reader);
                }
            }
            Query.connection.Close();
            user.id = userDetails.id;
            user.type = userDetails.type;
            user.account = userDetails.account;
            return user;
        }

        public User getUserDetailsById(Int64 userId)
        {
            String query = QueryConstants.USER_DETAILS_BY_ID;
            WSQuery Query = new WSQuery(query);
            Query.parameters["userId"] = userId;           
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            User user = new User();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    user = new User(reader);
                }
            }       
            Query.connection.Close();
            user.contacts = getCantactsForUser(user.id);
            return user;
        }

        public User getUserDetailsByUniqueId(String uniqueId, Int64 accountId)
        {
            String query = QueryConstants.USER_DETAILS_BY_UNIQUE_ID;
            WSQuery Query = new WSQuery(query);
            Query.parameters["uniqueId"] = uniqueId;
            Query.parameters["accountId"] = accountId; 
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            User user = new User();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    user = new User(reader);
                }
            }
            Query.connection.Close();            
            return user;
        }

        public String getAlphaNumericCode(int n)
        {
            int maxSize = n;
            char[] chars = new char[62];
            chars =
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
            byte[] data = new byte[1];
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetNonZeroBytes(data);
                data = new byte[maxSize];
                crypto.GetNonZeroBytes(data);
            }
            StringBuilder result = new StringBuilder(maxSize);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }

        public bool saveUsersOfferCodes(User user, WSTransaction transaction)
        {
            String query = "INSERT INTO users_offer_codes(offer_id, user_id, code, status) VALUES(@offerId, @userId, @code, @status);";
            WSQuery Query = new WSQuery(query);
            Query.parameters["offerId"] = user.offer.id;
            Query.parameters["userId"] = user.id;
            Query.parameters["code"] = user.offer.code;
            Query.parameters["status"] = 0;
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);
        }
                 
        public bool saveUserContacts(User user, WSTransaction transaction)
        {
            String query = "INSERT INTO users_contacts(user_id, contact_id) VALUES(@userId, @contactId);";
            WSQuery Query = new WSQuery(query);
            Query.parameters["userId"] = user.id;
            Query.parameters["contactId"] = user.contact.id;
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);
        }       

        public bool saveUserSocialLogin(User user, WSTransaction transaction)
        {
            String query = "INSERT INTO users_social_login(user_id, social_service, unique_id) VALUES(@userId, @socialService, @uniqueId)";
            WSQuery Query = new WSQuery(query);
            Query.parameters["userId"] = user.id;
            Query.parameters["socialService"] = user.socialService;
            Query.parameters["uniqueId"] = user.uniqueId;
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);
        }


        public bool saveUserBeacons(User user, WSTransaction transaction)
        {
            String query = "UPDATE users_beacons SET is_registered = 1, is_availed = 1 WHERE users_beacons.user_id = @userId AND users_beacons.offer_id = @offerId AND users_beacons.beacon_id = @beaconId";
            WSQuery Query = new WSQuery(query);
            Query.parameters["userId"] = user.id;
            Query.parameters["beaconId"] = user.beacon.id;
            Query.parameters["offerId"] = user.offer.id;
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);
        }

        public bool saveUserKiosks(User user, WSTransaction transaction)
        {

            String query = "INSERT INTO users_kiosks(user_id, kiosk_id, offer_id, date, is_registered, is_availed, count) VALUES(@userId, @kioskId, @offerId, @date, @isRegistered, @isAvailed, @count);";
            WSQuery Query = new WSQuery(query);
            Query.parameters["userId"] = user.id;
            Query.parameters["kioskId"] = user.kiosk.id;
            Query.parameters["offerId"] = user.offer.id;
            Query.parameters["date"] = DateTime.UtcNow.ToString();
            Query.parameters["isRegistered"] = user.is_registered;
            Query.parameters["isAvailed"] = 1;
            Query.parameters["count"] = 1;
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);
        }

        public bool updateUserKiosks(User user, WSTransaction transaction)
        {
            String query = "UPDATE users_kiosks SET is_availed = 1, date = @date WHERE user_id = @userId AND kiosk_id = @kioskId AND offer_id = @offerId";
            WSQuery Query = new WSQuery(query);
            Query.parameters["userId"] = user.id;
            Query.parameters["kioskId"] = user.kiosk.id;
            Query.parameters["offerId"] = user.offer.id;
            Query.parameters["date"] = DateTime.UtcNow.ToString();            
            Query.transaction = transaction;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeUpdateQuery(Query);
        }

        private string PopulateBodyForOfferEmail(User user)
        {
            String categories = String.Empty;
            String categoryValue = String.Empty;
            Country country = user.offer.countries.First<Country>();
            int i = 0;
            foreach(Category category in user.offer.categories)
            {
                i++;
                if (user.offer.categories.Count == i)
                {
                    categories += category.name;
                    if (category.CategoryUnit == Category.Category_Unit.CURRENCY)
                    {
                        categoryValue += country.currency.ToString() + category.value + " " + category.name + ".";
                    }
                    else
                    {
                        categoryValue += category.value + " " + category.name + ".";
                    }
                }
                else
                {
                    categories += category.name + " and ";
                    if (category.CategoryUnit == Category.Category_Unit.CURRENCY)
                    {
                        categoryValue += country.currency.ToString() + category.value + " " + category.name + " and ";
                    }
                    else
                    {
                        categoryValue += category.value + " " + category.name + " and ";
                    }

                }
            }         
            dynamic model = new ExpandoObject();
            model.picUrl = WebConfigurationManager.AppSettings["IMAGE_PATH"].ToString() + user.account.picUrl;
            model.sender = user.account.name;                       
            model.categories = categories;
            model.category_value = categoryValue;
            model.offer_code = user.offer.code;
            model.member_id = user.memberId;
            model.website = "";
            model.facebook = "";
            model.twitter = "";
            model.socialService = "";
            foreach (Contact contact in user.account.accountContacts)
            {
                if (contact.ContactType == Contact.Contact_Type.WEBSITE)
                    model.website = contact.value;
                if (contact.ContactType == Contact.Contact_Type.FACEBOOK)
                    model.facebook = contact.value;
                if (contact.ContactType == Contact.Contact_Type.TWITTER)
                    model.twitter = contact.value;
            }  
            if(user.socialService != null)
            {
                model.socialService = user.socialService;
            }                                   
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/bnsMail.cshtml")))
            {
                body = reader.ReadToEnd();
            }
            body = Engine.Razor.RunCompile(body, new Random().Next(100, 10000).ToString(), null, (object)model); //rendering the template with our model                        
            return body;
        }

        public bool sendOfferEmail(User user)
        {
            Contact emailContact = user.contacts.First<Contact>();
            Category offerCategory = user.offer.categories.First<Category>();
            String body = PopulateBodyForOfferEmail(user);
            bool enableSSL = false;             
            string emailTo = emailContact.value;
            string subject = user.offer.title;

            try
            {
                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress(EmailConfig.emailFromOffers, EmailConfig.OffersDisplayName);
                    mail.To.Add(emailTo);
                    mail.Subject = subject;
                    mail.Body = body;
                    mail.IsBodyHtml = true;

                    using (SmtpClient smtp = new SmtpClient(EmailConfig.smtpAddress, EmailConfig.portNumber))
                    {
                        smtp.Credentials = new NetworkCredential(EmailConfig.emailFromOffers, EmailConfig.offersPassword);
                        smtp.EnableSsl = enableSSL;
                        smtp.Send(mail);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                return false;
            }          
        }        
        
        public bool isRegisteredUser(User user)
        {
            String query = "SELECT CASE WHEN EXISTS (Select * from users_beacons where user_id = @userId and is_registered = 1 ) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["userId"] = user.id;          
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }

        public bool isAvailed(User userInfo)
        {
            String query = "SELECT CASE WHEN EXISTS (Select * FROM users_beacons WHERE user_id = @userId AND offer_id = @offerId AND is_availed = 1) OR EXISTS (Select * FROM users_kiosks WHERE user_id = @userId AND offer_id = @offerId AND is_availed = 1) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["userId"] = userInfo.id;
            Query.parameters["offerId"] = userInfo.offer.id;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }

        public bool userBeaconsPresent(User user)
        {
            String query = "SELECT CASE WHEN EXISTS (SELECT * FROM users_beacons WHERE user_id = @userId AND beacon_id = @beaconId AND offer_id = @offerId) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["userId"] = user.id;
            Query.parameters["offerId"] = user.offer.id;
            Query.parameters["beaconId"] = user.beacon.id;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }

        public bool updateUsersBeacons(User user)
        {           
            var is_registered = "";
            if (user.UserType == User.User_Type.MEMBER)
            {
                is_registered = "0";
            }
            else if(user.UserType == User.User_Type.NON_MEMBER)
            {
                is_registered = "1";
            }

            String query = "UPDATE users_beacons SET is_registered = " + is_registered + ", is_availed = 1 WHERE users_beacons.user_id = @userId AND beacon_id = @beaconId AND users_beacons.offer_id = @offerId";
            WSQuery Query = new WSQuery(query);
            Query.parameters["userId"] = user.id;
            Query.parameters["beaconId"] = user.beacon.id;
            Query.parameters["offerId"] = user.offer.id;
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeUpdateQuery(Query);
        }

        public bool checkIfUserNameExists(String userName)
        {
            String query = "SELECT CASE WHEN EXISTS (Select * from users where username = @userName) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["userName"] = userName;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }

        public User getDetailsFromMemberId(User userInfo)
        {            
            bool isTextSent = false;
            bool isEmailSent = false;
            User user = new User();        
            user = this.getUserSocialLoginDetails(userInfo);
            user.id = userInfo.id;
            user.offer = offerManager.getDetails(user.offer);
            user.memberId = userInfo.memberId;
            user.account.id = userInfo.account.id;
            user.beacon.id = userInfo.beacon.id;
            user.account = accountManager.getDetails(user.account);
            user.offer.code = this.getAlphaNumericCode(8);
            user.contacts = getCantactsForUser(user.id);                    
            String query = "INSERT INTO users_offer_codes(offer_id, user_id, code, status) VALUES(@offerId, @userId, @code, @status);";
            WSQuery Query = new WSQuery(query);
            Query.parameters["offerId"] = user.offer.id;
            Query.parameters["userId"] = user.id;
            Query.parameters["code"] = user.offer.code;
            Query.parameters["status"] = 0;
            bool result = WSDatabaseManager.sharedDatabaseManagerinstance.executeInsertQuery(Query);
            if(result && user.contacts.Count != 0)
            {
                foreach(Contact contact in user.contacts)
                {
                    if(contact.ContactType == Contact.Contact_Type.EMAIL)
                    {
                        isEmailSent = this.sendOfferEmail(user);
                    }
                    if (contact.ContactType == Contact.Contact_Type.PHONE)
                    {
                        contact.country.countryCode = new DALCountryManager().getCountryCode(contact.country.id);
                        try
                        {
                            String message = user.offer.description + ". Your offer code is:" + user.offer.code + ". Your member ID is:" + user.memberId + ".";
                            String To = String.Concat(contact.country.countryCode, contact.value);
                            isTextSent = MessageService.SendSms(To, message);
                        }
                        catch (ApiException Ex)
                        {
                            Console.WriteLine(Ex.ToString());
                        }                       
                    }
                }
                
            }
            if (isEmailSent)
                user.userOfferStatus = 1;
            if (isTextSent)
                user.userOfferStatus = 2;
            if (isEmailSent && isTextSent)
                user.userOfferStatus = 3;
            if (!isEmailSent && !isTextSent)
                user.userOfferStatus = 4;          
            return user;
        }

        public User getUserOfferDetails(User user)
        {
            String query = QueryConstants.USER_OFFER_DETAILS;
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = user.offer.account.id;
            Query.parameters["code"] = user.offer.code;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    user = new User(reader);
                }
            }
            Query.connection.Close();
            user.contacts = getCantactsForUser(user.id);
            user.offer.categories = offerManager.getCategoriesForOffer(user.offer.id);
            user.offer.countries = offerManager.getCountriesForOffer(user.offer.id);
            user.offer.termsAndConditions = offerManager.getTermsAndConditionsForOffer(user.offer.id);
            return user;
        }

        public bool setUserOfferCodeStatus(User user, int status)
        {           
            String query = "update users_offer_codes set status = @status, date = @date where user_id = @userId and offer_id = @offerId and code = @code";
            WSQuery Query = new WSQuery(query);
            Query.parameters["userId"] = user.id;           
            Query.parameters["offerId"] = user.offer.id;
            Query.parameters["code"] = user.offer.code;
            Query.parameters["status"] = status;
            Query.parameters["date"] = DateTime.UtcNow.ToString();
            return WSDatabaseManager.sharedDatabaseManagerinstance.executeUpdateQuery(Query);
        }

        public List<User> getUsersOfferCodeByStatus(Account account, int status)
        {
            String query = QueryConstants.ALL_USERS_OFFER_DETAILS;
            WSQuery Query = new WSQuery(query);
            Query.parameters["accountId"] = account.id;
            Query.parameters["status"] = status;         
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            List<User> usersList = new List<User>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    usersList.Add(new User(reader));
                }
            }
            Query.connection.Close();
            foreach (User user in usersList)
            {
                user.contacts = getCantactsForUser(user.id);
                user.offer.categories = offerManager.getCategoriesForOffer(user.offer.id);
                user.offer.countries = offerManager.getCountriesForOffer(user.offer.id);
                user.offer.termsAndConditions = offerManager.getTermsAndConditionsForOffer(user.offer.id);
            }
            return usersList;                       
        }

        //Multiple Social Login

        public bool verifyMemberId(String memberId, Int64 accountId)
        {
            String query = "SELECT CASE WHEN EXISTS (Select * from users where member_id = @memberId and account_id = @accountId) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["memberId"] = memberId;
            Query.parameters["accountId"] = accountId;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }

        public bool verifyUserUniqueId(String uniqueId, Int64 userId)
        {
            String query = "SELECT CASE WHEN EXISTS (SELECT * FROM users USR JOIN users_social_login USR_SOC_LOG ON USR.id = USR_SOC_LOG.user_id WHERE USR_SOC_LOG.unique_id = @uniqueId and USR.id = @userId) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["uniqueId"] = uniqueId;
            Query.parameters["userId"] = userId;            
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }

        public bool isUserUniqueIdExists(String uniqueId, Int64 accountId)
        {
            String query = "SELECT CASE WHEN EXISTS (SELECT * FROM users USR JOIN users_social_login USR_SOC_LOG ON USR.id = USR_SOC_LOG.user_id WHERE USR_SOC_LOG.unique_id = @uniqueId and USR.account_id = @accountId) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["uniqueId"] = uniqueId;
            Query.parameters["accountId"] = accountId;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }

        public bool isUserEmailIdExists(String emailId, Int64 userId)
        {
            String query = "SELECT CASE WHEN EXISTS (SELECT * FROM contacts CONTACT JOIN users NewUSER ON NewUSER.contact_id = CONTACT.id where CONTACT.value like '@emailId' AND NewUSER.id = @userId) THEN 1 ELSE 0 END AS Result";
            WSQuery Query = new WSQuery(query);
            Query.parameters["emailId"] = emailId;
            Query.parameters["userId"] = userId;
            return Convert.ToBoolean(WSDatabaseManager.sharedDatabaseManagerinstance.executeScalar(Query));
        }


    }
}