﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BNS.Models.Countries;
using BNS.DL.WSDatabase;
using System.Data.SqlClient;
using WSDatabase;

namespace BNS.DAL.DALCountry
{
    public class DALCountryManager
    {
        public List<Country> getAll()
        {
            WSQuery Query = new WSQuery();
            Query.query = QueryConstants.ALL_COUNTRIES;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);

            List<Country> CountriesList = new List<Country>();
            if(reader.HasRows)
            {
                while(reader.Read())
                {
                    CountriesList.Add(new Country(reader));                      
                }               
            }
            Query.connection.Close();
            return CountriesList;
        }

        public List<Country> getCountryCodes()
        {
            WSQuery Query = new WSQuery();
            Query.query = QueryConstants.ALL_COUNTRY_CODE;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);

            List<Country> CountriesList = new List<Country>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    CountriesList.Add(new Country(reader));
                }            
            }
            Query.connection.Close();
            return CountriesList;
        }

        public String getCountryCode(Int64 countryId)
        {
            WSQuery Query = new WSQuery();
            Query.query = "SELECT COUNTRY.country_code AS COUNTRY_CODE FROM Countries COUNTRY WHERE COUNTRY.id = @countryId";
            Query.parameters["countryId"] = countryId;
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);           
            String code = "";
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if (WSDatabaseHelper.isValidField(reader, "COUNTRY_CODE"))                  
                        code = reader.GetString(reader.GetOrdinal("COUNTRY_CODE"));                  
                }
            }
            Query.connection.Close();
            return code;
        }

        public Country getIdFromName(String countryName)
        {
            int id = 0;

            WSQuery Query = new WSQuery();
            Query.query = "SELECT Countries.id AS COUNTRY_ID, currency AS COUNTRY_CURRENCY FROM Countries WHERE Countries.name = '" + countryName + "';";
            SqlDataReader reader = WSDatabaseManager.sharedDatabaseManagerinstance.executeSelectQuery(Query);
            Country country = new Country();
            if(reader.HasRows)
            {
                while(reader.Read())
                {
                    country = new Country(reader);
                }
            }

            return country;
        }
    }
}