angular.module('bns.mobile.landing', [])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
    .state('mobile.landing', {
        url: "/beacon/landing",
        views: {
            "landing": {
                templateUrl: "App/Mobile/landing/landing.html",
                controller: 'landingCtrl'
            },
        }
    });
}])
.controller('landingCtrl', ['$state', '$scope', '$stateParams','$rootScope', 'API_URL', 'API_SERVICE', '$http', function ($state, $scope, $stateParams, $rootScope, API_URL, API_SERVICE, $http) {
    alert($rootScope.beaconId);
    function getAllOffers() {
        function success(data, status, headers, config) {
            $scope.offers = data;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
        }
        var url = "/Offers/getOffersForLandingPage";
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getAllOffers();

    $scope.gotoViewOffer = function (offerId,category) {
        $scope.offer = { offerId: "", category: "" };
        $scope.offer.offerId = offerId;
        $scope.offer.category = category;
        $state.go("mobile.viewOffer", { "offer": $scope.offer });
    }

    $scope.getBackgroundPic = function (category) {
        if (category == "Cash")
        {
            return ("http://localhost:5040/Resources/offerImages/Cash.jpg");
        } else if (category == "Points")
        {
            return ("http://localhost:5040/Resources/offerImages/Points.jpg");
        } else if (category == "Promotions")
        {
            return ("http://localhost:5040/Resources/offerImages/Promotions.jpg");
        }
    }

    $scope.currentDate = new Date();

}])
