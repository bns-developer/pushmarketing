angular.module('bns.master.luckyCharacters', [])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.luckyCharacters', {
        url: '/kiosks/luckyCharacters?kioskId',
        views: {
            "luckyCharacters": {
                templateUrl: 'App/Master/Kiosks/LuckyCharacters/luckyCharacters.html',
                controller: 'luckyCharactersController'
            }
        }
    });
}])
.controller("luckyCharactersController", ['$scope', '$http', 'API_URL', '$stateParams', 'API_SERVICE', 'MEDIA_URL','$state', function ($scope, $http, API_URL, $stateParams, API_SERVICE, MEDIA_URL, $state) {

    $scope.isPresent = false;

    $scope.getLuckyItemPic = function (icon) {
        $scope.imagePath = MEDIA_URL + icon;
        return $scope.imagePath;
    }

    $scope.goSetKioskOffer = function (luckyItemId) {
        $state.go("master.setKioskOffer", { "kioskId": $stateParams.kioskId,"luckyItemId": luckyItemId });
    }

    function getLuckyItemsByKioskId() {
        $scope.loading = true;
        $scope.dataLoader = 0;
        function success(data, status, headers, config) {
            $scope.luckyItemsList = data;
            if ($scope.luckyItemsList.luckyItems.length == 0)
            {
                $scope.isPresent = false;
            }
            else
            {
                $scope.isPresent = true;
            }
            console.log($scope.luckyItemsList);
            $scope.loading = false;
            $scope.dataLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.dataLoader = 2;
        }
        var url = "/LuckyItems/byKioskId?kioskId="+$stateParams.kioskId;
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getLuckyItemsByKioskId();
    }])