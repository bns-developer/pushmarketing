angular.module('bns.mobile.getUserDetail', [])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
    .state('mobile.getUserDetail', {
        url: "/beacon/get-user-details?value",
        views: {
            "getUserDetail": {
                templateUrl: "App/Mobile/getUserDetail/getUserDetail.html",
                controller: 'getUserDetailCtrl'
            },
        },
        params: {
            userOffer:{value:"",category:"",offerId:""}
        }
    });
}])
.controller('getUserDetailCtrl', ['$state', '$scope', '$rootScope', '$stateParams', '$state', 'API_URL', 'API_SERVICE', '$http', function ($state, $scope, $rootScope, $stateParams, $state, API_URL, API_SERVICE, $http) {
    
    $scope.userdetail = {
        email_id: "",
        phone: "",
        country:{
            id: 1
        },
        registered_from: 1,
        beacon:{
            id: $rootScope.beaconId
        },
        offer:{
            id: $stateParams.userOffer.offerId
        }
    }
    $scope.submitted = false;

    function init() {
        $scope.value = $stateParams.userOffer.value;
        if ($stateParams.userOffer.category == "Cash") {
            $scope.imgsrc = "http://localhost:5040/Resources/offerImages/Cash.jpg";
        } else if ($stateParams.userOffer.category == "Points") {
            $scope.imgsrc = ("http://localhost:5040/Resources/offerImages/Points.jpg");
        } else if ($stateParams.userOffer.category == "Promotions") {
            $scope.imgsrc = ("http://localhost:5040/Resources/offerImages/Promotions.jpg");
        }
    }
    init();

    $scope.submitForm = function (isValid) {
        $scope.submitted = true;
        if (isValid)
        {
                function success(data, status, headers, config) {
                    $scope.submitted = false;
                    $state.go('mobile.congoNewUser', { 'offerId': $stateParams.userOffer.offerId });
                }
                function failure(data, status, headers, config) {
                    $scope.submitted = false;
                }
                var url = "/users/registration";
                API_SERVICE.postData($scope, $http, $scope.userdetail, url, success, failure);
        }
    }

    $scope.currentDate = new Date();

    }])
