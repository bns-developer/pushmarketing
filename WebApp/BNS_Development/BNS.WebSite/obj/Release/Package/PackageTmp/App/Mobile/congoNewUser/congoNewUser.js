angular.module('bns.mobile.congoNewUser', [])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider
    .state('mobile.congoNewUser', {
        url: "/beacon/congo-new-user",
        views: {
            "congoNewUser": {
                templateUrl: "App/Mobile/congoNewUser/congoNewUser.html",
                controller: 'congoNewUserCtrl'
            },
        },
        params: {
            offerId:""
        }
    });
}])
.controller('congoNewUserCtrl', ['$state', '$scope', '$rootScope', '$stateParams', function ($state, $scope, $rootScope, $stateParams) {

    $scope.value = $stateParams.offerId;

    $scope.currentDate = new Date();

    }])
