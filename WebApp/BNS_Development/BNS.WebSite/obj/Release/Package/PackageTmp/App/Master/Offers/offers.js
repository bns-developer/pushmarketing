angular.module('bns.master.offers', ['oc.lazyLoad'])

.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('master.offers', {
        url: '/offers',
        views: {
            "offers": {
                templateUrl: 'App/Master/Offers/offers.html',
                controller: 'offersController'
            }
        }

    });
}])
    .directive('fileInput', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attributes) {
                element.bind('change', function () {
                    $parse(attributes.fileInput)
                    .assign(scope, element[0].files)
                    scope.$apply()
                });
            }
        };
    }])
.controller("offersController", ['$scope', '$http', 'API_URL', 'ngDialog', 'API_SERVICE', function ($scope, $http, API_URL, ngDialog, API_SERVICE) {

    $scope.submitted = false;
    $scope.dateError = false;
    $scope.successAlert = false;
    $scope.faliureAlert = false;
    $scope.image = "";
    var today = new Date();
    today.setHours(0, 0, 0, 0);

    $('#offerDatetime').daterangepicker({

        timePicker: true,
        timePickerIncrement: 1,
        format: 'MM/DD/YYYY h:mm:ss A',
        minDate: today,
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear'
        },
        "startDate": new Date(),
        "endDate": new Date()
    });
    $scope.newOffer = {
        "categories": "",
        "countries": "",
        "title": "",
        "description": "",
        "startTime": "",
        "endTime": "",
        "number": "",
        "imagePath": "",
        "value": ""
    };

    //function getTodaysDate()
    //{
    //    var today = new Date();
    //    var dd = today.getDate();
    //    var mm = today.getMonth() + 1; //January is 0! 
    //    var yyyy = today.getFullYear();
    //    if (dd < 10) { dd = '0' + dd }
    //    if (mm < 10) { mm = '0' + mm }
    //    var today = dd + '/' + mm + '/' + yyyy;
    //    return today;
    //}

    //------------------ here we will create category object-----------//
    $scope.category = {
        "id": "",
        "value": ""
    };

    var table_advanced = function () {

        var handleDatatable = function () {
            var spinner = $(".spinner").spinner();
            var table = $('#table_id').dataTable({
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "order": [],
                "aoColumnDefs": [{
                    'bSortable': false,
                    'aTargets': [0]
                },
                {
                    'bSortable': false,
                    'aTargets': [9]
                }]
            });

            var tableTools = new $.fn.dataTable.TableTools(table, {
                "sSwfPath": "libs/assets/vendors/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                "buttons": [
                    "copy",
                    "csv",
                    "xls",
                    "pdf",
                    { "type": "print", "buttonText": "Print me!" }
                ]
            });
            $(".DTTT_container").css("float", "right");

        };
        return {
            init: function () {
                handleDatatable();
            }
        };
    }(jQuery);

    setTimeout(function () {
        //$('.selectpicker').selectpicker({
        //    iconBase: 'fa',
        //    tickIcon: 'fa-check'
        //});
        table_advanced.init();
        $('input[name="daterangepicker-date-time"]').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' });

    }, 500);

    $scope.addOffer = function () {
        ngDialog.open({
            template: 'addOfferDialog',
            scope: $scope,
            preCloseCallback: function () { $scope.clearModal() }
        });
    }

    $scope.closeModal = function () {
        alert("Cancel Clicked...!!!");
        ngDialog.close({
            template: 'addStoreDialog',
            scope: $scope
        });
        $scope.clearModal();
    }

    $scope.clearModal = function () {
        $scope.submitted = false;
        $scope.successAlert = false;
        $scope.faliureAlert = false;
        $scope.date = "";
        $scope.newOffer = {
            "categories": [],
            "countries": [],
            "title": "",
            "description": "",
            "startTime": "",
            "endTime": "",
            "number": "",
            "imagePath": "",
            "value": ""
        };
        $('#offerDatetime').val('');
        $scope.dateError = false;
    }

    $scope.checkDate = function () {
        $scope.newOffer.startTime = ($('#offerDatetime').val()).split('-')[0];
        $scope.newOffer.endTime = ($('#offerDatetime').val()).split('-')[1];
        if ($scope.newOffer.startTime == "" || $scope.newOffer.endTime == "" || $scope.newOffer.startTime == undefined || $scope.newOffer.endTime == undefined) {
            $scope.dateError = true;
        }
        else {
            $scope.dateError = false;
        }
    }

    $scope.saveNewOffer = function () {
        console.log($('#offerDatetime').val());
        console.log($scope.image.file);
        $scope.submitted = true;

        //var tempArray = [];
        //var countries = $scope.countries;
        //for (var i = 0; i < countries.length ; i++)
        //{
        //    var temp = {"id": countries[i]};

        //    tempArray.push(temp);
        //}
        //$scope.countries = tempArray;
        $scope.newOffer.startTime = ($('#offerDatetime').val()).split('-')[0];
        $scope.newOffer.endTime = ($('#offerDatetime').val()).split('-')[1];
        $scope.newOffer.imagePath = $scope.image[0];
        if ($scope.newOffer.categories == "" || $scope.newOffer.categories == undefined || $scope.newOffer.countries == "" || $scope.newOffer.countries == undefined || $scope.newOffer.title == ""
            || $scope.newOffer.title == undefined || $scope.newOffer.description == "" || $scope.newOffer.description == undefined || $scope.newOffer.startTime == "" || $scope.newOffer.startTime == undefined
            || $scope.newOffer.endTime == "" || $scope.newOffer.endTime == undefined) {
            if ($scope.newOffer.startTime == "" || $scope.newOffer.startTime == undefined
            || $scope.newOffer.endTime == "" || $scope.newOffer.endTime == undefined) {
                $scope.dateError = true;
            }
            else {
                $scope.dateError = false;
            }
        }
        else {
            $scope.dateError = false;
            function success(data, status, headers, config) {
                getAllOffersList();
                $scope.submitted = false;
                $scope.successAlert = true;
                $scope.faliureAlert = false;
                $scope.newOffer = {
                    "category": [],
                    "country": [],
                    "title": "",
                    "description": "",
                    "startTime": "",
                    "endTime": "",
                    "number": "",
                    "imagePath": "",
                    "value": ""
                };
                $('#offerDatetime').val('');
                $scope.loading = false;
            }
            function failure(data, status, headers, config) {
                $scope.loading = false;
                $scope.successAlert = false;
                $scope.faliureAlert = true;
                if (status == 409) {
                    $scope.errorMessage = data;
                }
                else {
                    $scope.errorMessage = "Could not add offer to the list. Please check all the fields."
                }
            }
            var url = "/Offers/addOffer";
            //API_SERVICE.postMultiPartData($scope, $http, $scope.newOffer, url, success, failure);
            API_SERVICE.postData($scope, $http, $scope.newOffer, url, success, failure);
        }
    }

    function getAllOffersList() {
        $scope.loading = true;
        $scope.dataLoader = 0;
        function success(data, status, headers, config) {
            $scope.offers = data;
            console.log($scope.offers);
            $scope.loading = false;
            $scope.dataLoader = 1;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
            $scope.dataLoader = 2;
        }
        var url = "/Offers/all";
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getAllOffersList();

    function getAllCountries() {
        $scope.loading = true;
        function success(data, status, headers, config) {
            $scope.countries = data;
            console.log($scope.stores);
            $scope.loading = false;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
        }
        var url = "/countries/all";
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getAllCountries();

    function getAllCategories() {
        $scope.loading = true;
        function success(data, status, headers, config) {
            //$scope.categories = data;
            setTimeout(function () {
                $scope.newOffer.categories = data;
            }, 0);
            console.log($scope.categories);
            $scope.loading = false;
        }
        function failure(data, status, headers, config) {
            $scope.loading = false;
        }
        var url = "/Categories/all";
        API_SERVICE.getData($scope, $http, url, success, failure);
    }
    getAllCategories();

    $scope.reloadOfferList = function () {
        getAllOffersList();
    }

    $scope.onSelectListChange = function () {
        if ($scope.selectedCategories == undefined)
            return;
        for (var index in $scope.newOffer.categories) {
            if (isSelectedChecked($scope.newOffer.categories[index].id))
                $scope.newOffer.categories[index].is_selected = true;
            else
                $scope.newOffer.categories[index].is_selected = false;
        }
    }

    function isSelectedChecked(id) {
        for (var selectedCategory in $scope.selectedCategories) {
            if (id == $scope.selectedCategories[selectedCategory]) {
                return true;
            }
        }
        return false;
    }

}]);